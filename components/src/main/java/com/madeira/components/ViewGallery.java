package com.madeira.components;

import android.content.Context;
import android.content.res.ColorStateList;
import android.net.Uri;
import android.os.Debug;
import android.support.v7.widget.CardView;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.MediaController;
import android.widget.VideoView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

public class ViewGallery extends LinearLayout {
    private static final String TAG = "ViewGallery";

    private static final int MEDIA_TYPE_IMAGE = 100;
    private static final int MEDIA_TYPE_VIDEO = 200;

    FrameLayout containerImage;

    CardView cardView;
    ImageView imageView1;
    //    ImageView imageView2;
    ImageView imageViewLeft;
    ImageView imageViewRight;
    VideoView videoView;

    ImageView frontImage;
//    ImageView backImage;

    List<String> listImage = new ArrayList<>();
    List<Integer> listMediaType = new ArrayList<>();

    int currentIndex;
    IViewGallery callback;

    public interface IViewGallery {
        void onClickPrev(int newIndex);

        void onClickNext(int newIndex);

        void onClickImage(int index);

        void unFocused();

    }

    public ViewGallery(Context context) {
        super(context);
    }

    public ViewGallery(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public ViewGallery(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {

        inflate(getContext(), R.layout.view_gallery, this);

        bind();

        setupButton();
    }

//    @Override
//    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
//        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
//
////        Log.i(TAG, "onMeasure");
//
//        int width, height;
//
////        int mode = MeasureSpec.getMode(widthMeasureSpec);
////        width = MeasureSpec.getSize(widthMeasureSpec);
////        height = MeasureSpec.getSize(heightMeasureSpec);
//
////        Log.i(TAG, "onMeasure " + width + " x " + height);
//
////        float ratio = 1;
//
//        int mwidth = imageView1.getMeasuredWidth();
//        int mheight = imageView1.getMeasuredHeight();
//        width = MeasureSpec.getSize(mwidth);
//        height = MeasureSpec.getSize(mheight);
//        float ratio = (float) width / (float) height;
//
//
////        660 x 362 = 1.8232044 (390)
//
////        Log.i(TAG, "onMeasure imageView1 " + width + " x " + height + " = " + ratio);
//
////        float ratioView = (float)height / (float)width;
////
////        // 726 x 388
////        float newWidth;
////        float newHeight;
////
////        if (ratio > ratioView){
////            newWidth = height * (1/ratio);
////            newHeight = height;
////        }else{
////            newHeight = width * ratio;
////            newWidth = width;
////        }
////
////        Log.i(TAG, "newSize\n" + newWidth +" x "+ newHeight);
////
////        LinearLayout.LayoutParams lp = (LinearLayout.LayoutParams) containerImage.getLayoutParams();
////        lp.height = (int) newHeight;
////        lp.width = (int) newWidth;
////        containerImage.setLayoutParams(lp);
//    }

    public void setCallback(IViewGallery callback) {
        try {
            this.callback = callback;
        } catch (Exception ex) {

            ex.printStackTrace();
        }
    }

    public int getCurrentIndex() {
        return currentIndex;
    }

    public void setColorFocus(int color) {

        try {
            ColorStateList colorStateList = createColorStateList(color, color);

            imageViewLeft.setBackgroundTintList(colorStateList);
            imageViewRight.setBackgroundTintList(colorStateList);
            containerImage.setBackgroundTintList(colorStateList);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void bind() {

        try {
            containerImage = (FrameLayout) findViewById(R.id.container_image);

            cardView = (CardView) findViewById(R.id.card_view);

            imageView1 = (ImageView) findViewById(R.id.image_view_1);
//        imageView2 = findViewById(R.id.image_view_2);
            videoView = (VideoView) findViewById(R.id.video_view);

            imageViewLeft = (ImageView) findViewById(R.id.image_left);
            imageViewRight = (ImageView) findViewById(R.id.image_right);

            frontImage = imageView1;
//        backImage = imageView1;

            imageViewLeft.setVisibility(INVISIBLE);
            imageViewRight.setVisibility(INVISIBLE);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void setupButton() {

        containerImage.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
//                Log.i(TAG, "iamge click");
                try {
                    callback.onClickImage(currentIndex);
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        });

        imageViewLeft.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
//                Log.i(TAG, "onClick Left");
                try {
                    prevImage();
                    callback.onClickPrev(currentIndex);
                } catch (Exception ex) {
                    ex.printStackTrace();
                }

            }
        });

        imageViewRight.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
//                Log.i(TAG, "onClick Right");
                try {
                    nextImage();
                    callback.onClickNext(currentIndex);
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        });

        containerImage.setOnFocusChangeListener(new OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                try {
                    if (!hasFocus) {

                        if (imageViewLeft.getVisibility() == View.INVISIBLE) {
                            callback.unFocused();
                        }

                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }

            }
        });
        imageViewLeft.setOnFocusChangeListener(new OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                try {
                    if (!hasFocus)
                        callback.unFocused();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }

            }
        });


    }

    public void reset() {
        try {
            listImage.clear();
            listMediaType.clear();
            currentIndex = 0;

            if (videoView.isPlaying()) videoView.stopPlayback();
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

    public void addImage(String urlImage) {
        try {
            listImage.add(urlImage);
            listMediaType.add(MEDIA_TYPE_IMAGE);

            if (listImage.size() == 1) {
                setImage(0);
            }

            updateButtonVisibility(false);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void addVideo(String urlVideo) {
        try {
            listImage.add(urlVideo);
            listMediaType.add(MEDIA_TYPE_VIDEO);

            if (listImage.size() == 1) {
                setImage(0);
            }

            updateButtonVisibility(false);
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

    private void setImage(int index) {

        try {

            String url = listImage.get(index);

            Log.i(TAG, url);

            if (listMediaType.get(index) == MEDIA_TYPE_IMAGE) {

                Glide.with(getContext()).load(url).into(frontImage);
                frontImage.bringToFront();
                frontImage.setVisibility(VISIBLE);
                videoView.setVisibility(INVISIBLE);

                if (videoView.isPlaying()) videoView.stopPlayback();

            } else {

                if (videoView.isPlaying()) videoView.stopPlayback();
                MediaController mc = new MediaController(getContext());
                mc.setAnchorView(videoView);
                videoView.setMediaController(mc);
                videoView.setFocusable(true);
                videoView.requestFocus();
                videoView.setVideoURI(Uri.parse(url));
                videoView.start();

                videoView.bringToFront();
                videoView.setVisibility(VISIBLE);
                frontImage.setVisibility(INVISIBLE);
            }

            currentIndex = index;
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void prevImage() {

        try {
            ImageView temp;

            int prevIdx = currentIndex - 1;

            //next image tdk ada, exit
            if (prevIdx < 0) return;

            setImage(prevIdx);

            updateButtonVisibility(true);
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

    private void nextImage() {

        try {
            ImageView temp;

            int nextIdx = currentIndex + 1;

            //next image tdk ada, exit
            if (nextIdx >= listImage.size()) {
                return;
            }

            setImage(nextIdx);

            updateButtonVisibility(true);
        }catch (Exception ex)
        {
            ex.printStackTrace();
        }

    }

    void updateButtonVisibility(boolean canSetFocus) {

        try {
            //check apakah sudah idx ke 0 ?
            //kalo ya maka tombol kiri tdk ada
            //
            if (currentIndex > 0) {
                imageViewLeft.setVisibility(VISIBLE);
            } else {
                imageViewLeft.setVisibility(INVISIBLE);
                if (canSetFocus) imageViewRight.requestFocus();
            }

            //check apakah masih ada next image ?
            //kalo masih ada tempilkan button kanan
            if (currentIndex + 1 < listImage.size()) {
                imageViewRight.setVisibility(VISIBLE);
            } else {
                imageViewRight.setVisibility(INVISIBLE);
                if (canSetFocus) imageViewLeft.requestFocus();
            }
        }catch (Exception ex)
        {
            ex.printStackTrace();
        }

    }

    public ColorStateList createColorStateList(int selectionColorSelected, int selectionColorFocus) {
        return new ColorStateList(
                new int[][]{
                        new int[]{android.R.attr.state_selected},
                        new int[]{android.R.attr.state_focused}
                },
                new int[]{
                        selectionColorSelected,
                        selectionColorFocus
                }
        );

    }


}

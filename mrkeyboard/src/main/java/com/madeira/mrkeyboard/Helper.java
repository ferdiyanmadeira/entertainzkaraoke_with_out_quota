package com.madeira.mrkeyboard;

import com.madeira.library.RootUtil;

public class Helper {

    static public void installKeyboard(String applicationId){
        install(applicationId, KeyboardService.class.getName());
    }

    static public void installDefaultKeyboard(){
        //com.android.inputmethod.latin/.LatinIME
        install("com.android.inputmethod.latin", ".LatinIME");
    }

    static public void install(String applicationId, String packageName){
        try {
            String id = applicationId + "/" + packageName;

            //enable keyboard
            RootUtil.su("ime enable " + id);

            //set active
            RootUtil.su("ime set " + id);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}

/*
 * Copyright (C) 2008-2009 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.madeira.mrkeyboard;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.inputmethodservice.Keyboard;
import android.inputmethodservice.Keyboard.Key;
import android.inputmethodservice.KeyboardView;
import android.util.AttributeSet;
import android.util.Log;
import android.view.inputmethod.InputMethodSubtype;

import java.util.ArrayList;
import java.util.List;

public class LatinKeyboardView extends KeyboardView {
    private static final String TAG = "LatinKeyboardView";

    static final int KEYCODE_OPTIONS = -100;
    // TODO: Move this into android.inputmethodservice.Keyboard
    static final int KEYCODE_LANGUAGE_SWITCH = -101;

    private static final int FOCUS_DIRECTION_LEFT = 1;
    private static final int FOCUS_DIRECTION_UP = 2;
    private static final int FOCUS_DIRECTION_RIGHT = 3;
    private static final int FOCUS_DIRECTION_DOWN = 4;

//    int currentFocusKey = -1;
    NavMap currentKey;

    class NavMap {
        public Key key;
        public int index;
        public int indexLeft = -1;
        public int indexRight = -1;
        public int indexUp = -1;
        public int indexDown = -1;
    }

    List<NavMap> listNavMap;
//    int matrixY; //row
//    int matrixX; //col

    private OnDpadAction callback;

    public interface OnDpadAction{
        void onCenterKey(int primaryCode, int[] keyCodes);
    }

    public LatinKeyboardView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public LatinKeyboardView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    public void setKeyboard(Keyboard keyboard) {
        super.setKeyboard(keyboard);

        //lakukan pemetaan dari 1d array ke 2d array

        List<Key> keys = keyboard.getKeys();

        //create baru setiap kali set keyboard
        List<List<NavMap>> matrixKeys; //index dari keys, dalam 2 dimensi

        matrixKeys = new ArrayList<>();
        listNavMap = new ArrayList<>();

        int y=-1;
        int rowIndex=-1;

        for (int idx=0; idx<keys.size(); idx++) {
            Key key = keys.get(idx);

            NavMap navMap = new NavMap();
            navMap.index = idx;
            navMap.key = key;
            listNavMap.add(navMap);

            //row di tentukan dari nilai y
            if (key.y!=y){
                //buat row baru untuk nampung semua key pada y yg sama
                matrixKeys.add(new ArrayList<NavMap>());
                y = key.y;
                rowIndex = rowIndex +1;
            }

            List<NavMap> row = matrixKeys.get(rowIndex);
            row.add(navMap);

        }

        //build navigation map
        buildNavigationMap(matrixKeys);

        //reset setiap kali set keyboard
        currentKey = null;
    }

    @Override
    protected boolean onLongPress(Key key) {
        if (key.codes[0] == Keyboard.KEYCODE_CANCEL) {
            getOnKeyboardActionListener().onKey(KEYCODE_OPTIONS, null);
            return true;
        /*} else if (key.codes[0] == 113) {

            return true; */
        } else {
            //Log.d("LatinKeyboardView", "KEY: " + key.codes[0]);
            return super.onLongPress(key);
        }
    }

    @Override
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        Drawable keyBackground = getResources().getDrawable( R.drawable.key_background);
        keyBackground.setState(new int[]{android.R.attr.state_focused});

        List<Key> keys = getKeyboard().getKeys();
        for(int idx=0; idx<keys.size(); idx++) {

            if (currentKey!=null && currentKey.index==idx){
                Key key = keys.get(idx);
                keyBackground.setBounds(key.x, key.y, key.x+key.width, key.y+key.height);
                keyBackground.draw(canvas);
            }
        }
    }

    public void setDpadActionListener(OnDpadAction callback){
        this.callback = callback;
    }

    void setSubtypeOnSpaceKey(final InputMethodSubtype subtype) {
        final LatinKeyboard keyboard = (LatinKeyboard)getKeyboard();
        //keyboard.setSpaceIcon(getResources().getDrawable(subtype.getIconResId()));
        invalidateAllKeys();
    }

    public boolean onDpadLeft(){
        int r = focus1stKey();
        if (r==1) return true;
        if (r==-1) return false;

        if (currentKey==null) return false;

//        Log.i(TAG, "onDpadLeft: " + currentKey.key.label + " left=" + currentKey.indexLeft);

        //exit apabila prevkey tdk ada
        int nextIndex = currentKey.indexLeft;
        if (nextIndex<0) return false;

        currentKey = listNavMap.get(nextIndex);
        invalidate();
        return true;
    }

    public boolean onDpadRight() {
        int r = focus1stKey();
        if (r==1) return true;
        if (r==-1) return false;

//        Log.i(TAG, "onDpadRight: " + currentKey.key.label + " left=" + currentKey.indexRight);

        //exit apabila prevkey tdk ada
        int nextIndex = currentKey.indexRight;

        if (nextIndex<0) return false;

        currentKey = listNavMap.get(nextIndex);
        invalidate();
        return true;
    }

    public boolean onDpadUp(){
        int r = focus1stKey();
        if (r==1) return true;
        if (r==-1) return false;

        if (currentKey==null) return false;

        int nextIndex = currentKey.indexUp;

        if (nextIndex<0) return false;

        currentKey = listNavMap.get(nextIndex);
        invalidate();
        return true;
    }

    public boolean onDpadDown(){
        int r = focus1stKey();
        if (r==1) return true;
        if (r==-1) return false;

        if (currentKey==null) return false;

        Log.i(TAG, "onDpadDown: " + currentKey.key.label + " " + currentKey.indexDown);

        int nextIndex = currentKey.indexDown;

        if (nextIndex<0) return false;

        currentKey = listNavMap.get(nextIndex);
        invalidate();
        return true;
    }

    public void onDpadCenter(){
        try {
            if (currentKey!=null && callback!=null) {
                callback.onCenterKey(currentKey.key.codes[0], currentKey.key.codes);
            }
        } catch (Exception e) {
//            e.printStackTrace();
        }
    }

    /**
     * focus pada key pertama apabila blm ada focus
     *
     * @return -1 = fail, 1= success, 0=already focus to other key
     */
    private int focus1stKey(){

        //lakukan focus pada key pertama apabila blm ada focus
        if (currentKey==null){
            if (listNavMap==null) return -1;

            currentKey = listNavMap.get(0);
            invalidate();

            return 1;
        }

        return 0;
    }

    private void buildNavigationMap(List<List<NavMap>> matrixKeys){

        //exit apabila matrix key blm di buat
        if (matrixKeys==null) return;

        List<Key> keys = getKeyboard().getKeys();

        //buat navmap utk LEFT
        for (List<NavMap> list: matrixKeys) {

            int preKey = 0;

            for(int idx=0; idx<list.size(); idx++){
                NavMap navMap = list.get(idx);
                Key key = navMap.key;

                //key pertama dalam array
                if (idx==0){
                    //key tdk pada x==0, jadi ada key di sebelah kiri
                    if (key.x>0){
                        navMap.indexLeft = findPrevKey(key, keys);
                    }
                    preKey = navMap.index;
                } else {
                    //key selanjut nya dalam row yg sama
                    navMap.indexLeft = preKey;
                    preKey = navMap.index;
                }

//                Log.i(TAG, "buildNavigationMap: " + navMap.key.label + " " + navMap.indexLeft);
            }
        }

        //cari index utk NAV RIGHT, semua next key
        for (List<NavMap> list: matrixKeys) {

            for(int idx=0; idx<list.size(); idx++){
                NavMap navMap = list.get(idx);

                //apakah ada next key
                if (list.size()>idx+1){
                    navMap.indexRight = list.get(idx+1).index;
                } else {
                    navMap.indexRight = findNextKey(navMap.key, keys);
                }
            }
        }

        //cari next key DOWN
        for (int row=0; row<matrixKeys.size(); row++) {
            List<NavMap> list = matrixKeys.get(row);

            for(int idx=0; idx<list.size(); idx++){
                NavMap navMap = list.get(idx);

                //last row tdk perlu cari index down
                if (row>=matrixKeys.size()){
                    continue; //tdk ada next row, continue
                }

                navMap.indexDown = findNextDown(navMap.key, keys);

//                Log.i(TAG, "indexDown " + navMap.key.label + " index=" + navMap.indexDown);
            }

        }

        //cari prev key UP
        for (int row=0; row<matrixKeys.size(); row++) {
            List<NavMap> list = matrixKeys.get(row);

            for(int idx=0; idx<list.size(); idx++){
                NavMap navMap = list.get(idx);

                //ini adalah row pertama, jadi tdk perlu set indexup
                if (row==0){
                    continue; //tdk ada next row, continue
                }

                navMap.indexUp = findPrevUp(navMap.key, keys);

//                Log.i(TAG, "indexDown " + navMap.key.label + " index=" + navMap.indexDown);
            }

        }






    }

    /**
     * cari dari array key, key terdekat di sebelah kiri
     *
     * @param key
     * @return
     */
    private int findPrevKey(Key key, List<Key> keys){

        int dd;
        int ddChosen = Integer.MAX_VALUE;
        int idxChosen = -1;

        for (int idx=0; idx<keys.size(); idx++) {
            Key searchKey = keys.get(idx);

            //hanya key yg ada di sebelah kiri dari key anchor
            if (searchKey.x < key.x){

                //formula dd ini hanya utk compare, bukan formula utk cari distance
                dd = (key.x-searchKey.x) * (key.x-searchKey.x) + (key.y-searchKey.y)*(key.y-searchKey.y);
                if (dd<ddChosen) {
                    ddChosen = dd;
                    idxChosen = idx;
                }
            }
        }

        return idxChosen;
    }
    private int findNextKey(Key key, List<Key> keys) {

        int dd;
        int ddChosen = Integer.MAX_VALUE;
        int idxChosen = -1;

        for (int idx = 0; idx < keys.size(); idx++) {
            Key searchKey = keys.get(idx);

            //hanya key yg ada di sebelah kiri dari key anchor
            if (searchKey.x > key.x) {

                //formula dd ini hanya utk compare, bukan formula utk cari distance
                dd = (key.x - searchKey.x) * (key.x - searchKey.x) + (key.y - searchKey.y) * (key.y - searchKey.y);
                if (dd < ddChosen) {
                    ddChosen = dd;
                    idxChosen = idx;
                }
            }
        }

        return idxChosen;
    }

    private int findNextDown(Key key, List<Key> keys){
        int dd;
        int ddChosen = Integer.MAX_VALUE;
        int idxChosen = -1;

//        Log.i(TAG, "findNextDown: " + key.label + "(" + key.x +"," + key.y + ")" );
        for (int idx = 0; idx < keys.size(); idx++) {
            Key searchKey = keys.get(idx);

            //cari key yg ada di bawah
            if (searchKey.y > key.y) {

                //formula dd ini hanya utk compare, bukan formula utk cari distance
                dd = (key.x - searchKey.x) * (key.x - searchKey.x) + (key.y - searchKey.y) * (key.y - searchKey.y);
                if (dd < ddChosen) {
//                    Log.i(TAG, "\tidx=" + searchKey.label+ "(" + searchKey.x +"," + searchKey.y + ") = " + dd );
                    ddChosen = dd;
                    idxChosen = idx;
                }
            }
        }
        return idxChosen;
    }

    private int findPrevUp(Key key, List<Key> keys){
        int dd;
        int ddChosen = Integer.MAX_VALUE;
        int idxChosen = -1;

//        Log.i(TAG, "findNextDown: " + key.label + "(" + key.x +"," + key.y + ")" );
        for (int idx = 0; idx < keys.size(); idx++) {
            Key searchKey = keys.get(idx);

            //cari key yg ada di bawah
            if (searchKey.y < key.y) {

                //formula dd ini hanya utk compare, bukan formula utk cari distance
                dd = (key.x - searchKey.x) * (key.x - searchKey.x) + (key.y - searchKey.y) * (key.y - searchKey.y);
                if (dd < ddChosen) {
//                    Log.i(TAG, "\tidx=" + searchKey.label+ "(" + searchKey.x +"," + searchKey.y + ") = " + dd );
                    ddChosen = dd;
                    idxChosen = idx;
                }
            }
        }
        return idxChosen;
    }

}

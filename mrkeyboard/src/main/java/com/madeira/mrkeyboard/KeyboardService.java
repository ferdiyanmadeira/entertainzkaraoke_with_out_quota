package com.madeira.mrkeyboard;

import android.inputmethodservice.InputMethodService;
import android.inputmethodservice.Keyboard;
import android.inputmethodservice.KeyboardView;
import android.media.AudioManager;
import android.util.DebugUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.InputConnection;
import android.view.inputmethod.InputMethodManager;

public class KeyboardService extends InputMethodService {
    private static final String TAG = "KeyboardService";

    private static final int CODE_CLEAR = -100;
//    private KeyboardView keyboardView;
//    private Keyboard keyboard;

    private boolean caps = false;

    private InputMethodManager mInputMethodManager;
    private LatinKeyboardView mInputView;

    private LatinKeyboard keyboardSymbol;
    private LatinKeyboard keyboardAbc;

    private LatinKeyboard mCurKeyboard;


    @Override
    public void onCreate() {
        super.onCreate();

        mInputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);

    }

    /**
     * This is the point where you can do all of your UI initialization.  It
     * is called after creation and any configuration change.
     */
    @Override
    public void onInitializeInterface() {

        keyboardAbc = new LatinKeyboard(this, R.xml.keyboard_layout_abc);
        keyboardSymbol = new LatinKeyboard(this, R.xml.keyboard_layout_symbol);

    }

    @Override
    public View onCreateInputView() {
        View root = getLayoutInflater().inflate(R.layout.keyboard_view, null);

        mInputView = root.findViewById(R.id.keyboard_view);
        mInputView.setOnKeyboardActionListener(onKeyboardActionListener);
        mInputView.setPreviewEnabled(false);

        setLatinKeyboard(keyboardAbc);

        mInputView.setDpadActionListener(new LatinKeyboardView.OnDpadAction() {
            @Override
            public void onCenterKey(int primaryCode, int[] keyCodes) {
                processKey(primaryCode, keyCodes);
            }
        });

        return root;
    }

    /**
     * disable fullscreen
     *
     * @return
     */
    @Override
    public boolean onEvaluateFullscreenMode() {
        return false;
    }

    private void playClick(int keyCode) {
        AudioManager am = (AudioManager) getSystemService(AUDIO_SERVICE);
        switch (keyCode) {
            case 32:
                am.playSoundEffect(AudioManager.FX_KEYPRESS_SPACEBAR);
                break;
            case Keyboard.KEYCODE_DONE:
            case 10:
                am.playSoundEffect(AudioManager.FX_KEYPRESS_RETURN);
                break;
            case Keyboard.KEYCODE_DELETE:
                am.playSoundEffect(AudioManager.FX_KEYPRESS_DELETE);
                break;
            default:
                am.playSoundEffect(AudioManager.FX_KEYPRESS_STANDARD);
        }
    }

    private void setLatinKeyboard(LatinKeyboard nextKeyboard) {
//        final boolean shouldSupportLanguageSwitchKey =
//                mInputMethodManager.shouldOfferSwitchingToNextInputMethod(getToken());
//        nextKeyboard.setLanguageSwitchKeyVisibility(shouldSupportLanguageSwitchKey);
        mInputView.setKeyboard(nextKeyboard);

        mCurKeyboard = nextKeyboard;
    }

//    private IBinder getToken() {
//        final Dialog dialog = getWindow();
//        if (dialog == null) {
//            return null;
//        }
//        final Window window = dialog.getWindow();
//        if (window == null) {
//            return null;
//        }
//        return window.getAttributes().token;
//    }

    /**
     * Event ini di generate oleh hardware keyboard
     *
     * @param keyCode
     * @param event
     * @return
     */
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {

        //proses saat soft keyboard tampil
        if (isInputViewShown()) {

            //semua key yg di prosess return true
            if (canProcessKey(keyCode, event)) return true;
        }

        return super.onKeyDown(keyCode, event);
    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {

        //proses saat soft keyboard tampil
        if (isInputViewShown() && canProcessKey(keyCode, event)) {

            //dispatched to keyboardview, hanya saat keyup bukan keydown
            dispatchKey(keyCode);

            return true;
        }

        return super.onKeyUp(keyCode, event);
    }

    /**
     * hanya key utk arraw dan center dpad yg di process
     *
     * @param keyCode
     * @param event
     * @return
     */
    private boolean canProcessKey(int keyCode, KeyEvent event) {

//        Log.i(TAG, "canProcessKey: " + event.toString());

        switch (keyCode) {
            case KeyEvent.KEYCODE_DPAD_DOWN:
                return true;
            case KeyEvent.KEYCODE_DPAD_UP:
                return true;
            case KeyEvent.KEYCODE_DPAD_LEFT:
                return true;
            case KeyEvent.KEYCODE_DPAD_RIGHT:
                return true;
            case KeyEvent.KEYCODE_DPAD_CENTER:
            case KeyEvent.KEYCODE_ENTER:
                return true;
        }

        return false;
    }

    /**
     * lempar key ke keyboard view
     *
     * @param keyCode
     */
    private void dispatchKey(int keyCode) {
        if (mInputView == null) return;

        switch (keyCode) {
            case KeyEvent.KEYCODE_DPAD_LEFT:
                mInputView.onDpadLeft();
                break;
            case KeyEvent.KEYCODE_DPAD_RIGHT:
                mInputView.onDpadRight();
                break;
            case KeyEvent.KEYCODE_DPAD_UP:
                mInputView.onDpadUp();
                break;
            case KeyEvent.KEYCODE_DPAD_DOWN:
                mInputView.onDpadDown();
                break;
            case KeyEvent.KEYCODE_DPAD_CENTER:
            case KeyEvent.KEYCODE_ENTER:
                mInputView.onDpadCenter();
                break;
        }
    }

    private void processKey(int primaryCode, int[] keyCodes) {
        InputConnection ic = getCurrentInputConnection();
        playClick(primaryCode);

        Log.i(TAG, "onKey: " + primaryCode);

        switch (primaryCode) {
            case Keyboard.KEYCODE_DELETE:
                try {
                    ic.deleteSurroundingText(1, 0);
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
                break;
            case Keyboard.KEYCODE_SHIFT:
                try {
                    caps = !caps;
                    mCurKeyboard.setShifted(caps);
                    mInputView.invalidateAllKeys();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
                break;
            case Keyboard.KEYCODE_DONE:
                try {
                    ic.sendKeyEvent(new KeyEvent(KeyEvent.ACTION_DOWN, KeyEvent.KEYCODE_ENTER));
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
                break;
            case Keyboard.KEYCODE_MODE_CHANGE:
                try {
                    if (mCurKeyboard == keyboardAbc) {
                        setLatinKeyboard(keyboardSymbol);
                    } else {
                        setLatinKeyboard(keyboardAbc);
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
                break;

            case CODE_CLEAR:
                //clear all texts
                try {
                    ic.deleteSurroundingText(Integer.MAX_VALUE, Integer.MAX_VALUE);
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
                break;

            default:
                try {
                    char code = (char) primaryCode;
                    if (Character.isLetter(code) && caps) {
                        code = Character.toUpperCase(code);
                    }
                    ic.commitText(String.valueOf(code), 1);
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
        }

    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////
    KeyboardView.OnKeyboardActionListener onKeyboardActionListener = new KeyboardView.OnKeyboardActionListener() {

        @Override
        public void onKey(int primaryCode, int[] keyCodes) {
            processKey(primaryCode, keyCodes);
        }

        @Override
        public void onPress(int primaryCode) {
        }

        @Override
        public void onRelease(int primaryCode) {
        }

        @Override
        public void onText(CharSequence text) {
        }

        @Override
        public void swipeDown() {
        }

        @Override
        public void swipeLeft() {
        }

        @Override
        public void swipeRight() {
        }

        @Override
        public void swipeUp() {
        }

    };
////////////////////////////////////////////////////////////////////////////////////////////////////
}
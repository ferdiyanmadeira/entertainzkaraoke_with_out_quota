package com.madeira.entertainz.controller;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TableRow;
import android.widget.TextView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.DownloadListener;
import com.androidnetworking.interfaces.DownloadProgressListener;
import com.madeira.entertainz.karaoke.BuildConfig;
import com.madeira.entertainz.karaoke.Debug;
import com.madeira.entertainz.karaoke.MyDeviceAdminReceiver;
import com.madeira.entertainz.karaoke.R;
import com.madeira.entertainz.karaoke.config.C;
import com.madeira.entertainz.karaoke.config.Global;
import com.madeira.entertainz.karaoke.menu_main.MainActivity;
import com.madeira.entertainz.karaoke.model_online.ModelApp;
import com.madeira.entertainz.library.Bps;
import com.madeira.entertainz.library.RootUtil;
import com.madeira.entertainz.library.Util;
import com.madeira.entertainz.library.UtilDeviceOwner;
import com.madeira.entertainz.library.UtilTime;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import es.dmoral.toasty.Toasty;

public class STBControl {
    static String TAG = "STBControl";

    public static final String KEY_TOTAL_SIZE = "TOTAL_SIZE_SONG";
    public static final String KEY_DOWNLOADED_SIZE = "DOWNLOADED_SIZE";
    public static final String KEY_DOWNLOAD_SPEED = "DOWNLOAD_SPEED";
    public static final String KEY_DOWNLOADED_IN_KB = "DOWNLOADED_IN_KB";
    public static final String KEY_TOTAL_SIZE_IN_KB = "TOTAL_SIZE_IN_KB";
    public static final String KEY_RESULT_DOWNLOAD = "RESULT_DOWNLOAD";

    /**
     * untuk grant permission yang dibutuhkan app
     */
    public boolean forcePermission(Context context) {
        boolean result;
        try {
            String packageName = context.getApplicationContext().getPackageName();
            Process p = Runtime.getRuntime().exec("su");
            DataOutputStream dos = new DataOutputStream(p.getOutputStream());

            dos.writeBytes("adb shell\n");
            dos.writeBytes("pm grant " + packageName + " android.permission.WRITE_EXTERNAL_STORAGE\n");
            dos.writeBytes("pm grant " + packageName + " android.permission.READ_EXTERNAL_STORAGE\n");
            dos.writeBytes("pm grant " + packageName + " android.permission.RECORD_AUDIO\n");
//            dos.writeBytes("pm grant " + packageName + " android.permission.INSTALL_PACKAGES\n");

            dos.flush();
            dos.close();
            p.waitFor();
            BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));
            StringBuffer output = new StringBuffer();
            StringBuffer errorSB = new StringBuffer();

            BufferedReader stdError = new BufferedReader(new
                    InputStreamReader(p.getErrorStream()));

            String line = "";
            String stringResult = "";
            while ((line = reader.readLine()) != null) {
                output.append(line + "; ");
            }

            while ((line = stdError.readLine()) != null) {
                errorSB.append(line + "; ");
            }

            stringResult = output.toString() + errorSB.toString();
            Log.d(TAG, stringResult);

            result = true;

        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
            Log.d(TAG, e.getMessage());
            result = false;
        }
        return result;
    }

    /**
     * untuk memaksa wifi harus on, ketika pertama kali boot, agar tidak perlu toggle wifi on ketika memilih jaringan
     */
    public boolean forceWifiOn() {
        boolean result;
        try {
            Process p = Runtime.getRuntime().exec("su");
            DataOutputStream dos = new DataOutputStream(p.getOutputStream());

            dos.writeBytes("adb shell\n");
            dos.writeBytes("svc wifi enable\n");

            dos.flush();
            dos.close();
            p.waitFor();
            BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));
            StringBuffer output = new StringBuffer();
            StringBuffer errorSB = new StringBuffer();

            BufferedReader stdError = new BufferedReader(new
                    InputStreamReader(p.getErrorStream()));

            String line = "";
            String stringResult = "";
            while ((line = reader.readLine()) != null) {
                output.append(line + "; ");
            }

            while ((line = stdError.readLine()) != null) {
                errorSB.append(line + "; ");
            }

            stringResult = output.toString() + errorSB.toString();

            Log.d(TAG, stringResult);
            result = true;

        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
            Log.d(TAG, e.getMessage());
            result = false;
        }
        return result;
    }

    /**
     * set device owner
     */

    public void setAndRemoveDeviceOwner(Activity activity) {
        try {

            boolean isKioskModeEnable = Global.getDeviceOwnerConfig();

            if (C.FOR_HOTEL) {
                if (isKioskModeEnable == true) {
                    UtilDeviceOwner.removeDeviceOwner(BuildConfig.APPLICATION_ID, MyDeviceAdminReceiver.class);
                    Global.setDeviceOwnerConfig(false);
                }
            } else {
                if (!isKioskModeEnable) {
                    UtilDeviceOwner.enableDeviceOwner(BuildConfig.APPLICATION_ID, MyDeviceAdminReceiver.class);
                    UtilDeviceOwner.enableKioskMode(activity, BuildConfig.APPLICATION_ID, MainActivity.class, MyDeviceAdminReceiver.class);
                    Global.setDeviceOwnerConfig(true);
                }

                //set device owner jika debug == false
                if (BuildConfig.DEBUG == false) {
                    UtilDeviceOwner.enableDeviceOwner(BuildConfig.APPLICATION_ID, MyDeviceAdminReceiver.class);
                    UtilDeviceOwner.enableKioskMode(activity, BuildConfig.APPLICATION_ID, MainActivity.class, MyDeviceAdminReceiver.class);
                    Global.setDeviceOwnerConfig(true);
                }
            }

        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    /**
     * set timezone
     */
    public void setTimeZone(Context context) {
        try {
            String timeZone = Global.getTimeZone();
            UtilTime.setTimezone(context, timeZone);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    /**
     * untuk hide system UI
     */

    public void hideSystemUI(Activity context) {
        try {
            View decorView = context.getWindow().getDecorView();
            decorView.setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                            | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION // hide nav bar
                            | View.SYSTEM_UI_FLAG_FULLSCREEN // hide status bar
                            | View.SYSTEM_UI_FLAG_IMMERSIVE);
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }


    /**
     * untuk set date time
     */
    public String setCurrentDateTime() {
        String result = "";
        try {
            SimpleDateFormat sdfDate = new SimpleDateFormat("d MMM yyyy HH:mm:ss");
            Date now = new Date();
            result = sdfDate.format(now);
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }

        return result;
    }

    /**
     * check update apk
     */
    public void updateApp(Activity activity) {
        try {
            String pathUpdateAPK = Global.getPathUpdate();
            int currentVersion = activity.getPackageManager().getPackageInfo(activity.getPackageName(), 0).versionCode;
            int nextAppCode = Global.getNextAppCode();
            if (currentVersion < nextAppCode) {
                if (!pathUpdateAPK.equals("")) {
                    showUpdateAlert(pathUpdateAPK, activity);
                }
            }
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    /**
     * muncul ketika ada update apk
     */
    void showUpdateAlert(String path, Activity activity) {
        try {
            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(activity);
            LayoutInflater inflater = activity.getLayoutInflater();
            View dialogView = inflater.inflate(R.layout.alert_confirm_update, null);
            dialogBuilder.setView(dialogView);

            TextView textView = dialogView.findViewById(R.id.textView);
            Button updateButton = dialogView.findViewById(R.id.updateButton);
            Button cancelButton = dialogView.findViewById(R.id.cancelButton);
            String currentVersion = activity.getPackageManager().getPackageInfo(activity.getPackageName(), 0).versionName;
            String text = String.format(activity.getString(R.string.updateInformation), currentVersion, Global.getNextAppCodeName());
            textView.setText(text);

            AlertDialog alertDialog = dialogBuilder.create();

            updateButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    alertDialog.dismiss();
                    showUpdateAlertContinue(path, activity);

                }
            });

            cancelButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    alertDialog.dismiss();
                }
            });

            alertDialog.show();
            WindowManager.LayoutParams layoutParams = alertDialog.getWindow().getAttributes();
            layoutParams.width = ViewGroup.LayoutParams.WRAP_CONTENT;
            alertDialog.show();
            alertDialog.getWindow().setLayout(430, WindowManager.LayoutParams.WRAP_CONTENT);
            Util.hideNavigationBar(activity);


        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }

    }


    /**
     * muncul ketika pilih continue ketika upgrade software dipilih
     */
    void showUpdateAlertContinue(String path, Activity activity) {
        try {
            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(activity);
            LayoutInflater inflater = activity.getLayoutInflater();
            View dialogView = inflater.inflate(R.layout.alert_confirm_update, null);
            dialogBuilder.setView(dialogView);

            TextView textView = dialogView.findViewById(R.id.textView);
            Button updateButton = dialogView.findViewById(R.id.updateButton);
            Button cancelButton = dialogView.findViewById(R.id.cancelButton);
            String text = activity.getString(R.string.updateInformation_continue);
            textView.setText(text);
            updateButton.setText(activity.getString(R.string.updateApp_continue));

            AlertDialog alertDialog = dialogBuilder.create();

            updateButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    alertDialog.dismiss();
                    showProcessUpdateDialog(path, activity);
                }
            });

            cancelButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    alertDialog.dismiss();
                }
            });

            alertDialog.show();
            WindowManager.LayoutParams layoutParams = alertDialog.getWindow().getAttributes();
            layoutParams.width = ViewGroup.LayoutParams.WRAP_CONTENT;
            alertDialog.show();
            alertDialog.getWindow().setLayout(430, WindowManager.LayoutParams.WRAP_CONTENT);
            Util.hideNavigationBar(activity);


        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }

    }


    /**
     * muncul ketika confirm dipilih, untuk prcess update
     */
    void showProcessUpdateDialog(String path, Activity activity) {
        try {
            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(activity);
            LayoutInflater inflater = activity.getLayoutInflater();
            View dialogView = inflater.inflate(R.layout.alert_process_update, null);
            dialogBuilder.setView(dialogView);

            TextView textView = dialogView.findViewById(R.id.textView);
            TextView textView2 = dialogView.findViewById(R.id.textView2);
            Button updateButton = dialogView.findViewById(R.id.updateButton);
            Button cancelButton = dialogView.findViewById(R.id.cancelButton);
            /** untuk different color di 1 textview */
            String text1 = activity.getString(R.string.update_process);
            String text2 = Util.getColoredSpanned(activity.getString(R.string.update_process2), "#ff3333");
            String text3 = activity.getString(R.string.update_process3);
            String text4 = activity.getString(R.string.update_process4);

            textView.setText(Html.fromHtml(text1 + " " + text2 + " " + text3));
            textView2.setText(Html.fromHtml(text4));
            AlertDialog alertDialog = dialogBuilder.create();

            updateButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    alertDialog.dismiss();
                    showUpgradeInProcess(activity);
                    updateLastUpdateApp(path, activity);
                }
            });

            cancelButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    alertDialog.dismiss();
                }
            });

            alertDialog.show();
            WindowManager.LayoutParams layoutParams = alertDialog.getWindow().getAttributes();
            layoutParams.width = ViewGroup.LayoutParams.WRAP_CONTENT;
            alertDialog.show();
            alertDialog.getWindow().setLayout(430, WindowManager.LayoutParams.WRAP_CONTENT);
            Util.hideNavigationBar(activity);


        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }

    }


    void showUpgradeInProcess(Activity activity) {
        try {
            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(activity);
            LayoutInflater inflater = activity.getLayoutInflater();
            View dialogView = inflater.inflate(R.layout.alert_update_in_progress, null);
            dialogBuilder.setView(dialogView);

            TextView textView = dialogView.findViewById(R.id.textView);
            TextView textView2 = dialogView.findViewById(R.id.textView2);
            TextView textView3 = dialogView.findViewById(R.id.textView3);
            TextView textView4 = dialogView.findViewById(R.id.textView4);

            Button okButton = dialogView.findViewById(R.id.okButton);
            /** untuk different color di 1 textview */
            String text1 = activity.getString(R.string.update_in_process);
            String text2 = Util.getColoredSpanned(activity.getString(R.string.update_in_process2), "#ff3333");
            String text3 = Util.getColoredSpanned(activity.getString(R.string.update_in_process3), "#ff3333");
            String text4 = activity.getString(R.string.update_in_process4);

            textView.setText(text1);
            textView2.setText(Html.fromHtml(text2));
            textView3.setText(Html.fromHtml(text3));
            textView4.setText(Html.fromHtml(text4));
            AlertDialog alertDialog = dialogBuilder.create();

            okButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    alertDialog.dismiss();
                }
            });

            alertDialog.show();
            WindowManager.LayoutParams layoutParams = alertDialog.getWindow().getAttributes();
            layoutParams.width = ViewGroup.LayoutParams.WRAP_CONTENT;
            alertDialog.show();
            alertDialog.getWindow().setLayout(430, WindowManager.LayoutParams.WRAP_CONTENT);
            Util.hideNavigationBar(activity);

        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }

    }

    void updateLastUpdateApp(String path, Activity activity) {

        try {
            Date c = Calendar.getInstance().getTime();
            System.out.println("Current time => " + c);
            SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
            String formattedDate = df.format(c);

            Global.setLastUpdate(formattedDate);
            Global.setPathUpdate("");
            String packageName = activity.getPackageName();
            Util.installAPK(path, packageName);

        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }


    //update apk
    public boolean downloadUpdate(Context context, ModelApp.appItem result) {
        boolean alreadyUpdate = false;
        try {

            //check update app
            ModelApp.subAppItem subAppItem = result.app;
            int currentVersionCode = BuildConfig.VERSION_CODE;
            int appCodeDownloaded = Global.getNextAppCode();
            int latestAppCode = Integer.valueOf(subAppItem.versionCode);
            if (Integer.valueOf(subAppItem.versionCode) > currentVersionCode && appCodeDownloaded < latestAppCode && C.UPDATING_APK == false) {
                Global.setNextAppCodeName(subAppItem.versionName);
                boolean fgDownloadAPK = false;
                String pathUpdate = Global.getPathUpdate();
                if (pathUpdate.equals(""))
                    fgDownloadAPK = true;
                else {
                    //check apakah file update exist, jika tidak download lagi latest version dari server
                    File apkFile = new File(pathUpdate);
                    if(apkFile.exists()) {
                        List<String> apkInfo = Util.getAPKInfo(context, pathUpdate);
                        int versionCode = 0;
                        String versionName;
                        String packageName = null;
                        if (apkInfo.size() == 3) {
                            packageName = apkInfo.get(0);
                            versionCode = Integer.valueOf(apkInfo.get(1));
                            versionName = apkInfo.get(2);
                        }

                        if (versionCode > currentVersionCode)
                            fgDownloadAPK = true;
                    }
                    else
                    {
                        fgDownloadAPK = true;
                    }

                }
                if (fgDownloadAPK) {
                    String appDir = context.getFilesDir().toString();
                    String[] fileNameArray = subAppItem.urlApk.split("/");
                    String fileName = fileNameArray[fileNameArray.length - 1];
                    AndroidNetworking.download(subAppItem.urlApk, appDir, fileName).build().setDownloadProgressListener(new DownloadProgressListener() {
                        @Override
                        public void onProgress(long bytesDownloaded, long totalBytes) {
                            try {
                                Log.d("Update APp", "Download apk");
                                DecimalFormat df = new DecimalFormat("#.##");
                                double kbDownloaded = ((double) bytesDownloaded / 1024);
                                double mbDownloaded = (double) bytesDownloaded / (double) (1024 * 1024);
                                double totalKb = ((double) totalBytes / (double) 1024);
                                double totalMb = (double) totalBytes / (double) (1024 * 1024);

                                //dalam mega bits per second
                                double speedMbps = Bps.calcBps(bytesDownloaded);

                                String downloaded = String.format("%.2f MB", mbDownloaded);
//                                                remainingKbTV.setText(text);
                                String textTotal = String.format("%.2f MB", totalMb);
                                String speed = String.format("%.2f Mbit/s", speedMbps);
                                sendBC(context, downloaded, textTotal, speed, kbDownloaded, totalKb);
                                C.UPDATING_APK = true;
                            } catch (Exception ex) {
                                Debug.e(TAG, ex);
                            }
                        }
                    }).startDownload(new DownloadListener() {
                        @Override
                        public void onDownloadComplete() {
                            try {
//                                Toasty.info(context, context.getString(R.string.downloadUpdate)).show();
//                                String usbPath = Global.getUSBPath() + "/" + C.PATH_EKB_APK + "/" + fileName;
                                String pathAPK = appDir + "/" + fileName;
                                File apkFile = new File(pathAPK);
                                if (apkFile.exists()) {
                                    Global.setPathUpdate(pathAPK);
                                    Global.setNextAppCode(Integer.valueOf(result.app.versionCode));
//                                    updateApp(context);
                                    sendBcDownloadFinish(context, true);
                                    C.UPDATING_APK = false;

                                    /*if (usbPath != null) {

                                     *//*  String moveFile = RootUtil.copyFile(pathAPK, usbPath);
                                        if (moveFile.equals("")) {
                                            Global.setPathUpdate(usbPath);
                                            Global.setNextAppCode(Integer.valueOf(result.app.versionCode));
                                        }
                                        updateApp(context);*//*
                                    }*/
                                }
                            } catch (Exception ex) {
                                Debug.e(TAG, ex);
                            }
                        }

                        @Override
                        public void onError(ANError anError) {
                            Global.setPathUpdate("");
                            C.UPDATING_APK = false;

                        }
                    });
                } else {
                    alreadyUpdate = true;
                }
            } else {
                alreadyUpdate = true;
            }
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }

        return alreadyUpdate;
    }


    public static void registerReceiver(Context context, BroadcastReceiver br) {
        try {
            IntentFilter intFilter = new IntentFilter(C.BOTTOM_NAVIGATION_FRAGMENT_UPDATE_APK);
            context.registerReceiver(br, intFilter);
        } catch (Exception e) {
            Debug.e(TAG, e);
        }
    }

    public static void unregisterReceiver(Context context, BroadcastReceiver br) {
        try {
            context.unregisterReceiver(br);
        } catch (Exception e) {
            Debug.e(TAG, e);
        }
    }

    public static void registerFinishDownloadReceiver(Context context, BroadcastReceiver br) {
        try {
            IntentFilter intFilter = new IntentFilter(C.BOTTOM_NAVIGATION_FRAGMENT_FINISH_DOWNLOAD_UPDATE_APK);
            context.registerReceiver(br, intFilter);
        } catch (Exception e) {
            Debug.e(TAG, e);
        }
    }

    public static void unregisterFinishDownloadReceiver(Context context, BroadcastReceiver br) {
        try {
            context.unregisterReceiver(br);
        } catch (Exception e) {
            Debug.e(TAG, e);
        }
    }


    /**
     * untuk send bc progress download
     */
    void sendBC(Context context, String downloaded, String totalSize, String speed, double kbDownloaded, double totalSizeInKb) {
        try {

            Intent sendBC = new Intent();
            sendBC.setFlags(Intent.FLAG_INCLUDE_STOPPED_PACKAGES)
                    .setAction(C.BOTTOM_NAVIGATION_FRAGMENT_UPDATE_APK);
            sendBC.putExtra(KEY_DOWNLOADED_SIZE, downloaded);
            sendBC.putExtra(KEY_DOWNLOAD_SPEED, speed);
            sendBC.putExtra(KEY_TOTAL_SIZE, totalSize);
            sendBC.putExtra(KEY_DOWNLOADED_IN_KB, kbDownloaded);
            sendBC.putExtra(KEY_TOTAL_SIZE_IN_KB, totalSizeInKb);
            sendBC.putExtra(KEY_DOWNLOADED_SIZE, downloaded);
            sendBC.putExtra(KEY_DOWNLOAD_SPEED, speed);
            sendBC.putExtra(KEY_TOTAL_SIZE, totalSize);
            sendBC.putExtra(KEY_DOWNLOADED_IN_KB, kbDownloaded);
            sendBC.putExtra(KEY_TOTAL_SIZE_IN_KB, totalSizeInKb);
            context.sendBroadcast(sendBC);
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }


    /**
     * untuk send bc sudah selesai download
     */
    void sendBcDownloadFinish(Context activity, boolean result) {
        try {
            Intent sendBC = new Intent();
            sendBC.setFlags(Intent.FLAG_INCLUDE_STOPPED_PACKAGES)
                    .setAction(C.BOTTOM_NAVIGATION_FRAGMENT_FINISH_DOWNLOAD_UPDATE_APK);
            sendBC.putExtra(KEY_RESULT_DOWNLOAD, result);
            activity.sendBroadcast(sendBC);
            if (result) {
            }
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

}

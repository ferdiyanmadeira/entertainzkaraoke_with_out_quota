package com.madeira.entertainz.controller;

import android.app.Activity;
import android.bluetooth.BluetoothClass;
import android.content.Context;
import android.util.Log;

import com.google.android.exoplayer2.extractor.ts.TsExtractor;
import com.google.gson.Gson;
import com.madeira.entertainz.karaoke.DBLocal.TElement;
import com.madeira.entertainz.karaoke.DBLocal.TElementMedia;
import com.madeira.entertainz.karaoke.DBLocal.TLocalFile;
import com.madeira.entertainz.karaoke.DBLocal.TMessageMedia;
import com.madeira.entertainz.karaoke.DBLocal.TPlaylist;
import com.madeira.entertainz.karaoke.DBLocal.TSong;
import com.madeira.entertainz.karaoke.DBLocal.TSongCategory;
import com.madeira.entertainz.karaoke.DBLocal.TSongPlaylist;
import com.madeira.entertainz.karaoke.DBLocal.TSongYoutubePlaylist;
import com.madeira.entertainz.karaoke.DBLocal.TSticker;
import com.madeira.entertainz.karaoke.DBLocal.TYoutubePlaylist;
import com.madeira.entertainz.karaoke.Debug;
import com.madeira.entertainz.karaoke.config.C;
import com.madeira.entertainz.karaoke.config.CacheData;
import com.madeira.entertainz.karaoke.config.Global;
import com.madeira.entertainz.karaoke.model.ModelMessageLocal;
import com.madeira.entertainz.karaoke.model.ModelPlaylist;
import com.madeira.entertainz.karaoke.model.ModelSongLocal;
import com.madeira.entertainz.karaoke.model.ModelStickerLocal;
import com.madeira.entertainz.karaoke.model.ModelThemeLocal;
import com.madeira.entertainz.karaoke.model.ModelYoutubePlaylist;
import com.madeira.entertainz.karaoke.model_room_db.ModelSong;
import com.madeira.entertainz.library.PerformAsync2;
import com.madeira.entertainz.library.RootUtil;
import com.madeira.entertainz.library.Util;
import com.madeira.entertainz.library.UtilWriteJSONFile;
import com.madeira.entertainz.library.VarDump;

import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class USBControl {
    static String TAG = "USBControl";

    /**
     * untuk get content element, dan element media
     */
    public ModelThemeLocal.ResultGetThemeList getContentElement() {
        ModelThemeLocal.ResultGetThemeList resultGetThemeList = null;
        try {
            TLocalFile tLocalFileTheme = CacheData.hashFile.get(C.JSON_THEME);
            resultGetThemeList = ModelThemeLocal.getThemeFromLocal(tLocalFileTheme.content);

        } catch (Exception ex) {
            Log.d(TAG, ex.getMessage());
        }
        return resultGetThemeList;
    }

    /**
     * untuk replace path url image, dengan full path directory di usb drive
     */
    public List<TElementMedia> replacePathImageElementMedia(List<TElementMedia> prmElementMediaList) {
        List<TElementMedia> tElementMediaList = new ArrayList<>();
        try {
            for (Iterator<TElementMedia> itemIterator = prmElementMediaList.iterator(); itemIterator.hasNext(); ) {
                TElementMedia item = itemIterator.next();
                TLocalFile tLocalFile = CacheData.hashFile.get(item.urlImage);
                if (tLocalFile != null) {
                    TElementMedia TElementMedia = new TElementMedia();
                    TElementMedia = item;
                    if (item.urlImage != null && item.urlImage.equals(tLocalFile.fileName)) {
                        TElementMedia.urlImage = tLocalFile.content;
                        tElementMediaList.add(TElementMedia);
                    }
                }
            }
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }

        return tElementMediaList;
    }

    /**
     * untuk get content song categories
     */
    public List<TSongCategory> getSongCategories() {
        List<TSongCategory> result = new ArrayList<>();
        try {
            TLocalFile tLocalFileSongCategories = CacheData.hashFile.get(C.JSON_SONG_CATEGORY);
            result = ModelSongLocal.geTSongCategoryFromLocal(tLocalFileSongCategories.content);
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }

        return result;
    }

    /**
     * untuk get content song, dan replace song url dan thumbnail nya menjadi full path sesuai usb drive nya
     */
    public List<TSong> getSongFullPath(Activity activity) {
        List<TSong> result = new ArrayList<>();
        //insert song
        int i = 0;
        try {
            TLocalFile tLocalFileSong = CacheData.hashFile.get(C.JSON_SONG);
            String pathEncrypted = tLocalFileSong.content;
            String pathDecrypted = activity.getFilesDir() + "/" + C.JSON_SONG;
            String mac = Util.getMACAddress("eth0");
//                    mac = mac.replace(":", "");
            boolean songDecrypt = Util.decrypt(pathEncrypted, mac, pathDecrypted);
            if (songDecrypt) {
                List<TSong> oriTSongList = ModelSongLocal.geTSongFromLocal(pathDecrypted);
                for (Iterator<TSong> TSongIterator = oriTSongList.iterator(); TSongIterator.hasNext(); ) {
                    TSong itemTsong = TSongIterator.next();
                    i++;
                    TLocalFile tLocalFileVideo = CacheData.hashFile.get(itemTsong.songURL);
                    if (tLocalFileVideo != null) {
                        TSong tempTSong = new TSong();
                        tempTSong = itemTsong;
                        tempTSong.songURL = tLocalFileVideo.content;
                        TLocalFile tLocalFileImage = CacheData.hashFile.get(itemTsong.thumbnail);
                        if (tLocalFileImage != null)
                            tempTSong.thumbnail = tLocalFileImage.content;
                        result.add(tempTSong);

                        //delete file yg sudah di decrypt
                        File delFile = new File(pathDecrypted);
                        if (delFile.exists())
                            delFile.delete();
                    }
                }
            }
        } catch (Exception ex) {
            Log.d(TAG, " item " + String.valueOf(i));
        }
        return result;
    }

    /**
     * untuk get content message, dan message media
     */
    public ModelMessageLocal.ResultGetMessageList getMessage() {
        ModelMessageLocal.ResultGetMessageList result = null;
        try {
            TLocalFile tLocalFileMessage = CacheData.hashFile.get(C.JSON_MESSAGE);
            if (tLocalFileMessage != null) {
                result = ModelMessageLocal.geMessageFromLocal(tLocalFileMessage.content);
            }
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
        return result;
    }

    /**
     * untuk replace path url, dangan full path dari usb
     */
    public List<TMessageMedia> replaceMessageMedia(List<TMessageMedia> prmMessageMedia) {
        List<TMessageMedia> result = new ArrayList<>();
        try {
            for (Iterator<TMessageMedia> itemIterator = prmMessageMedia.iterator(); itemIterator.hasNext(); ) {
                TMessageMedia item = itemIterator.next();
                TMessageMedia tMessageMedia = new TMessageMedia();
                tMessageMedia = item;
                if (item.urlImage != null) {
                    TLocalFile tLocalFile = CacheData.hashFile.get(tMessageMedia.urlImage);
                    tMessageMedia.urlImage = tLocalFile.content;
                    result.add(tMessageMedia);
                } else if (item.urlVideo != null) {
                    TLocalFile tLocalFile = CacheData.hashFile.get(tMessageMedia.urlVideo);
                    tMessageMedia.urlVideo = tLocalFile.content;
                    result.add(tMessageMedia);
                }
            }
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
        return result;
    }

    /**
     * untuk get list sticker
     */
    public List<TSticker> getSticker() {
        List<TSticker> result = new ArrayList<>();
        try {
            TLocalFile tLocalFileTheme = CacheData.hashFile.get(C.JSON_STICKER);
            ModelStickerLocal.ResultGetStickerList resultGetStickerList = ModelStickerLocal.getStickerFromLocal(tLocalFileTheme.content);
            List<TSticker> oriTStickerList = resultGetStickerList.list;
            for (Iterator<TSticker> TSongIterator = oriTStickerList.iterator(); TSongIterator.hasNext(); ) {
                TSticker itemTSticker = TSongIterator.next();
                TLocalFile tlocalImage = CacheData.hashFile.get(itemTSticker.urlImage);
                if (tlocalImage != null) {
                    TSticker tempTSticker = new TSticker();
                    tempTSticker = itemTSticker;
                    tempTSticker.urlImage = tlocalImage.content;
                    result.add(tempTSticker);
                }
            }

        } catch (Exception ex) {
            Log.d(TAG, ex.getMessage());
        }

        return result;
    }

    /**
     * untuk get list playlist
     */
    public List<TPlaylist> getPlaylist() {
        List<TPlaylist> result = new ArrayList<>();
        try {
            TLocalFile tLocalFile = CacheData.hashFile.get(C.JSON_PLAYLIST);
            result = ModelPlaylist.getPlaylist(tLocalFile.content);
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }

        return result;
    }

    /**
     * untuk get list song playlist
     */
    public List<TSongPlaylist> getSongPlaylist() {
        List<TSongPlaylist> result = new ArrayList<>();
        try {
            TLocalFile tLocalFile = CacheData.hashFile.get(C.JSON_SONG_PLAYLIST);
            result = ModelPlaylist.getSongPlaylist(tLocalFile.content);
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
        return result;
    }

    /**
     * untuk get youtube playlist
     */
    public List<TYoutubePlaylist> getYoutubePlaylist() {
        List<TYoutubePlaylist> result = new ArrayList<>();

        try {
            TLocalFile tLocalFile = CacheData.hashFile.get(C.JSON_YOUTUBE_PLAYLIST);
            result = ModelYoutubePlaylist.getYoutubePlaylist(tLocalFile.content);
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }

        return result;
    }

    /**
     * untuk get song youtube playlist
     */
    public List<TSongYoutubePlaylist> getSongYoutubePlaylist() {
        List<TSongYoutubePlaylist> result = new ArrayList<>();
        try {
            TLocalFile tLocalFile = CacheData.hashFile.get(C.JSON_SONG_YOUTUBE_PLAYLIST);
            result = ModelYoutubePlaylist.getSongPlaylist(tLocalFile.content);
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
        return result;
    }

    /**
     * untuk write json file karaoke playlist
     */
    public boolean writePlaylistFile(Context context) {
        boolean result = false;
        try {

            com.madeira.entertainz.karaoke.model_room_db.ModelPlaylist.getPlaylist(context, new com.madeira.entertainz.karaoke.model_room_db.ModelPlaylist.IGetPlaylist() {
                @Override
                public void onGetListPlaylist(List<TPlaylist> tPlaylistList) {
                    if (tPlaylistList.size() != 0)
                        UtilWriteJSONFile.writePlaylistToFile(context, tPlaylistList);
                }
            });

            com.madeira.entertainz.karaoke.model_room_db.ModelPlaylist.getSongPlaylist(context, new com.madeira.entertainz.karaoke.model_room_db.ModelPlaylist.IGetSongPlaylist() {
                @Override
                public void onGetListSongPlaylist(List<TSongPlaylist> tSongPlaylistList) {
                    if (tSongPlaylistList.size() != 0)
                        UtilWriteJSONFile.writeSongPlaylistToFile(context, tSongPlaylistList);
                }
            });

            result = true;

        } catch (Exception ex) {

        }

        return result;
    }


    /**
     * untuk buat json file youtube playlist dan song youtube playlist
     */
    public boolean writeYoutubeSongsPlaylistToFile(Context context) {

        boolean result = false;
        try {
            com.madeira.entertainz.karaoke.model_room_db.ModelYoutubePlaylist.getPlaylist(context, new com.madeira.entertainz.karaoke.model_room_db.ModelYoutubePlaylist.IGetPlaylist() {
                @Override
                public void onGetListPlaylist(List<TYoutubePlaylist> tYoutubePlaylistList) {
                    UtilWriteJSONFile.writeYoutubePlaylistToFile(context, tYoutubePlaylistList);
                }
            });

            com.madeira.entertainz.karaoke.model_room_db.ModelYoutubePlaylist.getSongPlaylist(context, new com.madeira.entertainz.karaoke.model_room_db.ModelYoutubePlaylist.IGetSongPlaylist() {
                @Override
                public void onGetListSongPlaylist(List<TSongYoutubePlaylist> tSongPlaylistList) {
                    UtilWriteJSONFile.writeSongYoutubePlaylistToFile(context, tSongPlaylistList);
                }
            });

            result = true;


        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
        return result;

    }

    /**
     * simpan song youtube playlist saja ke usb
     *
     * @param context
     */
    public static void writeSongYoutubePlaylist(Context context){
        PerformAsync2.run(new PerformAsync2.Callback() {
            @Override
            public Object onBackground(PerformAsync2 performAsync) {

                try {
                    //load dari db
                    List<TSongYoutubePlaylist> list = com.madeira.entertainz.karaoke.model_room_db.ModelYoutubePlaylist.syncGetSongPlaylist(context);

                    //write ke file
                    UtilWriteJSONFile.writeSongYoutubePlaylistToFile(context, list);

                } catch (Exception e) {
                    e.printStackTrace();
                }

                return null; //dont care for return
            }
        });
    }

    public interface IWriteFileSong {
        void onWriteFileSong(boolean result);
    }

    public void writeSongFileToJSON(Context context, IWriteFileSong callback) {
        ModelSong.getListSong(context, new ModelSong.IGetListSong() {
            @Override
            public void onGetListSong(List<TSong> tSongList) {

                try {
                    Gson gson = new Gson();
                    List<TSong> newTSongList = new ArrayList<>();
                    for (TSong tSong : tSongList) {
                        String[] fileNameSongArray = tSong.songURL.split("/");
                        String fileNameSong = fileNameSongArray[fileNameSongArray.length - 1];

                        String[] fileNameThumbnailArray = tSong.thumbnail.split("/");
                        String fileNameThumbnail = fileNameThumbnailArray[fileNameThumbnailArray.length - 1];

                        tSong.songURL = fileNameSong;
                        tSong.thumbnail = fileNameThumbnail;
                        newTSongList.add(tSong);
                    }

                    ModelSongLocal.ResultGetSongList resultGetSongList = new ModelSongLocal.ResultGetSongList();
                    resultGetSongList.count = newTSongList.size();
                    resultGetSongList.list = newTSongList;

                    String songString = gson.toJson(resultGetSongList);
                    String pathSTB = Global.getUSBPath() + "/" + C.PATH_JSON_KARAOKE;

                    //write song
                    String mac = Util.getMACAddress("eth0");
                    String usbSongPath = pathSTB + "/" + C.JSON_SONG;
                    String tempSongPath = context.getFilesDir() + "/" + C.JSON_SONG;
                    boolean resultWriteYoutube = Util.writeJsonToFile(tempSongPath, songString);
                    String encQuotaPath = Util.encryptFile(tempSongPath, mac);
                    if (encQuotaPath != null) {
                        if (resultWriteYoutube)
                            RootUtil.moveFile(encQuotaPath, usbSongPath);

                        C.FG_UPDATE_SONG = false;
                    }
                    CacheData.hashSongNameNArtis.clear();
                    callback.onWriteFileSong(true);
                } catch (Exception ex) {
                    callback.onWriteFileSong(false);
                    Debug.e(TAG, ex);
                }

            }
        });
    }

}

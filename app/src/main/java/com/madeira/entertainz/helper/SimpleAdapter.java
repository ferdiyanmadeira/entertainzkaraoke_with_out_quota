package com.madeira.entertainz.helper;

import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

/**
 * Created by Erick on 3/16/2017.
 */

public class SimpleAdapter extends BaseAdapter {

    public interface ISimpleAdapterCallback {
        public int onGetCount(SimpleAdapter adapter);
//        public Object onGetItem(SimpleAdapter adapter, int i);
        public View onGetView(SimpleAdapter adapter, int i, View view, ViewGroup viewGroup);
    }

    public interface ISpinnnerDropDown{
        public View onGetDropDownView(SimpleAdapter adapter, int i, View view, ViewGroup viewGroup);
    }

    private ISimpleAdapterCallback callback;
    private ISpinnnerDropDown callback2;

    public SimpleAdapter(ISimpleAdapterCallback callback){
        this.callback = callback;
    }


    public SimpleAdapter(ISimpleAdapterCallback callback, ISpinnnerDropDown callback2){
        this.callback = callback;
        this.callback2 = callback2;
    }


    public void setCallback(ISimpleAdapterCallback callback) {
        this.callback = callback;
    }

    @Override
    public int getCount() {
        if (callback!=null) return callback.onGetCount(this);
        return 0;
    }

    @Override
    public Object getItem(int i) {
//        if (iRadioTheme!=null) return iRadioTheme.onGetItem(this, i);
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        if (callback!=null) return callback.onGetView(this, i, view, viewGroup);
        return null;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        if (callback2!=null) return callback2.onGetDropDownView(this, position, convertView, parent);
        return null;
//        return super.getDropDownView(position, convertView, parent);
    }

}

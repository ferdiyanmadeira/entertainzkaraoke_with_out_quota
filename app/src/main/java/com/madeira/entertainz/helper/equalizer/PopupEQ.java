package com.madeira.entertainz.helper.equalizer;

import android.content.Context;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.media.audiofx.Equalizer;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Surface;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.SeekBar;
import android.widget.TextView;

import com.madeira.entertainz.karaoke.Debug;
import com.madeira.entertainz.karaoke.R;
import com.madeira.entertainz.karaoke.config.Global;


/**
 * Created by MR on 11/8/2016.
 */
public class PopupEQ {
    final String PARENT_TAG = "PopupEQ";


    Context context;

    View rootView;
    PopupWindow popupWindow;

    Equalizer eq;
    LinearLayout layout;

    short numBands;
    short[] bandLevel;
    short lowerLevel = -15;
    short upperLevel = 15;

    public PopupEQ(Context _context, int audioSessionId, int orientation) {
        String TAG = PARENT_TAG + "-PopupEQ";
        try {
            context = _context;

            eq = new Equalizer(0, audioSessionId);
            eq.setEnabled(true);

//        String test = eq.getPresetName();

            numBands = eq.getNumberOfBands();
            bandLevel = eq.getBandLevelRange();

            LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            if (orientation == Surface.ROTATION_90) {
                rootView = layoutInflater.inflate(R.layout.pop_eq_portrait, null);
            } else {
                rootView = layoutInflater.inflate(R.layout.pop_eq, null);
            }
            rootView.setOnClickListener(onClickBackground);
            popupWindow = new PopupWindow(rootView, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);

            layout = (LinearLayout) rootView.findViewById(R.id.content);
            createUI(layout, layoutInflater);
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    void createUI(LinearLayout layout, LayoutInflater inflater) {

//        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        String TAG = PARENT_TAG + "-createUI";
        try {
            for (int i = 0; i < numBands; i++) {
                TextView tv;

                tv = new TextView(context);
                tv.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                tv.setGravity(Gravity.CENTER_HORIZONTAL);
                tv.setText((eq.getCenterFreq((short) i) / 1000) + " Hz");
                layout.addView(tv);


                LinearLayout cell = (LinearLayout) inflater.inflate(R.layout.cell_equalizerband, null);

                tv = (TextView) cell.findViewById(R.id.tvLower);
                tv.setText((bandLevel[0] / 100) + " dB");
                tv = (TextView) cell.findViewById(R.id.tvUpper);
                tv.setText((bandLevel[1] / 100) + " dB");

                int midValue = (bandLevel[1] - bandLevel[0]) / 2;
                int value = eq.getBandLevel((short) i) + midValue;
                if (i == 0) {
                    int lastValue = Global.getFirstBand();
                    if (lastValue == 0)
                        Global.setFirstBand(value);
                    else
                        value = Global.getFirstBand();
                } else if (i == 1) {
                    int lastValue = Global.getSecondsBand();
                    if (lastValue == 0)
                        Global.setSecondsBand(value);
                    else
                        value = Global.getSecondsBand();
                } else if (i == 2) {
                    int lastValue = Global.getThirdBand();
                    if (lastValue == 0)
                        Global.setThirdBand(value);
                    else
                        value = Global.getThirdBand();
                } else if (i == 3) {
                    int lastValue = Global.getForthBand();
                    if (lastValue == 0)
                        Global.setForthBand(value);
                    else
                        value = Global.getForthBand();
                } else if (i == 4) {
                    int lastValue = Global.getFiveBand();
                    if (lastValue == 0)
                        Global.setFiveBand(value);
                    else
                        value = Global.getFiveBand();
                }
                SeekBar seekBar = (SeekBar) cell.findViewById(R.id.seekBar);
                seekBar.setMax(bandLevel[1] - bandLevel[0]);
                seekBar.setProgress(value);
                seekBar.setId(i);
                seekBar.getProgressDrawable().setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_IN);
                seekBar.getThumb().setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_IN);
                int finalI = i;
                seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                    @Override
                    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                        int value = progress + bandLevel[0];
                        eq.setBandLevel((short) seekBar.getId(), (short) (value));
                        if (finalI == 0) {
                            Global.setFirstBand(progress);
                        } else if (finalI == 1) {
                            Global.setSecondsBand(progress);
                        } else if (finalI == 2) {
                            Global.setThirdBand(progress);
                        } else if (finalI == 3) {
                            Global.setForthBand(progress);
                        } else if (finalI == 4) {
                            Global.setFiveBand(progress);
                        }
                    }

                    @Override
                    public void onStartTrackingTouch(SeekBar seekBar) {
                        int progress = seekBar.getProgress();
                        eq.setBandLevel((short) seekBar.getId(), (short) (progress + bandLevel[0]));
                        int value = eq.getBandLevel((short) finalI) + midValue;
                        if (finalI == 0) {
                            int lastValue = Global.getFirstBand();
                            if (lastValue == 0)
                                Global.setFirstBand(value);
                            else
                                value = Global.getFirstBand();
                        } else if (finalI == 1) {
                            int lastValue = Global.getSecondsBand();
                            if (lastValue == 0)
                                Global.setSecondsBand(value);
                            else
                                value = Global.getSecondsBand();
                        } else if (finalI == 2) {
                            int lastValue = Global.getThirdBand();
                            if (lastValue == 0)
                                Global.setThirdBand(value);
                            else
                                value = Global.getThirdBand();
                        } else if (finalI == 3) {
                            int lastValue = Global.getForthBand();
                            if (lastValue == 0)
                                Global.setForthBand(value);
                            else
                                value = Global.getForthBand();
                        } else if (finalI == 4) {
                            int lastValue = Global.getFiveBand();
                            if (lastValue == 0)
                                Global.setFiveBand(value);
                            else
                                value = Global.getFiveBand();
                        }
                    }

                    @Override
                    public void onStopTrackingTouch(SeekBar seekBar) {

                    }
                });

//            Log.i(TAG, tv.getText() + " " + bandLevel[1] + " - " + bandLevel[0] + " = " + eq.getBandLevel((short) i));

                layout.addView(cell);
            }
            Button button = new Button(context, null, android.R.style.DeviceDefault_ButtonBar_AlertDialog);
            button.setText("Close");
            button.setGravity(Gravity.RIGHT);
            button.setClickable(true);
            button.setBackground(context.getDrawable(R.drawable.selector_button));
            button.setTextColor(context.getColor(R.color.selector_text_button));
            button.setFocusable(true);
            LinearLayout.LayoutParams param = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT);
            param.gravity = Gravity.RIGHT;
//        param.setMargins(100,100,200,200);
            button.setPadding(10, 10, 10, 10);
            button.setLayoutParams(param);
            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    popupWindow.dismiss();
                }
            });
            layout.addView(button);
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    public void show(View viewAnchor) {
//        View view = rootView.findViewById(android.R.id.content);
        String TAG = PARENT_TAG + "-show";
        try {
            popupWindow.setFocusable(true);
            popupWindow.showAtLocation(viewAnchor, Gravity.NO_GRAVITY, 0, 0);
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    View.OnClickListener onClickBackground = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            String TAG = PARENT_TAG + "-ClickListener";
            try {
                popupWindow.dismiss();
            } catch (Exception ex) {
                Debug.e(TAG, ex);
            }
        }
    };

  /*  @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        eq.setBandLevel((short) seekBar.getId(), (short) (progress + bandLevel[0]));
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {
        int progress = seekBar.getProgress();
        eq.setBandLevel((short) seekBar.getId(), (short) (progress + bandLevel[0]));
    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {

    }*/
}

package com.madeira.entertainz.helper;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import com.madeira.entertainz.karaoke.Debug;

/**
 * Created by Erick on 9/21/2018.
 */

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    String TAG = "RecyclerViewAdapter";

    public interface IRecyclerViewAdapter {
        RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType);
        void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position);
        int getItemCount();
    }

    public interface IOnViewType {
        int onGetViewType(int position);
    }

    private IRecyclerViewAdapter callback;
    private IOnViewType callbackViewType;

    public RecyclerViewAdapter(IRecyclerViewAdapter callback){
        this.callback = callback;
    }

    public void setOnViewType(IOnViewType callback){
        callbackViewType = callback;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return callback.onCreateViewHolder(parent, viewType);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        try {
            callback.onBindViewHolder(holder, position);
        }catch (Exception ex)
        {
            Debug.e(TAG, ex);
        }
    }

    @Override
    public int getItemCount() {
        return callback.getItemCount();
    }

    @Override
    public int getItemViewType(int position) {
//        if (callbackViewType==null) return super.getItemViewType(position);
//        return callbackViewType.onGetViewType(position);
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

}

package com.madeira.entertainz.helper.controller.media_exoplayer;

import android.app.Activity;
import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.DrawableRes;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewManager;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.SeekBar;
import android.widget.TextView;

import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.ExoPlayer;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.ui.SimpleExoPlayerView;
import com.madeira.entertainz.karaoke.R;

import java.lang.ref.WeakReference;
import java.util.Formatter;
import java.util.Locale;


/**
 * Created by Bruce Too
 * On 7/12/16.
 * At 16:09
 */
public class ExoControllerView extends FrameLayout {

    private static final String TAG = "ExoControllerView";

    private static final int HANDLER_ANIMATE_OUT = 1;// out animate
    private static final int HANDLER_UPDATE_PROGRESS = 2;//cycle update progress
    private static final long PROGRESS_SEEK = 500;
    private static final long ANIMATE_TIME = 300;
    private static final long interval = 5000;
    private SimpleExoPlayer mediaPlayer;

    private View rootView; // root view of this
    private SeekBar seekBar; //seek bar for video
    private TextView endTimeTV, currentTimeTV;
    private boolean isShowing;//controller view showing
    private boolean isDragging; //is dragging seekBar
    private StringBuilder formatBuilder;
    private Formatter formatter;

    private Activity context;
    private boolean canSeekVideo;
    private boolean canControllBrightnes;
    private String videoTitle;
    private MediaPlayerControlListener mediaPlayerControlListener;
    private ViewGroup anchorView;
    private SimpleExoPlayerView surfaceView;

    @DrawableRes
    private int pauseIcon;
    @DrawableRes
    private int playIcon;
    @DrawableRes
    private int prevSecIcon;
    @DrawableRes
    private int prevIcon;
    @DrawableRes
    private int nextSecIcon;
    @DrawableRes
    private int nextIcon;
    @DrawableRes
    private int equalizerIcon;


    private View topLayout;
    private TextView titleText;


    private View bottomLayout;
    private ImageButton playButton;
    private ImageButton prevSecButton;
    private ImageButton prevButton;
    private ImageButton nextSecButton;
    private ImageButton nextButton;
    private ImageButton equalizerButton;

    private Handler handler = new ControllerViewHandler(this);

    public ExoControllerView(Builder builder) {
        super(builder.context);
        this.context = builder.context;
        this.mediaPlayerControlListener = builder.mediaPlayerControlListener;
        this.videoTitle = builder.videoTitle;
        this.canSeekVideo = builder.canSeekVideo;
//        this.mCanControlVolume = builder.canControlVolume;
        this.canControllBrightnes = builder.canControlBrightness;
        this.pauseIcon = builder.pauseIcon;
        this.playIcon = builder.playIcon;
        this.surfaceView = builder.surfaceView;
        this.prevSecIcon = builder.prevSecIcon;
        this.prevIcon = builder.prevIcon;
        this.nextSecIcon = builder.nextSecIcon;
        this.nextIcon = builder.nextIcon;
        this.equalizerIcon = builder.equalizerIcon;

        setAnchorView(builder.anchorView);
        this.surfaceView.setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                toggleControllerView();
                return false;
            }
        });

        this.surfaceView.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                toggleControllerView();

            }
        });

        mediaPlayer = surfaceView.getPlayer();

        this.mediaPlayer.addListener(new ExoPlayer.EventListener() {
            @Override
            public void onLoadingChanged(boolean isLoading) {

            }

            @Override
            public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
                if (playWhenReady && playbackState == ExoPlayer.STATE_READY) {
                    // media actually playing
                    mediaPlayerControlListener.start(mediaPlayer);
                }
            }

            @Override
            public void onTimelineChanged(Timeline timeline, Object manifest) {

            }

            @Override
            public void onPlayerError(ExoPlaybackException error) {

            }

            @Override
            public void onPositionDiscontinuity() {

            }
        });

       /* mediaPlayer.addListener(new ExoPlayer.EventListener() {
            @Override
            public void onLoadingChanged(boolean isLoading) {

            }

            @Override
            public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
                if (playWhenReady && playbackState == ExoPlayer.STATE_READY) {
                    // media actually playing
                    mediaPlayerControlListener.start(mediaPlayer);
                }
            }

            @Override
            public void onTimelineChanged(Timeline timeline, Object manifest) {

            }

            @Override
            public void onPlayerError(ExoPlaybackException error) {

            }

            @Override
            public void onPositionDiscontinuity() {

            }
        });

        mediaPlayer.addListener(new Player.EventListener() {
            @Override
            public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {

            }
        });*/

       /* this.surfaceView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                mediaPlayerControlListener.start(mp);
                mediaPlayer = mp;
            }
        });*/

        this.surfaceView.setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                toggleControllerView();
                return false;
            }
        });

        this.surfaceView.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                toggleControllerView();

            }
        });

        this.surfaceView.setOnFocusChangeListener(new OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                  /*  if (!isShowing())
                        show();*/
                } else {
//                    if (!playButton.isFocused() && !mFullscreenButton.isFocused()&& !seekBar.isFocused())
                    if (!playButton.isFocused() && !seekBar.isFocused() && !prevButton.isFocused() && !prevSecButton.isFocused() && !nextButton.isFocused() && !nextSecButton.isFocused() && !equalizerButton.isFocused())
                        if (isShowing()) {
                            unShow();
                        }
                }
            }
        });

        this.playButton.setOnFocusChangeListener(new OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
//                    if (!surfaceView.isFocused() && !mFullscreenButton.isFocused()&& !seekBar.isFocused())
                    if (!surfaceView.isFocused() && !seekBar.isFocused() && !prevButton.isFocused() && !prevSecButton.isFocused() && !nextButton.isFocused() && !nextSecButton.isFocused())
                        if (isShowing()) {
                            unShow();
                            mediaPlayerControlListener.unFocused();
                        }
                }
            }
        });

        this.seekBar.setOnFocusChangeListener(new OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
//                    if (!playButton.isFocused() && !surfaceView.isFocused()&& !mFullscreenButton.isFocused())
                    if (!playButton.isFocused() && !surfaceView.isFocused())
                        if (isShowing())
                            unShow();
                }
            }
        });

        if (builder.videoTitle.equals(""))
            topLayout.setVisibility(GONE);
        else
            topLayout.setVisibility(VISIBLE);
    }

    public static class Builder {

        private Activity context;
        private boolean canSeekVideo = false;
        private boolean canControlVolume = true;
        private boolean canControlBrightness = true;
        private String videoTitle = "";
        private MediaPlayerControlListener mediaPlayerControlListener;
        private ViewGroup anchorView;
        private SimpleExoPlayerView surfaceView;
        @DrawableRes
        private int pauseIcon = R.drawable.selector_pause;
        @DrawableRes
        private int playIcon = R.drawable.selector_play;
        @DrawableRes
        private int shrinkIcon = R.drawable.selector_fullscreen;
        @DrawableRes
        private int stretchIcon = R.drawable.selector_fullscreen_exit;
        @DrawableRes
        private int prevIcon = R.drawable.selector_previous;
        @DrawableRes
        private int prevSecIcon = R.drawable.selector_prev_sec;
        @DrawableRes
        private int nextIcon = R.drawable.selector_next;
        @DrawableRes
        private int nextSecIcon = R.drawable.selector_next_sec;
        @DrawableRes
        private int equalizerIcon = R.drawable.selector_equalizer;

        //Required
        public Builder(@Nullable Activity context, @Nullable MediaPlayerControlListener mediaControlListener) {
            this.context = context;
            this.mediaPlayerControlListener = mediaControlListener;
        }

        public Builder with(@Nullable Activity context) {
            this.context = context;
            return this;
        }

        public Builder withMediaControlListener(@Nullable MediaPlayerControlListener mediaControlListener) {
            this.mediaPlayerControlListener = mediaControlListener;
            return this;
        }

        //Options
        public Builder withVideoTitle(String videoTitle) {
            this.videoTitle = videoTitle;
            return this;
        }

        public Builder withVideoSurfaceView(@Nullable SimpleExoPlayerView surfaceView) {
            this.surfaceView = surfaceView;
            return this;
        }

        public Builder exitIcon(@DrawableRes int exitIcon) {
            return this;
        }

        public Builder pauseIcon(@DrawableRes int pauseIcon) {
            this.pauseIcon = pauseIcon;
            return this;
        }

        public Builder playIcon(@DrawableRes int playIcon) {
            this.playIcon = playIcon;
            return this;
        }

        public Builder shrinkIcon(@DrawableRes int shrinkIcon) {
            this.shrinkIcon = shrinkIcon;
            return this;
        }

        public Builder stretchIcon(@DrawableRes int stretchIcon) {
            this.stretchIcon = stretchIcon;
            return this;
        }

        public Builder canSeekVideo(boolean canSeekVideo) {
            this.canSeekVideo = canSeekVideo;
            return this;
        }

        public Builder canControlVolume(boolean canControlVolume) {
            this.canControlVolume = canControlVolume;
            return this;
        }

        public Builder canControlBrightness(boolean canControlBrightness) {
            this.canControlBrightness = canControlBrightness;
            return this;
        }

        public Builder prevSecIcon(@DrawableRes int prevSecIcon) {
            this.prevSecIcon = prevSecIcon;
            return this;
        }

        public Builder prevIcon(@DrawableRes int prevIcon) {
            this.prevIcon = prevIcon;
            return this;
        }

        public Builder nextSecIcon(@DrawableRes int nextSecIcon) {
            this.nextSecIcon = nextSecIcon;
            return this;
        }

        public Builder nextIcon(@DrawableRes int nextIcon) {
            this.nextIcon = nextIcon;
            return this;
        }

        public Builder equalizerIcon(@DrawableRes int equalizerIcon) {
            this.equalizerIcon = equalizerIcon;
            return this;
        }

        public ExoControllerView build(@Nullable ViewGroup anchorView) {
            this.anchorView = anchorView;
            return new ExoControllerView(this);
        }

    }

    /**
     * Handler prevent leak memory.
     */
    private static class ControllerViewHandler extends Handler {
        private final WeakReference<ExoControllerView> mView;

        ControllerViewHandler(ExoControllerView view) {
            mView = new WeakReference<>(view);
        }

        @Override
        public void handleMessage(Message msg) {
            ExoControllerView view = mView.get();
            if (view == null || view.mediaPlayerControlListener == null) {
                return;
            }

            int pos;
            switch (msg.what) {
                case HANDLER_ANIMATE_OUT:
                    view.hide();
                    break;
                case HANDLER_UPDATE_PROGRESS://cycle update seek bar progress
                    pos = view.setSeekProgress();
                    if (!view.isDragging && view.isShowing && view.mediaPlayerControlListener.isPlaying()) {//just in case
                        //cycle update
                        msg = obtainMessage(HANDLER_UPDATE_PROGRESS);
                        sendMessageDelayed(msg, 1000 - (pos % 1000));
                    }
                    break;
            }
        }
    }

    /**
     * Inflate view from exit xml layout
     *
     * @return the root view of {@link ExoControllerView}
     */
    private View makeControllerView() {
        LayoutInflater inflate = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        rootView = inflate.inflate(R.layout.media_controller, null);
        initControllerView();

        return rootView;
    }

    /**
     * find all views inside {@link ExoControllerView}
     * and init params
     */

    public void exitController() {
        mediaPlayerControlListener.exit();
    }

    private void initControllerView() {
        topLayout = rootView.findViewById(R.id.layout_top);
        titleText = rootView.findViewById(R.id.top_title);
        bottomLayout = rootView.findViewById(R.id.layout_bottom);
        playButton = rootView.findViewById(R.id.playButton);
        prevSecButton = rootView.findViewById(R.id.prevSecButton);
        prevButton = rootView.findViewById(R.id.prevButton);
        nextSecButton = rootView.findViewById(R.id.nextSecButton);
        nextButton = rootView.findViewById(R.id.nextButton);
        equalizerButton = rootView.findViewById(R.id.equalizerButton);

        if (playButton != null) {
            playButton.requestFocus();
            playButton.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    doPauseResume();
                    show();
                }
            });
        }

        if (prevSecButton != null) {
            prevSecButton.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (mediaPlayerControlListener == null) {
                        return;
                    }
                    int currentSecond = mediaPlayerControlListener.getCurrentPosition();
                    int valueSeconds = 0;
                    if (currentSecond >= interval) {
                        valueSeconds = (int) (currentSecond - interval);
                    }
                    mediaPlayerControlListener.seekTo(valueSeconds);
                }
            });
        }

        if (prevButton != null) {
            prevButton.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    mediaPlayerControlListener.prevSong();
                }
            });
        }

        if (nextSecButton != null) {
            nextSecButton.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mediaPlayerControlListener == null) {
                        return;
                    }
                    int currentSecond = mediaPlayerControlListener.getCurrentPosition();
                    int totalDuration = mediaPlayerControlListener.getDuration();
                    int diffSeconds = totalDuration - currentSecond;
                    int valueSeconds = (int) (mediaPlayerControlListener.getDuration() - 1000L);
                    if (diffSeconds > interval) {
                        valueSeconds = (int) (currentSecond + interval);
                    }
                    mediaPlayerControlListener.seekTo(valueSeconds);
                }
            });
        }

        if (nextButton != null) {
            nextButton.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    mediaPlayerControlListener.nextSong();
                }
            });
        }


        seekBar = rootView.findViewById(R.id.bottom_seekbar);
        if (seekBar != null) {
            seekBar.setOnSeekBarChangeListener(mSeekListener);
            seekBar.setMax(1000);

        }

        endTimeTV = rootView.findViewById(R.id.bottom_time);
        currentTimeTV = rootView.findViewById(R.id.bottom_time_current);

        formatBuilder = new StringBuilder();
        formatter = new Formatter(formatBuilder, Locale.getDefault());

        if (equalizerButton != null) {
            equalizerButton.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    mediaPlayerControlListener.equalizer();
                }
            });
        }

    }

    /**
     * show controller view
     */
    private void show() {

        if (!isShowing && anchorView != null) {

            //add controller view to bottom of the AnchorView
            LayoutParams tlp = new LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT);
            View view = ExoControllerView.this;
           /* if (view.getParent() != null) {
                anchorView.removeView(view); // <- fix
            }*/


            ViewAnimator.putOn(topLayout)
                    .waitForSize(new ViewAnimator.Listeners.Size() {
                        @Override
                        public void onSize(ViewAnimator viewAnimator) {
                            viewAnimator.animate()
                                    .translationY(-topLayout.getHeight(), 0)
                                    .duration(ANIMATE_TIME)
                                    .andAnimate(bottomLayout)
                                    .translationY(bottomLayout.getHeight(), 0)
                                    .duration(ANIMATE_TIME)
                                    .start(new ViewAnimator.Listeners.Start() {
                                        @Override
                                        public void onStart() {
                                            isShowing = true;
                                            handler.sendEmptyMessage(HANDLER_UPDATE_PROGRESS);
                                        }
                                    });
                        }
                    });

           /* View checkIfExists = anchorView.findViewById(view.getId());
            if (checkIfExists == null) {
//                anchorView.removeAllViews();
                anchorView.addView(view, tlp);
            }
*/
            if (((ViewManager) view.getParent()) != null)
                ((ViewManager) view.getParent()).removeView(view);
            anchorView.addView(view, tlp);

        }

        setSeekProgress();
        if (playButton != null) {
            playButton.requestFocus();
        }
        togglePausePlay();
//        toggleFullScreen();
        //update progress
        handler.sendEmptyMessage(HANDLER_UPDATE_PROGRESS);

    }

    /**
     * toggle {@link ExoControllerView} show or not
     * this can be called when {@link View#onTouchEvent(MotionEvent)} happened
     */
    public void toggleControllerView() {
        if (!isShowing()) {
            show();
        } else {
            unShow();
        }
    }

    public void unShow() {
        Message msg = handler.obtainMessage(HANDLER_ANIMATE_OUT);
        //remove exist one first
        handler.removeMessages(HANDLER_ANIMATE_OUT);
        handler.sendMessageDelayed(msg, 100);
    }


    /**
     * if {@link ExoControllerView} is visible
     *
     * @return showing or not
     */
    public boolean isShowing() {
        return isShowing;
    }

    /**
     * hide controller view with animation
     * With custom animation
     */
    private void hide() {
        if (anchorView == null) {
            return;
        }

        ViewAnimator.putOn(topLayout)
                .animate()
                .translationY(-topLayout.getHeight())
                .duration(ANIMATE_TIME)

                .andAnimate(bottomLayout)
                .translationY(bottomLayout.getHeight())
                .duration(ANIMATE_TIME)
                .end(new ViewAnimator.Listeners.End() {
                    @Override
                    public void onEnd() {
                        anchorView.removeView(ExoControllerView.this);
                        handler.removeMessages(HANDLER_UPDATE_PROGRESS);
                        isShowing = false;
                    }
                });
    }

    /**
     * convert string to time
     *
     * @param timeMs time to be formatted
     * @return 00:00:00
     */
    private String stringToTime(int timeMs) {
        int totalSeconds = timeMs / 1000;

        int seconds = totalSeconds % 60;
        int minutes = (totalSeconds / 60) % 60;
        int hours = totalSeconds / 3600;

        formatBuilder.setLength(0);
        if (hours > 0) {
            return formatter.format("%d:%02d:%02d", hours, minutes, seconds).toString();
        } else {
            return formatter.format("%02d:%02d", minutes, seconds).toString();
        }
    }

    /**
     * set {@link #seekBar} progress
     * and video play time {@link #currentTimeTV}
     *
     * @return current play position
     */
    private int setSeekProgress() {
        if (mediaPlayerControlListener == null || isDragging) {
            return 0;
        }

        int position = mediaPlayerControlListener.getCurrentPosition();
        int duration = mediaPlayerControlListener.getDuration();
        if (seekBar != null) {
            if (duration > 0) {
                // use long to avoid overflow
                long pos = 1000L * position / duration;
                seekBar.setProgress((int) pos);
            }
            //get buffer percentage
            int percent = mediaPlayerControlListener.getBufferPercentage();
            //set buffer progress
            seekBar.setSecondaryProgress(percent * 10);
        }

        if (endTimeTV != null)
            endTimeTV.setText(stringToTime(duration));
        if (currentTimeTV != null) {
//            Log.e(TAG, "position:" + position + " -> duration:" + duration);
            currentTimeTV.setText(stringToTime(position));
            if (mediaPlayerControlListener.isComplete()) {
                currentTimeTV.setText(stringToTime(duration));
                playButton.setImageResource(playIcon);
            }
        }
        titleText.setText(videoTitle);
        return position;
    }


    /**
     * toggle pause or play
     */
    private void togglePausePlay() {
        if (rootView == null || playButton == null || mediaPlayerControlListener == null) {
            return;
        }

        if (mediaPlayerControlListener.isPlaying()) {
            playButton.setImageResource(pauseIcon);
        } else {
            playButton.setImageResource(playIcon);
        }
    }


    private void doPauseResume() {
        if (mediaPlayerControlListener == null) {
            return;
        }

        if (mediaPlayerControlListener.isPlaying()) {
            mediaPlayerControlListener.pause();
        } else {
            mediaPlayerControlListener.start(mediaPlayer);
        }
        togglePausePlay();
    }

    public void refreshProgress() {

        handler.sendEmptyMessage(HANDLER_UPDATE_PROGRESS);
        togglePausePlay();
//        playButton.setImageResource(pauseIcon);
//        toggleControllerView();

//        setSeekProgress();
    }

    private void doToggleFullscreen() {
        if (mediaPlayerControlListener == null) {
            return;
        }

        mediaPlayerControlListener.toggleFullScreen();
    }

    /**
     * Seek bar drag listener
     */
    private SeekBar.OnSeekBarChangeListener mSeekListener = new SeekBar.OnSeekBarChangeListener() {
        public void onStartTrackingTouch(SeekBar bar) {
            show();
            isDragging = true;
            handler.removeMessages(HANDLER_UPDATE_PROGRESS);
        }

        public void onProgressChanged(SeekBar bar, int progress, boolean fromuser) {
            if (mediaPlayerControlListener == null) {
                return;
            }

            if (!fromuser) {
                return;
            }

            long duration = mediaPlayerControlListener.getDuration();
            long newPosition = (duration * progress) / 1000L;
            mediaPlayerControlListener.seekTo((int) newPosition);
            if (currentTimeTV != null)
                currentTimeTV.setText(stringToTime((int) newPosition));
        }

        public void onStopTrackingTouch(SeekBar bar) {
            isDragging = false;
            setSeekProgress();
            togglePausePlay();
            show();
            handler.sendEmptyMessage(HANDLER_UPDATE_PROGRESS);
        }
    };

    @Override
    public void setEnabled(boolean enabled) {
        if (playButton != null) {
            playButton.setEnabled(enabled);
        }
        if (seekBar != null) {
            seekBar.setEnabled(enabled);
        }
        super.setEnabled(enabled);
    }


    /**
     * set top back click listener
     */
    private OnClickListener mBackListener = new OnClickListener() {
        public void onClick(View v) {
            mediaPlayerControlListener.exit();
        }
    };


    /**
     * set full screen click listener
     */
    private OnClickListener mFullscreenListener = new OnClickListener() {
        public void onClick(View v) {
            doToggleFullscreen();
            show();
        }
    };

    /**
     * setMediaPlayerControlListener update play state
     *
     * @param mediaPlayerListener self
     */
    public void setMediaPlayerControlListener(MediaPlayerControlListener mediaPlayerListener) {
        mediaPlayerControlListener = mediaPlayerListener;
        togglePausePlay();
//        toggleFullScreen();
    }

    /**
     * set anchor view
     *
     * @param view view that hold controller view
     */
    private void setAnchorView(ViewGroup view) {
        anchorView = view;
        LayoutParams frameParams = new LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
        );
        //remove all before add view
        removeAllViews();
        View v = makeControllerView();
        addView(v, frameParams);

    }

    /**
     * set gesture listen to control media player
     * include screen brightness and volume of video
     * and seek video play
     */


    private void seekBackWard() {
        if (mediaPlayerControlListener == null) {
            return;
        }

        int pos = mediaPlayerControlListener.getCurrentPosition();
        pos -= PROGRESS_SEEK;
        mediaPlayerControlListener.seekTo(pos);
        setSeekProgress();

        show();
    }

    private void seekForWard() {
        if (mediaPlayerControlListener == null) {
            return;
        }

        int pos = mediaPlayerControlListener.getCurrentPosition();
        pos += PROGRESS_SEEK;
        mediaPlayerControlListener.seekTo(pos);
        setSeekProgress();

        show();
    }


    public interface MediaPlayerControlListener {
        /**
         * start play video
         */
        void start(SimpleExoPlayer mp);

        /**
         * pause video
         */
        void pause();

        /**
         * get video total time
         *
         * @return total time
         */
        int getDuration();

        /**
         * get video current position
         *
         * @return current position
         */
        int getCurrentPosition();

        /**
         * seek video to exactly position
         *
         * @param position position
         */
        void seekTo(int position);

        /**
         * video is playing state
         *
         * @return is video playing
         */
        boolean isPlaying();

        /**
         * video is complete
         *
         * @return complete or not
         */
        boolean isComplete();

        /**
         * get buffer percent
         *
         * @return percent
         */
        int getBufferPercentage();

        /**
         * video is full screen
         * in order to control image src...
         *
         * @return fullScreen
         */
        boolean isFullScreen();

        /**
         * toggle fullScreen
         */
        void toggleFullScreen();

        /**
         * exit media player
         */
        void exit();

        void unFocused();

        void nextSong();

        void prevSong();

        void equalizer();

    }
}
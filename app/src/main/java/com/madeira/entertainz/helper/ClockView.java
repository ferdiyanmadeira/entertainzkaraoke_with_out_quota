package com.madeira.entertainz.helper;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.View;

import com.madeira.entertainz.karaoke.Debug;

import java.util.Calendar;
import java.util.TimeZone;

public class ClockView extends View {
    String TAG = "ClockView";

    private int height, width = 0;
    private int padding = 0;
    private int fontSize = 0;
    private int numeralSpacing = 0;
    private int handTruncation, hourHandTruncation = 0;
    private int radius = 0;
    private double hourmove = 0;
    private Paint paint;
    private boolean isInit;
    private int[] numbers = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12};
    private Rect rect = new Rect();
    public Canvas canvas;
    public String time = "";

    public ClockView(Context context) {
        super(context);
    }

    public ClockView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ClockView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public void settime(String timezone) {
        time = timezone;
    }

    private void initClock() {
        try {
            height = getHeight();
            width = getWidth();
            padding = numeralSpacing + 25;
            fontSize = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, 13,
                    getResources().getDisplayMetrics());
            int min = Math.min(240, 240);
            radius = min / 2 - padding;
            handTruncation = min / 20;
            hourHandTruncation = min / 9;
            paint = new Paint();
            isInit = true;
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    @Override
    protected void onDraw(Canvas canvas) {
        try {
            if (!isInit) {
                initClock();
            }
            this.canvas = canvas;

            canvas.drawColor(Color.TRANSPARENT);
            drawCircle(canvas);
            drawCenter(canvas);
            drawNumeral(canvas);
            drawHands(canvas);

            postInvalidateDelayed(500);
            invalidate();
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    //draw height hand
    private void drawHand(Canvas canvas, double loc, boolean isHour) {
        try {
            double angle = Math.PI * loc / 30 - Math.PI / 2;
            int handRadius = isHour ? radius - handTruncation - hourHandTruncation : radius - handTruncation;
            canvas.drawLine(width / 2, height / 2,
                    (float) (width / 2 + Math.cos(angle) * handRadius),
                    (float) (height / 2 + Math.sin(angle) * handRadius),
                    paint);
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    //draw hand position
    public void drawHands(Canvas canvas) {
        try {
            TimeZone tz = TimeZone.getTimeZone(time);
            Calendar c = Calendar.getInstance(tz);
            float hour = c.get(Calendar.HOUR_OF_DAY);
            hour = hour > 12 ? hour - 12 : hour;
            if (c.get(Calendar.MINUTE) >= 0 && c.get(Calendar.MINUTE) < 10) {
                hourmove = 0;
            } else if (c.get(Calendar.MINUTE) >= 10 && c.get(Calendar.MINUTE) < 20) {
                hourmove = 1;
            } else if (c.get(Calendar.MINUTE) >= 20 && c.get(Calendar.MINUTE) < 30) {
                hourmove = 2;
            } else if (c.get(Calendar.MINUTE) >= 30 && c.get(Calendar.MINUTE) < 40) {
                hourmove = 3;
            } else if (c.get(Calendar.MINUTE) >= 40 && c.get(Calendar.MINUTE) < 50) {
                hourmove = 4;
            } else if (c.get(Calendar.MINUTE) >= 50 && c.get(Calendar.MINUTE) < 60) {
                hourmove = 4.5;
            }
            drawHand(canvas, (hour + c.get(Calendar.MINUTE) / 60) * 5f + hourmove, true);
            drawHand(canvas, c.get(Calendar.MINUTE), false);
            drawHand(canvas, c.get(Calendar.SECOND), false);
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    private void drawNumeral(Canvas canvas) {
        try {
            paint.setTextSize(fontSize);

            for (int number : numbers) {
                String tmp = String.valueOf(number);
                paint.getTextBounds(tmp, 0, tmp.length(), rect);
                double angle = Math.PI / 6 * (number - 3);
                int x = (int) (width / 2 + Math.cos(angle) * radius - rect.width() / 2);
                int y = (int) (height / 2 + Math.sin(angle) * radius + rect.height() / 2);
                canvas.drawText(tmp, x, y, paint);
            }
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    private void drawCenter(Canvas canvas) {
        try {
            paint.setStyle(Paint.Style.FILL);
            canvas.drawCircle(width / 2, height / 2, 12, paint);
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    private void drawCircle(Canvas canvas) {
        try {
            paint.reset();
            paint.setColor(getResources().getColor(android.R.color.white));
            paint.setStrokeWidth(3);
            paint.setStyle(Paint.Style.STROKE);
            paint.setAntiAlias(true);
            canvas.drawCircle(width / 2, height / 2, radius + padding - 10, paint);
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

}
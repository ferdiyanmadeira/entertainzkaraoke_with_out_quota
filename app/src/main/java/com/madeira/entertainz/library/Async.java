package com.madeira.entertainz.library;


import android.os.Handler;
import android.os.Message;

public class Async extends Thread {
    private static final String TAG = "Async";
    private static final int VALUE_RESULT_OBJECT = 100;
    private static final int VALUE_OBJECT = 200;
    private static final int VALUE_INT = 300;
    private static final int VALUE_EXCEPTION = 999;


    public interface Callback{
        Object onBackground(Async async);
    }
    public interface CallbackException{
        void onException(Exception e);
    }
    public interface CallbackResult{
        void onResult(Object result);
    }
    public interface CallbackPostObject{
        void onPostObject(Object result);
    }
    public interface CallbackPostInt{
        void onPostInt(int value);
    }

    private Handler handler;

    private Callback mCallback;
    private CallbackResult mCallbackResult;
    private CallbackException mCallbackException;
    private CallbackPostObject mCallbackPostObject;
    private CallbackPostInt mCallbackPostInt;

    static public Async run(final Callback callback){

        Async async = new Async();

        async.mCallback = callback;
        async.start();

        return async;
    }

    public Async(){

        handler = new Handler(new Handler.Callback() {
            @Override
            public boolean handleMessage(Message msg) {

                switch (msg.what){
                    case VALUE_RESULT_OBJECT:
                        if (mCallbackResult!=null) mCallbackResult.onResult(msg.obj);
                        break;

                    case VALUE_EXCEPTION:
                        if (mCallbackException!=null) mCallbackException.onException((Exception) msg.obj);
                        break;

                    case VALUE_OBJECT:
                        if (mCallbackPostObject!=null) mCallbackPostObject.onPostObject(msg.obj);
                        break;

                    case VALUE_INT:
                        if (mCallbackPostInt!=null) mCallbackPostInt.onPostInt(msg.arg1);
                        break;
                }

                return false;
            }
        });
    }

    @Override
    public void run() {
        Object object = mCallback.onBackground(this);

        if (object instanceof Exception){
            //apabila return adalah exception, maka akan muncul di callbackexception
            postException((Exception) object);
        }
        else {
            //apabil return non exception maka akan muncul di normal callback result
            postResult(object);
        }
    }

    public Async setCallbackResult(CallbackResult callback){
        mCallbackResult = callback;
        return this;
    }

    public Async setCallbackException(CallbackException callback){
        mCallbackException = callback;
        return this;
    }

    public Async setCallbackPostObject(CallbackPostObject callback){
        mCallbackPostObject = callback;
        return this;
    }

    public Async setCallbackPostInt(CallbackPostInt callback){
        mCallbackPostInt = callback;
        return this;
    }

    private void postResult(Object object){
        Message msg = handler.obtainMessage();
        msg.what = VALUE_RESULT_OBJECT;
        msg.obj = object;
        handler.sendMessage(msg);
    }

    private void postException(Exception e){
        Message msg = handler.obtainMessage();
        msg.what = VALUE_EXCEPTION;
        msg.obj = e;
        handler.sendMessage(msg);
    }

    public void postObject(Object object){
        Message msg = handler.obtainMessage();
        msg.what = VALUE_OBJECT;
        msg.obj = object;
        handler.sendMessage(msg);
    }

    public void postInt(int value){
        Message msg = handler.obtainMessage();
        msg.what = VALUE_INT;
        msg.arg1 = value;
        handler.sendMessage(msg);
    }

    public void runInMainthread(Runnable runnable){
        handler.post(runnable);
    }
}

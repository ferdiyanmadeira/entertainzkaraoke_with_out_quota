package com.madeira.entertainz.library;

import com.google.gson.Gson;

public class ApiError extends Exception {

    //semua error yg di hasilkan dari app ini bernilai negatif
    //
    public static final int ERR_EXCEPTION = -9999;
    public static final int ERR_INVALID_URL = -1000;
    public static final int ERR_IO_EXCEPTION = -1002;
    public static final int ERR_RETURN_EMPTY = -1010;
    public static final int ERR_CONNECTION_TIMEOUT = -1020;
    public static final int ERR_UNKNOWN_RESPONSE = -1030;

    public static final String STR_ERR_RETURN_EMPTY = "Return emptry";


    public String errCode;
    public String errMsg;

    public static final ApiError newInstance(int errCode, String errMessage){
        ApiError apiError;
        return new ApiError(errCode, errMessage);
    }

    /**
     * conver Exception --> ApiError
     *
     * @param e
     * @return
     */
    public static final ApiError convert(Exception e){
        return new ApiError(ERR_EXCEPTION, e.getMessage());
    }

    public ApiError(int errCode, String errMessage){
        this.errCode = String.valueOf(errCode);
        this.errMsg = errMessage;
    }

    @Override
    public String toString() {
        return String.format("*** %s (%s) ***", errMsg, errCode);
    }

    /**
     * Function ini malakukan parse para str, apabila ada ApiError, apabila tdk ada
     * maka function akan lewat saja
     *
     * Akan throw error apabila ada ApiError
     *
     * @param gson
     * @param str
     * @throws ApiError
     */
    public static void process(Gson gson, String str) throws ApiError {

        ApiError apiError = null;
        try
        {
            apiError = gson.fromJson(str, ApiError.class);
            if (apiError.errCode==null) apiError = null; //reset null apabila tdk ada error

        } catch (Exception e){
            e.printStackTrace();
        }

        //apabila tdk ada error maka tdk perlu throw
        if (apiError!=null) throw apiError;
    }

    /**
     * Di pergunakan dalam PerformAsycn utk api
     *
     * @param gson
     * @param str apabila empty maka response nya UNKNOWN RESEPONSE
     * @return apiError atau null
     */
    public static ApiError parseApiError(Gson gson, String str) {

        ApiError apiError;

        try
        {
            if (str!=null){
                apiError = gson.fromJson(str, ApiError.class);
                if (apiError.errMsg==null) return null; //tdk ada error
                return apiError;
            }

        }catch (Exception e){
            e.printStackTrace();
            return new ApiError(ERR_UNKNOWN_RESPONSE, "Invalid response");
        }

        return new ApiError(ERR_IO_EXCEPTION, "Network Error");
    }

    /**
     * akan throw ApiError apabila str==null atau json error dari server
     * utk error connection tdk di handle disini
     *
     * @param gson pass gson object
     * @param str return string dari server
     * @throws ApiError
     */
    public static ApiError processError(Gson gson, String str) {
        ApiError apiError = null;
        try {
            if (str==null || str.length()==0){
                //apabila tdk ada value
                apiError = new ApiError(ERR_RETURN_EMPTY, STR_ERR_RETURN_EMPTY);
            } else {
                //error dari server
                apiError = gson.fromJson(str, ApiError.class);
                if (apiError.errCode==null) apiError = null; //reset null apabila tdk ada error
            }

        } catch (Exception e){
            e.printStackTrace();
            apiError = new ApiError(ERR_EXCEPTION, e.getMessage());
        }

        //apabila tdk ada error maka tdk perlu throw
//        if (apiError!=null) throw apiError;
        return apiError;
    }

}
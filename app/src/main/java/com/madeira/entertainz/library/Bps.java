package com.madeira.entertainz.library;

import android.util.Log;

import java.util.Date;

public class Bps {
    private static final String TAG = "Bps";

    private static long startTime;

    public static void reset(){
        startTime = getMillisecond();
    }

    /**
     * Hitung bit per second
     *
     * @param totalTransferedInBytes
     * @return
     */
    public static double calcBps(long totalTransferedInBytes){

        //lamanya transfer sejak terakhir di reset
        long duration = getMillisecond() - startTime;

        return (double) totalTransferedInBytes / ((double)duration/1000) * 8; //convert ke bit
    }

    private static long getMillisecond(){
        return new Date().getTime();
    }
}

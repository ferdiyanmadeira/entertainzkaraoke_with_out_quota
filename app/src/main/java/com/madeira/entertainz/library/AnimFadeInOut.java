package com.madeira.entertainz.library;

import android.os.Handler;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.LinearInterpolator;

public class AnimFadeInOut {
    private static final String TAG = "AnimFadeInOut";

    AnimationSet animationFadeInOut;
    Handler handler;

    View view;
    int duration;

    public AnimFadeInOut(View view){
        this.view = view;
        handler = new Handler();
    }

    public void start(int duration){

        this.duration = duration;

        Animation fadeIn = new AlphaAnimation(0.3f, 1);
        fadeIn.setDuration(duration);
        Animation fadeOut = new AlphaAnimation(1, 0.3f);
        fadeOut.setStartOffset(duration);
        fadeOut.setDuration(duration);
        animationFadeInOut = new AnimationSet(true);
        animationFadeInOut.addAnimation(fadeIn);
        animationFadeInOut.addAnimation(fadeOut);
        animationFadeInOut.setInterpolator(new LinearInterpolator());

        handler.post(run);
    }

    public void stop(){
        handler.removeCallbacksAndMessages(null);
    }

    Runnable run = new Runnable() {
        @Override
        public void run() {
            view.startAnimation(animationFadeInOut);
            handler.postDelayed(run, duration+duration);
        }
    };
}

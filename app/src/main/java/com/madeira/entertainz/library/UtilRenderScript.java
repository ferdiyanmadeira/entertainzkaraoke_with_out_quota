package com.madeira.entertainz.library;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.provider.MediaStore;
import android.renderscript.Allocation;
import android.renderscript.Element;
import android.renderscript.RenderScript;
//import android.support.v8.renderscript.Allocation;
//import android.support.v8.renderscript.RenderScript;
//import android.support.v8.renderscript.ScriptIntrinsicBlur;
//import android.support.v8.renderscript.ScriptIntrinsicResize;
//import android.support.v8.renderscript.Type;
import android.renderscript.ScriptIntrinsicBlur;
import android.renderscript.ScriptIntrinsicResize;
import android.renderscript.Type;
import android.widget.ImageView;

import com.madeira.entertainz.karaoke.Debug;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;


public class UtilRenderScript {

    static String TAG ="UtilRenderScript";

    public static Bitmap resizeBitmap(RenderScript rs, Bitmap src, int dstWidth) {
        Bitmap dst = null;
        try {
            if (src != null) {
                Bitmap.Config bitmapConfig = src.getConfig();
                int srcWidth = src.getWidth();
                int srcHeight = src.getHeight();
                float srcAspectRatio = (float) srcWidth / srcHeight;
                int dstHeight = (int) (dstWidth / srcAspectRatio);

                float resizeRatio = (float) srcWidth / dstWidth;

                /* Calculate gaussian's radius */
                float sigma = resizeRatio / (float) Math.PI;
                // https://android.googlesource.com/platform/frameworks/rs/+/master/cpu_ref/rsCpuIntrinsicBlur.cpp
                float radius = 2.5f * sigma - 1.5f;
                radius = Math.min(25, Math.max(0.0001f, radius));

                /* Gaussian filter */
                Allocation tmpIn = Allocation.createFromBitmap(rs, src);
                Allocation tmpFiltered = Allocation.createTyped(rs, tmpIn.getType());
                ScriptIntrinsicBlur blurInstrinsic = ScriptIntrinsicBlur.create(rs, tmpIn.getElement());

                blurInstrinsic.setRadius(radius);
                blurInstrinsic.setInput(tmpIn);
                blurInstrinsic.forEach(tmpFiltered);

                tmpIn.destroy();
                blurInstrinsic.destroy();

                /* Resize */
                dst = Bitmap.createBitmap(dstWidth, dstHeight, bitmapConfig);
                Type t = Type.createXY(rs, tmpFiltered.getElement(), dstWidth, dstHeight);
                Allocation tmpOut = Allocation.createTyped(rs, t);
                ScriptIntrinsicResize resizeIntrinsic = ScriptIntrinsicResize.create(rs);

                resizeIntrinsic.setInput(tmpFiltered);
                resizeIntrinsic.forEach_bicubic(tmpOut);
                tmpOut.copyTo(dst);

                tmpFiltered.destroy();
                tmpOut.destroy();
                resizeIntrinsic.destroy();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return dst;
    }

    public static Bitmap fetchBitmap(Context context, String src) {
        try {
           /* URL url = new URL(src);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();*/
            Uri uri = Uri.fromFile(new File(src));
            InputStream input = context.getContentResolver().openInputStream(uri);
            Bitmap myBitmap = BitmapFactory.decodeStream(input);
            return myBitmap;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    } // Author: silentnuke


    static public void fetchBitmap(Context context, String url, int widthInDp, ImageView imageView) {

        int newwidth = (int) Util.dp2Px(widthInDp);

        PerformAsync2.run(new PerformAsync2.Callback() {
            @Override
            public Object onBackground(PerformAsync2 performAsync) {

                Bitmap bitmap = null;
                try {
                    bitmap = fetchBitmap(context, url);
                    RenderScript rs = RenderScript.create(context);
                    bitmap = resizeBitmap(rs, bitmap, newwidth);
                }catch (Exception ex)
                {
                    Debug.e(TAG, ex);
                }

                return bitmap;
            }
        }).setCallbackResult(new PerformAsync2.CallbackResult() {
            @Override
            public void onResult(Object result) {
                if (result instanceof Bitmap) imageView.setImageBitmap((Bitmap) result);
            }
        }).setCallbackProgress(progress ->
        {

        });
    }


    static public void BitmapLoaderTask(Context context, String url, int widthInDp, ImageView imageView) {
        int newwidth = (int) Util.dp2Px(widthInDp);
        PerformAsync2.run(new PerformAsync2.Callback() {
            @Override
            public Object onBackground(PerformAsync2 performAsync) {

                Bitmap bitmap = null;
                try {
                    bitmap = ThumbnailUtils.createVideoThumbnail(url, MediaStore.Images.Thumbnails.FULL_SCREEN_KIND);

                    if (bitmap != null) {
                       /* RenderScript rs = RenderScript.create(context);
                        bitmap = resizeBitmap2(rs, bitmap, newwidth);*/
                        Bitmap.Config bitmapConfig = bitmap.getConfig();
                        int srcWidth = bitmap.getWidth();
                        int srcHeight = bitmap.getHeight();
                        float srcAspectRatio = (float) srcWidth / srcHeight;
                        int dstHeight = (int) (newwidth / srcAspectRatio);
                        bitmap = Bitmap.createScaledBitmap(bitmap, newwidth, dstHeight, false);

                        return bitmap;
                    }
                    return null;
                } catch (Exception e) {
                    if (e != null) {
                        e.printStackTrace();
                    }
                    return null;
                }
            }
        }).setCallbackResult(new PerformAsync2.CallbackResult() {
            @Override
            public void onResult(Object result) {
                if (result instanceof Bitmap) {
                    Bitmap bp = (Bitmap)result;
                    imageView.setImageBitmap(bp);
//                    bp.recycle();
                }

            }
        }).setCallbackProgress(progress ->
        {

        });
    }

  /*  static public void fetchBitmapFromVideo(Context context, String url, int widthInDp, ImageView imageView) {
        int newwidth = (int) Util.dp2Px(widthInDp);
        PerformAsync2.run(new PerformAsync2.Callback() {
            @Override
            public Object onBackground(PerformAsync2 performAsync) {

                Bitmap bitmap = null;
                try {
                    FFmpegMediaMetadataRetriever mmr = new FFmpegMediaMetadataRetriever();
                    mmr.setDataSource(url);
                    mmr.extractMetadata(FFmpegMediaMetadataRetriever.METADATA_KEY_ALBUM);
                    mmr.extractMetadata(FFmpegMediaMetadataRetriever.METADATA_KEY_ARTIST);
                    bitmap = mmr.getFrameAtTime(5000000, FFmpegMediaMetadataRetriever.OPTION_CLOSEST); // frame at 2 seconds
                    byte [] artwork = mmr.getEmbeddedPicture();
                    mmr.release();
                    RenderScript rs = RenderScript.create(context);
                    bitmap = resizeBitmap(rs, bitmap, newwidth);
                    return bitmap;
                } catch (Exception e) {
                    if (e != null) {
                        e.printStackTrace();
                    }
                    return null;
                }
            }
        }).setCallbackResult(new PerformAsync2.CallbackResult() {
            @Override
            public void onResult(Object result) {
                if (result instanceof Bitmap) {
                    Bitmap bp = (Bitmap)result;
                    imageView.setImageBitmap(bp);
//                    bp.recycle();
                }

            }
        }).setCallbackProgress(progress ->
        {

        });
    }*/

    public static Bitmap resizeBitmap2(RenderScript rs, Bitmap src, int dstWidth) {
        Bitmap dst = null;
        try {
            if (src != null) {
                Bitmap.Config bitmapConfig = src.getConfig();
                int srcWidth = src.getWidth();
                int srcHeight = src.getHeight();
                float srcAspectRatio = (float) srcWidth / srcHeight;
                int dstHeight = (int) (dstWidth / srcAspectRatio);

                float resizeRatio = (float) srcWidth / dstWidth;

                /* Calculate gaussian's radius */
                float sigma = resizeRatio / (float) Math.PI;
                // https://android.googlesource.com/platform/frameworks/rs/+/master/cpu_ref/rsCpuIntrinsicBlur.cpp
                float radius = 2.5f * sigma - 1.5f;
                radius = Math.min(25, Math.max(0.0001f, radius));
                dst = Bitmap.createScaledBitmap(src, dstWidth, dstHeight, false);
                src.recycle();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return dst;
    }

    /*private class BitmapLoaderTask extends AsyncTask<Void, Void, Bitmap> {
        private String mImageKey;

        public BitmapLoaderTask(String imageKey, GridViewAdapter adapter) {
            mAdapter = adapter;
            mImageKey = imageKey;
        }

        public BitmapLoaderTask(String imageKey, BucketGridAdapter adapter) {
            mBucketGridAdapter = adapter;
            mImageKey = imageKey;
        }

        @Override
        protected void onPreExecute() {
            mCurrentTasks.add(mImageKey);
        }

        @Override
        protected Bitmap doInBackground(Void... params) {
            Bitmap bitmap = null;
            try {
                bitmap = ThumbnailUtils.createVideoThumbnail(mImageKey, Thumbnails.FULL_SCREEN_KIND);

                if (bitmap != null) {
                    bitmap = Bitmap.createScaledBitmap(bitmap, mMaxWidth, mMaxWidth, false);
                    addBitmapToCache(mImageKey, bitmap);
                    return bitmap;
                }
                return null;
            } catch (Exception e) {
                if (e != null) {
                    e.printStackTrace();
                }
                return null;
            }
        }

        @Override
        protected void onPostExecute(Bitmap param) {
            mCurrentTasks.remove(mImageKey);
            if (param != null) {
                if(mAdapter != null){
                    mAdapter.notifyDataSetChanged();
                }else{
                    mBucketGridAdapter.notifyDataSetChanged();
                }
            }
        }
    }

    public void clear() {
        mBitmapCache.evictAll();
    }*/
}

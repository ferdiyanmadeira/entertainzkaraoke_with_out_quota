package com.madeira.entertainz.library;

import android.content.Context;

import com.google.gson.Gson;
import com.madeira.entertainz.karaoke.DBLocal.TPlaylist;
import com.madeira.entertainz.karaoke.DBLocal.TSongPlaylist;
import com.madeira.entertainz.karaoke.DBLocal.TSongYoutubePlaylist;
import com.madeira.entertainz.karaoke.DBLocal.TYoutubePlaylist;
import com.madeira.entertainz.karaoke.Debug;
import com.madeira.entertainz.karaoke.config.C;
import com.madeira.entertainz.karaoke.config.Global;
import com.madeira.entertainz.karaoke.model.ModelPlaylist;
import com.madeira.entertainz.karaoke.model.ModelYoutubePlaylist;

import java.util.List;

public class UtilWriteJSONFile {

    /** untuk write playlist ke json file (di fd) */
    public  static  void writePlaylistToFile(Context context, List<TPlaylist> tPlaylistList) {
        String TAG = "writePlaylistToFile";
        try {
            Gson gson = new Gson();
            ModelPlaylist.ResultPlaylistList resultPlaylistList = new ModelPlaylist.ResultPlaylistList();
            resultPlaylistList.list = tPlaylistList;
            String tPlayListString = gson.toJson(resultPlaylistList);

            String pathSTB = Global.getUSBPath() + "/" + C.PATH_JSON_KARAOKE;

            //write playlist
            String path = pathSTB + "/" + C.JSON_PLAYLIST;
            String tempPath = context.getFilesDir() + "/" + C.JSON_PLAYLIST;
            boolean result = Util.writeJsonToFile(tempPath, tPlayListString);
            if (result)
                RootUtil.moveFile(tempPath, path);

        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }

    }

    /** untuk write song playlist ke json file (di fd) */
    public  static  void writeSongPlaylistToFile(Context context, List<TSongPlaylist> tSongPlaylistList) {
        String TAG = "writePlaylistToFile";
        try {
            Gson gson = new Gson();
            ModelPlaylist.ResultSongPlaylistList resultSongPlaylistList = new ModelPlaylist.ResultSongPlaylistList();
            resultSongPlaylistList.list= tSongPlaylistList;
            String tSongPlayListString = gson.toJson(resultSongPlaylistList);

            String pathSTB = Global.getUSBPath() + "/" + C.PATH_JSON_KARAOKE;

            //write playlist
            String path = pathSTB + "/" + C.JSON_SONG_PLAYLIST;
            String tempPath = context.getFilesDir() + "/" + C.JSON_SONG_PLAYLIST;
            boolean result = Util.writeJsonToFile(tempPath, tSongPlayListString);
            if (result)
                RootUtil.moveFile(tempPath, path);

        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }

    }


    /** untuk write youtube playlist ke json file (di fd) */
    public  static  void writeYoutubePlaylistToFile(Context context, List<TYoutubePlaylist> tYoutubePlaylistList) {
        String TAG = "writePlaylistToFile";
        try {
            Gson gson = new Gson();
            ModelYoutubePlaylist.ResultYoutubePlaylistList resultYoutubePlaylistList = new ModelYoutubePlaylist.ResultYoutubePlaylistList();
            resultYoutubePlaylistList.list = tYoutubePlaylistList;
            String tPlayListString = gson.toJson(resultYoutubePlaylistList);

            String pathSTB = Global.getUSBPath() + "/" + C.PATH_JSON_KARAOKE;

            //write playlist
            String path = pathSTB + "/" + C.JSON_YOUTUBE_PLAYLIST;
            String tempPath = context.getFilesDir() + "/" + C.JSON_YOUTUBE_PLAYLIST;
            boolean result = Util.writeJsonToFile(tempPath, tPlayListString);
            if (result)
                RootUtil.moveFile(tempPath, path);

        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }

    }

    /** untuk write song playlist ke json file (di fd) */
    public  static  void writeSongYoutubePlaylistToFile(Context context, List<TSongYoutubePlaylist> tSongPlaylistList) {
        String TAG = "writePlaylistToFile";
        try {
            Gson gson = new Gson();
            ModelYoutubePlaylist.ResultSongYoutubePlaylistList resultSongYoutubePlaylistList = new ModelYoutubePlaylist.ResultSongYoutubePlaylistList();
            resultSongYoutubePlaylistList.list = tSongPlaylistList;
            String tSongPlayListString = gson.toJson(resultSongYoutubePlaylistList);

            String pathSTB = Global.getUSBPath() + "/" + C.PATH_JSON_KARAOKE;

            //write playlist
            String path = pathSTB + "/" + C.JSON_SONG_YOUTUBE_PLAYLIST;
            String tempPath = context.getFilesDir() + "/" + C.JSON_SONG_YOUTUBE_PLAYLIST;
            boolean result = Util.writeJsonToFile(tempPath, tSongPlayListString);
            if (result)
                RootUtil.moveFile(tempPath, path);

        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }

    }
}

package com.madeira.entertainz.library;

import android.util.Log;
import android.view.KeyEvent;

public class KeyGuard {
    private static final String TAG = "Keypad";

    StringBuilder sb = new StringBuilder();
    String pin;
    int maxLengthForErase;

    public KeyGuard(String pin){
        setPin(pin);
    }


    public boolean processKey(int keyCode){

        switch (keyCode){
            case KeyEvent.KEYCODE_0:
                sb.append('0');
                break;
            case KeyEvent.KEYCODE_1:
                sb.append('1');
                break;
            case KeyEvent.KEYCODE_2:
                sb.append('2');
                break;
            case KeyEvent.KEYCODE_3:
                sb.append('3');
                break;
            case KeyEvent.KEYCODE_4:
                sb.append('4');
                break;
            case KeyEvent.KEYCODE_5:
                sb.append('5');
                break;
            case KeyEvent.KEYCODE_6:
                sb.append('6');
                break;
            case KeyEvent.KEYCODE_7:
                sb.append('7');
                break;
            case KeyEvent.KEYCODE_8:
                sb.append('8');
                break;
            case KeyEvent.KEYCODE_9:
                sb.append('9');
                break;
        }


        if (sb.length()>maxLengthForErase) sb.setLength(0);

        return checkPin(sb.toString());
    }

    public void setPin(String pin){
        this.pin = pin;
        maxLengthForErase = pin.length() * 10;
    }

    boolean checkPin(String entry){

        //check length
        if (entry.length()< pin.length()) return false;

        //ambil stirng dari belakang, sebanyak pin
        String strCut = entry.substring(entry.length()- pin.length());

        //reset string builder
        sb.setLength(0);

        Log.i(TAG, "strCut=" + strCut);

        return strCut.equals(pin);
    }

}

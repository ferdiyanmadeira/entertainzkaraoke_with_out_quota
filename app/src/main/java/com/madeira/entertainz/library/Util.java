package com.madeira.entertainz.library;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageInstaller;
import android.content.pm.PackageManager;
import android.content.res.AssetManager;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaMetadataRetriever;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Environment;
import android.os.StatFs;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.OkHttpResponseAndJSONObjectRequestListener;
import com.androidnetworking.interfaces.OkHttpResponseListener;
import com.google.gson.GsonBuilder;
import com.madeira.entertainz.karaoke.BuildConfig;
import com.madeira.entertainz.karaoke.Debug;
import com.madeira.entertainz.karaoke.MainApplication;
import com.madeira.entertainz.karaoke.config.C;
import com.madeira.entertainz.karaoke.config.Global;
import com.madeira.entertainz.karaoke.DBLocal.TLocalFile;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.RandomAccessFile;
import java.io.Writer;
import java.net.HttpURLConnection;
import java.net.InetAddress;
import java.net.MalformedURLException;
import java.net.NetworkInterface;
import java.net.URL;
import java.net.URLConnection;
import java.net.UnknownHostException;
import java.nio.charset.StandardCharsets;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import javax.crypto.Cipher;
import javax.crypto.CipherInputStream;
import javax.crypto.CipherOutputStream;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESKeySpec;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import okhttp3.Response;


/**
 * Created by Erick on 3/16/2017.
 */

public class Util {
    private final static String TAG = "Util";

    static boolean isConnect = false;

    public static void appendLog(String text) {
        String path = getPathExternal() + "/log.file";
        File logFile = new File(path);
        if (!logFile.exists()) {
            try {
                logFile.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        try {
            //BufferedWriter for performance, true to set append to file flag
            BufferedWriter buf = new BufferedWriter(new FileWriter(logFile, true));
            buf.append(text);
            buf.newLine();
            buf.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static String getPathExternal() {
        return Environment.getExternalStorageDirectory().getAbsolutePath();
    }

    public static boolean isEmulator() {
        Log.i(TAG, "Build.PRODUCT=" + Build.PRODUCT);
        return Build.PRODUCT.contains("sdk");
    }

    public static float dp2Px(int dp) {
        return (float) dp * Resources.getSystem().getDisplayMetrics().density;
    }

    public static float dp2Px(float dp, Context context) {
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        float px = dp * ((float) metrics.densityDpi / DisplayMetrics.DENSITY_DEFAULT);
        return px;
    }


    public static String Sha256(String input) {
        MessageDigest md = null;
        byte[] digest = null;

        try {
            md = MessageDigest.getInstance("SHA-256");
            md.update(input.getBytes());
            digest = md.digest();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        return Base64.encodeToString(digest, Base64.DEFAULT).trim();
    }

    public static String stringToSHA256(String param) {
        String hashString = null;
        MessageDigest digest = null;
        try {
            digest = MessageDigest.getInstance("SHA-256");
            byte[] hash = digest.digest(param.getBytes(StandardCharsets.UTF_8));
            StringBuffer hexString = new StringBuffer();
            for (int i = 0; i < hash.length; i++) {
                String hex = Integer.toHexString(0xff & hash[i]);
                if (hex.length() == 1) hexString.append('0');
                hexString.append(hex);
            }
            hashString = hexString.toString();

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        return hashString;
    }


    public static String genHash(String string1, String string2) {

        MessageDigest md;

        byte[] byteSig = string1.getBytes();
        byte[] byteSalt = string2.getBytes();
        byte[] result;
//        byte[] encodedBytes = null;

        boolean sigLonger;

        //Create buffer for longest string
        if (byteSig.length >= byteSalt.length) {
            sigLonger = true;
            result = new byte[byteSig.length];
        } else {
            sigLonger = false;
            result = new byte[byteSalt.length];
        }

        byte b1, b2, r;

        for (int i = 0; i < result.length; i++) {
            if (sigLonger) {
                b1 = byteSig[i];
                b2 = byteSalt[i % byteSalt.length];
            } else {
                b1 = byteSig[i % byteSig.length];
                b2 = byteSalt[i];
            }
            r = (byte) (b1 ^ b2);
            result[i] = r;
        }

        String hash = null;

        try {
            md = MessageDigest.getInstance("SHA-256");
            md.update(result);
            hash = Base64.encodeToString(md.digest(), Base64.DEFAULT).trim();

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        System.out.println("Hash " + string1 + " + " + string2 + " => '" + hash + "'");

        return hash;
    }


    static public String getJSONFromTextFile(InputStream is) throws IOException {

        JSONObject jsonObject = new JSONObject();
        String str = "";
        int sizeOfJSONFile = is.available();

        //array that will store all the data
        byte[] bytes = new byte[sizeOfJSONFile];

        //reading data into the array from the file
        is.read(bytes);

        //close the input stream
        is.close();

        String JSONString = new String(bytes, "UTF-8");
        try {
            if (!JSONString.equals(""))
                jsonObject = new JSONObject(JSONString);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return JSONString;
    }

    public static String genSignature(String anyString, String saltBase64) {

        if (saltBase64 == null) return null;

        byte[] byteSig = anyString.getBytes();
        byte[] byteSalt = Base64.decode(saltBase64, Base64.DEFAULT);
        byte[] result;

        boolean sigLonger;

        //Create buffer for longest string
        if (byteSig.length >= byteSalt.length) {
            sigLonger = true;
            result = new byte[byteSig.length];
        } else {
            sigLonger = false;
            result = new byte[byteSalt.length];
        }

        byte b1, b2, r;

        for (int i = 0; i < result.length; i++) {
            if (sigLonger) {
                b1 = byteSig[i];
                b2 = byteSalt[i % byteSalt.length];
            } else {
                b1 = byteSig[i % byteSig.length];
                b2 = byteSalt[i];
            }
            r = (byte) (b1 ^ b2);
            result[i] = r;
        }

        return Base64.encodeToString(result, Base64.DEFAULT).trim();
    }

   /* static public String getPackageName() {
        return "com.madeira.ott2";
    }*/

    static String appPath = null;

    static public String getAppPath(Context context) {
        if (appPath == null) {
            File f;
            String packageName = "/Android/data/" + context.getPackageName();
            String state = Environment.getExternalStorageState();

            //external storage not availabel
            if (Environment.MEDIA_MOUNTED.equals(state) == false) {
                f = context.getFilesDir();
                appPath = f.getAbsolutePath() + packageName;
                f = new File(appPath);
                f.mkdirs();
                Log.i(TAG, "Internal storage " + appPath);
            } else {
                f = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_MOVIES);
//                f = Environment.getExternalStorageDirectory();
                appPath = f.getAbsolutePath() + packageName;
                f = new File(appPath);
                f.mkdirs();
                Log.i(TAG, "External storage " + appPath);
            }
        }
        return appPath;
    }

    public static String randomNumber(int len) {
        final String AB = "0123456789";
        SecureRandom rnd = new SecureRandom();

        StringBuilder sb = new StringBuilder(len);
        for (int i = 0; i < len; i++)
            sb.append(AB.charAt(rnd.nextInt(AB.length())));
        return sb.toString();
    }

    public static String randomString(int len) {
        final String AB = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
        SecureRandom rnd = new SecureRandom();

        StringBuilder sb = new StringBuilder(len);
        for (int i = 0; i < len; i++)
            sb.append(AB.charAt(rnd.nextInt(AB.length())));
        return sb.toString();
    }

    /***
     * This function will take datetime+tz and converted to local timezone
     *
     * @param datetime string of datetime in format yyyy-MM-dd HH:mm:ssZ
     * @return converted datetime with local timezone
     * <p>
     * eg: 2017-01-13 02:52:17+0000 --> Fri Jan 13 09:52:17 GMT+07:00 2017
     */
    public static Date convertTime(String datetime) {
        Calendar cal = Calendar.getInstance();
        TimeZone tz = cal.getTimeZone();
        Date date = null;

//        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ssZ");
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        sdf.setTimeZone(tz);
        try {
            date = sdf.parse(datetime);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return date;
    }

    /**
     * Convert bentuk date native ke format string + timezone
     */
    static SimpleDateFormat sdf = null;

    public static String convertTime(Date dt) {
//        if (sdf == null) {
        sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ssZ");
//        }
        return sdf.format(dt);
    }

    static SimpleDateFormat sqliteDateFormat = null;

    public static String sqliteDate(Date dt) {
//        if (sqliteDateFormat == null) {
        sqliteDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//        }
        return sqliteDateFormat.format(dt);
    }

    public static Date sqliteStr2Date(String str) {
//        if (sqliteDateFormat == null) {
        sqliteDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//        }
        try {
            return sqliteDateFormat.parse(str);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static Double bytesToMegabytes(long prmBytes) {
        Double result = 0.0;
//        int divide=1000000;
        int divide = 1000000;
        result = Double.valueOf(prmBytes / divide);

        return result;
    }

    public static String sqliteDate2Time(Date dt) {
        sqliteDateFormat = new SimpleDateFormat("HH:mm");
        return sqliteDateFormat.format(dt);
    }

    /**
     * @return 0 = no internet, 1 = MOBILE, 2 = WIFI
     */
    static public int getInternetType(Context context) {

        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        @SuppressLint("MissingPermission") NetworkInfo[] netInfo = cm.getAllNetworkInfo();
        for (NetworkInfo ni : netInfo) {
            if (ni.getTypeName().equalsIgnoreCase("WIFI"))
                if (ni.isConnected())
                    return 2;
            if (ni.getTypeName().equalsIgnoreCase("MOBILE"))
                if (ni.isConnected()) return 1;
        }

        return 0;
    }


    static public boolean isConnectedNetwork(Context context) {
        boolean result = false;
        try {
            ConnectivityManager mgr = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo netInfo = mgr.getActiveNetworkInfo();
            if (netInfo != null) {
                if (netInfo.isConnected()) {
                    // Internet Available
                    result = true;
                } else {
                    //No internet
                    result = false;
                }
            } else {
                //No internet
                result = false;
            }
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
        return result;
    }

    static public int isConnectedInternet() {

        int result = 0;
        try {

            Process p = Runtime.getRuntime().exec("su");
            DataOutputStream dos = new DataOutputStream(p.getOutputStream());
            dos.writeBytes("ping -c 1 " + C.ONLY_DOMAIN + "\n");

            dos.flush();
            dos.close();
            p.waitFor();
            BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));
            StringBuffer output = new StringBuffer();
            StringBuffer errorSB = new StringBuffer();

            BufferedReader stdError = new BufferedReader(new
                    InputStreamReader(p.getErrorStream()));

            String line = "";
            String resultString = "";
            while ((line = reader.readLine()) != null) {
                output.append(line + "; ");
            }

            while ((line = stdError.readLine()) != null) {
                errorSB.append(line + "; ");
            }
            if (errorSB.toString().equals(""))
                result = 1;
            else
                result = 0;

            resultString = output.toString() + errorSB.toString();

            Log.d(TAG, resultString);

        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
            Log.d(TAG, e.getMessage());

        }
        return result;
    }

    static public String readText(String urlString) {
        byte[] all = null;

        try {
            URLConnection conn = new URL(urlString).openConnection();
            conn.setConnectTimeout(20000);
            conn.setReadTimeout(20000);

            int bufferLength;
            byte[] buffer = new byte[8192];

            BufferedInputStream bins = new BufferedInputStream(conn.getInputStream());

            all = new byte[0];
            while ((bufferLength = bins.read(buffer)) > 0) {
                all = join(all, buffer, bufferLength);
            }

            bins.close();

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        if (all == null) return null;
        return new String(all);
    }

    private static byte[] join(byte[] all, byte[] buffer, int bufferLength) {
        int idx = all.length;
        byte[] newAll = Arrays.copyOf(all, all.length + bufferLength); //copy and make array bigger
        System.arraycopy(buffer, 0, newAll, idx, bufferLength);
        return newAll;
    }


    static public void varDump(Object o) {
        String str = new GsonBuilder().setPrettyPrinting().create().toJson(o);
        System.out.print(str);
    }

    static public void setFragment(FragmentActivity activity, Fragment fragment, int viewId) {
        FragmentManager fragmentManager = activity.getSupportFragmentManager();
        FragmentTransaction fragmentTransaction =
                fragmentManager.beginTransaction();
        fragmentTransaction.replace(viewId, fragment);
        fragmentTransaction.commit();
    }

    static public void removeFragment(FragmentActivity activity, Fragment fragment) {
        if (fragment != null)
            activity.getSupportFragmentManager().beginTransaction().remove(fragment).commit();
    }

    public static String getmacAdress(Context context) {
        WifiManager manager = (WifiManager) context.getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        WifiInfo info = manager.getConnectionInfo();
        String address = info.getMacAddress();
        return address;
    }

    static public void hideKeyboard(Activity activity) {
        InputMethodManager inputManager = (InputMethodManager) activity.
                getSystemService(Context.INPUT_METHOD_SERVICE);
        inputManager.hideSoftInputFromWindow(
                activity.getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);

    }


    static public ArrayList<String> convertInputStreamToArrayList(InputStream is) {
        ArrayList<String> result = new ArrayList<String>();

        InputStreamReader isr = new InputStreamReader(is,
                StandardCharsets.UTF_8);
        BufferedReader reader = new BufferedReader(isr);
        String line = null;
        try {
            while ((line = reader.readLine()) != null) {
                result.add(line);
            }

        } catch (IOException e) {
            Log.d(TAG, e.getMessage());
        } finally {
            try {
                reader.close();
                return result;
            } catch (IOException e) {
                Log.d(TAG, e.getMessage());
            }
        }

        return result;
    }

    static public InputStream convertArrayListToInputStream(ArrayList<String> arrayList) {
        ByteArrayInputStream result = null;
        try {
            StringBuilder sb = new StringBuilder();
            for (String s : arrayList) {
                sb.append(s);
            }
            result = new ByteArrayInputStream(sb.toString().getBytes("UTF-8"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }

    static public ArrayList<TLocalFile> getUSBFile(String fileType) {
        ArrayList<TLocalFile> tLocalFileArrayList = new ArrayList<TLocalFile>();
        int i = 1;
        List<StorageUtils.StorageInfo> storageInfoList = StorageUtils.getStorageList();
        try {
            for (StorageUtils.StorageInfo storageInfo : storageInfoList) {
//                if (storageInfo.removable) {
//                    String usbPath = storageInfo.path;
                String usbPath = "";
                if (storageInfo.removable) {
                    String[] split = storageInfo.path.split("/");
                    usbPath = "/storage/" + split[split.length - 1] + "/data/mr";
                } else
//                    usbPath = storageInfo.path+"/data/mr/";
                    usbPath = "/data/mr";

                Global.setUSBPath(usbPath + "/EKB");
                File directory = new File(usbPath);
                if (directory.exists()) {
                    File[] listFirstTreeDir = directory.listFiles();
                    for (File firstTree : listFirstTreeDir) {
                        if (firstTree.isDirectory()) {
                            if (firstTree.getName().toLowerCase().equals("ekb")) {
                                File ekbDir = new File(firstTree.getPath());
                                if (ekbDir.exists()) {
                                    File[] listSubEKB = ekbDir.listFiles();
                                    for (File secondTree : listSubEKB) {
                                        if (fileType.toLowerCase().equals("json")) {
                                            if (secondTree.getName().toLowerCase().equals("json")) {
                                                File jsonDir = new File(secondTree.getPath());
                                                if (jsonDir.exists()) {
                                                    File[] jSONDirList = jsonDir.listFiles();
                                                    for (File childJSONDir : jSONDirList) {
                                                        File[] child2JSONDirList = childJSONDir.listFiles();
                                                        for (File child2JSONDir : child2JSONDirList) {
                                                            if (child2JSONDir.isFile()) {
                                                                TLocalFile tLocalFile = new TLocalFile();
                                                                tLocalFile.id = i;
                                                                tLocalFile.fileType = "json";
                                                                tLocalFile.fileName = child2JSONDir.getName();
                                                                tLocalFile.contentType = childJSONDir.getName().toLowerCase();
                                                                tLocalFile.content = child2JSONDir.getAbsolutePath();
                                                                tLocalFileArrayList.add(tLocalFile);
                                                                i++;
                                                            }
                                                        }

                                                    }
                                                }
                                            }
                                        } else {

                                            File threeTreeDir = new File(secondTree.getPath());
                                            if (threeTreeDir.exists()) {
                                                File[] threeTreeDirList = threeTreeDir.listFiles();
                                                for (File childThreeTreeDir : threeTreeDirList) {
                                                    File[] child2ThreeTreeDirList = childThreeTreeDir.listFiles();
                                                    for (File child2ThreeTreeDir : child2ThreeTreeDirList) {
                                                        if (child2ThreeTreeDir.isFile()) {
                                                            TLocalFile tLocalFile = new TLocalFile();
                                                            tLocalFile.id = i;
                                                            tLocalFile.fileType = secondTree.getName().toLowerCase();
                                                            tLocalFile.fileName = child2ThreeTreeDir.getName();
                                                            tLocalFile.contentType = childThreeTreeDir.getName().toLowerCase();
                                                            tLocalFile.content = child2ThreeTreeDir.getAbsolutePath();
                                                            tLocalFileArrayList.add(tLocalFile);
                                                            i++;
                                                        }
                                                        if (child2ThreeTreeDir.isDirectory()) {
                                                            File[] directorySplitVideo = child2ThreeTreeDir.listFiles();
                                                            if (directorySplitVideo != null)

                                                                for (File directoryVideo : directorySplitVideo) {
                                                                    if (directoryVideo.isFile()) {
                                                                        TLocalFile tLocalFile = new TLocalFile();
                                                                        tLocalFile.id = i;
                                                                        tLocalFile.fileType = secondTree.getName().toLowerCase();
                                                                        tLocalFile.fileName = directoryVideo.getName();
                                                                        tLocalFile.contentType = childThreeTreeDir.getName().toLowerCase();
                                                                        tLocalFile.content = directoryVideo.getAbsolutePath();
                                                                        tLocalFileArrayList.add(tLocalFile);
                                                                        i++;
                                                                    }
                                                                }
                                                        }

                                                    }

                                                }
                                            }
                                        }

                                    }
                                }
                            }
                        }
                    }
                }
            }

//            }
        } catch (Exception ex) {
            Log.d(TAG, ex.getMessage());
        }
        return tLocalFileArrayList;
    }

    static SimpleDateFormat sdfFull = new SimpleDateFormat("EEEE d, MMMM yyyy HH:mm:ss");

    static public String dateFormatFull(Date date) {
        return sdfFull.format(date);
    }

    static private SimpleDateFormat sdfConvertMySqlDate;

    static public Date convertMySqlDate(String mysqlDate) {
        if (sdfConvertMySqlDate == null) {
            sdfConvertMySqlDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        }
        try {
            return sdfConvertMySqlDate.parse(mysqlDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return null;
    }

    public static String getCurrency(int currency) {
        String str = NumberFormat.getNumberInstance(Locale.US).format(currency);
        String newstr = str.replace(",", ".");
        return newstr;
    }

    public static int dpToPx(int dp, Context context) {
        float density = context.getResources()
                .getDisplayMetrics()
                .density;
        return Math.round((float) dp * density);
    }

    static public String sdFormatDate(Date date) {
        SimpleDateFormat sdfFull = new SimpleDateFormat("E, d MMMM yyyy HH:mm:ss");
        return sdfFull.format(date);
    }

    static public String convertMilisToTime(int millis) {
        String result = String.format("%d:%d",
                TimeUnit.MILLISECONDS.toMinutes(millis),
                TimeUnit.MILLISECONDS.toSeconds(millis) -
                        TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis))
        );

        return result;
    }

    static public String convertMilisToTime2(int millis) {
        long minutes = TimeUnit.MILLISECONDS.toMinutes(millis);
        long second = TimeUnit.MILLISECONDS.toSeconds(millis) -
                TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis));
        String result = (minutes < 10 ? "0" : "") + minutes + ":" + (second < 10 ? "0" : "") + second;

        return result;
    }

    static public String convertMilisToTimeWithText(int millis) {
        String result = String.format("%d min %d sec",
                TimeUnit.MILLISECONDS.toMinutes(millis),
                TimeUnit.MILLISECONDS.toSeconds(millis) -
                        TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis))
        );

        return result;
    }

    static public String formatMoney(Double money) {
        NumberFormat formatter = NumberFormat.getCurrencyInstance();
        DecimalFormatSymbols decimalFormatSymbols = ((DecimalFormat) formatter).getDecimalFormatSymbols();
        decimalFormatSymbols.setCurrencySymbol("");
        ((DecimalFormat) formatter).setDecimalFormatSymbols(decimalFormatSymbols);
        String moneyString = formatter.format(money);
        return moneyString;
    }

    static public void hideNavigationBar(Activity activity) {
        View decorView = activity.getWindow().getDecorView();
        decorView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_FULLSCREEN
                | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY | 8);
    }

    public static Bitmap retriveVideoFrameFromVideo(String videoPath)
            throws Throwable {
        Bitmap bitmap = null;
        MediaMetadataRetriever mediaMetadataRetriever = null;
        try {
            mediaMetadataRetriever = new MediaMetadataRetriever();
            if (Build.VERSION.SDK_INT >= 14)
                mediaMetadataRetriever.setDataSource(videoPath, new HashMap<String, String>());
            else
                mediaMetadataRetriever.setDataSource(videoPath);

            bitmap = mediaMetadataRetriever.getFrameAtTime(1, MediaMetadataRetriever.OPTION_CLOSEST_SYNC);
        } catch (Exception e) {
            e.printStackTrace();
            throw new Throwable(
                    "Exception in retriveVideoFrameFromVideo(String videoPath)"
                            + e.getMessage());

        } finally {
            if (mediaMetadataRetriever != null) {
                mediaMetadataRetriever.release();
            }
        }
        return bitmap;
    }

    public static boolean decrypt(String filePath) throws IOException, NoSuchAlgorithmException,
            NoSuchPaddingException, InvalidKeyException {

        boolean result = false;
        File file = new File(filePath);
        byte[] byteArray = C.ENCYPTION_KEY.getBytes(StandardCharsets.US_ASCII);

        FileInputStream fis = new FileInputStream(file.getAbsolutePath());
//        FileOutputStream fos = new FileOutputStream(path);
        DESKeySpec dks = new DESKeySpec(byteArray);
        SecretKeyFactory skf = SecretKeyFactory.getInstance("DES");
        try {
            SecretKey desKey = skf.generateSecret(dks);
            Cipher cipher = Cipher.getInstance("DES"); // DES/ECB/PKCS5Padding for SunJCE
            cipher.init(Cipher.DECRYPT_MODE, desKey);
            CipherInputStream cis = new CipherInputStream(fis, cipher);

            cis.close();
            result = true;
        } catch (InvalidKeySpecException e) {
            e.printStackTrace();
        }

        return result;
    }


    public static ArrayList<String> getAPKInfo(Context context, String pathAPK) {
        ArrayList<String> result = new ArrayList<>();
        try {
            PackageManager pm = context.getPackageManager();
            PackageInfo info = pm.getPackageArchiveInfo(pathAPK, 0);
            result.add(info.packageName);
            result.add(String.valueOf(info.versionCode));
            result.add(info.versionName);

        } catch (Exception ex) {

        }
        return result;
    }

    public static String installAPK(String pathAPK, String packageName) {
        String result = "";
        try {
            Process p = Runtime.getRuntime().exec("su");
            DataOutputStream dos = new DataOutputStream(p.getOutputStream());

            String shell1 = "pm install -r " + pathAPK + "\n";
            String shell2 = "am start -n " + packageName + "/com.madeira.entertainz.karaoke.SplashScreenActivity\n";
            dos.writeBytes(shell1);
            dos.writeBytes(shell2);

            dos.flush();
            dos.close();
            p.waitFor();
            BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));
            StringBuffer output = new StringBuffer();
            StringBuffer errorSB = new StringBuffer();

            BufferedReader stdError = new BufferedReader(new
                    InputStreamReader(p.getErrorStream()));

            String line = "";
            while ((line = reader.readLine()) != null) {
                output.append(line + "; ");
            }

            while ((line = stdError.readLine()) != null) {
                errorSB.append(line + "; ");
            }

            result = output.toString() + errorSB.toString();

            Log.d(TAG, result);


        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
            Log.d(TAG, e.getMessage());

        }

        return result;
    }


    public static String launchApp(String packageName) {
        String result = "";
        try {
            Process p = Runtime.getRuntime().exec("su");
            DataOutputStream dos = new DataOutputStream(p.getOutputStream());

            String shell2 = "am start -n " + packageName + "/com.madeira.entertainz.karaoke.SplashScreenActivity\n";
            dos.writeBytes(shell2);

            dos.flush();
            dos.close();
            p.waitFor();
            BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));
            StringBuffer output = new StringBuffer();
            StringBuffer errorSB = new StringBuffer();

            BufferedReader stdError = new BufferedReader(new
                    InputStreamReader(p.getErrorStream()));

            String line = "";
            while ((line = reader.readLine()) != null) {
                output.append(line + "; ");
            }

            while ((line = stdError.readLine()) != null) {
                errorSB.append(line + "; ");
            }

            result = output.toString() + errorSB.toString();

            Log.d(TAG, result);


        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
            Log.d(TAG, e.getMessage());

        }

        return result;
    }

    static public ArrayList<TLocalFile> getAssetFile(String fileType, Context context) {
        ArrayList<TLocalFile> tLocalFileArrayList = new ArrayList<TLocalFile>();
        int i = 1;
        String usbPath = "";


        Global.setUSBPath(usbPath);
        File directory = new File(usbPath);
        if (directory.exists()) {
            File[] listFirstTreeDir = directory.listFiles();
            for (File firstTree : listFirstTreeDir) {
                if (firstTree.isDirectory()) {
                    if (firstTree.getName().toLowerCase().equals("ekb")) {
                        File ekbDir = new File(firstTree.getPath());
                        if (ekbDir.exists()) {
                            File[] listSubEKB = ekbDir.listFiles();
                            for (File secondTree : listSubEKB) {
                                if (fileType.toLowerCase().equals("json")) {
                                    if (secondTree.getName().toLowerCase().equals("json")) {
                                        File jsonDir = new File(secondTree.getPath());
                                        if (jsonDir.exists()) {
                                            File[] jSONDirList = jsonDir.listFiles();
                                            for (File childJSONDir : jSONDirList) {
                                                File[] child2JSONDirList = childJSONDir.listFiles();
                                                for (File child2JSONDir : child2JSONDirList) {
                                                    if (child2JSONDir.isFile()) {
                                                        TLocalFile tLocalFile = new TLocalFile();
                                                        tLocalFile.id = i;
                                                        tLocalFile.fileType = "json";
                                                        tLocalFile.fileName = child2JSONDir.getName();
                                                        tLocalFile.contentType = childJSONDir.getName().toLowerCase();
                                                        tLocalFile.content = child2JSONDir.getAbsolutePath();
                                                        tLocalFileArrayList.add(tLocalFile);
                                                        i++;
                                                    }
                                                }

                                            }
                                        }
                                    }
                                } else {

                                    File threeTreeDir = new File(secondTree.getPath());
                                    if (threeTreeDir.exists()) {
                                        File[] threeTreeDirList = threeTreeDir.listFiles();
                                        for (File childThreeTreeDir : threeTreeDirList) {
                                            File[] child2ThreeTreeDirList = childThreeTreeDir.listFiles();
                                            for (File child2ThreeTreeDir : child2ThreeTreeDirList) {
                                                if (child2ThreeTreeDir.isFile()) {
                                                    TLocalFile tLocalFile = new TLocalFile();
                                                    tLocalFile.id = i;
                                                    tLocalFile.fileType = secondTree.getName().toLowerCase();
                                                    tLocalFile.fileName = child2ThreeTreeDir.getName();
                                                    tLocalFile.contentType = childThreeTreeDir.getName().toLowerCase();
                                                    tLocalFile.content = child2ThreeTreeDir.getAbsolutePath();
                                                    tLocalFileArrayList.add(tLocalFile);
                                                    i++;
                                                }
                                                if (child2ThreeTreeDir.isDirectory()) {
                                                    File[] directorySplitVideo = child2ThreeTreeDir.listFiles();
                                                    if (directorySplitVideo != null)

                                                        for (File directoryVideo : directorySplitVideo) {
                                                            if (directoryVideo.isFile()) {
                                                                TLocalFile tLocalFile = new TLocalFile();
                                                                tLocalFile.id = i;
                                                                tLocalFile.fileType = secondTree.getName().toLowerCase();
                                                                tLocalFile.fileName = directoryVideo.getName();
                                                                tLocalFile.contentType = childThreeTreeDir.getName().toLowerCase();
                                                                tLocalFile.content = directoryVideo.getAbsolutePath();
                                                                tLocalFileArrayList.add(tLocalFile);
                                                                i++;
                                                            }
                                                        }
                                                }

                                            }

                                        }
                                    }
                                }

                            }
//                                    break;

                        }
                    }
                }
            }
        }

//            }
        return tLocalFileArrayList;
    }

    public static String extractYoutubeId(String url) throws MalformedURLException {
        String query = new URL(url).getQuery();
        String[] param = query.split("&");
        String id = null;
        for (String row : param) {
            String[] param1 = row.split("=");
            if (param1[0].equals("v")) {
                id = param1[1];
            }
        }
        return id;
    }

    public static void setLanguage(Context context, String languageCode) {
        try {
            Resources res = context.getResources();
            // Change locale settings in the app.
            DisplayMetrics dm = res.getDisplayMetrics();
            android.content.res.Configuration conf = res.getConfiguration();
            conf.setLocale(new Locale(languageCode.toLowerCase())); // API 17+ only.
            // Use conf.locale = new Locale(...) if targeting lower versions
            res.updateConfiguration(conf, dm);
        } catch (Exception ex) {

        }
    }

    public static String bytesToHex(byte[] bytes) {
        StringBuilder sbuf = new StringBuilder();
        for (int idx = 0; idx < bytes.length; idx++) {
            int intVal = bytes[idx] & 0xff;
            if (intVal < 0x10) sbuf.append("0");
            sbuf.append(Integer.toHexString(intVal).toUpperCase());
        }
        return sbuf.toString();
    }

    /**
     * Get utf8 byte array.
     *
     * @param str which to be converted
     * @return array of NULL if error was found
     */
    public static byte[] getUTF8Bytes(String str) {
        try {
            return str.getBytes("UTF-8");
        } catch (Exception ex) {
            return null;
        }
    }

    /**
     * Load UTF8withBOM or any ansi text file.
     *
     * @param filename which to be converted to string
     * @return String value of File
     * @throws java.io.IOException if error occurs
     */
    public static String loadFileAsString(String filename) throws java.io.IOException {
        final int BUFLEN = 1024;
        BufferedInputStream is = new BufferedInputStream(new FileInputStream(filename), BUFLEN);
        try {
            ByteArrayOutputStream baos = new ByteArrayOutputStream(BUFLEN);
            byte[] bytes = new byte[BUFLEN];
            boolean isUTF8 = false;
            int read, count = 0;
            while ((read = is.read(bytes)) != -1) {
                if (count == 0 && bytes[0] == (byte) 0xEF && bytes[1] == (byte) 0xBB && bytes[2] == (byte) 0xBF) {
                    isUTF8 = true;
                    baos.write(bytes, 3, read - 3); // drop UTF8 bom marker
                } else {
                    baos.write(bytes, 0, read);
                }
                count += read;
            }
            return isUTF8 ? new String(baos.toByteArray(), "UTF-8") : new String(baos.toByteArray());
        } finally {
            try {
                is.close();
            } catch (Exception ignored) {
            }
        }
    }

    /**
     * Returns MAC address of the given interface name.
     *
     * @param interfaceName eth0, wlan0 or NULL=use first interface
     * @return mac address or empty string
     */
    public static String getMACAddress(String interfaceName) {
        try {
            List<NetworkInterface> interfaces = Collections.list(NetworkInterface.getNetworkInterfaces());
            for (NetworkInterface intf : interfaces) {
                if (interfaceName != null) {
                    if (!intf.getName().equalsIgnoreCase(interfaceName)) continue;
                }
                byte[] mac = intf.getHardwareAddress();
                if (mac == null) return "";
                StringBuilder buf = new StringBuilder();
                for (byte aMac : mac) buf.append(String.format("%02X:", aMac));
                if (buf.length() > 0) buf.deleteCharAt(buf.length() - 1);
                return buf.toString();
            }
        } catch (Exception ignored) {
        } // for now eat exceptions
        return "";
        /*try {
            // this is so Linux hack
            return loadFileAsString("/sys/class/net/" +interfaceName + "/address").toUpperCase().trim();
        } catch (IOException ex) {
            return null;
        }*/
    }

    /**
     * Get IP address from first non-localhost interface
     *
     * @param useIPv4 true=return ipv4, false=return ipv6
     * @return address or empty string
     */
    public static String getIPAddress(boolean useIPv4) {
        try {
            List<NetworkInterface> interfaces = Collections.list(NetworkInterface.getNetworkInterfaces());
            for (NetworkInterface intf : interfaces) {
                List<InetAddress> addrs = Collections.list(intf.getInetAddresses());
                for (InetAddress addr : addrs) {
                    if (!addr.isLoopbackAddress()) {
                        String sAddr = addr.getHostAddress();
                        //boolean isIPv4 = InetAddressUtils.isIPv4Address(sAddr);
                        boolean isIPv4 = sAddr.indexOf(':') < 0;

                        if (useIPv4) {
                            if (isIPv4)
                                return sAddr;
                        } else {
                            if (!isIPv4) {
                                int delim = sAddr.indexOf('%'); // drop ip6 zone suffix
                                return delim < 0 ? sAddr.toUpperCase() : sAddr.substring(0, delim).toUpperCase();
                            }
                        }
                    }
                }
            }
        } catch (Exception ignored) {
        } // for now eat exceptions
        return "";
    }

    public static String encryptFile(String path, String password) {
        String prmPath = path.concat(".crypt");
        String extPassword = "1234567891234567";
        try {
            if (password.length() > 16)
                password = password.substring(0, 15);
            else {
                password = password + extPassword;
                password = password.substring(0, 15);
            }

            // Here you read the cleartext.
            FileInputStream fis = new FileInputStream(path);
            // This stream write the encrypted text. This stream will be wrapped by another stream.
            FileOutputStream fos = new FileOutputStream(prmPath);

            byte[] keyByte = new byte[16];
            keyByte = password.getBytes("UTF-16");
            // Length is 16 byte
            // Careful when taking user input!!! https://stackoverflow.com/a/3452620/1188357
            SecretKeySpec sks = new SecretKeySpec(keyByte, "AES");
            // Create cipher
            Cipher cipher = Cipher.getInstance("AES");
            cipher.init(Cipher.ENCRYPT_MODE, sks);
            // Wrap the output stream
            CipherOutputStream cos = new CipherOutputStream(fos, cipher);
            // Write bytes
            int b;
            byte[] d = new byte[8];
            while ((b = fis.read(d)) != -1) {
                cos.write(d, 0, b);
            }
            // Flush and close streams.
            cos.flush();
            cos.close();
            fis.close();

            //untuk menghapus file yang belum di encrypt
            File sourceFile = new File(path);
            sourceFile.delete();
         /*   //untuk rename file
            File destFile = new File(prmPath);
            destFile.renameTo(sourceFile);*/
        } catch (IOException e) {
            e.printStackTrace();
            prmPath = null;
        } catch (NoSuchPaddingException e) {
            e.printStackTrace();
            prmPath = null;
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            prmPath = null;
        } catch (InvalidKeyException e) {
            e.printStackTrace();
            prmPath = null;

        }
        return prmPath;
    }

    public static boolean decrypt(String path, String password, String outPath) {
        boolean result = false;
        try {
            String extPassword = "1234567891234567";
            if (password.length() > 16)
                password = password.substring(0, 15);
            else {
                password = password + extPassword;
                password = password.substring(0, 15);
            }
            FileInputStream fis = new FileInputStream(path);
            FileOutputStream fos = new FileOutputStream(outPath);
            byte[] keyByte = new byte[16];
            keyByte = password.getBytes("UTF-16");
            SecretKeySpec sks = new SecretKeySpec(keyByte, "AES");
            Cipher cipher = Cipher.getInstance("AES");
            cipher.init(Cipher.DECRYPT_MODE, sks);
            CipherInputStream cis = new CipherInputStream(fis, cipher);
            int b;
            byte[] d = new byte[8];
            while ((b = cis.read(d)) != -1) {
                fos.write(d, 0, b);
            }
            fos.flush();
            fos.close();
            cis.close();
            result = true;
            /*FileInputStream fis = new FileInputStream(path);
            FileOutputStream fos = new FileOutputStream(outPath);
            byte[] key = (password).getBytes("UTF-8");
            MessageDigest sha = MessageDigest.getInstance("SHA-1");
            key = sha.digest(key);
            key = Arrays.copyOf(key, 16);
            SecretKeySpec sks = new SecretKeySpec(key, "AES");
            Cipher cipher = Cipher.getInstance("AES");
            cipher.init(Cipher.DECRYPT_MODE, sks);
            CipherInputStream cis = new CipherInputStream(fis, cipher);
            int b;
            byte[] d = new byte[8];
            while ((b = cis.read(d)) != -1) {
                fos.write(d, 0, b);
            }
            fos.flush();
            fos.close();
            cis.close();*/
        } catch (IOException e) {
            e.printStackTrace();
        } catch (NoSuchPaddingException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        }
        return result;
    }

    public static boolean writeJsonToFile(String outputPath, String jsonString) {
        boolean result = false;
        Writer output = null;
        File file = new File(outputPath);
        if (file.exists())
            file.delete();
        try {
            output = new BufferedWriter(new FileWriter(file));
            output.write(jsonString);
            output.close();
            result = true;
        } catch (IOException e) {
            e.printStackTrace();
        }

        return result;
    }

    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public static String thousandFormat(long prm) {
        String result = "";

        try {
//            result = String.format("%,d", prm);
            DecimalFormat formatter = (DecimalFormat) NumberFormat.getInstance(Locale.US);
            formatter.applyPattern("#,###");
            return formatter.format(prm);
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }

        return result;
    }

    public static String readTextFile(String filename) {

        StringBuilder sb = new StringBuilder();
        File file = new File(filename);

        try {
            BufferedReader br = new BufferedReader(new FileReader(file));

            String st;
            while ((st = br.readLine()) != null) sb.append(st);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return sb.toString();
    }

    public static boolean deleteFile(String filaname) {
        File f = new File(filaname);
        if (f.exists()) return f.delete();
        return true;
    }

    public static void showKeyboard(Context context) {
        try {
            InputMethodManager inputMethodManager = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
            inputMethodManager.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    public static void closeKeyboard(Context context) {
        try {
            InputMethodManager inputMethodManager = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
            inputMethodManager.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    public static void freeMemory() {
        try {
            System.runFinalization();
            Runtime.getRuntime().gc();
            System.gc();
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    public static String getColoredSpanned(String text, String color) {
        String input = "<font color=" + color + ">" + text + "</font>";
        return input;
    }

    public static String base64ToFile(String base64Str, String destinationPath) {
        String result = "";
        try {
            byte[] data = Base64.decode(base64Str, Base64.DEFAULT);
            FileOutputStream out = new FileOutputStream(destinationPath);
            out.write(data);
            out.flush();
            out.close();

            result = destinationPath;

//            byte[] decodedString = Base64.decode(data, Base64.DEFAULT);
//            Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
//
//            try (FileOutputStream out = new FileOutputStream(destinationPath)) {
//                decodedByte.compress(Bitmap.CompressFormat.PNG, 100, out); // bmp is your Bitmap instance
//                // PNG is a lossless format, the compression factor (100) is ignored
//                result = destinationPath;
//            } catch (IOException e) {
//                e.printStackTrace();
//            }


        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
        return result;
    }

    /**
     * jgn di pakai utk file yg besar
     *
     * @param filename
     * @return
     */
    public static String fileToBase64(String filename) {
        String result = "";
        try {
            InputStream inputStream = new FileInputStream(filename);

            //baca seluru file masuk ke memory, ini tdk boleh utk file besar
            byte[] data = new byte[inputStream.available()];
            int read = inputStream.read(data);

            return Base64.encodeToString(data, Base64.DEFAULT);

//            Bitmap bm = BitmapFactory.decodeFile(path);
//            ByteArrayOutputStream baos = new ByteArrayOutputStream();
//            bm.compress(Bitmap.CompressFormat.PNG, 100, baos); // bm is the bitmap object
//            byte[] b = baos.toByteArray();
//            result = Base64.encodeToString(b, Base64.DEFAULT);
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }

        return result;
    }

    public static ResultStorage getUSBInfoSize() {
        ResultStorage resultStorage = new ResultStorage();
        try {
            String usbPath = "";

            List<StorageUtils.StorageInfo> storageInfoList = StorageUtils.getStorageList();
            for (StorageUtils.StorageInfo storageInfo : storageInfoList) {
                if (storageInfo.removable) {
                    String[] split = storageInfo.path.split("/");
                    usbPath = "/storage/" + split[split.length - 1];
                }
            }
            StatFs statFs = new StatFs(usbPath);
            long blockSize = statFs.getBlockSize();
            long totalSize = statFs.getBlockCount() * blockSize;
            long availableSize = statFs.getAvailableBlocks() * blockSize;
            long usedSize = totalSize - availableSize;
            long megTotal = totalSize / (1024 * 1024);
            long megAvailable = availableSize / (1024 * 1024);
            long megUsed = usedSize / (1024 * 1024);
            String availableStr = Util.thousandFormat(megAvailable) + " Mb";
            String usedStr = Util.thousandFormat(megUsed) + " Mb";
            String totalStr = Util.thousandFormat(megTotal) + " Mb";
            resultStorage.freeSpace = megAvailable;
            resultStorage.freeSpaceStr = availableStr;
            resultStorage.totalSpace = megTotal;
            resultStorage.totalSpaceStr = totalStr;
            resultStorage.usedSpace = megUsed;
            resultStorage.usedSpaceStr = usedStr;
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }

        return resultStorage;
    }

    public static class ResultStorage {
        public String freeSpaceStr;
        public String totalSpaceStr;
        public String usedSpaceStr;
        public long freeSpace;
        public long totalSpace;
        public long usedSpace;
    }
}





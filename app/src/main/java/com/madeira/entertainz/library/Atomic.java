package com.madeira.entertainz.library;

import java.util.concurrent.atomic.AtomicReference;

public class Atomic<T> {

    //ref dari value si simpan disini
    private AtomicReference<T> value;

    public Atomic(T initialValue){
        value = new AtomicReference<>(initialValue);
    }

    public T get() {
        return value.get();
    }

    public void set(T value) {

        //loop lagi, apabila gagal rubbah nilai
        while(true) {
            T existingValue = this.value.get();
            if(this.value.compareAndSet(existingValue, value)) break;
        }
    }
}

package com.madeira.entertainz.library;

/**
 * Created by erick on 10/7/16.
 */

import android.os.AsyncTask;

/**
 * Created by MR on 7/10/2016.
 */
public class PerformAsync extends AsyncTask<Object, Integer, PerformAsync.Result> {
    final String TAG = "PerformAsync";

    public interface IPerformAsync{
        Object onPerformAsyncBackground(Object reference);
        void onPerformAsyncResult(Object reference, Object result);
    }

    //Class to wrap result and error
    class Result{
        Object reference;
        Object result;
    }

    private Object _reference;
    private IPerformAsync _callback;

    public static PerformAsync run(Object reference, IPerformAsync callback) {
        PerformAsync req = new PerformAsync();
        req._callback = callback;
        req._reference = reference;
        req.execute(reference);
        return req;
    }

    /***
     * Perform http Request in background
     * @param arObj
     * @return
     */
    protected Result doInBackground(Object... arObj) {
        Result r = new Result();

        r.reference = arObj[0];
        r.result = _callback.onPerformAsyncBackground(r.reference);

        return r;
    }

//    protected void onProgressUpdate(Integer... progress) {
//        Log.i(TAG, "progress " + progress[0] + " / " + progress[1]);
//    }

    protected void onPostExecute(Result result) {
        _callback.onPerformAsyncResult(_reference, result.result);
    }

}

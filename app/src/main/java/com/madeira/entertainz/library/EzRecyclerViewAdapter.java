package com.madeira.entertainz.library;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by Erick on 9/21/2018.
 *
 * Adapter ini dipergunakan hanya utk item2 yg memiliki id yg unique.
 * Apabila tdk ada harap mempergunakan RecyclerViewAdapter yg normal
 *
 * getItemId harus return id dari setiap item, bukan index
 *
 */

public class EzRecyclerViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static final String TAG = "EzRecyclerViewAdapter";

    public interface IEzRecyclerViewAdapter {
        RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType);
        void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position);
        int getItemCount();
        long getItemId(int pos);
    }

    public interface IOnViewType {
        int onGetViewType(int position);
    }

    private IEzRecyclerViewAdapter callback;
    private IOnViewType callbackViewType;

    public EzRecyclerViewAdapter(IEzRecyclerViewAdapter callback){
        this.callback = callback;

        setHasStableIds(true);
    }

    public void setOnViewType(IOnViewType callback){
        callbackViewType = callback;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return callback.onCreateViewHolder(parent, viewType);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        callback.onBindViewHolder(holder, position);
    }

    @Override
    public int getItemCount() {
        return callback.getItemCount();
    }

    @Override
    public int getItemViewType(int position) {
        if (callbackViewType==null) return super.getItemViewType(position);
        return callbackViewType.onGetViewType(position);
    }

    @Override
    public long getItemId(int position) {
        return callback.getItemId(position);
    }

    /**
     * Helper utk inflate
     *
     * @param context
     * @param resId
     * @param parent
     * @return
     */
    static public View inflate(Context context, int resId, ViewGroup parent){

        try {

            return LayoutInflater.from(context).inflate(resId, parent, false);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

}

package com.madeira.entertainz.library;

import android.app.Activity;
import android.app.admin.DevicePolicyManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.IntentFilter;
import android.util.Log;
import android.view.WindowManager;

import com.madeira.entertainz.karaoke.BuildConfig;
import com.madeira.entertainz.karaoke.config.C;

import java.io.BufferedReader;
import java.io.InputStreamReader;

import static android.content.Intent.ACTION_MAIN;
import static android.content.Intent.CATEGORY_DEFAULT;
import static android.content.Intent.CATEGORY_HOME;

/**
 * make device owner
 * ----------------
 * adb shell dpm set-device-owner com.madeiraresearch.hoteliptv2/.MyDeviceAdminReceiver
 * must have declared "android:testOnly" in the application manifest
 *
 * to remove device owner
 * ----------------------
 * adb shell dpm remove-active-admin  com.madeirareseatch.hoteliptv2/.MyDeviceAdminReceiver
 *
 */

public class UtilDeviceOwner {
    private static final String TAG = "UtilDeviceOwner";

    static public boolean getDeviceOwnerState(String packageName, Context context){

        DevicePolicyManager devicePolicyManager = (DevicePolicyManager) context.getSystemService(Context.DEVICE_POLICY_SERVICE);

        //check apakah sdh dalam kiosk mode?
        return devicePolicyManager.isDeviceOwnerApp(packageName);
    }

    static public boolean enableKioskMode(Activity activity, String packageName, Class classMainActivity, Class classDeviceAdmin){
        ComponentName componentName;
        DevicePolicyManager devicePolicyManager;

        componentName = getComponentName(activity, classDeviceAdmin);
        devicePolicyManager = (DevicePolicyManager) activity.getSystemService(Context.DEVICE_POLICY_SERVICE);

        //check apakah sdh dalam kiosk mode?
        if (devicePolicyManager.isDeviceOwnerApp(packageName)==false) {
//            Log.i(TAG, "NORMAL mode");
            return false;
        }

//        Log.i(TAG, "KIOSK mode");

//        String[]ar = { packageName, "com.madeira.entertainz.karaoke", "com.google.android.youtube.tv" };
        String[] ar = {BuildConfig.APPLICATION_ID, C.YOUTUBE_ID, C.APPLICATION_ID_FILEMANAGER, C.APPLICATION_ID_VIDIO};
        devicePolicyManager.setLockTaskPackages(componentName, ar );
        activity.startLockTask();

        //set our app as default app
        IntentFilter intentFilter = new IntentFilter(ACTION_MAIN);
        intentFilter.addCategory(CATEGORY_HOME);
        intentFilter.addCategory(CATEGORY_DEFAULT);
        devicePolicyManager.addPersistentPreferredActivity(componentName, intentFilter, getComponentName(activity, classMainActivity));

        //disable keyguarg, shg saat reboot otomatis masuk ke app
        devicePolicyManager.setKeyguardDisabled(componentName, true);

        //screen on always
        activity.getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        //never dimmed walaupun di charge
//        int opt = BatteryManager.BATTERY_PLUGGED_AC | BatteryManager.BATTERY_PLUGGED_USB | BatteryManager.BATTERY_PLUGGED_WIRELESS;
//            devicePolicyManager.setGlobalSetting(componentName, Settings.Global.STAY_ON_WHILE_PLUGGED_IN, String.valueOf(opt));

        return true;
    }

    /**
     * make this app device owner
     *
     * https://www.sisik.eu/blog/android/dev-admin/set-dev-owner
     *
     * @param packagename
     * @param classReceiver
     */
    public static void enableDeviceOwner(String packagename, Class classReceiver){
        su("dpm set-device-owner " + packagename + "/" + classReceiver.getName());
    }

    public static void removeDeviceOwner(String packageName, Class classReceiver){
        su("dpm remove-active-admin " + packageName + "/" + classReceiver.getName());
    }

    /**
     * Execute with ROOT privilage
     *
     * @param command
     */
    static public String su(String command) {
        try {
            Process proc = Runtime.getRuntime().exec(new String[] { "su", "-c", command });
            Log.i(TAG, "su -c " + command);
            proc.waitFor();

            BufferedReader reader = new BufferedReader(new InputStreamReader(proc.getInputStream()));
            StringBuffer output = new StringBuffer();
            StringBuffer errorSB = new StringBuffer();

            BufferedReader stdError = new BufferedReader(new
                    InputStreamReader(proc.getErrorStream()));

            String line = "";
            String result="";
            while ((line = reader.readLine()) != null) {
                output.append(line + "; ");
            }

            while ((line = stdError.readLine()) != null) {
                errorSB.append(line + "; ");
            }

            output.append(errorSB);

            String out = output.toString();

            Log.i(TAG, "out=" + out);

            return out;

        } catch (Exception e) {
            Log.e(TAG, "exec " + command + " FAIL " + e.getMessage());
            return null;
        }
    }
    public static ComponentName getComponentName(Context context, Class className){
        return new ComponentName(context.getApplicationContext(), className);
    }

}

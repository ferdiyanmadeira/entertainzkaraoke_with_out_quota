package com.madeira.entertainz.library;

import android.util.Log;

import java.util.List;

public class UtilArrayList {
    private static final String TAG = "UtilArrayList";

    static public <T> void moveUp(List<T> list, int currentPosition){

        //exit apabila posisi sdh paling atas
        if (currentPosition==0) return;

        T object = list.get(currentPosition);
        list.remove( currentPosition);

        list.add(currentPosition-1, object);
    }

    static public <T> void moveDown(List<T> list, int currentPosition){
        int lastIndex = list.size()-1;

        //exit apabila item sdh ada paling bawah
        if (currentPosition==lastIndex) return;

        T object = list.get(currentPosition);

        //add item di bawah dari item ke 2
        if (currentPosition+2>lastIndex){
            list.add( object);
        } else {
            list.add(currentPosition+2, object);
        }

        //remove item lama
        list.remove( currentPosition);
    }

}

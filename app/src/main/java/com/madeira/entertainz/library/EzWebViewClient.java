package com.madeira.entertainz.library;

import android.util.Log;
import android.view.KeyEvent;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public class EzWebViewClient extends WebViewClient {
    private static final String TAG = "EzWebViewClient";

    Callback callback;

    public interface Callback {
        boolean shouldOverrideKeyEvent(WebView view, KeyEvent event);
        boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request);
    }

    public EzWebViewClient(Callback callback){
        this.callback = callback;
    }

    @Override
    public boolean shouldOverrideKeyEvent(WebView view, KeyEvent event) {

        if (callback.shouldOverrideKeyEvent(view, event)) return true;

        return super.shouldOverrideKeyEvent(view, event);
    }

    @Override
    public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {

        if(callback.shouldOverrideUrlLoading(view, request)) return true;

        return super.shouldOverrideUrlLoading(view, request);
    }


}

package com.madeira.entertainz.library;

import java.lang.reflect.Array;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Set;

public class VarDump {
    private static final String TAG = "VarDump";

    private static final int MAX_DEPTH = 10;

    private static final String TYPE_SIMPLE = "SIMPLE";
    private static final String TYPE_CLASS = "java.lang.Class";
    private static final String TYPE_ARRAYLIST = "java.util.ArrayList";
    private static final String TYPE_HASHTABLE = "java.util.Hashtable";
    private static final String TYPE_UNKNOWN= "UNKNOWN";

    static public void dump(Object o){
        StackTraceElement[] stackTraceElement = Thread.currentThread().getStackTrace();
        System.out.print("VarDump -- START ("+stackTraceElement[3].getFileName()+":"+stackTraceElement[3].getLineNumber()+")\n");

        String out = dump_object(o, 0, null, null);
        System.out.print(out);
        System.out.print("VarDump -- END\n");
    }

    static String dump_object(Object o, int level, Object arrayIndex, Field field) {

        StringBuilder sb = new StringBuilder();
        String indent = "\t";

        for (int i = 0; i < level; i++) indent += '\t';

        if (level>MAX_DEPTH) return indent + "Max depth reached, qutting\n";

        if (o==null && field==null) return indent + "Object is null\n";

        Class c;
        String name;
        String nameLong;
        String displayName;
        String out = null;
        String fieldName = "";
        boolean isArray;

        String type = null;

        //apabila field, tdk null
        //maka process object dari field
        //
        if (field!=null){

            try {
                o = field.get(o);
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }

            //apabila object exist, maka hapus field,
            //sehingga object o di proses normal
            //
            if (o!=null) {
                fieldName = " " + field.getName();
                field = null;
            }
        }

        //Apabila field null, maka proses object
        //
        if (field==null){
            c = o.getClass();
            name = c.getSimpleName();
            nameLong = c.getName();
            displayName = name;

            isArray = c.isArray();
        }else {
            name = field.getType().getSimpleName();
            nameLong = field.getType().getName(); //nama jenis
            displayName = name;
            fieldName = " " + field.getName(); //nama field

            isArray = field.getClass().isArray();
        }

        //this will handle array
        if (isArray){
            sb.append(indent).append(name).append(fieldName).append(" = size(").append(Array.getLength(o)).append(")\n");
            sb.append(process_array(o, level+1));
            return sb.toString();
        }

        //apabila o==null, tdk perlu process
        if (o==null) {
            out = "null\n";
        }else{
            switch (nameLong){
                case "int":
                case "long":
                case "float":
                case "double":
                case "boolean":
                case "java.lang.String":
                case "java.util.Date":
                case "java.lang.Integer":
                case "java.lang.Long":
                case "java.lang.Boolean":
                case "java.lang.Float":
                case "java.lang.Double":
                    out = o.toString() + '\n';
                    break;
                case TYPE_ARRAYLIST:
                    out = process_array_list(o, level+1);
                    break;
                case TYPE_HASHTABLE:
                    out = process_hashtable(o, level+1);
                    break;
                case TYPE_CLASS:
                    out = process_class(o, level+1);
                    displayName = "class " + ((Class) o).getSimpleName();
                    break;
                default:
                    out = process_class(o, level+1);
                    break;
            }
        }

        sb.append(indent);

        if (arrayIndex!=null){
            sb.append("[").append(arrayIndex).append("] ");
        }

        sb.append(displayName).append(fieldName).append(" = ").append(out);

        return sb.toString();
    }

    static String process_array_list(Object o, int level){
        Class c = o.getClass();
        StringBuilder sb = new StringBuilder();

        if (o instanceof ArrayList){
            ArrayList<Object> arrayList = (ArrayList<Object>) o;
            sb.append("size(").append(arrayList.size()).append(")\n");

            for (int idx=0; idx<arrayList.size(); idx++) {
                Object item = arrayList.get(idx);
                sb.append(dump_object(item, level, idx, null));
            }

            return sb.toString();
        }

        return "process_array_list = " + c.getName();
    }

    static String process_hashtable(Object o, int level){
        Class c = o.getClass();
        StringBuilder sb = new StringBuilder();

        if (o instanceof Hashtable){

            Hashtable<Object, Object> hashtable = (Hashtable<Object, Object>) o;
            sb.append("size(").append(hashtable.size()).append(")\n");

            Set set = hashtable.keySet();
            Iterator it = set.iterator();

            while (it.hasNext()){
                Object key = it.next();
                Object value = hashtable.get(key);
                sb.append(dump_object(value, level, key, null));
            }

            return sb.toString();
        }

        return "process_hashtable = " + c.getName();
    }

    /**
     * Utk process type static class dan unknown
     *
     * @param o
     * @param level
     * @return
     */
    static String process_class(Object o, int level){
        Class c;
        StringBuilder sb = new StringBuilder();

        if (o instanceof Class){
            c = (Class) o;
            o = null; //utk type class, o tdk di butuhkan
        } else {
            c = o.getClass();

            sb.append(o.toString()).append('\n');
        }

        //process inner class class
        Class[]arClass = c.getClasses();
        if (arClass!=null && arClass.length>0){
            sb.append('\n');//.append(indent).append("class[").append(arClass.length).append("]\n");
            for (Class cls: arClass) {
                sb.append(dump_object(cls, level, null, null)).append('\n');
            }
        }

        //process field
        Field[]arField = c.getDeclaredFields();
        if (arField!=null && arField.length>0){
            sb.append('\n');//.append(indent).append("field[").append(arField.length).append("]\n");
            try{
                for (Field field: arField) {
                    field.setAccessible(true);

                    if (o==null) {
                        //static class
                        //hanya proses static field saja
                        if (!isFieldStatic(field)) continue;
                        sb.append(dump_object(field.get(null), level, null, field));
                    } else {
                        //instantiate class
                        sb.append(dump_object(o, level, null, field));
                    }
                }

            }catch (Exception e){e.printStackTrace();}
        }

        return sb.toString();
    }

    static String process_array(Object o, int level){
        StringBuilder sb = new StringBuilder();
        int idx = 0;

        String name = o.getClass().getSimpleName();

        switch (name){
            case "byte[]":
                byte []arByte = (byte[]) o;
                sb.append("byte[").append(arByte.length).append("]");
                break;
            case "char[]":
                String str = new String((char[]) o);
                sb.append("char[]");
                break;
            case "int[]":
                int []arInt = (int[]) o;
                for (int value: arInt) {
                    sb.append(dump_object(value, level, idx++, null));
                }
                break;
            default:
                Object[]arObj = (Object[]) o;
                for (Object value: arObj) {
                    sb.append(dump_object(value, level, idx++, null));
                }
                break;
        }

        return sb.toString();
    }

    static boolean isFieldStatic(Field f){
        return java.lang.reflect.Modifier.isStatic(f.getModifiers());
    }
}

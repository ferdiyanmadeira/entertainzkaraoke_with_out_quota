package com.madeira.entertainz.library;

import android.content.Context;
import android.content.SharedPreferences;

import com.madeira.entertainz.karaoke.BuildConfig;


public class HelperSharedPreferences {

    private final static String NAME = BuildConfig.APPLICATION_ID;

    public static Boolean readBoolean(Context context, String name, Boolean def) {
        SharedPreferences sharedPref = context.getSharedPreferences(NAME, Context.MODE_PRIVATE);
        return sharedPref.getBoolean(name, def);
    }

    public static String readString(Context context, String name, String def) {
        SharedPreferences sharedPref = context.getSharedPreferences(NAME, Context.MODE_PRIVATE);
        return sharedPref.getString(name, def);
    }

    public static int readInt(Context context, String name, int def) {
        SharedPreferences sharedPref = context.getSharedPreferences(NAME, Context.MODE_PRIVATE);
        return sharedPref.getInt(name, def);
    }


    public static long readLong(Context context, String name, long def) {
        SharedPreferences sharedPref = context.getSharedPreferences(NAME, Context.MODE_PRIVATE);
        return sharedPref.getLong(name, def);
    }
    public static void writeBoolean(Context context, String name, Boolean value) {
        SharedPreferences sharedPref = context.getSharedPreferences(NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putBoolean(name, value);
        editor.apply();
    }

    public static void writeString(Context context, String name, String value) {
        SharedPreferences sharedPref = context.getSharedPreferences(NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(name, value);
        editor.apply();
    }

    public static void writeInt(Context context, String name, int value) {
        SharedPreferences sharedPref = context.getSharedPreferences(NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putInt(name, value);
        editor.apply();
    }

    public static void writeLong(Context context, String name, long value) {
        SharedPreferences sharedPref = context.getSharedPreferences(NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putLong(name, value);
        editor.apply();
    }
    /**
     * Clear data from Shared Preferences where user logout from application
     */
    public static void clearPref(Context context) {
        SharedPreferences sharedPref = context.getSharedPreferences(NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.clear();
        editor.commit();
    }

    /*delete data from shared preference by key*/
    public static void deletePrefByKey(Context context, String key)
    {
        SharedPreferences sharedPref =context.getSharedPreferences(NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.remove(key);
        editor.apply();
    }
}

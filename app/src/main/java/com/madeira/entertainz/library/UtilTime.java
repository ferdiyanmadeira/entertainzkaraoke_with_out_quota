package com.madeira.entertainz.library;

import android.app.AlarmManager;
import android.content.Context;


import com.madeira.entertainz.karaoke.Debug;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.SimpleTimeZone;
import java.util.TimeZone;

public class UtilTime {

    //Set timezone
    //array timezone https://en.wikipedia.org/wiki/List_of_tz_database_time_zones
    public static void setTimezone(Context context, String tz){
        try {
            AlarmManager am = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
            am.setTimeZone(tz);
        }catch (Exception e){
            Debug.e("setTimezone", e);
        }
    }


    public static List<String> getAvailableTimeZone(){
        List<String> result = new ArrayList<>();
        try {

            for(String item : TimeZone.getAvailableIDs())
            {
                if(item.contains("Asia"))
                {
                    result.add(item);
                }
            }
//            result = TimeZone.getAvailableIDs();
        }catch (Exception e){
            Debug.e("setTimezone", e);
        }

        return  result;
    }

    public static void setDateTime(Context context, String dateTime){
        try {
            String[] dateTimeArray =dateTime.split(" ");
            String date = dateTimeArray[0];
            String time = dateTimeArray[1];
            String[] dateArray = date.split("-");
            String[] timeArray = time.split(":");
            int year = Integer.valueOf(dateArray[0]);
            int month = Integer.valueOf(dateArray[1]);
            int dd = Integer.valueOf(dateArray[2]);

            int hour = Integer.valueOf(timeArray[0]);
            int minute = Integer.valueOf(timeArray[1]);
            int second = Integer.valueOf(timeArray[2]);

            Calendar c = Calendar.getInstance();
            c.set(year, month, dd, hour, minute, second);
            AlarmManager am = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
            am.setTime(c.getTimeInMillis());
        }catch (Exception e){}
    }

    static private SimpleDateFormat sdfConvertMySqlDate;
    static public Date convertMySqlDate(String mysqlDate){
        if (mysqlDate==null) return new Date(0);

        if (sdfConvertMySqlDate==null){
            sdfConvertMySqlDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        }
        try {
            return sdfConvertMySqlDate.parse(mysqlDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return null;
    }

    /***
     * This function will take datetime+tz and converted to local timezone
     *
     * @param datetime string of datetime in format yyyy-MM-dd HH:mm:ssZ
     * @return converted datetime with local timezone
     * <p>
     * eg: 2017-01-13 02:52:17+0000 --> Fri Jan 13 09:52:17 GMT+07:00 2017
     */
    public static Date convertTime(String datetime) {
        Calendar cal = Calendar.getInstance();
        TimeZone tz = cal.getTimeZone();
        Date date = null;

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ssZ");
        sdf.setTimeZone(tz);
        try {
            date = sdf.parse(datetime);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return date;
    }

}

package com.madeira.entertainz.library;

import android.os.AsyncTask;

public class PerformAsync2 extends AsyncTask<Object, Integer, Object> {
    final String TAG = "PerformAsync2";

    public interface Callback{
        Object onBackground(PerformAsync2 performAsync);
    }
    public interface CallbackResult{
        void onResult(Object result);
    }
    public interface CallbackProgress{
        void onProgress(Integer... progress);
    }

    private Callback mCallback;
    private CallbackResult mCallbackResult;
    private CallbackProgress mCallbackProgress;

    public static PerformAsync2 run(Callback callback) {
        PerformAsync2 req = new PerformAsync2();
        req.mCallback = callback;
        req.execute();
        return req;
    }

    protected Object doInBackground(Object... arObj) {
        return mCallback.onBackground(this);
    }

    protected void onProgressUpdate(Integer... progress) {
        if(mCallbackProgress!=null) mCallbackProgress.onProgress(progress);
    }

    protected void onPostExecute(Object result) {
        if (mCallbackResult!=null) mCallbackResult.onResult(result);
    }

    public void publishProgreess(Integer... values){
        publishProgress(values);
    }

    public PerformAsync2 setCallbackResult(CallbackResult callback){
        mCallbackResult = callback;
        return this;
    }
    public PerformAsync2 setCallbackProgress(CallbackProgress callback){
        mCallbackProgress = callback;
        return this;
    }
}

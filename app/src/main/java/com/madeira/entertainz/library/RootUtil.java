package com.madeira.entertainz.library;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInstaller;
import android.util.Log;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Erick on 8/27/2018.
 */

public class RootUtil {
    private static final String TAG = "RootUtil";

    public static boolean isDeviceRooted() {
        return checkRootMethod1() || checkRootMethod2() || checkRootMethod3();
    }

    private static boolean checkRootMethod1() {
        Log.i(TAG, "checkRootMethod1");
        String buildTags = android.os.Build.TAGS;
        return buildTags != null && buildTags.contains("test-keys");
    }

    private static boolean checkRootMethod2() {
        Log.i(TAG, "checkRootMethod2");
        String[] paths = {"/system/app/Superuser.apk", "/sbin/su", "/system/bin/su", "/system/xbin/su", "/data/local/xbin/su", "/data/local/bin/su", "/system/sd/xbin/su",
                "/system/bin/failsafe/su", "/data/local/su", "/su/bin/su"};
        for (String path : paths) {
            if (new File(path).exists()) return true;
        }
        return false;
    }

    private static boolean checkRootMethod3() {
        Log.i(TAG, "checkRootMethod3");
        Process process = null;
        try {
            process = Runtime.getRuntime().exec(new String[]{"/system/xbin/which", "su"});
            BufferedReader in = new BufferedReader(new InputStreamReader(process.getInputStream()));
            if (in.readLine() != null) return true;
            return false;
        } catch (Throwable t) {
            return false;
        } finally {
            if (process != null) process.destroy();
        }
    }

    static public void installApk(String filename) {
        File file = new File(filename);
        if (file.exists()) {
            try {
                final String command = "pm install -r " + file.getAbsolutePath();
                Process proc = Runtime.getRuntime().exec(new String[]{"su", "-c", command});
                proc.waitFor();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Execute with ROOT privilage
     *
     * @param command
     */
    static public String su(String command) {
        try {
            String cmd[] = new String[]{"su", "-c", command};
            Process proc = Runtime.getRuntime().exec(cmd);
            Log.i(TAG, "su -c " + command);
            proc.waitFor();

            BufferedReader reader = new BufferedReader(new InputStreamReader(proc.getInputStream()));
            StringBuffer output = new StringBuffer();
            StringBuffer errorSB = new StringBuffer();

            BufferedReader stdError = new BufferedReader(new
                    InputStreamReader(proc.getErrorStream()));

            String line = "";
            String result = "";
            while ((line = reader.readLine()) != null) {
                output.append(line + "; ");
            }

            while ((line = stdError.readLine()) != null) {
                errorSB.append(line + "; ");
            }

            output.append(errorSB);

            String out = output.toString();

            Log.i(TAG, "out=" + out);

            return out;

        } catch (Exception e) {
            Log.e(TAG, "exec " + command + " FAIL " + e.getMessage());
            return null;
        }
    }


    static SimpleDateFormat sdfSetDate;

    //    "date 060910002018.00; am broadcast -a android.intent.action.TIME_SET"
    static public String setDate(Date dt) {

        if (sdfSetDate == null) sdfSetDate = new SimpleDateFormat("MMddHHmmyyyy.ss");

        String strDate = sdfSetDate.format(dt);

        Log.i(TAG, "setDate " + strDate);

        return su("date " + strDate + "; am broadcast -a android.intent.action.TIME_SET");

//        try {
//
//            Process p = Runtime.getRuntime().exec("su");
//            DataOutputStream dos = new DataOutputStream(p.getOutputStream());
//            dos.writeBytes("date 060910002018.00; am broadcast -a android.intent.action.TIME_SET\n");
//            dos.flush();
//            dos.close();
//            p.waitFor();
//            BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));
//            StringBuffer output = new StringBuffer();
//            StringBuffer errorSB = new StringBuffer();
//
//            BufferedReader stdError = new BufferedReader(new
//                    InputStreamReader(p.getErrorStream()));
//
//            String line = "";
//            String result="";
//            while ((line = reader.readLine()) != null) {
//                output.append(line + "; ");
//            }
//
//            while ((line = stdError.readLine()) != null) {
//                errorSB.append(line + "; ");
//            }
//
//            result = output.toString()+errorSB.toString();
//
//            Log.d(TAG,result);
//
//        } catch (Exception e) {
//            e.printStackTrace();
//            Log.d(TAG, e.getMessage());
//
//        }
    }

    /**
     * Required
     * 1. deviceowner
     * 2. new apk must set textOnly=true
     *
     * @param context
     * @param packageName
     * @param file
     */
    public static void install(Context context, String packageName, String file) {

        try {


            // PackageManager provides an instance of PackageInstaller
            PackageInstaller packageInstaller = context.getPackageManager().getPackageInstaller();

            // Prepare params for installing one APK file with MODE_FULL_INSTALL
            // We could use MODE_INHERIT_EXISTING to install multiple split APKs
            PackageInstaller.SessionParams params = new PackageInstaller.SessionParams(PackageInstaller.SessionParams.MODE_FULL_INSTALL);
            params.setAppPackageName(packageName);

            // Get a PackageInstaller.Session for performing the actual update
            int sessionId = packageInstaller.createSession(params);
            PackageInstaller.Session session = packageInstaller.openSession(sessionId);

            // Copy APK file bytes into OutputStream provided by install Session

            InputStream fis = new FileInputStream(new File(file));

            OutputStream out = session.openWrite(packageName, 0, -1);
            byte[] buffer = new byte[65536];
            int c;
            while ((c = fis.read(buffer)) != -1) {
                out.write(buffer, 0, c);
            }

            session.fsync(out);
            out.close();
            fis.close();

            // The app gets killed after installation session commit
            session.commit(PendingIntent.getBroadcast(context, sessionId,
                    new Intent(Intent.ACTION_MAIN), 0).getIntentSender());


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void rm(String filename) {
        RootUtil.su("rm " + filename);
    }

    public static void chmod(String permission, String filename) {
        RootUtil.su("chmod " + permission + " " + filename);
    }

    public static void mkdirs(String path) {
        RootUtil.su("mkdir -p " + path); //create folder outside app path, this must using rooted device
    }

    public static void setAutomaticDateTime(boolean on) {
        int value = 0;
        if (on) value = 1;
        RootUtil.su("settings put global auto_time " + value);
    }

    public static String moveFile(String srcPath, String destPath) {
        String cmd = "mv " + srcPath + " " + destPath;
        return RootUtil.su(cmd);
    }

    public static String copyFile(String srcPath, String destPath) {
        String cmd = "cp " + srcPath + " " + destPath;
        return RootUtil.su(cmd);
    }

    public static String deleteFile(String path) {
        String cmd = "rm " + path;
        return RootUtil.su(cmd);
    }

    public static String createDir(String path) {
        String cmd = "mkdir " + path;
        return RootUtil.su(cmd);
    }

}
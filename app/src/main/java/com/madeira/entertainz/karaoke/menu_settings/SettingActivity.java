package com.madeira.entertainz.karaoke.menu_settings;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.net.wifi.WifiManager;
import android.os.Handler;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.androidnetworking.error.ANError;
import com.kinda.alert.KAlertDialog;
import com.madeira.entertainz.karaoke.DBLocal.JoinSettingElementMedia;
import com.madeira.entertainz.karaoke.DBLocal.TSticker;
import com.madeira.entertainz.karaoke.Debug;
import com.madeira.entertainz.karaoke.R;
import com.madeira.entertainz.karaoke.SplashScreenActivity;
import com.madeira.entertainz.karaoke.config.C;
import com.madeira.entertainz.karaoke.config.Global;
import com.madeira.entertainz.karaoke.menu_main.BottomNavigationFragment;
import com.madeira.entertainz.karaoke.menu_main.ListMoodFragment;
import com.madeira.entertainz.karaoke.menu_navigation.TopNavigationFragment;
import com.madeira.entertainz.karaoke.model_online.ModelApp;
import com.madeira.entertainz.karaoke.model_room_db.ModelElementMedia;
import com.madeira.entertainz.library.Util;

import net.yslibrary.android.keyboardvisibilityevent.KeyboardVisibilityEvent;
import net.yslibrary.android.keyboardvisibilityevent.KeyboardVisibilityEventListener;

import java.io.File;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import es.dmoral.toasty.Toasty;

import static com.madeira.entertainz.karaoke.config.CacheData.lastIndexBackground;

public class SettingActivity extends AppCompatActivity {
    String TAG = "SettingActivity";
    String lastLanguageId = "";
    String lastTitleApp = "";
    ImageView backgroundIV;
    int intentFromOTT = 0;
    int lastDuration = 0;
    List<Integer> activeBackgroundId = new ArrayList<>();

    Handler handler = new Handler();
    Runnable runnable;
    TopNavigationFragment topNavigationFragment = new TopNavigationFragment();
    SettingCategoryFragment settingCategoryFragment = new SettingCategoryFragment();
    SettingBackgroundFragment settingBackgroundFragment = new SettingBackgroundFragment();
    SettingGeneralFragment settingGeneralFragment = new SettingGeneralFragment();
    SettingConnectionFragment settingConnectionFragment = new SettingConnectionFragment();
    SettingCheckUpdateFragment settingCheckUpdateFragment = new SettingCheckUpdateFragment();
    SettingStorageFragment settingStorageFragment = new SettingStorageFragment();
    SettingHelpFragment settingHelpFragment = new SettingHelpFragment();
    SettingMyQuotaFragment settingMyQuotaFragment = new SettingMyQuotaFragment();
    SettingBuyQuotaFragment settingBuyQuotaFragment = new SettingBuyQuotaFragment();
    BottomNavigationFragment bottomNavigationFragment = new BottomNavigationFragment();
    ListMoodFragment listMoodFragment = new ListMoodFragment();
    SettingDeleteSongFragment settingDeleteSongFragment = new SettingDeleteSongFragment();
    SettingServerFragment settingServerFragment = new SettingServerFragment();

    View listMoodView;

    static ISettingActivityListener iSettingActivityListener;
    static FragmentTransaction ft;
    ProgressBar progressBar;

    static ISettingActivity mListener;

    /**
     * untuk interface ke SettingDeleteSongFragment
     */
    public interface ISettingActivity {
        void onCancelDeleteSong();

        void onDeleteSong();
    }

    /**
     * untuk interface ke MainActivity
     */
    public interface ISettingActivityListener {
        // TODO: Update argument type and name
        void onRefreshTitleApp();

        void onChangeBackground();

        void onChangeLanguage();

    }


    public static void startActivity(Activity activity, int prmIntentFromOTT) {
        Intent intent = new Intent(activity, SettingActivity.class);
        intent.putExtra(SplashScreenActivity.KEY_INTENT_FROM_OTT, prmIntentFromOTT);
        activity.startActivity(intent);
//        activity.finish();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        try {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_setting);
            intentFromOTT = getIntent().getIntExtra(SplashScreenActivity.KEY_INTENT_FROM_OTT, 0);
            bind();
            bindFragment();
            fetchListBackground();
            //for trigger change title
            lastTitleApp = Global.getTitleApp();


            lastDuration = Global.getDuration();
            Util.hideNavigationBar(this);
            KeyboardVisibilityEvent.setEventListener(
                    this,
                    new KeyboardVisibilityEventListener() {
                        @Override
                        public void onVisibilityChanged(boolean isOpen) {
                            // some code depending on keyboard visiblity status
                            if (isOpen) {
                                Toasty.info(SettingActivity.this, getString(R.string.pressBackToHideKeyBoard)).show();
                            }

                        }
                    });

            String languageId = Global.getLanguageId();
            Util.setLanguage(this, languageId);
            lastLanguageId = languageId;
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }


    void bind() {
        try {
            backgroundIV = (ImageView) findViewById(R.id.backgroundIV);
            listMoodView = findViewById(R.id.listMoodFrameLayout);
            progressBar = findViewById(R.id.progressBar);
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    void bindFragment() {
//        topNavigationFragment = topNavigationFragment.newInstance(true);
        topNavigationFragment = topNavigationFragment.newInstance(C.MENU_SETTING);
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.topNavFrameLayout, topNavigationFragment);
        ft.replace(R.id.bottomFrameLayout, bottomNavigationFragment);
        ft.replace(R.id.listMoodFrameLayout, listMoodFragment);
        ft.replace(R.id.categoryFrameLayout, settingCategoryFragment);
        ft.commit();

        settingCategoryFragment.setCallback(iSettingCategory);
        bottomNavigationFragment.setCallback(bottomNavigationFragmentListener);
        listMoodFragment.setCallBack(listMoodListener);
    }

    //todo untuk fetch list image background
    void fetchListBackground() {
        ModelElementMedia.getListElementMediaActive(this, new ModelElementMedia.IGetListElementMediaActive() {
            @Override
            public void onGetListElementMedia(List<JoinSettingElementMedia> joinSettingElementMediaList) {
                int totalIndex = joinSettingElementMediaList.size();
                runnable = new Runnable() {
                    @Override
                    public void run() {
                        try {
                            if (joinSettingElementMediaList.size() != 0) {
                                if (lastIndexBackground >= totalIndex)
                                    lastIndexBackground = 0;

                                JoinSettingElementMedia joinSettingElementMedia = joinSettingElementMediaList.get(lastIndexBackground);
                                Uri uri = Uri.fromFile(new File(joinSettingElementMedia.urlImage));
//                            Picasso.with(KaraokeActivity.this).load(uri).into(backgroundIV);
                                InputStream inputStream = getContentResolver().openInputStream(uri);
                                BitmapFactory.Options options = new BitmapFactory.Options();
                                options.inPreferredConfig = Bitmap.Config.RGB_565;
                                Bitmap image = BitmapFactory.decodeStream(inputStream, null, options);
                                backgroundIV.setImageBitmap(image);

                                lastIndexBackground++;
                                handler.postDelayed(this::run, joinSettingElementMedia.duration);
                            }
                        } catch (Exception ex) {
                            Debug.e(TAG, ex);
                        }
                    }
                };
                handler.postDelayed(runnable, 100);
            }
        });
    }

    @Override
    public void onBackPressed() {
        //trigger for change title app
        //if digunakan untuk handle fragment delete song
        if (mListener == null) {
            super.onBackPressed();
            String newTitleApp = "";
            newTitleApp = Global.getTitleApp();

            if (!lastTitleApp.equals(newTitleApp)) {
                iSettingActivityListener.onRefreshTitleApp();
            }
            if (lastDuration != Global.getDuration()) {
                iSettingActivityListener.onChangeBackground();
            } else {
                checkBackgroundChange();
            }

            iSettingActivityListener.onChangeLanguage();

            //agar runable tidak running ketika di finish activity
            Util.hideNavigationBar(this);
            handler.removeCallbacks(runnable);
            handler.removeCallbacksAndMessages(null);
            finish();
        }
        else
        {
            mListener.onCancelDeleteSong();

        }
//        MainActivity.startActivity(SettingActivity.this, 0);
    }


    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        try {
            if (keyCode == KeyEvent.KEYCODE_DEL) {
                if (mListener != null) {
                    mListener.onDeleteSong();
                }
            }
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public void onResume() {
        super.onResume();
        Util.hideNavigationBar(this);
    }

    SettingCategoryFragment.ISettingCategory iSettingCategory = new SettingCategoryFragment.ISettingCategory() {
        @Override
        public void onCategoryClick(int categoryId, String category) {
            try {
                //todo harus commit satu persatu, karena ada fragment yang jika ingin diakses butuh check connection ke internet
                ft = getSupportFragmentManager().beginTransaction();
                switch (categoryId) {
                    case 1:
                        ft.replace(R.id.detailFrameLayout, settingGeneralFragment).addToBackStack(null);
                        ft.commit();
                        break;
                    case 2:
                        ft.replace(R.id.detailFrameLayout, settingBackgroundFragment).addToBackStack(null);
                        ft.commit();
                        break;
                    case 3:
                        ft.replace(R.id.detailFrameLayout, settingConnectionFragment).addToBackStack(null);
                        ft.commit();
                        break;
                    case 4:
                        ft.replace(R.id.detailFrameLayout, settingCheckUpdateFragment).addToBackStack(null);
                        ft.commit();
                        break;
                    case 5:
                        ft.replace(R.id.detailFrameLayout, settingStorageFragment).addToBackStack(null);
                        ft.commit();
                        break;
                  /*  case 6:
                        showMyQuota();
                        break;*/
                    case 7:
                        ft.replace(R.id.detailFrameLayout, settingServerFragment).addToBackStack(null);
                        ft.commit();
                        break;
                    case 8:
                        ft.replace(R.id.detailFrameLayout, settingDeleteSongFragment).addToBackStack(null);
                        ft.commit();
                        break;
                    case 9:
                        ft.replace(R.id.detailFrameLayout, settingHelpFragment).addToBackStack(null);
                        ft.commit();
                        break;
                }

            } catch (Exception ex) {
                Debug.e(TAG, ex);
            }
        }
    };


    public static void setCallback(ISettingActivityListener listener) {
        iSettingActivityListener = listener;
    }


    void checkBackgroundChange() {
        ModelElementMedia.getListElementMediaActive(SettingActivity.this, new ModelElementMedia.IGetListElementMediaActive() {
            @Override
            public void onGetListElementMedia(List<JoinSettingElementMedia> joinSettingElementMediaList) {
                for (JoinSettingElementMedia item : joinSettingElementMediaList) {
                    if (!activeBackgroundId.contains(item.elementMediaId)) {
                        iSettingActivityListener.onChangeBackground();
                        break;
                    }
                }
            }
        });
       /* try {
            PerformAsync2.run(performAsync ->
            {
                List<JoinSettingElementMedia> joinSettingElementMediaList = new ArrayList<>();
                try {
                    DbAccess dbAccess = DbAccess.Instance.create(SettingActivity.this);
                    joinSettingElementMediaList = dbAccess.daoAccessThemeMedia().joinSettingElementMediaActive();
                    dbAccess.close();
                } catch (Exception ex) {

                }
                return joinSettingElementMediaList;
            }).setCallbackResult(result ->
            {
                List<JoinSettingElementMedia> joinSettingElementMediaList = (List<JoinSettingElementMedia>) result;
                for (JoinSettingElementMedia item : joinSettingElementMediaList) {
                    if (!activeBackgroundId.contains(item.elementMediaId)) {
                        iSettingActivityListener.onChangeBackground();
                        break;
                    }
                }

            });
        } catch (Exception ex) {
            Log.d(TAG, ex.getMessage());
        }*/

    }

    //todo dibuat terpisah karena harus ada pengecekan connect internet atau tidak
    void showBuyQuota() {
        String TAG = this.TAG + "-showBuyQuota";
        progressBar.setVisibility(View.VISIBLE);
        ModelApp.checkInternet(new ModelApp.ICheckConnectToHost() {
            @Override
            public void onConnected(boolean connected) {
                progressBar.setVisibility(View.GONE);
                if (connected) {
                    ft.replace(R.id.detailFrameLayout, settingBuyQuotaFragment).addToBackStack(null);
                    ft.commit();
                } else {
                    showDialogToConnectWifi();

                }
            }

            @Override
            public void onError(ANError e) {
                progressBar.setVisibility(View.GONE);
                showDialogToConnectWifi();
            }
        });

    }

    //todo dibuat terpisah karena harus ada pengecekan connect internet atau tidak
    void showMyQuota() {
       /* progressBar.setVisibility(View.VISIBLE);
        PerformAsync2.run(performAsync -> {
            int checkInternet = Util.isConnectedInternet();
            return checkInternet;
        }).setCallbackResult(result ->
        {
            try {
                progressBar.setVisibility(View.GONE);
                int checkInternet = (int) result;
                if (checkInternet != 0) {
                    ft.replace(R.id.detailFrameLayout, settingMyQuotaFragment).addToBackStack(null);
                    ft.commit();
                } else {
                    showDialogToConnectWifi();
                }
            } catch (Exception ex) {
                Debug.e(TAG, ex);
            }
        });*/

        progressBar.setVisibility(View.VISIBLE);
        ModelApp.checkInternet(new ModelApp.ICheckConnectToHost() {
            @Override
            public void onConnected(boolean connected) {
                try {
                    progressBar.setVisibility(View.GONE);
                    if (connected) {
                        ft.replace(R.id.detailFrameLayout, settingMyQuotaFragment).addToBackStack(null);
                        ft.commit();
                    } else {
                        showDialogToConnectWifi();

                    }
                }catch (Exception ex)
                {
                    Debug.e(TAG, ex);
                }
            }

            @Override
            public void onError(ANError e) {
                try {
                    progressBar.setVisibility(View.GONE);
                    showDialogToConnectWifi();
                }catch (Exception ex)
                {
                    Debug.e(TAG, ex);
                }
            }
        });

    }

    //todo untuk memunculkan dialog connect ke wifi, karena device offline
    void showDialogToConnectWifi() {
        try {
            String titleApp = "";
            if (C.FOR_HOTEL)
                titleApp = getString(R.string.titleKaraoke);
            else
                titleApp = getString(R.string.titleKaraokeHeyoo);
            KAlertDialog pDialog = new KAlertDialog(SettingActivity.this, KAlertDialog.WARNING_TYPE);
            pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
            pDialog.setTitleText(getString(R.string.information));
            pDialog.setContentText(getString(R.string.titleKaraoke) + " " + getString(R.string.you_not_connect_internet));
            pDialog.setCancelable(true);
            pDialog.setConfirmText(getString(R.string.connect));
            pDialog.setConfirmClickListener(new KAlertDialog.OnSweetClickListener() {
                @Override
                public void onClick(KAlertDialog kAlertDialog) {
                    try {
                        pDialog.dismissWithAnimation();
                        Intent intent = new Intent(WifiManager.ACTION_PICK_WIFI_NETWORK);
                        startActivity(intent);
                    }catch (Exception ex)
                    {
                        Debug.e(TAG, ex);
                    }
                }
            });
            pDialog.show();
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    /**
     * listener ketika mood di click
     */
    BottomNavigationFragment.IMoodFragmentListener bottomNavigationFragmentListener = new BottomNavigationFragment.IMoodFragmentListener() {
        @Override
        public void onMoodClick() {
            listMoodView.setVisibility(View.VISIBLE);
        }
    };

    /**
     * listener ketika mood dipilih
     */
    ListMoodFragment.IListMoodListener listMoodListener = new ListMoodFragment.IListMoodListener() {
        @Override
        public void onClick(TSticker tSticker) {
            try {
                listMoodView.setVisibility(View.GONE);
                bottomNavigationFragment.updateMood(tSticker);
                Global.setStickerId(tSticker.stickerId);
            } catch (Exception ex) {
                Debug.e(TAG, ex);
            }
        }
    };

    public static void setCallBackDeleteSong(ISettingActivity iSettingActivity) {
        mListener = iSettingActivity;
    }

    @Override
    public void onDestroy()
    {
        try
        {
            super.onDestroy();
            Util.freeMemory();
        }
        catch (Exception ex)
        {
            Debug.e(TAG, ex);
        }
    }
}

package com.madeira.entertainz.karaoke.DBLocal;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import com.google.gson.annotations.SerializedName;

@Entity(tableName = "tlocalfile_apk")
public class TLocalFileAPK {
    @PrimaryKey
    @NonNull
    @SerializedName("content")
    public String content;
}

package com.madeira.entertainz.karaoke.menu_settings;


import android.content.res.ColorStateList;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.madeira.entertainz.karaoke.Debug;
import com.madeira.entertainz.karaoke.R;
import com.madeira.entertainz.karaoke.config.C;
import com.madeira.entertainz.karaoke.config.Global;
import com.madeira.entertainz.karaoke.menu_navigation.TopNavigationFragment;
import com.madeira.entertainz.library.MacUtils;
import com.madeira.entertainz.library.Util;
import com.madeira.entertainz.library.UtilTime;
import com.mukesh.OtpView;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import es.dmoral.toasty.Toasty;

/**
 * A simple {@link Fragment} subclass.
 */
public class SettingGeneralFragment extends Fragment implements SettingsTheme.ISettingsTheme {

    String TAG = "SettingGeneralFragment";

    View root;
    LinearLayout rootLL;
    Spinner languageSpinner;
    List<String> languageList = new ArrayList<>();
    List<String> codeLanguageList = new ArrayList<>();
    TextView titleTV;
    //    EditText companyNameET;
    OtpView oldPasswordOTP;
    OtpView newPasswordOTP;
    OtpView confirmPasswordOTPView;
    Button savePasswordButton;
    Boolean validation = false;
    List<String> timeZoneList = new ArrayList<>();
    List<String> codeTimeZoneList = new ArrayList<>();
    Spinner timeZoneSpinner;
    TextView ipAddressTV;

    SettingsTheme settingsTheme;
    static Button timeZoneButton;

    public SettingGeneralFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        root = inflater.inflate(R.layout.fragment_setting_general, container, false);
        try {
            bind();
            settingsTheme = new SettingsTheme(getActivity(), this);
            settingsTheme.setTheme();
            setSpinnerAdapter();
            setSpinnerTimeZoneAdapter();
            setActionObject();
//            companyNameET.setText(Global.getTitleApp());

//        languageSpinner.requestFocus();
            String ip4 = MacUtils.getIPAddress(true);

            ipAddressTV.setText(ip4);
            String timeZone =Global.getTimeZone();
            timeZoneButton.setText(" "+timeZone+" ");

//           List<String> timeZone = UtilTime.getAvailableTimeZone();
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
        return root;
    }

    @Override
    public void setThemeUpdateDate(Date lastUpdateTheme) {

    }

    @Override
    public void setListColorTheme(int color) {
        try {
            rootLL.setBackgroundResource(R.drawable.tags_rounded_corners);

            GradientDrawable categoryDrawable = (GradientDrawable) rootLL.getBackground();
            categoryDrawable.setColor(color);
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    @Override
    public void setTextAndSelectionColorTheme(ColorStateList colorSelection, ColorStateList colorText, int colorTextSelected, int colorTextFocus, int colorTextNormal, int colorSelectionSelected, int colorSelectionFocus) {

    }

    void bind() {
        try {
            languageSpinner = root.findViewById(R.id.languageSpinner);
            rootLL = root.findViewById(R.id.rootLL);
            titleTV = root.findViewById(R.id.titleTV);
//            companyNameET = root.findViewById(R.id.companyNameET);
            oldPasswordOTP = root.findViewById(R.id.oldPasswordOTP);
            newPasswordOTP = root.findViewById(R.id.newPasswordOTP);
            confirmPasswordOTPView = root.findViewById(R.id.confirmPasswordOTPView);
            savePasswordButton = root.findViewById(R.id.savePasswordButton);
            timeZoneSpinner = root.findViewById(R.id.time_zone_spinner);
            ipAddressTV = root.findViewById(R.id.ip_textview);
            timeZoneButton = root.findViewById(R.id.time_zone_button);
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    void setSelectedSpinner() {
        try {
            String languageId = Global.getLanguageId();
            if (!languageId.equals("")) {
                int index = codeLanguageList.indexOf(languageId);
                languageSpinner.setSelection(index);
            }
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    void setSpinnerAdapter() {
        try {
            codeLanguageList = new ArrayList<>();
            codeLanguageList.add("en");
            codeLanguageList.add("in");
//            codeLanguageList.add("cn");
//            codeLanguageList.add("ja");
//        codeLanguageList.add("ar");

            languageList = new ArrayList<>();
            languageList.add("English");
            languageList.add("Bahasa Indonesia");
//            languageList.add("Chinese");
//            languageList.add("Japanese");
//        languageList.add("Arabic");
            ArrayAdapter<String> adapter = new ArrayAdapter<>(getActivity(),
                    R.layout.cell_language_list, languageList);
            adapter.setDropDownViewResource(R.layout.cell_language_list);
            languageSpinner.setAdapter(adapter);
            setSelectedSpinner();
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    void setActionObject() {
        try {
            languageSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//                Toast.makeText(getActivity(), "" + languageList.get(position), Toast.LENGTH_SHORT).show();
                    String languageCode = codeLanguageList.get(position);
                    String languageId = Global.getLanguageId();

                    if (!languageCode.equals(languageId)) {
                        Global.setLanguageId(languageCode);
                        languageId = Global.getLanguageId();
                        Util.setLanguage(getActivity(), languageId);
                        getActivity().finish();
                        SettingActivity.startActivity(getActivity(), 0);
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });

            timeZoneSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    String curTimZone = Global.getTimeZone();
                    String timeZone = codeTimeZoneList.get(position);
                    if (!curTimZone.equals(timeZone)) {
                        Global.setTimeZone(timeZone);
                        UtilTime.setTimezone(getActivity(), timeZone);
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });

            oldPasswordOTP.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {
//                    companyNameET.setFocusable(false);
                    int length = s.length();
                    if (length == 6) {
                        String passwordSetting = Global.getDefaultPasswordSetting();
                        String input = s.toString();
                        if (!input.equals(passwordSetting)) {
                            oldPasswordOTP.setError(getString(R.string.oldPasswordWrong));
                            validation = false;
                        } else {
                            validation = true;
                            newPasswordOTP.requestFocus();
                        }

                    }

                }
            });

            newPasswordOTP.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {
                    int length = s.length();
                    if (length == 6) {
                        confirmPasswordOTPView.requestFocus();
                    }
                }
            });

            confirmPasswordOTPView.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {
                    int length = s.length();
                    if (length == 6) {
                        String newPassword = newPasswordOTP.getText().toString();
                        if (!s.toString().equals(newPassword)) {
                            confirmPasswordOTPView.setError(getString(R.string.yourPasswordDidntMatch));
                            validation = false;
                        } else {
                            validation = true;
                            savePasswordButton.requestFocus();
                        }
                    }
                }
            });

            savePasswordButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (validation) {
                        Global.setDefaultPasswordSetting(confirmPasswordOTPView.getText().toString());
                        Toasty.success(getContext(), getString(R.string.successChangePassword), Toast.LENGTH_SHORT, true).show();
                        getActivity().finish();

                    } else {
                        Toasty.error(getContext(), getString(R.string.failedChangePassword), Toast.LENGTH_SHORT, true).show();

                    }
                    oldPasswordOTP.setText("");
                    newPasswordOTP.setText("");
                    confirmPasswordOTPView.setText("");
//                    companyNameET.setFocusable(true);
                }
            });

            timeZoneButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    TimeZoneActivity timeZoneActivity = new TimeZoneActivity();
                    timeZoneActivity.startActivity(getActivity(), iTimeZone);
                }
            });

            timeZoneButton.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View v, boolean hasFocus) {
                    if(hasFocus)
                    {
                        timeZoneButton.setTextColor(getContext().getColor(R.color.black));
                    }
                    else
                    {
                        timeZoneButton.setTextColor(getContext().getColor(R.color.white));
                    }
                }
            });
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    /**
     * untuk set selected spinner time zone
     **/

    void setSelectedTimeZoneSpinner() {
        try {
            String languageId = Global.getTimeZone();
            if (!languageId.equals("")) {
                int index = codeTimeZoneList.indexOf(languageId);
                timeZoneSpinner.setSelection(index);
            }
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    /**
     * untuk set spinner time zone
     **/
    void setSpinnerTimeZoneAdapter() {
        try {
            codeTimeZoneList = new ArrayList<>();
            codeTimeZoneList.add("Asia/Jakarta");
            codeTimeZoneList.add("Asia/Makassar");
            codeTimeZoneList.add("Asia/Jayapura");

            timeZoneList = new ArrayList<>();
            timeZoneList.add(getString(R.string.wib));
            timeZoneList.add(getString(R.string.wita));
            timeZoneList.add(getString(R.string.wit));
            ArrayAdapter<String> adapter = new ArrayAdapter<>(getActivity(),
                    R.layout.cell_timezone_list, timeZoneList);
            adapter.setDropDownViewResource(R.layout.cell_timezone_list);
            timeZoneSpinner.setAdapter(adapter);
            setSelectedTimeZoneSpinner();
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }



   public static TimeZoneActivity.ITimeZone iTimeZone = new TimeZoneActivity.ITimeZone() {
        @Override
        public void onChangeTimeZone(String timeZone) {
            timeZoneButton.setText(" "+timeZone+" ");
        }
    };
}

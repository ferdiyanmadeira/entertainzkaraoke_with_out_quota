package com.madeira.entertainz.karaoke.DBLocal;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

@Entity(tableName = "TSTBInfo")
public class TSTBInfo implements Serializable {

    //table ini harus berisi cuma 1 row
    @PrimaryKey(autoGenerate = true)
    @SerializedName("id")
    public int id;
    @SerializedName("sTBId")
    public String sTBId;
    @SerializedName("sN")
    public String sN;
    //dicomment sementara, sampai logic quota fix
//    @SerializedName("totalSong")
//    public String totalSong;
    @SerializedName("variant_id")
    public String variantId;

}

package com.madeira.entertainz.karaoke.menu_news;

import android.content.Context;
import android.content.res.ColorStateList;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.madeira.entertainz.karaoke.DBLocal.TNewsCategory;
import com.madeira.entertainz.karaoke.Debug;
import com.madeira.entertainz.karaoke.R;
import com.madeira.entertainz.karaoke.model_online.ModelNews;
import com.madeira.entertainz.library.ApiError;
import com.madeira.entertainz.library.EzRecyclerViewAdapter;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ControllerCategoryList implements NewsTheme.INewsTheme {
    private static final String TAG = "ControllerList";

    private Context context;

    private RecyclerView recyclerView;
    private EzRecyclerViewAdapter adapter;

    private List<TNewsCategory> mainList;

    private Callback callback;
    ColorStateList colorSelection, colorText;
    NewsTheme newsTheme;

    @Override
    public void setThemeUpdateDate(Date lastUpdateTheme) {

    }

    @Override
    public void setListColorTheme(int color) {

    }

    @Override
    public void setTextAndSelectionColorTheme(ColorStateList colorSelection, ColorStateList colorText, int colorTextSelected, int colorTextFocus, int colorTextNormal, int colorSelectionSelected, int colorSelectionFocus) {
        try {
            this.colorSelection = colorSelection;
            this.colorText = colorText;
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    public interface Callback {
        void onSelectedCategory(int categoryId);
    }

    ControllerCategoryList(Context context) {
        try {
            this.context = context;
            initialize();
            newsTheme = new NewsTheme(context, this);
            newsTheme.setTheme();
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    private void initialize() {
        try {
            mainList = new ArrayList<>();
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    public void attach(RecyclerView recyclerView) {
        fetchData();
        this.recyclerView = recyclerView;
        setupRecyclerView();
    }

    public void setCallback(Callback callback) {
        this.callback = callback;
    }

    private void setupRecyclerView() {

        adapter = new EzRecyclerViewAdapter(new EzRecyclerViewAdapter.IEzRecyclerViewAdapter() {
            @Override
            public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

                //cara biasa
                View view = EzRecyclerViewAdapter.inflate(context, R.layout.cell_news_category, parent);
                return new ItemViewHolder(view);

                //cara experimental
//                return EzRecyclerViewAdapter.inflate(context, R.layout.cell_news_category, parent, ItemViewHolder.class);
            }

            @Override
            public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
                try {
                    ItemViewHolder item = (ItemViewHolder) holder;
                    item.bind(mainList.get(position), position);
                } catch (Exception ex) {
                    Debug.e(TAG, ex);
                }
            }

            @Override
            public int getItemCount() {
                return mainList.size();
            }

            @Override
            public long getItemId(int pos) {
                return mainList.get(pos).newsCategoryId;
            }
        });

        recyclerView.setAdapter(adapter);
    }

    private void fetchData() {
        ModelNews.fetchCategoryList(new ModelNews.FetchCategoryListListener() {
            @Override
            public void onSuccess(List<TNewsCategory> list) {

                try {
                    //TODO:
                    //1. load list dari local db dulu (utk awal pasti blank)
                    //2. fetch list dari api
                    //3. merge hasil fetch dgn local list
                    //4. update list

                    mainList.addAll(list);
                    adapter.notifyDataSetChanged();
                } catch (Exception ex) {
                    Debug.e(TAG, ex);
                }
            }

            @Override
            public void onError(ApiError e) {

            }
        });
    }

////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////

    View selectedItem;

    public class ItemViewHolder extends RecyclerView.ViewHolder {

        TextView textView;

        public ItemViewHolder(View itemView) {
            super(itemView);

            textView = itemView.findViewById(R.id.text_view_category);
        }

        public void bind(TNewsCategory item, int pos) {
            try {
                textView.setText(item.category);

                //select first item apabila belum ada
                if (selectedItem==null){
                    selectedItem = itemView;
                    selectedItem.setSelected(true);
                    callback.onSelectedCategory(item.newsCategoryId);
                }

                //handle event click
                itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (selectedItem!=null) selectedItem.setSelected(false);
                        selectedItem = itemView;
                        selectedItem.setSelected(true);
                        callback.onSelectedCategory(item.newsCategoryId);
                    }
                });


//                if (colorText != null) textView.setTextColor(colorText);

//                itemView.setOnFocusChangeListener(new View.OnFocusChangeListener() {
//                    @Override
//                    public void onFocusChange(View view, boolean b) {
//                        if (b) {
//                            if (selectedItem != null) selectedItem.setSelected(false);
//                            view.setSelected(true);
//
//                            //apabila yg di focus item yg berbeda maka lakukan callback
//                            if (selectedItem != view)
//                                callback.onSelectedCategory(item.newsCategoryId);
//
//                            selectedItem = view;
//
//                        } else {
//                            Log.i(TAG, "onFocusChange: LOST " + item.category);
//                        }
//                    }
//                });
            } catch (Exception ex) {
                Debug.e(TAG, ex);
            }

        }
    }
}

package com.madeira.entertainz.karaoke.menu_news;

import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.GradientDrawable;
import android.net.Uri;
import android.os.Handler;
import android.support.v4.app.FragmentTransaction;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.madeira.entertainz.karaoke.BaseActivity;
import com.madeira.entertainz.karaoke.DBLocal.JoinSettingElementMedia;
import com.madeira.entertainz.karaoke.DBLocal.TNews;
import com.madeira.entertainz.karaoke.DBLocal.TSticker;
import com.madeira.entertainz.karaoke.Debug;
import com.madeira.entertainz.karaoke.R;
import com.madeira.entertainz.karaoke.config.C;
import com.madeira.entertainz.karaoke.config.Global;
import com.madeira.entertainz.karaoke.menu_main.BottomNavigationFragment;
import com.madeira.entertainz.karaoke.menu_main.ListMoodFragment;
import com.madeira.entertainz.karaoke.menu_navigation.TopNavigationFragment;
import com.madeira.entertainz.karaoke.model_room_db.ModelElementMedia;
import com.madeira.entertainz.library.Util;

import java.io.File;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static com.madeira.entertainz.karaoke.config.CacheData.lastIndexBackground;

public class NewsActivity extends BaseActivity implements NewsTheme.INewsTheme {
    private static final String TAG = "NewsActivity";

    TopNavigationFragment navigationFragment = new TopNavigationFragment();
    BottomNavigationFragment bottomNavigationFragment = new BottomNavigationFragment();
    ListMoodFragment listMoodFragment = new ListMoodFragment();
    ControllerCategoryList controllerCategoryList;
    ControllerNewsGrid controllerNewsGrid;
    Handler handler = new Handler();
    Runnable runnable;
    ImageView backgroundIV;
    List<Integer> activeBackgroundId = new ArrayList<>();
    NewsTheme newsTheme;
    RecyclerView categoryRV;
    RecyclerView newsRV;
    View topNavigationView;
    View listMoodView;
    ProgressBar progressBar;

    public static void startActivity(Context context) {
        Intent intent;
        intent = new Intent(context, NewsActivity.class);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            setContentView(R.layout.activity_news);
            //hide bottom android nav bar
            Util.hideNavigationBar(this);
            bind();
            fetchListBackground();
            setupUi();
            newsTheme = new NewsTheme(this, this);
            newsTheme.setTheme();
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    private void bind() {
//        navigationFragment = (TopNavigationFragment) getSupportFragmentManager().findFragmentById(R.id.fragment_top_navigation);
        try {
            backgroundIV = (ImageView) findViewById(R.id.image_view_background);
            navigationFragment = navigationFragment.newInstance(C.MENU_NEWS);
            categoryRV = findViewById(R.id.recycler_view_category_list);
            newsRV = findViewById(R.id.recycler_view_news_list);
            topNavigationView = findViewById(R.id.fragment_top_navigation);
            listMoodView = findViewById(R.id.list_mood_frameLayout);
            progressBar = findViewById(R.id.progress_bar);

            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.fragment_top_navigation, navigationFragment);
            ft.replace(R.id.fragment_bottom_navigation, bottomNavigationFragment);
            ft.replace(R.id.list_mood_frameLayout, listMoodFragment);
            ft.commit();
            controllerCategoryList = new ControllerCategoryList(this);
            controllerCategoryList.attach(categoryRV);

            controllerNewsGrid = new ControllerNewsGrid(this);
            controllerNewsGrid.attach(newsRV);

            bottomNavigationFragment.setCallback(bottomNavigationFragmentListener);
            listMoodFragment.setCallBack(listMoodListener);
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    private void setupUi() {
//        navigationFragment.setTitle("News");

        controllerCategoryList.setCallback(new ControllerCategoryList.Callback() {
            @Override
            public void onSelectedCategory(int categoryId) {
                progressBar.setVisibility(View.VISIBLE);
                controllerNewsGrid.loadNews(categoryId);
            }
        });

        controllerNewsGrid.setCallback(new ControllerNewsGrid.Callback() {
            @Override
            public void onNewsClick(TNews item) {
                NewsDetailActivity.startActivity(getBaseContext(), item);
            }

            @Override
            public void onFinishLoadNews() {
                progressBar.setVisibility(View.GONE);
            }
        });
    }

    /**
     * untuk fetch list image background
     */
    void fetchListBackground() {
        ModelElementMedia.getListElementMediaActive(this, new ModelElementMedia.IGetListElementMediaActive() {
            @Override
            public void onGetListElementMedia(List<JoinSettingElementMedia> joinSettingElementMediaList) {
                int totalIndex = joinSettingElementMediaList.size();
                runnable = new Runnable() {
                    @Override
                    public void run() {
                        try {
                            if (joinSettingElementMediaList.size() != 0) {
                                if (lastIndexBackground >= totalIndex)
                                    lastIndexBackground = 0;

                                JoinSettingElementMedia joinSettingElementMedia = joinSettingElementMediaList.get(lastIndexBackground);
                                Uri uri = Uri.fromFile(new File(joinSettingElementMedia.urlImage));
//                            Picasso.with(KaraokeActivity.this).load(uri).into(backgroundIV);
                                InputStream inputStream = getContentResolver().openInputStream(uri);
                                BitmapFactory.Options options = new BitmapFactory.Options();
                                options.inPreferredConfig = Bitmap.Config.RGB_565;
                                Bitmap image = BitmapFactory.decodeStream(inputStream, null, options);
                                backgroundIV.setImageBitmap(image);

                                lastIndexBackground++;
                                handler.postDelayed(this::run, joinSettingElementMedia.duration);
                            }
                        } catch (Exception ex) {
                            Debug.e(TAG, ex);
                        }
                    }
                };
                handler.postDelayed(runnable, 100);
            }
        });
    }

    @Override
    public void setThemeUpdateDate(Date lastUpdateTheme) {

    }

    @Override
    public void setListColorTheme(int color) {
        try
        {
            categoryRV.setBackgroundResource(R.drawable.tags_rounded_corners);
            GradientDrawable recycleDrawable = (GradientDrawable) categoryRV.getBackground();
            recycleDrawable.setColor(color);

            newsRV.setBackgroundResource(R.drawable.tags_rounded_corners);
            GradientDrawable newsRecycleDrawable = (GradientDrawable) newsRV.getBackground();
            newsRecycleDrawable.setColor(color);

//            newsRV.setBackgroundResource(R.drawable.tags_rounded_corners);
            GradientDrawable navigationViewBackground = (GradientDrawable) topNavigationView.getBackground();
            navigationViewBackground.setColor(color);
        }catch (Exception ex)
        {
            Debug.e(TAG, ex);
        }
    }

    @Override
    public void setTextAndSelectionColorTheme(ColorStateList colorSelection, ColorStateList colorText, int colorTextSelected, int colorTextFocus, int colorTextNormal, int colorSelectionSelected, int colorSelectionFocus) {

    }

    @Override
    public void onDestroy() {
        try {
            super.onDestroy();
            Util.freeMemory();
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    /**
     * listener ketika mood di click
     */
    BottomNavigationFragment.IMoodFragmentListener bottomNavigationFragmentListener = new BottomNavigationFragment.IMoodFragmentListener() {
        @Override
        public void onMoodClick() {
            listMoodView.setVisibility(View.VISIBLE);
        }
    };

    /**
     * listener ketika mood dipilih
     */
    ListMoodFragment.IListMoodListener listMoodListener = new ListMoodFragment.IListMoodListener() {
        @Override
        public void onClick(TSticker tSticker) {
            try {
                listMoodView.setVisibility(View.GONE);
                bottomNavigationFragment.updateMood(tSticker);
                Global.setStickerId(tSticker.stickerId);
            } catch (Exception ex) {
                Debug.e(TAG, ex);
            }
        }
    };
}

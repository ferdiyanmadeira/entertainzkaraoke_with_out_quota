package com.madeira.entertainz.karaoke.model;

import android.util.Log;

import com.google.gson.Gson;
import com.madeira.entertainz.karaoke.DBLocal.TLocalFile;
import com.madeira.entertainz.karaoke.DBLocal.TSong;
import com.madeira.entertainz.karaoke.DBLocal.TSongCategory;
import com.madeira.entertainz.library.ApiError;
import com.madeira.entertainz.library.Util;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ModelSongLocal {

    static String TAG = "ModelSongLocal";

    public static List<TSong> geTSongFromLocal(String filePath) {
        List<TSong> TSongList = new ArrayList<TSong>();

//        List<TLocalFile> tLocalFileList = Util.getUSBFile("json");
//
//        for (TLocalFile tLocalFile : tLocalFileList) {
//            if (tLocalFile.fileType.equals("json") && tLocalFile.fileName.toLowerCase().equals("song.json")) {
                String stringJSON = "";
                try {
                    File file = new File(filePath);
                    FileInputStream fileInputStream = new FileInputStream(file);
                    stringJSON = Util.getJSONFromTextFile(fileInputStream);
                    Log.d(TAG, filePath);
                    Gson gson = new Gson();

                    //throw apabila ada apiError
                    try {
                        ApiError.process(gson, stringJSON);
                        ResultGetSongList r = gson.fromJson(stringJSON, ResultGetSongList.class);
                        TSongList = r.list;
                    } catch (ApiError apiError) {
                        apiError.printStackTrace();
                    }

                    //if no error, next convert the real data

                } catch (IOException e) {
                    e.printStackTrace();
                }
//                break;
//            }
//        }


        return TSongList;
    }

    public static List<TSongCategory> geTSongCategoryFromLocal(String filePath) {
        List<TSongCategory> TSongCategoryList = new ArrayList<TSongCategory>();

//        List<TLocalFile> tLocalFileList = Util.getUSBFile("json");

//        for (TLocalFile tLocalFile : tLocalFileList) {
//            if (tLocalFile.fileType.equals("json") && tLocalFile.fileName.toLowerCase().equals("songcategory.json")) {
                String stringJSON = "";
                try {
                    File file = new File(filePath);
                    FileInputStream fileInputStream = new FileInputStream(file);
                    stringJSON = Util.getJSONFromTextFile(fileInputStream);
                    Log.d(TAG, filePath);
                    Gson gson = new Gson();

                    //throw apabila ada apiError
                    try {
                        ApiError.process(gson, stringJSON);
                        ResultGetSongCategoryList r = gson.fromJson(stringJSON, ResultGetSongCategoryList.class);
                        TSongCategoryList = r.list;
                    } catch (ApiError apiError) {
                        apiError.printStackTrace();
                    }

                    //if no error, next convert the real data

                } catch (IOException e) {
                    e.printStackTrace();
                }
//                break;
//            }
//        }


        return TSongCategoryList;
    }

    public static class ResultGetSongList {
        public List<TSong> list;
        public int count;
    }

    public static class ResultGetSongCategoryList {
        public List<TSongCategory> list;
        public int count;
    }


}

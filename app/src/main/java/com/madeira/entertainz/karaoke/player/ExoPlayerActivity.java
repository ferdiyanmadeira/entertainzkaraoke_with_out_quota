package com.madeira.entertainz.karaoke.player;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Resources;
import android.net.Uri;
import android.os.CountDownTimer;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Surface;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;


import com.google.android.exoplayer2.DefaultLoadControl;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.ExoPlayer;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.LoadControl;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.extractor.DefaultExtractorsFactory;
import com.google.android.exoplayer2.extractor.ExtractorsFactory;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.trackselection.AdaptiveVideoTrackSelection;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelection;
import com.google.android.exoplayer2.trackselection.TrackSelector;
import com.google.android.exoplayer2.ui.SimpleExoPlayerView;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter;
import com.google.android.exoplayer2.upstream.FileDataSourceFactory;
import com.madeira.entertainz.helper.controller.media_exoplayer.ExoControllerView;
import com.madeira.entertainz.helper.equalizer.PopupEQ;
import com.madeira.entertainz.karaoke.DBLocal.DbSong;
import com.madeira.entertainz.karaoke.DBLocal.JoinSongCountPlayed;
import com.madeira.entertainz.karaoke.DBLocal.JoinSongPlaylist;
import com.madeira.entertainz.karaoke.DBLocal.TCountSongPlayed;
import com.madeira.entertainz.karaoke.DBLocal.TLogSong;
import com.madeira.entertainz.karaoke.DBLocal.TSTBInfo;
import com.madeira.entertainz.karaoke.DBLocal.TSong;
import com.madeira.entertainz.karaoke.Debug;
import com.madeira.entertainz.karaoke.R;
import com.madeira.entertainz.karaoke.config.C;
import com.madeira.entertainz.karaoke.config.Global;
import com.madeira.entertainz.library.EncryptedFileDataSourceFactory;
import com.madeira.entertainz.library.PerformAsync2;
import com.madeira.entertainz.library.Util;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

public class ExoPlayerActivity extends AppCompatActivity {
    static String TAG_Activity = "ExoPlayerActivity";
    public static String KEY_TSONG = "KEY_TSONG";
    public static String KEY_LASTPOSITION = "KEY_LASTPOSITION";

    public static final String AES_ALGORITHM = "AES";
    public static final String AES_TRANSFORMATION = "AES/CTR/NoPadding";


    private static final int LYRIC_SCROLL_SIZE = 30;

    private static final int ANIM_DURATION_PLAYBACK_CONTROL = 200;
    private static final int ANIM_DURATION_LYIRC = 200;

    private static final int DURATION_AUTOHIDE_PLAYBACK_CONTROL = 3000; //3 second

    boolean isPlaying = false;

    ArrayList<JoinSongPlaylist> songArrayList = new ArrayList<>();
    boolean isMute = true;
    static JoinSongPlaylist tSong = null;
    static int lastPosition = 0;

    private Cipher mCipher;
    private SecretKeySpec mSecretKeySpec;
    private IvParameterSpec mIvParameterSpec;

    private File mEncryptedFile;


    static SimpleExoPlayerView mExoPlayerView;

    ExoControllerView controller;

    boolean isPlaybackControlVisible = false;

    TextView title;
    TextView artist;
    TextView duration;
    static ImageView muteIV;
    static LinearLayout controlVocalLL;
    static ImageView unMuteIV;

    static int tempDuration = 0;
    private boolean mIsComplete;
    ProgressBar progressBar;
    PopupEQ eq;
    RelativeLayout rootRL;
    Handler handler = new Handler();
    Runnable runnable;
    int count = 0;
    SimpleExoPlayer simpleExoPlayer;

    IExoPlayerActivity callback;

    private long startTime = 5 * 1000; // 5 detik
    private final long interval = 1 * 1000;
    SimpleCountDownTimer simpleCountDownTimer = new SimpleCountDownTimer(startTime, interval);


    boolean isLyricVisible = false;
    ViewGroup containerLyric;
    ImageView imageViewScrollDown;
    ImageView imageViewScrollUp;
    ImageView imageViewShowHide;
    //    TextView textViewLyric;
    HandlerUiLyric handlerUiLyric;

    public static interface IExoPlayerActivity {
        //untuk update count song played adapter di karaokelist
        void onChangeCountSongPlayed();
    }

    /**
     * list tsong dibuat arraylist, karena ada fitur, next dan prev song
     */
    public static void startActivity(Activity activity, ArrayList<JoinSongPlaylist> tSongArrayList, int prmLastPosition) {
        Intent intent = new Intent(activity, ExoPlayerActivity.class);
        intent.putExtra(KEY_TSONG, tSongArrayList);

        intent.putExtra(KEY_LASTPOSITION, prmLastPosition);
        activity.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        String TAG = TAG_Activity + "-onCreate";
        try {
            setContentView(R.layout.activity_exo_player);
            bind();


            try {
                mCipher = Cipher.getInstance(AES_TRANSFORMATION);
                mCipher.init(Cipher.DECRYPT_MODE, mSecretKeySpec, mIvParameterSpec);
            } catch (Exception e) {
                e.printStackTrace();
            }

            songArrayList = (ArrayList<JoinSongPlaylist>) getIntent().getSerializableExtra(KEY_TSONG);
            lastPosition = getIntent().getIntExtra(KEY_LASTPOSITION, 0);
            if (songArrayList.size() != 0) {
                handlerUiLyric = new HandlerUiLyric(containerLyric);
                playSong(songArrayList, lastPosition);
            }
            Util.hideNavigationBar(ExoPlayerActivity.this);

            String languageId = Global.getLanguageId();
            Util.setLanguage(this, languageId);

        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    void bind() {
        mExoPlayerView = findViewById(R.id.exoplayerview);
        title = findViewById(R.id.title);
        artist = findViewById(R.id.artist);
        duration = findViewById(R.id.duration);
        progressBar = findViewById(R.id.progressBar);
        controlVocalLL = findViewById(R.id.controlVocalLL);
        muteIV = findViewById(R.id.muteIV);
        unMuteIV = findViewById(R.id.unMuteIV);
        rootRL = findViewById(R.id.rootRL);
        containerLyric = findViewById(R.id.container_lyric);
        imageViewScrollUp = findViewById(R.id.image_view_scroll_up);
        imageViewScrollDown = findViewById(R.id.image_view_scroll_down);
        imageViewShowHide = findViewById(R.id.image_view_showhide);
//        textViewLyric = findViewById(R.id.text_view_lyric);

//        textViewLyric.setMovementMethod(new ScrollingMovementMethod());
    }


    public void playSong(ArrayList<JoinSongPlaylist> prmTSongArrayList, int prmPosition) {
        String TAG = TAG_Activity + "-playSong";

        try {
            if (C.FG_SYNC_QUOTA) {

                long leftQuota = Global.getQuota();
                if (leftQuota == 100) {
                    showQuotaDialog(leftQuota);
                } else if (leftQuota == 50) {
                    showQuotaDialog(leftQuota);
                } else if (leftQuota == 20) {
                    showQuotaDialog(leftQuota);
                } else if (leftQuota == 3) {
                    showQuotaDialog(leftQuota);
                } else if (leftQuota == 0 || leftQuota < 0) {
                    showQuotaIsReached();
                    return;
                }
            }
            try {
                lastPosition = prmPosition;
                tSong = prmTSongArrayList.get(prmPosition);


                String lyric = tSong.lyric;//"Tetesan air mata jatuh lagi\r\nIngat kau di sana, kau tertawa, kau bahagia\r\nCobalah sejenak sabar menanti\r\nTunggu aku pulang bawa sebuah penawar lara\r\n\r\n'Ku lakukan semua ini untukmu\r\nAgar kita bahagia untuk selamanya\r\nAku lelah, lelah 'ku terbiasa\r\nSakit 'ku terbiasa, semua untuk kamu\r\n'Tak mengapa peluhku bercucuran\r\nSakit pun 'ku acuhkan, semua demi kamu\r\n'Ku lakukan semua ini untukmu\r\nAgar kita bahagia, bahagia kita bersama, oh\r\nAku lelah, lelah 'ku terbiasa\r\nSakit 'ku terbiasa, semua…";

                //empty string apabila lyric null
                if (lyric == null) lyric = "";


//                lyric = lyric.replaceAll("\r\n", "\n"); //rubah /r/n jadi /n
//                lyric = lyric.replaceAll("\n", "\n\n"); //rubah single /n jadi /n/n, agar ada jarak antara 1 bait dgn bait selanjutnya

//                textViewLyric.setText(lyric);

                handlerUiLyric.setLyric(lyric);


                title.setText(tSong.songName);
                artist.setText(tSong.artist);
                tempDuration = Integer.valueOf(tSong.duration) * 1000;
                duration.setText(Util.convertMilisToTimeWithText(tempDuration));
                mEncryptedFile = new File(tSong.songURL);

                DefaultBandwidthMeter bandwidthMeter = new DefaultBandwidthMeter();
                TrackSelection.Factory videoTrackSelectionFactory = new AdaptiveVideoTrackSelection.Factory(bandwidthMeter);
                TrackSelector trackSelector = new DefaultTrackSelector(new Handler());
                LoadControl loadControl = new DefaultLoadControl();
                simpleExoPlayer = (SimpleExoPlayer) ExoPlayerFactory.newSimpleInstance(this, trackSelector, loadControl);
                mExoPlayerView.setPlayer(simpleExoPlayer);
                mExoPlayerView.setUseController(false);
                ExtractorsFactory extractorsFactory = new DefaultExtractorsFactory();

                MediaSource videoSource = null;
                Uri uri = Uri.fromFile(mEncryptedFile);

                if ((tSong.iv != null && tSong.key != null) && (!tSong.iv.equals("") && !tSong.key.equals(""))) {
                    byte[] iv;
                    byte[] key;
                    iv = tSong.iv.getBytes();
                    key = tSong.key.getBytes();
                    mSecretKeySpec = new SecretKeySpec(key, AES_ALGORITHM);
                    mIvParameterSpec = new IvParameterSpec(iv);
                    DataSource.Factory dataSourceFactory = new EncryptedFileDataSourceFactory(mCipher, mSecretKeySpec, mIvParameterSpec, bandwidthMeter);
                    videoSource = new ExtractorMediaSource(uri, dataSourceFactory, extractorsFactory, null, null);

                } else {
                    videoSource = new ExtractorMediaSource(uri, new FileDataSourceFactory(), extractorsFactory, null, null);
                }

                simpleExoPlayer.prepare(videoSource);

                simpleExoPlayer.setPlayWhenReady(true);
                simpleExoPlayer.addListener(new ExoPlayer.EventListener() {
                    @Override
                    public void onLoadingChanged(boolean isLoading) {

                    }

                    @Override
                    public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
                        if (playWhenReady && playbackState == ExoPlayer.STATE_READY) {
                            // media actually playing
//                        isPlaying = true;
//                        muteVocal(true, ExoPlayerActivity.this);
                        } else if (playbackState == ExoPlayer.STATE_ENDED) {
                            if (lastPosition >= prmTSongArrayList.size() - 1) {
                                stopKaraoke();
                                finish();
                                return;
                            } else {
                                lastPosition = lastPosition + 1;
                                mIsComplete = true;
                                isPlaying = false;
                                playSong(prmTSongArrayList, lastPosition);
                            }

                        } else if (playWhenReady) {
                            isPlaying = false;
                        } else {
                            // player paused in any state
                            isPlaying = false;
                        }
                    }

                    @Override
                    public void onTimelineChanged(Timeline timeline, Object manifest) {

                    }


                    @Override
                    public void onPlayerError(ExoPlaybackException error) {

                    }

                    @Override
                    public void onPositionDiscontinuity() {

                    }
                });

                setController(prmTSongArrayList);
                increaseCountSongPlayed(tSong.songId);


            } catch (Exception ex) {
                Debug.e(TAG, ex);
            }

        } catch (
                Exception ex) {
            Debug.e(TAG, ex);
        }



        /*} else {
            showQuotaIsReached();
        }*/

    }


    void setController(ArrayList<JoinSongPlaylist> prmTSongArrayList) {
        try {
            controller = new ExoControllerView.Builder(this, new ExoControllerView.MediaPlayerControlListener() {
                @Override
                public void start(SimpleExoPlayer mp) {
                    if (null != simpleExoPlayer) {
                        mp = simpleExoPlayer;
                        progressBar.setVisibility(View.GONE);
                        mp.setPlayWhenReady(true);
                        mp.getPlaybackState();
                        mIsComplete = false;
                        muteVocal(isMute, ExoPlayerActivity.this);
                        controller.refreshProgress();
                        if (C.FG_SYNC_QUOTA)
                            increaseLogSong(tSong.songId);
                        isPlaying = true;
                    }
                }

                @Override
                public void pause() {
                    if (null != mExoPlayerView) {
                        simpleExoPlayer.setPlayWhenReady(false);
                        simpleExoPlayer.getPlaybackState();
                        isPlaying = false;
                    }
                }

                @Override
                public int getDuration() {
                    int duration = 0;
                    try {
                        if (null != mExoPlayerView) {
//                                Player mplayer = mExoPlayerView.getPlayer();
                            duration = (int) simpleExoPlayer.getDuration();
                        } else
                            return 0;
                    } catch (Exception ex) {

                    }
                    return duration;
                }

                @Override
                public int getCurrentPosition() {
                    int position = 0;
                    try {
                        if (null != mExoPlayerView) {
                            position = (int) simpleExoPlayer.getCurrentPosition();
                        } else
                            position = 0;
                    } catch (Exception ex) {

                    }
                    return position;
                }

                @Override
                public void seekTo(int position) {
                    if (null != mExoPlayerView) {
                        simpleExoPlayer.seekTo(position);
                    }
                }

                @Override
                public boolean isPlaying() {
                    if (null != mExoPlayerView) {
                        boolean prmPlay = false;
                        int playbackState = simpleExoPlayer.getPlaybackState();
                        if (playbackState == simpleExoPlayer.STATE_READY && isPlaying)
                            prmPlay = true;
                        else
                            prmPlay = false;


                        return prmPlay;
                    } else {
                        return false;
                    }
                }

                @Override
                public boolean isComplete() {
                    progressBar.setVisibility(View.GONE);
                    return mIsComplete;
                }

                @Override
                public int getBufferPercentage() {
                    return 0;
                }

                @Override
                public boolean isFullScreen() {
                    return getRequestedOrientation() == ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE ? true : false;

                }

                @Override
                public void toggleFullScreen() {
                    if (isFullScreen()) {
                        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
                    } else {
                        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
                    }
                }

                @Override
                public void exit() {

                }

                @Override
                public void unFocused() {

                }

                @Override
                public void nextSong() {
                    if (null != mExoPlayerView) {

                        if (prmTSongArrayList.size() > lastPosition + 1)
                            lastPosition = lastPosition + 1;
                        else
                            lastPosition = 0;
                        controller.unShow();
//                            Player mplayer = mExoPlayerView.getPlayer();
                        simpleExoPlayer.release();
                        playSong(prmTSongArrayList, lastPosition);
                    }
                }

                @Override
                public void prevSong() {
                    if (null != mExoPlayerView) {

                        if (prmTSongArrayList.size() < lastPosition + 1)
                            lastPosition = lastPosition - 1;
                        else
                            lastPosition = 0;
                        simpleExoPlayer.release();
                        controller.unShow();
                        playSong(prmTSongArrayList, lastPosition);
                    }
                }

                @Override
                public void equalizer() {
                    showEqualizer();
                }
            })
                    .withVideoSurfaceView(mExoPlayerView)//to enable toggle display controller view
                    .canControlBrightness(false)
                    .canControlVolume(false)
                    .canSeekVideo(false)
                    .build((FrameLayout) findViewById(R.id.video_layout));//layout container that hold video play view
            controller.exitController();
            //todo set hide controller
            controller.unShow();
        } catch (Exception ex) {
            Debug.e(TAG_Activity, ex);
        }
    }

    public void stopKaraoke() {
        String TAG = TAG_Activity + "-stopKaraoke";
        try {
            simpleExoPlayer.release();
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    @Override
    public void onBackPressed() {
        String TAG = TAG_Activity + "-onBackPressed";
        try {
            if (controller != null) {
                if (!controller.isShowing()) {
                    stopKaraoke();
                    overridePendingTransition(0, 0);
                } else {
                    controller.unShow();
                    return;
                }
            }
            super.onBackPressed();
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }

    }

    void showEqualizer() {
        String TAG = TAG_Activity + "-showEqualizer";
        try {
            if (mExoPlayerView == null) return;
            eq = new PopupEQ(this, simpleExoPlayer.getAudioSessionId(), Surface.ROTATION_0);
            eq.show(rootRL);
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }


    public void muteVocal(boolean mute, Context context) {
        String TAG = TAG_Activity + "-muteVocal";
        try {
            controlVocalLL.setVisibility(View.VISIBLE);
            if (mExoPlayerView != null) {
                if (mute) {
                    muteIV.setVisibility(View.VISIBLE);
                    unMuteIV.setVisibility(View.GONE);
                    if (Integer.valueOf(tSong.vocalSound) == 2) {
                        simpleExoPlayer.setVolumeCustom(1, 0);
                    } else if (Integer.valueOf(tSong.vocalSound) == 1) {
                        simpleExoPlayer.setVolumeCustom(0, 1);
                    } else {
                        simpleExoPlayer.setVolumeCustom(1, 1);
                    }
                } else {
                    muteIV.setVisibility(View.GONE);
                    unMuteIV.setVisibility(View.VISIBLE);
                    simpleExoPlayer.setVolumeCustom(1, 1);
                }

                //untuk memunculkan animation mute and unmute di pojok kanan atas
                Animation fadeOut = new AlphaAnimation(1, 0);
                fadeOut.setInterpolator(new AccelerateInterpolator()); // and this
                fadeOut.setStartOffset(3000);
                fadeOut.setDuration(3000);

                fadeOut.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                        controlVocalLL.setVisibility(View.GONE);
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {

                    }
                });
                controlVocalLL.startAnimation(fadeOut);
            }
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    /**
     * untuk mencatat log lagu yang pernah di play dan mengurangin quota
     */

    void increaseLogSong(String prmSongId) {
        String TAG = TAG_Activity + "-increaseLogSong";
        try {
            int increase = 1000;
            runnable = new Runnable() {
                @Override
                public void run() {
                    count = count + increase;
                    if (count > C.LIMIT_COUNT_SONG) {
                        try {
                            int songId = Integer.valueOf(prmSongId);
                            PerformAsync2.run(performAsync ->
                            {
                                int result = 0;
                                try {
                                    DbSong dbSong = DbSong.Instance.create(ExoPlayerActivity.this);
                                    DbSong.DaoAccessTLogSong dbDaoAccessTLogSong = dbSong.daoAccessTLogSong();
                                    int rowNumber = dbDaoAccessTLogSong.existSong(songId);
                                    if (rowNumber == 0) {
                                        TLogSong tLogSong = new TLogSong();
                                        tLogSong.songId = String.valueOf(songId);
                                        tLogSong.totalCount = 1;
                                        dbDaoAccessTLogSong.insert(tLogSong);
                                        result = 1;

                                    } else {
                                        dbDaoAccessTLogSong.increaseCount(songId);
                                        result = 1;
                                    }
                                    List<TLogSong> tLogSongList = dbDaoAccessTLogSong.getAll();
                                    dbSong.close();

                                    //decrease quota
                                    long quota = Global.getQuota();
                                    quota = quota - 1;
                                    Global.setQuota(quota);

                                } catch (Exception ex) {
                                    Log.d(TAG, "line : " + ex.getStackTrace()[0].getLineNumber() + ex.getMessage());
                                    result = 0;
                                }

                                return result;

                            }).setCallbackResult(result ->
                            {
                                int prm = (int) result;
                                if (prm == 1) {

                                }
                                handler.removeCallbacks(runnable);
                            });
                        } catch (Exception ex) {
                            Debug.e(TAG, ex);
                        }

                    } else {
                        //post delayed runnable selama 1 detik
                        handler.postDelayed(runnable, increase);
                    }
                }
            };
            //post delayed runnable selama 1 detik
            handler.postDelayed(runnable, increase);
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        switch (keyCode) {
            case KeyEvent.KEYCODE_0:
                isMute = isMute == true ? false : true;
                muteVocal(isMute, this);
                return true;
            case KeyEvent.KEYCODE_DPAD_RIGHT:
                //right arrow, utk show/hide lyric, apabila playback control tdk show
                if (controller.isShowing() == false) {
                    if (isLyricVisible) {
                        animHideLyric();
                    } else {
                        animShowLyric();
                    }
                    return true;
                }
            case KeyEvent.KEYCODE_DPAD_DOWN:
                //right arrow, utk show/hide lyric, apabila playback control tdk show
                if (controller.isShowing() == false) {
                    if (isLyricVisible) {
                        handlerUiLyric.onKeyDown(keyCode, event);
                    }
                    return true;
                }
                break;
            case KeyEvent.KEYCODE_DPAD_UP:
                //right arrow, utk show/hide lyric, apabila playback control tdk show
                if (controller.isShowing() == false) {
                    if (isLyricVisible) {
                        handlerUiLyric.onKeyDown(keyCode, event);
                    }
                    return true;
                }
                break;
//            default:
//                return super.onKeyUp(keyCode, event);
        }
        return super.onKeyDown(keyCode, event);
    }

    void showQuotaDialog(long prmleftOfQuota) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(ExoPlayerActivity.this);
        LayoutInflater inflater = this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.alert_quota, null);
        dialogBuilder.setView(dialogView);

        TextView textView = (TextView) dialogView.findViewById(R.id.textView);
        Button okButton = (Button) dialogView.findViewById(R.id.okButton);

        String text = String.format(getString(R.string.warning_quota_left), String.valueOf(prmleftOfQuota)) + "\n" + getString(R.string.warning_quota_left2);
        textView.setText(text);

        AlertDialog alertDialog = dialogBuilder.create();

        okButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();

            }
        });

        alertDialog.show();
        WindowManager.LayoutParams layoutParams = alertDialog.getWindow().getAttributes();
        layoutParams.width = ViewGroup.LayoutParams.WRAP_CONTENT;
        alertDialog.show();
        alertDialog.getWindow().setLayout(430, WindowManager.LayoutParams.WRAP_CONTENT);
        Util.hideNavigationBar(ExoPlayerActivity.this);
    }

    void showQuotaIsReached() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(ExoPlayerActivity.this);
        LayoutInflater inflater = this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.alert_quota_zero, null);
        dialogBuilder.setView(dialogView);

        TextView textView = (TextView) dialogView.findViewById(R.id.textView);
        Button okButton = (Button) dialogView.findViewById(R.id.okButton);

        String text = getString(R.string.warning_quota_reach);
        textView.setText(text);

        AlertDialog alertDialog = dialogBuilder.create();

        okButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                finish();
            }
        });

        alertDialog.show();
        WindowManager.LayoutParams layoutParams = alertDialog.getWindow().getAttributes();
        layoutParams.width = ViewGroup.LayoutParams.WRAP_CONTENT;
        alertDialog.show();
        alertDialog.getWindow().setLayout(430, WindowManager.LayoutParams.WRAP_CONTENT);
        Util.hideNavigationBar(ExoPlayerActivity.this);
    }


    //todo untuk mencatat lagu yang pernah di play, tidak bisa menggunakan tlogsong, karena table tersebut, jika berhasil di sync akan di truncate
    void increaseCountSongPlayed(String prmSongId) {
        String TAG = TAG_Activity + "-increaseCountSongPlayed";
        //todo ditambahkan try diluar perform async karena ada convert song id ke int, agar tidak force close ketika terjadi error
        try {
            int songId = Integer.valueOf(prmSongId);
            PerformAsync2.run(performAsync ->
            {
                int result = 0;
                try {
                    DbSong dbSong = DbSong.Instance.create(ExoPlayerActivity.this);
                    DbSong.DaoAccessTCountSongPlayed daoAccessTCountSongPlayed = dbSong.daoAccessTCountSongPlayed();
                    int rowNumber = daoAccessTCountSongPlayed.existSong(songId);
                    if (rowNumber == 0) {
                        TCountSongPlayed tCountSongPlayed = new TCountSongPlayed();
                        tCountSongPlayed.songId = String.valueOf(songId);
                        tCountSongPlayed.totalCount = 1;
                        daoAccessTCountSongPlayed.insert(tCountSongPlayed);
                        result = 1;

                    } else {
                        daoAccessTCountSongPlayed.increaseCount(songId);
                        result = 1;
                    }
                    dbSong.close();


                } catch (Exception ex) {
                    Debug.e(TAG_Activity, ex);
                    result = 0;
                }

                return result;

            }).setCallbackResult(result ->
            {
                int prm = (int) result;
                if (prm == 1) {
                    //todo jika dipanggil dari
                    if (callback != null) {
                        callback.onChangeCountSongPlayed();
                    }
                }
            });
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }


    }


    /**
     * untuk timer hide control video view
     **/
    public class SimpleCountDownTimer extends CountDownTimer {
        public SimpleCountDownTimer(long startTime, long interval) {
            super(startTime, interval);
        }

        @Override
        public void onFinish() {
            //DO WHATEVER YOU WANT HERE
            if (controller != null)
                controller.unShow();
        }

        @Override
        public void onTick(long millisUntilFinished) {
        }

    }

    @Override
    public void onUserInteraction() {
        super.onUserInteraction();

        //Reset the timer on user interaction...
        simpleCountDownTimer.cancel();
        simpleCountDownTimer.start();
    }

    final int screenWidth = Resources.getSystem().getDisplayMetrics().widthPixels;
    int[] location = new int[2];

    private void scrollLyricUp() {
//        int scrollY = textViewLyric.getScrollY();
//        int scrollAmount = LYRIC_SCROLL_SIZE;
//
//        if (scrollY<LYRIC_SCROLL_SIZE) scrollAmount = scrollY;
//
//        textViewLyric.scrollBy(0, -scrollAmount);
    }

    private void scrollLyricDown() {
//        final int PADDING_TOP_BOTTOM = textViewLyric.getPaddingTop() + textViewLyric.getPaddingBottom();
//
//        int scrollY = textViewLyric.getScrollY();
//
//        //actual content height dari textview
//        final int contentHeight = textViewLyric.getLayout().getLineTop(textViewLyric.getLineCount()) - textViewLyric.getHeight() + PADDING_TOP_BOTTOM;
//        int scrollAmount = contentHeight - scrollY;
//
//        if (scrollAmount>LYRIC_SCROLL_SIZE) scrollAmount = LYRIC_SCROLL_SIZE;
//
////        Log.i(TAG, "scrollLyricDown: \ncontentHeight=" + contentHeight + "\nscrollY=" + scrollY+ "\nscrollAmount="+scrollAmount);
//
//        //scroll apabila nilai scrollAmount positif
//        if (scrollAmount>0) textViewLyric.scrollBy(0, scrollAmount);

    }

    /**
     * Animate show lyric dari kanan ke kiri
     */
    private void animShowLyric() {

        //exit apabila lyric sdh show
        if (isLyricVisible) return;

        isLyricVisible = true;

        containerLyric.getLocationOnScreen(location); //posisi containerlyric di layar

        //cari width posisi containerlyric dgn sisi kanan screen
        float initialPos = screenWidth - location[0];

        Animation animation = new TranslateAnimation(initialPos, 0, 0, 0);
        animation.setDuration(ANIM_DURATION_LYIRC);
        animation.setFillAfter(false);

        containerLyric.startAnimation(animation);
        containerLyric.setVisibility(View.VISIBLE);
    }

    /**
     * Animate hide lyric dari kiri ke kanan
     */
    private void animHideLyric() {

        //exit apabila lyric sdh hide
        if (isLyricVisible == false) return;

        isLyricVisible = false;

        float finalPos = screenWidth - location[0];

        Animation animation = new TranslateAnimation(0, finalPos, 0, 0);
        animation.setDuration(ANIM_DURATION_LYIRC);
        animation.setFillAfter(false);

        containerLyric.startAnimation(animation);
        containerLyric.setVisibility(View.INVISIBLE);
    }


    /**
     * class ini utk handle ui lyric saat up dan down
     */
    class HandlerUiLyric {
        private static final String TAG = "HandlerUiLyric";
        private static final int FOCUS_BACKGROUND_COLOR = 0x20FFFFFF;
        private static final int MAX_LINE = 7;
        private static final int MIDDLE_LINE = 4;

        TextView lyricLine1;
        TextView lyricLine2;
        TextView lyricLine3;
        TextView lyricLine4;
        TextView lyricLine5;
        TextView lyricLine6;
        TextView lyricLine7;

        TextView textViewFocus; //line yg dalam focus

        String[] arLine = new String[]{}; //create empty array

        int linePos; //start dari 0
        int cursorPos; //start dari 1

        public HandlerUiLyric(View root) {
            bind(root);
        }

        public void setLyric(String lyric) {

            if (lyric == null || lyric.length() == 0) return;

            lyric = lyric.replaceAll("\r\n", "\n");

            arLine = lyric.split("\n");

            ArrayList<String> listNew = new ArrayList<>();

            int idx = 0;
            while (idx < (arLine.length)) {
                String text = arLine[idx];
                text = text.replaceAll("\n", ""); //hapus semua \n
                if (text.length() > 0) listNew.add(text);
                idx++;
            }

            arLine = listNew.toArray(new String[0]);

            clear();
            update();
        }

        private void bind(View root) {
            lyricLine1 = root.findViewById(R.id.lyric_line_1);
            lyricLine2 = root.findViewById(R.id.lyric_line_2);
            lyricLine3 = root.findViewById(R.id.lyric_line_3);
            lyricLine4 = root.findViewById(R.id.lyric_line_4);
            lyricLine5 = root.findViewById(R.id.lyric_line_5);
            lyricLine6 = root.findViewById(R.id.lyric_line_6);
            lyricLine7 = root.findViewById(R.id.lyric_line_7);
        }

        public boolean onKeyDown(int keyCode, KeyEvent event) {

            if (arLine.length == 0) return false;

            switch (keyCode) {
                case KeyEvent.KEYCODE_DPAD_UP:
                    scrollUp();
                    return true;

                case KeyEvent.KEYCODE_DPAD_DOWN:
                    scrollDown();
                    return true;
            }

            return false;
        }

        private void clear() {
            lyricLine1.setText(null);
            lyricLine2.setText(null);
            lyricLine3.setText(null);
            lyricLine4.setText(null);
            lyricLine5.setText(null);
            lyricLine6.setText(null);
            lyricLine7.setText(null);
        }

        private void update() {
            if (arLine.length == 0) return;

            if ((arLine.length - linePos) > 0) lyricLine1.setText(arLine[linePos]);
            if ((arLine.length - linePos) > 1) lyricLine2.setText(arLine[linePos + 1]);
            if ((arLine.length - linePos) > 2) lyricLine3.setText(arLine[linePos + 2]);
            if ((arLine.length - linePos) > 3) lyricLine4.setText(arLine[linePos + 3]);
            if ((arLine.length - linePos) > 4) lyricLine5.setText(arLine[linePos + 4]);
            if ((arLine.length - linePos) > 5) lyricLine6.setText(arLine[linePos + 5]);
            if ((arLine.length - linePos) > 6) lyricLine7.setText(arLine[linePos + 6]);

            setFocus(cursorPos);
        }

        private void setFocus(int cursorPos) {
            if (textViewFocus != null) textViewFocus.setBackgroundColor(0);

            if (cursorPos < 1) cursorPos = 1;
            if (cursorPos > 7) cursorPos = 7;

            switch (cursorPos) {
                case 1:
                    textViewFocus = lyricLine1;
                    break;
                case 2:
                    textViewFocus = lyricLine2;
                    break;
                case 3:
                    textViewFocus = lyricLine3;
                    break;
                case 4:
                    textViewFocus = lyricLine4;
                    break;
                case 5:
                    textViewFocus = lyricLine5;
                    break;
                case 6:
                    textViewFocus = lyricLine6;
                    break;
                case 7:
                    textViewFocus = lyricLine7;
                    break;
            }

            textViewFocus.setBackgroundColor(FOCUS_BACKGROUND_COLOR);
        }

        private void scrollUp() {
            int delta = arLine.length - linePos;
            int numberOfLineLeft = delta;

            if (linePos == 0) {
                cursorPos--;
                if (cursorPos < 1) cursorPos = 1;
                update();
                return;
            }

            if (cursorPos == MIDDLE_LINE) {
                linePos--;
                update();
                return;
            }

            cursorPos--;
            if (cursorPos < 1) cursorPos = 1;
            update();
        }

        private void scrollDown() {
            int delta = arLine.length - linePos;
            int numberOfLineLeft = delta;

            //apabila numberOfLine <= MAX_LINE artinya tdk boleh scroll lagi
            if (numberOfLineLeft <= MAX_LINE) {
                cursorPos++;
                if (cursorPos > delta) cursorPos = delta;
                update();
                return;
            }

            if (cursorPos == MIDDLE_LINE) {
                linePos++;
                update();
                return;
            }

            cursorPos++;
            update();
        }

    }

    @Override
    public void onStop() {
        super.onStop();
        String TAG = TAG_Activity + "-onBackPressed";
        try {
            stopKaraoke();
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }


}



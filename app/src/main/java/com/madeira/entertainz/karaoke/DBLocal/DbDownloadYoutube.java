package com.madeira.entertainz.karaoke.DBLocal;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Database;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

import java.util.List;

@Database(entities = {TDownloadYoutube.class, TDownloadYoutubeDt.class}, version = 2, exportSchema = false)

/**
 * Database News, berisikan NewsCategory dan News
 */
public abstract class DbDownloadYoutube extends RoomDatabase {

    public abstract DaoDownloadYoutube daoDownloadYoutube();
    public abstract DaoDownloadYoutubeDt daoDownloadYoutubeDt();

    public static class Instance {
        private static final String DATABASE_NAME = "YoutubeDownload.db";

        public static DbDownloadYoutube create(Context context) {
            return Room.databaseBuilder(context, DbDownloadYoutube.class, DATABASE_NAME)
                    .fallbackToDestructiveMigration()
                    .build();
        }
    }

    @Dao
    public interface DaoDownloadYoutube {

        @Insert(onConflict = OnConflictStrategy.REPLACE)
        void insert(TDownloadYoutube item);

        @Insert
        void insertAll(List<TDownloadYoutube> list);

        @Query("SELECT * FROM TDownloadYoutube")
        List<TDownloadYoutube> getAll();

        @Query("DELETE FROM TDownloadYoutube")
        void deleteAll();

    }

    @Dao
    public interface DaoDownloadYoutubeDt {

        @Insert(onConflict = OnConflictStrategy.REPLACE)
        void insert(TDownloadYoutubeDt item);

        @Insert
        void insertAll(List<TDownloadYoutubeDt> list);

        @Query("SELECT * FROM TDownloadYoutubeDt WHERE songId=:songId")
        List<TDownloadYoutubeDt> getDownloadYoutubeDtById(String songId);

        @Query("DELETE FROM TDownloadYoutubeDt")
        void deleteAll();
    }

}

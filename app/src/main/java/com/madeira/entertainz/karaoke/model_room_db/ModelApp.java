package com.madeira.entertainz.karaoke.model_room_db;

import android.content.Context;

import com.google.gson.Gson;
import com.madeira.entertainz.karaoke.BuildConfig;
import com.madeira.entertainz.karaoke.DBLocal.DbAccess;
import com.madeira.entertainz.karaoke.DBLocal.JoinSongCountCategory;
import com.madeira.entertainz.karaoke.DBLocal.TSTBInfo;
import com.madeira.entertainz.karaoke.Debug;
import com.madeira.entertainz.karaoke.MainApplication;
import com.madeira.entertainz.karaoke.config.Global;
import com.madeira.entertainz.library.Util;
import com.madeira.eznet.lib.HelperGson;

import java.util.ArrayList;
import java.util.List;

public class ModelApp {
    static String TAG = "ModelApp";


    public static TSTBInfo syncGetSTBInfo(Context context) {
        List<TSTBInfo> result = new ArrayList<>();
        TSTBInfo tstbInfo = new TSTBInfo();
        try {
            DbAccess dbAccess = DbAccess.Instance.create(context);
            result = dbAccess.daoAccessTSTBInfo().getAll();
            if (result.size() > 0)
                tstbInfo = result.get(0);
            dbAccess.close();
        } catch (Exception ex) {

        }
        return tstbInfo;
    }

    public static final String EKMAGetSTBInfo() {
        String result = "";
        try {
            TSTBInfo tstbInfo = syncGetSTBInfo(MainApplication.context);
            ResultSTBInfo resultSTBInfo = new ResultSTBInfo();
            resultSTBInfo.sn = tstbInfo.sN;

            Util.ResultStorage resultStorage = Util.getUSBInfoSize();
            resultSTBInfo.totalSpace = resultStorage.totalSpaceStr;
            resultSTBInfo.freeSpace = resultStorage.freeSpaceStr;
            resultSTBInfo.usedSpace = resultStorage.usedSpaceStr;
            resultSTBInfo.totalSpaceValue = resultStorage.totalSpace;
            resultSTBInfo.freeSpaceValue = resultStorage.freeSpace;
            resultSTBInfo.usedSpaceValue = resultStorage.usedSpace;
            resultSTBInfo.macAddress = Util.getMACAddress("eth0");
            String versionName = BuildConfig.VERSION_NAME;
            resultSTBInfo.version = versionName;
            resultSTBInfo.updateDate = Global.getLastUpdate();
            resultSTBInfo.songGroupByCategory = ModelSong.syncGetJoinCountSongCategory(MainApplication.context);
                result = HelperGson.toString(resultSTBInfo);
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
        return result;
    }



    public static final String EKMAGetServer() {
        String result = "";
        try {
            ResultServerInfo resultServerInfo = new ResultServerInfo();
            resultServerInfo.server = Global.getApiHostname();

            result = HelperGson.toString(resultServerInfo);
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
        return result;
    }

    public static final String EKMASetServer(String jsonString) {
        String result = "0";
        try {
            Gson gson = new Gson();
            Global.setApiHostname(jsonString);
            result ="1";
            ResultEKMA resultEKMA = new ResultEKMA();
            resultEKMA.result = result;
            result = HelperGson.toString(resultEKMA);
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }

        return result;
    }
    public static class ResultEKMA {
        String result;
    }


    static class ResultServerInfo
    {
        public  String server;
    }

    static class ResultSTBInfo {
        public String sn;
        public String totalSpace;
        public String usedSpace;
        public String freeSpace;
        public long totalSpaceValue;
        public long usedSpaceValue;
        public long freeSpaceValue;
        public String version;
        public String updateDate;
        public String macAddress;
        public List<JoinSongCountCategory> songGroupByCategory;

    }


}

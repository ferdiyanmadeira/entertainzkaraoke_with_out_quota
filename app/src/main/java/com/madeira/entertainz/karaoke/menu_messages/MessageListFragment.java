package com.madeira.entertainz.karaoke.menu_messages;

import android.content.res.ColorStateList;
import android.graphics.Typeface;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.madeira.entertainz.helper.RecyclerViewAdapter;
import com.madeira.entertainz.karaoke.DBLocal.DbMessage;
import com.madeira.entertainz.karaoke.DBLocal.TMessage;
import com.madeira.entertainz.karaoke.Debug;
import com.madeira.entertainz.karaoke.R;
import com.madeira.entertainz.karaoke.config.C;
import com.madeira.entertainz.karaoke.model_room_db.ModelMessage;
import com.madeira.entertainz.library.PerformAsync2;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class MessageListFragment extends Fragment implements MessagesTheme.IMessagesTheme {
    String TAG = "MessageListFragment";
    int selectedItem;

    View root;
    LinearLayout categoryLL;
    TextView categoryTV;
    LinearLayout recyclerLL;
    RecyclerView recyclerView;
    View selectedView;

    RecyclerViewAdapter adapter;
    private IMessageListListener mListener;
    MessagesTheme messagesTheme;
    ColorStateList colorSelection, colorText;
    List<TMessage> tMessageList = new ArrayList<>();


    public MessageListFragment() {
        // Required empty public constructor
    }

    public static MessageListFragment newInstance() {
        MessageListFragment fragment = new MessageListFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        try {
            root = inflater.inflate(R.layout.fragment_message_list, container, false);
            bind();
            messagesTheme = new MessagesTheme(getContext(), this);
            messagesTheme.setTheme();
            fetchMessage();
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
        return root;
    }

    @Override
    public void setThemeUpdateDate(Date lastUpdateTheme) {

    }

    @Override
    public void setListColorTheme(int color) {
        try {
            categoryLL.setBackgroundResource(R.drawable.tags_rounded_corners);
            recyclerLL.setBackgroundResource(R.drawable.tags_rounded_corners);

            GradientDrawable categoryDrawable = (GradientDrawable) categoryLL.getBackground();
            categoryDrawable.setColor(color);

            GradientDrawable recycleDrawable = (GradientDrawable) recyclerLL.getBackground();
            recycleDrawable.setColor(color);
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    @Override
    public void setTextAndSelectionColorTheme(ColorStateList colorSelection, ColorStateList colorText, int colorTextSelected, int colorTextFocus, int colorTextNormal, int colorSelectionSelected, int colorSelectionFocus) {
        try {
            categoryTV.setTextColor(colorTextNormal);
            this.colorSelection = colorSelection;
            this.colorText = colorText;
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    public interface IMessageListListener {
        // TODO: Update argument type and name
        void onMessageListClick(int messageId);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    void bind() {
        try {
            categoryLL = (LinearLayout) root.findViewById(R.id.categoryLL);
            categoryTV = (TextView) root.findViewById(R.id.categoryTV);
            recyclerLL = (LinearLayout) root.findViewById(R.id.recyclerLL);
            recyclerView = (RecyclerView) root.findViewById(R.id.recyclerView);
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    void fetchMessage() {
        ModelMessage.getListMessage(getContext(), new ModelMessage.IGetListMessage() {
            @Override
            public void onGetListMessage(List<TMessage> TMessageList) {
                try {
                    tMessageList = TMessageList;
                    Log.d(TAG, String.valueOf(tMessageList.size()));
                    setRecyclerView();
                    Log.d(TAG, String.valueOf(tMessageList.size()));
                } catch (Exception ex) {
                    Debug.e(TAG, ex);
                }
            }
        });
        /*try {
            PerformAsync2.run(performAsync -> {
                int result = 0;
                try {
                    DbMessage dbMessage = DbMessage.Instance.create(getActivity());
                    tMessageList = dbMessage.daoAccessMessage().getAll();
                    Log.d(TAG, String.valueOf(tMessageList.size()));
                    dbMessage.close();
                    result = 1;
                } catch (Exception ex) {

                }
                return result;
            }).setCallbackResult(result -> {
                int i = (int) result;
                if (i != 1) {
                    return;
                } else {
                    setRecyclerView();
                    Log.d(TAG, String.valueOf(tMessageList.size()));
                }
            });

        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }*/
    }

    void setRecyclerView() {
        try {
            adapter = new RecyclerViewAdapter(new RecyclerViewAdapter.IRecyclerViewAdapter() {
                @Override
                public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
                    View view = LayoutInflater.from(getActivity()).inflate(R.layout.cell_message_list, parent, false);
                    ItemViewHolder item = new ItemViewHolder(view);
                    return item;
                }

                @Override
                public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
                    ItemViewHolder item = (ItemViewHolder) holder;
                    item.bind(tMessageList.get(position), position);
                }

                @Override
                public int getItemCount() {
                    return tMessageList.size();
                }
            });

            recyclerView.setAdapter(adapter);
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    class ItemViewHolder extends RecyclerView.ViewHolder {
        private static final String TAG = "ItemViewHolder";

        View root;
        TextView tvTitle;

        public ItemViewHolder(View itemView) {
            super(itemView);
            root = itemView;
            root.setFocusable(true);
            tvTitle = itemView.findViewById(R.id.text_title);
        }

        public void bind(TMessage tMessage, int pos) {
            tvTitle.setText(tMessage.title);

            if (tMessage.status.equals(C.MESSAGE_STATUS_READ)) {
                tvTitle.setTypeface(null, Typeface.NORMAL);
            } else {
                tvTitle.setTypeface(null, Typeface.BOLD);

            }

            if (selectedItem == pos) {
                root.requestFocus();
                selectView(root);
                mListener.onMessageListClick(tMessage.messageId);
            }

//            untuk merubah warna selection
            if (colorSelection != null) root.setBackgroundTintList(colorSelection);

//            untuk merubah warna text
            if (colorText != null) tvTitle.setTextColor(colorText);

           /* if (pos == 0) {
                root.setSelected(true);
                mListener.onMessageListClick(tMessage.messageId);
            }*/

            root.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    selectView(v);
//                    mListener.onMessageListClick(tMessage.messageId);

                    selectedItem = pos;
                    mListener.onMessageListClick(tMessage.messageId);
                    tvTitle.setTypeface(null, Typeface.NORMAL);

                }
            });
        }

        void selectView(View v) {
            //clear selection
            if (selectedView != null) {
                selectedView.setSelected(false);
            }

            //make selection
            selectedView = v;
            selectedView.setSelected(true);

//            selectedView.requestFocusFromTouch();
//            selectedView.requestFocus();

        }
    }


    public void setCallback(IMessageListListener listener) {
        this.mListener = listener;
    }
}

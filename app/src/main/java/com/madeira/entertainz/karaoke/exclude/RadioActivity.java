package com.madeira.entertainz.karaoke.exclude;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Handler;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;

import com.madeira.entertainz.karaoke.DBLocal.DbAccess;
import com.madeira.entertainz.karaoke.DBLocal.JoinSettingElementMedia;
import com.madeira.entertainz.karaoke.Debug;
import com.madeira.entertainz.karaoke.R;
import com.madeira.entertainz.karaoke.config.Global;
import com.madeira.entertainz.karaoke.menu_navigation.TopNavigationFragment;
import com.madeira.entertainz.library.PerformAsync2;
import com.madeira.entertainz.library.Util;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import static com.madeira.entertainz.karaoke.config.CacheData.lastIndexBackground;

public class RadioActivity extends AppCompatActivity {
    String TAG = "KaraokeActivity";
    ImageView backgroundIV;
    TopNavigationFragment topNavigationFragment = new TopNavigationFragment();
    RadioListFragment radioListFragment = new RadioListFragment();
    RadioDetailFragment radioDetailFragment = new RadioDetailFragment();
    Handler handler = new Handler();
    Runnable runnable;

    public static void startActivity(Activity activity) {
        Intent intent = new Intent(activity, RadioActivity.class);
        activity.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        try {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_radio);
            bind();
            bindFragment();
            fetchListBackground();
            Util.hideNavigationBar(this);

            String languageId = Global.getLanguageId();
            Util.setLanguage(this, languageId);
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    void bind() {
        backgroundIV = (ImageView) findViewById(R.id.backgroundIV);
    }

    void bindFragment() {
        try {
//            topNavigationFragment = topNavigationFragment.newInstance(getString(R.string.title_radio));
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.topNavFrameLayout, topNavigationFragment);
            ft.replace(R.id.categoryFrameLayout, radioListFragment);
            ft.replace(R.id.detailFrameLayout, radioDetailFragment).addToBackStack(null);

            ft.commit();
            radioListFragment.setCallback(iRadioListListener);
            radioDetailFragment.setCallback(iRadioDetailListener);
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    void fetchListBackground() {
        try {
            PerformAsync2.run(performAsync ->
                    {
                        List<JoinSettingElementMedia> result = new ArrayList<>();
                        try {
                            DbAccess dbAccess = DbAccess.Instance.create(RadioActivity.this);
                            result = dbAccess.daoAccessThemeMedia().joinSettingElementMediaActive();
                            dbAccess.close();
                        } catch (Exception ex) {

                        }
                        return result;
                    }
            ).setCallbackResult(result ->
            {
                List<JoinSettingElementMedia> joinSettingElementMediaList = (List<JoinSettingElementMedia>) result;
                int totalIndex = joinSettingElementMediaList.size();
                runnable = new Runnable() {
                    @Override
                    public void run() {
                        if (joinSettingElementMediaList.size() != 0) {
                            if (lastIndexBackground >= totalIndex)
                                lastIndexBackground = 0;

                            JoinSettingElementMedia joinSettingElementMedia = joinSettingElementMediaList.get(lastIndexBackground);
                            Uri uri = Uri.fromFile(new File(joinSettingElementMedia.urlImage));
//                    Picasso.with(KaraokeActivity.this).load(uri).into(backgroundIV);
                            try {
                                InputStream inputStream = getContentResolver().openInputStream(uri);
                                BitmapFactory.Options options = new BitmapFactory.Options();
                                options.inPreferredConfig = Bitmap.Config.RGB_565;
                                Bitmap image = BitmapFactory.decodeStream(inputStream, null, options);
                                backgroundIV.setImageBitmap(image);
                            } catch (FileNotFoundException e) {
                                e.printStackTrace();
                            }
                            lastIndexBackground++;
                            handler.postDelayed(this::run, joinSettingElementMedia.duration);
                        }

                    }
                };
                handler.postDelayed(runnable, 100);
            });
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        try {
            //agar runable tidak running ketika di finish activity
            Util.hideNavigationBar(this);
            radioDetailFragment.releaseExoPlayer();
            finish();
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    RadioListFragment.IRadioListListener iRadioListListener = new RadioListFragment.IRadioListListener() {
        @Override
        public void onCategoryClick(int categoryId, int position) {
            try {
                radioDetailFragment.releaseExoPlayer();
                radioDetailFragment = new RadioDetailFragment();
                Bundle listBundle = new Bundle();
                listBundle.putInt(radioDetailFragment.KEY_RADIOID, categoryId);
                listBundle.putInt(radioDetailFragment.KEY_POSITION, position);
                radioDetailFragment.setArguments(listBundle);

                FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                ft.replace(R.id.detailFrameLayout, radioDetailFragment).addToBackStack(null);
                ft.commit();
                radioDetailFragment.setCallback(iRadioDetailListener);
                radioListFragment.setCallback(iRadioListListener);
            } catch (Exception ex) {
                Debug.e(TAG, ex);
            }

        }
    };

    RadioDetailFragment.IRadioDetailListener iRadioDetailListener = new RadioDetailFragment.IRadioDetailListener() {
        @Override
        public void onPrev(int prmRadioId, int position) {

            try {
                radioListFragment = new RadioListFragment();
                Bundle listBundle = new Bundle();
                listBundle.putBoolean(radioListFragment.KEY_FLAG_INCREASE, false);
                listBundle.putInt(radioListFragment.KEY_LAST_POSITION, position);
                listBundle.putInt(radioListFragment.KEY_RADIOID, prmRadioId);

                radioListFragment.setArguments(listBundle);

                FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                ft.replace(R.id.categoryFrameLayout, radioListFragment).addToBackStack(null);
                ft.commit();
                radioListFragment.setCallback(iRadioListListener);
            } catch (Exception ex) {
                Debug.e(TAG, ex);
            }
        }

        @Override
        public void onNext(int prmRadioId, int position) {
            try {
                radioListFragment = new RadioListFragment();
                Bundle listBundle = new Bundle();
                listBundle.putBoolean(radioListFragment.KEY_FLAG_INCREASE, true);
                listBundle.putInt(radioListFragment.KEY_LAST_POSITION, position);
                listBundle.putInt(radioListFragment.KEY_RADIOID, prmRadioId);

                radioListFragment.setArguments(listBundle);

                FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                ft.replace(R.id.categoryFrameLayout, radioListFragment).addToBackStack(null);
                ft.commit();
            } catch (Exception ex) {
                Debug.e(TAG, ex);
            }
        }
    };

}

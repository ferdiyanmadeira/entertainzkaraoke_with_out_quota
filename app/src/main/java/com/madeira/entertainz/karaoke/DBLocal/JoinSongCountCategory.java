package com.madeira.entertainz.karaoke.DBLocal;

import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class JoinSongCountCategory implements Serializable
{
    @PrimaryKey
    @NonNull
    @SerializedName("songCategoryId")
    public int songCategoryId;
    @SerializedName("songCategory")
    public String songCategory;
    @SerializedName("totalCount")
    public int totalCount;

}

package com.madeira.entertainz.karaoke.DBLocal;

import android.arch.persistence.db.SupportSQLiteOpenHelper;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Database;
import android.arch.persistence.room.DatabaseConfiguration;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.InvalidationTracker;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;
import android.support.annotation.NonNull;

import java.util.List;

@Database(entities = {TNewsCategory.class, TNews.class}, version = 1, exportSchema = false)

/**
 * Database News, berisikan NewsCategory dan News
 */
public abstract class DbNews extends RoomDatabase {



    public static class Instance {
        private static final String DATABASE_NAME = "news.db";

        public static DbNews create(Context context) {
            return Room.databaseBuilder(context, DbNews.class, DATABASE_NAME)
                    .fallbackToDestructiveMigration()
                    .build();
        }
    }

    @Dao
    public interface DaoNewsCategory {

        @Insert(onConflict = OnConflictStrategy.REPLACE)
        void insert(TNewsCategory element);

        @Insert
        void insertAll(List<TNewsCategory> list);

        @Query("SELECT * FROM tnews_category")
        List<TNewsCategory> getAll();

        @Query("DELETE FROM tnews_category")
        void deleteAll();

    }

    @Dao
    public interface DaoNews {

        @Insert(onConflict = OnConflictStrategy.REPLACE)
        void insert(TNews element);

        @Insert
        void insertAll(List<TNews> list);

        @Query("SELECT * FROM tnews WHERE newsCategoryId=:categoryId")
        List<TNews> getNewsByCategory(int categoryId);

        @Query("DELETE FROM tnews")
        void deleteAll();
    }

}

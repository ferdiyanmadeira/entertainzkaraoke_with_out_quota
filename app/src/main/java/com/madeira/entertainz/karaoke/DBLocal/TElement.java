package com.madeira.entertainz.karaoke.DBLocal;

import android.arch.persistence.room.Entity;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.util.Log;


import com.google.gson.annotations.SerializedName;

import java.util.Hashtable;
import java.util.List;

@Entity(tableName = "telement", primaryKeys = {"themeId","elementId"})
public class TElement {
    private static final String TAG = "TElement";

    @NonNull
    @SerializedName("theme_id")
    public int themeId;

    @NonNull
    @SerializedName("element_id")
    public int elementId;

    @SerializedName("update_date")
    public String updateDate;

    @SerializedName("url_image")
    public String urlImage;

    @SerializedName("color_value")
    public String colorValue;

    public String type;

    //equals dipergunakan ukt indexOf
    //
    @Override
    public boolean equals(Object obj) {
        TElement element = (TElement) obj;
        return element.elementId==elementId;
    }

    //function utk search element dari itemsList
    static public TElement find(List<TElement> list, int elementId){
        Log.i(TAG, "find=" + elementId);
        TElement findElement = new TElement();
        findElement.elementId = elementId;

        int idx = list.indexOf(findElement);
        if (idx<0) return null;
        return list.get(idx);
    }

    static public int findAndGetColor(List<TElement> list, int elementId){
        TElement element = find(list, elementId);
        if (element==null) return 0;
        return Color.parseColor("#" + element.colorValue);
    }

    static public int findAndGetColor(Hashtable<Integer, TElement> list, int elementId){
        TElement element = list.get(elementId);
        if (element==null) return 0;
        return Color.parseColor("#" + element.colorValue);
    }

    public int parseColor(){
        if (colorValue==null) return 0;
        if (colorValue.length()==0) return 0;
        if (colorValue.length()< 8) {
            colorValue = "FFFFFFFF" + colorValue;
            colorValue = colorValue.substring( colorValue.length()-8);
        }

        try{
            return Color.parseColor("#" + colorValue);

        }catch (Exception e){
            e.printStackTrace();
        }

        return 0;
    }


}
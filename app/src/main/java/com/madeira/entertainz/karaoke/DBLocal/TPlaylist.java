package com.madeira.entertainz.karaoke.DBLocal;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;

@Entity(tableName = "tplaylist")
public class TPlaylist {

    @PrimaryKey(autoGenerate = true)
    @SerializedName("playlistId")
    public int playlistId;
    @SerializedName("playlistName")
    public String playlistName;
}

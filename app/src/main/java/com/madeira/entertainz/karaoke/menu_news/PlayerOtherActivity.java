package com.madeira.entertainz.karaoke.menu_news;

import android.app.Activity;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.ToggleButton;
import android.widget.VideoView;

import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.ui.SimpleExoPlayerView;
import com.madeira.entertainz.karaoke.BaseActivity;
import com.madeira.entertainz.karaoke.R;
import com.madeira.entertainz.library.Util;

import java.io.IOException;

public class PlayerOtherActivity extends BaseActivity {
    private static final String TAG = "PlayerExoActivity";

    private static final String INTENT_URL = "INTENT_URL";

    final float VIDEO_RATIO = 9F / 16F;

    String url;


    VideoView videoView;

    public static void startActivity(Activity activity, String url){
        Intent intent = new Intent(activity, PlayerOtherActivity.class);

        intent.putExtra(INTENT_URL, url);

        activity.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_player_other);

//        Util.hideNavigationBar(this);

//        // This work only for android 4.4+
//        if(android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT)
//        {
//            final int flags = View.SYSTEM_UI_FLAG_LAYOUT_STABLE
//                    | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
//                    | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
//                    | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
//                    | View.SYSTEM_UI_FLAG_FULLSCREEN
//                    | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;
//
//
//            // Code below is to handle presses of Volume up or Volume down.
//            // Without this, after pressing volume buttons, the navigation bar will
//            // show up and won't hide
//            final View decorView = getWindow().getDecorView();
//
//            decorView.setSystemUiVisibility(flags);
//
//            decorView.setOnSystemUiVisibilityChangeListener(new View.OnSystemUiVisibilityChangeListener()
//                    {
//
//                        @Override
//                        public void onSystemUiVisibilityChange(int visibility)
//                        {
////                            Util.hideNavigationBar(PlayerOtherActivity.this);
//
//                            decorView.setSystemUiVisibility(flags);
//                        }
//                    });
//        }

        bind();

        setupPlayback();
    }

    private void bind(){
        videoView = findViewById(R.id.video_view);
    }

    private void setupPlayback(){
        url = getIntent().getStringExtra(INTENT_URL);

        videoView.setVideoURI(Uri.parse(url));
        videoView.start();

        //playback control
        MediaController mediaController = new MediaController(this);
        videoView.setMediaController(mediaController);

        videoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mediaPlayer) {
                finish(); //close activity when finish
            }
        });
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event){
        switch (keyCode){
            case KeyEvent.KEYCODE_BACK:
                videoView.stopPlayback();
                finish();
                break;
        }
        return super.onKeyDown(keyCode, event);
    }


}

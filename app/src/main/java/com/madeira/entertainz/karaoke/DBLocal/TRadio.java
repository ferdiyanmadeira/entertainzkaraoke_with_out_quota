package com.madeira.entertainz.karaoke.DBLocal;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;


@Entity(tableName = "tradio")
public class TRadio implements Serializable {

    @PrimaryKey @NonNull
    @SerializedName("radioId")
    public int radioId;
    @SerializedName("radioName")
    public String radioName;
    @SerializedName("urlImage")
    public String urlImage;
    @SerializedName("urlRadio")
    public String urlRadio;
    @SerializedName("description")
    public String description;
}

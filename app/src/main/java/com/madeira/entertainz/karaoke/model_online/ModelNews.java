package com.madeira.entertainz.karaoke.model_online;

import android.net.Uri;
import android.os.AsyncTask;

import com.google.gson.Gson;
import com.madeira.entertainz.karaoke.DBLocal.TNews;
import com.madeira.entertainz.karaoke.DBLocal.TNewsCategory;
import com.madeira.entertainz.karaoke.config.Global;
import com.madeira.entertainz.library.ApiError;
import com.madeira.entertainz.library.PerformAsync2;
import com.madeira.entertainz.library.Util;

import java.util.List;

import static com.madeira.entertainz.karaoke.config.C.DEFAULT_API;

public class ModelNews {
    private static final String TAG = "ModelNews";

    private static final String APIHOST = Global.getApiHostname() + "/v1/news.php";

    public interface FetchCategoryListListener {
        void onSuccess(List<TNewsCategory> list);
        void onError(ApiError e);
    }
    public interface FetchNewsListListener {
        void onSuccess(List<TNews> list);
        void onError(ApiError e);
    }

    public static AsyncTask fetchCategoryList(FetchCategoryListListener callback){

        return PerformAsync2.run(new PerformAsync2.Callback() {
            @Override
            public Object onBackground(PerformAsync2 performAsync) {

                ResultFetchCategoryList r = null;

                try
                {
                    String url = Uri.parse(APIHOST)
                            .buildUpon()
                            .appendQueryParameter("action", "get_category_list")
                            .build().toString();

                    //3 type of error
                    //1. Network error / http error
                    //2. Response error,  json error or blank response
                    //3. Api error

                    String result = Util.readText(url);

                    Gson gson =  new Gson();

                    //parse api error
                    ApiError apiError = ApiError.parseApiError(gson, result);
                    if (apiError!=null) return apiError;

                    //if no error, next convert the real data
                    r = gson.fromJson(result, ResultFetchCategoryList.class);
                }
                catch (Exception e){
                    return ApiError.convert(e);
                }

                return r.list;
            }
        }).setCallbackResult(new PerformAsync2.CallbackResult() {
            @Override
            public void onResult(Object result) {
                if (result instanceof Exception){
                    callback.onError((ApiError) result);
                } else {
                    callback.onSuccess((List<TNewsCategory>) result);
                }
            }
        });
    }

    public static AsyncTask fetchNewsList(int categoryId, int offset, int limit, FetchNewsListListener callback){

        return PerformAsync2.run(new PerformAsync2.Callback() {
            @Override
            public Object onBackground(PerformAsync2 performAsync) {

                ResultFetchNewsList r = null;

                try
                {
                    String url = Uri.parse(APIHOST)
                            .buildUpon()
                            .appendQueryParameter("action", "get_news_list")
                            .appendQueryParameter("news_category_id", String.valueOf(categoryId))
                            .appendQueryParameter("offset", String.valueOf(offset))
                            .appendQueryParameter("limit", String.valueOf(limit))
                            .build().toString();

                    //3 type of error
                    //1. Network error / http error
                    //2. Response error,  json error or blank response
                    //3. Api error

                    String result = Util.readText(url);

                    Gson gson =  new Gson();

                    //parse api error
                    ApiError apiError = ApiError.parseApiError(gson, result);
                    if (apiError!=null) return apiError;

                    //if no error, next convert the real data
                    r = gson.fromJson(result, ResultFetchNewsList.class);
                }
                catch (Exception e){
                    return ApiError.convert(e);
                }

                return r.list;
            }
        }).setCallbackResult(new PerformAsync2.CallbackResult() {
            @Override
            public void onResult(Object result) {
                if (result instanceof Exception){
                    callback.onError((ApiError) result);
                } else {
                    callback.onSuccess((List<TNews>) result);
                }
            }
        });
    }

    class ResultFetchCategoryList {
        public int count;
        public List<TNewsCategory> list;
    }

    class ResultFetchNewsList {
        public int count;
        public List<TNews> list;
    }
}

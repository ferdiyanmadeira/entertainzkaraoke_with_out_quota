package com.madeira.entertainz.karaoke.DBLocal;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Database;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

import java.util.List;


@Database(entities = {TMessage.class, TMessageMedia.class},
        version = 2, exportSchema = false)

public abstract class DbMessage extends RoomDatabase {

    public abstract DaoAccessMessage daoAccessMessage();


    public static class Instance {
        private static final String DB_THEME = "message.db";

        public static DbMessage create(Context context) {
            return Room.databaseBuilder(context, DbMessage.class, DB_THEME)
                    .fallbackToDestructiveMigration()
//                    .addMigrations(MIGRATION_1_2)
                    .build();
        }

        //example migration jangan digunakan dulu, masih tahap research
      /*  static final Migration MIGRATION_1_2 = new Migration(1, 2) {
            @Override
            public void migrate(SupportSQLiteDatabase database) {
                // Your migration strategy here
                database.execSQL("ALTER TABLE Users ADD COLUMN user_score INTEGER");
            }
        };*/
    }

    @Dao
    public interface DaoAccessMessage {

        @Insert(onConflict = OnConflictStrategy.REPLACE)
        void insertAll(List<TMessage> list);

        @Insert(onConflict = OnConflictStrategy.FAIL)
        void insert(TMessage tMessage);

        @Query("SELECT * FROM tmessage order by messageId desc")
        List<TMessage> getAll();

        @Query("DELETE FROM tmessage")
        void deleteAll();

        @Query("UPDATE tmessage SET status=:prmStatus where messageId=:messageId")
        void updateStatusMessage(String prmStatus, int messageId);

        @Insert(onConflict = OnConflictStrategy.REPLACE)
        void insertAllMedia(List<TMessageMedia> list);

        @Query("SELECT * FROM tmessage_image WHERE messageId=:messageId")
        List<TMessageMedia> getAllMedia(int messageId);

        @Query("DELETE FROM tmessage_image")
        void deleteAllMedia();

        @Query("SELECT * FROM tmessage where messageId=:messageId")
        TMessage getMessageById(int messageId);
    }



}
package com.madeira.entertainz.karaoke.model_room_db;

import android.content.Context;

import com.madeira.entertainz.controller.USBControl;
import com.madeira.entertainz.karaoke.DBLocal.DbAccess;
import com.madeira.entertainz.karaoke.DBLocal.DbSong;
import com.madeira.entertainz.karaoke.DBLocal.JoinPlaylistDt;
import com.madeira.entertainz.karaoke.DBLocal.JoinSettingElementMedia;
import com.madeira.entertainz.karaoke.DBLocal.JoinSongCountPlayed;
import com.madeira.entertainz.karaoke.DBLocal.JoinSongPlaylist;
import com.madeira.entertainz.karaoke.DBLocal.TPlaylist;
import com.madeira.entertainz.karaoke.DBLocal.TSongPlaylist;
import com.madeira.entertainz.karaoke.Debug;
import com.madeira.entertainz.library.PerformAsync2;
import com.madeira.entertainz.library.RootUtil;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class ModelPlaylist {
    static String TAG = "ModelElementMedia";

    /**
     * untuk get list Song Playlist
     */
    public interface IGetPlaylistDt {
        void onGetListPlaylistDt(List<JoinPlaylistDt> tPlaylistList);
    }

    public static void getPlaylistDt(Context context, IGetPlaylistDt callback) {
        PerformAsync2.run(performAsync ->
        {
            List<JoinPlaylistDt> result = new ArrayList<>();
            try {
                DbSong dbSong = DbSong.Instance.create(context);
                result = dbSong.daoAccessTPlaylist().joinPlaylist();
                dbSong.close();
            } catch (Exception ex) {
                Debug.e(TAG, ex);
            }
            return result;
        }).
                setCallbackResult(result ->

                {
                    try {
                        List<JoinPlaylistDt> joinPlaylistDtList = (List<JoinPlaylistDt>) result;
                        callback.onGetListPlaylistDt(joinPlaylistDtList);
                    } catch (Exception ex) {
                        Debug.e(TAG, ex);
                    }

                });
    }


    /**
     * untuk get list save Song Playlist
     */
    public interface ISaveSongPlaylist  {
        void onSaveSongPlaylist(int result);
    }

    public static void saveSongPlaylist(Context context, String songId, String playlistName, int playlistId, ISaveSongPlaylist callback) {
        PerformAsync2.run(performAsync ->
                {
                    int result = 0;
                    try {
                        DbSong dbSong = DbSong.Instance.create(context);

                        TPlaylist tPlaylist = new TPlaylist();
                        int lastPlaylistId = 0;
                        if (playlistId == 0) {
                            //cek apakah sudah ada nama playlist yg sama
                            int isExist = dbSong.daoAccessTPlaylist().existPlaylistName(playlistName);
                            if (isExist == 0) {
                                tPlaylist.playlistName = playlistName;
                                dbSong.daoAccessTPlaylist().insert(tPlaylist);
                                lastPlaylistId = dbSong.daoAccessTPlaylist().lastId();
                            } else
                                result = 2;
                        } else {
                            lastPlaylistId = playlistId;
                        }
                        if (lastPlaylistId != 0) {
                            int isExistSong = dbSong.daoAccessTSongPlaylist().existSongPlaylistBySongId(songId, lastPlaylistId);
                            if (isExistSong == 0) {
                                TSongPlaylist tSongPlaylist = new TSongPlaylist();
                                tSongPlaylist.playlistId = lastPlaylistId;
                                tSongPlaylist.songId = songId;
                                dbSong.daoAccessTSongPlaylist().insert(tSongPlaylist);
                                result = 1;
                            } else
                                result = 3;
                        }
                        dbSong.close();
                    } catch (Exception ex) {
                        Debug.e(TAG, ex);
                    }
                    return result;
                }
        ).setCallbackResult(result ->

        {
            try {
                int r = (int) result;
                callback.onSaveSongPlaylist(r);
            } catch (Exception ex) {
                Debug.e(TAG, ex);
            }

        });
    }


    /** untuk get list playlist */
    public interface IGetPlaylist {
        void onGetListPlaylist(List<TPlaylist> tPlaylistList);
    }

    public static void getPlaylist(Context context, IGetPlaylist callback) {
        PerformAsync2.run(performAsync ->
        {
            List<TPlaylist> result = new ArrayList<>();
            try {
                DbSong dbSong = DbSong.Instance.create(context);
                result = dbSong.daoAccessTPlaylist().getAll();
                dbSong.close();
            } catch (Exception ex) {
                Debug.e(TAG, ex);
            }
            return result;
        }).
                setCallbackResult(result ->

                {
                    try {
                        List<TPlaylist> joinPlaylistList = (List<TPlaylist>) result;
                        callback.onGetListPlaylist(joinPlaylistList);
                    } catch (Exception ex) {
                        Debug.e(TAG, ex);
                    }

                });
    }


    /** untuk get list */
    public interface IGetSongPlaylist {
        void onGetListSongPlaylist(List<TSongPlaylist> tSongPlaylistList);
    }

    public static void getSongPlaylist(Context context, IGetSongPlaylist callback) {
        PerformAsync2.run(performAsync ->
        {
            List<TSongPlaylist> result = new ArrayList<>();
            try {
                DbSong dbSong = DbSong.Instance.create(context);
                result = dbSong.daoAccessTSongPlaylist().getAll();
                dbSong.close();
            } catch (Exception ex) {
                Debug.e(TAG, ex);
            }
            return result;
        }).
                setCallbackResult(result ->

                {
                    try {
                        List<TSongPlaylist> tSongPlaylists = (List<TSongPlaylist>) result;
                        callback.onGetListSongPlaylist(tSongPlaylists);
                    } catch (Exception ex) {
                        Debug.e(TAG, ex);
                    }

                });
    }


    /**
     * untuk get list save Playlist
     */
    public interface ISavePlaylist  {
        void onSavePlaylist(int result);
    }

    public static void savePlaylist(Context context, String playlistName, ISavePlaylist callback) {
        PerformAsync2.run(performAsync ->
                {
                    int result = 0;
                    try {
                        DbSong dbSong = DbSong.Instance.create(context);

                        TPlaylist tPlaylist = new TPlaylist();
                        tPlaylist.playlistName = playlistName;
                        dbSong.daoAccessTPlaylist().insert(tPlaylist);
                        result = dbSong.daoAccessTPlaylist().lastId();
                        dbSong.close();
                    } catch (Exception ex) {
                        Debug.e(TAG, ex);
                    }
                    return result;
                }
        ).setCallbackResult(result ->

        {
            try {
                int r = (int) result;
                callback.onSavePlaylist(r);
            } catch (Exception ex) {
                Debug.e(TAG, ex);
            }

        });
    }


    /** untuk get Playlist by id */
    public interface IGetPlaylistById {
        void onGetListPlaylistById(List<JoinSongPlaylist> joinSongPlaylists);
    }

    public static void getPlaylistById(Context context, int playlistId, IGetPlaylistById callback) {
        PerformAsync2.run(performAsync ->
        {
            List<JoinSongPlaylist> result = new ArrayList<>();
            try {
                DbSong dbSong = DbSong.Instance.create(context);
                result = dbSong.daoAccessTSongPlaylist().getByPlaylistId(playlistId);
                dbSong.close();
            } catch (Exception ex) {
                Debug.e(TAG, ex);
            }
            return result;
        }).
                setCallbackResult(result ->

                {
                    try {
                        List<JoinSongPlaylist> joinSongPlaylists = (List<JoinSongPlaylist>) result;
                        callback.onGetListPlaylistById(joinSongPlaylists);
                    } catch (Exception ex) {
                        Debug.e(TAG, ex);
                    }

                });
    }



    /**
     * untuk change order song playlist
     */
    public interface IChangeOrderSongPlaylist  {
        void onChangeOrderSongPlaylist(int result);
    }

    public static void ChangeOrderSongPlaylist(Context context, int playlistId, List<JoinSongPlaylist> list, IChangeOrderSongPlaylist callback) {
        PerformAsync2.run(performAsync ->
                {
                    int result = 0;
                    try {
                        //replace playlist yg di room dgn yg sdh di rubah
                        DbSong dbSong = DbSong.Instance.create(context);

                        //remove semua yg ada di playlist
                        dbSong.daoAccessTSongPlaylist().deleteByPlaylist(playlistId);

                        //reassign id, shg akan berurut
                        List<TSongPlaylist> tSongPlaylists = new ArrayList<>();
                        int id = 1;
                        for (JoinSongPlaylist item: list) {
                        /*item.songPlaylistId = id;
                        id++;*/
                            TSongPlaylist itemSongPlaylist = new TSongPlaylist();
                            itemSongPlaylist.songPlaylistId = id;
                            itemSongPlaylist.songId = item.songId;
                            itemSongPlaylist.playlistId = item.playlistId;
                            id++;
                            tSongPlaylists.add(itemSongPlaylist);
                        }

                        //add list yg sdh di rubah order nya
                        dbSong.daoAccessTSongPlaylist().insertAll(tSongPlaylists);

                        dbSong.close();
                        result =1;
                    } catch (Exception ex) {
                        Debug.e(TAG, ex);
                    }
                    return result;
                }
        ).setCallbackResult(result ->

        {
            try {
                int r = (int) result;
                callback.onChangeOrderSongPlaylist(r);
            } catch (Exception ex) {
                Debug.e(TAG, ex);
            }

        });
    }


    /**
     * untuk Rename Playlist
     */
    public interface IRenamePlaylist  {
        void onRenamePlaylist(int result);
    }

    public static void renamePlaylist(Context context, int playlistId,  String playlistName, IRenamePlaylist callback) {
        PerformAsync2.run(performAsync ->
                {
                    int result = 0;
                    try {
                        DbSong dbAccess = DbSong.Instance.create(context);

                        int isExist = dbAccess.daoAccessTPlaylist().existPlaylistName(playlistName);
                        if (isExist == 0) {
                            dbAccess.daoAccessTPlaylist().renamePlaylist(playlistId, playlistName);
                            result = 1;
                        } else
                            result = 2;
                        dbAccess.close();
                    } catch (Exception ex) {
                        result = 0;
                    }
                    return result;
                }
        ).setCallbackResult(result ->

        {
            try {
                int r = (int) result;
                callback.onRenamePlaylist(r);
            } catch (Exception ex) {
                Debug.e(TAG, ex);
            }

        });
    }


    /**
     * untuk DeleteSong Playlist
     */
    public interface IDeleteSongPlaylist  {
        void onDeleteSongPlaylist(int result);
    }

    public static void deleteSongPlaylist(Context context, int playlistId,  String songId, IDeleteSongPlaylist callback) {
        PerformAsync2.run(performAsync ->
                {
                    int result = 0;
                    try {
                        DbSong dbAccess = DbSong.Instance.create(context);
                        dbAccess.daoAccessTSongPlaylist().delete(songId, playlistId);
                        dbAccess.close();
                        result = 1;
                    } catch (Exception ex) {
                        result = 0;
                    }
                    return result;
                }
        ).setCallbackResult(result ->

        {
            try {
                int r = (int) result;
                callback.onDeleteSongPlaylist(r);
            } catch (Exception ex) {
                Debug.e(TAG, ex);
            }

        });
    }

    /**
     * untuk Delete Playlist
     */
    public interface IDeletePlaylist  {
        void onDeletePlaylist(int result);
    }

    public static void deletePlaylist(Context context, int playlistId,  IDeletePlaylist callback) {
        PerformAsync2.run(performAsync ->
                {
                    int result = 0;
                    try {
                        DbSong dbAccess = DbSong.Instance.create(context);
                        dbAccess.daoAccessTPlaylist().delete(playlistId);
                        dbAccess.daoAccessTSongPlaylist().deleteByPlaylist(playlistId);
                        dbAccess.close();
                        result = 1;
                    } catch (Exception ex) {
                        result = 0;
                    }
                    return result;
                }
        ).setCallbackResult(result ->

        {
            try {
                int r = (int) result;
                callback.onDeletePlaylist(r);
            } catch (Exception ex) {
                Debug.e(TAG, ex);
            }

        });
    }


    public static String syncDeleteSongPlaylistById(List<JoinSongCountPlayed> joinSongCountPlayedList, Context context) {
        String result = "0";
        try {
            USBControl usbControl = new USBControl();
            DbSong dbSong = DbSong.Instance.create(context);
            for (JoinSongCountPlayed joinSongCountPlayed : joinSongCountPlayedList) {
                dbSong.daoAccessTSongPlaylist().deleteBySongId(joinSongCountPlayed.songId);
            }
            dbSong.close();
            result = "1";
            usbControl.writePlaylistFile(context);
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
        return result;
    }




}

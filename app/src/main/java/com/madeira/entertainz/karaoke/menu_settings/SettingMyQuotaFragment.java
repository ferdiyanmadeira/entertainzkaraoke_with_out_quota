package com.madeira.entertainz.karaoke.menu_settings;


import android.content.res.ColorStateList;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.androidnetworking.error.ANError;
import com.madeira.entertainz.helper.RecyclerViewAdapter;
import com.madeira.entertainz.karaoke.Debug;
import com.madeira.entertainz.karaoke.R;
import com.madeira.entertainz.karaoke.config.Global;
import com.madeira.entertainz.karaoke.model_online.ModelPurchase;
import com.madeira.entertainz.library.Util;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class SettingMyQuotaFragment extends Fragment implements SettingsTheme.ISettingsTheme {

    static String TAG ="SettingMyQuotaFragment";
    View root;
    SettingsTheme settingsTheme;

    LinearLayout rootLL;
    TextView totalQuotaTV;
    RecyclerView recyclerView;
    RecyclerViewAdapter adapter;

    List<ModelPurchase.PurchaseHistoryItem> purchaseHistoryItemList = new ArrayList<>();


    public SettingMyQuotaFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        root = inflater.inflate(R.layout.fragment_setting_my_quota, container, false);
        try {
            bind();
            settingsTheme = new SettingsTheme(getActivity(), this);
            settingsTheme.setTheme();
            totalQuotaTV.setText(Util.thousandFormat(Global.getQuota()));
            fetchPurchaseHistory();
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
        return root;
    }

    void bind()
    {
        try {
            rootLL = root.findViewById(R.id.rootLL);
            totalQuotaTV = root.findViewById(R.id.total_quota_tv);
            recyclerView = root.findViewById(R.id.recycler_view);
        }catch (Exception ex)
        {
            Debug.e(TAG, ex);
        }
    }

    @Override
    public void setThemeUpdateDate(Date lastUpdateTheme) {

    }

    @Override
    public void setListColorTheme(int color) {
        rootLL.setBackgroundResource(R.drawable.tags_rounded_corners);

        GradientDrawable categoryDrawable = (GradientDrawable) rootLL.getBackground();
        categoryDrawable.setColor(color);
    }

    @Override
    public void setTextAndSelectionColorTheme(ColorStateList colorSelection, ColorStateList colorText, int colorTextSelected, int colorTextFocus, int colorTextNormal, int colorSelectionSelected, int colorSelectionFocus) {

    }

    /** get purchase history dari server */
    void fetchPurchaseHistory()
    {
        ModelPurchase.getHistoryPurchase(new ModelPurchase.IGetHistoryPurchase() {
            @Override
            public void onGetHistoryPurchase(ModelPurchase.PurchaseHistory result) {
                try
                {
                    if(result!=null)
                    {
                        purchaseHistoryItemList = result.list;
                        setRecyclerView();
//                        adapter.notifyDataSetChanged();
                    }
                }catch (Exception ex)
                {
                    Debug.e(TAG, ex);
                }
            }

            @Override
            public void onError(ANError e) {

            }
        });
    }

    void setRecyclerView() {
        try {
            adapter = new RecyclerViewAdapter(new RecyclerViewAdapter.IRecyclerViewAdapter() {
                @Override
                public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
                    View view = LayoutInflater.from(getActivity()).inflate(R.layout.cell_purchase_history, parent, false);
                    ItemViewHolder item = new ItemViewHolder(view);
                    return item;
                }

                @Override
                public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
                    ItemViewHolder item = (ItemViewHolder) holder;
                    item.bind(purchaseHistoryItemList.get(position), position);
                }

                @Override
                public int getItemCount() {
                    return purchaseHistoryItemList.size();
                }
            });

            recyclerView.setAdapter(adapter);
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    View selectedView;
    int selectedItem;

    class ItemViewHolder extends RecyclerView.ViewHolder {
        private static final String TAG = "ItemViewHolder";

//        View root;
        TextView dateTV;
        TextView packageTV;
        TextView priceTV;
        TextView paymentCodeTV;
        TextView statusTV;
        TextView currencyTV;

        public ItemViewHolder(View itemView) {
            super(itemView);
//            root = itemView;

            dateTV = itemView.findViewById(R.id.date_tv);
            packageTV = itemView.findViewById(R.id.package_tv);
            priceTV = itemView.findViewById(R.id.price_tv);
            paymentCodeTV = itemView.findViewById(R.id.payment_code_tv);
            statusTV = itemView.findViewById(R.id.status_tv);
            currencyTV = itemView.findViewById(R.id.currency_tv);
        }

        public void bind(ModelPurchase.PurchaseHistoryItem purchaseHistoryItem, int pos) {
            try {
                dateTV.setText(purchaseHistoryItem.date);
                String packageString = Util.thousandFormat(Long.valueOf(purchaseHistoryItem.quota));
                packageTV.setText(packageString);
                double priceDouble = Double.valueOf(purchaseHistoryItem.price);
                Long priceLong = (long)priceDouble;
                String price = Util.thousandFormat(priceLong);
                priceTV.setText(price);
                paymentCodeTV.setText(purchaseHistoryItem.transId);
                statusTV.setText(purchaseHistoryItem.status);
                currencyTV.setText(purchaseHistoryItem.currency);

            } catch (Exception ex) {
                Debug.e(TAG, ex);
            }

        }

        void selectView(View v) {
            try {
                //clear selection
                if (selectedView != null) {
                    selectedView.setSelected(false);
                }

                //make selection
                selectedView = v;
                selectedView.setSelected(true);
            } catch (Exception ex) {
                Debug.e(TAG, ex);
            }
        }
    }

}

package com.madeira.entertainz.karaoke;

import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.Process;
import android.util.Log;

import com.androidnetworking.error.ANError;
import com.madeira.entertainz.karaoke.config.Global;
import com.madeira.entertainz.karaoke.model.ModelDebug;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Debug {
    private static final String TAG = "Debug";

    private static final int TYPE_INFO = 100;
    private static final int TYPE_DEBUG = 200;
    private static final int TYPE_ERROR = 300;
    private static final int TYPE_FATAL = 900;

    private static volatile boolean isReady;
    private static Thread thread;
    private static Handler handler;
    private static boolean isBusy;
    private static SimpleDateFormat sdf;
    private static SimpleDateFormat sdfDate;

    private static String pathLog;


    public static void initialize(String pathWork){

        try {
            try
            {
                isBusy = false;
                sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                sdfDate = new SimpleDateFormat("yyyyMMdd");
                pathLog = pathWork;

                thread = new Thread(runMain);
                thread.start();
                //looop sampai isReady == true
                while (isReady==false);

            } catch (Exception e){
                e.printStackTrace();
            }
        } catch (Exception e) {
            Debug.e(TAG, e);
        }
    }

    private static Runnable runMain = new Runnable() {
        @Override
        public void run() {

            try {
                Process.setThreadPriority(Process.THREAD_PRIORITY_BACKGROUND);
                setupHandler();
            } catch (SecurityException e) {
                Debug.e(TAG, e);
            }
        }
    };

    /**
     * Need to call this on the background thread
     */
    private static void setupHandler(){

        try {
            Looper.prepare();

            //all post will received here
            //
            handler = new Handler(new Handler.Callback() {
                @Override
                public boolean handleMessage(Message message) {


                    try {
                        Bundle bundle = message.getData();

                        processLog(message.what, bundle.getString("tag"), bundle.getString("message"));

                        //apabila semua message sdh di proses maka isbusy=false
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            if (handler.getLooper().getQueue().isIdle()){
                                isBusy = false;
                            }
                        }
                    } catch (Exception e) {
                        Debug.e(TAG, e);
                    }

                    return true;
                }
            });

            //we are done here send notify, this will release wait
            isReady = true;

            //this will block
            Looper.loop();
        } catch (Exception e) {
            Debug.e(TAG, e);
        }
    }

    /**
     * This will wait after all the message complete
     */
    public static void sync(){
        //wait while Debug is complete
        try {
            int counter = 100;
            while (Debug.isBusy){
                if (--counter<0) break;
                try {
                    Thread.sleep(100); //check every 100ms
                } catch (InterruptedException e1) {
                    e1.printStackTrace();
                }
            }
        } catch (Exception e) {
            Debug.e(TAG, e);
        }
    }

    private static boolean post(int type, String tag, String message){

        try {
            if (handler==null) return false;

            isBusy=true;

            try
            {
                Bundle data = new Bundle();
                data.putString("tag", tag);
                data.putString("message", message);

                //use formated message for local log file
                Date now = new Date();
                message = sdf.format(now) + " " + tag + " " + message;
                writeToFile(message);

                Message msg = handler.obtainMessage();
                msg.what = type;
                msg.setData(data);
                return handler.sendMessage(msg);
            }
            catch (Exception e){
                e.printStackTrace();
            }
        } catch (Exception e) {
            Debug.e(TAG, e);
        }

        return false;
    }

    static public void i(String tag, String message){
        try {
            Log.i(tag, message);
            post(TYPE_INFO, tag, message);
        } catch (Exception e) {
            Debug.e(TAG, e);
        }
    }

    static public void d(String tag, String message){
        try {
            Log.d(tag, message);
            post(TYPE_DEBUG, tag, message);
        } catch (Exception e) {
            Debug.e(TAG, e);
        }
    }

    static public void f(String tag, Throwable throwable){
        try {
            throwable.printStackTrace();

            try{
                StringWriter writer = new StringWriter();
                PrintWriter printWriter= new PrintWriter(writer);
                throwable.printStackTrace(printWriter);

                post(TYPE_FATAL, tag, writer.toString());
            }catch (Exception e){
                e.printStackTrace();
            }
        } catch (Exception e) {
            Debug.e(TAG, e);
        }
    }

    static public void e(String tag, Exception e){
        try {
            e.printStackTrace();
            int line  = e.getStackTrace()[0].getLineNumber();

            try {
                StringWriter writer = new StringWriter();
                PrintWriter printWriter= new PrintWriter(writer);
                e.printStackTrace(printWriter);

//                post(TYPE_ERROR, tag, writer.toString());

            }catch (Exception ex){
                ex.printStackTrace();
            }
        } catch (Exception e1) {
            Debug.e(TAG, e);
        }
    }

    static public void e(String tag, ANError e){
        try {
            e.printStackTrace();
            int line  = e.getStackTrace()[0].getLineNumber();

            try {
                StringWriter writer = new StringWriter();
                PrintWriter printWriter= new PrintWriter(writer);
                e.printStackTrace(printWriter);

//                post(TYPE_ERROR, tag, writer.toString());

            }catch (Exception ex){
                ex.printStackTrace();
            }
        } catch (Exception e1) {
            Debug.e(TAG, e);
        }
    }

    private static void processLog(int type, String tag, String message){

        try {
            String sessionId = Global.getSessionId();
            String strType = null;

            try{
                switch (type){
                    case TYPE_INFO:
                        strType = "INFO";
                        break;
                    case TYPE_DEBUG:
                        strType = "DEBUG";
                        break;
                    case TYPE_ERROR:
                        strType = "ERROR";
                        break;
                    case TYPE_FATAL:
                        strType = "FATAL";
                        break;
                }

                ModelDebug.addLog(sessionId, strType, tag, message);

            }
            catch (Exception e) {
                e.printStackTrace();
            }
        } catch (Exception e) {
            Debug.e(TAG, e);
        }
    }

    private static void writeToFile(String message){

        try {
            Date now = new Date();
            String filename = pathLog + "/" + sdfDate.format(now) + ".log";

            try {
                BufferedWriter out = new BufferedWriter(new FileWriter(filename, true));
                out.write(message+"\r\n");
                out.close();
            } catch (IOException e) {
            }
        } catch (Exception e) {
            Debug.e(TAG, e);
        }

    }
}

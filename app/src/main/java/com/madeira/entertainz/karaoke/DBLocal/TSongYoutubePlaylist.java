package com.madeira.entertainz.karaoke.DBLocal;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.util.Log;

import com.google.gson.annotations.SerializedName;

import java.util.List;

@Entity(tableName = "TSongYoutubePlaylist")
public class TSongYoutubePlaylist {

    @PrimaryKey(autoGenerate = true)
    @SerializedName("songPlaylistId")
    public int songPlaylistId;
    @SerializedName("playlistId")
    public int playlistId;
    @SerializedName("songId")
    public String songId;
    @SerializedName("duration")
    public int duration;

    @SerializedName("songName")
    public String songName;

    @SerializedName("artist")
    public String artist;

    @SerializedName("songURL")
    public String songURL;

    @SerializedName("vocalSound")
    public int vocalSound;

    @SerializedName("thumbnail")
    public String thumbnail;

    public String lyric;

    public int formServer;

    public int ord; //ini ordinal di pakai utk change order

    public static int getIndexBySongId(List<TSongYoutubePlaylist> list, String songId){
        //set default -1; jika index tidak ditemukan

        Log.i("TSongYoutubePlaylist", "getIndexBySongId: " + songId);

        for(int idx=0; idx<list.size(); idx++)
        {
            TSongYoutubePlaylist item = list.get(idx);

            if(item.songId.equals(songId))
            {
                return idx;
            }
        }
        return -1;
    }
}

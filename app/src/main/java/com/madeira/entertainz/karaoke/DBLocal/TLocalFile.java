package com.madeira.entertainz.karaoke.DBLocal;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;

@Entity(tableName = "tlocalfile")
public class TLocalFile {

    @PrimaryKey
    @SerializedName("id")
    public int id;
    @SerializedName("fileType")
    public String fileType;
    @SerializedName("fileName")
    public String fileName;
    @SerializedName("contentType")
    public String contentType;
    @SerializedName("content")
    public String content;
}

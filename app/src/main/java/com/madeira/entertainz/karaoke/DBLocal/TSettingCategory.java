package com.madeira.entertainz.karaoke.DBLocal;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;

@Entity(tableName = "tsettingcategory")
public class TSettingCategory {

    @PrimaryKey
    @SerializedName("settingCategoryId")
    public int settingCategoryId;
    @SerializedName("settingCategory")
    public String settingCategory;
}

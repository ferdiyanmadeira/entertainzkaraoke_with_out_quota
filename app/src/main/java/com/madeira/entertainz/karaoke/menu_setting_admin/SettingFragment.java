package com.madeira.entertainz.karaoke.menu_setting_admin;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.madeira.entertainz.karaoke.BuildConfig;
import com.madeira.entertainz.karaoke.Debug;
import com.madeira.entertainz.karaoke.MyDeviceAdminReceiver;
import com.madeira.entertainz.karaoke.R;
import com.madeira.entertainz.karaoke.config.C;
import com.madeira.entertainz.karaoke.config.Global;
import com.madeira.entertainz.karaoke.menu_main.MainActivity;
import com.madeira.entertainz.karaoke.model.ModelInstaller;
import com.madeira.entertainz.library.MacUtils;
import com.madeira.entertainz.library.Util;
import com.madeira.entertainz.library.UtilDeviceOwner;

public class SettingFragment extends Fragment {
    private static final String TAG = "SettingFragment";

    private static final String BUNDLE_ADMIN_SESSIONID = "BUNDLE_ADMIN_SESSIONID";
    private static final String BUNDLE_SALT = "BUNDLE_SALT";

    ISettingFragment callback;

    EditText editIp;
    EditText editMacWifi;
    EditText editMacEth;
    EditText editSessionId;
    EditText editRoom;
    EditText editAppId;
    EditText editVersion;
    EditText editHost;

    Button btnRestart;
    Button btnAndroidSettings;
    Button btnCreateSession;
    Button btnFileManager;
    Button btnToogleKioskMode;
    Button resetHostAPIButton;

    String adminSessionId;
    String salt;
    boolean isKioskModeEnble;

    interface ISettingFragment {
        void onReload();
    }

    public SettingFragment() {
    }

    public static SettingFragment newInstance(FragmentActivity fragmentActivity, int viewId,
                                              String adminSessionId, String salt,
                                              ISettingFragment callback) {

        SettingFragment fragment = new SettingFragment();
        fragment.callback = callback;
        Util.setFragment(fragmentActivity, fragment, viewId);

        Bundle bundle = new Bundle();
        bundle.putString(BUNDLE_ADMIN_SESSIONID, adminSessionId);
        bundle.putString(BUNDLE_SALT, salt);
        fragment.setArguments(bundle);

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_setting_admin, container, false);
        try {
            Bundle bundle = this.getArguments();
            adminSessionId = (String) bundle.get(BUNDLE_ADMIN_SESSIONID);
            salt = (String) bundle.get(BUNDLE_SALT);

            bind(v);

            setupButton();
            setupEdit();

            updateUi();
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }

        return v;
    }

    void bind(View v) {
        try {
            btnRestart = v.findViewById(R.id.button_restart);
            btnAndroidSettings = v.findViewById(R.id.button_android_settings);
            btnCreateSession = v.findViewById(R.id.button_create_session);

            editIp = v.findViewById(R.id.edit_ip);
            editMacWifi = v.findViewById(R.id.edit_mac_wifi);
            editMacEth = v.findViewById(R.id.edit_mac_eth);
            editRoom = v.findViewById(R.id.edit_room);
            editSessionId = v.findViewById(R.id.edit_sessionid);
            editAppId = v.findViewById(R.id.edit_app_id);
            editVersion = v.findViewById(R.id.edit_version);
            editHost = v.findViewById(R.id.edit_host);
            btnFileManager = v.findViewById(R.id.button_file_manager);
            btnToogleKioskMode = v.findViewById(R.id.button_toggle_kiosk);
            resetHostAPIButton = v.findViewById(R.id.button_reset_host);

        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    void updateUi() {

        try {
            String ip4 = MacUtils.getIPAddress(true);
            String macWifi = MacUtils.getMACAddress("wlan0");
            String macEth = MacUtils.getMACAddress("eth0");

            if (macWifi.isEmpty()) macWifi = "-";
            if (macEth.isEmpty()) macEth = "-";

            editIp.setText(ip4);
            editMacWifi.setText(macWifi);
            editMacEth.setText(macEth);
            editSessionId.setText(Global.getSessionId());
            editAppId.setText(BuildConfig.APPLICATION_ID + " (" + BuildConfig.BUILD_TYPE + ")");
            editVersion.setText(BuildConfig.VERSION_NAME + " (" + BuildConfig.VERSION_CODE + ")");
            editHost.setText(Global.getApiHostname());

            isKioskModeEnble = UtilDeviceOwner.getDeviceOwnerState(BuildConfig.APPLICATION_ID, getActivity());
            if (isKioskModeEnble) {
                btnToogleKioskMode.setText("Disable Device Owner");
            } else {
                btnToogleKioskMode.setText("Enable Device Owner");
            }
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    void setupButton() {

        try {
            btnRestart.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    callback.onReload();
                }
            });

            btnAndroidSettings.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 0);
                }
            });

            btnCreateSession.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    DialogSelectStb dlg = DialogSelectStb.newInstance(getActivity(), adminSessionId, salt);

              /*  dlg.setCallback(new DialogSelectStb.IDialogRequestSessionFragment() {
                    @Override
                    public void onItemSelected(ModelInstaller.ResultGetStbPartial.Stb stb) {
                        Log.i(TAG, "stbId="+stb.stbId);
                        createSession(stb);
                    }
                });*/

                }
            });

            btnFileManager.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent launchIntent = getActivity().getPackageManager().getLaunchIntentForPackage("com.android.rockchip");
                    if (launchIntent != null) {
                        startActivity(launchIntent);//null pointer check in case package name was not found
                    }
                }
            });

            btnToogleKioskMode.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        if (isKioskModeEnble) {
                            UtilDeviceOwner.removeDeviceOwner(BuildConfig.APPLICATION_ID, MyDeviceAdminReceiver.class);
                            Global.setDeviceOwnerConfig(false);
                        } else {
                            UtilDeviceOwner.enableDeviceOwner(BuildConfig.APPLICATION_ID, MyDeviceAdminReceiver.class);
                            UtilDeviceOwner.enableKioskMode(getActivity(), BuildConfig.APPLICATION_ID, MainActivity.class, MyDeviceAdminReceiver.class);
                            Global.setDeviceOwnerConfig(true);
                        }
                        updateUi();
                    } catch (Exception e) {
                        Debug.e(TAG, e);
                    }
                }
            });

            resetHostAPIButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        editHost.setText(C.DEFAULT_API);
                        Global.setApiHostname(C.DEFAULT_API);
                    } catch (Exception ex) {
                        Debug.e(TAG, ex);
                    }

                }
            });
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    void setupEdit() {

        try {
            //ini untuk handle user pecnet di soft keyboard
            editHost.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                @Override
                public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                    if (actionId == EditorInfo.IME_ACTION_DONE) {
                        Global.setApiHostname(String.valueOf(editHost.getText()));
                        return true;
                    }
                    return false;
                }
            });

            //yg ini untk handle apabila user pencent di physicall keyboard
            editHost.setOnKeyListener(new View.OnKeyListener() {
                @Override
                public boolean onKey(View v, int keyCode, KeyEvent event) {

                    if ((event.getAction() == KeyEvent.ACTION_DOWN) && (keyCode == KeyEvent.KEYCODE_ENTER)) {
                        Global.setApiHostname(String.valueOf(editHost.getText()));
                        return true;
                    }
                    return false;
                }
            });
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    void createSession(ModelInstaller.ResultGetStbPartial.Stb stb) {

        ModelInstaller.createSession(adminSessionId, salt, stb.stbId, new ModelInstaller.ICreateSession() {
            @Override
            public void onCreateSession(String stbSessionId) {
                Log.i(TAG, "onCreateSession=" + stbSessionId);
                Global.setSessionId(stbSessionId);
                editSessionId.setText(stbSessionId);

                editRoom.setText(stb.roomName + "/" + stb.stbName + "  " + stb.location);
            }

            @Override
            public void onError(Exception e) {
                Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_LONG).show();
            }
        });

    }
}

package com.madeira.entertainz.karaoke.model_room_db;

import android.content.Context;

import com.madeira.entertainz.karaoke.DBLocal.DbAccess;
import com.madeira.entertainz.karaoke.DBLocal.JoinSettingElementMedia;
import com.madeira.entertainz.karaoke.Debug;
import com.madeira.entertainz.library.PerformAsync2;

import java.util.ArrayList;
import java.util.List;

public class ModelElementMedia {
    static String TAG = "ModelElementMedia";

    /**
     * untuk get list Element Media
     */
    public interface IGetListElementMediaActive {
        void onGetListElementMedia(List<JoinSettingElementMedia> joinSettingElementMediaList);
    }

    public static void getListElementMediaActive(Context context, IGetListElementMediaActive callback) {
        PerformAsync2.run(performAsync ->
        {
            List<JoinSettingElementMedia> result = new ArrayList<>();
            try {
                DbAccess dbAccess = DbAccess.Instance.create(context);
                result = dbAccess.daoAccessThemeMedia().joinSettingElementMediaActive();
                dbAccess.close();
            } catch (Exception ex) {
                Debug.e(TAG, ex);
            }
            return result;
        }).
                setCallbackResult(result ->

                {
                    try {
                        List<JoinSettingElementMedia> tStickerList = (List<JoinSettingElementMedia>) result;
                        callback.onGetListElementMedia(tStickerList);
                    } catch (Exception ex) {
                        Debug.e(TAG, ex);
                    }

                });
    }

    /**
     * untuk get list Element Media
     */
    public interface IGetListElementMedia {
        void onGetListElementMedia(List<JoinSettingElementMedia> joinSettingElementMediaList);
    }

    public static void getListElementMedia(Context context, IGetListElementMedia callback) {
        PerformAsync2.run(performAsync ->
        {
            List<JoinSettingElementMedia> result = new ArrayList<>();
            try {
                DbAccess dbAccess = DbAccess.Instance.create(context);
                result = dbAccess.daoAccessThemeMedia().joinSettingElementMedia();
                dbAccess.close();
            } catch (Exception ex) {
                Debug.e(TAG, ex);
            }
            return result;
        }).
                setCallbackResult(result ->

                {
                    try {
                        List<JoinSettingElementMedia> tStickerList = (List<JoinSettingElementMedia>) result;
                        callback.onGetListElementMedia(tStickerList);
                    } catch (Exception ex) {
                        Debug.e(TAG, ex);
                    }

                });
    }


    /**
     * untuk Update setting media
     */
    public interface IUpdateSettingMedia {
        void onUpdateSettingMedia(int result);
    }

    public static void updateSettingMedia(Context context, int mediaId, int isActive, IUpdateSettingMedia callback) {
        PerformAsync2.run(performAsync ->
                {
                    int result = 0;
                    try {
                        DbAccess dbAccess = DbAccess.Instance.create(context);
                        dbAccess.daoAccessThemeMedia().updateSettingMedia(mediaId, isActive);
                        dbAccess.close();
                        result = 1;
                    } catch (Exception ex) {
                        Debug.e(TAG, ex);
                    }
                    return result;
                }
        ).setCallbackResult(result ->

        {
            try {
                int r = (int) result;
                callback.onUpdateSettingMedia(r);
            } catch (Exception ex) {
                Debug.e(TAG, ex);
            }

        });
    }

    /**
     * untuk UpdateDuration setting media
     */
    public interface IUpdateDurationSettingMedia {
        void onUpdateDurationSettingMedia(int result);
    }

    public static void updateDurationSettingMedia(Context context, int duration, IUpdateDurationSettingMedia callback) {
        PerformAsync2.run(performAsync ->
                {
                    int result = 0;
                    try {
                        DbAccess dbAccess = DbAccess.Instance.create(context);
                        dbAccess.daoAccessThemeMedia().updateAllDuration(duration);
                        dbAccess.close();
                        result = 1;
                    } catch (Exception ex) {
                        Debug.e(TAG, ex);
                    }
                    return result;
                }
        ).setCallbackResult(result ->

        {
            try {
                int r = (int) result;
                callback.onUpdateDurationSettingMedia(r);
            } catch (Exception ex) {
                Debug.e(TAG, ex);
            }

        });
    }

}

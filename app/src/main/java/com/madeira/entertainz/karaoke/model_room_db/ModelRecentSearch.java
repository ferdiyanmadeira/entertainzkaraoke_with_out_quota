package com.madeira.entertainz.karaoke.model_room_db;

import android.content.Context;

import com.madeira.entertainz.karaoke.DBLocal.DbRecentSearchSong;
import com.madeira.entertainz.karaoke.DBLocal.DbSong;
import com.madeira.entertainz.karaoke.DBLocal.JoinSongCountPlayed;
import com.madeira.entertainz.karaoke.DBLocal.TRecentSearchSong;
import com.madeira.entertainz.karaoke.DBLocal.TSongCategory;
import com.madeira.entertainz.karaoke.Debug;
import com.madeira.entertainz.karaoke.config.C;
import com.madeira.entertainz.library.PerformAsync2;

import java.util.ArrayList;
import java.util.List;

public class ModelRecentSearch {
    static String TAG = "ModelRecentSearch";


    /**
     * untuk add recent search
     */
    public interface IAddRecentSearch {
        void onAddRecentSearch(int result);
    }

    public static void addRecentSearch(Context context, String songName, int category, IAddRecentSearch callback) {
        PerformAsync2.run(performAsync ->
        {
            int result = 0;
            try {
                DbRecentSearchSong dbRecentSearchSong = DbRecentSearchSong.Instance.create(context);
                List<TRecentSearchSong> tRecentSearchSongs = dbRecentSearchSong.daoAccessRecentSearchSong().getAll(category);

                //delete recent id satu persatu yang lebih dari 50
                for (int i = 50; i < tRecentSearchSongs.size(); i++) {
                    dbRecentSearchSong.daoAccessRecentSearchSong().deleteById(tRecentSearchSongs.get(i).recentId);
                }

                TRecentSearchSong tRecentSearchSong = dbRecentSearchSong.daoAccessRecentSearchSong().checkSong(songName, category);
                if (tRecentSearchSong != null) {
                    //hapus recent terakhir yang menggunakan song name ini, dan create ulang
                    dbRecentSearchSong.daoAccessRecentSearchSong().deleteById(tRecentSearchSong.recentId);
                }
                TRecentSearchSong addTRecentSearchSong = new TRecentSearchSong();
                addTRecentSearchSong.recentSong = songName;
                addTRecentSearchSong.category = category;
                dbRecentSearchSong.daoAccessRecentSearchSong().insert(addTRecentSearchSong);
                dbRecentSearchSong.close();
                result = 1;
            } catch (Exception ex) {
                Debug.e(TAG, ex);
            }
            return result;
        }).
                setCallbackResult(result ->

                {
                    try {
                        int r = (int) result;
                        callback.onAddRecentSearch(r);
                    } catch (Exception ex) {
                        Debug.e(TAG, ex);
                    }

                });
    }


    /**
     * untuk get list recent Song
     */
    public interface IGetListRecentSong {
        void onGetListRecentSong(List<TRecentSearchSong> tRecentSearchSongList);
    }

    public static void getRecentSong(Context context, int category, IGetListRecentSong callback) {
        PerformAsync2.run(performAsync ->
        {
            List<TRecentSearchSong> result = new ArrayList<>();
            try {
                DbRecentSearchSong dbRecentSearchSong = DbRecentSearchSong.Instance.create(context);
                result = dbRecentSearchSong.daoAccessRecentSearchSong().getAll(category);
                dbRecentSearchSong.close();
            } catch (Exception ex) {
                Debug.e(TAG, ex);
            }
            return result;
        }).
                setCallbackResult(result ->

                {
                    try {
                        List<TRecentSearchSong> tRecentSearchSongs = (List<TRecentSearchSong>) result;
                        callback.onGetListRecentSong(tRecentSearchSongs);
                    } catch (Exception ex) {
                        Debug.e(TAG, ex);
                    }

                });
    }


}

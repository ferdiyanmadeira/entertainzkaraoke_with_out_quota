package com.madeira.entertainz.karaoke;

import android.app.Application;
import android.content.Context;
import android.net.IpSecManager;
import android.os.Handler;
import android.util.Log;

import com.google.gson.Gson;
import com.madeira.entertainz.karaoke.DBLocal.TConfig;
import com.madeira.entertainz.karaoke.config.C;
import com.madeira.entertainz.karaoke.config.Global;
import com.madeira.entertainz.karaoke.DBLocal.DbAccess;
import com.madeira.entertainz.library.MacUtils;
import com.madeira.entertainz.library.Util;
import com.madeira.entertainz.library.UtilTime;
import com.madeira.library.RootUtil;
import com.madeira.mrkeyboard.Helper;

import java.io.File;
import java.util.logging.ConsoleHandler;

import static com.madeira.entertainz.karaoke.config.C.DEFAULT_API;

public class MainApplication extends Application {
    private static final String TAG = "MainApplication";

    public static Context context;

    private DbAccess dbAccess;

    private BackgroundThread mainBackground;

    android.os.Handler handler = new Handler();
    Runnable runnable;
    int periode = 1000;

    @Override
    public void onCreate() {
        super.onCreate();

        try {
            //set system wide crash catch
            //
            if (Util.isEmulator() == false) {
                Thread.setDefaultUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {
                    @Override
                    public void uncaughtException(Thread thread, Throwable ex) {

//                    FirebaseCrash.report(ex);
//                    Exception e = new Exception(ex);
//                    Crashlytics.logException(e);
//                    if (BuildConfig.DEBUG) {
//                        ErrorActivity.startActivity(AppMain.getContext(), ex);
//                    }
                        Log.e(TAG, "Catch: ", ex);

                        System.exit(0);
                    }
                });
            }


            MainApplication.context = this;

            String address = MacUtils.getMACAddress("eth0");
            Log.i("macadress", "" + address);

            //set api hostname utk development, nanti saat final code ini harus di hapus
            Global.setSessionId("SESSIONID");

            String apiHostName = Global.getApiHostname();
            if (apiHostName.equals(""))
                Global.setApiHostname(DEFAULT_API);

            //start background thread
            mainBackground = new BackgroundThread(this);
            mainBackground.setSessionId(Global.getSessionId());


            //install mrkeyboard
            Helper.installKeyboard(BuildConfig.APPLICATION_ID);
            readConfig();
//            resetPreferredLauncherAndOpenChooser(this);
            clearDumpFiles();

            //ini service utk EKMA
            showMgmtService();
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    void showMgmtService() {
        runnable = new Runnable() {
            @Override
            public void run() {
                try {
                    /**check apakah device connect dengan network device, jika connect baru jalankan service untuk ekma*/
                    boolean connected = Util.isConnectedNetwork(getBaseContext());
                    if (connected)
                        MgmtService.startService(getBaseContext());
                    else
                        handler.postDelayed(runnable, periode);

                } catch (Exception ex) {
                    Debug.e(TAG, ex);
                }
            }
        };

        handler.postDelayed(runnable, periode);
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mainBackground.stop();
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
        mainBackground.stop();
    }


    void readConfig() {
        try {
            String str;
            String strPath = C.FULLPATH_MR + "/" + C.FILE_CONFIG;

            File f = new File(strPath);
            if (f.exists() == false) return;

            str = Util.readTextFile(strPath);

            //apabila tdk bisa hapus normal, maka lewat su
            if (Util.deleteFile(strPath) == false) RootUtil.rm(strPath);

            Log.i(TAG, "readConfig\n" + str);

            //exit apabila tdk ada file nya
            if (str.length() == 0) return;

            try {
                Gson gson = new Gson();

                TConfig config = gson.fromJson(str, TConfig.class);

                if (config.timeZone != null) UtilTime.setTimezone(context, config.timeZone);
                if (config.quota != 0) Global.setQuota(config.quota);

            } catch (Exception e) {
                e.printStackTrace();
            }
        } catch (Exception e) {
            Debug.e(TAG, e);
        }
    }


    /** set default launcher menjadi app ini *//*
    public static void resetPreferredLauncherAndOpenChooser(Context context) {
        try {
            PackageManager packageManager = context.getPackageManager();
            ComponentName componentName = new ComponentName(context, FakeLauncherActivity.class);
            packageManager.setComponentEnabledSetting(componentName, PackageManager.COMPONENT_ENABLED_STATE_ENABLED, PackageManager.DONT_KILL_APP);

            Intent selector = new Intent(Intent.ACTION_MAIN);
            selector.addCategory(Intent.CATEGORY_HOME);
            selector.addCategory(Intent.CATEGORY_LAUNCHER);
            selector.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(selector);

            packageManager.setComponentEnabledSetting(componentName, PackageManager.COMPONENT_ENABLED_STATE_DEFAULT, PackageManager.DONT_KILL_APP);
        }
        catch (Exception ex)
        {
            Debug.e(TAG, ex);
        }
    }*/

    /**
     * untuk clear file song yang gagal di download
     */
    void clearDumpFiles() {

        try {
            String appFileDir = getFilesDir().toString();
            // go to your directory
            File fileList = new File(appFileDir);

            //check if dir is not null
            if (fileList != null) {

                // so we can list all files
                File[] filenames = fileList.listFiles();

                // loop through each file and delete
                for (File tmpf : filenames) {
                    tmpf.delete();
                }
            }
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }


}

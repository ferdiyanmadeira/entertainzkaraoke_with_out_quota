package com.madeira.entertainz.karaoke.menu_update_song;

import android.content.Context;
import android.content.res.ColorStateList;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.ScaleAnimation;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.davidecirillo.multichoicerecyclerview.MultiChoiceAdapter;
import com.madeira.entertainz.karaoke.DBLocal.TLocalFile;
import com.madeira.entertainz.karaoke.DBLocal.TSongItemServer;
import com.madeira.entertainz.karaoke.Debug;
import com.madeira.entertainz.karaoke.R;
import com.madeira.entertainz.karaoke.config.C;
import com.madeira.entertainz.karaoke.config.CacheData;
import com.madeira.entertainz.karaoke.config.Global;
import com.madeira.entertainz.karaoke.model_online.ModelSong;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.List;

public class UpdateSongViewAdapter extends MultiChoiceAdapter<UpdateSongViewAdapter.SampleCustomViewHolder> {
    //kenapa dibuat terpisah dari Recyclerviewadapter, karena entah kenapa item dibagian paling bawah dari recylerview akan selalu hilang ketika diclick

    String TAG="UpdateSongViewAdapter";

    public interface IUpdateSongViewAdapter{
        void onAddItem(TSongItemServer songItem);
        void onDeleteItem(TSongItemServer songItem);
    }

    IUpdateSongViewAdapter callBack;

    private final List<TSongItemServer> songItemArrayList;
    private final Context mContext;
    ColorStateList colorSelection, colorText;

    public UpdateSongViewAdapter(List<TSongItemServer> songItemArrayList, Context context, ColorStateList colorSelection, ColorStateList colorText, IUpdateSongViewAdapter callBack) {
        this.songItemArrayList = songItemArrayList;
        this.mContext = context;
        this.colorSelection = colorSelection;
        this.colorText = colorText;
        this.callBack = callBack;
    }

    @Override
    public SampleCustomViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new SampleCustomViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.cell_updatesong_list, parent, false));
    }

    @Override
    public void onBindViewHolder(SampleCustomViewHolder holder, int position) {
        super.onBindViewHolder(holder, position);

        TSongItemServer songItem = songItemArrayList.get(position);

        holder.tvTitle.setText(songItem.songName);
        holder.tvSinger.setText(songItem.artist);
        if (songItem.thumbnail != null) {
            Picasso.with(mContext)
                    .load(songItem.thumbnail).placeholder(R.drawable.blank_thumbnail)
                    .fit()
                    .into(holder.thumbnailIV);
        }
        String[] fileNameSplit = songItem.songPath.split("/");
        String fileName = fileNameSplit[fileNameSplit.length - 1];
        String pathSTB ="";
//      String pathSTB = Global.getUSBPath() + "/" + C.PATH_VIDEO_KARAOKE + "/" + fileName;
        TLocalFile tLocalFile = CacheData.hashFile.get(fileName);
        if(tLocalFile!=null)
            pathSTB = tLocalFile.content;
        File file = new File(pathSTB);
        if (file.exists()) {
            int file_size = Integer.parseInt(String.valueOf(file.length()/1024));
            int serverFileSize = Integer.valueOf(songItem.filesize);
            if(file_size==serverFileSize) {
                holder.isExistIV.setVisibility(View.VISIBLE);
                CacheData.hashSongNameNArtis.put(songItem.songId, songItem);
            }
        } else
            holder.isExistIV.setVisibility(View.GONE);



        if (colorSelection != null) holder.root.setBackgroundTintList(colorSelection);
//            untuk merubah warna text
        if (colorText != null) {
            holder.tvTitle.setTextColor(colorText);
            holder.tvSinger.setTextColor(colorText);
        }

        holder.root.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    updateSelectedViewUi(v, true);
                } else {
                    updateSelectedViewUi(null, false);

                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return songItemArrayList.size();
    }

    @Override
    public void setActive(@NonNull View view, boolean state) {

       /* ImageView imageView = (ImageView) view.findViewById(R.id.imageView);
        RelativeLayout relativeLayout = (RelativeLayout) view.findViewById(R.id.container);
        if (state) {
            relativeLayout.setBackgroundColor(ContextCompat.getColor(mContext, R.color.colorBackgroundLight));
            imageView.setBackgroundColor(ContextCompat.getColor(mContext, R.color.colorPrimaryDark));
        } else {
            relativeLayout.setBackgroundColor(ContextCompat.getColor(mContext, android.R.color.transparent));
            imageView.setBackgroundColor(ContextCompat.getColor(mContext, android.R.color.transparent));
        }*/
    }

    @Override
    protected View.OnClickListener defaultItemViewClickListener(SampleCustomViewHolder holder, final int position) {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TSongItemServer songItem = songItemArrayList.get(position);
//                Toast.makeText(mContext, "Click on item " + position, Toast.LENGTH_SHORT).show();
                boolean isAdd = CacheData.hashSongNameNArtis.containsKey(songItem.songId);
                if (!isAdd) {
//                    addNewSong.add(songItem);
                    callBack.onAddItem(songItem);
                    holder.isExistIV.setVisibility(View.VISIBLE);
                    CacheData.hashSongNameNArtis.put(songItem.songId, songItem);

                } else {
                    callBack.onDeleteItem(songItem);
//                        addNewSong.removeIf(x -> (x.songName == songItem.songName && x.artist == songItem.artist));
                        CacheData.hashSongNameNArtis.remove(songItem.songId);
                        holder.isExistIV.setVisibility(View.GONE);
                }
            }
        };
    }

    class SampleCustomViewHolder extends RecyclerView.ViewHolder {

        View root;
        TextView tvTitle;
        TextView tvSinger;
        ImageView thumbnailIV;
        ImageView isExistIV;

        SampleCustomViewHolder(View itemView) {
            super(itemView);

            root = itemView;
            selectedView = root;
            tvTitle = itemView.findViewById(R.id.text_title);
            tvSinger = itemView.findViewById(R.id.text_singer);
            thumbnailIV = itemView.findViewById(R.id.thumbnailIV);
            isExistIV = itemView.findViewById(R.id.isExistIV);
        }
    }

    void updateSelectedViewUi(View view, boolean hasFocus) {
        try {
            if (selectedView != null) {
                scaleView(selectedView, 1.1f, 1);
                ViewCompat.setTranslationZ(selectedView, 0);
            }

            if (hasFocus) {
                selectedView = view;
                ViewCompat.setTranslationZ(selectedView, 100);
//                selectedView.setBackgroundResource(R.drawable.box_white);
                scaleView(selectedView, 1, 1.1f);
            } else {
                //remove selected & focus dari grid
                selectedView = null;
            }
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    public void scaleView(View v, float startScale, float endScale) {
        try {
            Animation anim = new ScaleAnimation(
                    startScale, endScale, // Start and end values for the X axis scaling
                    startScale, endScale, // Start and end values for the Y axis scaling
                    Animation.RELATIVE_TO_SELF, 0.5f, // Pivot point of X scaling
                    Animation.RELATIVE_TO_SELF, 0.5f); // Pivot point of Y scaling
            anim.setFillAfter(true); // Needed to keep the result of the animation
            anim.setDuration(100);
            v.startAnimation(anim);
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    static View selectedView;
}
package com.madeira.entertainz.karaoke.DBLocal;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

@Entity(tableName = "TDownloadYoutube")
public class TDownloadYoutube implements Serializable {

    @PrimaryKey @NonNull
    @SerializedName("youtube_id")
    public String songId;
    @SerializedName("songName")
    public String songName;
    @SerializedName("artist")
    public String artist;
    @SerializedName("genre_id")
    public int songCategoryId;
    @SerializedName("songCategory")
    public String songCategory;
    @SerializedName("duration")
    public int duration;
    @SerializedName("thumbnail")
    @Nullable
    public String thumbnail;
    @SerializedName("url_video")
    public String urlVideo;
    @SerializedName("url_sound")
    public String urlSound;
}

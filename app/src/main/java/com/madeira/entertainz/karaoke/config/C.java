package com.madeira.entertainz.karaoke.config;

public class C {
    static public final boolean FOR_HOTEL = false;
    static public final boolean FG_SYNC_QUOTA = false;
    //todo perlu dipisah, karena untuk melakukan pengecekan connection ke server
    static public final String ONLY_DOMAIN = "entertainzkaraoke.com";
    //todo untuk sementara menggunakan http karena, ssl di server entah kenapa tidak jalan di STB
    static public final String DOMAIN = "http://" + ONLY_DOMAIN;
    static public final String DOMAIN_ROOT = DOMAIN + "/ekb";
    static public final String DEFAULT_API = DOMAIN_ROOT + "/api";
    static public final String ESHOPING_URL = DOMAIN + "/eshopping";

    static public final int LIMIT_SONG_IN_FOLDER = 10;


//    static public final int DEFAULT_QUOTA = 2000;
    static public final long DEFAULT_QUOTA = 999999999;

    public static int ELEMENT_MEDIA_DURATION = 1000 * 60 * 2;

    //theme Main
    static public final int ELEMENT_MAIN_BACKGROUND_1 = 10;

    //Theme main
    static public final int THEME_MAIN_BACKGROUND = 101;
    static public final int THEME_MAIN_LIST_BACKGROUND = 1010;
    static public final int THEME_MAIN_TEXTCOLOR_SELECTED = 1011;
    static public final int THEME_MAIN_TEXTCOLOR_NORMAL = 1012;
    static public final int THEME_MAIN_TEXTCOLOR_FOCUS = 1017;
    static public final int THEME_MAIN_LIST_SELECTION_SELECTED = 1013;
    static public final int THEME_MAIN_LIST_SELECTION_FOCUS = 1014;
    static public final int THEME_MAIN_HEADER_BACKGROUND = 1015;
    static public final int THEME_MAIN_FOOTER_BACKGROUND = 1016;

    //    theme karaoke
    static public final int THEME_KARAOKE_LIST_BACKGROUND = 1018;
    static public final int THEME_KARAOKE_LIST_SELECTION_SELECTED = 1019;
    static public final int THEME_KARAOKE_LIST_SELECTION_FOCUS = 1020;
    static public final int THEME_KARAOKE_TEXTCOLOR_SELECTED = 1021;
    static public final int THEME_KARAOKE_TEXTCOLOR_NORMAL = 1022;
    static public final int THEME_KARAOKE_TEXTCOLOR_FOCUS = 1023;
    static public final int THEME_KARAOKE_HEADER_BACKGROUND = 1030;

    //    theme playlist
    static public final int THEME_PLAYLIST_LIST_BACKGROUND = 1024;
    static public final int THEME_PLAYLIST_LIST_SELECTION_SELECTED = 1025;
    static public final int THEME_PLAYLIST_LIST_SELECTION_FOCUS = 1026;
    static public final int THEME_PLAYLIST_TEXTCOLOR_SELECTED = 1027;
    static public final int THEME_PLAYLIST_TEXTCOLOR_NORMAL = 1028;
    static public final int THEME_PLAYLIST_TEXTCOLOR_FOCUS = 1029;

    //    theme UPDATE SONG
    static public final int THEME_UPDATESONG_LIST_BACKGROUND = 1031;
    static public final int THEME_UPDATESONG_LIST_SELECTION_SELECTED = 1032;
    static public final int THEME_UPDATESONG_LIST_SELECTION_FOCUS = 1033;
    static public final int THEME_UPDATESONG_TEXTCOLOR_SELECTED = 1034;
    static public final int THEME_UPDATESONG_TEXTCOLOR_NORMAL = 1035;
    static public final int THEME_UPDATESONG_TEXTCOLOR_FOCUS = 1036;

    //    theme Accessories
    static public final int THEME_ACCESSORIES_LIST_BACKGROUND = 1037;
    static public final int THEME_ACCESSORIES_LIST_SELECTION_SELECTED = 1038;
    static public final int THEME_ACCESSORIES_LIST_SELECTION_FOCUS = 1039;
    static public final int THEME_ACCESSORIES_TEXTCOLOR_SELECTED = 1040;
    static public final int THEME_ACCESSORIES_TEXTCOLOR_NORMAL = 1041;
    static public final int THEME_ACCESSORIES_TEXTCOLOR_FOCUS = 1042;

    //    theme MESSAGES
    static public final int THEME_MESSAGES_LIST_BACKGROUND = 1043;
    static public final int THEME_MESSAGES_LIST_SELECTION_SELECTED = 1044;
    static public final int THEME_MESSAGES_LIST_SELECTION_FOCUS = 1045;
    static public final int THEME_MESSAGES_TEXTCOLOR_SELECTED = 1046;
    static public final int THEME_MESSAGES_TEXTCOLOR_NORMAL = 1047;
    static public final int THEME_MESSAGES_TEXTCOLOR_FOCUS = 1048;

    //    theme SETTING
    static public final int THEME_SETTING_LIST_BACKGROUND = 1049;
    static public final int THEME_SETTING_LIST_SELECTION_SELECTED = 1050;
    static public final int THEME_SETTING_LIST_SELECTION_FOCUS = 1051;
    static public final int THEME_SETTING_TEXTCOLOR_SELECTED = 1052;
    static public final int THEME_SETTING_TEXTCOLOR_NORMAL = 1053;
    static public final int THEME_SETTING_TEXTCOLOR_FOCUS = 1054;

    //    theme RADIO
    static public final int THEME_RADIO_LIST_BACKGROUND = 1055;
    static public final int THEME_RADIO_LIST_SELECTION_SELECTED = 1056;
    static public final int THEME_RADIO_LIST_SELECTION_FOCUS = 1057;
    static public final int THEME_RADIO_TEXTCOLOR_SELECTED = 1058;
    static public final int THEME_RADIO_TEXTCOLOR_NORMAL = 1059;
    static public final int THEME_RADIO_TEXTCOLOR_FOCUS = 1060;


    //constant utk shared pref

    // message
    static public final String SHARED_PREFERENCES_HAVE_UNREAD_MESSAGE = "HAVE_UNREAD_MESSAGE";

    static public final String SHARED_PREFERENCES_SESSIONID = "SESSIONID";
    static public final String SHARED_PREFERENCES_API_HOSTNAME = "API_HOSTNAME";
    static public final String SHARED_PREFERENCES_USB_PATH = "USB_PATH";
    static public final String SHARED_PREFERENCES_TITLEAPP = "TITLEAPP";
    static public final String SHARED_PREFERENCES_PASSWORDSETTING = "PASSWORDSETTING";
    static public final String SHARED_PREFERENCES_CONNECTED_USB = "CONNECTED_USB";

    static public final String SHARED_PREFERENCES_FIRST_BAND = "FIRST_BAND";
    static public final String SHARED_PREFERENCES_SECONDS_BAND = "SECONDS_BAND";
    static public final String SHARED_PREFERENCES_THIRD_BAND = "THIRD_BAND";
    static public final String SHARED_PREFERENCES_FORTH_BAND = "FORTH_BAND";
    static public final String SHARED_PREFERENCES_FIVE_BAND = "FIVE_BAND";

    static public final String SHARED_PREFERENCES_LAST_UPDATE = "LAST_UPDATE";

    static public final String SHARED_PREFERENCES_PATH_UPDATE = "PATH_UPDATE";

    static public final String SHARED_PREFERENCES_PATH_EKB = "PATH_EKB";
    //element media
    static public final String SHARED_PREFERENCES_DURATION_ELEMENT_MEDIA = "DURATION_ELEMENT_MEDIA";

    //sn menggunakan shared preference agar lebih mudah dalam pemanggilan SN nya
    static public final String SHARED_PREFERENCES_SN = "SN";

    //untuk parameter purchase quota
    static public final String SHARED_PREFERENCES_NAME = "PREF_NAME";
    static public final String SHARED_PREFERENCES_EMAIL = "PREF_EMAIL";


    //timezone
    static public final String SHARED_PREFERENCES_TIME_ZONE = "PREF_TIME_ZONE";

    static public final String SHARED_PREFERENCES_PREV_VERSION = "PREF_PREV_VERSION";
    static public final String SHARED_PREFERENCES_NEXT_VERSION = "PREF_NEXT_VERSION";


    //event apabila ada message yg belum di baca
    public static final int EVENT_UNREAD_MESSAGE = 10001;

    static public final String PIN_FOR_SETTING = "9119";

    static public final String MESSAGE_STATUS_NEW = "NEW";
    static public final String MESSAGE_STATUS_UNREAD = "UNREAD";
    static public final String MESSAGE_STATUS_READ = "READ";

    //itemsList main menu
    public static final int MENU_SEARCH_SONG = 1000;
    public static final int MENU_PLAYLIST = 1100;
    public static final int MENU_UPDATE_SONG = 1200;
    public static final int MENU_ACCESSORIES = 1300;
    public static final int MENU_MESSAGES = 1500;
    public static final int MENU_SETTING = 1400;
    public static final int MENU_LIVE_CHANNEL = 1600;
    public static final int MENU_YOUTUBE = 1700;
    public static final int MENU_YOUTUBE_KARAOKE = 1701;
    public static final int MENU_VIDIO = 1800;
    public static final int MENU_RADIO = 1900;
    public static final int MENU_YOUTUBE_PLAYLIST = 2000;
    public static final int MENU_NEWS = 2100;

    //itemlist live channel
    public static final int MENU_LIVE_CHANNEL_TV = 1000;
    public static final int MENU_LIVE_CHANNEL_RADIO = 1001;


    //json filename
    public static final String JSON_THEME = "theme.json";
    public static final String JSON_SONG = "Song.json";
    public static final String JSON_SONG_DEC = "Song.json.dec";
    public static final String JSON_SONG_CATEGORY = "SongCategory.json";
    public static final String JSON_MESSAGE = "message.json";
    public static final String JSON_STICKER = "Sticker.json";
    public static final String JSON_RADIO = "Radio.json";
    public static final String JSON_SONG_YOUTUBE = "SongYoutube.json";
    public static final String JSON_SONG_YOUTUBE_CATEGORY = "SongYoutubeCategory.json";
    public static final String JSON_STB_QUOTA = "quota.json";
    //    public static final String JSON_STB_KEY = "key.json";
    public static final String JSON_STB_INFO = "info.json";
    //    public static final String JSON_STB_QUOTA_DEC = "quota.json.dec";
    public static final String JSON_PLAYLIST = "Playlist.json";
    public static final String JSON_SONG_PLAYLIST = "SongPlaylist.json";
    public static final String JSON_YOUTUBE_PLAYLIST = "YoutubePlaylist.json";
    public static final String JSON_SONG_YOUTUBE_PLAYLIST = "SongYoutubePlaylist.json";

    //default password setting
    public static final String default_password_setting = "123123";

    public final static String SHARED_PREFERENCES_LANGUAGEID = "LANGUAGEID";
    public final static String SHARED_PREFERENCES_DURATION = "DURATION";
    public final static String SHARED_PREFERENCES_STICKERID_USED = "STICKERID_USED";

    public static final String SHARED_PREFERENCES_DEVICE_OWNER_CONFIG = "DEVICE_OWNER_CONFIG";
    public static final String SHARED_PREFERENCES_QUOTA = "QUOTA";
    public static final String SHARED_PREFERENCES_NEXT_CODE_NAME = "NEXT_CODE_NAME";

    //satuan byte
    public final static int SIZE_ENCRYPTION_FILE = 32;
//    public final static int SIZE_ENCRYPTION_FILE = 320000;


    //base 64
    public final static String ENCYPTION_KEY = "YXNkZmc=";

    public final static String YOUTUBE_ID = "com.google.android.youtube.tv";
    public static final String APPLICATION_ID_FILEMANAGER = "com.android.rockchip";
    public static final String APPLICATION_ID_VIDIO = "com.vidio.android";


    //limit
    public final static int LIMIT_COUNT_SONG = 20000;

    public final static String PATH_CONFIG = "/storage/emulated/0/Entertainz";


    //time to sync ke server
    public final static int INTERVAL_SERVER_SYNC = 1000 * 60 * 5;


    //path folder ekb
    public final static String PATH_JSON = "JSON";
    public final static String PATH_IMAGE = "IMAGE";
    public final static String PATH_VIDEO = "VIDEO";
    public final static String PATH_EKB_APK = PATH_JSON + "/" + "APK";
    public final static String PATH_JSON_STB = PATH_JSON + "/" + "STB";
    public final static String PATH_JSON_KARAOKE = PATH_JSON + "/" + "Karaoke";
    public final static String PATH_VIDEO_KARAOKE = PATH_VIDEO + "/" + "Karaoke";
    public final static String PATH_IMAGE_KARAOKE = PATH_IMAGE + "/" + "Karaoke";


    //flag untuk mengetahui apakah ada song update list
    public static boolean FG_UPDATE_SONG = false;


    //intent filter untuk download song
    public final static String BOTTOM_NAVIGATION_FRAGMENT = "com.madeira.entertainz.karaoke.menu_main.BottomNavigationFragment";
    public final static String BOTTOM_NAVIGATION_FRAGMENT_FINISH_DOWNLOAD = "com.madeira.entertainz.karaoke.menu_main.BottomNavigationFragment.FinishDownload";
    public final static String MAIN_ACTIVITY_FINISH_DOWNLOAD = "com.madeira.entertainz.karaoke.menu_main.MainActivity.FinishDownload";
    public final static String BOT_NAV_YOUTUBE_DOWNLOAD= "com.madeira.entertainz.karaoke.menu_main.BottomNavigationFragment.YoutubeDownload";
    public final static String BOT_NAV_YOUTUBE_DOWNLOAD_FINISH = "com.madeira.entertainz.karaoke.menu_main.BottomNavigationFragment.YoutubeDownload.FinishDownload";



   /* public final static String BOTTOM_NAVIGATION_FRAGMENT_UPDATE_APK = "com.madeira.entertainz.karaoke.menu_main.BottomNavigationFragment.UpdateAPK";
    public final static String BOTTOM_NAVIGATION_FRAGMENT_FINISH_DOWNLOAD_UPDATE_APK = "com.madeira.entertainz.karaoke.menu_main.BottomNavigationFragment.UpdateAPK.FinishDownload";*/
   public final static String BOTTOM_NAVIGATION_FRAGMENT_UPDATE_APK = "BottomNavigationFragment.UpdateAPK";
    public final static String BOTTOM_NAVIGATION_FRAGMENT_FINISH_DOWNLOAD_UPDATE_APK = "BottomNavigationFragment.UpdateAPK.FinishDownload";
    public final static String CONNECT_WITH_MANAGEMENT_SERVICE = "com.madeira.entertainz.karaoke.MgmtService";


    // untuk config pertama, quota pertama kali beli device dan timezone
    static public final String FULLPATH_MR = "/data/mr";
    static public final String FILE_CONFIG = "config.json";


    //limit coba ulang rescan usb
    static public final int LIMIT_RESCAN_USB = 3;


    //datetime format room
    static public final String TIME_STAMP_FORMAT = "yyyy-MM-dd HH:mm:ss";


    //untuk kategori recent search
    static public final int SEARCH_SONG_KARAOKE = 1;
    static public final int SEARCH_SONG_YOUTUBE = 2;
    static public final int SEARCH_SONG_YOUTUBE_KARAOKE = 3;


    //untuk limit fetch online data
    public final static int LIMIT_FETCH_DATA = 50;


    // duration change time
    public final static int changeTimeInterval = 1000;
    public final static int oneSecondInterval = 1000;

    public final static int checkConnectionInterval = 10000;

    //one hour interval in cron services
    public final static  int cronInterval = 1000 * 60 * 60;

    //untuk menghindari double progress download apk
    static public boolean UPDATING_APK = false;

}


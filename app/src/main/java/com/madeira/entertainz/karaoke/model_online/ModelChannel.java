package com.madeira.entertainz.karaoke.model_online;

import android.net.Uri;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.OkHttpResponseAndJSONObjectRequestListener;
import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;
import com.madeira.entertainz.karaoke.Debug;
import com.madeira.entertainz.karaoke.config.Global;
import com.madeira.entertainz.library.Util;

import org.json.JSONObject;

import java.io.Serializable;
import java.util.List;

import okhttp3.Response;

import static com.madeira.entertainz.karaoke.config.C.DEFAULT_API;

public class ModelChannel {
    private static final String TAG = "ModelChannel";

    private static final String APIHOST = Global.getApiHostname() + "/v1/channel.php";

    public interface IGetChannel {
        void onGetSTBInfo(channelItem result);

        void onError(ANError e);
    }
    static public void getChannel(IGetChannel callBack) {
        String mac = Util.getMACAddress("eth0");
        mac = mac.replace(":", "");
        try {
            String uri = Uri.parse(APIHOST)
                    .buildUpon()
                    .appendQueryParameter("action", "get_channel")
                    .appendQueryParameter("mac", mac)
                    .build().toString();

            AndroidNetworking.get(uri)
                    .setPriority(Priority.MEDIUM)
                    .build()
                    .getAsOkHttpResponseAndJSONObject(new OkHttpResponseAndJSONObjectRequestListener() {
                        @Override
                        public void onResponse(Response okHttpResponse, JSONObject response) {
                            try {
                                if (okHttpResponse.isSuccessful()) {
                                    Gson gson = new Gson();
                                    String jsonString = response.toString();
                                    channelItem stbItem = gson.fromJson(jsonString, channelItem.class);
                                    callBack.onGetSTBInfo(stbItem);
                                }
                            } catch (Exception ex) {

                            }
                        }

                        @Override
                        public void onError(ANError anError) {
                            callBack.onError(anError);
                        }
                    });
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }


    public class channelItem {
        @SerializedName("list_channel")
        public List<subChannelItem> list;
    }

    public static class subChannelItem implements Serializable {
        @SerializedName("channelId")
        public String channelId;
        @SerializedName("channel_name")
        public String channelName;
        @SerializedName("url_channel")
        public String urlChannel;
        @SerializedName("category_id")
        public String categoryId;
        @SerializedName("category_name")
        public String categoryname;
    }

}

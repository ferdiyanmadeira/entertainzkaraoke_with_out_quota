package com.madeira.entertainz.karaoke.model_room_db;

import android.content.Context;

import com.madeira.entertainz.karaoke.DBLocal.DbSong;
import com.madeira.entertainz.karaoke.DBLocal.DbYoutube;
import com.madeira.entertainz.karaoke.DBLocal.JoinSongPlaylist;
import com.madeira.entertainz.karaoke.DBLocal.JoinSongYoutubePlaylist;
import com.madeira.entertainz.karaoke.DBLocal.JoinYoutubePlaylistDt;
import com.madeira.entertainz.karaoke.DBLocal.TSongYoutube;
import com.madeira.entertainz.karaoke.DBLocal.TSongYoutubePlaylist;
import com.madeira.entertainz.karaoke.DBLocal.TYoutubePlaylist;
import com.madeira.entertainz.karaoke.Debug;
import com.madeira.entertainz.library.PerformAsync2;
import com.madeira.entertainz.library.VarDump;

import java.util.ArrayList;
import java.util.List;

public class ModelYoutubePlaylist {
    static String TAG = "ModelYoutubePlaylist";


    /** untuk get list playlist */
    public interface IGetJoinPlaylist {
        void onGetListPlaylist(List<JoinYoutubePlaylistDt> joinYoutubePlaylistDts);
    }

    public static void getJoinPlaylist(Context context, IGetJoinPlaylist callback) {
        PerformAsync2.run(performAsync ->
        {
            List<JoinYoutubePlaylistDt> tPlaylistList = new ArrayList<>();
            try {
                DbYoutube dbYoutube = DbYoutube.Instance.create(context);
                tPlaylistList = dbYoutube.daoAccessTYoutubePlaylist().joinPlaylist();
                dbYoutube.close();
            } catch (Exception ex) {

            }
            return tPlaylistList;
        }).
                setCallbackResult(result ->

                {
                    try {
                        List<JoinYoutubePlaylistDt> joinYoutubePlaylistDts = (List<JoinYoutubePlaylistDt>) result;
                        callback.onGetListPlaylist(joinYoutubePlaylistDts);
                    } catch (Exception ex) {
                        Debug.e(TAG, ex);
                    }

                });
    }


    /** untuk get list playlist */
    public interface IGetPlaylist {
        void onGetListPlaylist(List<TYoutubePlaylist> tYoutubePlaylistList);
    }

    public static void getPlaylist(Context context, IGetPlaylist callback) {
        PerformAsync2.run(performAsync ->
        {
            List<TYoutubePlaylist> tPlaylistList = new ArrayList<>();
            try {
                DbYoutube dbYoutube = DbYoutube.Instance.create(context);
                tPlaylistList = dbYoutube.daoAccessTYoutubePlaylist().getAll();
                dbYoutube.close();
            } catch (Exception ex) {

            }
            return tPlaylistList;
        }).
                setCallbackResult(result ->

                {
                    try {
                        List<TYoutubePlaylist> YoutubePlaylistDts = (List<TYoutubePlaylist>) result;
                        callback.onGetListPlaylist(YoutubePlaylistDts);
                    } catch (Exception ex) {
                        Debug.e(TAG, ex);
                    }

                });
    }


    /** untuk get list */
    public interface IGetSongPlaylist {
        void onGetListSongPlaylist(List<TSongYoutubePlaylist> tSongPlaylistList);
    }

    /**
     * Versi sync dari getSongPlaylist
     *
     * func ini di pakai juga saat simpan TSongYoutubePlaylist --> file
     *
     * @param context
     * @return
     */
    public static List<TSongYoutubePlaylist> syncGetSongPlaylist(Context context){
        List<TSongYoutubePlaylist> result = new ArrayList<>();
        try {
            DbYoutube dbYoutube = DbYoutube.Instance.create(context);
            result = dbYoutube.daoAccessTSongYoutubePlaylist().getAll();
            dbYoutube.close();
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
        return result;
    }

    public static void getSongPlaylist(Context context, IGetSongPlaylist callback) {
        PerformAsync2.run(performAsync ->
        {
            return syncGetSongPlaylist(context);

//            di pindahkan ke function syncGetSongPlaylist, shg nanti bisa di pakai utk simpan ke usb
//            List<TSongYoutubePlaylist> result = new ArrayList<>();
//            try {
//                DbYoutube dbYoutube = DbYoutube.Instance.create(context);
//                result = dbYoutube.daoAccessTSongYoutubePlaylist().getAll();
//                dbYoutube.close();
//            } catch (Exception ex) {
//                Debug.e(TAG, ex);
//            }
//            return result;
        }).
                setCallbackResult(result ->

                {
                    try {
                        List<TSongYoutubePlaylist> tSongYoutubePlaylists = (List<TSongYoutubePlaylist>) result;
                        callback.onGetListSongPlaylist(tSongYoutubePlaylists);
                    } catch (Exception ex) {
                        Debug.e(TAG, ex);
                    }

                });
    }

    /**
     * untuk get list save Song Playlist
     */
    public interface ISaveSongPlaylist  {
        void onSaveSongPlaylist(int result);
    }

    public static void saveSongPlaylist(Context context, TSongYoutube song, String playlistName, int playlistId, ISaveSongPlaylist callback) {
        PerformAsync2.run(performAsync ->
                {
                    int result = 0;
                    try {
                        DbYoutube dbYoutube = DbYoutube.Instance.create(context);

                        TYoutubePlaylist tPlaylist = new TYoutubePlaylist();
                        int lastPlaylistId = 0;
                        if (playlistId == 0) {
                            //cek apakah sudah ada nama playlist yg sama
                            int isExist = dbYoutube.daoAccessTYoutubePlaylist().existPlaylistName(playlistName);
                            if (isExist == 0) {
                                tPlaylist.playlistName = playlistName;
                                dbYoutube.daoAccessTYoutubePlaylist().insert(tPlaylist);
                                lastPlaylistId = dbYoutube.daoAccessTYoutubePlaylist().lastId();
                            } else
                                result = 2;
                        } else {
                            lastPlaylistId = playlistId;
                        }
                        if (lastPlaylistId != 0) {
                            int isExistSong = dbYoutube.daoAccessTSongYoutubePlaylist().exisTSongYoutubePlaylistBySongId(song.songId, lastPlaylistId);
                            if (isExistSong == 0) {
                                TSongYoutubePlaylist tSongYoutubePlaylist = new TSongYoutubePlaylist();
                                tSongYoutubePlaylist.playlistId = lastPlaylistId;
                                tSongYoutubePlaylist.songId = song.songId;
                                tSongYoutubePlaylist.artist = song.artist;
                                tSongYoutubePlaylist.duration = song.duration;
                                tSongYoutubePlaylist.lyric = song.lyric;
                                tSongYoutubePlaylist.songName = song.songName;
                                tSongYoutubePlaylist.songURL = song.songURL;
                                tSongYoutubePlaylist.formServer = song.fromServer;
                                tSongYoutubePlaylist.thumbnail = song.thumbnail;

                                dbYoutube.daoAccessTSongYoutubePlaylist().insert(tSongYoutubePlaylist);
                                result = 1;
                            } else
                                result = 3;
                        }
                        dbYoutube.close();
                    } catch (Exception ex) {
                        Debug.e(TAG, ex);
                    }
                    return result;
                }
        ).setCallbackResult(result ->

        {
            try {
                int r = (int) result;
                callback.onSaveSongPlaylist(r);
            } catch (Exception ex) {
                Debug.e(TAG, ex);
            }

        });
    }


    /**
     * untuk get list save Playlist
     */
    public interface ISavePlaylist  {
        void onSavePlaylist(int result);
    }

    public static void savePlaylist(Context context, String playlistName, ISavePlaylist callback) {
        PerformAsync2.run(performAsync ->
                {
                    int result = 0;
                    try {
                        DbYoutube dbYoutube = DbYoutube.Instance.create(context);

                        TYoutubePlaylist tYoutubePlaylist = new TYoutubePlaylist();
                        tYoutubePlaylist.playlistName = playlistName;
                        dbYoutube.daoAccessTYoutubePlaylist().insert(tYoutubePlaylist);
                        result = dbYoutube.daoAccessTYoutubePlaylist().lastId();
                        dbYoutube.close();
                    } catch (Exception ex) {
                        Debug.e(TAG, ex);
                    }
                    return result;
                }
        ).setCallbackResult(result ->

        {
            try {
                int r = (int) result;
                callback.onSavePlaylist(r);
            } catch (Exception ex) {
                Debug.e(TAG, ex);
            }

        });
    }


    /** untuk get song Playlist by id */
    public interface IGetSongPlaylistById {
        void onGetListPlaylistById(List<JoinSongPlaylist> joinSongPlaylists);
    }

    public static void getSongPlaylistById(Context context, int playlistId, IGetSongPlaylistById callback) {
        PerformAsync2.run(performAsync ->
        {
            List<JoinSongPlaylist> result = new ArrayList<>();
            try {
                DbSong dbSong = DbSong.Instance.create(context);
                result = dbSong.daoAccessTSongPlaylist().getByPlaylistId(playlistId);
                dbSong.close();
            } catch (Exception ex) {
                Debug.e(TAG, ex);
            }
            return result;
        }).
                setCallbackResult(result ->

                {
                    try {
                        List<JoinSongPlaylist> joinSongPlaylists = (List<JoinSongPlaylist>) result;
                        callback.onGetListPlaylistById(joinSongPlaylists);
                    } catch (Exception ex) {
                        Debug.e(TAG, ex);
                    }

                });
    }



    /**
     * untuk change order song playlist
     */
    public interface IChangeOrderSongPlaylist  {
        void onChangeOrderSongPlaylist(int result);
    }

//    public static void ChangeOrderSongPlaylist(Context context, int playlistId, List<JoinSongYoutubePlaylist> list, IChangeOrderSongPlaylist callback) {
//        PerformAsync2.run(performAsync ->
//                {
//                    int result = 0;
//                    try {
//                        //replace playlist yg di room dgn yg sdh di rubah
//                        DbYoutube dbYoutube = DbYoutube.Instance.create(context);
//
//                        //remove semua yg ada di playlist
//                        dbYoutube.daoAccessTSongYoutubePlaylist().deleteByPlaylist(playlistId);
//
//                        //reassign id, shg akan berurut
//                        List<TSongYoutubePlaylist> tSongYoutubePlaylists = new ArrayList<>();
//                        int id = 1;
//                        for (JoinSongYoutubePlaylist item : list) {
//                            TSongYoutubePlaylist tSongYoutubePlaylist = new TSongYoutubePlaylist();
//                            tSongYoutubePlaylist.songPlaylistId = id;
//                            tSongYoutubePlaylist.playlistId = item.playlistId;
//                            tSongYoutubePlaylist.songId = item.songId;
//                            tSongYoutubePlaylists.add(tSongYoutubePlaylist);
//                            id++;
//                        }
//
//                        //add list yg sdh di rubah order nya
//                        dbYoutube.daoAccessTSongYoutubePlaylist().insertAll(tSongYoutubePlaylists);
//
//                        dbYoutube.close();
//                        result =1;
//                    } catch (Exception ex) {
//                        Debug.e(TAG, ex);
//                    }
//                    return result;
//                }
//        ).setCallbackResult(result ->
//
//        {
//            try {
//                int r = (int) result;
//                callback.onChangeOrderSongPlaylist(r);
//            } catch (Exception ex) {
//                Debug.e(TAG, ex);
//            }
//
//        });
//    }

    public static void changeOrderSongPlaylist(Context context, int playlistId, List<TSongYoutubePlaylist> list, IChangeOrderSongPlaylist callback) {
        PerformAsync2.run(performAsync ->
                {
                    int result = 0;
                    try {

                        //reassign id, shg akan berurut
                        int ord = 1;
                        for (TSongYoutubePlaylist item : list) {
                            item.ord = ord; //ord ini yg menentukan order dari lagu
                            ord++;
                        }

                        DbYoutube dbYoutube = DbYoutube.Instance.create(context);

                        //update list yg sdh di rubah order nya
                        dbYoutube.daoAccessTSongYoutubePlaylist().insertAll(list); //func insert juga sbg update

                        dbYoutube.close();
                        result =1;
                    } catch (Exception ex) {
                        Debug.e(TAG, ex);
                    }
                    return result;
                }
        ).setCallbackResult(result ->

        {
            try {
                int r = (int) result;
                callback.onChangeOrderSongPlaylist(r);
            } catch (Exception ex) {
                Debug.e(TAG, ex);
            }

        });
    }

    /** untuk get list join by id */
    public interface IGetJoinSongPlaylistById {
        void onGetListSongPlaylist(List<JoinSongYoutubePlaylist> tSongPlaylistList);
    }

    public static void getJoinSongPlaylistById(Context context, int playlistId, IGetJoinSongPlaylistById callback) {
        PerformAsync2.run(performAsync ->
        {
            List<TSongYoutubePlaylist> result = new ArrayList<>();
            try {
                DbYoutube dbYoutube = DbYoutube.Instance.create(context);
                result = dbYoutube.daoAccessTSongYoutubePlaylist().getByPlaylistId(playlistId);
                dbYoutube.close();
            } catch (Exception ex) {
                Debug.e(TAG, ex);
            }
            return result;
        }).
                setCallbackResult(result ->

                {
                    try {
                        List<JoinSongYoutubePlaylist> tSongYoutubePlaylists = (List<JoinSongYoutubePlaylist>) result;
                        callback.onGetListSongPlaylist(tSongYoutubePlaylists);
                    } catch (Exception ex) {
                        Debug.e(TAG, ex);
                    }

                });
    }

    /** untuk get list by id*/
    public interface IGetSongPlaylistByPlaylistId {
        void onGetListSongPlaylist(List<TSongYoutubePlaylist> tSongPlaylistList);
    }

    public static void getSongPlaylistByPlaylistId(Context context, int playlistId, IGetSongPlaylist callback) {
        PerformAsync2.run(performAsync ->
        {
            List<TSongYoutubePlaylist> result = new ArrayList<>();
            try {
                DbYoutube dbYoutube = DbYoutube.Instance.create(context);
                result = dbYoutube.daoAccessTSongYoutubePlaylist().getByPlaylistId(playlistId);
                dbYoutube.close();
            } catch (Exception ex) {
                Debug.e(TAG, ex);
            }
            return result;
        }).
                setCallbackResult(result ->

                {
                    try {
                        List<TSongYoutubePlaylist> tSongYoutubePlaylists = (List<TSongYoutubePlaylist>) result;
                        callback.onGetListSongPlaylist(tSongYoutubePlaylists);
                    } catch (Exception ex) {
                        Debug.e(TAG, ex);
                    }

                });
    }

    /**
     * untuk  Rename Playlist
     */
    public interface IRenamePlaylist  {
        void onRenamePlaylist(int result);
    }

    public static void renamePlaylist(Context context, String playlistName, int playlistId, IRenamePlaylist callback) {
        PerformAsync2.run(performAsync ->
                {
                    int result = 0;
                    try {
                        DbYoutube dbYoutube = DbYoutube.Instance.create(context);

                        int isExist = dbYoutube.daoAccessTYoutubePlaylist().existPlaylistName(playlistName);
                        if (isExist == 0) {
                            dbYoutube.daoAccessTYoutubePlaylist().renamePlaylist(playlistId, playlistName);
                            result = 1;
                        } else
                            result = 2;
                        dbYoutube.close();
                    } catch (Exception ex) {
                        Debug.e(TAG, ex);
                    }
                    return result;
                }
        ).setCallbackResult(result ->

        {
            try {
                int r = (int) result;
                callback.onRenamePlaylist(r);
            } catch (Exception ex) {
                Debug.e(TAG, ex);
            }

        });
    }


    /**
     * untuk  DeleteSong Playlist
     */
    public interface IDeleteSongPlaylist  {
        void onDeleteSongPlaylist(int result);
    }

    public static void deleteSongPlaylist(Context context, int playlistId, String songId, IDeleteSongPlaylist callback) {
        PerformAsync2.run(performAsync ->
                {
                    int result = 0;
                    try {
                        DbYoutube dbYoutube = DbYoutube.Instance.create(context);
                        dbYoutube.daoAccessTSongYoutubePlaylist().delete(playlistId, songId);
                        dbYoutube.close();
                        result = 1;
                    } catch (Exception ex) {
                        Debug.e(TAG, ex);
                    }
                    return result;
                }
        ).setCallbackResult(result ->

        {
            try {
                int r = (int) result;
                callback.onDeleteSongPlaylist(r);
            } catch (Exception ex) {
                Debug.e(TAG, ex);
            }

        });
    }


    /**
     * untuk  delete Playlist
     */
    public interface IDeletePlaylist  {
        void onDeletePlaylist(int result);
    }

    public static void deletePlaylist(Context context, int playlistId, IDeletePlaylist callback) {
        PerformAsync2.run(performAsync ->
                {
                    int result = 0;
                    try {
                        DbYoutube dbYoutube = DbYoutube.Instance.create(context);
                        dbYoutube.daoAccessTYoutubePlaylist().delete(playlistId);
                        dbYoutube.daoAccessTSongYoutubePlaylist().deleteByPlaylist(playlistId);
                        dbYoutube.close();
                        result = 1;
                    } catch (Exception ex) {
                        Debug.e(TAG, ex);
                    }
                    return result;
                }
        ).setCallbackResult(result ->

        {
            try {
                int r = (int) result;
                callback.onDeletePlaylist(r);
            } catch (Exception ex) {
                Debug.e(TAG, ex);
            }

        });
    }



}

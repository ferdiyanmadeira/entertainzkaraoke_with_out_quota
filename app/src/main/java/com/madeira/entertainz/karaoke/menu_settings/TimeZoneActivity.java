package com.madeira.entertainz.karaoke.menu_settings;

import android.app.Activity;
import android.content.Intent;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.madeira.entertainz.controller.STBControl;
import com.madeira.entertainz.helper.RecyclerViewAdapter;
import com.madeira.entertainz.karaoke.Debug;
import com.madeira.entertainz.karaoke.EKMAConnectedActivity;
import com.madeira.entertainz.karaoke.R;
import com.madeira.entertainz.karaoke.config.C;
import com.madeira.entertainz.karaoke.config.Global;
import com.madeira.entertainz.karaoke.menu_main.MainActivity;
import com.madeira.entertainz.karaoke.menu_main.MenuFragment;
import com.madeira.entertainz.library.Util;
import com.madeira.entertainz.library.UtilTime;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class TimeZoneActivity extends AppCompatActivity {

    static final String TAG = "TimeZoneActivity";

    RecyclerView recyclerView;
    List<String> timeZoneList = new ArrayList<>();
    RecyclerViewAdapter adapter;
    TextView timeView;
    TextView timeZoneView;
    ITimeZone callback;
    Handler handler = new Handler();
    Runnable runnable;
    STBControl stbControl = new STBControl();
    EditText searchET;

    public interface ITimeZone extends Serializable {
        public void onChangeTimeZone(String timeZone);
    }


    public static void startActivity(Activity activity, ITimeZone iTimeZone) {
        try {
            TimeZoneActivity timeZoneActivity = new TimeZoneActivity();
            Intent intent = new Intent(activity, timeZoneActivity.getClass());
            intent.putExtra("interface", iTimeZone);
            activity.startActivity(intent);
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_time_zone);
        bind();
        setActionObject();
        Intent intent = getIntent();
        callback = (ITimeZone) intent.getSerializableExtra("interface");
        Util.hideNavigationBar(this);
        setRecyclerView();
        String tz = Global.getTimeZone();
        timeZoneView.setText(tz);
        showDateTime();
    }

    void bind() {
        try {
            recyclerView = findViewById(R.id.recycler_view);
            timeView = findViewById(R.id.time_view);
            timeZoneView = findViewById(R.id.timezone_view);
            searchET = findViewById(R.id.searchET);
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }


    void setActionObject() {
        try {
            searchET.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View v, boolean hasFocus) {
                    try {
                        if (!hasFocus) {
//                            Util.closeKeyboard(getActivity());
                            searchET.setHintTextColor(getResources().getColor(R.color.texthintcolor_unfocus));
                            String keyword = searchET.getText().toString();
                        } else {
                            Util.showKeyboard(TimeZoneActivity.this);
//                            searchET.setHintTextColor(colorTextNormal);
                        }
                    } catch (Exception ex) {
                        Debug.e(TAG, ex);
                    }

                }
            });

            searchET.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {
                    searchTimeZone(s.toString());
                }
            });
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    void setRecyclerView() {
        try {

            timeZoneList = UtilTime.getAvailableTimeZone();

            adapter = new RecyclerViewAdapter(new RecyclerViewAdapter.IRecyclerViewAdapter() {

                @Override
                public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
                    View view = LayoutInflater.from(getApplicationContext()).inflate(R.layout.cell_timezone,
                            parent, false);
                    return new ItemViewHolder(view);
                }

                @Override
                public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
                    ItemViewHolder item = (ItemViewHolder) holder;

                    String timezone = timeZoneList.get(position);
//                    int menuId = listMenuId.get(position);
//                    String title = getMenuTitle(menuId);
//                    int imageId = getMenuImage(menuId);

                    item.bind(timezone, position);
                }

                @Override
                public int getItemCount() {
                    return timeZoneList.size();
                }
            });

            ;
            GridLayoutManager gridLayoutManager = new GridLayoutManager(this, 5);
            recyclerView.setLayoutManager(gridLayoutManager);
            recyclerView.setAdapter(adapter);
//            recyclerView.requestFocus();


        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    class ItemViewHolder extends RecyclerView.ViewHolder {

        View root;
        TextView timeZoneTV;

        public ItemViewHolder(View itemView) {
            super(itemView);
            root = itemView;
            timeZoneTV = itemView.findViewById(R.id.timezoneTV);
        }

        public void bind(String title, int position) {

            try {
                if (position == 0)
                    itemView.requestFocus();
                timeZoneTV.setText(title);
               /* itemView.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                    @Override
                    public void onFocusChange(View v, boolean hasFocus) {
//                        updateFocus(root, hasFocus);

                    }
                });*/

                itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        try {
                            Global.setTimeZone(title);
                            UtilTime.setTimezone(TimeZoneActivity.this, title);
                            callback.onChangeTimeZone(title);
                            finish();
                        } catch (Exception ex) {
                            Debug.e(TAG, ex);
                        }
                    }
                });
            } catch (Exception ex) {
                Debug.e(TAG, ex);
            }

        }
    }

    public void setCallback(ITimeZone iTimeZone) {
        callback = iTimeZone;
    }


    void showDateTime() {
        runnable = new Runnable() {
            @Override
            public void run() {
                try {
//                    SimpleDateFormat sdfDate = new SimpleDateFormat("E, d MMM yyyy HH:mm:ss");
                    String dateTime = stbControl.setCurrentDateTime();
                    timeView.setText(dateTime);
                    handler.postDelayed(runnable, C.changeTimeInterval);
                } catch (Exception ex) {
                    Debug.e(TAG, ex);
                }
            }
        };
        handler.postDelayed(runnable, C.oneSecondInterval);

    }

    void searchTimeZone(String prmTZ) {
        try {
            //reset list
            timeZoneList.clear();
            List<String> tzList = UtilTime.getAvailableTimeZone();
            if (prmTZ.equals("")) {
                timeZoneList=tzList;
            } else {
                for (String timeZone : tzList) {
                    String tz = timeZone;
                    if (timeZone.toLowerCase().contains(prmTZ.toLowerCase()))
                        timeZoneList.add(tz);

                }
            }
            adapter.notifyDataSetChanged();

        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }


}
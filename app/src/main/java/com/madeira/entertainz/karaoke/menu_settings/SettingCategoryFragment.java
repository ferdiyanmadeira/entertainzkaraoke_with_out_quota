package com.madeira.entertainz.karaoke.menu_settings;


import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.madeira.entertainz.helper.RecyclerViewAdapter;
import com.madeira.entertainz.karaoke.DBLocal.TSettingCategory;
import com.madeira.entertainz.karaoke.Debug;
import com.madeira.entertainz.karaoke.R;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class SettingCategoryFragment extends Fragment implements SettingsTheme.ISettingsTheme {

    String TAG = "SettingCategoryFragment";

    View root;
    LinearLayout categoryLL;
    TextView categoryTV;
    LinearLayout recyclerLL;
    RecyclerView recyclerView;
    RecyclerViewAdapter adapter;

    private ISettingCategory mListener;
    SettingsTheme settingsTheme;
    List<TSettingCategory> tSettingCategoryList = new ArrayList<>();

    public SettingCategoryFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        root = inflater.inflate(R.layout.fragment_setting_category, container, false);
        try {
            bind();
            settingsTheme = new SettingsTheme(getActivity(), this);
            settingsTheme.setTheme();
            setSettingCategory();
            setRecyclerView();
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
        return root;
    }

    @Override
    public void setThemeUpdateDate(Date lastUpdateTheme) {

    }

    @Override
    public void setListColorTheme(int color) {
        try {
            categoryLL.setBackgroundResource(R.drawable.tags_rounded_corners);
            recyclerLL.setBackgroundResource(R.drawable.tags_rounded_corners);

            GradientDrawable categoryDrawable = (GradientDrawable) categoryLL.getBackground();
            categoryDrawable.setColor(color);

            GradientDrawable recycleDrawable = (GradientDrawable) recyclerLL.getBackground();
            recycleDrawable.setColor(color);
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    @Override
    public void setTextAndSelectionColorTheme(ColorStateList colorSelection, ColorStateList colorText, int colorTextSelected, int colorTextFocus, int colorTextNormal, int colorSelectionSelected, int colorSelectionFocus) {
        try {
            categoryTV.setTextColor(colorTextNormal);
            this.colorSelection = colorSelection;
            this.colorText = colorText;
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    void bind() {
        try {
            categoryLL = (LinearLayout) root.findViewById(R.id.categoryLL);
            categoryTV = (TextView) root.findViewById(R.id.categoryTV);
            recyclerLL = (LinearLayout) root.findViewById(R.id.recyclerLL);
            recyclerView = (RecyclerView) root.findViewById(R.id.recyclerView);
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    void setSettingCategory() {
        try {
            TSettingCategory prefTSettingCategory = new TSettingCategory();
            prefTSettingCategory.settingCategoryId = 1;
            prefTSettingCategory.settingCategory = getString(R.string.general);

            TSettingCategory backgroundTSettingCategory = new TSettingCategory();
            backgroundTSettingCategory.settingCategoryId = 2;
            backgroundTSettingCategory.settingCategory = getString(R.string.background);

            TSettingCategory conTSettingCategory = new TSettingCategory();
            conTSettingCategory.settingCategoryId = 3;
            conTSettingCategory.settingCategory = getString(R.string.connection);

            TSettingCategory updateTSettingCategory = new TSettingCategory();
            updateTSettingCategory.settingCategoryId = 4;
            updateTSettingCategory.settingCategory = getString(R.string.checkUpdate);

            TSettingCategory storageSettingCategory = new TSettingCategory();
            storageSettingCategory.settingCategoryId = 5;
            storageSettingCategory.settingCategory = getString(R.string.storage);

            /*TSettingCategory myQuotaSettingCategory = new TSettingCategory();
            myQuotaSettingCategory.settingCategoryId = 6;
            myQuotaSettingCategory.settingCategory = getString(R.string.my_quota);*/

            TSettingCategory serverSetting = new TSettingCategory();
            serverSetting.settingCategoryId = 7;
            serverSetting.settingCategory = "Server";

            TSettingCategory deleteSongSettingCategory = new TSettingCategory();
            deleteSongSettingCategory.settingCategoryId = 8;
            deleteSongSettingCategory.settingCategory = getString(R.string.deleteSong);

            TSettingCategory helpSettingCategory = new TSettingCategory();
            helpSettingCategory.settingCategoryId = 9;
            helpSettingCategory.settingCategory = getString(R.string.help);

      /*  TSettingCategory stickerTSettingCategory = new TSettingCategory();
        stickerTSettingCategory.settingCategoryId = 3;
        stickerTSettingCategory.settingCategory = "Sticker";*/

            tSettingCategoryList.add(prefTSettingCategory);
            tSettingCategoryList.add(backgroundTSettingCategory);
            tSettingCategoryList.add(conTSettingCategory);
            tSettingCategoryList.add(updateTSettingCategory);
            tSettingCategoryList.add(storageSettingCategory);
//            tSettingCategoryList.add(myQuotaSettingCategory);
//            tSettingCategoryList.add(buyQuotaSettingCategory);
            tSettingCategoryList.add(serverSetting);
            tSettingCategoryList.add(deleteSongSettingCategory);
            tSettingCategoryList.add(helpSettingCategory);

//        tSettingCategoryList.add(stickerTSettingCategory);
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    public interface ISettingCategory {
        // TODO: Update argument type and name
        void onCategoryClick(int categoryId, String category);
    }

    void setRecyclerView() {
        try {
            adapter = new RecyclerViewAdapter(new RecyclerViewAdapter.IRecyclerViewAdapter() {
                @Override
                public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
                    View view = LayoutInflater.from(getActivity()).inflate(R.layout.cell_setting_category, parent, false);
                    ItemViewHolder item = new ItemViewHolder(view);
                    return item;
                }

                @Override
                public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
                    ItemViewHolder item = (ItemViewHolder) holder;
                    item.bind(tSettingCategoryList.get(position), position);
                }

                @Override
                public int getItemCount() {
                    return tSettingCategoryList.size();
                }
            });

            recyclerView.setAdapter(adapter);
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    //view ini khusus di pakai utk ItemViewHolder, utk selectedItemView
    View selectedView;
    int selectedItem;

    class ItemViewHolder extends RecyclerView.ViewHolder {
        private static final String TAG = "ItemViewHolder";

        View root;
        TextView tvTitle;

        public ItemViewHolder(View itemView) {
            super(itemView);
            root = itemView;

            tvTitle = itemView.findViewById(R.id.text_title);

//            selectionColor = itemView.findViewById(R.id.selectioncolor);
        }

        public void bind(TSettingCategory tSettingCategory, int pos) {
            try {
                tvTitle.setText(tSettingCategory.settingCategory);

                if (selectedItem == pos) {
                    selectView(root);
                }

//            untuk merubah warna selection
                if (colorSelection != null) root.setBackgroundTintList(colorSelection);

//            untuk merubah warna text
                if (colorText != null) tvTitle.setTextColor(colorText);

                if (pos == 0) {
                    root.setSelected(true);
                    mListener.onCategoryClick(tSettingCategory.settingCategoryId, tSettingCategory.settingCategory);
                }

                root.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        selectView(v);
                        selectedItem = pos;
                        mListener.onCategoryClick(tSettingCategory.settingCategoryId, tSettingCategory.settingCategory);
                    }
                });
            } catch (Exception ex) {
                Debug.e(TAG, ex);
            }
        }

        void selectView(View v) {
            try {
                //clear selection
                if (selectedView != null) {
                    selectedView.setSelected(false);
                }

                //make selection
                selectedView = v;
                selectedView.setSelected(true);
            } catch (Exception ex) {
                Debug.e(TAG, ex);
            }
        }
    }

    ColorStateList colorSelection, colorText;

    public void setCallback(ISettingCategory listener) {
        this.mListener = listener;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }



}

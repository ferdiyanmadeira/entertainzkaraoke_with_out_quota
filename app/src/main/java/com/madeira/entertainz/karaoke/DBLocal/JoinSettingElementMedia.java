package com.madeira.entertainz.karaoke.DBLocal;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class JoinSettingElementMedia implements Serializable
{

    @SerializedName("element_media_id")
    public int elementMediaId;

    @SerializedName("element_id")
    public int elementId;

    @SerializedName("url_image")
    public String urlImage;

    @SerializedName("duration")
    public int duration;

    @SerializedName("isActive")
    public int isActive;



}

package com.madeira.entertainz.karaoke.menu_live_channel;


import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.madeira.entertainz.helper.RecyclerViewAdapter;
import com.madeira.entertainz.karaoke.Debug;
import com.madeira.entertainz.karaoke.R;
import com.madeira.entertainz.karaoke.config.C;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/** fragment category dibagian atas */
public class LiveChannelListFragment extends Fragment implements LiveChannelTheme.ILiveChannelTheme {
    String TAG = "LiveChannelListFragment";


   /* public static final String KEY_LAST_POSITION = "KEY_LAST_POSITION";
    public static final String KEY_FLAG_INCREASE = "KEY_FLAG_INCREASE";
    public static final String KEY_CHANNEL = "KEY_CHANNEL";

    int lastPosition;
    boolean flagIncrease = false;
    int liveChannelId;*/

    View root;
    LinearLayout recyclerLL;
    RecyclerView recyclerView;
    RecyclerViewAdapter adapter;

    private ILiveChannelListListener mListener;
    LiveChannelTheme liveChannelTheme;
    List<Integer> liveChannelList = new ArrayList<>();

    public LiveChannelListFragment() {
        // Required empty public constructor
    }

    public static LiveChannelListFragment newInstance(String param1, String param2) {
        LiveChannelListFragment fragment = new LiveChannelListFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
           /* liveChannelId = getArguments().getInt(KEY_CHANNEL);
            lastPosition = getArguments().getInt(KEY_LAST_POSITION);
            flagIncrease = getArguments().getBoolean(KEY_FLAG_INCREASE);*/
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        root = inflater.inflate(R.layout.fragment_live_channel_list, container, false);
        try {
            bind();
            liveChannelTheme = new LiveChannelTheme(getActivity(), this);
            liveChannelTheme.setTheme();
            setLiveChannelMenu();
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
        return root;
    }

    @Override
    public void setThemeUpdateDate(Date lastUpdateTheme) {

    }

    @Override
    public void setListColorTheme(int color) {
        try {
            recyclerLL.setBackgroundResource(R.drawable.tags_rounded_corners);

            GradientDrawable recycleDrawable = (GradientDrawable) recyclerLL.getBackground();
            recycleDrawable.setColor(color);
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    @Override
    public void setTextAndSelectionColorTheme(ColorStateList colorSelection, ColorStateList colorText, int colorTextSelected, int colorTextFocus, int colorTextNormal, int colorSelectionSelected, int colorSelectionFocus) {
        try {
            this.colorSelection = colorSelection;
            this.colorText = colorText;
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    public interface ILiveChannelListListener {
        // TODO: Update argument type and name
        void onCategoryClick(int categoryId);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
       /* if (context instanceof IRadioListListener) {
            mListener = (IRadioListListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }*/
    }

    @Override
    public void onDetach() {
        super.onDetach();
        try {
            mListener = null;
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }

    }

    void bind() {
        recyclerLL = (LinearLayout) root.findViewById(R.id.recyclerLL);
        recyclerView = (RecyclerView) root.findViewById(R.id.recyclerView);
    }

    void setLiveChannelMenu() {
        try {
            liveChannelList.add(C.MENU_LIVE_CHANNEL_RADIO);
            liveChannelList.add(C.MENU_LIVE_CHANNEL_TV);

            setRecyclerView();
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    void setRecyclerView() {
        try {
            adapter = new RecyclerViewAdapter(new RecyclerViewAdapter.IRecyclerViewAdapter() {
                @Override
                public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
                    View view = LayoutInflater.from(getActivity()).inflate(R.layout.cell_live_channel_list, parent, false);
                    ItemViewHolder item = new ItemViewHolder(view);
                    return item;
                }

                @Override
                public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
                    ItemViewHolder item = (ItemViewHolder) holder;
                    item.bind(liveChannelList.get(position), position);
                }

                @Override
                public int getItemCount() {
                    return liveChannelList.size();
                }
            });
            GridLayoutManager gridLayoutManager = new GridLayoutManager(getActivity(), liveChannelList.size());
            recyclerView.setLayoutManager(gridLayoutManager);
            recyclerView.setAdapter(adapter);
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    //view ini khusus di pakai utk ItemViewHolder, utk selectedItemView
    View selectedView;
    int selectedItem;

    class ItemViewHolder extends RecyclerView.ViewHolder {
        private static final String TAG = "ItemViewHolder";

        View root;
        TextView tvTitle;
        ImageView logoIV;

        public ItemViewHolder(View itemView) {
            super(itemView);
            root = itemView;
            tvTitle = itemView.findViewById(R.id.text_title);
            logoIV = itemView.findViewById(R.id.logo_image_view);
        }

        public void bind(Integer liveChannel, int pos) {
            try {
                int menu = getMenuImage(liveChannel);
                String title = getMenuTitle(liveChannel);
                logoIV.setBackgroundResource(menu);
                tvTitle.setText(title);
                if (selectedItem == pos) {
                    selectView(root);
                }
//            untuk merubah warna selection
                if (colorSelection != null) root.setBackgroundTintList(colorSelection);
//            untuk merubah warna text
                if (colorText != null) tvTitle.setTextColor(colorText);
                if (pos == 0) {
                    root.setSelected(true);
                    selectedItem = pos;
                    mListener.onCategoryClick(liveChannel);
                }

               /* if (flagIncrease) {
                    if (pos == lastPosition + 1) {
                        root.setSelected(true);
                        selectedItem = pos;
                        mListener.onRadioCategoryClick(liveChannel);
                    }
                } else {
                    if (pos == lastPosition - 1) {
                        root.setSelected(true);
                        selectedItem = pos;
                        mListener.onRadioCategoryClick(liveChannel);
                    }
                }*/

                root.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        selectView(v);
                        selectedItem = pos;
                        mListener.onCategoryClick(liveChannel);
                    }
                });
            } catch (Exception ex) {
                Debug.e(TAG, ex);
            }
        }

        void selectView(View v) {
            //clear selection
            if (selectedView != null) {
                selectedView.setSelected(false);
            }

            //make selection
            selectedView = v;
            selectedView.setSelected(true);
        }
    }

    ColorStateList colorSelection, colorText;

    public void setCallback(ILiveChannelListListener listener) {
        this.mListener = listener;
    }

    int getMenuImage(int menuId) {
        int imageId = 0;

        try {
            switch (menuId) {

                case C.MENU_LIVE_CHANNEL_TV:
                    imageId = R.drawable.ic_nav_tv;
                    break;
                case C.MENU_LIVE_CHANNEL_RADIO:
                    imageId = R.drawable.ic_nav_radio;
                    break;
            }
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }

        return imageId;
    }


    String getMenuTitle(int menuId) {
        String title = null;

        try {
            switch (menuId) {
                case C.MENU_LIVE_CHANNEL_TV:
                    title = getString(R.string.title_live_tv);
                    break;
                case C.MENU_LIVE_CHANNEL_RADIO:
                    title = getString(R.string.title_radio);
                    break;
            }
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
        return title;
    }
}

package com.madeira.entertainz.karaoke.model_room_db;

import android.content.Context;

import com.madeira.entertainz.karaoke.DBLocal.DbMessage;
import com.madeira.entertainz.karaoke.DBLocal.TMessage;
import com.madeira.entertainz.karaoke.Debug;
import com.madeira.entertainz.library.PerformAsync2;

import java.util.ArrayList;
import java.util.List;

public class ModelMessage {
    static String TAG = "ModelMessage";

    /**
     * untuk get list Message
     */
    public interface IGetListMessage {
        void onGetListMessage(List<TMessage> TMessageList);
    }

    public static void getListMessage(Context context, IGetListMessage callback) {
        PerformAsync2.run(performAsync ->
        {
            List<TMessage> result = new ArrayList<>();
            try {
                DbMessage dbMessage = DbMessage.Instance.create(context);
                result = dbMessage.daoAccessMessage().getAll();
                dbMessage.close();
            } catch (Exception ex) {
                Debug.e(TAG, ex);
            }
            return result;
        }).
                setCallbackResult(result ->

                {
                    try {
                        List<TMessage> TMessageList = (List<TMessage>) result;
                        callback.onGetListMessage(TMessageList);
                    } catch (Exception ex) {
                        Debug.e(TAG, ex);
                    }

                });
    }

    /**
     * untuk get Message
     */
    public interface IGetMessage {
        void IGetMessage(TMessage TMessage);
    }

    public static void getMessage(Context context, int messageId, IGetMessage callback) {
        PerformAsync2.run(performAsync ->
        {
            TMessage TMessage = null;
            try {
                if (messageId != 0) {
                    DbMessage dbMessage = DbMessage.Instance.create(context);
                    TMessage = dbMessage.daoAccessMessage().getMessageById(messageId);
                    dbMessage.close();
                }
            } catch (Exception ex) {
                Debug.e(TAG, ex);
            }

            return TMessage;
        }).
                setCallbackResult(result ->
                {
                    try {
                        TMessage TMessage = (TMessage) result;
                        callback.IGetMessage(TMessage);
                    } catch (Exception ex) {
                        Debug.e(TAG, ex);
                    }

                });
    }

    /**
     * untuk update status message
     */

    public interface IUpdateMessage {
        void IUpdateMessage(int result);
    }

    public static void updateMessage(Context context, int messageId, String status, IUpdateMessage callback) {
        PerformAsync2.run(performAsync ->
        {
            int result = 0;
            try {
                DbMessage dbMessage = DbMessage.Instance.create(context);
                dbMessage.daoAccessMessage().updateStatusMessage(status, messageId);
                dbMessage.close();
                result = 1;
            } catch (Exception ex) {
                Debug.e(TAG, ex);
            }

            return result;
        }).
                setCallbackResult(result ->

                {
                    try {
                        int r = (int) result;
                        callback.IUpdateMessage(r);
                    } catch (Exception ex) {
                        Debug.e(TAG, ex);
                    }

                });
    }

}

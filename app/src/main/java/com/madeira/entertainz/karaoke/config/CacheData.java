package com.madeira.entertainz.karaoke.config;

import com.madeira.entertainz.karaoke.DBLocal.TElement;
import com.madeira.entertainz.karaoke.DBLocal.TLocalFile;
import com.madeira.entertainz.karaoke.DBLocal.TSong;
import com.madeira.entertainz.karaoke.DBLocal.TSongItemServer;
import com.madeira.entertainz.karaoke.model_online.ModelChannel;
import com.madeira.entertainz.karaoke.model_online.ModelSong;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

public class CacheData {
    //untuk menyimpan itemsList theme element yang diambil dari server
    public static List<TElement> listElement = new ArrayList<>();
    public static Hashtable<Integer, TElement> hashElement = new Hashtable<Integer, TElement>();

    public static Hashtable<String, TLocalFile> hashFile = new Hashtable<String, TLocalFile>();
//    public static Hashtable<String, TLocalFile> hashFileImage = new Hashtable<String, TLocalFile>();
//    public static Hashtable<String, TLocalFile> hashFileVideo = new Hashtable<String, TLocalFile>();
    public static Hashtable<String, TSongItemServer> hashSongNameNArtis = new Hashtable<String, TSongItemServer>();

    public static List<ModelChannel.subChannelItem> channelItemList = new ArrayList<>();

    public static int lastIndexBackground = 0;
}

package com.madeira.entertainz.karaoke.model_online;

import android.net.Uri;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.OkHttpResponseAndJSONObjectRequestListener;
import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;
import com.madeira.entertainz.karaoke.Debug;
import com.madeira.entertainz.karaoke.config.Global;
import com.madeira.entertainz.library.Util;

import org.json.JSONObject;

import java.io.Serializable;
import java.util.List;

import okhttp3.Response;

import static com.madeira.entertainz.karaoke.config.C.DEFAULT_API;

public class ModelRadio {
    private static final String TAG = "ModelRadio";

    private static final String APIHOST = Global.getApiHostname() + "/v1/radio.php";

    public interface IGetRadio {
        void onGetRadio(radioItem result);

        void onError(ANError e);
    }
    static public void getRadio(IGetRadio callBack) {
        String mac = Util.getMACAddress("eth0");
        mac = mac.replace(":", "");
        try {
            String uri = Uri.parse(APIHOST)
                    .buildUpon()
                    .appendQueryParameter("action", "get_radio")
                    .appendQueryParameter("mac", mac)
                    .build().toString();

            AndroidNetworking.get(uri)
                    .setPriority(Priority.MEDIUM)
                    .build()
                    .getAsOkHttpResponseAndJSONObject(new OkHttpResponseAndJSONObjectRequestListener() {
                        @Override
                        public void onResponse(Response okHttpResponse, JSONObject response) {
                            try {
                                if (okHttpResponse.isSuccessful()) {
                                    Gson gson = new Gson();
                                    String jsonString = response.toString();
                                    radioItem stbItem = gson.fromJson(jsonString, radioItem.class);
                                    callBack.onGetRadio(stbItem);
                                }
                            } catch (Exception ex) {
                                Debug.e(TAG, ex);
                            }
                        }

                        @Override
                        public void onError(ANError anError) {
                            callBack.onError(anError);
                        }
                    });
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }


    public class radioItem {
        @SerializedName("list_radio")
        public List<subRadioItem> list;
    }


    public static class subRadioItem implements Serializable {
        @SerializedName("radio_name")
        public String radioName;
        @SerializedName("url_image")
        public String urlImage;
        @SerializedName("url_radio")
        public String urlRadio;
        @SerializedName("description")
        public String description;
    }

}

package com.madeira.entertainz.karaoke.DBLocal;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.TypeConverter;
import android.arch.persistence.room.TypeConverters;
import android.support.annotation.NonNull;

import com.google.gson.annotations.SerializedName;
import com.madeira.entertainz.karaoke.config.C;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;


@Entity(tableName = "trecent_search_song")
public class TRecentSearchSong implements Serializable {

    @PrimaryKey(autoGenerate = true)
    @NonNull
    @SerializedName("recent_id")
    public int recentId;
    @SerializedName("recent_song")
    public String recentSong;
    public int category;

}




package com.madeira.entertainz.karaoke.menu_news;

import android.content.Context;
import android.content.Intent;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.madeira.entertainz.karaoke.BaseActivity;
import com.madeira.entertainz.karaoke.DBLocal.TNews;
import com.madeira.entertainz.karaoke.R;
import com.madeira.entertainz.karaoke.menu_youtube.YouTubeVideoActivity;
import com.madeira.entertainz.library.EzWebViewClient;
import com.madeira.entertainz.library.Util;
import com.squareup.picasso.Picasso;

import java.net.MalformedURLException;

public class NewsDetailActivity extends BaseActivity {
    private static final String TAG = "NewsDetailActivity";

    private static final String INTENT_NEWS_DATA = "INTENT_NEWS_DATA";
    private static final String VIDEO_TYPE_YOUTUBE = "YOUTUBE";
    private static final String VIDEO_TYPE_HLS = "HLS";

    private static final int SCROLL_STEP = 25;

    TNews news;

    EzWebViewClient ezWebViewClient;

    LinearLayout containerHeader;
    ConstraintLayout containerPoster;

    ImageView imageViewPlayIcon;
    ImageView imageViewPoster;
    TextView textViewTitle;
    WebView webView;

//    boolean isWebViewFocus;
    boolean isHeaderVisible;

    public static void startActivity(Context context, TNews news){
        Intent intent;
        intent = new Intent(context, NewsDetailActivity.class);

        Bundle bundle = new Bundle();
        bundle.putSerializable(INTENT_NEWS_DATA, news);
        intent.putExtras(bundle);

        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle bundle = getIntent().getExtras();

        news = (TNews) bundle.getSerializable(INTENT_NEWS_DATA);

        setContentView(R.layout.activity_news_detail);

        //hide bottom android nav bar
        Util.hideNavigationBar(this);

        bind();

        setupUi();

        setupWebview();


//        String html = "<p style=\"text-align: justify;\">Jalan Tol Trans Jawa menjadi salah satu proyek infrastruktur yang terus dikebut penyelesaiannya. Sekitar delapan ruas tol dengan panjang tambahan total mencapai 649 kilometer (km) ditargetkan bisa beroperasi hingga tahun 2019 nanti. Tol Trans Jawa akan menghubungkan Merak sampai Surabaya dengan jalan tol tanpa putus.<br /><br />Ruas-ruas tersebut di antaranya Pejagan-Pemalang, Pemalang-Batang, Batang-Semarang, Semarang-Solo, Solo-Ngawi, Ngawi-Kertosono, Mojokerto-Jombang-Kertosono, Mojokerto-Surabaya, Gempol-Pasuruan, dan Pasuruan-Probolinggo.<br /><br />Berdasarkan data Badan Pengatur Jalan Tol (BPJT) yang diterima&nbsp;<strong>detikFinance</strong>, seperti dikutip Rabu (5/7/2017), hingga akhir Juni 2017, ruas-ruas yang dibangun terus mengalami progres sejak dimulai konstruksi hingga saat ini. Sejumlah seksi dari beberapa ruas juga telah beroperasi. Seperti Jalan Tol Cikopo-Palimanan (Cipali) yang telah beroperasi pada Juni 2015.<br /><br />Untuk ruas Pejagan-Pemalang sepanjang 57,5 km, dengan masa pelaksanaan konstruksi dari 2015-2017, progres tanah secara keseluruhan saat ini telah mencapai 99,41% dengan konstruksi 65,66% (Seksi I dan II sudah beroperasi). Seksi III dan IV yang menyambungkan Brebes Timur hingga Pemalang ditarget bisa segera beroperasi tahun ini.<br /><br />Di ruas Pemalang-Batang sepanjang 39,2 km (masa konstruksi 2016-2018), progres tanahnya saat ini telah mencapai 92,07%. Namun konstruksinya masih sekitar 19,42%. Dua seksi dari ruas ini ditarget bisa beroperasi penuh pada tahun 2018.<br /><br />Ruas Batang-Semarang sepanjang 75 km (masa konstruksi 2016-2018), progres tanahnya saat ini sebesar 76,34% dengan konstruksi mencapai 24,95%. Ruas ini telah digunakan secara fungsional saat mudik Lebaran kemarin dan ditarget bisa beroperasi secara penuh pada 2018.<br /><br />Pada ruas Semarang-Solo sepanjang 72,64 km, progres tanahnya saat ini telah mencapai 98,25% dengan konstruksi 57,72%. Dua seksi dari ruas yang telah dibangun sejak 2019 ini telah beroperasi, sedangkan seksi III yang menyambungkan Bawen dan Salatiga akan beroperasi tahun ini. Sementara sisa seksi dari ruas ini diharapkan beroperasi tahun depan.<br /><br />Menuju ke arah Timur ada ruas Solo-Ngawi sepanjang 90,1 km yang telah dibangun sejak 2010. Progres tanah saat ini telah mencapai 99,7% dengan konstruksi 74,49%. Ruas yang porsi pembangunannya dibagi dua oleh pemerintah dan swasta ini diharapkan bisa beroperasi tahun ini.<br /><br />Selanjutnya ruas Ngawi-Kertosono yang dibangun sejak 2015 sepanjang 87,02 km, pembebasan lahan telah mencapai 97,96% dengan progres konstruksi 41,47%. Sebagian seksi dari ruas ini ditarget bisa operasional pada tahun ini dan secara penuh pada tahun depan.<br /><br />Jalan Tol di Jawa Timur menunjukkan progres pembebasan lahan lebih baik dengan rata-rata telah mencapai 100%. Pada ruas Tol Mojokerto-Jombang-Kertosono sepanjang 40,5 km, konstruksi saat ini telah mencapai 90,88% (seksi I dan III telah beroperasi). Jalan tol yang dibangun sejak 2013 ini ditarget bisa beroperasi tahun ini.<br /><br />Ruas Tol Mojokerto-Surabaya sepanjang 36,47 km telah bebas 100% tanahnya. Tol yang dibangun sejak 2010 ini telah mengoperasikan seksi IA dan seksi IV. Dengan progres konstruksi 86,76%, Jalan bebas hambatan menuju ke Surabaya ini ditarget beroperasi pada September tahun ini.<br /><br />Sisa ruas lainnya di Jawa Timur, yakni Gempol-Pasuruan sepanjang 34,15 km dan Pasuruan-Probolinggo sepanjang 31,3 km. Kedua jalan Tol yang dibangun sejak 2015 dan 2016 ini ditarget bisa beroperasi penuh pada 2019. Adapun saat ini Gempol-Pasuruan progres tanahnya mencapai 76,47% dengan konstruksi 45,4%, sedangkan Pasuruan-Probolinggo progres tanahnya telah mencapai 96,85%, dengan konstruksi baru 9,22%.&nbsp;</p>";

        String content = "<html><head><meta http-equiv=\"content-type\" content=\"text/html; charset=utf-8\" /></head><body>";
        content += news.body + "</body></html>";

        webView.loadData(content, "text/html", "UTF-8");
    }

    int y=0;

    @Override
    protected void onResume() {
        super.onResume();
        Util.hideNavigationBar(this);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {

        switch (keyCode){
            case KeyEvent.KEYCODE_DPAD_UP:
                //bagian ini utk memngerakkan poster ke bawah,
                //event ini terjadi apabila ada tombol playback muncul
                y = y-SCROLL_STEP;
                if (y<0) y = 0; //set y=0 apabila sdh lewat dari batas
                isHeaderVisible = moveHeader(y);
                break;
        }

        return super.onKeyDown(keyCode, event);
    }

    private void bind(){

        containerHeader = findViewById(R.id.container_header);
        containerPoster = findViewById(R.id.container_poster);

        imageViewPlayIcon = findViewById(R.id.image_view_play);
        imageViewPoster = findViewById(R.id.image_view_poster);
        textViewTitle = findViewById(R.id.text_view_title);
        webView = findViewById(R.id.web_view);
    }

    private void setupUi(){

        isHeaderVisible = true;

        //apabila poster tdk ada maka hide poster
        if (news.urlThumb==null || news.urlThumb.length()==0){
            containerPoster.setVisibility(View.GONE);
        } else {
            Picasso.with(this).load(news.urlThumb).into(imageViewPoster);
        }

        textViewTitle.setText(news.title);

        //hide playicon apabila tdk ada video
        if (news.urlVideo==null || news.urlVideo.length()==0) {
            imageViewPlayIcon.setVisibility(View.INVISIBLE);
        }

        imageViewPlayIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.i(TAG, "onClick: " + news.urlVideo);
                if (news.videoType.equals(VIDEO_TYPE_YOUTUBE)){
                    try {
                        String videoId = Util.extractYoutubeId(news.urlVideo);
                        YouTubeVideoActivity.startActivity(NewsDetailActivity.this, videoId);
                    } catch (MalformedURLException e) {
                        e.printStackTrace();
                    }
                }
                else
                    PlayerOtherActivity.startActivity(NewsDetailActivity.this, news.urlVideo);
            }
        });
    }

    private void setupWebview(){

        ezWebViewClient = new EzWebViewClient(new EzWebViewClient.Callback() {
            @Override
            public boolean shouldOverrideKeyEvent(WebView view, KeyEvent event) {

                int scrollY = view.getScrollY();

//                Log.i(TAG, "\nshouldOverrideKeyEvent: isPosterVisible="+ isHeaderVisible + "\nscrollY=" + scrollY);

                //lakukan move header, apabila posisi webview sudah paling atas
                if (event.getAction()==KeyEvent.ACTION_DOWN && scrollY==0){
                    switch (event.getKeyCode()){
                        case KeyEvent.KEYCODE_DPAD_DOWN:

                            //scroll terus apabila header masih visible
                            if (isHeaderVisible) y = y+SCROLL_STEP;
                            isHeaderVisible = moveHeader(y);
                            break;
                        case KeyEvent.KEYCODE_DPAD_UP:
                            y = y-SCROLL_STEP;
                            if (y<0) y=0;
                            isHeaderVisible = moveHeader(y);

                            break;
                    }
                }

                //block webview scroll apabile hader masih visible
                if (isHeaderVisible) return true;

                //apabila header sdh hilang, maka webview boleh scroll
                return false;
            }

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
                return true;
            }
        });

        webView.setWebChromeClient(new WebChromeClient());
        //use this to prevent web view from opening the browser
        webView.setWebViewClient(ezWebViewClient);

        webView.getSettings().setJavaScriptEnabled(true);

//        webView.setOnFocusChangeListener(new View.OnFocusChangeListener() {
//            @Override
//            public void onFocusChange(View view, boolean b) {
//                Log.i(TAG, "onFocusChange: FOCUS=" + b);
//                isWebViewFocus = b;
//            }
//        });

    }

    int headerHeight;
    /**
     * Move header sampai tersembunyi
     *
     * @param y
     * @return true = poster masih muncul, false = poster sdh tersembunyi
     */
    private boolean moveHeader(int y){

        //set tinggi header di awal
        if (headerHeight==0) headerHeight = imageViewPoster.getHeight();

        //apabila header sdh tersemubunyi maka tdk bisa lbh lagi
        if (y > headerHeight) y = headerHeight;

//        Log.i(TAG, "moveHeader: y=" + y + "\nheaderHeight="+headerHeight);

        containerHeader.setPadding(0,-y,0,0);

        if (y==headerHeight) {
            return false;
        } else {
            return true;
        }
    }
}

package com.madeira.entertainz.karaoke.DBLocal;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import com.google.gson.annotations.SerializedName;

@Entity(tableName = "tsticker")
public class TSticker {
    private static final String TAG = "TSticker";

    @PrimaryKey
    @NonNull
    @SerializedName("stickerId")
    public int stickerId;

    @NonNull
    @SerializedName("stickerName")
    public String stickerName;

    @SerializedName("url_image")
    public String urlImage;
}
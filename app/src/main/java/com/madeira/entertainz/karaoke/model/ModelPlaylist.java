package com.madeira.entertainz.karaoke.model;

import android.util.Log;

import com.google.gson.Gson;
import com.madeira.entertainz.karaoke.DBLocal.TPlaylist;
import com.madeira.entertainz.karaoke.DBLocal.TSongPlaylist;
import com.madeira.entertainz.library.ApiError;
import com.madeira.entertainz.library.Util;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ModelPlaylist {

    static String TAG = "ModelPlaylist";

    public static List<TPlaylist> getPlaylist(String filePath) {
        List<TPlaylist> tPlaylistList = new ArrayList<TPlaylist>();
                String stringJSON = "";
                try {
                    File file = new File(filePath);
                    FileInputStream fileInputStream = new FileInputStream(file);
                    stringJSON = Util.getJSONFromTextFile(fileInputStream);
                    Log.d(TAG, filePath);
                    Gson gson = new Gson();

                    //throw apabila ada apiError
                    try {
                        ApiError.process(gson, stringJSON);
                        ResultPlaylistList r = gson.fromJson(stringJSON, ResultPlaylistList.class);
                        tPlaylistList = r.list;
                    } catch (ApiError apiError) {
                        apiError.printStackTrace();
                    }

                    //if no error, next convert the real data

                } catch (IOException e) {
                    e.printStackTrace();
                }
        return tPlaylistList;
    }

    public static List<TSongPlaylist> getSongPlaylist(String filePath) {
        List<TSongPlaylist> tSongPlaylistList = new ArrayList<TSongPlaylist>();

                String stringJSON = "";
                try {
                    File file = new File(filePath);
                    FileInputStream fileInputStream = new FileInputStream(file);
                    stringJSON = Util.getJSONFromTextFile(fileInputStream);
                    Log.d(TAG, filePath);
                    Gson gson = new Gson();

                    //throw apabila ada apiError
                    try {
                        ApiError.process(gson, stringJSON);
                        ResultSongPlaylistList r = gson.fromJson(stringJSON, ResultSongPlaylistList.class);
                        tSongPlaylistList = r.list;
                    } catch (ApiError apiError) {
                        apiError.printStackTrace();
                    }
                    //if no error, next convert the real data
                } catch (IOException e) {
                    e.printStackTrace();
                }
        return tSongPlaylistList;
    }

    public static class ResultPlaylistList {
        public List<TPlaylist> list;
    }

    public static class ResultSongPlaylistList {
        public List<TSongPlaylist> list;
    }


}

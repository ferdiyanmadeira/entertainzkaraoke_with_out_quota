package com.madeira.entertainz.karaoke;

import android.app.Service;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.os.Binder;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.madeira.entertainz.controller.USBControl;
import com.madeira.entertainz.karaoke.DBLocal.JoinSongCountPlayed;
import com.madeira.entertainz.karaoke.DBLocal.TSong;
import com.madeira.entertainz.karaoke.DBLocal.TSongCategory;
import com.madeira.entertainz.karaoke.DBLocal.TSongYoutube;
import com.madeira.entertainz.karaoke.config.C;
import com.madeira.entertainz.karaoke.config.Global;
import com.madeira.entertainz.karaoke.menu_main.MainActivity;
import com.madeira.entertainz.karaoke.model_room_db.ModelSong;
import com.madeira.entertainz.library.RootUtil;
import com.madeira.entertainz.library.Util;
import com.madeira.eznet.NetTransport;
import com.madeira.eznet.ProtocolEvent;
import com.madeira.eznet.Query;
import com.madeira.eznet.Response;
import com.madeira.eznet.Rpc;
import com.madeira.eznet.Server;
import com.madeira.eznet.lib.HelperGson;

import java.io.File;
import java.lang.reflect.Type;
import java.nio.channels.SocketChannel;
import java.util.ArrayList;
import java.util.List;

public class MgmtService extends Service {
    private static final String TAG = "MgmtService";

    Server server;

    MutableLiveData<String> ldControlIp;
    MutableLiveData<String> ldPingData;
    private MutableLiveData<String> ldMessage;
    private MutableLiveData<String> ldFilename;

    public static String KEY_CONNECTED = "CONNECTED";

    /**
     * call this from application
     *
     * @param context
     */
    public static void startService(Context context) {
        Intent i = new Intent(context, MgmtService.class);
        context.startService(i);
    }

    /**
     * called from activity, to force to create service
     * <p>
     * saat pertama kali muncul, service yg di run dgn startservice tdk otomatis munucl
     * shg harus di panggil lagi dgn bindService
     *
     * @param context
     * @param callback
     * @return
     */
    public static boolean bindService(Context context, ServiceConnection callback) {
        Intent intent = new Intent(context, MgmtService.class);
        return context.bindService(intent, callback, Context.BIND_AUTO_CREATE);
    }

    public MgmtService() {
    }

    public MutableLiveData<String> getLdControlIp() {
        return ldControlIp;
    }

    public MutableLiveData<String> getLdPingData() {
        return ldPingData;
    }

    public MutableLiveData<String> getLdMessage() {
        return ldMessage;
    }

    public void setLdMessage(MutableLiveData<String> ldMessage) {
        this.ldMessage = ldMessage;
    }

    public MutableLiveData<String> getLdFilename() {
        return ldFilename;
    }

    public void setLdFilename(MutableLiveData<String> ldFilename) {
        this.ldFilename = ldFilename;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        init();

        setupEznet();
    }

    @Override
    public void onDestroy() {
        release();

        super.onDestroy();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return Service.START_STICKY;
    }

    @Override
    public IBinder onBind(Intent intent) {
        return new ThisBinder();
    }

    private void init() {
        try {
            ldControlIp = new MutableLiveData<String>();
            ldPingData = new MutableLiveData<String>();
            ldMessage = new MutableLiveData<>();
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }

    }

    private void release() {
        if (server != null) server.finish();
    }

    private void setupEznet() {

        try {
            String appPath = getBaseContext().getFilesDir().getAbsolutePath();

            //default port 56789
            server = new Server(appPath, BuildConfig.APPLICATION_ID);
//        server.setPort(56789); //set different port
            server.setSocketHandlerCallback(new ProtocolEvent() {

                @Override
                public Response onBgRecv(String ip, String message, String filename) {
                    //TODO: isikan dgn handler saat request dari c# masuk
                    // semua yg di kirim lewat EznetMessage akan masuk kesini

                    Query query = new Query();
                    if (query.parse(message) == false)
                        query = null; //apabila tdk ada query maka null


                    return processMessageInBackground(message, query, filename, appPath);
                }

                // 3 event di bawah di pakai utk EznetControl
                @Override
                public void onBgConnect(String remoteIp) {
                    Log.i(TAG, "onBgConnect: " + remoteIp);
                    ldControlIp.postValue(remoteIp); //apabila control connect maka remoteip akan terisi
//                sendBcEKMAConnected(true);
                    disableUserAction(false);
                }

                @Override
                public void onBgDisconnect(String remoteIp) {
                    Log.i(TAG, "onBgDisconnect: ");
                    ldControlIp.postValue(null); //set nilai null apabila disconnected
//                sendBcEKMAConnected(false);
                    disableUserAction(true);
                }

                @Override
                public void onBgPing(String remoteIp, int count, String pingData) {
//                Log.i(TAG, "onBgPing: " + pingData);
                    ldPingData.postValue(pingData);

                }
            });

            server.start();
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }


    //dipakai saat onBind
    public class ThisBinder extends Binder {
        MgmtService getService() {
            return MgmtService.this;
        }
    }

    /**
     * untuk send bc app sedang connect dengan EKMA(Management Service)
     *//*
    void sendBcEKMAConnected(boolean result) {
        try {
            Intent sendBC = new Intent();
            sendBC.setFlags(Intent.FLAG_INCLUDE_STOPPED_PACKAGES)
                    .setAction(C.CONNECT_WITH_MANAGEMENT_SERVICE);
            sendBC.putExtra(KEY_CONNECTED, result);
            sendBroadcast(sendBC);
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    public static void registerEKMAConnectedReceiver(Context context, BroadcastReceiver br) {
        try {
            IntentFilter intFilt = new IntentFilter(C.CONNECT_WITH_MANAGEMENT_SERVICE);
            context.registerReceiver(br, intFilt);
        } catch (Exception e) {
            Debug.e(TAG, e);
        }
    }

    public static void unregisterEKMAConnectedReceiver(Context context, BroadcastReceiver br) {
        try {
            context.unregisterReceiver(br);
        } catch (Exception e) {
            Debug.e(TAG, e);
        }
    }*/

    void disableUserAction(boolean prm) {
        try {
            if (prm) {
                MainActivity.startActivityFromEkma(getApplicationContext());
            } else {
                EKMAConnectedActivity.startActivity(getApplicationContext());
            }
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    //nama query ini dipakai saat ekma request
    final public String QUERY_YOTUBE_PERSONAL_KARAOKE = "YOUTUBE-PERSONAL-KARAOKE";

    private Response processMessageInBackground(String message, Query query, String filename, String appPath) {

        if (query == null) {
            //hardcode file
//            String outFilename = "/data/data/com.madeira.sonardemo/files/abc.txt";
//            NetTransport.send(socketChannel, "Hahahaha ❤❤❤", outFilename);
//            return new Response("Hahahaha ❤❤❤", outFilename);
        }

        switch (query.getName()) {
            case "testPushPull": //hanya utk testing kirim dan terima file saja
                return onTestPushPull(filename);
            case "rpc":
                return onRpc(message, query);
            case "sendSong":
                return onSendSong(query, filename);
            case "exportSong":
                return onExportSong(query);
            case QUERY_YOTUBE_PERSONAL_KARAOKE:
                return onYoutubePersonalKaraoke(query);
        }

//        //hardcode file
//        String outFilename = "/data/data/com.madeira.sonardemo/files/abc.txt";

        //test message with unicode
//        NetTransport.send(socketChannel, "Hahahaha ❤❤❤", outFilename);

        return null;
    }

    /**
     * di pakai hanya utk testing terima dan kirim file
     * jgn lupa hapus file2 yg di kirim dari ekma
     *
     * @param filename file yg di terima
     * @return response filename yg akan di kirim
     */
    private Response onTestPushPull(String filename) {
        Log.i(TAG, "onTestPushPull: \nreceive=" + filename);

        //lokasi file yg di terima
        String appPath = getBaseContext().getFilesDir().getAbsolutePath();

        //kirim file yg sama ke ekma
        return new Response("Hellloooo", appPath + "/" + filename);
    }

    private Response onRpc(String message, Query query) {

        Log.i(TAG, "onRpc: " + message);
        //ambil parameter dari query, dan di parse ke rpc
        String parameter = query.get("rpc");

        ldMessage.postValue(message);

        Rpc rpc = new Rpc();
        rpc.parse(parameter);

        //akan memanggil method yg terdapat dalam rpc
        String result = (String) rpc.invoke();

        //kirim reply
        return new Response(result);
    }

    /**
     * handle semua request youtube personal karaoke
     * <p>
     * rpc --> model_room_db\ModelYoutube.java
     * <p>
     * 1. syncAdd       -> utk add & update youtube personal karaoke
     * 2. syncGetAll    -> ambil semua list personal youtube karaoke
     * 3. syncDelete    -> delete
     *
     * @param query
     * @return
     */
    private Response onYoutubePersonalKaraoke(Query query) {

        String parameter = query.get("rpc");

        Rpc rpc = new Rpc();
        rpc.parse(parameter);

        //akan memanggil method yg terdapat dalam rpc
        String result = (String) rpc.invoke();

//        List<TSongYoutube> list = ModelYoutube.syncGetAll(MgmtService.this.getBaseContext());
//        Log.i(TAG, "onYoutubePersonalKaraoke: " + HelperGson.toString(list));

        //kirim reply
        return new Response(result);
    }


    /**
     * terima kiriman gambar dari .net
     *
     * @param query
     */
    private Response onSendSong(Query query, String filename) {

        try {
            String appFileDir = getFilesDir().toString();
            String pathSong = Global.getUSBPath() + "/" + C.PATH_VIDEO_KARAOKE;
            String jsonTSong = query.get("TSong");
            Gson gson = new Gson();

            TSong tSong = gson.fromJson(jsonTSong, TSong.class);

            String thumbnailFileName = tSong.songId + ".jpg";
            String pathImage = Global.getUSBPath() + "/" + C.PATH_IMAGE_KARAOKE + "/" + thumbnailFileName;
            String thumbnailPath = Util.base64ToFile(tSong.thumbnail, appFileDir + "/" + thumbnailFileName);
            //move thumbnail to usb
            if (thumbnailPath != "") {
                RootUtil.moveFile(thumbnailPath, pathImage);
                tSong.thumbnail = pathImage;
                String message = "onSendSong: \n" + jsonTSong + "\n" + filename;
                ldMessage.postValue(message);


                //split song directory karena jika tidak dilakukan seperti akan lama ketika play song
                File rootSong = new File(pathSong);
                File[] listDirSong = rootSong.listFiles();
                List<File> listFolder = new ArrayList<>();
                for (File itemFile : listDirSong) {
                    if (itemFile.isDirectory())
                        listFolder.add(itemFile);
                }
                int lastFolder = 0;
                //jika belum ada folder sama sekali di path karaoke
                if (listFolder.size() == 0) {
                    String newDir = pathSong + "/" + String.valueOf(1);
                    String msg = RootUtil.createDir(newDir);
                    if (msg.equals(""))
                        lastFolder = 1;
                } else {
                    boolean fgCreateDir = true;
                    int i = 1;
                    for (File file : listFolder) {
                        File[] listFiles = file.listFiles();
                        if (listFiles.length < C.LIMIT_SONG_IN_FOLDER) {
                            lastFolder = i;
                            fgCreateDir = false;
                            break;
                        }
                    }
                    if (fgCreateDir) {
                        int nextFolder = i + 1;
                        String newDir = pathSong + "/" + String.valueOf(nextFolder);
                        String msg = RootUtil.createDir(newDir);
                        if (msg.equals(""))
                            lastFolder = nextFolder;

                    }
                }
                String downloadPath = pathSong + "/" + String.valueOf(lastFolder);
                File file = new File(appFileDir + "/" + filename);
                if (file.exists()) {
                    RootUtil.moveFile(appFileDir + "/" + filename, downloadPath + "/" + filename);
                    tSong.songURL = downloadPath + "/" + filename;
                    insertSongToRoom(tSong);
                    File f = new File(downloadPath + "/" + filename);
                    ldFilename.postValue(f.getAbsolutePath());
                }
            }

        } catch (Exception e) {
            Debug.e(TAG, e);
//            e.printStackTrace();
        }

        return new Response("Song receive and saved as " + filename);
    }

    void insertSongToRoom(TSong tSong) {
        try {
            ModelSong.syncAddAndReplaceSong(MainApplication.context, tSong);
            writeSongFileToJSON();
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    void writeSongFileToJSON() {
        USBControl usbControl = new USBControl();
        usbControl.writeSongFileToJSON(getApplicationContext(), new USBControl.IWriteFileSong() {
            @Override
            public void onWriteFileSong(boolean result) {
            }
        });
    }


    private Response onExportSong(Query query) {

        try {

            String jsonTSong = query.get("JoinSongCountPlayed");
            Gson gson = new Gson();
            JoinSongCountPlayed tSong = gson.fromJson(jsonTSong, JoinSongCountPlayed.class);
            String outFileName = tSong.songURL;
//            String thumbnailBase64 = Util.fileToBase64(tSong.thumbnail);
//            tSong.thumbnail = thumbnailBase64;
            File file = new File(outFileName);
            if (file.exists())
                return new Response(HelperGson.toString(tSong), outFileName);


        } catch (Exception e) {
            Debug.e(TAG, e);
        }

        return new Response("Failed export song");
    }

}
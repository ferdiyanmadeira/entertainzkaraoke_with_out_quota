package com.madeira.entertainz.karaoke.menu_settings;


import android.app.AlertDialog;
import android.content.res.ColorStateList;
import android.graphics.drawable.GradientDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.androidnetworking.error.ANError;
import com.madeira.entertainz.controller.STBControl;
import com.madeira.entertainz.karaoke.BuildConfig;
import com.madeira.entertainz.karaoke.Debug;
import com.madeira.entertainz.karaoke.R;
import com.madeira.entertainz.karaoke.config.C;
import com.madeira.entertainz.karaoke.config.Global;
import com.madeira.entertainz.karaoke.model_online.ModelApp;
import com.madeira.entertainz.karaoke.model_online.ModelSTB;
import com.madeira.entertainz.library.Util;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * A simple {@link Fragment} subclass.
 */
public class SettingCheckUpdateFragment extends Fragment implements SettingsTheme.ISettingsTheme {
    String parentTAG = "SettingCheckUpdateFragment";
    View root;
    LinearLayout rootLL;
    SettingsTheme settingsTheme;
    TextView currentVersionTV;
    Button updateButton;
    TextView lastUpdateTV;
    TextView pleaseCheckTV;
    TextView updateDetectedTV;
    Button checkUpdateButton;

    ColorStateList colorSelection, colorText;
    STBControl stbControl = new STBControl();

    public SettingCheckUpdateFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        root = inflater.inflate(R.layout.fragment_setting_check_update, container, false);
        try {
            bind();
            setActionObject();
            settingsTheme = new SettingsTheme(getActivity(), this);
            settingsTheme.setTheme();
            int versionCode = BuildConfig.VERSION_CODE;
            String versionName = BuildConfig.VERSION_NAME;
            currentVersionTV.setText(versionName);
        } catch (Exception ex) {
            Debug.e(parentTAG, ex);
        }
        return root;
    }

    @Override
    public void setThemeUpdateDate(Date lastUpdateTheme) {

    }

    @Override
    public void setListColorTheme(int color) {
        try {
            rootLL.setBackgroundResource(R.drawable.tags_rounded_corners);

            GradientDrawable categoryDrawable = (GradientDrawable) rootLL.getBackground();
            categoryDrawable.setColor(color);
        } catch (Exception ex) {
            Debug.e(parentTAG, ex);
        }
    }

    @Override
    public void setTextAndSelectionColorTheme(ColorStateList colorSelection, ColorStateList colorText, int colorTextSelected, int colorTextFocus, int colorTextNormal, int colorSelectionSelected, int colorSelectionFocus) {
        try {
            this.colorSelection = colorSelection;
            this.colorText = colorText;
        } catch (Exception ex) {
            Debug.e(parentTAG, ex);
        }
    }

    void bind() {
        try {
            rootLL = root.findViewById(R.id.rootLL);
            currentVersionTV = root.findViewById(R.id.currentVersionTV);
            updateButton = root.findViewById(R.id.updateButton);
            lastUpdateTV = root.findViewById(R.id.lastUpdateTV);
            pleaseCheckTV = root.findViewById(R.id.pleaseCheckTV);
            updateDetectedTV = root.findViewById(R.id.updateDetectedTV);
            checkUpdateButton = root.findViewById(R.id.checkUpdateButton);
            String lastUpdate = Global.getLastUpdate();
            if (lastUpdate.equals(""))
                lastUpdate = getString(R.string.never_updated);

            lastUpdateTV.setText(lastUpdate);
        } catch (Exception ex) {
            Debug.e(parentTAG, ex);
        }
    }

    void setActionObject() {
        try {
            updateButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                }
            });
            checkUpdateButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (C.UPDATING_APK == false)
                        fetchAPKList();
                }
            });
        } catch (Exception ex) {
            Debug.e(parentTAG, ex);
        }
    }

    void fetchAPKList() {
        try {
//            Global.setPathUpdate("");
            String pathUpdateAPK = Global.getPathUpdate();
            if (pathUpdateAPK.equals("")) {
                updateSTBInfo();

            } else {
                int currentVersion = getActivity().getPackageManager().getPackageInfo(getActivity().getPackageName(), 0).versionCode;
                int nextAppCode = Global.getNextAppCode();
                int flagUpdateAgain = nextAppCode - currentVersion;
                if (flagUpdateAgain > 1) {
                    updateSTBInfo();
                } else {
                    stbControl.updateApp(getActivity());
                }
            }
          /*  PerformAsync2.run(performAsync ->
            {
                List<TLocalFileAPK> tLocalFileAPKArrayList = new ArrayList<>();
                try {
                    DbAccess dbAccess = DbAccess.Instance.create(getActivity());
                    tLocalFileAPKArrayList = dbAccess.daoAccessTLocalFileAPK().getAll();
                    dbAccess.close();
                } catch (Exception ex) {

                }
                return tLocalFileAPKArrayList;
            }).setCallbackResult(result ->
            {
                boolean fgAPKnotFound = true;
                List<TLocalFileAPK> tLocalFileAPKList = (List<TLocalFileAPK>) result;
                if (tLocalFileAPKList.size() != 0) {
                    for (TLocalFileAPK item : tLocalFileAPKList) {
                        List<String> apkInfo = Util.getAPKInfo(getActivity(), item.content);
                        int currentVersionCode = BuildConfig.VERSION_CODE;
                        int versionCode = 0;
                        String versionName;
                        String packageName = null;
                        if (apkInfo.size() == 3) {
                            packageName = apkInfo.get(0);
                            versionCode = Integer.valueOf(apkInfo.get(1));
                            versionName = apkInfo.get(2);
                        }
                        if (packageName.equals(BuildConfig.APPLICATION_ID)) {
                            fgAPKnotFound = false;
                            if (versionCode > currentVersionCode) {
                                showUpdateAlert(item.content);
                            } else {
//                            Toasty.info(getActivity(), "This app is latest version").show();

                                showAlreadyUpdateAlert();
                            }
                            break;
                        }
                    }
                    if (fgAPKnotFound)
                        showUpdateNotFoundAlert();
                } else {
                    showUpdateNotFoundAlert();
                }
            });*/
        } catch (Exception ex) {
            Debug.e(parentTAG, ex);
        }
    }


    void showUpdateAlertContinue(String path) {
        try {
            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
            LayoutInflater inflater = this.getLayoutInflater();
            View dialogView = inflater.inflate(R.layout.alert_confirm_update, null);
            dialogBuilder.setView(dialogView);

            TextView textView = (TextView) dialogView.findViewById(R.id.textView);
            Button updateButton = (Button) dialogView.findViewById(R.id.updateButton);
            Button cancelButton = (Button) dialogView.findViewById(R.id.cancelButton);
            String text = getString(R.string.updateInformation_continue);
            textView.setText(text);
            updateButton.setText(getString(R.string.updateApp_continue));

            AlertDialog alertDialog = dialogBuilder.create();

            updateButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    alertDialog.dismiss();
                    updateLastUpdateApp(path);

                }
            });

            cancelButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    alertDialog.dismiss();
                }
            });

            alertDialog.show();
            WindowManager.LayoutParams layoutParams = alertDialog.getWindow().getAttributes();
            layoutParams.width = ViewGroup.LayoutParams.WRAP_CONTENT;
            alertDialog.show();
            alertDialog.getWindow().setLayout(430, WindowManager.LayoutParams.WRAP_CONTENT);
            Util.hideNavigationBar(getActivity());


        } catch (Exception ex) {
            Debug.e(parentTAG, ex);
        }

    }


    void showAlreadyUpdateAlert() {
        try {
            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
            LayoutInflater inflater = this.getLayoutInflater();
            View dialogView = inflater.inflate(R.layout.alert_already_update, null);
            dialogBuilder.setView(dialogView);

            TextView textView = (TextView) dialogView.findViewById(R.id.textView);
            Button updateButton = (Button) dialogView.findViewById(R.id.okButton);
            textView.setText(getString(R.string.alreadyLastVersion));

            AlertDialog alertDialog = dialogBuilder.create();

            updateButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    alertDialog.dismiss();

                }
            });

            alertDialog.show();
            WindowManager.LayoutParams layoutParams = alertDialog.getWindow().getAttributes();
            layoutParams.width = ViewGroup.LayoutParams.WRAP_CONTENT;
            alertDialog.show();
            alertDialog.getWindow().setLayout(430, WindowManager.LayoutParams.WRAP_CONTENT);
            Util.hideNavigationBar(getActivity());
        } catch (Exception ex) {
            Debug.e(parentTAG, ex);
        }

    }


    void updateLastUpdateApp(String path) {

        try {
            Date c = Calendar.getInstance().getTime();
            System.out.println("Current time => " + c);
            SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
            String formattedDate = df.format(c);

            Global.setLastUpdate(formattedDate);
            String packageName = getActivity().getPackageName();
            Global.setPathUpdate("");
            Util.installAPK(path, packageName);
//            Util.install(getActivity(),packageName,path);
        } catch (Exception ex) {
            Debug.e(parentTAG, ex);
        }
    }

    void showUpdateNotFoundAlert() {
        try {
            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
            LayoutInflater inflater = this.getLayoutInflater();
            View dialogView = inflater.inflate(R.layout.alert_already_update, null);
            dialogBuilder.setView(dialogView);

            TextView textView = (TextView) dialogView.findViewById(R.id.textView);
            Button updateButton = (Button) dialogView.findViewById(R.id.okButton);
            textView.setText(getString(R.string.updateNotFound));

            AlertDialog alertDialog = dialogBuilder.create();

            updateButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    alertDialog.dismiss();

                }
            });

            alertDialog.show();
            WindowManager.LayoutParams layoutParams = alertDialog.getWindow().getAttributes();
            layoutParams.width = ViewGroup.LayoutParams.WRAP_CONTENT;
            alertDialog.show();
            alertDialog.getWindow().setLayout(430, WindowManager.LayoutParams.WRAP_CONTENT);
            Util.hideNavigationBar(getActivity());
        } catch (Exception ex) {
            Debug.e(parentTAG, ex);
        }

    }

    void updateSTBInfo() {
        String TAG = parentTAG + "-" + "updateSTBInfo";
        try {
            String appId = getActivity().getPackageName();
            String mac = Util.getMACAddress("eth0");
            mac = mac.replace(":", "");
            String versionCode = String.valueOf(BuildConfig.VERSION_CODE);
            String versionName = BuildConfig.VERSION_NAME;
            String androidVersion = String.valueOf(Build.VERSION.SDK_INT);

            ModelSTB.updateSTBInfo(mac, appId, versionCode, versionName, androidVersion, new ModelSTB.IUpdateSTBInfo() {
                @Override
                public void onUpdateSTBInfo(ModelApp.appItem result) {
                    if (result != null) {
//                        if (Global.getPathUpdate().equals(""))
                        checkUpdate(result);
                    }
                }

                @Override
                public void onError(ANError e) {

                }
            });
        } catch (Exception ex) {
            ex.printStackTrace();
            Debug.e(TAG, ex);
        }
    }

    //update apk
    void checkUpdate(ModelApp.appItem result) {
        try {
            int currentVersion = getActivity().getPackageManager().getPackageInfo(getActivity().getPackageName(), 0).versionCode;
            if (currentVersion == Integer.valueOf(result.app.versionCode)) {
                showAlreadyUpdateAlert();
                return;
            }

            boolean alreadyUpdate = false;
            alreadyUpdate = stbControl.downloadUpdate(getActivity(), result);
            if (alreadyUpdate)
                stbControl.updateApp(getActivity());
        } catch (Exception ex) {
            Debug.e(parentTAG, ex);
        }


       /* //check update app
        ModelApp.subAppItem subAppItem = result.app;
        int currentVersionCode = BuildConfig.VERSION_CODE;
        if (Integer.valueOf(subAppItem.versionCode) > currentVersionCode) {
            Global.setNextAppCodeName(subAppItem.versionName);
            boolean fgDownloadAPK = false;
            String pathUpdate = Global.getPathUpdate();
            if (pathUpdate.equals(""))
                fgDownloadAPK = true;
            else {
                List<String> apkInfo = Util.getAPKInfo(getActivity(), pathUpdate);
                int versionCode = 0;
                String versionName;
                String packageName = null;
                if (apkInfo.size() == 3) {
                    packageName = apkInfo.get(0);
                    versionCode = Integer.valueOf(apkInfo.get(1));
                    versionName = apkInfo.get(2);
                }

                if (versionCode > currentVersionCode)
                    fgDownloadAPK = true;

            }
            if (fgDownloadAPK) {
                String appDir = getActivity().getFilesDir().toString();
                String[] fileNameArray = subAppItem.urlApk.split("/");
                String fileName = fileNameArray[fileNameArray.length - 1];

                AndroidNetworking.download(subAppItem.urlApk, appDir, fileName).build().startDownload(new DownloadListener() {
                    @Override
                    public void onDownloadComplete() {
                        String usbPath = Global.getUSBPath() + "/" + C.PATH_EKB_APK + "/" + fileName;
                        if (usbPath != null) {
                            String pathAPK = appDir + "/" + fileName;
                            Global.setPathUpdate(pathAPK);
                            String result = RootUtil.copyFile(pathAPK, usbPath);
                            if (result.equals(""))
//                                showUpdateAlert(usbPath);
                                stbControl.updateApp(getActivity());
                        }
                        Toasty.info(getActivity(), getString(R.string.downloadUpdate)).show();
                        stbControl.updateApp(getActivity());
//                        showUpdateAlert(usbPath);

                    }

                    @Override
                    public void onError(ANError anError) {
                        Global.setPathUpdate("");
                    }
                });
            }
            else
            {
                showAlreadyUpdateAlert();
            }
        }
        else
        {
            showAlreadyUpdateAlert();
        }*/
    }


}

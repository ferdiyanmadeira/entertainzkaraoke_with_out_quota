package com.madeira.entertainz.karaoke.DBLocal;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;

@Entity(tableName = "tsongcategory")
public class TSongCategory {

    @PrimaryKey
    @SerializedName("songCategoryId")
    public int songCategoryId;
    @SerializedName("songCategory")
    public String songCategory;
}

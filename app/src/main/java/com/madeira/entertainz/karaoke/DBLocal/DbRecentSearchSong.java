package com.madeira.entertainz.karaoke.DBLocal;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Database;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

import java.util.List;


@Database(entities = { TRecentSearchSong.class},
        version = 3, exportSchema = false)

public abstract class DbRecentSearchSong extends RoomDatabase {
    public abstract DaoAccessRecentSearchSong daoAccessRecentSearchSong();

    public static class Instance {
        private static final String DB_THEME = "RecentSearchSong.db";

        public static DbRecentSearchSong create(Context context) {
            return Room.databaseBuilder(context, DbRecentSearchSong.class, DB_THEME)
                    .fallbackToDestructiveMigration()
//                    .addMigrations(MIGRATION_1_2)
                    .build();
        }

        //example migration jangan digunakan dulu, masih tahap research
      /*  static final Migration MIGRATION_1_2 = new Migration(1, 2) {
            @Override
            public void migrate(SupportSQLiteDatabase database) {
                // Your migration strategy here
                database.execSQL("ALTER TABLE Users ADD COLUMN user_score INTEGER");
            }
        };*/
    }

    @Dao
    public interface DaoAccessRecentSearchSong {

        @Insert(onConflict = OnConflictStrategy.REPLACE)
        void insert(TRecentSearchSong tRecentSearchSong);

        @Query("SELECT * FROM trecent_search_song where category =:category order by recentId desc")
        List<TRecentSearchSong> getAll(int category);

        @Query("DELETE FROM trecent_search_song")
        void delete();

        @Query("SELECT * FROM trecent_search_song where trim(lower(recentSong))=trim(lower(:songName)) and category=:category")
        TRecentSearchSong checkSong(String songName, int category);

        @Query("DELETE FROM trecent_search_song where recentId=:recentId")
        void deleteById(int recentId);

        @Query("SELECT count(*) FROM trecent_search_song where category =:category")
        int countRow(int category);

    }
}
package com.madeira.entertainz.karaoke.menu_settings;


import android.content.res.ColorStateList;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.ScaleAnimation;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;

import com.madeira.entertainz.helper.RecyclerViewAdapter;
import com.madeira.entertainz.karaoke.DBLocal.DbAccess;
import com.madeira.entertainz.karaoke.DBLocal.JoinSettingElementMedia;
import com.madeira.entertainz.karaoke.DBLocal.TSong;
import com.madeira.entertainz.karaoke.DBLocal.TSongItemServer;
import com.madeira.entertainz.karaoke.Debug;
import com.madeira.entertainz.karaoke.R;
import com.madeira.entertainz.karaoke.config.Global;
import com.madeira.entertainz.karaoke.model_online.ModelSong;
import com.madeira.entertainz.karaoke.model_room_db.ModelElementMedia;
import com.madeira.entertainz.library.PerformAsync2;
import com.madeira.entertainz.library.UtilRenderScript;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import es.dmoral.toasty.Toasty;

/**
 * A simple {@link Fragment} subclass.
 */
public class SettingBackgroundFragment extends Fragment implements SettingsTheme.ISettingsTheme {
    String TAG = "SettingBackgroundFragment";

    View root;
    LinearLayout rootLL;
    Spinner timingSpinner;
    RecyclerView recyclerView;

    SettingsTheme settingsTheme;
    List<String> timingList = new ArrayList<>();
    List<Integer> codeTimingList = new ArrayList<>();
    RecyclerViewAdapter adapter;
    List<JoinSettingElementMedia> joinSettingElementMedia = new ArrayList<>();
//    public boolean isMultipleSelection = false;

    public SettingBackgroundFragment() {
        // Required empty public constructor
    }

    public interface ISong {
        void onUpdate(TSongItemServer songItem, TSong tSong);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        root = inflater.inflate(R.layout.fragment_setting_background, container, false);
        try {
            bind();
            setActionObject();
            settingsTheme = new SettingsTheme(getActivity(), this);
            settingsTheme.setTheme();
            setSpinnerAdapter();
            fetchMedia();
            setSelectedSpinner();
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
        return root;
    }

    @Override
    public void setThemeUpdateDate(Date lastUpdateTheme) {

    }

    @Override
    public void setListColorTheme(int color) {

        try {
            rootLL.setBackgroundResource(R.drawable.tags_rounded_corners);

            GradientDrawable categoryDrawable = (GradientDrawable) rootLL.getBackground();
            categoryDrawable.setColor(color);
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    @Override
    public void setTextAndSelectionColorTheme(ColorStateList colorSelection, ColorStateList colorText, int colorTextSelected, int colorTextFocus, int colorTextNormal, int colorSelectionSelected, int colorSelectionFocus) {
        try {
            this.colorSelection = colorSelection;
            this.colorText = colorText;
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    void bind() {
        try {
            rootLL = root.findViewById(R.id.rootLL);
            timingSpinner = root.findViewById(R.id.timingSpinner);
            recyclerView = root.findViewById(R.id.recyclerView);
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    void setSpinnerAdapter() {
        try {
            codeTimingList = new ArrayList<>();
            codeTimingList.add(120000);
            codeTimingList.add(300000);
            codeTimingList.add(600000);
            codeTimingList.add(1800000);
            codeTimingList.add(3600000);


            timingList = new ArrayList<>();
            timingList.add("2 Minutes");
            timingList.add("5 Minutes");
            timingList.add("10 Minutes");
            timingList.add("30 Minutes");
            timingList.add("60 Minutes");

            ArrayAdapter<String> adapter = new ArrayAdapter<>(getActivity(),
                    R.layout.cell_language_list, timingList);
            adapter.setDropDownViewResource(R.layout.cell_language_list);
            timingSpinner.setAdapter(adapter);
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    void fetchMedia() {
        ModelElementMedia.getListElementMedia(getContext(), new ModelElementMedia.IGetListElementMedia() {
            @Override
            public void onGetListElementMedia(List<JoinSettingElementMedia> joinSettingElementMediaList) {
                try {
                    joinSettingElementMedia = joinSettingElementMediaList;
                    setRecyclerView();
                } catch (Exception ex) {
                    Debug.e(TAG, ex);
                }
            }
        });
       /* try {
            PerformAsync2.run(performAsync ->
            {
                List<JoinSettingElementMedia> result = new ArrayList<>();
                try {
                    DbAccess dbAccess = DbAccess.Instance.create(getContext());
                    result = dbAccess.daoAccessThemeMedia().joinSettingElementMedia();
                    dbAccess.close();
                } catch (Exception ex) {

                }
                return result;
            }).setCallbackResult(result ->
            {
                List<JoinSettingElementMedia> joinSettingElementMediaList = (List<JoinSettingElementMedia>) result;

                joinSettingElementMedia = joinSettingElementMediaList;
                setRecyclerView();
            });
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }*/
    }

    void setRecyclerView() {
        try {
            adapter = new RecyclerViewAdapter(new RecyclerViewAdapter.IRecyclerViewAdapter() {
                @Override
                public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
                    View view = LayoutInflater.from(getActivity()).inflate(R.layout.cell_setting_background, parent, false);
                    ItemViewHolder item = new ItemViewHolder(view);
                    return item;
                }

                @Override
                public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
                    ItemViewHolder item = (ItemViewHolder) holder;
                    item.bind(joinSettingElementMedia.get(position), position);
                }

                @Override
                public int getItemCount() {
                    return joinSettingElementMedia.size();
                }
            });

            GridLayoutManager gridLayoutManager = new GridLayoutManager(getActivity(), 3);
            recyclerView.setLayoutManager(gridLayoutManager);
            adapter.setHasStableIds(true);
            recyclerView.setAdapter(adapter);
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    static View selectedView;

    class ItemViewHolder extends RecyclerView.ViewHolder {
        private static final String TAG = "ItemViewHolder";

        View root;
        ImageView thumbnailIV;
        ImageView isActiveIV;

        public ItemViewHolder(View itemView) {
            super(itemView);
            root = itemView;
            selectedView = root;
            thumbnailIV = itemView.findViewById(R.id.thumbnailIV);
            isActiveIV = itemView.findViewById(R.id.isActiveIV);
        }

        public void bind(JoinSettingElementMedia joinSettingElementMedia, int pos) {
            try {
                if (joinSettingElementMedia.isActive == 1)
                    isActiveIV.setVisibility(View.VISIBLE);
                else
                    isActiveIV.setVisibility(View.GONE);

                if (joinSettingElementMedia.urlImage != null) {
                    UtilRenderScript.fetchBitmap(getContext(), joinSettingElementMedia.urlImage, 300, thumbnailIV);

                }

                if (colorSelection != null) root.setBackgroundTintList(colorSelection);
//            untuk merubah warna text
                if (colorText != null) {
//                songNameTV.setTextColor(colorText);
//                singerTV.setTextColor(colorText);
                }


                root.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
//                    mListener.onListClick(joinSettingElementMediaActive.songId);
//                    if (isMultipleSelection) {
                        if (isActiveIV.getVisibility() == View.VISIBLE) {
                            isActiveIV.setVisibility(View.GONE);
                            setActiveBackground(joinSettingElementMedia.elementMediaId, 0);
                        } else {
                            isActiveIV.setVisibility(View.VISIBLE);
                            setActiveBackground(joinSettingElementMedia.elementMediaId, 1);
                        }
//                    }
                    }
                });
                root.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                    @Override
                    public void onFocusChange(View v, boolean hasFocus) {
                        if (hasFocus) {
                            updateSelectedViewUi(v, true);
                        } else {
                            updateSelectedViewUi(null, false);

                        }
                    }
                });

           /* root.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    isMultipleSelection = true;
                  *//*  if(isActiveIV.getVisibility()==View.VISIBLE)
                        isActiveIV.setVisibility(View.GONE);
                    else
                        isActiveIV.setVisibility(View.VISIBLE);*//*
                    return false;
                }
            });*/
            } catch (Exception ex) {
                Debug.e(TAG, ex);
            }
        }


    }

    ColorStateList colorSelection, colorText;

    void updateSelectedViewUi(View view, boolean hasFocus) {
        try {
            if (selectedView != null) {
                scaleView(selectedView, 1.1f, 1);
                ViewCompat.setTranslationZ(selectedView, 0);
            }

            if (hasFocus) {
                selectedView = view;
                ViewCompat.setTranslationZ(selectedView, 100);
                scaleView(selectedView, 1, 1.1f);
            } else {
                selectedView = null;
            }
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    void scaleView(View v, float startScale, float endScale) {
        try {
            Animation anim = new ScaleAnimation(
                    startScale, endScale, // Start and end values for the X axis scaling
                    startScale, endScale, // Start and end values for the Y axis scaling
                    Animation.RELATIVE_TO_SELF, 0.5f, // Pivot point of X scaling
                    Animation.RELATIVE_TO_SELF, 0.5f); // Pivot point of Y scaling
            anim.setFillAfter(true); // Needed to keep the result of the animation
            anim.setDuration(100);
            v.startAnimation(anim);
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    void setActiveBackground(int mediaId, int prmIsActive) {
        ModelElementMedia.updateSettingMedia(getContext(), mediaId, prmIsActive, new ModelElementMedia.IUpdateSettingMedia() {
            @Override
            public void onUpdateSettingMedia(int result) {
                if (result != 1) {
                    Toasty.error(getContext(), getString(R.string.failedChangeBackground)).show();
                }
            }
        });

    }

    void setActionObject() {
        timingSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                int duration = codeTimingList.get(position);
                if (duration != Global.getDuration())
                    updateAllDuration(duration);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    void updateAllDuration(int duration) {
        ModelElementMedia.updateDurationSettingMedia(getContext(), duration, new ModelElementMedia.IUpdateDurationSettingMedia() {
            @Override
            public void onUpdateDurationSettingMedia(int result) {
                if (result == 1) {
                    Toasty.success(getContext(), "Success for update background duration").show();
                    Global.setDuration(duration);
                } else
                    Toasty.error(getContext(), "Failed for update background duration").show();
            }
        });
       /* try {
            PerformAsync2.run(performAsync ->
            {
                int result = 0;
                try {
                    DbAccess dbAccess = DbAccess.Instance.create(getContext());
                    dbAccess.daoAccessThemeMedia().updateAllDuration(duration);
                    dbAccess.close();
                    result = 1;
                } catch (Exception ex) {

                }
                return result;
            }).setCallbackResult(result ->
            {
                int r = (int) result;
                if (r == 1) {
                    Toasty.success(getContext(), "Success for update background duration").show();
                    Global.setDuration(duration);
                } else
                    Toasty.error(getContext(), "Failed for update background duration").show();
            });
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }*/
    }

    void setSelectedSpinner() {
        try {
            int duration = Global.getDuration();
            if (duration != 0) {
                int index = codeTimingList.indexOf(duration);
                if (index != -1)
                    timingSpinner.setSelection(index);
            }
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }


}

package com.madeira.entertainz.karaoke.DBLocal;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;

@Entity(tableName = "tmessage")
public class TMessage {

    @PrimaryKey
    @SerializedName("message_id")
    public int messageId;
    @SerializedName("from")
    public String from;
    @SerializedName("title")
    public String title;
    public String message;
    @SerializedName("status")
    public String status;
    @SerializedName("create_date")
    public String createDate;
    @SerializedName("update_date")
    public String updateDate;
    @SerializedName("url_image")
    public String urlImage;
    @SerializedName("url_video")
    public String urlVideo;
}


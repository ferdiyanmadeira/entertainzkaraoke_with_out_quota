package com.madeira.entertainz.karaoke.DBLocal;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Database;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

import java.util.List;

@Database(entities = {TSongYoutube.class, TSongYoutubeCategory.class, TSongYoutubePlaylist.class, TYoutubePlaylist.class,},
        version = 7, exportSchema = false)

public abstract class DbYoutube extends RoomDatabase {

    public abstract DaoAccessTSongYoutube daoAccessTSongYoutube();

    public abstract DaoAccessTSongYoutubeCategory daoAccessTSongYoutubeCategory();

    public abstract DaoAccessTSongYoutubePlaylist daoAccessTSongYoutubePlaylist();

    public abstract DaoAccessTYoutubePlaylist daoAccessTYoutubePlaylist();

    public static class Instance {
        private static final String DB_THEME = "youtube.db";

        public static DbYoutube create(Context context) {
            return Room.databaseBuilder(context, DbYoutube.class, DB_THEME)
                    .fallbackToDestructiveMigration()
//                    .addMigrations(MIGRATION_1_2)
                    .build();
        }

        //example migration jangan digunakan dulu, masih tahap research
      /*  static final Migration MIGRATION_1_2 = new Migration(1, 2) {
            @Override
            public void migrate(SupportSQLiteDatabase database) {
                // Your migration strategy here
                database.execSQL("ALTER TABLE Users ADD COLUMN user_score INTEGER");
            }
        };*/
    }

    @Dao
    public interface DaoAccessTSongYoutubeCategory {

        @Insert(onConflict = OnConflictStrategy.REPLACE)
        void insertAll(List<TSongYoutubeCategory> list);

        @Query("SELECT * FROM TSongYoutubecategory")
        List<TSongYoutubeCategory> getAll();

        @Query("DELETE FROM TSongYoutubecategory")
        void deleteAll();
    }

    @Dao
    public interface DaoAccessTSongYoutube {

        @Insert(onConflict = OnConflictStrategy.REPLACE)
        void insert(TSongYoutube item);

        @Insert(onConflict = OnConflictStrategy.REPLACE)
        void insertAll(List<TSongYoutube> list);

        @Query("SELECT * FROM TSongYoutube order by songId desc")
        List<TSongYoutube> getAll();

        @Query("SELECT * FROM TSongYoutube WHERE songCategoryId=:songCategoryId order by songId desc")
        List<TSongYoutube> getByCategory(int songCategoryId);

        @Query("DELETE FROM TSongYoutube")
        void deleteAll();

        @Query("DELETE FROM TSongYoutube where songId=:songId")
        void deleteWithId(String songId);

        @Query("update TSongYoutube set songName=:songName, songCategoryId=:songCategoryId, lyric=:lyric  where songId=:songId")
        void update(String songId, String songName, int songCategoryId, String lyric);

        @Query("SELECT * FROM TSongYoutube WHERE songCategoryId=:songCategoryId and (artist like '%'||:keyword||'%' or songName like '%'||:keyword||'%')")
        List<TSongYoutube> getSearchByCategory(int songCategoryId, String keyword);

        @Query("SELECT * FROM TSongYoutube WHERE  (artist like '%'||:keyword||'%' or songName like '%'||:keyword||'%')")
        List<TSongYoutube> getSearchAll(String keyword);

        @Query("SELECT * FROM TSongYoutube WHERE songId=:songId")
        TSongYoutube geTSongYoutubeById(int songId);
    }

    @Dao
    public interface DaoAccessTSongYoutubePlaylist {

        @Insert(onConflict = OnConflictStrategy.REPLACE)
        void insertAll(List<TSongYoutubePlaylist> list);

        @Insert(onConflict = OnConflictStrategy.REPLACE)
        void insert(TSongYoutubePlaylist TSongYoutubePlaylist);

        @Query("SELECT * FROM TSongYoutubePlaylist")
        List<TSongYoutubePlaylist> getAll();

        @Query("DELETE FROM TSongYoutubeplaylist WHERE songId=:songId and playlistId=:playlistId")
        void delete(int playlistId, String songId);

        @Query("DELETE FROM TSongYoutubePlaylist")
        void deleteAll();

        @Query("SELECT count(*) FROM TSongYoutubePlaylist where songId=:songId and playlistId=:playlistId")
        int exisTSongYoutubePlaylistBySongId(String songId, int playlistId);

        /*@Query("SELECT a.*,b.songName, b.artist, b.songURL, b.vocalSound, b.duration,b.thumbnail,b.lyric FROM TSongYoutubePlaylist a left join  TSongYoutube b on a.songId = b. songId WHERE a.playlistId=:playlistId")
        List<JoinSongYoutubePlaylist> getByPlaylistId(int playlistId);
         */
        @Query("SELECT a.* FROM TSongYoutubePlaylist a  WHERE a.playlistId=:playlistId ORDER BY ord")
        List<TSongYoutubePlaylist> getByPlaylistId(int playlistId);

        @Query("DELETE FROM TSongYoutubeplaylist WHERE playlistId=:playlistId")
        void deleteByPlaylist(int playlistId);

    }

    @Dao
    public interface DaoAccessTYoutubePlaylist {

        @Insert(onConflict = OnConflictStrategy.REPLACE)
        void insertAll(List<TYoutubePlaylist> list);

        @Insert(onConflict = OnConflictStrategy.REPLACE)
        void insert(TYoutubePlaylist tYoutubePlaylist);

        @Query("SELECT * FROM TYoutubePlaylist")
        List<TYoutubePlaylist> getAll();

        @Query("DELETE FROM TYoutubePlaylist WHERE playlistId=:playlistId")
        void delete(int playlistId);

        @Query("DELETE FROM TYoutubePlaylist")
        void deleteAll();

        @Query("UPDATE TYoutubePlaylist SET playlistName=:playlistName WHERE playlistId=:playlistId")
        void renamePlaylist(int playlistId, String playlistName);

        @Query("SELECT * FROM TYoutubePlaylist order by playlistId desc limit 0,1")
        int lastId();

        @Query("SELECT a.playlistId, a.playlistName, count(b.songId) as sumOfSong, sum(b.duration) as sumOfDuration FROM TYoutubePlaylist a left join tsongyoutubeplaylist b on a.playlistId = b.playlistId left join TSongYoutube c on b.songId = c.songId group by b.playlistId ")
        List<JoinYoutubePlaylistDt> joinPlaylist();

        @Query("SELECT count(*) FROM TYoutubePlaylist where lower(playlistName)=lower(:playlistName)")
        int existPlaylistName(String playlistName);

        @Query("SELECT * FROM TYoutubePlaylist where playlistId=:playlistId")
        TYoutubePlaylist getById(int playlistId);

        @Query("SELECT count(*) FROM TYoutubePlaylist")
        int checkExistPlaylist();
    }

}
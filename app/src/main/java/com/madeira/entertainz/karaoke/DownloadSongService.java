package com.madeira.entertainz.karaoke;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.DownloadListener;
import com.androidnetworking.interfaces.DownloadProgressListener;
import com.madeira.entertainz.controller.USBControl;
import com.madeira.entertainz.karaoke.DBLocal.DbAccess;
import com.madeira.entertainz.karaoke.DBLocal.DbSong;
import com.madeira.entertainz.karaoke.DBLocal.TLocalFile;
import com.madeira.entertainz.karaoke.DBLocal.TSong;
import com.madeira.entertainz.karaoke.DBLocal.TSongItemServer;
import com.madeira.entertainz.karaoke.config.C;
import com.madeira.entertainz.karaoke.config.CacheData;
import com.madeira.entertainz.karaoke.config.Global;
import com.madeira.entertainz.karaoke.model_room_db.ModelSong;
import com.madeira.entertainz.library.Bps;
import com.madeira.entertainz.library.PerformAsync2;
import com.madeira.entertainz.library.RootUtil;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import es.dmoral.toasty.Toasty;


public class DownloadSongService extends Service {
    static String TAG = "DownloadSongService";
    public static final String KEY_SONGITEMLIST = "SONG_ITEM_LIST";
    public static final String KEY_TOTAL_SIZE = "TOTAL_SIZE_SONG";
    public static final String KEY_DOWNLOADED_SIZE = "DOWNLOADED_SIZE";
    public static final String KEY_DOWNLOAD_SPEED = "DOWNLOAD_SPEED";
    public static final String KEY_DOWNLOAD_ITEM_ORDER = "DOWNLOAD_ITEM_ORDER";
    public static final String KEY_SUM_ITEM_DOWNLOAD = "SUM_ITEM_DOWNLOAD";
    public static final String KEY_DOWNLOADED_IN_KB = "DOWNLOADED_IN_KB";
    public static final String KEY_TOTAL_SIZE_IN_KB = "TOTAL_SIZE_IN_KB";
    public static final String KEY_RESULT_DOWNLOAD = "RESULT_DOWNLOAD";

    Runnable runnable;
    Handler handler = new Handler();
    int downloadPosition = 0;
    ArrayList<TSongItemServer> songItemArrayList = new ArrayList<>();
    DbSong dbSong;
    USBControl usbControl = new USBControl();
    ModelSong modelSong = new ModelSong();

    public interface IDownloadSong {
        void onDownloading(String totalMb, String downloadedAndSpeedMB);
    }

    public interface IFinishDownload {
        void onFinish();
    }

    public DownloadSongService() {

    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        dbSong = DbSong.Instance.create(getApplicationContext());
        songItemArrayList = (ArrayList<TSongItemServer>) intent.getSerializableExtra(KEY_SONGITEMLIST);
       /* if (songItemArrayList != null)
            deleteSong();*/
        download();
        //save ke local db untuk jaga2 ketika stb mendadak mati ketika downloading, dan akan resume ketika stb bootup kembali
        saveSongToLocal(songItemArrayList);
        return START_NOT_STICKY;
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }


    void deleteSong() {
        PerformAsync2.run(performAsync ->
        {
            List<TSong> tSongArrayList = new ArrayList<>();
            try {
                tSongArrayList = dbSong.daoAccessTSong().getAllTSong();
            } catch (Exception ex) {

            }
            return tSongArrayList;
        }).setCallbackResult(result ->
        {
            try {
                List<TSong> tSongList = (List<TSong>) result;
                for (TSong tSong : tSongList) {
                    boolean hasKey = CacheData.hashSongNameNArtis.containsKey(String.valueOf(tSong.songId));
                    if (!hasKey) {
                        File thumbnailFile = new File(tSong.thumbnail);
                        if (thumbnailFile.exists())
                            RootUtil.deleteFile(tSong.thumbnail);
                        File songFile = new File(tSong.songURL);
                        if (songFile.exists())
                            RootUtil.deleteFile(tSong.songURL);
//                        deleteSongFromRoom(Integer.valueOf(tSong.songId));
                    }
                }
                CacheData.hashSongNameNArtis.clear();
                download();
            } catch (Exception ex) {
                Debug.e(TAG, ex);

            }
        });
    }

    void deleteSongFromRoom(int songId) {
        PerformAsync2.run(performAsync ->
        {
            int result = 0;
            try {
                DbAccess dbAccess = DbAccess.Instance.create(getApplicationContext());
                dbAccess.close();
                result = 1;
            } catch (Exception ex) {
                Debug.e(TAG, ex);
            }
            return result;
        }).setCallbackResult(result ->
        {
            int r = (int) result;
            if (r == 1) {

            }
        });
    }

    void download() {

        runnable = new Runnable() {
            @Override
            public void run() {
                try {
                    if (downloadPosition < songItemArrayList.size()) {
                        downloadSong();
                    } else {
                        downloadFinish();
                    }
                } catch (Exception ex) {
                    Debug.e(TAG, ex);
                }

            }
        };
        handler.postDelayed(runnable, 1);
    }

    void downloadSong() {
        try {
            String appFileDir = getFilesDir().toString();

            TSongItemServer songItem = songItemArrayList.get(downloadPosition);
            modelSong.getSongById(getApplicationContext(), songItem.songId, new ModelSong.IGetSongById() {
                @Override
                public void onGeSongById(TSong tSong) {
                    //jika song belum didownload
                    try {
                        if (tSong == null) {
                            //check file sudah pernah didownload atau belum
                            String song = songItem.songPath;
                            String[] fileNameSongArray = song.split("/");
                            String fileNameSong = fileNameSongArray[fileNameSongArray.length - 1];

                            TLocalFile tLocalFile = CacheData.hashFile.get(fileNameSong);
                            Boolean fileExist = false;
                            if (tLocalFile != null) {
                                File existFile = new File(tLocalFile.content);
                                if (existFile.length() == Long.valueOf(songItem.filesize))
                                    fileExist = true;
                            }

                            if (fileExist == false) {

                                //check folder exist atau tidak, jika tidak maka create folder
                                String pathSong = Global.getUSBPath() + "/" + C.PATH_VIDEO_KARAOKE;
                                String tempSong = getFilesDir() + "/" + fileNameSong;

                                File rootSong = new File(pathSong);
                                File[] listDirSong = rootSong.listFiles();
                                List<File> listFolder = new ArrayList<>();
                                for (File itemFile : listDirSong) {
                                    if (itemFile.isDirectory())
                                        listFolder.add(itemFile);
                                }
                                int lastFolder = 0;
                                if (listFolder.size() == 0) {
                                    String newDir = pathSong + "/" + String.valueOf(1);
                                    String msg = RootUtil.createDir(newDir);
                                    if (msg.equals(""))
                                        lastFolder = 1;
                                } else {
                                    boolean fgCreateDir = true;
                                    int i = 1;
                                    for (File file : listFolder) {
                                        File[] listFiles = file.listFiles();
                                        if (listFiles.length < C.LIMIT_SONG_IN_FOLDER) {
                                            lastFolder = i;
                                            fgCreateDir = false;
                                            break;
                                        }
                                        i++;
                                    }
                                    if (fgCreateDir) {
                                        int nextFolder = i;
                                        String newDir = pathSong + "/" + String.valueOf(nextFolder);
                                        String msg = RootUtil.createDir(newDir);
                                        if (msg.equals(""))
                                            lastFolder = nextFolder;

                                    }
                                }

                                if (lastFolder > 0) {

                                    //setiap download baru, harus reset bps
                                    Bps.reset();

                                    String downloadPath = pathSong + "/" + String.valueOf(lastFolder);
                                    AndroidNetworking.download(song, appFileDir, fileNameSong)
                                            .setTag("DOWNLOAD_SONG")
                                            .setPriority(Priority.MEDIUM)
                                            .build()
                                            .setDownloadProgressListener(new DownloadProgressListener() {
                                                @Override
                                                public void onProgress(long bytesDownloaded, long totalBytes) {
                                                    try {
                                                        double kbDownloaded = ((double) bytesDownloaded / 1024);
                                                        double mbDownloaded = (double) bytesDownloaded / (double) (1024 * 1024);
                                                        double totalKb = ((double) totalBytes / (double) 1024);
                                                        double totalMb = (double) totalBytes / (double) (1024 * 1024);

                                                        //dalam mega bits per second
                                                        double speedMbps = Bps.calcBps(bytesDownloaded) / 1000000f;

                                                        String downloadedSong = String.format("%.2f MB", mbDownloaded);
//                                                remainingKbTV.setText(text);
                                                        String textTotal = String.format("%.2f MB", totalMb);
                                                        String speed = String.format("%.2f Mbit/s", speedMbps);
                                                        int itemPosition = downloadPosition + 1;
                                                        sendBC(downloadedSong, textTotal, speed, String.valueOf(itemPosition), String.valueOf(songItemArrayList.size()), kbDownloaded, totalKb);
                                                    } catch (Exception ex) {
                                                        Debug.e(TAG, ex);
                                                    }

                                                }
                                            })
                                            .startDownload(new DownloadListener() {
                                                @Override
                                                public void onDownloadComplete() {
                                                    try {
                                                        // do anything after completion
                                                        RootUtil.moveFile(appFileDir + "/" + fileNameSong, downloadPath + "/" + fileNameSong);
                                                        downloadImage(songItem, downloadPath, fileNameSong);
                                                    } catch (Exception ex) {
                                                        Debug.e(TAG, ex);
                                                    }
                                                }

                                                @Override
                                                public void onError(ANError error) {
                                                    // handle error
                                                    //error code 0 jika download di cancel
                                                    if (error.getErrorCode() != 0) {
                                                        Toasty.error(getApplicationContext(), error.getMessage()).show();
                                                        Debug.e("Download song", error);
                                                    }
                                                }
                                            });
                                }
                            } else {
                                String downloadPath = tLocalFile.content.replace("/" + fileNameSong, "");
                                downloadImage(songItem, downloadPath, fileNameSong);

                            }
                        }
                        //jika song sudah didownload
                        else {
                            downloadPosition++;
                            handler.postDelayed(runnable, 1);
                        }
                    } catch (Exception ex) {
                        Debug.e(TAG, ex);
                    }
                }
            });


        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    void downloadImage(TSongItemServer songItem, String downloadPath, String fileNameSong) {
        String appFileDir = getFilesDir().toString();

        String image = songItem.thumbnail;
        String[] fileNameImageArray = image.split("/");
        String fileNameImage = fileNameImageArray[fileNameImageArray.length - 1];
        //download image
        String pathImage = Global.getUSBPath() + "/" + C.PATH_IMAGE_KARAOKE;
        AndroidNetworking.download(image, appFileDir, fileNameImage)
                .setTag("DOWNLOAD_SONG")
                .setPriority(Priority.MEDIUM)

                .build()
                .setDownloadProgressListener(new DownloadProgressListener() {
                    @Override
                    public void onProgress(long bytesDownloaded, long totalBytes) {
                        try {
                            // do anything with progress
                            double kbDownloaded = ((double) bytesDownloaded / 1024);
                            double mbDownloaded = (double) bytesDownloaded / (double) (1024 * 1024);
                            double totalKb = ((double) totalBytes / (double) 1024);
                            double totalMb = (double) totalBytes / (double) (1024 * 1024);

                            //dalam mega bits per second
                            double speedMbps = Bps.calcBps(bytesDownloaded) / 1000000f;

                            String downloadedSong = String.format("%.2f MB", mbDownloaded);
//                                                remainingKbTV.setText(text);
                            String textTotal = String.format("%.2f MB", totalMb);
                            String speed = String.format("%.2f Mbit/s", speedMbps);
                            int itemPosition = downloadPosition + 1;
                            sendBC(downloadedSong, textTotal, speed, String.valueOf(itemPosition), String.valueOf(songItemArrayList.size()), kbDownloaded, totalKb);
                        } catch (Exception ex) {
                            Debug.e(TAG, ex);
                        }

                    }
                })
                .startDownload(new DownloadListener() {
                    @Override
                    public void onDownloadComplete() {
                        try {
                            // do anything after completion
                            RootUtil.moveFile(appFileDir + "/" + fileNameImage, pathImage + "/" + fileNameImage);
                            TSong tSong = new TSong();
                            tSong.songId = songItem.songId;
                            tSong.songName = songItem.songName;
                            tSong.songURL = downloadPath + "/" + fileNameSong;
                            tSong.duration = songItem.duration;
                            tSong.iv = songItem.iv;
                            tSong.key = songItem.key;
                            tSong.thumbnail = pathImage + "/" + fileNameImage;
                            tSong.artist = songItem.artist;
                            tSong.vocalSound = songItem.vocalSound;
                            tSong.songCategoryId = Integer.valueOf(songItem.genreId);
                            insertSongToRoom(tSong);
                            writeSongFileToJSON();
                            downloadPosition++;
                            handler.postDelayed(runnable, 1);
                        } catch (Exception ex) {
                            Debug.e(TAG, ex);
                        }
                    }

                    @Override
                    public void onError(ANError error) {
                        // handle error
                        //error code 0 jika download di cancel
                        if (error.getErrorCode() != 0) {
                            Toasty.error(getApplicationContext(), error.getMessage()).show();
                            Debug.e("Download thumbnail", error);
                        }
                    }
                });
    }

    void insertSongToRoom(TSong tSong) {
        modelSong.addSong(getApplicationContext(), tSong, new ModelSong.IAddSong() {
            @Override
            public void onAddSong(int result) {
                Log.d(TAG, String.valueOf(result));
            }
        });
    }

    /**
     * hanya dipanggil ketika dowload selesai semua
     **/
    void downloadFinish() {
        //panggil ulang writeSongFileToJSON, untuk memastikan song yang sudah didownload di simpan ke json
        usbControl.writeSongFileToJSON(getApplicationContext(), new USBControl.IWriteFileSong() {
            @Override
            public void onWriteFileSong(boolean result) {
                sendBcDownloadFinish(result);
                stopSelf();
            }
        });

    }

    /**
     * dipanggil ketika selesai download song dan thumbnail, fungsi ini dibuat agar ketika download tidak selesai karena stb mati
     **/
    void writeSongFileToJSON() {
        try {
            usbControl.writeSongFileToJSON(getApplicationContext(), new USBControl.IWriteFileSong() {
                @Override
                public void onWriteFileSong(boolean result) {
                    if (result)
                        Log.d(TAG, "Success");
                    else
                        Log.d(TAG, "Failed");

                }
            });

        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    /**
     * untuk send bc progress download lagu
     */
    void sendBC(String downloaded, String totalSize, String speed, String downloadItemOrder, String sumItemDownload, double kbDownloaded, double totalSizeInKb) {
        try {

            Intent sendBC = new Intent();
            sendBC.setFlags(Intent.FLAG_INCLUDE_STOPPED_PACKAGES)
                    .setAction(C.BOTTOM_NAVIGATION_FRAGMENT);
            sendBC.putExtra(KEY_DOWNLOADED_SIZE, downloaded);
            sendBC.putExtra(KEY_DOWNLOAD_SPEED, speed);
            sendBC.putExtra(KEY_TOTAL_SIZE, totalSize);
            sendBC.putExtra(KEY_DOWNLOAD_ITEM_ORDER, downloadItemOrder);
            sendBC.putExtra(KEY_SUM_ITEM_DOWNLOAD, sumItemDownload);
            sendBC.putExtra(KEY_DOWNLOADED_IN_KB, kbDownloaded);
            sendBC.putExtra(KEY_TOTAL_SIZE_IN_KB, totalSizeInKb);
            sendBC.putExtra(KEY_DOWNLOADED_SIZE, downloaded);
            sendBC.putExtra(KEY_DOWNLOAD_SPEED, speed);
            sendBC.putExtra(KEY_TOTAL_SIZE, totalSize);
            sendBC.putExtra(KEY_DOWNLOAD_ITEM_ORDER, downloadItemOrder);
            sendBC.putExtra(KEY_SUM_ITEM_DOWNLOAD, sumItemDownload);
            sendBC.putExtra(KEY_DOWNLOADED_IN_KB, kbDownloaded);
            sendBC.putExtra(KEY_TOTAL_SIZE_IN_KB, totalSizeInKb);

            sendBroadcast(sendBC);
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    public static void registerReceiver(Context context, BroadcastReceiver br) {
        try {
            IntentFilter intFilt = new IntentFilter(C.BOTTOM_NAVIGATION_FRAGMENT);
            context.registerReceiver(br, intFilt);
        } catch (Exception e) {
            Debug.e(TAG, e);
        }
    }

    public static void unregisterReceiver(Context context, BroadcastReceiver br) {
        try {
            context.unregisterReceiver(br);
        } catch (Exception e) {
            Debug.e(TAG, e);
        }
    }


    /**
     * untuk send bc sudah selesai download
     */
    void sendBcDownloadFinish(boolean result) {
        try {
            Intent sendBC = new Intent();
            sendBC.setFlags(Intent.FLAG_INCLUDE_STOPPED_PACKAGES)
                    .setAction(C.BOTTOM_NAVIGATION_FRAGMENT_FINISH_DOWNLOAD);
            sendBC.putExtra(KEY_RESULT_DOWNLOAD, result);
            sendBroadcast(sendBC);
            if (result) {
                deleteAllSongLocal();
            }
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    public static void registerFinishDownloadReceiver(Context context, BroadcastReceiver br) {
        try {
            IntentFilter intFilt = new IntentFilter(C.BOTTOM_NAVIGATION_FRAGMENT_FINISH_DOWNLOAD);
            context.registerReceiver(br, intFilt);
        } catch (Exception e) {
            Debug.e(TAG, e);
        }
    }

    public static void unregisterFinishDownloadReceiver(Context context, BroadcastReceiver br) {
        try {
            context.unregisterReceiver(br);
        } catch (Exception e) {
            Debug.e(TAG, e);
        }
    }


    /**
     * save json song download ke local
     */
    void saveSongToLocal(List<TSongItemServer> tSongItemServerList) {
        modelSong.addServerSong(getApplicationContext(), tSongItemServerList, new ModelSong.IAddServerSong() {
            @Override
            public void onAddSong(int result) {
                Log.d(TAG, String.valueOf(result));
            }
        });
    }


    /**
     * delete all json song download dari local
     */
    void deleteAllSongLocal() {
        modelSong.deleteServerSong(getApplicationContext(), new ModelSong.IDeleteServerSong() {
            @Override
            public void onDeleteSong(int result) {
                Log.d(TAG, String.valueOf(result));
            }
        });
    }

    /**
     * hanya untuk test service sudah di stop atau belum, jika sudah test, toast nya di comment aja
     */
    @Override
    public void onDestroy() {
        super.onDestroy();
        try {
            handler.removeCallbacks(runnable);
            AndroidNetworking.forceCancel("DOWNLOAD_SONG");
            deleteAllSongLocal();
            sendBcDownloadFinish(false);
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

}

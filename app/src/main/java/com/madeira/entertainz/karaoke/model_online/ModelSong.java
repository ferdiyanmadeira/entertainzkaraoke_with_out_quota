package com.madeira.entertainz.karaoke.model_online;

import android.net.Uri;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.OkHttpResponseAndJSONObjectRequestListener;
import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;
import com.madeira.entertainz.karaoke.DBLocal.TSongItemServer;
import com.madeira.entertainz.karaoke.Debug;
import com.madeira.entertainz.karaoke.config.Global;

import org.json.JSONObject;

import java.io.Serializable;
import java.util.List;

import okhttp3.Response;

import static com.madeira.entertainz.karaoke.config.C.DEFAULT_API;

public class ModelSong {
    private static final String TAG = "ModelSong";

    private static final String APIHOST = Global.getApiHostname() + "/v1/song.php";

    public interface IGetGenre {
        void onGetGenre(listGenreItem result);

        void onError(ANError e);
    }

    static public void getGenre(IGetGenre callBack) {
        try {
            String uri = Uri.parse(APIHOST)
                    .buildUpon()
                    .appendQueryParameter("action", "get_all_genre")
                    .build().toString();

            AndroidNetworking.get(uri)
                    .setPriority(Priority.MEDIUM)
                    .build()
                    .getAsOkHttpResponseAndJSONObject(new OkHttpResponseAndJSONObjectRequestListener() {
                        @Override
                        public void onResponse(Response okHttpResponse, JSONObject response) {
                            try {
                                if (okHttpResponse.isSuccessful()) {
                                    if (response.has("errCode")) {
                                        int errorCode = response.getInt("errCode");
                                        String errorMessage = response.getString("errMsg");
                                        ANError anError = new ANError();
                                        anError.setErrorCode(errorCode);
                                        anError.setErrorBody(errorMessage);
                                        anError.setErrorDetail(errorMessage);
                                        callBack.onError(anError);
                                        return;
                                    }
                                    Gson gson = new Gson();
                                    String jsonString = response.toString();
                                    listGenreItem stbItem = gson.fromJson(jsonString, ModelSong.listGenreItem.class);
                                    callBack.onGetGenre(stbItem);
                                }
                            } catch (Exception ex) {

                            }
                        }

                        @Override
                        public void onError(ANError anError) {
                            callBack.onError(anError);
                        }
                    });
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    public class listGenreItem {
        @SerializedName("list_genre")
        public List<genreItem> listGenre;

    }

    public static class genreItem {
        @SerializedName("genre_id")
        public String genreId;
        @SerializedName("genre")
        public String genre;

    }


    public interface IGetAllSong {
        void onGetAllSong(allSongItem result);

        void onError(ANError e);
    }

    static public void getAllSong(String mac, IGetAllSong callBack) {
        try {
            String uri = Uri.parse(APIHOST)
                    .buildUpon()
                    .appendQueryParameter("action", "get_list")
                    .appendQueryParameter("mac", mac)
                    .build().toString();

            AndroidNetworking.get(uri)
                    .setPriority(Priority.MEDIUM)
                    .build()
                    .getAsOkHttpResponseAndJSONObject(new OkHttpResponseAndJSONObjectRequestListener() {
                        @Override
                        public void onResponse(Response okHttpResponse, JSONObject response) {
                            try {
                                if (okHttpResponse.isSuccessful()) {
                                    if (response.has("errCode")) {
                                        int errorCode = response.getInt("errCode");
                                        String errorMessage = response.getString("errMsg");
                                        ANError anError = new ANError();
                                        anError.setErrorCode(errorCode);
                                        anError.setErrorBody(errorMessage);
                                        anError.setErrorDetail(errorMessage);
                                        callBack.onError(anError);
                                        return;
                                    }
                                    Gson gson = new Gson();
                                    String jsonString = response.toString();
                                    allSongItem stbItem = gson.fromJson(jsonString, allSongItem.class);
                                    callBack.onGetAllSong(stbItem);
                                }
                            } catch (Exception ex) {
                                Debug.e(TAG, ex);
                            }
                        }

                        @Override
                        public void onError(ANError anError) {

                            callBack.onError(anError);
                        }
                    });
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    public interface IGetSong {
        void onGetSong(listSongItem result);

        void onError(ANError e);
    }

    static public void getSong(String mac, int offset, int limit, IGetSong callBack) {
        try {
            String uri = Uri.parse(APIHOST)
                    .buildUpon()
                    .appendQueryParameter("action", "get_song")
                    .appendQueryParameter("mac", mac)
                    .appendQueryParameter("offset", String.valueOf(offset))
                    .appendQueryParameter("limit", String.valueOf(limit))
                    .build().toString();

            AndroidNetworking.get(uri)
                    .setPriority(Priority.MEDIUM)
                    .build()
                    .getAsOkHttpResponseAndJSONObject(new OkHttpResponseAndJSONObjectRequestListener() {
                        @Override
                        public void onResponse(Response okHttpResponse, JSONObject response) {
                            try {
                                if (okHttpResponse.isSuccessful()) {
                                    if (response.has("errCode")) {
                                        int errorCode = response.getInt("errCode");
                                        String errorMessage = response.getString("errMsg");
                                        ANError anError = new ANError();
                                        anError.setErrorCode(errorCode);
                                        anError.setErrorBody(errorMessage);
                                        anError.setErrorDetail(errorMessage);
                                        callBack.onError(anError);
                                        return;
                                    }
                                    Gson gson = new Gson();
                                    String jsonString = response.toString();
                                    listSongItem stbItem = gson.fromJson(jsonString, listSongItem.class);
                                    callBack.onGetSong(stbItem);
                                }
                            } catch (Exception ex) {
                                Debug.e(TAG, ex);
                            }
                        }

                        @Override
                        public void onError(ANError anError) {

                            callBack.onError(anError);
                        }
                    });
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    public static class allSongItem {
        @SerializedName("list_song")
        public List<TSongItemServer> listSong;
        @SerializedName("list_genre")
        public List<genreItem> listGenre;
    }

    public static class listSongItem {
        @SerializedName("list_song")
        public List<TSongItemServer> listSong;
    }

   /* public class songItem implements Serializable {
        @SerializedName("batchNumber")
        public String batchNumber;
        @SerializedName("songId")
        public String songId;
        @SerializedName("songPath")
        public String songPath;
        @SerializedName("songName")
        public String songName;
        @SerializedName("artist")
        public String artist;
        @SerializedName("songWriter")
        public String songWriter;
        @SerializedName("duration")
        public String duration;
        @SerializedName("vocalSound")
        public String vocalSound;
        @SerializedName("thumbnail")
        public String thumbnail;
        @SerializedName("filesize")
        public String filesize;
        @SerializedName("genre_id")
        public String genreId;
        @SerializedName("iv")
        public String iv;
        @SerializedName("key")
        public String key;
    }*/


}

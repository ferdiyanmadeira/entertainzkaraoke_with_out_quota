package com.madeira.entertainz.karaoke.menu_settings;


import android.content.res.ColorStateList;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.os.StatFs;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.madeira.entertainz.karaoke.DBLocal.DbAccess;
import com.madeira.entertainz.karaoke.DBLocal.TSong;
import com.madeira.entertainz.karaoke.Debug;
import com.madeira.entertainz.karaoke.R;
import com.madeira.entertainz.library.PerformAsync2;
import com.madeira.entertainz.library.StorageUtils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class SettingHelpFragment extends Fragment implements SettingsTheme.ISettingsTheme {

    static String PARENT_TAG ="SettingHelpFragment";
    View root;
    SettingsTheme settingsTheme;

    LinearLayout rootLL;


    public SettingHelpFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        root = inflater.inflate(R.layout.fragment_setting_help, container, false);
        try {
            bind();
            settingsTheme = new SettingsTheme(getActivity(), this);
            settingsTheme.setTheme();
        } catch (Exception ex) {
            Debug.e(PARENT_TAG, ex);
        }
        return root;
    }

    void bind()
    {
        rootLL = root.findViewById(R.id.rootLL);

    }

    @Override
    public void setThemeUpdateDate(Date lastUpdateTheme) {

    }

    @Override
    public void setListColorTheme(int color) {
        rootLL.setBackgroundResource(R.drawable.tags_rounded_corners);

        GradientDrawable categoryDrawable = (GradientDrawable) rootLL.getBackground();
        categoryDrawable.setColor(color);
    }

    @Override
    public void setTextAndSelectionColorTheme(ColorStateList colorSelection, ColorStateList colorText, int colorTextSelected, int colorTextFocus, int colorTextNormal, int colorSelectionSelected, int colorSelectionFocus) {

    }



}

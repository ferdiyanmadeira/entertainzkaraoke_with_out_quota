package com.madeira.entertainz.karaoke.model_online;

import android.net.Uri;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.OkHttpResponseAndJSONObjectRequestListener;
import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;
import com.madeira.entertainz.karaoke.Debug;
import com.madeira.entertainz.karaoke.config.Global;
import com.madeira.entertainz.library.Util;

import org.json.JSONObject;

import java.io.Serializable;
import java.util.List;

import okhttp3.Response;

import static com.madeira.entertainz.karaoke.config.C.DEFAULT_API;

public class ModelPriceQuota {
    private static final String TAG = "ModelPriceQuota";

    private static final String APIHOST = Global.getApiHostname() + "/v1/price_quota.php";

    public interface IGetPriceQuota {
        void onGetPriceQuota(PriceQuotaList result);

        void onError(ANError e);
    }
    static public void getPriceQuota( IGetPriceQuota callBack) {
        try {

            String mac = Util.getMACAddress("eth0");
            mac = mac.replace(":", "");
            String uri = Uri.parse(APIHOST)
                    .buildUpon()
                    .appendQueryParameter("action", "get_list")
                    .appendQueryParameter("mac", mac)
                    .build().toString();

            AndroidNetworking.get(uri)
                    .setPriority(Priority.MEDIUM)
                    .build()
                    .getAsOkHttpResponseAndJSONObject(new OkHttpResponseAndJSONObjectRequestListener() {
                        @Override
                        public void onResponse(Response okHttpResponse, JSONObject response) {
                            try {
                                if (okHttpResponse.isSuccessful()) {
                                    if(response.has("errCode"))
                                    {
                                        int errorCode =response.getInt("errCode");
                                        String errorMessage = response.getString("errMsg");
                                        ANError anError = new ANError();
                                        anError.setErrorCode(errorCode);
                                        anError.setErrorBody(errorMessage);
                                        anError.setErrorDetail(errorMessage);
                                        callBack.onError(anError);
                                        return;
                                    }
                                    Gson gson = new Gson();
                                    String jsonString = response.toString();
                                    PriceQuotaList stbItem = gson.fromJson(jsonString, PriceQuotaList.class);
                                    callBack.onGetPriceQuota(stbItem);
                                }
                            } catch (Exception ex) {

                            }
                        }

                        @Override
                        public void onError(ANError anError) {
                            callBack.onError(anError);
                        }
                    });
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }


    public class PriceQuotaList {
        @SerializedName("list")
        public List<PriceQuotaItem> priceQuotaItemList;
    }

    public class PriceQuotaItem implements Serializable {
        //        @SerializedName("version_code")
//        public String versionCode;
//
        @SerializedName("quota_id")
        public String quotaId;
        @SerializedName("quota")
        public String quota;
        @SerializedName("price")
        public String price;
        @SerializedName("url_logo")
        public String urlLogo;
        public String currency;

    }

}

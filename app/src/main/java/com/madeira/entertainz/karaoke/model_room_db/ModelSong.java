package com.madeira.entertainz.karaoke.model_room_db;

import android.content.Context;
import android.graphics.Paint;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.madeira.entertainz.controller.USBControl;
import com.madeira.entertainz.karaoke.DBLocal.DbAccess;
import com.madeira.entertainz.karaoke.DBLocal.DbRecentSearchSong;
import com.madeira.entertainz.karaoke.DBLocal.DbSong;
import com.madeira.entertainz.karaoke.DBLocal.DbYoutube;
import com.madeira.entertainz.karaoke.DBLocal.JoinSongCountCategory;
import com.madeira.entertainz.karaoke.DBLocal.JoinSongCountPlayed;
import com.madeira.entertainz.karaoke.DBLocal.TLogSong;
import com.madeira.entertainz.karaoke.DBLocal.TRecentSearchSong;
import com.madeira.entertainz.karaoke.DBLocal.TSong;
import com.madeira.entertainz.karaoke.DBLocal.TSongCategory;
import com.madeira.entertainz.karaoke.DBLocal.TSongItemServer;
import com.madeira.entertainz.karaoke.DBLocal.TSongYoutube;
import com.madeira.entertainz.karaoke.Debug;
import com.madeira.entertainz.karaoke.MainApplication;
import com.madeira.entertainz.karaoke.config.C;
import com.madeira.entertainz.library.Async;
import com.madeira.entertainz.library.PerformAsync2;
import com.madeira.entertainz.library.RootUtil;
import com.madeira.entertainz.library.Util;
import com.madeira.eznet.lib.HelperGson;
import com.madeira.mrkeyboard.Helper;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class ModelSong {
    static String TAG = "ModelSong";

    /**
     * untuk get list Song category
     */
    public interface IGetListSongCategory {
        void onGetListSongCategory(List<TSongCategory> tSongCategoryList);
    }

    public static void getSongCategory(Context context, IGetListSongCategory callback) {
        PerformAsync2.run(performAsync ->
        {
            List<TSongCategory> result = new ArrayList<>();
            result = syncGetSongCategory(context);
            return result;
        }).
                setCallbackResult(result ->

                {
                    try {
                        List<TSongCategory> tSongCategoryList = (List<TSongCategory>) result;
                        callback.onGetListSongCategory(tSongCategoryList);
                    } catch (Exception ex) {
                        Debug.e(TAG, ex);
                    }

                });
    }

    public static List<TSongCategory> syncGetSongCategory(Context context) {
        List<TSongCategory> result = new ArrayList<>();
        try {
            DbSong dbSong = DbSong.Instance.create(context);
            result = dbSong.daoAccessTSongCategory().getAll();
            dbSong.close();
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
        return result;
    }

    /**
     * untuk get list Song
     */
    public interface IGetListJoinSong {
        void onGetListJoinSong(List<JoinSongCountPlayed> tSongList);
    }

    public static void getJoinSong(Context context, IGetListJoinSong callback) {
        PerformAsync2.run(performAsync ->
        {
            List<JoinSongCountPlayed> result = new ArrayList<>();
            result = syncGetJoinSong(context);
            return result;

        }).
                setCallbackResult(result ->

                {
                    try {
                        List<JoinSongCountPlayed> tSongList = (List<JoinSongCountPlayed>) result;
                        callback.onGetListJoinSong(tSongList);
                    } catch (Exception ex) {
                        Debug.e(TAG, ex);
                    }

                });
    }

    public static List<JoinSongCountPlayed> syncGetJoinSong(Context context) {
        List<JoinSongCountPlayed> result = new ArrayList<>();
        try {
            DbSong dbSong = DbSong.Instance.create(context);
            result = dbSong.daoAccessTSong().getAll();
            dbSong.close();
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
        return result;
    }

    /**
     * untuk get list Song
     */
    public interface IGetListSongByCategory {
        void onGetListSongByCategory(List<JoinSongCountPlayed> tSongList);
    }

    public static void getSongByCategory(Context context, int categoryId, IGetListSongByCategory callback) {
        PerformAsync2.run(performAsync ->
        {
            List<JoinSongCountPlayed> result = new ArrayList<>();
            try {
                DbSong dbSong = DbSong.Instance.create(context);
                result = dbSong.daoAccessTSong().getByCategory(categoryId);
                dbSong.close();
            } catch (Exception ex) {
                Debug.e(TAG, ex);
            }
            return result;
        }).
                setCallbackResult(result ->

                {
                    try {
                        List<JoinSongCountPlayed> tSongList = (List<JoinSongCountPlayed>) result;
                        callback.onGetListSongByCategory(tSongList);
                    } catch (Exception ex) {
                        Debug.e(TAG, ex);
                    }

                });
    }

    /**
     * untuk add recent search
     */
    public interface IAddRecentSearch {
        void onAddRecentSearch(int result);
    }

    public static void addRecentSearch(Context context, String songName, IAddRecentSearch callback) {
        PerformAsync2.run(performAsync ->
        {
            int result = 0;
            try {
                DbRecentSearchSong dbRecentSearchSong = DbRecentSearchSong.Instance.create(context);
                List<TRecentSearchSong> tRecentSearchSongs = dbRecentSearchSong.daoAccessRecentSearchSong().getAll(C.SEARCH_SONG_KARAOKE);

                //delete recent id satu persatu yang lebih dari 50
                for (int i = 50; i < tRecentSearchSongs.size(); i++) {
                    dbRecentSearchSong.daoAccessRecentSearchSong().deleteById(tRecentSearchSongs.get(i).recentId);
                }

                TRecentSearchSong tRecentSearchSong = dbRecentSearchSong.daoAccessRecentSearchSong().checkSong(songName, C.SEARCH_SONG_KARAOKE);
                if (tRecentSearchSong != null) {
                    //hapus recent terakhir yang menggunakan song name ini, dan create ulang
                    dbRecentSearchSong.daoAccessRecentSearchSong().deleteById(tRecentSearchSong.recentId);
                }
                TRecentSearchSong addTRecentSearchSong = new TRecentSearchSong();
                addTRecentSearchSong.recentSong = songName;
                addTRecentSearchSong.category = C.SEARCH_SONG_KARAOKE;
                dbRecentSearchSong.daoAccessRecentSearchSong().insert(addTRecentSearchSong);
                dbRecentSearchSong.close();
                result = 1;
            } catch (Exception ex) {
                Debug.e(TAG, ex);
            }
            return result;
        }).
                setCallbackResult(result ->

                {
                    try {
                        int r = (int) result;
                        callback.onAddRecentSearch(r);
                    } catch (Exception ex) {
                        Debug.e(TAG, ex);
                    }

                });
    }

    /**
     * untuk get list Search Song
     */
    public interface IGetListSearchSong {
        void onGetListSearchSong(List<JoinSongCountPlayed> tSongList);
    }

    public static void getSearchSong(Context context, String keyword, IGetListSearchSong callback) {
        PerformAsync2.run(performAsync ->
        {
            List<JoinSongCountPlayed> result = new ArrayList<>();
            try {
                DbSong dbSong = DbSong.Instance.create(context);
                result = dbSong.daoAccessTSong().getSearchAll(keyword);
                dbSong.close();
            } catch (Exception ex) {
                Debug.e(TAG, ex);
            }
            return result;
        }).
                setCallbackResult(result ->

                {
                    try {
                        List<JoinSongCountPlayed> tSongList = (List<JoinSongCountPlayed>) result;
                        callback.onGetListSearchSong(tSongList);
                    } catch (Exception ex) {
                        Debug.e(TAG, ex);
                    }

                });
    }

    /**
     * untuk get list Search Song
     */
    public interface IGetListSearchSongByCategory {
        void onGetListSearchSongByCategory(List<JoinSongCountPlayed> tSongList);
    }

    public static void getSearchSongByCategory(Context context, String keyword, int categoryId, IGetListSearchSongByCategory callback) {
        PerformAsync2.run(performAsync ->
        {
            List<JoinSongCountPlayed> result = new ArrayList<>();
            try {
                DbSong dbSong = DbSong.Instance.create(context);
                result = dbSong.daoAccessTSong().getSearchByCategory(categoryId, keyword);
                dbSong.close();
            } catch (Exception ex) {
                Debug.e(TAG, ex);
            }
            return result;
        }).
                setCallbackResult(result ->

                {
                    try {
                        List<JoinSongCountPlayed> tSongList = (List<JoinSongCountPlayed>) result;
                        callback.onGetListSearchSongByCategory(tSongList);
                    } catch (Exception ex) {
                        Debug.e(TAG, ex);
                    }

                });
    }

    /**
     * untuk get list recent Song
     */
    public interface IGetListRecentSong {
        void onGetListRecentSong(List<TRecentSearchSong> tRecentSearchSongList);
    }

    public static void getRecentSong(Context context, IGetListRecentSong callback) {
        PerformAsync2.run(performAsync ->
        {
            List<TRecentSearchSong> result = new ArrayList<>();
            try {
                DbRecentSearchSong dbRecentSearchSong = DbRecentSearchSong.Instance.create(context);
                result = dbRecentSearchSong.daoAccessRecentSearchSong().getAll(C.SEARCH_SONG_KARAOKE);
                dbRecentSearchSong.close();
            } catch (Exception ex) {
                Debug.e(TAG, ex);
            }
            return result;
        }).
                setCallbackResult(result ->

                {
                    try {
                        List<TRecentSearchSong> tRecentSearchSongs = (List<TRecentSearchSong>) result;
                        callback.onGetListRecentSong(tRecentSearchSongs);
                    } catch (Exception ex) {
                        Debug.e(TAG, ex);
                    }

                });
    }

    /**
     * untuk get list log Song
     */
    public interface IGetListLogSong {
        void onGetListLogSong(List<TLogSong> tLogSongList);
    }

    public static void getLogSong(Context context, IGetListLogSong callback) {
        Async.run(performAsync ->
        {
            List<TLogSong> result = new ArrayList<>();
            try {
                DbSong dbSong = DbSong.Instance.create(context);
                result = dbSong.daoAccessTLogSong().getAll();
                dbSong.close();
            } catch (Exception ex) {
                Debug.e(TAG, ex);
            }
            return result;
        }).
                setCallbackResult(result ->

                {
                    try {
                        List<TLogSong> tLogSongs = (List<TLogSong>) result;
                        callback.onGetListLogSong(tLogSongs);
                    } catch (Exception ex) {
                        Debug.e(TAG, ex);
                    }

                });
    }

    /**
     * untuk delete log song
     */
    public interface IDeleteLogSong {
        void onDeleteLogSong(int result);
    }

    public static void deleteLogSong(Context context, IDeleteLogSong callback) {
        Async.run(performAsync ->
        {
            int result = 0;
            try {
                try {
                    DbSong dbSong = DbSong.Instance.create(context);
                    dbSong.daoAccessTLogSong().deleteAll();
                    dbSong.close();
                    result = 1;
                } catch (Exception ex) {
                    Debug.e(TAG, ex);
                }
            } catch (Exception ex) {
                Debug.e(TAG, ex);
            }
            return result;
        }).
                setCallbackResult(result ->

                {
                    try {
                        int r = (int) result;
                        callback.onDeleteLogSong(r);
                    } catch (Exception ex) {
                        Debug.e(TAG, ex);
                    }

                });
    }

    /**
     * untuk get Song BY ID
     */
    public interface IGetSongById {
        void onGeSongById(TSong tSong);
    }

    public static void getSongById(Context context, String songId, IGetSongById callback) {
        PerformAsync2.run(performAsync ->
        {
            TSong result = new TSong();
            try {
                DbSong dbSong = DbSong.Instance.create(context);
                result = dbSong.daoAccessTSong().getSongById(songId);
                dbSong.close();
            } catch (Exception ex) {
                Debug.e(TAG, ex);
            }
            return result;
        }).
                setCallbackResult(result ->

                {
                    try {
                        TSong tSong = (TSong) result;
                        callback.onGeSongById(tSong);
                    } catch (Exception ex) {
                        Debug.e(TAG, ex);
                    }

                });
    }

    /**
     * untuk add song
     */
    public interface IAddSong {
        void onAddSong(int result);
    }

    public static void addSong(Context context, TSong tSong, IAddSong callback) {
        Async.run(performAsync ->
        {
            int result = 0;
            try {
                try {
                    syncAddSong(context, tSong);
                    result = 1;
                } catch (Exception ex) {
                    Debug.e(TAG, ex);
                }
            } catch (Exception ex) {
                Debug.e(TAG, ex);
            }
            return result;
        }).
                setCallbackResult(result ->

                {
                    try {
                        int r = (int) result;
                        callback.onAddSong(r);
                    } catch (Exception ex) {
                        Debug.e(TAG, ex);
                    }

                });
    }

    public static void syncAddSong(Context context, TSong tSong) {
        try {
            DbSong dbSong = DbSong.Instance.create(context);
            dbSong.daoAccessTSong().insert(tSong);
            dbSong.close();
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    public static void syncAddAndReplaceSong(Context context, TSong tSong) {
        try {
            DbSong dbSong = DbSong.Instance.create(context);
            TSong item = dbSong.daoAccessTSong().getSongById(tSong.songId);
            List<TSong> tSongList = new ArrayList<>();
            tSongList.add(item);
            if (tSongList.size() > 0) {
                syncReplaceDeleteSong(item, context);
            }
            dbSong.daoAccessTSong().insert(tSong);
            dbSong.close();
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    /**
     * untuk get list Song
     */
    public interface IGetListSong {
        void onGetListSong(List<TSong> tSongList);
    }

    public static void getListSong(Context context, IGetListSong callback) {
        PerformAsync2.run(performAsync ->
        {
            List<TSong> result = new ArrayList<>();
            try {
                DbSong dbSong = DbSong.Instance.create(context);
                result = dbSong.daoAccessTSong().getAllTSong();
                dbSong.close();
            } catch (Exception ex) {
                Debug.e(TAG, ex);
            }
            return result;
        }).
                setCallbackResult(result ->

                {
                    try {
                        List<TSong> tSongList = (List<TSong>) result;
                        callback.onGetListSong(tSongList);
                    } catch (Exception ex) {
                        Debug.e(TAG, ex);
                    }

                });
    }

    /**
     * untuk add song from server untuk resume download ketika device restart ketika belum selesai download
     */
    public interface IAddServerSong {
        void onAddSong(int result);
    }

    public static void addServerSong(Context context, List<TSongItemServer> tSongItemServerList, IAddServerSong callback) {
        Async.run(performAsync ->
        {
            int result = 0;
            try {
                try {
                    DbAccess dbAccess = DbAccess.Instance.create(context);
                    DbAccess.DaoAccessTSongItemServer daoAccessTSongItemServer = dbAccess.daoAccessTSongItemServer();
                    daoAccessTSongItemServer.insertAll(tSongItemServerList);
                    dbAccess.close();
                    result = 1;
                } catch (Exception ex) {
                    Debug.e(TAG, ex);
                }
            } catch (Exception ex) {
                Debug.e(TAG, ex);
            }
            return result;
        }).
                setCallbackResult(result ->

                {
                    try {
                        int r = (int) result;
                        callback.onAddSong(r);
                    } catch (Exception ex) {
                        Debug.e(TAG, ex);
                    }

                });
    }

    /**
     * untuk clear list resume download
     */
    public interface IDeleteServerSong {
        void onDeleteSong(int result);
    }

    public static void deleteServerSong(Context context, IDeleteServerSong callback) {
        Async.run(performAsync ->
        {
            int result = 0;
            try {
                try {
                    DbAccess dbAccess = DbAccess.Instance.create(context);
                    DbAccess.DaoAccessTSongItemServer daoAccessTSongItemServer = dbAccess.daoAccessTSongItemServer();
                    daoAccessTSongItemServer.deleteAll();
                    dbAccess.close();
                    result = 1;
                } catch (Exception ex) {
                    Debug.e(TAG, ex);
                }
            } catch (Exception ex) {
                Debug.e(TAG, ex);
            }
            return result;
        }).
                setCallbackResult(result ->

                {
                    try {
                        int r = (int) result;
                        callback.onDeleteSong(r);
                    } catch (Exception ex) {
                        Debug.e(TAG, ex);
                    }

                });
    }

    /**
     * untuk get list download Song(untuk keperluan resume download)
     */
    public interface IGetListServerSong {
        void onGetListServerSong(List<TSongItemServer> tSongList);
    }

    public static void getListServerSong(Context context, IGetListServerSong callback) {
        PerformAsync2.run(performAsync ->
        {
            List<TSongItemServer> result = new ArrayList<>();
            try {
                DbAccess dbAccess = DbAccess.Instance.create(context);
                result = dbAccess.daoAccessTSongItemServer().getAll();
                dbAccess.close();
            } catch (Exception ex) {
                Debug.e(TAG, ex);
            }
            return result;
        }).
                setCallbackResult(result ->

                {
                    try {
                        List<TSongItemServer> tSongList = (List<TSongItemServer>) result;
                        callback.onGetListServerSong(tSongList);
                    } catch (Exception ex) {
                        Debug.e(TAG, ex);
                    }

                });
    }

    public static String syncDeleteSong(List<JoinSongCountPlayed> joinSongCountPlayedList, Context context) {
        String result = "0";
        try {
            DbSong dbSong = DbSong.Instance.create(context);
            for (JoinSongCountPlayed item : joinSongCountPlayedList) {
                dbSong.daoAccessTSong().delete(item.songId);
                File thumbnailFile = new File(item.thumbnail);
                if (thumbnailFile.exists())
                    RootUtil.deleteFile(item.thumbnail);
                File songFile = new File(item.songURL);
                if (songFile.exists())
                    RootUtil.deleteFile(item.songURL);
            }
            dbSong.close();
            //replace json file
            writeSongFileToJSON();
            result = "1";
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
        return result;
    }


    public static String syncUpdateSong(JoinSongCountPlayed joinSongCountPlayed, Context context) {
        String result = "0";
        try {
            DbSong dbSong = DbSong.Instance.create(context);
            dbSong.daoAccessTSong().updateFromEKMA(joinSongCountPlayed.songId, joinSongCountPlayed.songName, joinSongCountPlayed.artist, joinSongCountPlayed.songCategoryId, joinSongCountPlayed.lyric);
            //replace json file
            writeSongFileToJSON();
            result = "1";
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
        return result;
    }

    public static String syncReplaceDeleteSong(TSong item, Context context) {
        String result = "0";
        try {
            DbSong dbSong = DbSong.Instance.create(context);
            dbSong.daoAccessTSong().delete(item.songId);
            File thumbnailFile = new File(item.thumbnail);
            if (thumbnailFile.exists())
                RootUtil.deleteFile(item.thumbnail);
            File songFile = new File(item.songURL);
            if (songFile.exists())
                RootUtil.deleteFile(item.songURL);
            dbSong.close();
            //replace json file
            result = "1";
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
        return result;
    }

    public static List<JoinSongCountCategory> syncGetJoinCountSongCategory(Context context) {
        List<JoinSongCountCategory> result = new ArrayList<>();
        try {
            DbSong dbSong = DbSong.Instance.create(context);
            result = dbSong.daoAccessTSong().getSongCountByCategory();
            dbSong.close();
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
        return result;
    }

    static void writeSongFileToJSON() {
        USBControl usbControl = new USBControl();
        usbControl.writeSongFileToJSON(MainApplication.context, new USBControl.IWriteFileSong() {
            @Override
            public void onWriteFileSong(boolean result) {
            }
        });
    }

    public static final String EKMAGetSongCategory() {
        String result = "";
        try {
            List<TSongCategory> tSongCategories = syncGetSongCategory(MainApplication.context);
            tSongCategories.remove(0);
            result = HelperGson.toString(tSongCategories);
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
        return result;
    }

    public static final String EKMAGetSong() {
        String result = "";
        try {
            List<JoinSongCountPlayed> base64List = new ArrayList<>();
            List<JoinSongCountPlayed> list = syncGetJoinSong(MainApplication.context);
            for (JoinSongCountPlayed item : list) {
                File songFile = new File((item.songURL));
                item.songSize = songFile.length();
                File thumbnailSize = new File(item.thumbnail);
                item.thumbnailSize = thumbnailSize.length();
                item.thumbnail = Util.fileToBase64(item.thumbnail);
                base64List.add(item);
            }

            result = HelperGson.toString(base64List);
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
        return result;
    }

    public static final String EKMADeleteSong(String jsonString) {
        String result = "0";
        try {
            Gson gson = new Gson();
            List<JoinSongCountPlayed> joinSongCountPlayedList = gson.fromJson(jsonString, new TypeToken<List<JoinSongCountPlayed>>() {
            }.getType());
            String r = syncDeleteSong(joinSongCountPlayedList, MainApplication.context);
            ModelPlaylist.syncDeleteSongPlaylistById(joinSongCountPlayedList, MainApplication.context);
            ResultEKMA resultEKMA = new ResultEKMA();
            resultEKMA.result = r;
            result = HelperGson.toString(resultEKMA);
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }

        return result;
    }

    public static final String EKMAUpdateSong(String jsonString) {
        String result = "0";
        try {
            Gson gson = new Gson();
            JoinSongCountPlayed joinSongCountPlayedList = gson.fromJson(jsonString, JoinSongCountPlayed.class);
            String r = syncUpdateSong(joinSongCountPlayedList, MainApplication.context);
            ResultEKMA resultEKMA = new ResultEKMA();
            resultEKMA.result = r;
            result = HelperGson.toString(resultEKMA);
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }

        return result;
    }

    public static class ResultEKMA {
        String result;
    }


}

package com.madeira.entertainz.karaoke.DBLocal;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.Nullable;

import com.google.gson.annotations.SerializedName;

import java.util.List;

@Entity(tableName = "tsongplaylist")
public class TSongPlaylist {

    @PrimaryKey(autoGenerate = true)
    @SerializedName("songPlaylistId")
    public int songPlaylistId;
    @SerializedName("playlistId")
    public int playlistId;
    @SerializedName("songId")
    public String songId;
   /* @SerializedName("songName")
    public String songName;
    @SerializedName("artist")
    public String artist;
    @SerializedName("songURL")
    public String songURL;
    @SerializedName("vocalSound")
    public int vocalSound;
    @SerializedName("duration")
    public int duration;
    @SerializedName("thumbnail")
    public String thumbnail;
    @SerializedName("iv")
    public String iv;
    @SerializedName("key")
    public String key;*/

    @Override
    public boolean equals(@Nullable Object obj) {
        TSongPlaylist item = (TSongPlaylist) obj;

        return item.songId==songId;
    }

    /**
     * return index dari item berdasarkan songId
     *
     * @param list
     * @param songId
     * @return
     */
    public static int getIndexBySongId(List<TSongPlaylist> list, String songId){

        //set default -1; jika index tidak ditemukan
        int index =-1;
        TSongPlaylist item = new TSongPlaylist();

        for(TSongPlaylist tSongPlaylist : list)
        {
            if(tSongPlaylist.songId.equals(songId))
            {
                index = list.indexOf(tSongPlaylist);
                break;
            }
        }
        return index;
       /* TSongPlaylist item = new TSongPlaylist();
        item.songId = songId;
        return list.indexOf(item);*/
    }
}

package com.madeira.entertainz.karaoke.DBLocal;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Database;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

import java.util.ArrayList;
import java.util.List;

@Database(entities = {TSong.class, TSongCategory.class, TSongPlaylist.class, TPlaylist.class,TLogSong.class, TCountSongPlayed.class},
        version = 3, exportSchema = false)


public abstract class DbSong extends RoomDatabase {

    public abstract DaoAccessTSong daoAccessTSong();
//
    public abstract DaoAccessTSongCategory daoAccessTSongCategory();
//
    public abstract DaoAccessTSongPlaylist daoAccessTSongPlaylist();


    public abstract DaoAccessTPlaylist daoAccessTPlaylist();


    public abstract DaoAccessTLogSong daoAccessTLogSong();

    public abstract DaoAccessTCountSongPlayed daoAccessTCountSongPlayed();


    public static class Instance {
        private static final String DB_THEME = "song.db";

        public static DbSong create(Context context) {
            return Room.databaseBuilder(context, DbSong.class, DB_THEME)
                    .fallbackToDestructiveMigration()
//                    .addMigrations(MIGRATION_1_2)
                    .build();
        }

        //example migration jangan digunakan dulu, masih tahap research
      /*  static final Migration MIGRATION_1_2 = new Migration(1, 2) {
            @Override
            public void migrate(SupportSQLiteDatabase database) {
                // Your migration strategy here
                database.execSQL("ALTER TABLE Users ADD COLUMN user_score INTEGER");
            }
        };*/
    }


    @Dao
    public interface DaoAccessTSongCategory {

        @Insert(onConflict = OnConflictStrategy.REPLACE)
        void insertAll(List<TSongCategory> list);

        @Query("SELECT * FROM TSongcategory")
        List<TSongCategory> getAll();

        @Query("DELETE FROM TSongcategory")
        void deleteAll();
    }

    @Dao
    public interface DaoAccessTSong {

        @Insert(onConflict = OnConflictStrategy.REPLACE)
        void insertAll(List<TSong> list);

        @Insert(onConflict = OnConflictStrategy.FAIL)
        void insert(TSong tSong);

        @Query("SELECT a.*, c.songCategory, b.totalCount FROM TSong a left join TCountSongPlayed b on a.songId = b.songId left join tsongcategory c on a.songCategoryId = c.songCategoryId order by a.songId desc")
        List<JoinSongCountPlayed> getAll();

        @Query("SELECT a.*,b.totalCount FROM TSong a left join TCountSongPlayed b on a.songId=b.songId WHERE a.songCategoryId=:songCategoryId order by a.songId desc")
        List<JoinSongCountPlayed> getByCategory(int songCategoryId);

        @Query("DELETE FROM TSong")
        void deleteAll();

        //todo dibuat join, karen untuk memunculukan jumlah count played nya
        @Query("SELECT a.*,b.totalCount FROM TSong a  left join TCountSongPlayed b on a.songId= b.songId WHERE a.songCategoryId=:songCategoryId and (a.artist like '%'||:keyword||'%' or a.songName like '%'||:keyword||'%')")
        List<JoinSongCountPlayed> getSearchByCategory(int songCategoryId, String keyword);

        @Query("SELECT a.*,b.totalCount FROM TSong a left join TCountSongPlayed b on a.songId=b.songId WHERE  (a.artist like '%'||:keyword||'%' or a.songName like '%'||:keyword||'%')")
        List<JoinSongCountPlayed> getSearchAll(String keyword);

        @Query("SELECT * FROM TSong WHERE songId=:songId")
        TSong getSongById(String songId);

        @Query("DELETE FROM tsong WHERE songId=:songId")
        void delete(String songId);

        @Query("DELETE FROM tsong WHERE songId in (:songId)")
        void deleteWhereIn(String songId);

        @Query("SELECT * FROM TSong order by songId desc")
        List<TSong> getAllTSong();

        @Query("SELECT a.songCategoryId,b.songCategory, COUNT(a.songCategoryId) as totalCount FROM TSong a left join tsongcategory b on a.songCategoryId = b.songCategoryId GROUP BY a.songCategoryId ")
        List<JoinSongCountCategory> getSongCountByCategory();

        @Query("UPDATE TSong SET songName=:songName, artist=:artist, songCategoryId=:songCategoryId, lyric=:lyric WHERE songId=:songId")
        void updateFromEKMA(String songId, String songName, String artist, int songCategoryId, String lyric);
    }

    @Dao
    public interface DaoAccessTSongPlaylist {

        @Insert(onConflict = OnConflictStrategy.REPLACE)
        void insertAll(List<TSongPlaylist> list);

        @Insert(onConflict = OnConflictStrategy.REPLACE)
        void insert(TSongPlaylist TSongPlaylist);

        @Query("SELECT * FROM TSongPlaylist")
        List<TSongPlaylist> getAll();

        @Query("DELETE FROM tsongplaylist WHERE songId=:songId and playlistId=:playlistId")
        void delete(String songId, int playlistId);

        @Query("DELETE FROM tsongplaylist WHERE songId=:songId")
        void deleteBySongId(String songId);

        @Query("DELETE FROM TSongPlaylist")
        void deleteAll();

        @Query("SELECT count(*) FROM TSongPlaylist where songId=:songId and playlistId=:playlistId")
        int existSongPlaylistBySongId(String songId, int playlistId);

        @Query("SELECT a.*, b.songName,b.artist,b.songURL,b.vocalSound,b.duration,b.thumbnail,b.iv,b.'key' FROM TSongPlaylist a left join tsong b on a.songId = b.songId  WHERE a.playlistId=:playlistId")
        List<JoinSongPlaylist> getByPlaylistId(int playlistId);

        @Query("DELETE FROM tsongplaylist WHERE playlistId=:playlistId")
        void deleteByPlaylist(int playlistId);

        @Query("DELETE FROM TSongPlaylist WHERE songId in (:songId)")
        void deleteWhereIn(String songId);


    }


    @Dao
    public interface DaoAccessTPlaylist {

        @Insert(onConflict = OnConflictStrategy.REPLACE)
        void insertAll(List<TPlaylist> list);

        @Insert(onConflict = OnConflictStrategy.REPLACE)
        void insert(TPlaylist TPlaylist);

        @Query("SELECT * FROM TPlaylist")
        List<TPlaylist> getAll();

        @Query("DELETE FROM TPlaylist WHERE playlistId=:playlistId")
        void delete(int playlistId);

        @Query("DELETE FROM TPlaylist")
        void deleteAll();

        @Query("UPDATE TPlaylist SET playlistName=:playlistName WHERE playlistId=:playlistId")
        void renamePlaylist(int playlistId, String playlistName);

        @Query("SELECT * FROM TPlaylist order by playlistId desc limit 0,1")
        int lastId();

        @Query("SELECT a.playlistId, a.playlistName, count(b.songId) as sumOfSong, sum(c.duration) as sumOfDuration FROM TPlaylist a left join tsongplaylist b on a.playlistId = b.playlistId left join tsong c on b.songId = c.songId group by b.playlistId ")
        List<JoinPlaylistDt> joinPlaylist();

        @Query("SELECT count(*) FROM TPlaylist where lower(playlistName)=lower(:playlistName)")
        int existPlaylistName(String playlistName);

        @Query("SELECT * FROM TPlaylist where playlistId=:playlistId")
        TPlaylist getById(int playlistId);

        @Query("SELECT count(*) FROM TPlaylist")
        int checkExistPlaylist();
    }

    @Dao
    public interface DaoAccessTLogSong {

        @Insert(onConflict = OnConflictStrategy.REPLACE)
        void insertAll(List<TLogSong> list);

        @Insert(onConflict = OnConflictStrategy.REPLACE)
        void insert(TLogSong TLogSong);

        @Query("SELECT * FROM TLogSong")
        List<TLogSong> getAll();

        @Query("DELETE FROM TLogSong WHERE songId=:songId")
        void delete(int songId);

        @Query("DELETE FROM TLogSong")
        void deleteAll();

        @Query("UPDATE TLogSong SET totalCount=totalCount+1 WHERE songId=:songId")
        void increaseCount(int songId);

        @Query("SELECT count(*) FROM TLogSong where songId=:songId")
        int existSong(int songId);


    }


    @Dao
    public interface DaoAccessTCountSongPlayed {

        @Insert(onConflict = OnConflictStrategy.REPLACE)
        void insertAll(List<TCountSongPlayed> list);

        @Insert(onConflict = OnConflictStrategy.REPLACE)
        void insert(TCountSongPlayed TCountSongPlayed);

        @Query("SELECT * FROM TCountSongPlayed")
        List<TCountSongPlayed> getAll();

        @Query("DELETE FROM TCountSongPlayed WHERE songId=:songId")
        void delete(int songId);

        @Query("DELETE FROM TCountSongPlayed")
        void deleteAll();

        @Query("UPDATE TCountSongPlayed SET totalCount=totalCount+1 WHERE songId=:songId")
        void increaseCount(int songId);

        @Query("SELECT count(*) FROM TCountSongPlayed where songId=:songId")
        int existSong(int songId);

    }

}
package com.madeira.entertainz.karaoke.menu_live_channel;


import android.content.res.ColorStateList;
import android.graphics.drawable.GradientDrawable;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.VideoView;

import com.google.android.exoplayer2.DefaultLoadControl;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.ExoPlayer;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.LoadControl;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.extractor.DefaultExtractorsFactory;
import com.google.android.exoplayer2.extractor.ExtractorsFactory;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.hls.HlsMediaSource;
import com.google.android.exoplayer2.trackselection.AdaptiveVideoTrackSelection;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelection;
import com.google.android.exoplayer2.trackselection.TrackSelector;
import com.google.android.exoplayer2.ui.SimpleExoPlayerView;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.upstream.DefaultHttpDataSourceFactory;
import com.google.android.exoplayer2.util.Util;
import com.madeira.entertainz.karaoke.DBLocal.TRadio;
import com.madeira.entertainz.karaoke.Debug;
import com.madeira.entertainz.karaoke.R;
import com.madeira.entertainz.karaoke.config.CacheData;
import com.madeira.entertainz.karaoke.model_online.ModelChannel;

import java.util.Date;

import es.dmoral.toasty.Toasty;


public class LiveChannelTVFragment extends Fragment implements LiveChannelTheme.ILiveChannelTheme {
    String TAG = "LiveChannelTVFragment";
    View root;
    LiveChannelTheme liveChannelTheme;
    RelativeLayout layoutDetail;
    ColorStateList colorSelection, colorText;
    ModelChannel.subChannelItem subChannelItem = new ModelChannel.subChannelItem();
    //    VideoView videoView;
    StringBuilder sb = new StringBuilder();
    SimpleExoPlayer simpleExoPlayer;
    static SimpleExoPlayerView mExoPlayerView;
    LinearLayout navInfoView;
    RelativeLayout exoplayerRelativeLayout;
    TextView channelNameTV;

    public static final String KEY_CHANNEL = "KEY_CHANNEL";

    ProgressBar progressBar;


    public LiveChannelTVFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            subChannelItem = (ModelChannel.subChannelItem) getArguments().getSerializable(KEY_CHANNEL);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        try {
            root = inflater.inflate(R.layout.fragment_live_channel_tv, container, false);
            bind();
            liveChannelTheme = new LiveChannelTheme(getActivity(), this);
            liveChannelTheme.setTheme();
            setupTV();
            LiveChannelActivity.setCallBack(iLiveChannelActivityListener);
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
        return root;
    }

    void bind() {
        try {
            layoutDetail = root.findViewById(R.id.layout_detail);
//            videoView = root.findViewById(R.id.videoView);
            mExoPlayerView = root.findViewById(R.id.exoplayerview);
            progressBar = root.findViewById(R.id.progressBar);
            navInfoView = root.findViewById(R.id.nav_info_view);
            exoplayerRelativeLayout = root.findViewById(R.id.exoplayer_rl);
            channelNameTV = root.findViewById(R.id.channel_name_tv);
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }


    @Override
    public void onDetach() {
        try {
            super.onDetach();
            simpleExoPlayer.release();
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    @Override
    public void setThemeUpdateDate(Date lastUpdateTheme) {

    }

    @Override
    public void setListColorTheme(int color) {
        try {
            layoutDetail.setBackgroundResource(R.drawable.tags_rounded_corners);
            GradientDrawable categoryDrawable = (GradientDrawable) layoutDetail.getBackground();
            categoryDrawable.setColor(color);

            navInfoView.setBackgroundResource(R.drawable.tags_rounded_corners);
            GradientDrawable navInfoDrawable = (GradientDrawable) navInfoView.getBackground();
            navInfoDrawable.setColor(color);
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    @Override
    public void setTextAndSelectionColorTheme(ColorStateList colorSelection, ColorStateList colorText, int colorTextSelected, int colorTextFocus, int colorTextNormal, int colorSelectionSelected, int colorSelectionFocus) {
        try {
            this.colorSelection = colorSelection;
            this.colorText = colorText;
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    void setupTV() {
        try {
            channelNameTV.setText(subChannelItem.channelName);
            Uri url = Uri.parse(subChannelItem.urlChannel);
            Handler mainHandler = new Handler();
            DefaultBandwidthMeter bandwidthMeter = new DefaultBandwidthMeter();
            TrackSelector trackSelector = new DefaultTrackSelector(new Handler());
            LoadControl loadControl = new DefaultLoadControl();
            if (simpleExoPlayer != null)
                simpleExoPlayer.release();
            simpleExoPlayer = ExoPlayerFactory.newSimpleInstance(getActivity(), trackSelector, loadControl);
            mExoPlayerView.setPlayer(simpleExoPlayer);
            mExoPlayerView.setUseController(false);
            DataSource.Factory dataSourceFactory = new DefaultHttpDataSourceFactory(
                    Util.getUserAgent(getActivity(), getString(R.string.app_name)), bandwidthMeter);
            ExtractorsFactory extractorsFactory = new DefaultExtractorsFactory();

//            Uri uri = Uri.fromFile(mEncryptedFile);
            MediaSource videoSource = new HlsMediaSource(url, dataSourceFactory, mainHandler, null);

            simpleExoPlayer.prepare(videoSource);
            simpleExoPlayer.setPlayWhenReady(true);
            simpleExoPlayer.addListener(new ExoPlayer.EventListener() {
                @Override
                public void onLoadingChanged(boolean isLoading) {

                }

                @Override
                public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
                    try {
                        if (playbackState == ExoPlayer.STATE_BUFFERING) {
                            progressBar.setVisibility(View.VISIBLE);
                        } else {
                            // player paused in any state
                            progressBar.setVisibility(View.GONE);
                        }
                    } catch (Exception ex) {
                        Debug.e(TAG, ex);
                    }
                }

                @Override
                public void onTimelineChanged(Timeline timeline, Object manifest) {

                }


                @Override
                public void onPlayerError(ExoPlaybackException error) {
                    try {
                        //dipanggil setup tv lagi, karena terkadang hls cacche nya bermasalah, solve sementara nanti harus dicari fix error nya
                        setupTV();
//                        Toasty.error(getContext(),"Can't play this channel",Toasty.LENGTH_LONG).show();
                    } catch (Exception ex) {
                        Debug.e(TAG, ex);
                    }
                }

                @Override
                public void onPositionDiscontinuity() {

                }
            });


           /* videoView.setVideoURI(url);
            videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    try {
//                        videoView.start();
                        mp.start();
                    } catch (Exception ex) {
                        Debug.e(TAG, ex);
                    }
                    mp.setOnVideoSizeChangedListener(new MediaPlayer.OnVideoSizeChangedListener() {
                        @Override
                        public void onVideoSizeChanged(MediaPlayer mp, int arg1, int arg2) {
                            try {
                                mp.start();
                            } catch (Exception ex) {
                                Debug.e(TAG, ex);
                            }
                        }
                    });
                }
            });

            videoView.setOnErrorListener(new MediaPlayer.OnErrorListener() {
                @Override
                public boolean onError(MediaPlayer mp, int what, int extra) {
                    try {
                        Log.d(TAG, "Video view error");
                        mp.release();
                    } catch (Exception ex) {
                        Debug.e(TAG, ex);
                    }
                    return true;
                }
            });*/
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }


    void processKey(int keyCode) {
        try {
            switch (keyCode) {
                case KeyEvent.KEYCODE_0:
                    sb.append('0');
                    break;
                case KeyEvent.KEYCODE_1:
                    sb.append('1');
                    break;
                case KeyEvent.KEYCODE_2:
                    sb.append('2');
                    break;
                case KeyEvent.KEYCODE_3:
                    sb.append('3');
                    break;
                case KeyEvent.KEYCODE_4:
                    sb.append('4');
                    break;
                case KeyEvent.KEYCODE_5:
                    sb.append('5');
                    break;
                case KeyEvent.KEYCODE_6:
                    sb.append('6');
                    break;
                case KeyEvent.KEYCODE_7:
                    sb.append('7');
                    break;
                case KeyEvent.KEYCODE_8:
                    sb.append('8');
                    break;
                case KeyEvent.KEYCODE_9:
                    sb.append('9');
                    break;
            }

            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    try {
                        if (!sb.toString().equals("")) {
                            if (Integer.valueOf(sb.toString()) <= CacheData.channelItemList.size() && Integer.valueOf(sb.toString()) >= 1) {
                               /* tvVideoViewFragment.stopPlaying();
                                CacheData.chanelPosition = Integer.valueOf(sb.toString()) - 1;
                                CacheData.chanelName = tvChanelList.get(CacheData.chanelPosition).name;*/
                                subChannelItem = CacheData.channelItemList.get(Integer.valueOf(sb.toString()) - 1);
                                setupTV();
                                //reset string
                                sb.setLength(0);
                                sb = new StringBuilder();
                            } else {
                                Toasty.info(getActivity(), "Total Chanel : " + CacheData.channelItemList.size()).show();
                                sb.setLength(0);
                                sb = new StringBuilder();
                            }
                        }
                    } catch (NumberFormatException e) {
                        Debug.e(TAG, e);
                    }
                }
            }, 900);
        } catch (Exception e) {
            Debug.e(TAG, e);
        }
    }

    LiveChannelActivity.ILiveChannelActivityListener iLiveChannelActivityListener = new LiveChannelActivity.ILiveChannelActivityListener() {
        @Override
        public void onFullScreen(boolean fullscreen) {
            try {
                RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) exoplayerRelativeLayout.getLayoutParams();
                if (fullscreen) {
                    layoutDetail.setPadding(0, 0, 0, 0);
                    navInfoView.setVisibility(View.GONE);
                    channelNameTV.setVisibility(View.GONE);
                    lp.width = RelativeLayout.LayoutParams.MATCH_PARENT;
                    lp.height = RelativeLayout.LayoutParams.MATCH_PARENT;
                } else {
                    int dimenPadding = (int) getResources().getDimension(R.dimen.padding_16dp);
                    layoutDetail.setPadding(dimenPadding, dimenPadding, dimenPadding, dimenPadding);
                    navInfoView.setVisibility(View.VISIBLE);
                    channelNameTV.setVisibility(View.VISIBLE);
                    lp.width = 500;
                    lp.height = 250;
                }
                exoplayerRelativeLayout.setLayoutParams(lp);

            } catch (Exception ex) {
                Debug.e(TAG, ex);
            }
        }
    };

    public void releaseExoPlayer() {
        try {
            if (simpleExoPlayer != null)
                simpleExoPlayer.release();
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        try {
            releaseExoPlayer();
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }
}

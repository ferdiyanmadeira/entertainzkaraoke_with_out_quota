package com.madeira.entertainz.karaoke.menu_setting_admin;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.madeira.entertainz.karaoke.Debug;
import com.madeira.entertainz.karaoke.R;
import com.madeira.entertainz.karaoke.SplashScreenActivity;
import com.madeira.entertainz.karaoke.config.Global;
import com.madeira.entertainz.library.Util;

import java.io.IOException;

public class SettingAdminActivity extends AppCompatActivity {
    static String TAG = "SettingAdminActivity";
    boolean isLogon;

    //2 variable ini tdk dimasukkan ke dalam shared pref, karena sifatnya temporary
    //berbeda dgn app android, dimana sessionId di pergunakan utk pemakaian sehari2
    //sessionId disini hanya di pakai utk setting saja, shg tdk boleh di simpan
    //
    String adminSessionId;
    String salt;

    public static void startActivity(Activity activity) {
        Intent intent = new Intent(activity, SettingAdminActivity.class);
        activity.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        try {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_setting_admin);

            try {
                Runtime.getRuntime().exec("service call activity 42 s16 com.android.systemui");
            } catch (IOException e) {
                e.printStackTrace();
            }

            hideNavigationBar();
            installNavigationListener();

            showFragment();

            String languageId = Global.getLanguageId();
            Util.setLanguage(this, languageId);
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    void hideNavigationBar() {
        try {
            View decorView = getWindow().getDecorView();
// Hide both the navigation bar and the status bar.
// SYSTEM_UI_FLAG_FULLSCREEN is only available on Android 4.1 and higher, but as
// a general r ule, you should design your app to hide the status bar whenever you
// hide the navigation bar.
//        int uiOptions = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION | View.SYSTEM_UI_FLAG_FULLSCREEN;
//        decorView.setSystemUiVisibility(uiOptions);

            decorView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                    | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    void installNavigationListener() {
        try {
            View decorView = getWindow().getDecorView();

            //this add event, will hide again if shown
            decorView.setOnSystemUiVisibilityChangeListener(new View.OnSystemUiVisibilityChangeListener() {
                @Override
                public void onSystemUiVisibilityChange(int visibility) {
                    // Order that system bars will only be "visible" if none of the
                    // LOW_PROFILE, HIDE_NAVIGATION, or FULLSCREEN flags are set.
                    if ((visibility & View.SYSTEM_UI_FLAG_FULLSCREEN) == 0) {
                        hideNavigationBar();
                    } else {
                        // adjustments to your UI, such as hiding the action bar or
                        // other navigational controls.
                    }
                }
            });
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    void showFragment() {

       /* if (isLogon==false){
            LoginFragment.newInstance(this, R.id.fragment, callbackLogin);
            return;
        }
*/
        SettingFragment.newInstance(this, R.id.fragment, adminSessionId, salt, callbackSetting);
    }


    SettingFragment.ISettingFragment callbackSetting = new SettingFragment.ISettingFragment() {
        @Override
        public void onReload() {
            finish();
            SplashScreenActivity.startActivity(SettingAdminActivity.this);
        }
    };

    @Override
    public void onResume() {
        super.onResume();
        Util.hideNavigationBar(this);
    }

    @Override
    public void onDestroy()
    {
        try
        {
            super.onDestroy();
            Util.freeMemory();
        }
        catch (Exception ex)
        {
            Debug.e(TAG, ex);
        }
    }
}

package com.madeira.entertainz.karaoke.menu_update_song;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.androidnetworking.error.ANError;
import com.madeira.entertainz.karaoke.DBLocal.JoinSettingElementMedia;
import com.madeira.entertainz.karaoke.DBLocal.TLocalFile;
import com.madeira.entertainz.karaoke.DBLocal.TSongItemServer;
import com.madeira.entertainz.karaoke.DBLocal.TSticker;
import com.madeira.entertainz.karaoke.Debug;
import com.madeira.entertainz.karaoke.DownloadSongService;
import com.madeira.entertainz.karaoke.R;
import com.madeira.entertainz.karaoke.config.C;
import com.madeira.entertainz.karaoke.config.CacheData;
import com.madeira.entertainz.karaoke.config.Global;
import com.madeira.entertainz.karaoke.menu_main.BottomNavigationFragment;
import com.madeira.entertainz.karaoke.menu_main.ListMoodFragment;
import com.madeira.entertainz.karaoke.menu_navigation.TopNavigationFragment;
import com.madeira.entertainz.karaoke.model_online.ModelSong;
import com.madeira.entertainz.karaoke.model_room_db.ModelElementMedia;
import com.madeira.entertainz.library.Util;

import java.io.File;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import static com.madeira.entertainz.karaoke.config.CacheData.lastIndexBackground;

public class UpdateSongActivity extends AppCompatActivity {
    String TAG = "UpdateSongActivity";
    ImageView backgroundIV;
    RelativeLayout progressBarRL;
    ProgressBar progressBar;
    ModelSong.allSongItem listSongItem = new ModelSong.allSongItem();
    Handler handler = new Handler();
    Runnable runnable;
    TopNavigationFragment topNavigationFragment = new TopNavigationFragment();
    BottomNavigationFragment bottomNavigationFragment = new BottomNavigationFragment();
    UpdateSongListFragment updateSongListFragment = new UpdateSongListFragment();
    UpdateSongCategoryFragment updateSongCategoryFragment = new UpdateSongCategoryFragment();
    ListMoodFragment listMoodFragment = new ListMoodFragment();
    View listMoodView;

    ArrayList<TSongItemServer> songItemList = new ArrayList<>();

    public static void startActivity(Activity activity) {
        Intent intent = new Intent(activity, UpdateSongActivity.class);
        activity.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            setContentView(R.layout.activity_update_song);
            bind();
            bindFragment();
            fetchListBackground();
            Util.hideNavigationBar(this);

            String languageId = Global.getLanguageId();
            Util.setLanguage(this, languageId);
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    void bind() {
        backgroundIV = (ImageView) findViewById(R.id.backgroundIV);
        progressBarRL = findViewById(R.id.progressBarRL);
        listMoodView = findViewById(R.id.listMoodFrameLayout);
        progressBar = findViewById(R.id.progressBar);
    }

    void bindFragment() {
        try {
            topNavigationFragment = topNavigationFragment.newInstance(C.MENU_UPDATE_SONG);
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.categoryFrameLayout, updateSongCategoryFragment);
            ft.replace(R.id.topNavFrameLayout, topNavigationFragment);
            ft.replace(R.id.bottomFrameLayout, bottomNavigationFragment);
            ft.replace(R.id.listMoodFrameLayout, listMoodFragment);

            Bundle listBundle = new Bundle();
            listBundle.putInt(updateSongListFragment.KEY_GENRE_ID, 0);
            updateSongListFragment.setArguments(listBundle);
            ft.replace(R.id.updateSongFrameLayout, updateSongListFragment).addToBackStack(null);
            ft.commit();
            updateSongListFragment.setCallBack(iUpdateSongListener);
            updateSongCategoryFragment.setCallback(iUpdateSongCategoryListener);
            bottomNavigationFragment.setCallback(bottomNavigationFragmentListener);
            listMoodFragment.setCallBack(listMoodListener);
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    ModelSong.allSongItem fetchSong() {
        listSongItem = new ModelSong.allSongItem();
        try {
            String mac = Util.getMACAddress("eth0");
            mac = mac.replace(":", "");
            ModelSong.getAllSong(mac, new ModelSong.IGetAllSong() {
                @Override
                public void onGetAllSong(ModelSong.allSongItem result) {
                    if (result != null) {
                        listSongItem = result;
                    }
                }

                @Override
                public void onError(ANError e) {
                    String test;
                }
            });
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
        return listSongItem;
    }

    void fetchListBackground() {
        ModelElementMedia.getListElementMediaActive(this, new ModelElementMedia.IGetListElementMediaActive() {
            @Override
            public void onGetListElementMedia(List<JoinSettingElementMedia> joinSettingElementMediaList) {
                int totalIndex = joinSettingElementMediaList.size();
                runnable = new Runnable() {
                    @Override
                    public void run() {
                        try {
                            if (joinSettingElementMediaList.size() != 0) {
                                if (lastIndexBackground >= totalIndex)
                                    lastIndexBackground = 0;

                                JoinSettingElementMedia joinSettingElementMedia = joinSettingElementMediaList.get(lastIndexBackground);
                                Uri uri = Uri.fromFile(new File(joinSettingElementMedia.urlImage));
//                            Picasso.with(KaraokeActivity.this).load(uri).into(backgroundIV);
                                InputStream inputStream = getContentResolver().openInputStream(uri);
                                BitmapFactory.Options options = new BitmapFactory.Options();
                                options.inPreferredConfig = Bitmap.Config.RGB_565;
                                Bitmap image = BitmapFactory.decodeStream(inputStream, null, options);
                                backgroundIV.setImageBitmap(image);

                                lastIndexBackground++;
                                handler.postDelayed(this::run, joinSettingElementMedia.duration);
                            }
                        } catch (Exception ex) {
                            Debug.e(TAG, ex);
                        }
                    }
                };
                handler.postDelayed(runnable, 100);
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        //agar runable tidak running ketika di finish activity
        try {
            if (checkUpdateList()) {
                showAlertUpdateSong();
            } else {
                handler.removeCallbacks(runnable);
                handler.removeCallbacksAndMessages(null);
                finish();
            }
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        try {
            Util.hideNavigationBar(this);
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    UpdateSongListFragment.IUpdateSongListener iUpdateSongListener = new UpdateSongListFragment.IUpdateSongListener() {
        @Override
        public void onUpdate(TSongItemServer songItem, boolean fgAdd) {
            try {
                if (fgAdd)
                    songItemList.add(songItem);
                else {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                        songItemList.removeIf(x -> (x.songName == songItem.songName && x.artist == songItem.artist));
                        C.FG_UPDATE_SONG = true;
                    } else {
                        int removeIndex = -1;
                        int index = 0;
                        for (TSongItemServer item : songItemList) {
                            if (item.songName.equals(songItem.songName) && item.artist.equals(songItem.artist)) {
                                removeIndex = index;
                                break;
                            }
                            index++;
                        }
                        songItemList.remove(removeIndex);
                    }

                }
            } catch (Exception ex) {
                Debug.e(TAG, ex);
            }
        }
    };
    UpdateSongCategoryFragment.IUpdateSongCategoryListener iUpdateSongCategoryListener = new UpdateSongCategoryFragment.IUpdateSongCategoryListener() {
        @Override
        public void onCategoryClick(int categoryId) {
            try {
                updateSongListFragment = new UpdateSongListFragment();
                FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                Bundle listBundle = new Bundle();
                listBundle.putInt(updateSongListFragment.KEY_GENRE_ID, categoryId);
                updateSongListFragment.setArguments(listBundle);
                ft.replace(R.id.updateSongFrameLayout, updateSongListFragment).addToBackStack(null);
                ft.commit();
                updateSongListFragment.setCallBack(iUpdateSongListener);
            } catch (Exception ex) {
                Debug.e(TAG, ex);
            }

        }
    };

    boolean checkUpdateList() {
        //check apakah harus update lagu
        try {
            if (C.FG_UPDATE_SONG == false) {
                for (TSongItemServer songItem : songItemList) {
                    String[] fileNameSplit = songItem.songPath.split("/");
                    String fileName = fileNameSplit[fileNameSplit.length - 1];
                    String pathSTB = "";
//                String pathSTB = Global.getUSBPath() + "/" + C.PATH_VIDEO_KARAOKE + "/" + fileName;

                    TLocalFile tLocalFile = CacheData.hashFile.get(fileName);
                    if (tLocalFile != null)
                        pathSTB = tLocalFile.content;
                    File file = new File(pathSTB);
                    if (file.exists()) {
                        int file_size = Integer.parseInt(String.valueOf(file.length() / 1024));
                        if (file_size != Integer.valueOf(songItem.filesize)) {
                            C.FG_UPDATE_SONG = true;
                            break;
                        } else
                            C.FG_UPDATE_SONG = false;
                    } else {
                        C.FG_UPDATE_SONG = true;
                        break;
                    }
                }
            }
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }

        return C.FG_UPDATE_SONG;
    }

    void showAlertUpdateSong() {
        try {
            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(UpdateSongActivity.this);
            LayoutInflater inflater = this.getLayoutInflater();
            View dialogView = inflater.inflate(R.layout.alert_update_song, null);
            dialogBuilder.setView(dialogView);

            TextView textView = dialogView.findViewById(R.id.textView);
            Button okButton = dialogView.findViewById(R.id.okButton);
            Button backSelectingButton = dialogView.findViewById(R.id.backSelectingButton);
            Button cancelButton = dialogView.findViewById(R.id.cancelButton);
            textView.setText(getString(R.string.saveUpdateSong));
            AlertDialog alertDialog = dialogBuilder.create();
            alertDialog.setCancelable(false);

            okButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        alertDialog.dismiss();
                        /** diganti menggunakan service agar bisa running background download song nya **/
                        Intent intent = new Intent(getApplicationContext(), DownloadSongService.class);
                        intent.putExtra(DownloadSongService.KEY_SONGITEMLIST, songItemList);
                        startService(intent);
//                DownloadContentActivity.startActivity(UpdateSongActivity.this, songItemList);
                        finish();
                    } catch (Exception ex) {
                        Debug.e(TAG, ex);
                    }
                }
            });
            backSelectingButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        alertDialog.dismiss();
                        topNavigationFragment = topNavigationFragment.newInstance(C.MENU_UPDATE_SONG);
                        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                        ft.replace(R.id.categoryFrameLayout, updateSongCategoryFragment);
                        ft.replace(R.id.topNavFrameLayout, topNavigationFragment);

                        Bundle listBundle = new Bundle();
                        listBundle.putInt(updateSongListFragment.KEY_GENRE_ID, 0);
                        updateSongListFragment.setArguments(listBundle);
                        ft.replace(R.id.updateSongFrameLayout, updateSongListFragment).addToBackStack(null);
                        ft.commit();
                        updateSongListFragment.setCallBack(iUpdateSongListener);
                        updateSongCategoryFragment.setCallback(iUpdateSongCategoryListener);
                    } catch (Exception ex) {
                        Debug.e(TAG, ex);
                    }

                }
            });
            cancelButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        alertDialog.dismiss();
                        CacheData.hashSongNameNArtis.clear();
                        C.FG_UPDATE_SONG = false;
                        finish();
                    } catch (Exception ex) {
                        Debug.e(TAG, ex);
                    }
                }
            });

            alertDialog.show();
            WindowManager.LayoutParams layoutParams = alertDialog.getWindow().getAttributes();
            layoutParams.width = ViewGroup.LayoutParams.WRAP_CONTENT;
            alertDialog.show();
            alertDialog.getWindow().setLayout(430, WindowManager.LayoutParams.WRAP_CONTENT);
            Util.hideNavigationBar(UpdateSongActivity.this);
        }catch (Exception ex)
        {
            Debug.e(TAG, ex);
        }

    }

    /**
     * listener ketika mood di click
     */
    BottomNavigationFragment.IMoodFragmentListener bottomNavigationFragmentListener = new BottomNavigationFragment.IMoodFragmentListener() {
        @Override
        public void onMoodClick() {
            try {
                listMoodView.setVisibility(View.VISIBLE);
            }catch (Exception ex)
            {
                Debug.e(TAG, ex);
            }
        }
    };

    /**
     * listener ketika mood dipilih
     */
    ListMoodFragment.IListMoodListener listMoodListener = new ListMoodFragment.IListMoodListener() {
        @Override
        public void onClick(TSticker tSticker) {
            try {
                listMoodView.setVisibility(View.GONE);
                bottomNavigationFragment.updateMood(tSticker);
                Global.setStickerId(tSticker.stickerId);
            } catch (Exception ex) {
                Debug.e(TAG, ex);
            }
        }
    };

    @Override
    public void onDestroy() {
        try {
            super.onDestroy();
            Util.freeMemory();
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }
}

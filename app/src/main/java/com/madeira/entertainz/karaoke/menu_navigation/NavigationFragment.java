package com.madeira.entertainz.karaoke.menu_navigation;

import android.content.Context;
import android.content.res.ColorStateList;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.madeira.entertainz.karaoke.DBLocal.TElement;
import com.madeira.entertainz.karaoke.Debug;
import com.madeira.entertainz.karaoke.R;
import com.madeira.entertainz.karaoke.SplashScreenActivity;
import com.madeira.entertainz.karaoke.config.C;
import com.madeira.entertainz.karaoke.config.Global;
import com.madeira.entertainz.karaoke.menu_main.MainTheme;
import com.madeira.entertainz.library.AnimFadeInOut;
import com.madeira.entertainz.library.Util;

import java.util.Date;

public class NavigationFragment extends Fragment implements MainTheme.IMainThemeListener {
    //todo digunakan untuk main activity saja
    String TAG = "NavigationFragment";
    private static final int ANIM_NEW_MESSAGE_DURATION = 1000;

    View root;
    RelativeLayout rootRL;
    ImageView logoIV;
    //    static TextView titleApp;
//    LinearLayout titleActivityLL;
//    ImageView titleActivityIV;
//    TextView titleActivityTV;
    LinearLayout notConnectedUSBLL;
    Button rescanButton;
    AnimFadeInOut animFadeInOut;
    TextView tvNewMessage;

    Runnable runCheckMessage;
    Handler handlerCheckMessage = new Handler();

    MainTheme mainTheme;

    private iTopNavigationListener mListener;

    static String KEY_ShowLogo = "SHOW_LOGO";
    static String KEY_TITLE = "KEY_MENU_ID";
    String titleActivity = "";

    public interface iTopNavigationListener {
        void onCheckMessage();
    }

    public NavigationFragment() {
    }

    // TODO: Rename and change types and number of parameters
    public static NavigationFragment newInstance(String title) {
        NavigationFragment fragment = new NavigationFragment();
        Bundle bundle = new Bundle();
        bundle.putString(KEY_TITLE, title);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
//            showLogo = getArguments().getBoolean(KEY_ShowLogo);
            titleActivity = getArguments().getString(KEY_TITLE);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        try {
            root = inflater.inflate(R.layout.fragment_navigation, container, false);
            bind();
            setLogoTitle();
            mainTheme = new MainTheme(getActivity(), this);
            mainTheme.setTheme();
            setTitleApp(Global.getTitleApp());
            //di comment karena diganti di bottom navigation
//            checkMessage();

        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }

        return root;
    }

    public void checkMessage() {
        runCheckMessage = new Runnable() {
            @Override
            public void run() {
                boolean hasUnReadMessage = Global.getUnReadMessage();
                if (hasUnReadMessage) {
                    showIconUnreadMessage();
                } else
                    tvNewMessage.setVisibility(View.INVISIBLE);

                handlerCheckMessage.postDelayed(runCheckMessage, 1000);
            }
        };
        handlerCheckMessage.postDelayed(runCheckMessage, 1000);

    }

    void bind() {
        try {
            rootRL = (RelativeLayout) root.findViewById(R.id.rootRL);
            logoIV = (ImageView) root.findViewById(R.id.logoIV);
//            titleApp = (TextView) root.findViewById(R.id.titleApp);
//            titleActivityLL = root.findViewById(R.id.titleActivityLL);
//            titleActivityIV = root.findViewById(R.id.titleActivityIV);
//            titleActivityTV = root.findViewById(R.id.titleActivityTV);
            notConnectedUSBLL = root.findViewById(R.id.notConnectedUSBLL);
            rescanButton = root.findViewById(R.id.rescanButton);
            tvNewMessage = root.findViewById(R.id.text_new_message);

        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    void setLogoTitle() {
        try {
            if (titleActivity.equals("")) {
                notConnectedUSBLL.setVisibility(View.GONE);
                int notConnectedUSBFlag = Global.getConnectedUSB();
                if (notConnectedUSBFlag == 1) {
                    notConnectedUSBLL.setVisibility(View.GONE);
                } else {
                    notConnectedUSBLL.setVisibility(View.VISIBLE);
                }

                rescanButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        SplashScreenActivity.startActivity(getActivity());
                        getActivity().finish();
                    }
                });
            } else {
//                titleActivityTV.setText(titleActivity);
            }
//        if(titleActivity.equals(getString(R.string.title_search_song)))
//            titleActivityIV.setBackground(getActivity().getDrawable(R.drawable.ic_arrow_up));

            if (C.FOR_HOTEL)
                logoIV.setVisibility(View.GONE);
            else
                logoIV.setVisibility(View.VISIBLE);
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    public static void setTitleApp(String prmTitle) {
//        titleApp.setText(prmTitle);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
//        mListener = null;
    }

    @Override
    public void setThemeUpdateDate(Date lastUpdateTheme) {

    }

    @Override
    public void setBackgroundTheme(TElement element) {

    }

    @Override
    public void setListColorTheme(int color) {

    }

    @Override
    public void setFooterColorTheme(int color) {
    }

    @Override
    public void setTextAndSelectionColorTheme(ColorStateList colorSelection, ColorStateList colorText, int colorTextSelected, int colorTextFocus, int colorTextNormal, int colorSelectionSelected, int colorSelectionFocus) {
        tvNewMessage.setTextColor(colorTextNormal);
    }

    @Override
    public void setHeaderColorTheme(int color) {
        rootRL.setBackgroundColor(color);
    }

    void showIconUnreadMessage() {

//        Log.i(TAG, "showIconUnreadMessage");


        tvNewMessage.setVisibility(View.VISIBLE);

        if (animFadeInOut == null) {
            animFadeInOut = new AnimFadeInOut(tvNewMessage);
            animFadeInOut.start(ANIM_NEW_MESSAGE_DURATION);
        }

    }

    public void setCallback(iTopNavigationListener listener) {
        this.mListener = listener;
    }

    @Override
    public void onDestroy() {
        try {
            super.onDestroy();
            Util.freeMemory();
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

}

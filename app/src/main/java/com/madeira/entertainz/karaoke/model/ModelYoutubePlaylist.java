package com.madeira.entertainz.karaoke.model;

import android.util.Log;

import com.google.gson.Gson;
import com.madeira.entertainz.karaoke.DBLocal.TSongYoutubePlaylist;
import com.madeira.entertainz.karaoke.DBLocal.TYoutubePlaylist;
import com.madeira.entertainz.library.ApiError;
import com.madeira.entertainz.library.Util;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ModelYoutubePlaylist {

    static String TAG = "ModelYoutubePlaylist";

    public static List<TYoutubePlaylist> getYoutubePlaylist(String filePath) {
        List<TYoutubePlaylist> tYoutubePlaylists = new ArrayList<TYoutubePlaylist>();
                String stringJSON = "";
                try {
                    File file = new File(filePath);
                    FileInputStream fileInputStream = new FileInputStream(file);
                    stringJSON = Util.getJSONFromTextFile(fileInputStream);
                    Log.d(TAG, filePath);
                    Gson gson = new Gson();

                    //throw apabila ada apiError
                    try {
                        ApiError.process(gson, stringJSON);
                        ResultYoutubePlaylistList r = gson.fromJson(stringJSON, ResultYoutubePlaylistList.class);
                        tYoutubePlaylists = r.list;
                    } catch (ApiError apiError) {
                        apiError.printStackTrace();
                    }

                    //if no error, next convert the real data

                } catch (IOException e) {
                    e.printStackTrace();
                }
        return tYoutubePlaylists;
    }

    public static List<TSongYoutubePlaylist> getSongPlaylist(String filePath) {
        List<TSongYoutubePlaylist> tSongPlaylistList = new ArrayList<TSongYoutubePlaylist>();

                String stringJSON = "";
                try {
                    File file = new File(filePath);
                    FileInputStream fileInputStream = new FileInputStream(file);
                    stringJSON = Util.getJSONFromTextFile(fileInputStream);
                    Log.d(TAG, filePath);
                    Gson gson = new Gson();

                    //throw apabila ada apiError
                    try {
                        ApiError.process(gson, stringJSON);
                        ResultSongYoutubePlaylistList r = gson.fromJson(stringJSON, ResultSongYoutubePlaylistList.class);
                        tSongPlaylistList = r.list;
                    } catch (ApiError apiError) {
                        apiError.printStackTrace();
                    }
                    //if no error, next convert the real data
                } catch (IOException e) {
                    e.printStackTrace();
                }
        return tSongPlaylistList;
    }

    public static class ResultYoutubePlaylistList {
        public List<TYoutubePlaylist> list;
    }

    public static class ResultSongYoutubePlaylistList {
        public List<TSongYoutubePlaylist> list;
    }


}

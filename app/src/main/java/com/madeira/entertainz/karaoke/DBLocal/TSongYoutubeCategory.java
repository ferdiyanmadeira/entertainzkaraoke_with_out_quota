package com.madeira.entertainz.karaoke.DBLocal;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;

@Entity(tableName = "TSongYoutubeCategory")
public class TSongYoutubeCategory {

    @PrimaryKey
    @SerializedName("genre_id")
    public int songCategoryId;
    @SerializedName("genre")
    public String songCategory;
}

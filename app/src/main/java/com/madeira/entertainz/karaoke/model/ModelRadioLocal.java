package com.madeira.entertainz.karaoke.model;

import com.google.gson.Gson;
import com.madeira.entertainz.karaoke.DBLocal.TRadio;
import com.madeira.entertainz.library.ApiError;
import com.madeira.entertainz.library.Util;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.List;

public class ModelRadioLocal {
    static String TAG = "ModelRadioLocal";

    public static ResultGetRadioList getRadioFromLocal(String filePath) {
        ResultGetRadioList resultGetThemeList = null;
                String stringJSON = "";
                try {
                    File file = new File(filePath);
                    FileInputStream fileInputStream = new FileInputStream(file);
                    stringJSON = Util.getJSONFromTextFile(fileInputStream);
                    Gson gson = new Gson();

                    try {
                        ApiError.process(gson, stringJSON);
                        resultGetThemeList = gson.fromJson(stringJSON, ResultGetRadioList.class);
                        return resultGetThemeList;
                    } catch (ApiError apiError) {
                        apiError.printStackTrace();
                    }


                } catch (IOException e) {
                    e.printStackTrace();
                }

        return resultGetThemeList;
    }

    public class ResultGetRadioList {
        public List<TRadio> list;
    }

}

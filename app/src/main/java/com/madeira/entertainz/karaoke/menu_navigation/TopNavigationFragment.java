package com.madeira.entertainz.karaoke.menu_navigation;

import android.content.Context;
import android.content.res.ColorStateList;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.madeira.entertainz.karaoke.Debug;
import com.madeira.entertainz.karaoke.R;
import com.madeira.entertainz.karaoke.config.Global;
import com.madeira.entertainz.karaoke.menu_main.MainTheme;
import com.madeira.entertainz.karaoke.DBLocal.TElement;
import com.madeira.entertainz.library.AnimFadeInOut;
import com.madeira.entertainz.library.Util;

import java.util.Date;

import static com.madeira.entertainz.karaoke.config.C.MENU_ACCESSORIES;
import static com.madeira.entertainz.karaoke.config.C.MENU_LIVE_CHANNEL;
import static com.madeira.entertainz.karaoke.config.C.MENU_MESSAGES;
import static com.madeira.entertainz.karaoke.config.C.MENU_NEWS;
import static com.madeira.entertainz.karaoke.config.C.MENU_PLAYLIST;
import static com.madeira.entertainz.karaoke.config.C.MENU_RADIO;
import static com.madeira.entertainz.karaoke.config.C.MENU_SEARCH_SONG;
import static com.madeira.entertainz.karaoke.config.C.MENU_SETTING;
import static com.madeira.entertainz.karaoke.config.C.MENU_UPDATE_SONG;
import static com.madeira.entertainz.karaoke.config.C.MENU_VIDIO;
import static com.madeira.entertainz.karaoke.config.C.MENU_YOUTUBE;
import static com.madeira.entertainz.karaoke.config.C.MENU_YOUTUBE_PLAYLIST;

public class TopNavigationFragment extends Fragment implements MainTheme.IMainThemeListener {
    String TAG = "TopNavigationFragment";
    private static final int ANIM_NEW_MESSAGE_DURATION = 1000;

    View root;
    RelativeLayout rootRL;
    //    ImageView logoIV;
//    static TextView titleApp;
    LinearLayout titleActivityLL;
    ImageView titleActivityIV;
    TextView titleActivityTV;
    LinearLayout notConnectedUSBLL;
    Button rescanButton;
    AnimFadeInOut animFadeInOut;
    TextView tvNewMessage;

    Runnable runCheckMessage;
    Handler handlerCheckMessage = new Handler();

    MainTheme mainTheme;

    private iTopNavigationListener mListener;

    static String KEY_ShowLogo = "SHOW_LOGO";
    static String KEY_MENU_ID = "KEY_MENU_ID";
//    String titleActivity = "";

    public interface iTopNavigationListener {
        void onCheckMessage();
    }

    public TopNavigationFragment() {
    }

    // TODO: Rename and change types and number of parameters
    public static TopNavigationFragment newInstance(int menuId) {
        TopNavigationFragment fragment = new TopNavigationFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(KEY_MENU_ID, menuId);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        try {
            root = inflater.inflate(R.layout.fragment_top_navigation, container, false);
            bind();
            // todo untuk set title dan image logo
            if (getArguments() != null) {
                int menuId = getArguments().getInt(KEY_MENU_ID);
                titleActivityTV.setText(getMenuTitle(menuId));
                titleActivityIV.setBackgroundResource(getMenuImage(menuId));
            }
            //todo setLogoTitle di comment karena fragment ini tidak dipanggil lagi di main activity
//            setLogoTitle();
            mainTheme = new MainTheme(getActivity(), this);
            mainTheme.setTheme();
            //todo setTitleApp di comment karena fragment ini tidak dipanggil lagi di main activity
//            setTitleApp(Global.getTitleApp());
            //di comment karena diganti di bottom navigation
//            checkMessageRoom();

        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }

        return root;
    }

    public void checkMessageRoom() {
        runCheckMessage = new Runnable() {
            @Override
            public void run() {
                boolean hasUnReadMessage = Global.getUnReadMessage();
                if (hasUnReadMessage) {
                    showIconUnreadMessage();
                } else
                    tvNewMessage.setVisibility(View.INVISIBLE);

                handlerCheckMessage.postDelayed(runCheckMessage, 1000);
            }
        };
        handlerCheckMessage.postDelayed(runCheckMessage, 1000);

    }

    void bind() {
        try {
            rootRL = (RelativeLayout) root.findViewById(R.id.rootRL);
//            logoIV = (ImageView) root.findViewById(R.id.logoIV);
//            titleApp = (TextView) root.findViewById(R.id.titleApp);
            titleActivityLL = root.findViewById(R.id.titleActivityLL);
            titleActivityIV = root.findViewById(R.id.titleActivityIV);
            titleActivityTV = root.findViewById(R.id.titleActivityTV);
            notConnectedUSBLL = root.findViewById(R.id.notConnectedUSBLL);
            rescanButton = root.findViewById(R.id.rescanButton);
            tvNewMessage = root.findViewById(R.id.text_new_message);

        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

   /* public void setTitle(String newTitle){
//        titleActivity = newTitle;
//        setLogoTitle();
    }*/

    void setLogoTitle() {
        try {
            /*if (titleActivity.equals("")) {
                notConnectedUSBLL.setVisibility(View.GONE);
                int notConnectedUSBFlag = Global.getConnectedUSB();
                if (notConnectedUSBFlag == 1) {
                    notConnectedUSBLL.setVisibility(View.GONE);
                } else {
                    notConnectedUSBLL.setVisibility(View.VISIBLE);
                }

                rescanButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        SplashScreenActivity.startActivity(getActivity());
                        getActivity().finish();
                    }
                });
            } else {
                titleActivityTV.setText(titleActivity);
            }*/
//        if(titleActivity.equals(getString(R.string.title_search_song)))
//            titleActivityIV.setBackground(getActivity().getDrawable(R.drawable.ic_arrow_up));

           /* if (C.FOR_HOTEL)
                logoIV.setVisibility(View.GONE);
            else
                logoIV.setVisibility(View.VISIBLE);*/
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    public static void setTitleApp(String prmTitle) {
//        titleApp.setText(prmTitle);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
//        mListener = null;
    }

    @Override
    public void setThemeUpdateDate(Date lastUpdateTheme) {

    }

    @Override
    public void setBackgroundTheme(TElement element) {

    }

    @Override
    public void setListColorTheme(int color) {

    }

    @Override
    public void setFooterColorTheme(int color) {
    }

    @Override
    public void setTextAndSelectionColorTheme(ColorStateList colorSelection, ColorStateList colorText, int colorTextSelected, int colorTextFocus, int colorTextNormal, int colorSelectionSelected, int colorSelectionFocus) {
        tvNewMessage.setTextColor(colorTextNormal);
    }

    @Override
    public void setHeaderColorTheme(int color) {
        rootRL.setBackgroundColor(color);
    }

    void showIconUnreadMessage() {

//        Log.i(TAG, "showIconUnreadMessage");


        tvNewMessage.setVisibility(View.VISIBLE);

        if (animFadeInOut == null) {
            animFadeInOut = new AnimFadeInOut(tvNewMessage);
            animFadeInOut.start(ANIM_NEW_MESSAGE_DURATION);
        }

    }

    public void setCallback(iTopNavigationListener listener) {
        this.mListener = listener;
    }

    String getMenuTitle(int menuId) {
        String title = null;

        try {
            switch (menuId) {
                case MENU_SEARCH_SONG:
                    title = getString(R.string.title_search_song);
                    break;
                case MENU_PLAYLIST:
                    title = getString(R.string.title_playlist);
                    break;
                case MENU_UPDATE_SONG:
                    title = getString(R.string.title_update_song);
                    break;
                case MENU_ACCESSORIES:
                    title = getString(R.string.title_accessories);
                    break;
                case MENU_MESSAGES:
                    title = getString(R.string.title_messages);
                    break;
                case MENU_SETTING:
                    title = getString(R.string.title_setting);
                    break;
                case MENU_YOUTUBE:
                    title = getString(R.string.title_youtube);
                    break;
                case MENU_LIVE_CHANNEL:
                    title = getString(R.string.title_live_channel);
                    break;
                case MENU_VIDIO:
                    title = getString(R.string.title_vidio);
                    break;
                case MENU_RADIO:
                    title = getString(R.string.title_radio);
                    break;
                case MENU_YOUTUBE_PLAYLIST:
                    title = getString(R.string.title_youtube_playlist);
                    break;
                case MENU_NEWS:
                    title = getString(R.string.news);
            }
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
        return title;
    }

    int getMenuImage(int menuId) {
        int imageId = 0;

        try {
            switch (menuId) {

                case MENU_SEARCH_SONG:
                    imageId = R.drawable.logo_search_karaoke;
                    break;
                case MENU_PLAYLIST:
                    imageId = R.drawable.logo_karaoke_playlist;
                    break;
                case MENU_UPDATE_SONG:
                    imageId = R.drawable.menu_update_song;
                    break;
                case MENU_ACCESSORIES:
                    imageId = R.drawable.logo_shopping;
                    break;
                case MENU_MESSAGES:
                    imageId = R.drawable.logo_message;
                    break;
                case MENU_SETTING:
                    imageId = R.drawable.logo_setting;
                    break;
                case MENU_LIVE_CHANNEL:
                    imageId = R.drawable.logo_live_channel;
                    break;
                case MENU_YOUTUBE:
                    imageId = R.drawable.logo_youtube;
                    break;
                case MENU_YOUTUBE_PLAYLIST:
                    imageId = R.drawable.logo_youtube_playlist;
                    break;
                case MENU_NEWS:
                    imageId = R.drawable.logo_news;
                    break;
            }
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }

        return imageId;
    }

    @Override
    public void onDestroy()
    {
        try
        {
            super.onDestroy();
            Util.freeMemory();
        }
        catch (Exception ex)
        {
            Debug.e(TAG, ex);
        }
    }

}

package com.madeira.entertainz.karaoke.model_online;

import android.net.Uri;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.OkHttpResponseAndJSONArrayRequestListener;
import com.androidnetworking.interfaces.OkHttpResponseAndJSONObjectRequestListener;
import com.androidnetworking.interfaces.OkHttpResponseListener;
import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;
import com.madeira.entertainz.karaoke.Debug;
import com.madeira.entertainz.karaoke.config.C;
import com.madeira.entertainz.karaoke.config.Global;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import okhttp3.Response;

import static com.madeira.entertainz.karaoke.config.C.DEFAULT_API;

public class ModelApp {
    private static final String TAG = "ModelSTB";

    private static final String APIHOST = Global.getApiHostname() + "/v1/app.php";

    public interface IModelApp {
        void onGetLatestApp(appItem result);

        void onError(ANError e);
    }

    static public void getLatestApp(String appId, IModelApp callBack) {
        try {

            String uri = Uri.parse(APIHOST)
                    .buildUpon()
                    .appendQueryParameter("action", "get_latest")
                    .appendQueryParameter("app_id", appId)
                    .build().toString();

            AndroidNetworking.get(uri)
                    .setPriority(Priority.MEDIUM)
                    .build()
                    .getAsOkHttpResponseAndJSONObject(new OkHttpResponseAndJSONObjectRequestListener() {
                        @Override
                        public void onResponse(Response okHttpResponse, JSONObject response) {
                            try {
                                JSONArray jsonArray = new JSONArray();
                                jsonArray.put(response);
                                if (okHttpResponse.isSuccessful()) {
                                    if(response.has("errCode"))
                                    {
                                        int errorCode =response.getInt("errCode");
                                        String errorMessage = response.getString("errMsg");
                                        ANError anError = new ANError();
                                        anError.setErrorCode(errorCode);
                                        anError.setErrorBody(errorMessage);
                                        anError.setErrorDetail(errorMessage);
                                        callBack.onError(anError);
                                        return;
                                    }
                                    Gson gson = new Gson();
                                    String jsonString = response.toString();
                                    ModelApp.appItem appItem = gson.fromJson(jsonString, ModelApp.appItem.class);
                                    callBack.onGetLatestApp(appItem);
                                }
                            } catch (Exception ex) {
                                ex.printStackTrace();
                            }
                        }

                        @Override
                        public void onError(ANError anError) {
                            callBack.onError(anError);
                        }
                    });

        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }




    public interface ICheckConnectToHost {
        void onConnected(boolean connected);

        void onError(ANError e);
    }

    static public void checkInternet(ICheckConnectToHost callBack) {
        try {

            String uri = Uri.parse(C.DOMAIN_ROOT)
                    .buildUpon()
                    .build().toString();

            AndroidNetworking.get(uri)
                    .setPriority(Priority.MEDIUM)
                    .build().getAsOkHttpResponse(new OkHttpResponseListener() {
                @Override
                public void onResponse(Response response) {
                    if (response.isSuccessful())
                        callBack.onConnected(true);
                    else
                    {
                        callBack.onConnected(false);
                    }
                }

                @Override
                public void onError(ANError anError) {
                    callBack.onError(anError);
                }
            });


        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    public class appItem {
        @SerializedName("app")
        public subAppItem app;
        @SerializedName("stb")
        public  ModelSTB.subSTBItem stb;
        @SerializedName("server_time")
        public String serverTime;

    }

    public class subAppItem {
        @SerializedName("version_code")
        public String versionCode;
        @SerializedName("version_name")
        public String versionName;
        @SerializedName("url_apk")
        public String urlApk;
    }
}

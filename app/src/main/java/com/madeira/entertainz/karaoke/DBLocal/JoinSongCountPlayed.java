package com.madeira.entertainz.karaoke.DBLocal;

import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class JoinSongCountPlayed implements Serializable
{
    @PrimaryKey
    @NonNull
    @SerializedName("songId")
    public String songId;
    @SerializedName("songName")
    public String songName;
    @SerializedName("artist")
    public String artist;
    @SerializedName("songCategoryId")
    public int songCategoryId;
    @SerializedName("songCategory")
    public String songCategory;
    @SerializedName("songURL")
    public String songURL;
    @SerializedName("vocalSound")
    public String vocalSound;
    @SerializedName("duration")
    public String duration;
    @SerializedName("thumbnail")
    public String thumbnail;
    @SerializedName("iv")
    public String iv;
    @SerializedName("key")
    public String key;
    @SerializedName("totalCount")
    public int totalCount;
    @SerializedName("lyric")
    public String lyric;
    //dikasih igone karena jika field tidak error ketika value tidak di set dalam query
    @Ignore
    @SerializedName("songSize")
        public long songSize;
    @Ignore
    @SerializedName("thumbnailSize")
    public long thumbnailSize;
}

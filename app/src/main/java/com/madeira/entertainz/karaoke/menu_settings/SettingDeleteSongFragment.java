package com.madeira.entertainz.karaoke.menu_settings;


import android.app.AlertDialog;
import android.content.res.ColorStateList;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.os.StatFs;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TabWidget;
import android.widget.TextView;

import com.google.gson.Gson;
import com.madeira.entertainz.helper.RecyclerViewAdapter;
import com.madeira.entertainz.karaoke.DBLocal.DbSong;
import com.madeira.entertainz.karaoke.DBLocal.JoinSongCountPlayed;
import com.madeira.entertainz.karaoke.DBLocal.TSong;
import com.madeira.entertainz.karaoke.Debug;
import com.madeira.entertainz.karaoke.R;
import com.madeira.entertainz.karaoke.config.C;
import com.madeira.entertainz.karaoke.config.CacheData;
import com.madeira.entertainz.karaoke.config.Global;
import com.madeira.entertainz.karaoke.model.ModelSongLocal;
import com.madeira.entertainz.library.PerformAsync;
import com.madeira.entertainz.library.PerformAsync2;
import com.madeira.entertainz.library.RootUtil;
import com.madeira.entertainz.library.StorageUtils;
import com.madeira.entertainz.library.Util;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import es.dmoral.toasty.Toasty;

/**
 * A simple {@link Fragment} subclass.
 */
public class SettingDeleteSongFragment extends Fragment implements SettingsTheme.ISettingsTheme {

    static String TAG = "SettingDeleteSongFragment";
    View root;
    SettingsTheme settingsTheme;
    List<JoinSongCountPlayed> tSongList = new ArrayList<>();
    RecyclerViewAdapter adapter;

    LinearLayout rootLL;
    TextView totalSongTV;
    ProgressBar progressBar;
    TextView freeSpaceTV;
    TextView usedSpaceTV;
    TextView totalSpaceTV;
    RecyclerView recyclerView;
    TextView selectedSongTV;
    TextView sizeSelectedSongTV;
    Button deleteButton;
    Button cancelButton;

    int selectedSong = 0;
    int sizeSelectedSong = 0;

    long megTotal;
    long megAvailable;
    long megUsed;
    ColorStateList colorSelection, colorText;
    List<JoinSongCountPlayed> tSongSelectedList = new ArrayList<>();

    public SettingDeleteSongFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        root = inflater.inflate(R.layout.fragment_setting_delete_song, container, false);
        try {
            bind();
            setActionObject();
            settingsTheme = new SettingsTheme(getActivity(), this);
            settingsTheme.setTheme();
            SettingActivity.setCallBackDeleteSong(iSettingActivity);
            showData();

        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
        return root;
    }

    void bind() {
        try {
            rootLL = root.findViewById(R.id.rootLL);
            totalSongTV = root.findViewById(R.id.totalSongTV);
            progressBar = root.findViewById(R.id.progressBar);
            freeSpaceTV = root.findViewById(R.id.used_space_tv);
            usedSpaceTV = root.findViewById(R.id.capacitySpaceTV);
            totalSpaceTV = root.findViewById(R.id.total_space_tv);
            recyclerView = root.findViewById(R.id.recyclerView);
            selectedSongTV = root.findViewById(R.id.selected_song_tv);
            sizeSelectedSongTV = root.findViewById(R.id.selected_song_size_tv);
            deleteButton = root.findViewById(R.id.delete_button);
            cancelButton = root.findViewById(R.id.cancel_button);
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    /**
     * kumpulan function untuk onclick, etc
     */
    void setActionObject() {
        try {
            deleteButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showWarningDelete();
                }
            });

            cancelButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    cancelDeleteSong();
                }
            });
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    @Override
    public void setThemeUpdateDate(Date lastUpdateTheme) {

    }

    @Override
    public void setListColorTheme(int color) {
        try {
            rootLL.setBackgroundResource(R.drawable.tags_rounded_corners);
            GradientDrawable categoryDrawable = (GradientDrawable) rootLL.getBackground();
            categoryDrawable.setColor(color);
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    @Override
    public void setTextAndSelectionColorTheme(ColorStateList colorSelection, ColorStateList colorText, int colorTextSelected, int colorTextFocus, int colorTextNormal, int colorSelectionSelected, int colorSelectionFocus) {
        try {
            this.colorSelection = colorSelection;
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }

    }


    /**
     * get size dari usb drive
     */
    void getUSBInfoSize() {
        String TAG = SettingDeleteSongFragment.TAG + "-" + "getUSBInfoSize";
        try {
            String usbPath = "";

            List<StorageUtils.StorageInfo> storageInfoList = StorageUtils.getStorageList();
            for (StorageUtils.StorageInfo storageInfo : storageInfoList) {
                if (storageInfo.removable) {
                    String[] split = storageInfo.path.split("/");
                    usbPath = "/storage/" + split[split.length - 1];
                }
            }
            StatFs statFs = new StatFs(usbPath);
            long blockSize = statFs.getBlockSize();
            long totalSize = statFs.getBlockCount() * blockSize;
            long availableSize = statFs.getAvailableBlocks() * blockSize;
//            long freeSize = statFs.getFreeBlocks() * blockSize;
            long usedSize = totalSize - availableSize;

            long megTotal = totalSize / (1024 * 1024);
            megAvailable = availableSize / (1024 * 1024);
            megUsed = usedSize / (1024 * 1024);
            freeSpaceTV.setText(Util.thousandFormat(megAvailable));
            usedSpaceTV.setText(Util.thousandFormat(megUsed));
            totalSpaceTV.setText(Util.thousandFormat(megTotal));
            progressBar.setMax((int) megTotal);
            progressBar.setProgress((int) megUsed);
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    /**
     * fetch song yang ada di stb
     */
    void fetchSong() {
        PerformAsync2.run(performAsync ->
        {
            List<JoinSongCountPlayed> tSongList = new ArrayList<>();
            try {
                DbSong dbSong = DbSong.Instance.create(getActivity());
                tSongList = dbSong.daoAccessTSong().getAll();
                dbSong.close();
            } catch (Exception ex) {

            }
            return tSongList;
        }).setCallbackResult(result ->
        {
            try {
                tSongList = (List<JoinSongCountPlayed>) result;
                totalSongTV.setText(String.valueOf(tSongList.size()));
                getUSBInfoSize();
                setRecyclerView();
            } catch (Exception ex) {
                Debug.e(TAG, ex);
            }
        });
    }


    void setRecyclerView() {
        try {
            adapter = new RecyclerViewAdapter(new RecyclerViewAdapter.IRecyclerViewAdapter() {
                @Override
                public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
                    View view = LayoutInflater.from(getActivity()).inflate(R.layout.cell_delete_song, parent, false);
                    ItemViewHolder item = new ItemViewHolder(view);
                    return item;
                }

                @Override
                public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
                    try {
                        ItemViewHolder item = (ItemViewHolder) holder;
                        item.bind(tSongList.get(position), position);
                    } catch (Exception ex) {
                        Debug.e(TAG, ex);
                    }
                }

                @Override
                public int getItemCount() {
                    return tSongList.size();
                }
            });

            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
            recyclerView.setLayoutManager(linearLayoutManager);
            adapter.setHasStableIds(true);
            recyclerView.setAdapter(adapter);

        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    //view ini khusus di pakai utk ItemViewHolder, utk selectedItemView
    View selectedView;
    int selectedItem;

    class ItemViewHolder extends RecyclerView.ViewHolder {
        private static final String TAG = "ItemViewHolder";

        View root;
        TextView songNameTV;
        TextView singerTV;
        TextView counterTV;
        TextView sizeTV;
        ImageView checkIV;
        TextView mbTV;
        TextView songTV;

        public ItemViewHolder(View itemView) {
            super(itemView);
            try {
                root = itemView;
                songNameTV = itemView.findViewById(R.id.song_name_tv);
                singerTV = itemView.findViewById(R.id.singer_tv);
                counterTV = itemView.findViewById(R.id.counter_tv);
                sizeTV = itemView.findViewById(R.id.size_tv);
                checkIV = itemView.findViewById(R.id.check_iv);
                mbTV = itemView.findViewById(R.id.mb_tv);
                songTV = itemView.findViewById(R.id.song_tv);

            } catch (Exception ex) {
                Debug.e(TAG, ex);
            }

        }

        public void bind(JoinSongCountPlayed tSong, int pos) {
            try {
                songNameTV.setText(tSong.songName);
                singerTV.setText(tSong.artist);
//                int tempDuration = Integer.valueOf(tSong.duration) * 1000;
                File file = new File(tSong.songURL);
                int file_size = 0;
                if (file.exists()) {
                    file_size = Integer.parseInt(String.valueOf(file.length() / (1024 * 1024)));
                    sizeTV.setText(Util.thousandFormat(file_size));
                }
                String count = String.valueOf(tSong.totalCount);
                counterTV.setText(count);



//                if (colorSelection != null) {
//                    root.setBackgroundTintList(colorSelection);
//                }
////            untuk merubah warna text
//                if (colorText != null) {
//                    songNameTV.setTextColor(colorText);
//                    singerTV.setTextColor(colorText);
//                }

                if (selectedItem == pos) {
//                    selectView(root);
                }

                int finalFile_size = file_size;
                root.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        try {
                            if (checkIV.getVisibility() == View.GONE) {
                                checkIV.setVisibility(View.VISIBLE);
                                selectedSong++;
                                selectedSongTV.setText(String.valueOf(selectedSong));
                                sizeSelectedSong += finalFile_size;
                                sizeSelectedSongTV.setText(Util.thousandFormat(sizeSelectedSong));
                                tSongSelectedList.add(tSong);
//                                nextFocus(pos);
                                //tidak jadi menggunakan selected, karena akan tidak terlihat ketika di focus  item nya
//                            root.setSelected(true);
                            } else {
                                selectedSong--;
                                selectedSongTV.setText(String.valueOf(selectedSong));
                                sizeSelectedSong -= finalFile_size;
                                sizeSelectedSongTV.setText(Util.thousandFormat(sizeSelectedSong));
                                checkIV.setVisibility(View.GONE);
                                List<JoinSongCountPlayed> tempJoinSongCountPlayeds = new ArrayList<>();
                                tempJoinSongCountPlayeds.add(tSong);
                                tSongSelectedList.removeAll(tempJoinSongCountPlayeds);
                            }
                        } catch (Exception ex) {
                            Debug.e(TAG, ex);
                        }
                    }
                });
                root.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                    @Override
                    public void onFocusChange(View v, boolean hasFocus) {
                        if (hasFocus) {
                            songNameTV.setSelected(true);
                            singerTV.setSelected(true);
                            counterTV.setSelected(true);
                            sizeTV.setSelected(true);
                            checkIV.setImageDrawable(getActivity().getDrawable(R.drawable.ic_check_black));
                            mbTV.setSelected(true);
                            songTV.setSelected(true);

                        } else {
                            songNameTV.setSelected(false);
                            singerTV.setSelected(false);
                            counterTV.setSelected(false);
                            sizeTV.setSelected(false);
                            checkIV.setImageDrawable(getActivity().getDrawable(R.drawable.ic_check_white));
                            mbTV.setSelected(false);
                            songTV.setSelected(false);
                        }
                    }
                });

                if(pos==0)
                {
//                    root.requestFocus();
//                    songNameTV.setSelected(true);
//                    singerTV.setSelected(true);
//                    counterTV.setSelected(true);
//                    sizeTV.setSelected(true);
//                    checkIV.setImageDrawable(getActivity().getDrawable(R.drawable.ic_check_black));
//                    mbTV.setSelected(true);
//                    songTV.setSelected(true);
                }
            } catch (Exception ex) {
                Debug.e(TAG, ex);
            }
        }

        void selectView(View v) {
            //clear selection
            /*if (selectedView != null) {
                selectedView.setSelected(false);
            }

            //make selection
            selectedView = v;
            selectedView.setSelected(true);*/
        }

        void nextFocus(int pos) {
            try {
                int nextViewPos = pos + 1;
                if (pos < tSongList.size()) {
                    View nextView = recyclerView.getChildAt(nextViewPos);
                    nextView.requestFocus();
                    adapter.notifyDataSetChanged();
                }
            } catch (Exception ex) {
                Debug.e(TAG, ex);
            }
        }
    }


    SettingActivity.ISettingActivity iSettingActivity = new SettingActivity.ISettingActivity() {
        @Override
        public void onCancelDeleteSong() {
            try {
                Log.d(TAG, "onCancelDeleteSong");
                //kalau ada selected song nya cancel delete song, jika tidak ada finish activity
                if (tSongSelectedList.size() > 0)
                    cancelDeleteSong();
                else {
                    getActivity().finish();
                }
            } catch (Exception ex) {
                Debug.e(TAG, ex);
            }
        }

        @Override
        public void onDeleteSong() {
            try {
                Log.d(TAG, "onDeleteSong");
                showWarningDelete();
            } catch (Exception ex) {
                Debug.e(TAG, ex);
            }
        }
    };

    void showWarningDelete() {
        try {
            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getContext());
            LayoutInflater inflater = this.getLayoutInflater();
            View dialogView = inflater.inflate(R.layout.alert_delete_song_confirmation, null);
            dialogBuilder.setView(dialogView);
            Button okButton = (Button) dialogView.findViewById(R.id.okButton);
            Button cancelButton = (Button) dialogView.findViewById(R.id.cancelButton);
            TextView warningTV = dialogView.findViewById(R.id.warning_tv);
            String totalDeleteMB = Util.thousandFormat(sizeSelectedSong);
            String text = String.format(getString(R.string.song_delete_warning), totalDeleteMB);
            warningTV.setText(text);
            AlertDialog alertDialog = dialogBuilder.create();

            okButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    alertDialog.dismiss();
                    deleteSong(tSongSelectedList);
                }
            });

            cancelButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    alertDialog.dismiss();

                }
            });

            alertDialog.show();
            WindowManager.LayoutParams layoutParams = alertDialog.getWindow().getAttributes();
            layoutParams.width = ViewGroup.LayoutParams.WRAP_CONTENT;
            alertDialog.show();
            alertDialog.getWindow().setLayout(350, WindowManager.LayoutParams.WRAP_CONTENT);
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    /**
     * untuk delete song yang sudah di pilih
     */
    void deleteSong(List<JoinSongCountPlayed> joinSongCountPlayedList) {
        try {

            String songId = "";
            for (JoinSongCountPlayed joinSongCountPlayed : joinSongCountPlayedList) {
                if (songId.equals(""))
                    songId += "'" + joinSongCountPlayed.songId + "'";
                else
                    songId += ",'" + joinSongCountPlayed.songId + "'";

                File thumbnailFile = new File(joinSongCountPlayed.thumbnail);
                if (thumbnailFile.exists())
                    RootUtil.deleteFile(joinSongCountPlayed.thumbnail);
                File songFile = new File(joinSongCountPlayed.songURL);
                if (songFile.exists())
                    RootUtil.deleteFile(joinSongCountPlayed.songURL);
            }
            deleteSongFromRoom(joinSongCountPlayedList);

        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    void deleteSongFromRoom(List<JoinSongCountPlayed> joinSongCountPlayedList) {
        PerformAsync2.run(performAsync ->
        {
            int result = 0;
            try {
                DbSong dbSong = DbSong.Instance.create(getContext());
                for (JoinSongCountPlayed joinSongCountPlayed : joinSongCountPlayedList) {
                    dbSong.daoAccessTSong().delete(joinSongCountPlayed.songId);
                }
                dbSong.close();
                result = 1;
            } catch (Exception ex) {
                Debug.e(TAG, ex);
            }
            return result;
        }).setCallbackResult(result ->
        {
            int r = (int) result;
            if (r == 1) {
                deleteSongPlaylistFromRoom(joinSongCountPlayedList);
            }
        });
    }

    void deleteSongPlaylistFromRoom(List<JoinSongCountPlayed> joinSongCountPlayedList) {
        PerformAsync2.run(performAsync ->
        {
            int result = 0;
            try {
                DbSong dbSong = DbSong.Instance.create(getContext());
                for (JoinSongCountPlayed joinSongCountPlayed : joinSongCountPlayedList) {
                    dbSong.daoAccessTSongPlaylist().deleteBySongId(joinSongCountPlayed.songId);
                }
                dbSong.close();
                result = 1;
            } catch (Exception ex) {
                Debug.e(TAG, ex);
            }
            return result;
        }).setCallbackResult(result ->
        {
            int r = (int) result;
            if (r == 1) {
                writeSongFileToJSON();
            }
        });
    }


    void writeSongFileToJSON() {
        PerformAsync2.run(performAsync ->
        {
            List<TSong> tSongList = new ArrayList<>();
            try {
                DbSong dbSong = DbSong.Instance.create(getContext());
                tSongList = dbSong.daoAccessTSong().getAllTSong();
                dbSong.close();
            } catch (Exception ex) {
                Debug.e(TAG, ex);
            }

            return tSongList;

        }).setCallbackResult(result ->
        {
            try {
                Gson gson = new Gson();
                List<TSong> newTSongList = new ArrayList<>();
                List<TSong> tSongList = (List<TSong>) result;
                for (TSong tSong : tSongList) {
                    String[] fileNameSongArray = tSong.songURL.split("/");
                    String fileNameSong = fileNameSongArray[fileNameSongArray.length - 1];

                    String[] fileNameThumbnailArray = tSong.thumbnail.split("/");
                    String fileNameThumbnail = fileNameThumbnailArray[fileNameThumbnailArray.length - 1];

                    tSong.songURL = fileNameSong;
                    tSong.thumbnail = fileNameThumbnail;
                    newTSongList.add(tSong);
                }

                ModelSongLocal.ResultGetSongList resultGetSongList = new ModelSongLocal.ResultGetSongList();
                resultGetSongList.count = newTSongList.size();
                resultGetSongList.list = newTSongList;

                String songString = gson.toJson(resultGetSongList);
                String pathSTB = Global.getUSBPath() + "/" + C.PATH_JSON_KARAOKE;

                //write song
                String mac = Util.getMACAddress("eth0");
//            mac = mac.replace(":", "");
                String usbSongPath = pathSTB + "/" + C.JSON_SONG;
                String tempSongPath = getContext().getFilesDir() + "/" + C.JSON_SONG;
                boolean resultWriteYoutube = Util.writeJsonToFile(tempSongPath, songString);
                String encQuotaPath = Util.encryptFile(tempSongPath, mac);
                if (encQuotaPath != null) {
                    if (resultWriteYoutube)
                        RootUtil.moveFile(encQuotaPath, usbSongPath);

                    C.FG_UPDATE_SONG = false;
                }

                Toasty.info(getContext(), getString(R.string.song_deleted)).show();
//                getActivity().finish();
                showData();
            } catch (Exception ex) {
                Debug.e(TAG, ex);
            }
        });
    }


    /**
     * clear arraylist yang ingin di delete, dan reload recyclerview
     */
    void cancelDeleteSong() {
        try {
            tSongSelectedList.clear();
            fetchSong();
            selectedSong = 0;
            sizeSelectedSong = 0;
            selectedSongTV.setText(String.valueOf(selectedSong));
            sizeSelectedSongTV.setText(Util.thousandFormat(sizeSelectedSong));
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    void showData()
    {
        fetchSong();
        tSongSelectedList.clear();
        selectedSong = 0;
        sizeSelectedSong = 0;
        selectedSongTV.setText(String.valueOf(selectedSong));
        sizeSelectedSongTV.setText(Util.thousandFormat(sizeSelectedSong));
    }


}

package com.madeira.entertainz.karaoke.model_room_db;

import android.content.Context;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.madeira.entertainz.controller.USBControl;
import com.madeira.entertainz.karaoke.DBLocal.DbAccess;
import com.madeira.entertainz.karaoke.DBLocal.DbDownloadYoutube;
import com.madeira.entertainz.karaoke.DBLocal.DbRecentSearchSong;
import com.madeira.entertainz.karaoke.DBLocal.DbSong;
import com.madeira.entertainz.karaoke.DBLocal.JoinSongCountCategory;
import com.madeira.entertainz.karaoke.DBLocal.JoinSongCountPlayed;
import com.madeira.entertainz.karaoke.DBLocal.TDownloadYoutube;
import com.madeira.entertainz.karaoke.DBLocal.TDownloadYoutubeDt;
import com.madeira.entertainz.karaoke.DBLocal.TLogSong;
import com.madeira.entertainz.karaoke.DBLocal.TRecentSearchSong;
import com.madeira.entertainz.karaoke.DBLocal.TSong;
import com.madeira.entertainz.karaoke.DBLocal.TSongCategory;
import com.madeira.entertainz.karaoke.DBLocal.TSongItemServer;
import com.madeira.entertainz.karaoke.Debug;
import com.madeira.entertainz.karaoke.MainApplication;
import com.madeira.entertainz.karaoke.config.C;
import com.madeira.entertainz.library.Async;
import com.madeira.entertainz.library.PerformAsync2;
import com.madeira.entertainz.library.RootUtil;
import com.madeira.entertainz.library.Util;
import com.madeira.eznet.lib.HelperGson;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class ModelDownloadYoutube {
    static String TAG = "ModelDownloadYoutube";


    /**
     * untuk get Youtube download BY ID
     */
    public interface IGetListDownload {
        void onGetListDownload(List<TDownloadYoutube> tDownloadYoutubes);
    }

    public static void getListDownload(Context context, IGetListDownload callback) {
        PerformAsync2.run(performAsync ->
        {
            List<TDownloadYoutube> result = new ArrayList<>();
            try {
                DbDownloadYoutube dbDownloadYoutube = DbDownloadYoutube.Instance.create(context);
                result = dbDownloadYoutube.daoDownloadYoutube().getAll();
                dbDownloadYoutube.close();
            } catch (Exception ex) {
                Debug.e(TAG, ex);
            }
            return result;
        }).
                setCallbackResult(result ->

                {
                    try {
                        List<TDownloadYoutube> tDownloadYoutubeDt = (List<TDownloadYoutube>) result;
                        callback.onGetListDownload(tDownloadYoutubeDt);
                    } catch (Exception ex) {
                        Debug.e(TAG, ex);
                    }

                });
    }
    /**
     * untuk get Youtube download BY ID
     */
    public interface IGetListDownloadById {
        void onGetListDownloadById(List<TDownloadYoutubeDt> tDownloadYoutubeDtList);
    }

    public static void getListDownloadById(Context context, String songId, IGetListDownloadById callback) {
        PerformAsync2.run(performAsync ->
        {
            List<TDownloadYoutubeDt> result = new ArrayList<>();
            try {
                DbDownloadYoutube dbDownloadYoutube = DbDownloadYoutube.Instance.create(context);
                result = dbDownloadYoutube.daoDownloadYoutubeDt().getDownloadYoutubeDtById(songId);
                dbDownloadYoutube.close();
            } catch (Exception ex) {
                Debug.e(TAG, ex);
            }
            return result;
        }).
                setCallbackResult(result ->

                {
                    try {
                        List<TDownloadYoutubeDt> tDownloadYoutubeDt = (List<TDownloadYoutubeDt>) result;
                        callback.onGetListDownloadById(tDownloadYoutubeDt);
                    } catch (Exception ex) {
                        Debug.e(TAG, ex);
                    }

                });
    }




}

package com.madeira.entertainz.karaoke;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.widget.ImageView;

import com.madeira.entertainz.karaoke.DBLocal.JoinSettingElementMedia;
import com.madeira.entertainz.karaoke.model_room_db.ModelElementMedia;
import com.madeira.entertainz.library.Util;

import java.io.File;
import java.io.InputStream;
import java.util.List;

import static com.madeira.entertainz.karaoke.config.CacheData.lastIndexBackground;

public class EKMAConnectedActivity extends AppCompatActivity {

    String TAG = "EKMAConnectedActivity";
    ImageView backgroundIV;
    Handler handler = new Handler();
    Runnable runnable;

    public static void startActivity(Context activity) {
        Intent intent = new Intent(activity, EKMAConnectedActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        activity.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            setContentView(R.layout.activity_ekmaconnected);
            Util.hideNavigationBar(EKMAConnectedActivity.this);
            bind();
            fetchListBackground();

        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }

    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {

        return false;
//        return super.onKeyDown(keyCode, event);
    }

    void bind() {
        try {
            backgroundIV = (ImageView) findViewById(R.id.backgroundIV);
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }


    //todo untuk fetch list image background
    void fetchListBackground() {
        ModelElementMedia.getListElementMediaActive(this, new ModelElementMedia.IGetListElementMediaActive() {
            @Override
            public void onGetListElementMedia(List<JoinSettingElementMedia> joinSettingElementMediaList) {
                int totalIndex = joinSettingElementMediaList.size();
                runnable = new Runnable() {
                    @Override
                    public void run() {
                        try {
                            if (joinSettingElementMediaList.size() != 0) {
                                if (lastIndexBackground >= totalIndex)
                                    lastIndexBackground = 0;

                                JoinSettingElementMedia joinSettingElementMedia = joinSettingElementMediaList.get(lastIndexBackground);
                                Uri uri = Uri.fromFile(new File(joinSettingElementMedia.urlImage));
//                            Picasso.with(KaraokeActivity.this).load(uri).into(backgroundIV);
                                InputStream inputStream = getContentResolver().openInputStream(uri);
                                BitmapFactory.Options options = new BitmapFactory.Options();
                                options.inPreferredConfig = Bitmap.Config.RGB_565;
                                Bitmap image = BitmapFactory.decodeStream(inputStream, null, options);
                                backgroundIV.setImageBitmap(image);

                                lastIndexBackground++;
                                handler.postDelayed(this::run, joinSettingElementMedia.duration);
                            }
                        } catch (Exception ex) {
                            Debug.e(TAG, ex);
                        }
                    }
                };
                handler.postDelayed(runnable, 100);
            }
        });
    }

}

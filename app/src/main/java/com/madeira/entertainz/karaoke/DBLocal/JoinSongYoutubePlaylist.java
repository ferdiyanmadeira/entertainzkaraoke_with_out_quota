package com.madeira.entertainz.karaoke.DBLocal;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;

import java.util.List;

@Entity(tableName = "TSongYoutubePlaylist")
public class JoinSongYoutubePlaylist {

    @PrimaryKey(autoGenerate = true)
    @SerializedName("songPlaylistId")
    public int songPlaylistId;
    @SerializedName("playlistId")
    public int playlistId;
    @SerializedName("songId")
    public String songId;
    @SerializedName("songName")
    public String songName;
    @SerializedName("artist")
    public String artist;
    @SerializedName("songURL")
    public String songURL;
    @SerializedName("vocalSound")
    public int vocalSound;
    @SerializedName("duration")
    public int duration;
    @SerializedName("thumbnail")
    public String thumbnail;
    public String lyric;

    public static int getIndexBySongId(List<JoinSongYoutubePlaylist> list, int songId){
        //set default -1; jika index tidak ditemukan
        int index =-1;
        JoinSongYoutubePlaylist item = new JoinSongYoutubePlaylist();

        for(JoinSongYoutubePlaylist tSongYoutubePlaylist : list)
        {
            if(tSongYoutubePlaylist.songPlaylistId== songId)
            {
                index = list.indexOf(tSongYoutubePlaylist);
                break;
            }
        }
        return index;
    }
}

package com.madeira.entertainz.karaoke.DBLocal;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import com.google.gson.annotations.SerializedName;

@Entity(tableName = "tsetting_element_media")
public class TSettingElementMedia {
    private static final String TAG = "TElementMedia";

    @PrimaryKey
    @NonNull
    @SerializedName("element_media_id")
    public int elementMediaId;

    @SerializedName("duration")
    public int duration;

    @SerializedName("isActive")
    public int isActive;
}
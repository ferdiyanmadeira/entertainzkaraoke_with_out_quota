package com.madeira.entertainz.karaoke.model_online;

import android.net.Uri;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.OkHttpResponseAndJSONObjectRequestListener;
import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;
import com.madeira.entertainz.karaoke.Debug;
import com.madeira.entertainz.karaoke.config.Global;
import com.madeira.entertainz.library.Util;

import org.json.JSONObject;

import java.io.Serializable;
import java.util.List;

import okhttp3.Response;

import static com.madeira.entertainz.karaoke.config.C.DEFAULT_API;

public class ModelPurchase {
    private static final String TAG = "ModelPurchase";

    private static final String APIHOST = Global.getApiHostname() + "/v1/purchase.php";

    public interface IDoPurchase {
        void onDoPurchase(PurchaseItem result);

        void onError(ANError e);
    }

    static public void doPurchase(String quotaId, String email, String name, IDoPurchase callBack) {
        try {
            String mac = Util.getMACAddress("eth0");
            mac = mac.replace(":", "");
            String concat = mac + quotaId;
            String hash = Util.stringToSHA256(concat);
            String uri = Uri.parse(APIHOST)
                    .buildUpon()
                    .appendQueryParameter("action", "purchase")
                    .appendQueryParameter("mac", mac)
                    .appendQueryParameter("quotaId", quotaId)
                    .appendQueryParameter("hash", hash)
                    .appendQueryParameter("email", email)
                    .appendQueryParameter("name", name)
                    .build().toString();

            AndroidNetworking.get(uri)
                    .setPriority(Priority.MEDIUM)
                    .build()
                    .getAsOkHttpResponseAndJSONObject(new OkHttpResponseAndJSONObjectRequestListener() {
                        @Override
                        public void onResponse(Response okHttpResponse, JSONObject response) {
                            try {
                                if (okHttpResponse.isSuccessful()) {
                                    if (response.has("errCode")) {
                                        int errorCode = response.getInt("errCode");
                                        String errorMessage = response.getString("errMsg");
                                        ANError anError = new ANError();
                                        anError.setErrorCode(errorCode);
                                        anError.setErrorBody(errorMessage);
                                        anError.setErrorDetail(errorMessage);
                                        callBack.onError(anError);
                                        return;
                                    }
                                    Gson gson = new Gson();
                                    String jsonString = response.toString();
                                    PurchaseItem item = gson.fromJson(jsonString, PurchaseItem.class);
                                    callBack.onDoPurchase(item);
                                }
                            } catch (Exception ex) {
                                ex.printStackTrace();
                            }
                        }

                        @Override
                        public void onError(ANError anError) {
                            callBack.onError(anError);
                        }
                    });
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }


    public interface IGetHistoryPurchase {
        void onGetHistoryPurchase(PurchaseHistory result);

        void onError(ANError e);
    }

    static public void getHistoryPurchase( IGetHistoryPurchase callBack) {
        try {
            String mac = Util.getMACAddress("eth0");
            mac = mac.replace(":", "");
            String uri = Uri.parse(APIHOST)
                    .buildUpon()
                    .appendQueryParameter("action", "get_history")
                    .appendQueryParameter("mac", mac)
                    .build().toString();

            AndroidNetworking.get(uri)
                    .setPriority(Priority.MEDIUM)
                    .build()
                    .getAsOkHttpResponseAndJSONObject(new OkHttpResponseAndJSONObjectRequestListener() {
                        @Override
                        public void onResponse(Response okHttpResponse, JSONObject response) {
                            try {
                                if (okHttpResponse.isSuccessful()) {
                                    if (response.has("errCode")) {
                                        int errorCode = response.getInt("errCode");
                                        String errorMessage = response.getString("errMsg");
                                        ANError anError = new ANError();
                                        anError.setErrorCode(errorCode);
                                        anError.setErrorBody(errorMessage);
                                        anError.setErrorDetail(errorMessage);
                                        callBack.onError(anError);
                                        return;
                                    }
                                    Gson gson = new Gson();
                                    String jsonString = response.toString();
                                    PurchaseHistory item = gson.fromJson(jsonString, PurchaseHistory.class);
                                    callBack.onGetHistoryPurchase(item);
                                }
                            } catch (Exception ex) {
                                ex.printStackTrace();
                            }
                        }

                        @Override
                        public void onError(ANError anError) {
                            callBack.onError(anError);
                        }
                    });
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }


    public class PurchaseItem implements Serializable {
        //        @SerializedName("version_code")
//        public String versionCode;
//
        @SerializedName("url")
        public String url;
        @SerializedName("mall_id")
        public String mallId;
        @SerializedName("shared_key")
        public String sharedKey;
        @SerializedName("words")
        public String words;
        @SerializedName("trans_id")
        public String transId;
        @SerializedName("request_date")
        public String requestDate;
        @SerializedName("price_quota")
        public ModelPriceQuota.PriceQuotaItem priceQuota;


    }

    public class PurchaseHistory implements Serializable
    {
        @SerializedName("list")
        public List <PurchaseHistoryItem> list;
    }

    public class PurchaseHistoryItem implements Serializable {

        @SerializedName("trans_id")
        public String transId;
        @SerializedName("quota_id")
        public String quotaId;
        @SerializedName("price")
        public String price;
        @SerializedName("quota")
        public String quota;
        @SerializedName("currency")
        public String currency;
        @SerializedName("status")
        public String status;
        @SerializedName("create_date")
        public String date;
    }
}

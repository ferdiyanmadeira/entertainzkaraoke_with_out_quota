package com.madeira.entertainz.karaoke.DBLocal;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import com.google.gson.annotations.SerializedName;

@Entity(tableName = "telement_media")
public class TElementMedia {
    private static final String TAG = "TElementMedia";

    @PrimaryKey
    @NonNull
    @SerializedName("element_media_id")
    public int elementMediaId;

    @NonNull
    @SerializedName("element_id")
    public int elementId;

    @SerializedName("url_image")
    public String urlImage;

    @SerializedName("duration")
    public int duration;
}
package com.madeira.entertainz.karaoke;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.madeira.entertainz.library.Util;

public class UpgradeReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        // launch your fav activity from here.
        String packageName = context.getPackageName();
        Util.launchApp(packageName);

    }
}
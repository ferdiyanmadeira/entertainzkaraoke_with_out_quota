package com.madeira.entertainz.karaoke.menu_live_channel;


import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.androidnetworking.error.ANError;
import com.madeira.entertainz.helper.RecyclerViewAdapter;
import com.madeira.entertainz.karaoke.Debug;
import com.madeira.entertainz.karaoke.R;
import com.madeira.entertainz.karaoke.config.C;
import com.madeira.entertainz.karaoke.config.CacheData;
import com.madeira.entertainz.karaoke.model_online.ModelChannel;
import com.madeira.entertainz.karaoke.model_online.ModelRadio;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * fragment yang dibawah sebelah kiri
 */
public class LiveChannelSubListRadioFragment extends Fragment implements LiveChannelTheme.ILiveChannelTheme {
    String TAG = "LiveChannelSubListFragment";

    public static final String KEY_FLAG_TV = "KEY_FLAG_TV";

    int lastPosition;
    //    boolean flagIncrease = false;
    int liveChannelId;

    View root;
    LinearLayout recyclerLL;
    RecyclerView recyclerView;
    RecyclerViewAdapter adapter;

    private ISubCategoryListListener mListener;
    LiveChannelTheme radioTheme;
    //    List<TRadio> tRadioList = new ArrayList<>();
    List<ModelRadio.subRadioItem> subRadioItemList = new ArrayList<>();
    List<ModelChannel.subChannelItem> subChannelItemList = new ArrayList<>();

    public LiveChannelSubListRadioFragment() {
        // Required empty public constructor
    }

    public static LiveChannelSubListRadioFragment newInstance(String param1, String param2) {
        LiveChannelSubListRadioFragment fragment = new LiveChannelSubListRadioFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            liveChannelId = getArguments().getInt(KEY_FLAG_TV);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        root = inflater.inflate(R.layout.fragment_live_channel_sub_list_radio, container, false);
        try {
            bind();
            radioTheme = new LiveChannelTheme(getActivity(), this);
            radioTheme.setTheme();
            //todo show sub category berdasarkan category
            switch (liveChannelId) {
                case C.MENU_LIVE_CHANNEL_RADIO:
                    fetchRadio();
                    break;
                case C.MENU_LIVE_CHANNEL_TV:
                    fetchChannel();
                    break;
            }
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
        return root;
    }


    @Override
    public void setThemeUpdateDate(Date lastUpdateTheme) {

    }

    @Override
    public void setListColorTheme(int color) {
        try {
            recyclerLL.setBackgroundResource(R.drawable.tags_rounded_corners);

            GradientDrawable recycleDrawable = (GradientDrawable) recyclerLL.getBackground();
            recycleDrawable.setColor(color);
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    @Override
    public void setTextAndSelectionColorTheme(ColorStateList colorSelection, ColorStateList colorText, int colorTextSelected, int colorTextFocus, int colorTextNormal, int colorSelectionSelected, int colorSelectionFocus) {
        try {
            this.colorSelection = colorSelection;
            this.colorText = colorText;
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    public interface ISubCategoryListListener {
        // TODO: url digunakan hanya untuk live tv, id digunakan untuk radio, livechannel adalah id menu radio atau id menu live tv
        void onRadioCategoryClick(int liveChannel, ModelRadio.subRadioItem tRadio, String url);

        void onChannelCategoryClick(int liveChannel, ModelChannel.subChannelItem channelItem, String url);

        void onFullScreen();


    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
       /* if (context instanceof IRadioListListener) {
            mListener = (IRadioListListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }*/
    }

    @Override
    public void onDetach() {
        super.onDetach();
        try {
            mListener = null;
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }

    }

    void bind() {
        recyclerLL = (LinearLayout) root.findViewById(R.id.recyclerLL);
        recyclerView = (RecyclerView) root.findViewById(R.id.recyclerView);
    }

    void fetchRadio() {
        try {
            //todo clear data sebelum di set
            subRadioItemList.clear();
            if (adapter != null)
                adapter.notifyDataSetChanged();


            ModelRadio.getRadio(new ModelRadio.IGetRadio() {
                @Override
                public void onGetRadio(ModelRadio.radioItem result) {
                    subRadioItemList = result.list;
                    setRadioRecyclerView();
                }

                @Override
                public void onError(ANError e) {

                }
            });

        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    void fetchChannel() {
        try {
            subRadioItemList.clear();
            if (adapter != null)
                adapter.notifyDataSetChanged();

            ModelChannel.getChannel(new ModelChannel.IGetChannel() {
                @Override
                public void onGetSTBInfo(ModelChannel.channelItem result) {
//                    subRadioItemList = result.list;
                    subChannelItemList = result.list;
                    //untuk menyimpan di cache data
                    CacheData.channelItemList = result.list;
                    setChannelRecyclerView();
                }

                @Override
                public void onError(ANError e) {

                }
            });


        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    void setRadioRecyclerView() {
        try {
            adapter = new RecyclerViewAdapter(new RecyclerViewAdapter.IRecyclerViewAdapter() {
                @Override
                public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
                    View view = LayoutInflater.from(getActivity()).inflate(R.layout.cell_radio_list, parent, false);
                    ItemViewHolderRadio item = new ItemViewHolderRadio(view);
                    return item;
                }

                @Override
                public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
                    try {
                        ItemViewHolderRadio item = (ItemViewHolderRadio) holder;
                        item.bind(subRadioItemList.get(position), position);
                    } catch (Exception ex) {
                        Debug.e(TAG, ex);
                    }
                }

                @Override
                public int getItemCount() {
                    return subRadioItemList.size();
                }
            });
            recyclerView.setAdapter(adapter);
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }


    void setChannelRecyclerView() {
        try {
            adapter = new RecyclerViewAdapter(new RecyclerViewAdapter.IRecyclerViewAdapter() {
                @Override
                public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
                    View view = LayoutInflater.from(getActivity()).inflate(R.layout.cell_radio_list, parent, false);
                    ItemViewHolderChannel item = new ItemViewHolderChannel(view);
                    return item;
                }

                @Override
                public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
                    try {
                        ItemViewHolderChannel item = (ItemViewHolderChannel) holder;
                        item.bind(subChannelItemList.get(position), position);
                    } catch (Exception ex) {
                        Debug.e(TAG, ex);
                    }
                }

                @Override
                public int getItemCount() {
                    return subChannelItemList.size();
                }
            });
            recyclerView.setAdapter(adapter);
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    //view ini khusus di pakai utk ItemViewHolder, utk selectedItemView
    View selectedView;
    int selectedItem;

    class ItemViewHolderRadio extends RecyclerView.ViewHolder {
        private static final String TAG = "ItemViewHolderRadio";

        View root;
        TextView tvTitle;

        public ItemViewHolderRadio(View itemView) {
            super(itemView);
            root = itemView;
            tvTitle = itemView.findViewById(R.id.text_title);
        }

        public void bind(ModelRadio.subRadioItem tRadio, int pos) {
            try {
                tvTitle.setText(tRadio.radioName);
                if (selectedItem == pos) {
                    selectView(root);
                }
//            untuk merubah warna selection
                if (colorSelection != null) root.setBackgroundTintList(colorSelection);
//            untuk merubah warna text
                if (colorText != null) tvTitle.setTextColor(colorText);
                if (pos == 0) {
                    root.setSelected(true);
                    selectedItem = pos;
                    mListener.onRadioCategoryClick(liveChannelId, tRadio, tRadio.urlRadio);

                }

                root.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        selectView(v);
                        selectedItem = pos;
                        mListener.onRadioCategoryClick(liveChannelId, tRadio, tRadio.urlRadio);
                    }
                });
            } catch (Exception ex) {
                Debug.e(TAG, ex);
            }
        }

        void selectView(View v) {
            //clear selection
            if (selectedView != null) {
                selectedView.setSelected(false);
            }

            //make selection
            selectedView = v;
            selectedView.setSelected(true);
        }
    }

    class ItemViewHolderChannel extends RecyclerView.ViewHolder {
        private static final String TAG = "ItemViewHolderChannel";

        View root;
        TextView tvTitle;

        public ItemViewHolderChannel(View itemView) {
            super(itemView);
            root = itemView;
            tvTitle = itemView.findViewById(R.id.text_title);
        }

        public void bind(ModelChannel.subChannelItem channelItem, int pos) {
            try {
                tvTitle.setText(channelItem.channelName);
                if (selectedItem == pos) {
                    selectView(root);
                }
//            untuk merubah warna selection
                if (colorSelection != null) root.setBackgroundTintList(colorSelection);
//            untuk merubah warna text
                if (colorText != null) tvTitle.setTextColor(colorText);

                if (selectedItem == pos) {
                    root.requestFocus();
                }
                if (pos == 0) {
                    root.setSelected(true);
                    selectedItem = pos;
                    mListener.onChannelCategoryClick(liveChannelId, channelItem, channelItem.urlChannel);

                }

                root.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                    @Override
                    public void onFocusChange(View v, boolean hasFocus) {
                        if (hasFocus) {
                            selectView(v);
                            selectedItem = pos;
                            mListener.onChannelCategoryClick(liveChannelId, channelItem, channelItem.urlChannel);
                        }
                    }
                });

                root.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                      /*  selectView(v);
                        selectedItem = pos;
                        mListener.onChannelCategoryClick(liveChannelId, channelItem, channelItem.urlChannel);*/
                        mListener.onFullScreen();
                    }
                });
            } catch (Exception ex) {
                Debug.e(TAG, ex);
            }
        }

        void selectView(View v) {
            //clear selection
            if (selectedView != null) {
                selectedView.setSelected(false);
            }


            //make selection
            selectedView = v;
            selectedView.setSelected(true);
        }
    }

    ColorStateList colorSelection, colorText;

    public void setCallback(ISubCategoryListListener listener) {
        this.mListener = listener;
    }

}

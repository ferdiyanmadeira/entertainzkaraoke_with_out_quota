package com.madeira.entertainz.karaoke.menu_accessories;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.Handler;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;

import com.madeira.entertainz.karaoke.DBLocal.JoinSettingElementMedia;
import com.madeira.entertainz.karaoke.Debug;
import com.madeira.entertainz.karaoke.R;
import com.madeira.entertainz.karaoke.config.C;
import com.madeira.entertainz.karaoke.config.Global;
import com.madeira.entertainz.karaoke.menu_navigation.TopNavigationFragment;
import com.madeira.entertainz.karaoke.model_room_db.ModelElementMedia;
import com.madeira.entertainz.library.Util;

import java.io.File;
import java.io.InputStream;
import java.util.List;

import static com.madeira.entertainz.karaoke.config.CacheData.lastIndexBackground;

public class AccessoriesActivity extends AppCompatActivity {
    static String TAG = "AccessoriesActivity";
    ImageView backgroundIV;
    WebView webView;

    Handler handler = new Handler();
    Runnable runnable;
    TopNavigationFragment topNavigationFragment = new TopNavigationFragment();

    public static void startActivity(Activity activity) {
        try {
            Intent intent = new Intent(activity, AccessoriesActivity.class);
            activity.startActivity(intent);
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        try {
            super.onCreate(savedInstanceState);

            setContentView(R.layout.activity_accessories);
            bind();
            bindFragment();
            fetchListBackground();
            Util.hideNavigationBar(this);

            String languageId = Global.getLanguageId();
            Util.setLanguage(this, languageId);
            setWebView();
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    void bind() {
        try {
            backgroundIV = (ImageView) findViewById(R.id.backgroundIV);
            webView = findViewById(R.id.webView);
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    void bindFragment() {
//        topNavigationFragment = topNavigationFragment.newInstance(true);
        try {
            topNavigationFragment = topNavigationFragment.newInstance(C.MENU_ACCESSORIES);
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.topNavFrameLayout, topNavigationFragment);
            ft.commit();
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    void fetchListBackground() {
        ModelElementMedia.getListElementMediaActive(this, new ModelElementMedia.IGetListElementMediaActive() {
            @Override
            public void onGetListElementMedia(List<JoinSettingElementMedia> joinSettingElementMediaList) {
                int totalIndex = joinSettingElementMediaList.size();
                runnable = new Runnable() {
                    @Override
                    public void run() {
                        try {
                            if (joinSettingElementMediaList.size() != 0) {
                                if (lastIndexBackground >= totalIndex)
                                    lastIndexBackground = 0;

                                JoinSettingElementMedia joinSettingElementMedia = joinSettingElementMediaList.get(lastIndexBackground);
                                Uri uri = Uri.fromFile(new File(joinSettingElementMedia.urlImage));
//                            Picasso.with(KaraokeActivity.this).load(uri).into(backgroundIV);
                                InputStream inputStream = getContentResolver().openInputStream(uri);
                                BitmapFactory.Options options = new BitmapFactory.Options();
                                options.inPreferredConfig = Bitmap.Config.RGB_565;
                                Bitmap image = BitmapFactory.decodeStream(inputStream, null, options);
                                backgroundIV.setImageBitmap(image);

                                lastIndexBackground++;
                                handler.postDelayed(this::run, joinSettingElementMedia.duration);
                            }
                        } catch (Exception ex) {
                            Debug.e(TAG, ex);
                        }
                    }
                };
                handler.postDelayed(runnable, 100);
            }
        });
    }

    @Override
    public void onBackPressed() {
        try {
            super.onBackPressed();
            //agar runable tidak running ketika di finish activity
            handler.removeCallbacks(runnable);
            handler.removeCallbacksAndMessages(null);
            finish();
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    @Override
    public void onResume() {
        try {
            super.onResume();
            Util.hideNavigationBar(this);
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    void setWebView() {
        try {
            webView.setWebChromeClient(new WebChromeClient());
            webView.setWebViewClient(new WebViewClient());
            webView.clearCache(true);
            webView.clearHistory();
            webView.getSettings().setJavaScriptEnabled(true);
            webView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);


            //TODO hit api doku
            webView.loadUrl(C.ESHOPING_URL);
            webView.setBackgroundColor(Color.parseColor("#919191"));
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    @Override
    public void onDestroy()
    {
        try
        {
            super.onDestroy();
            Util.freeMemory();
        }
        catch (Exception ex)
        {
            Debug.e(TAG, ex);
        }
    }
}

package com.madeira.entertainz.karaoke.DBLocal;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.Nullable;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class JoinSongPlaylist implements Serializable {

    @PrimaryKey(autoGenerate = true)
    @SerializedName("songPlaylistId")
    public int songPlaylistId;
    @SerializedName("playlistId")
    public int playlistId;
    @SerializedName("songId")
    public String songId;
    @SerializedName("songName")
    public String songName;
    @SerializedName("artist")
    public String artist;
    @SerializedName("songURL")
    public String songURL;
    @SerializedName("vocalSound")
    public int vocalSound;
    @SerializedName("duration")
    public int duration;
    @SerializedName("thumbnail")
    public String thumbnail;
    @SerializedName("iv")
    public String iv;
    @SerializedName("key")
    public String key;
    @SerializedName("lyric")
    public String lyric;

    @Override
    public boolean equals(@Nullable Object obj) {
        JoinSongPlaylist item = (JoinSongPlaylist) obj;

        return item.songId==songId;
    }

    /**
     * return index dari item berdasarkan songId
     *
     * @param list
     * @param songId
     * @return
     */
    public static int getIndexBySongId(List<JoinSongPlaylist> list, String songId){

        //set default -1; jika index tidak ditemukan
        int index =-1;
        JoinSongPlaylist item = new JoinSongPlaylist();

        for(JoinSongPlaylist tSongPlaylist : list)
        {
            if(tSongPlaylist.songId.equals(songId))
            {
                index = list.indexOf(tSongPlaylist);
                break;
            }
        }
        return index;
       /* TSongPlaylist item = new TSongPlaylist();
        item.songId = songId;
        return list.indexOf(item);*/
    }
}

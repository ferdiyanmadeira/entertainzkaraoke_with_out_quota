package com.madeira.entertainz.karaoke.menu_news;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;

import com.madeira.entertainz.karaoke.BaseActivity;
import com.madeira.entertainz.karaoke.R;
import com.madeira.entertainz.karaoke.player.YoutubePlayerActivity;
import com.madeira.entertainz.library.Util;
import com.pierfrancescosoffritti.youtubeplayer.player.AbstractYouTubePlayerListener;
import com.pierfrancescosoffritti.youtubeplayer.player.PlayerConstants;
import com.pierfrancescosoffritti.youtubeplayer.player.YouTubePlayer;
import com.pierfrancescosoffritti.youtubeplayer.player.YouTubePlayerInitListener;
import com.pierfrancescosoffritti.youtubeplayer.player.YouTubePlayerListener;
import com.pierfrancescosoffritti.youtubeplayer.player.YouTubePlayerView;
import com.pierfrancescosoffritti.youtubeplayer.ui.DefaultPlayerUIController;
import com.pierfrancescosoffritti.youtubeplayer.ui.PlayerUIController;

import java.net.MalformedURLException;

public class PlayerYoutubeActivity extends BaseActivity {
    private static final String TAG = "PlayerYoutubeActivity";

    private static final String INTENT_URL = "INTENT_URL"; //youtube video url

    YouTubePlayerView youTubePlayerView;
    private YouTubePlayer youtubePlayer;

    PlayerUIController uiController;

    String videoId;

    int playerState;

    public static void startActivity(Context context, String url){

        Intent intent;

        //exit apabila url kosong
        if (url==null) return;
        if (url.length()==0) return;

        intent = new Intent(context, PlayerYoutubeActivity.class);

        intent.putExtra(INTENT_URL, url);

        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Util.hideNavigationBar(this);

        try {
            videoId = Util.extractYoutubeId(getIntent().getStringExtra(INTENT_URL));
        } catch (MalformedURLException e) {
            e.printStackTrace();
            videoId = "";
        }

        setContentView(R.layout.activity_player_youtube);

        bind();

        setupPlayer();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {

//        Log.i(TAG, "onKeyDown: " + event);

        switch (keyCode){
            case KeyEvent.KEYCODE_MEDIA_PLAY_PAUSE:
                //play/pause pada keybaord external
                playPauseVideo();
                break;
            case KeyEvent.KEYCODE_BACK:
                finish();
                break;
        }

        return super.onKeyDown(keyCode, event);
    }

    private void bind(){

        youTubePlayerView = findViewById(R.id.youtube_player_view);

    }

    private void setupPlayer(){

        uiController = youTubePlayerView.getPlayerUIController();

        uiController.showUI(true);
        uiController.showCurrentTime(true);
        uiController.showDuration(true);
        uiController.showSeekBar(true);

        uiController.showMenuButton(false);
        uiController.showPlayPauseButton(false);
        uiController.showYouTubeButton(false);
        uiController.showFullscreenButton(false);

        youTubePlayerView.enterFullScreen();

        youTubePlayerView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //play / pause saat di click
                playPauseVideo();

            }
        });

        youTubePlayerView.initialize(new YouTubePlayerInitListener() {

            @Override
            public void onInitSuccess(final com.pierfrancescosoffritti.youtubeplayer.player.YouTubePlayer initializedYouTubePlayer) {

                initializedYouTubePlayer.addListener(new AbstractYouTubePlayerListener() {
                    @Override
                    public void onReady() {

                        youtubePlayer = initializedYouTubePlayer;

                        //pindahin dari bawah ke atas, biar lebih kelihatan proses initialisasinya
                        initializedYouTubePlayer.loadVideo(videoId, 0);

                        //ini utk capture event
                        youtubePlayer.addListener(youTubePlayerListener);

                    }
                });
            }
        }, true);
    }

    YouTubePlayerListener youTubePlayerListener = new YouTubePlayerListener() {
        @Override
        public void onReady() {

        }

        @Override
        public void onStateChange(int state) {
//            Log.i(TAG, "onStateChange: state=" + state);

            //simpan state
            playerState = state;

            switch (state){
                case PlayerConstants.PlayerState.ENDED:
                    finish(); //balik ke news setelah video selesai
                    break;
            }

        }

        @Override
        public void onPlaybackQualityChange(@NonNull String playbackQuality) {

        }

        @Override
        public void onPlaybackRateChange(@NonNull String playbackRate) {

        }

        @Override
        public void onError(int error) {
//            Log.i(TAG, "onError: error=" + error);
        }

        @Override
        public void onApiChange() {

        }

        @Override
        public void onCurrentSecond(float second) {

        }

        @Override
        public void onVideoDuration(float duration) {

        }

        @Override
        public void onVideoLoadedFraction(float loadedFraction) {

        }

        @Override
        public void onVideoId(@NonNull String videoId) {

        }
    };

    private void playPauseVideo(){

        if (playerState == PlayerConstants.PlayerState.PLAYING){
            youtubePlayer.pause();
        }
        if (playerState == PlayerConstants.PlayerState.PAUSED){
            youtubePlayer.play();
        }

    }


}

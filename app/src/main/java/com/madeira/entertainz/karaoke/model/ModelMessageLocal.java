package com.madeira.entertainz.karaoke.model;

import android.util.Log;

import com.google.gson.Gson;
import com.madeira.entertainz.karaoke.DBLocal.TLocalFile;
import com.madeira.entertainz.karaoke.DBLocal.TMessage;
import com.madeira.entertainz.karaoke.DBLocal.TMessageMedia;
import com.madeira.entertainz.library.ApiError;
import com.madeira.entertainz.library.Util;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.List;

public class ModelMessageLocal {

    static String TAG = "ModelMessageLocal";

    public static ResultGetMessageList geMessageFromLocal(String filePath) {
//        List<TLocalFile> tLocalFileList = Util.getUSBFile("json");
        ResultGetMessageList r = null;
//        for (TLocalFile tLocalFile : tLocalFileList) {
//            if (tLocalFile.fileType.equals("json") && tLocalFile.fileName.toLowerCase().equals("message.json")) {
                String stringJSON = "";
                try {
                    File file = new File(filePath);
                    FileInputStream fileInputStream = new FileInputStream(file);
                    stringJSON = Util.getJSONFromTextFile(fileInputStream);
                    Log.d(TAG, filePath);
                    Gson gson = new Gson();

                    //throw apabila ada apiError
                    try {
                        ApiError.process(gson, stringJSON);
                        r = gson.fromJson(stringJSON, ResultGetMessageList.class);
                    } catch (ApiError apiError) {
                        apiError.printStackTrace();
                    }

                } catch (IOException e) {
                    e.printStackTrace();
                }
//                break;
//            }
//        }


        return r;

    }

    public class ResultGetMessageList {
        public List<TMessage> list;
        public List<TMessageMedia> medias;
        public int count;
    }

}

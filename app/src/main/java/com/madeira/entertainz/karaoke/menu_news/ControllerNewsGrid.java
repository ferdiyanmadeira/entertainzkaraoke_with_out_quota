package com.madeira.entertainz.karaoke.menu_news;

import android.content.Context;
import android.content.res.ColorStateList;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.madeira.entertainz.karaoke.DBLocal.TNews;
import com.madeira.entertainz.karaoke.DBLocal.TNewsCategory;
import com.madeira.entertainz.karaoke.Debug;
import com.madeira.entertainz.karaoke.R;
import com.madeira.entertainz.karaoke.model_online.ModelNews;
import com.madeira.entertainz.library.ApiError;
import com.madeira.entertainz.library.EzRecyclerViewAdapter;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ControllerNewsGrid implements NewsTheme.INewsTheme {
    private static final String TAG = "ControllerNewsGrid";

    private Context context;
    private RecyclerView recyclerView;
    private EzRecyclerViewAdapter adapter;

    private int categoryId;
    private List<TNews> mainList;

    GridLayoutManager gridLayoutManager;

    Callback callback;
    ColorStateList colorSelection, colorText;
    NewsTheme newsTheme;

    @Override
    public void setThemeUpdateDate(Date lastUpdateTheme) {

    }

    @Override
    public void setListColorTheme(int color) {

    }

    @Override
    public void setTextAndSelectionColorTheme(ColorStateList colorSelection, ColorStateList colorText, int colorTextSelected, int colorTextFocus, int colorTextNormal, int colorSelectionSelected, int colorSelectionFocus) {
        try {
            this.colorSelection = colorSelection;
            this.colorText = colorText;
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    public interface Callback {
        void onNewsClick(TNews item);

        void onFinishLoadNews();
    }

    public ControllerNewsGrid(Context context) {
        try {
            this.context = context;
            initialize();
            newsTheme = new NewsTheme(context, this);
            newsTheme.setTheme();
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    private void initialize() {
        mainList = new ArrayList<>();
    }

    public void attach(RecyclerView recyclerView) {
        this.recyclerView = recyclerView;
        setupRecyclerView();
    }

    public void setCallback(Callback callback) {
        this.callback = callback;
    }

    public void loadNews(int categoryId) {
        try {
            if (this.categoryId != categoryId)
                mainList.clear();
            this.categoryId = categoryId;
            //reset data
            fetchData(mainList.size(), categoryId);
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    private void fetchData(int offset, int categoryId) {

        try {
            if (offset == 0) {
                mainList.clear();
                adapter.notifyDataSetChanged();
            }
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }

        ModelNews.fetchNewsList(categoryId, offset, 50, new ModelNews.FetchNewsListListener() {
            @Override
            public void onSuccess(List<TNews> list) {
                try {
                    mainList.addAll(list);
                    adapter.notifyDataSetChanged();
                    callback.onFinishLoadNews();
                } catch (Exception ex) {
                    Debug.e(TAG, ex);
                }
            }

            @Override
            public void onError(ApiError e) {
                try {
                    callback.onFinishLoadNews();
                } catch (Exception ex) {
                    Debug.e(TAG, ex);
                }
            }
        });

    }

    private void setupRecyclerView() {

        gridLayoutManager = (GridLayoutManager) recyclerView.getLayoutManager();
        gridLayoutManager.setSpanCount(3);

        adapter = new EzRecyclerViewAdapter(new EzRecyclerViewAdapter.IEzRecyclerViewAdapter() {
            @Override
            public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
                View view = EzRecyclerViewAdapter.inflate(context, R.layout.cell_news, parent);
                return new ItemViewHolder(view);
            }

            @Override
            public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
                try {
                    ItemViewHolder item = (ItemViewHolder) holder;
                    item.bind(mainList.get(position), position);
                } catch (Exception ex) {
                    Debug.e(TAG, ex);
                }
            }

            @Override
            public int getItemCount() {
                return mainList.size();
            }

            @Override
            public long getItemId(int pos) {
                return mainList.get(pos).newsId;
            }
        });

        recyclerView.setAdapter(adapter);
    }

///////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////

    public class ItemViewHolder extends RecyclerView.ViewHolder {

        TextView textViewTitle;
        TextView textViewDescription;
        TextView textViewDate;
        ImageView imageViewThumb;

        ItemViewHolder(View itemView) {
            super(itemView);

            imageViewThumb = itemView.findViewById(R.id.image_view_thumb);
            textViewTitle = itemView.findViewById(R.id.text_view_title);
            textViewDescription = itemView.findViewById(R.id.text_view_description);
        }

        public void bind(TNews item, int pos) {

            try {

                Picasso.with(ControllerNewsGrid.this.context).load(item.urlThumb).into(imageViewThumb);

                textViewTitle.setText(item.title);
//                if (colorText != null) textViewTitle.setTextColor(colorText);
                Spanned html;
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    html = Html.fromHtml(item.body, Html.FROM_HTML_MODE_COMPACT);
                } else {
                    html = Html.fromHtml(item.body);
                }
                textViewDescription.setText(html);
//                if (colorText != null) textViewDescription.setTextColor(colorText);

            } catch (Exception e) {
                e.printStackTrace();
            }

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    callback.onNewsClick(item);
                }
            });

//            itemView.setOnFocusChangeListener(new View.OnFocusChangeListener() {
//                @Override
//                public void onFocusChange(View view, boolean b) {
//                    if (b) {
//                        Log.i(TAG, "onFocusChange: FOCUS " + item.title);
//                        view.setSelected(true);
//                    } else {
//                        view.setSelected(false);
//                        Log.i(TAG, "onFocusChange: LOST " + item.title + " view=" + view.toString());
//                    }
//                }
//            });
        }
    }

}

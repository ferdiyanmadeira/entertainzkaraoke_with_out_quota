package com.madeira.entertainz.karaoke.model_room_db;

import android.content.Context;

import com.madeira.entertainz.karaoke.DBLocal.DbAccess;
import com.madeira.entertainz.karaoke.DBLocal.DbMessage;
import com.madeira.entertainz.karaoke.DBLocal.DbSong;
import com.madeira.entertainz.karaoke.DBLocal.DbYoutube;
import com.madeira.entertainz.karaoke.DBLocal.TElement;
import com.madeira.entertainz.karaoke.DBLocal.TElementMedia;
import com.madeira.entertainz.karaoke.DBLocal.TLocalFileAPK;
import com.madeira.entertainz.karaoke.DBLocal.TMessage;
import com.madeira.entertainz.karaoke.DBLocal.TMessageMedia;
import com.madeira.entertainz.karaoke.DBLocal.TPlaylist;
import com.madeira.entertainz.karaoke.DBLocal.TSettingElementMedia;
import com.madeira.entertainz.karaoke.DBLocal.TSong;
import com.madeira.entertainz.karaoke.DBLocal.TSongCategory;
import com.madeira.entertainz.karaoke.DBLocal.TSongPlaylist;
import com.madeira.entertainz.karaoke.DBLocal.TSongYoutubePlaylist;
import com.madeira.entertainz.karaoke.DBLocal.TSticker;
import com.madeira.entertainz.karaoke.DBLocal.TYoutubePlaylist;
import com.madeira.entertainz.karaoke.Debug;
import com.madeira.entertainz.karaoke.config.C;
import com.madeira.entertainz.karaoke.config.CacheData;
import com.madeira.entertainz.karaoke.config.Global;
import com.madeira.entertainz.library.PerformAsync2;

import java.util.ArrayList;
import java.util.List;

public class ModelRoom {
    static String TAG = "ModelRoom";

    /**
     * untuk insert content ke room dari splash screen
     */
    public interface IInsertContent {
        void onInsert(int result);
    }

    public void insertContent(List<TElement> tElementList, List<TElementMedia> tElementMediaList, List<TSong> tSongList, List<TSongCategory> tSongCategoryList, List<TMessage> tMessageList, List<TMessageMedia> tMessageMediaList, List<TSticker> tStickerList, List<TPlaylist> tPlaylistList, List<TSongPlaylist> tSongPlaylistList, List<TYoutubePlaylist> tYoutubePlaylistList, List<TSongYoutubePlaylist> tSongYoutubePlaylistList, List<TLocalFileAPK> tLocalFileAPKList, Context context, IInsertContent callback) {
        PerformAsync2.run(performAsync ->
        {
            int result = 0;
            try {
                DbAccess dbAccess = DbAccess.Instance.create(context);
                DbSong dbSong = DbSong.Instance.create(context);
                DbMessage dbMessage = DbMessage.Instance.create(context);
                DbYoutube dbYoutube = DbYoutube.Instance.create(context);
                if (tElementList.size() != 0) {
                    dbAccess.daoAccessTheme().deleteAll();
                    dbAccess.daoAccessTheme().insertAll(tElementList);
                }
                CacheData.listElement = tElementList;

                //pindahin semua ke hashtable utk faster search
                if (tElementList.isEmpty() == false) {
                    for (TElement item : tElementList) {
                        CacheData.hashElement.put(item.elementId, item);
                    }
                }

                if (tElementMediaList != null) {
                    for (TElementMedia item : tElementMediaList) {
                        int exist = dbAccess.daoAccessThemeMedia().checkExist(item.elementMediaId);
                        if (exist != 0) {
                            dbAccess.daoAccessThemeMedia().updateElementMedia(item.urlImage, item.elementMediaId);
                        } else {
                            item.duration = C.ELEMENT_MEDIA_DURATION;
                            dbAccess.daoAccessThemeMedia().insert(item);

                        }
                        int checkExistSetting = dbAccess.daoAccessThemeMedia().checkExistSetting(item.elementMediaId);
                        if (checkExistSetting == 0) {
                            TSettingElementMedia tSettingElementMedia = new TSettingElementMedia();
                            tSettingElementMedia.elementMediaId = item.elementMediaId;
                            tSettingElementMedia.duration = Global.getDuration();
                            tSettingElementMedia.isActive = 1;
                            dbAccess.daoAccessThemeMedia().insertSetting(tSettingElementMedia);
                        }
                    }
                }

                if (tSongCategoryList != null) {
                    if (tSongCategoryList.size() != 0) {
                        dbSong.daoAccessTSongCategory().deleteAll();
                        dbSong.daoAccessTSongCategory().insertAll(tSongCategoryList);
                        result = 1;
                    }
                }

                if (tSongList != null) {
                    if (tSongList.size() != 0) {
                        dbSong.daoAccessTSong().deleteAll();
                        dbSong.daoAccessTSong().insertAll(tSongList);
                        result = 1;
                    }
                }

                if (tMessageList != null) {
                    if (tMessageList.size() != 0) {
                        dbMessage.daoAccessMessage().deleteAll();
                        dbMessage.daoAccessMessage().insertAll(tMessageList);
                        result = 1;
                    }
                }

                if (tMessageMediaList != null) {
                    if (tMessageMediaList.size() != 0) {
                        dbMessage.daoAccessMessage().insertAllMedia(tMessageMediaList);
                        result = 1;
                    }
                }
                if (tStickerList != null) {
                    if (tStickerList.size() != 0) {
                        dbAccess.daoAccessSticker().deleteAll();
                        dbAccess.daoAccessSticker().insertAll(tStickerList);
                        result = 1;
                    }
                }

                if (tLocalFileAPKList != null) {
                    if (tLocalFileAPKList.size() != 0) {
                        dbAccess.daoAccessTLocalFileAPK().deleteAll();
                        dbAccess.daoAccessTLocalFileAPK().insertAll(tLocalFileAPKList);
                        result = 1;
                    }
                }

                if (tPlaylistList != null) {
                    if (tPlaylistList.size() != 0) {
                        dbSong.daoAccessTPlaylist().insertAll(tPlaylistList);
                        result = 1;
                    }
                }

                if (tSongPlaylistList != null) {
                    if (tSongPlaylistList.size() != 0) {
                        dbSong.daoAccessTSongPlaylist().insertAll(tSongPlaylistList);
                        result = 1;
                    }
                }

                if (tYoutubePlaylistList != null) {
                    if (tYoutubePlaylistList.size() != 0) {
                        dbYoutube.daoAccessTYoutubePlaylist().insertAll(tYoutubePlaylistList);
                        result = 1;
                    }
                }

                if (tSongYoutubePlaylistList != null) {
                    if (tSongYoutubePlaylistList.size() != 0) {
                        dbYoutube.daoAccessTSongYoutubePlaylist().insertAll(tSongYoutubePlaylistList);
                        result = 1;
                    }
                }
                dbAccess.close();
                dbSong.close();
                dbMessage.close();
            } catch (Exception ex) {
                result = 0;
            }
            return result;
        }).
                setCallbackResult(result ->

                {
                    try {
                        int i = (int) result;
                        callback.onInsert(i);
                    } catch (Exception ex) {
                        Debug.e(TAG, ex);
                    }

                });
    }


}

package com.madeira.entertainz.karaoke;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.content.Context;
import android.util.Log;

import com.madeira.entertainz.karaoke.config.C;
import com.madeira.entertainz.karaoke.config.CacheData;
import com.madeira.entertainz.karaoke.model.ModelThemeLocal;
import com.madeira.entertainz.karaoke.DBLocal.DbAccess;
import com.madeira.entertainz.karaoke.DBLocal.TElement;
import com.madeira.entertainz.library.Atomic;
import com.madeira.entertainz.library.PerformAsync2;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.util.Date;
import java.util.List;

public class BackgroundThread {
    private static final String TAG = "BackgroundThread";
    private static final int LISTEN_PORT = 2000;
    private static final int MAX_UDP_BUFFER = 570;
    private static final int SLEEP_TIME = 10000;

    private static BackgroundThread instance;

    volatile boolean isRunning;

    private Thread thread;

    private Atomic<String> atomicSessionId;

    private Context context;

    private MutableLiveData<Date> liveDataNewMessage = new MutableLiveData<>();
    private MutableLiveData<Date> liveDataThemeUpdate = new MutableLiveData<>();

    public static BackgroundThread getInstance(){
        return instance;
    }

    public BackgroundThread(Context context){
        BackgroundThread.instance = this;
        this.context = context;
        thread = new Thread(mainRun);
        thread.start();
    }

    public void stop(){
        isRunning = false;
    }

    public void setSessionId(String newSessionId){

        if (atomicSessionId==null) {
            atomicSessionId = new Atomic<>(newSessionId);
        } else {
            atomicSessionId.set(newSessionId);
        }
    }

    static public LiveData<Date> setObserverNewMessage(){
        return instance.liveDataNewMessage;
    }

    static public LiveData<Date> setObserverThemeUpdate(){
        return instance.liveDataThemeUpdate;
    }

    Runnable mainRun = new Runnable() {
        @Override
        public void run() {

            isRunning = true;

            while (isRunning){

                doMainLoop();

//                try {
//                    Thread.sleep(SLEEP_TIME);
//                } catch (InterruptedException e) {
//                }
            }

        }
    };

    void doMainLoop(){

        receiveMessage(LISTEN_PORT);

    }

    /**
     * telnet localhost 5554
     * redir add udp:2000:2000
     *
     * @param port
     */
    void receiveMessage(int port) {

        String message;
        byte[] lmessage = new byte[MAX_UDP_BUFFER];
        DatagramPacket packet = new DatagramPacket(lmessage, lmessage.length);

        SocketAddress sa = new InetSocketAddress(port);

        DatagramSocket socket = null;
        try {
            socket = new DatagramSocket(null);
            socket.setReuseAddress(true);
            socket.bind(sa);
//            socket.setSoTimeout(1000);
            socket.receive(packet);
            message = new String(lmessage, 0, packet.getLength());

            if (message!=null && message.length()>0){
                processMessage(message);
            }

        } catch (SocketException e) {
            e.printStackTrace();
        } catch (IOException e) {
            if (e instanceof SocketTimeoutException) {

            } else{
                e.printStackTrace();
            }
        } finally {
            socket.close();
        }
    }

    void processMessage(String msg){

        Log.i(TAG, "processMessage=" + msg);

        switch (msg.toLowerCase()){

            case "message":
                onMessage();
                break;
            case "theme":
                onThemeUpdate();
                break;
        }
    }

    void onMessage(){

       /* PerformAsync2.run(new PerformAsync2.Callback() {
            @Override
            public Object onBackground(PerformAsync2 performAsync) {

                ModelMessage.ResultGetMessageList result;

                try {
                    result = ModelMessage.syncGetMessageList(atomicSessionId.get());

                    if(result==null || result.list==null) return 0;

                    DbTheme dbTheme = DbTheme.Instance.create(context);

                    dbTheme.daoMessage().deleteAll();
                    dbTheme.daoMessage().insertAll(result.list);
                    dbTheme.daoMessage().deleteAllMedia();
                    dbTheme.daoMessage().insertAllMedia(result.medias);

                    dbTheme.close();

                    for (TMessage msg: result.list) {
                        if (msg.status.equals(C.MESSAGE_STATUS_NEW)){
                            return 1;
                        }
                    }

                } catch (ApiError apiError) {
                    apiError.printStackTrace();
                    Log.e(TAG, apiError.errMsg);
                }

                return 0;
            }
        }).setCallbackResult(new PerformAsync2.CallbackResult() {
            @Override
            public void onResult(Object result) {
                int value = (int) result;

                //apabila value==1 artinya ada NEW message
                //
                if (value==1) liveDataNewMessage.setValue(new Date());
            }
        });*/

    }

    void onThemeUpdate(){

       PerformAsync2.run(new PerformAsync2.Callback() {
            @Override
            public Object onBackground(PerformAsync2 performAsync) {

                List<TElement> list;

                //ambil theme dari api
                ModelThemeLocal.ResultGetThemeList resultGetThemeList = ModelThemeLocal.getThemeFromLocal(C.JSON_THEME);
                list = resultGetThemeList.list;
                if(list==null) return null;

                //replace record yg ada
                DbAccess dbAccess = DbAccess.Instance.create(context);
                dbAccess.daoAccessTheme().deleteAll();
                dbAccess.daoAccessTheme().insertAll(list);
                dbAccess.close();

                Log.i(TAG, "themeList=" + list.size());

                return list;

            }
        }).setCallbackResult(new PerformAsync2.CallbackResult() {
            @Override
            public void onResult(Object result) {

                if (result==null) return;

                if (result instanceof Exception){
                    //tdk perlu di isi apa2

                }else {

                    List<TElement> list = (List<TElement>) result;

                    CacheData.listElement = list;

                    CacheData.hashElement.clear();
                    for (TElement item: list) {
                        CacheData.hashElement.put(item.elementId, item);
                    }
                    liveDataThemeUpdate.setValue(new Date());
                }

            }
        });

    }

}

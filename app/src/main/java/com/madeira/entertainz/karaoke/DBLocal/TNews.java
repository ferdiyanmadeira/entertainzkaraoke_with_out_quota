package com.madeira.entertainz.karaoke.DBLocal;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

@Entity(tableName = "tnews")
public class TNews implements Serializable {

    @PrimaryKey
    @NonNull
    @SerializedName("news_id")
    @Expose
    public int newsId;

    @SerializedName("news_category_id")
    @Expose
    public int newsCategoryId;

    @SerializedName("ordinal")
    @Expose
    public int ordinal;

    @SerializedName("title")
    @Expose
    public String title;

    @SerializedName("body")
    @Expose
    public String body;

    @SerializedName("video_type")
    @Expose
    public String videoType; //YOUTUBE / OTHERS

    @SerializedName("url_video")
    @Expose
    public String urlVideo;

    @SerializedName("url_thumb")
    @Expose
    public String urlThumb;

    @SerializedName("create_date")
    @Expose
    public String createDate;

    @SerializedName("update_date")
    @Expose
    public String updateDate;

    @Override
    public boolean equals(@Nullable Object obj) {
        if (obj==null) return false;
        if (!(obj instanceof TNews)) return false;

        TNews tNews = (TNews) obj;
        return tNews.newsId==newsId;
    }
}
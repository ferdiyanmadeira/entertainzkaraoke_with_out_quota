package com.madeira.entertainz.karaoke.DBLocal;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

@Entity(tableName = "TDownloadYoutubeDt")
public class TDownloadYoutubeDt implements Serializable {

    @PrimaryKey @NonNull
    @SerializedName("youtube_id")
    public String songId;
    @SerializedName("url_video")
    public String urlVideo;
    @SerializedName("url_sound")
    public String urlSound;
    @SerializedName("filesize")
    public String filesize;
}

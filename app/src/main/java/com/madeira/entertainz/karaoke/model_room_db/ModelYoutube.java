package com.madeira.entertainz.karaoke.model_room_db;

import android.app.Application;
import android.content.Context;
import android.util.Log;

import com.madeira.entertainz.karaoke.DBLocal.DbYoutube;
import com.madeira.entertainz.karaoke.DBLocal.TSongYoutube;
import com.madeira.entertainz.karaoke.MainApplication;
import com.madeira.entertainz.library.PerformAsync2;
import com.madeira.eznet.lib.HelperGson;

import java.util.ArrayList;
import java.util.List;

/**
 * di pakai utk Youtube Karaoke Personal
 */
public class ModelYoutube {
    static String TAG = "ModelYoutube";

    public interface OnLoadPersonalList {
        void onLoadPersonalList(List<TSongYoutube> list);
    }

    /**
     * untuk get list youtube
     */
//    public interface IGetListYoutube {
//        void onGetListYoutube(List<TSongYoutube> joinYoutubePlaylistDts);
//    }

//    public static void getListYoutube(Context context, IGetListYoutube callback) {
//        PerformAsync2.run(performAsync ->
//        {
//            List<TSongYoutube> result = new ArrayList<>();
//            result = syncGetListYoutube(context);
//            return result;
//        }).
//                setCallbackResult(result ->
//
//                {
//                    try {
//                        List<TSongYoutube> tableList = (List<TSongYoutube>) result;
//                        callback.onGetListYoutube(tableList);
//                    } catch (Exception ex) {
//                        Debug.e(TAG, ex);
//                    }
//
//                });
//    }

    /**
     * ambil semua list yg ada pada personal youtube karaoke
     *
     * @param context
     * @return
     */
    public static List<TSongYoutube> syncGetAll(Context context) {
        List<TSongYoutube> result = new ArrayList<>();
        try {
            DbYoutube dbYoutube = DbYoutube.Instance.create(context);
            result = dbYoutube.daoAccessTSongYoutube().getAll();
            dbYoutube.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return result;
    }

    /**
     * wrapper utk syncGetAll
     * @return
     */
    public static String ekmaGetAll(){

        Log.i(TAG, "ekmaGetAll");

        List<TSongYoutube> list = syncGetAll(MainApplication.context);

        ResponseGetAll response = new ResponseGetAll();
        response.list = list;

        String r = HelperGson.toString(response);

        return r;
    }

    public static void loadPersonalList(Context context, OnLoadPersonalList callback){
        PerformAsync2.run(new PerformAsync2.Callback() {
            @Override
            public Object onBackground(PerformAsync2 performAsync) {

                List<TSongYoutube> list = syncGetAll(context);

                return list;
            }
        }).setCallbackResult(new PerformAsync2.CallbackResult() {
            @Override
            public void onResult(Object result) {
                callback.onLoadPersonalList((List<TSongYoutube>) result);
            }
        });
    }

    /**
     * untuk add youtube
     */
    public static boolean syncAdd(Context context,
                                 String videoId,
                                 String urlVideo,
                                 String urlThumbnail,
                                 String title,
                                 String artist,
                                 String lyric,
                                 Integer duration ) {
        try {

            TSongYoutube item = new TSongYoutube();
            item.songId = videoId;
            item.songURL = urlVideo;
            item.thumbnail = urlThumbnail;
            item.songName = title;
            item.artist = artist;
            item.duration = duration;
            item.lyric = lyric;

            DbYoutube dbYoutube = DbYoutube.Instance.create(context);
            dbYoutube.daoAccessTSongYoutube().insert(item);
            dbYoutube.close();

            return true;
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return false;
    }

    /**
     * function ini di panggil utk RPC,
     * karena context tdk bisa masuk melalui parameter
     *
     * @param videoId
     * @param urlVideo
     * @param urlThumbnail
     * @param title
     * @param artist
     * @param lyric
     * @param duration
     * @return
     */
    public static String ekmaAdd(
                                 String videoId,
                                 String urlVideo,
                                 String urlThumbnail,
                                 String title,
                                 String artist,
                                 String lyric,
                                 Integer duration ) {

        Log.i(TAG, "ekmaAdd: ");

        boolean r =  syncAdd(MainApplication.context,
                videoId, urlVideo, urlThumbnail, title, artist, lyric, duration);

        if (r) return "1"; else return "0";
    }


        /**
         * untuk Delete youtube
         */

    public static int syncDelete(Context context, String videoIds) {
        int counter = 0;

        try {

            String []ar = videoIds.split(",");

            DbYoutube dbYoutube = DbYoutube.Instance.create(context);
            for (String videoId : ar) {
                counter++;
                dbYoutube.daoAccessTSongYoutube().deleteWithId(videoId);
            }
            dbYoutube.close();

            return counter;

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return counter;
    }

    /**
     * Wrapper utk syncDelete
     *
     * @param videoIds
     * @return
     */
    public static String ekmaDelete(String videoIds){

        int counter = syncDelete(MainApplication.context, videoIds);

        return String.valueOf(counter);
    }

    /**
     * untuk Update youtube
     */

//    public static boolean syncUpdate(List<TSongYoutube> tSongYoutube, Context context) {
//        Boolean result = false;
//        try {
//            DbYoutube dbYoutube = DbYoutube.Instance.create(context);
//            for (TSongYoutube item : tSongYoutube) {
//                dbYoutube.daoAccessTSongYoutube().update(item.songId, item.songName, item.songCategoryId, item.lyric);
//            }
//            dbYoutube.close();
//            result = true;
//        } catch (Exception ex) {
//
//        }
//        return result;
//    }

//    public static final String EKMAGetYoutube() {
//        String result = "";
//        try {
//            List<TSongYoutube> tSongYoutubeList = syncGetListYoutube(MainApplication.context);
//            result = HelperGson.toString(tSongYoutubeList);
//        } catch (Exception ex) {
//            Debug.e(TAG, ex);
//        }
//        return result;
//    }
//
//    public static final boolean EKMAUpdateYoutube(String jsonString) {
//        boolean result = false;
//        try {
//            Gson gson = new Gson();
//            List<TSongYoutube> tSongYoutubeList = gson.fromJson(jsonString, new TypeToken<List<TSongYoutube>>() {
//            }.getType());
//            result = syncUpdateYoutube(tSongYoutubeList, MainApplication.context);
//
//        } catch (Exception ex) {
//            Debug.e(TAG, ex);
//        }
//
//        return result;
//    }


//    public static final boolean EKMAAddYoutube(String jsonString) {
//        boolean result = false;
//        try {
//            Gson gson = new Gson();
//            List<TSongYoutube> tSongYoutubeList = gson.fromJson(jsonString, new TypeToken<List<TSongYoutube>>() {
//            }.getType());
//            result = syncAddYoutube(tSongYoutubeList, MainApplication.context);
//
//        } catch (Exception ex) {
//            Debug.e(TAG, ex);
//        }
//
//        return result;
//    }

//    public static final boolean EKMADeleteYoutube(String jsonString) {
//        boolean result = false;
//        try {
//            Gson gson = new Gson();
//            List<TSongYoutube> tSongYoutubeList = gson.fromJson(jsonString, new TypeToken<List<TSongYoutube>>() {
//            }.getType());
//            result = syncDeleteYoutube(tSongYoutubeList, MainApplication.context);
//
//        } catch (Exception ex) {
//            Debug.e(TAG, ex);
//        }
//
//        return result;
//    }

    static class ResponseGetAll {
        public List<TSongYoutube> list;
    }
}

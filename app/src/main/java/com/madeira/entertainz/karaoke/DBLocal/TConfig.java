package com.madeira.entertainz.karaoke.DBLocal;

import com.google.gson.annotations.SerializedName;

public class TConfig {
    @SerializedName("time_zone")
    public String timeZone;

    @SerializedName("quota")
    public int quota;

}

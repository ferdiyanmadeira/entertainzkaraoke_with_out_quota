package com.madeira.entertainz.karaoke.DBLocal;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;


/** table ini digunakan untuk memprocess json song yg ada di server (simpan ke local untuk download & fetch data dari server)*/
@Entity(tableName = "tsong_server")
public class TSongItemServer implements Serializable {
    @PrimaryKey @NonNull
    @SerializedName("songId")
    public String songId;
    @SerializedName("batchNumber")
    public String batchNumber;
    @SerializedName("songPath")
    public String songPath;
    @SerializedName("songName")
    public String songName;
    @SerializedName("artist")
    public String artist;
    @SerializedName("songWriter")
    public String songWriter;
    @SerializedName("duration")
    public String duration;
    @SerializedName("vocalSound")
    public String vocalSound;
    @SerializedName("thumbnail")
    public String thumbnail;
    @SerializedName("filesize")
    public String filesize;
    @SerializedName("genre_id")
    public String genreId;
    @SerializedName("iv")
    public String iv;
    @SerializedName("key")
    public String key;
}

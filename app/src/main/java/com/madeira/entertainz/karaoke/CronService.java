package com.madeira.entertainz.karaoke;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;
import android.view.View;

import com.androidnetworking.error.ANError;
import com.google.gson.Gson;
import com.madeira.entertainz.controller.STBControl;
import com.madeira.entertainz.karaoke.DBLocal.DbAccess;
import com.madeira.entertainz.karaoke.DBLocal.DbMessage;
import com.madeira.entertainz.karaoke.DBLocal.TLocalFileAPK;
import com.madeira.entertainz.karaoke.DBLocal.TLogSong;
import com.madeira.entertainz.karaoke.DBLocal.TMessage;
import com.madeira.entertainz.karaoke.DBLocal.TSTBInfo;
import com.madeira.entertainz.karaoke.DBLocal.TSongCategory;
import com.madeira.entertainz.karaoke.config.C;
import com.madeira.entertainz.karaoke.config.Global;
import com.madeira.entertainz.karaoke.model.ModelSongLocal;
import com.madeira.entertainz.karaoke.model_online.ModelApp;
import com.madeira.entertainz.karaoke.model_online.ModelMessage;
import com.madeira.entertainz.karaoke.model_online.ModelSTB;
import com.madeira.entertainz.karaoke.model_online.ModelSong;
import com.madeira.entertainz.library.Async;
import com.madeira.entertainz.library.PerformAsync2;
import com.madeira.entertainz.library.RootUtil;
import com.madeira.entertainz.library.Util;

import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import es.dmoral.toasty.Toasty;

import static com.madeira.entertainz.karaoke.config.C.INTERVAL_SERVER_SYNC;
import static java.lang.Math.abs;
import static java.lang.Math.getExponent;

public class CronService extends Service {
    static String TAG = "CronService";
    Handler handler = new Handler();
    Runnable runnable;
    private static final long ONE_HOUR = 1000 * 60 * 60;
    static DbMessage dbMessage;
    Context context;

    public CronService() {
        try {
            // register EKMA Broadcast Receiver
            context = this;
//            MgmtService.registerEKMAConnectedReceiver(context, bc);
            runnable = new Runnable() {
                @Override
                public void run() {

                    ModelApp.checkInternet(new ModelApp.ICheckConnectToHost() {
                        @Override
                        public void onConnected(boolean connected) {
                            if (connected) {
                                try {
                                    File usbFile = new File(Global.getUSBPath());
                                    if (usbFile.exists()) {
                                        if (C.FG_SYNC_QUOTA) {
                                            getLogSong();
                                        } else
                                            updateSTBInfo();
                                    }
                                } catch (Exception ex) {
                                    Debug.e(TAG, ex);
                                }
                            }
                        }

                        @Override
                        public void onError(ANError e) {
                        }
                    });

                    handler.postDelayed(runnable, INTERVAL_SERVER_SYNC);
                }
            };
            handler = new Handler();
            handler.postDelayed(runnable, C.oneSecondInterval);

        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.

        throw new UnsupportedOperationException("Not yet implemented");
    }


    /**
     * send ke server lagu yang pernah di play
     */
    void getLogSong() {

        com.madeira.entertainz.karaoke.model_room_db.ModelSong.getLogSong(getApplicationContext(), new com.madeira.entertainz.karaoke.model_room_db.ModelSong.IGetListLogSong() {
            @Override
            public void onGetListLogSong(List<TLogSong> tLogSongList) {
                if (tLogSongList != null) {
                    //dicomment , tidak jadi digunakan, quota menggunakan shared preference
//                    syncQuota(tLogSongList);
                    try {
                        int usedQuota = 0;
                        for (TLogSong tLogSong : tLogSongList) {
                            usedQuota = usedQuota + tLogSong.totalCount;
                        }

                        String mac = Util.getMACAddress("eth0");
                        mac = mac.replace(":", "");

                        syncQuotaToServer(mac, usedQuota, tLogSongList);

                    } catch (Exception ex) {
                        Debug.e(TAG, ex);
                    }
                }
            }
        });

    }

    /**
     * untuk sync quota ke server
     */
    void syncQuotaToServer(String mac, int usedQuota, List<TLogSong> tLogSongList) {
        ModelSTB.syncQuota(mac, usedQuota, tLogSongList, new ModelSTB.ISyncQuota() {
            @Override
            public void onSync(JSONObject result) {
                com.madeira.entertainz.karaoke.model_room_db.ModelSong.deleteLogSong(getApplicationContext(), new com.madeira.entertainz.karaoke.model_room_db.ModelSong.IDeleteLogSong() {
                    @Override
                    public void onDeleteLogSong(int result) {
                        if (result == 1) {
                            updateSTBInfo();
                        }
                    }
                });
            }

            @Override
            public void onError(ANError e) {
                Debug.e(TAG, e);
            }
        });
    }

    void syncQuota(List<TLogSong> tLogSongList) {
        String TAG = CronService.TAG + "-" + "syncQuota";
        PerformAsync2.run(performAsync ->
        {
            List<TSTBInfo> tstbInfoList = new ArrayList<>();
            try {
                DbAccess dbAccess = DbAccess.Instance.create(getApplicationContext());
                tstbInfoList = dbAccess.daoAccessTSTBInfo().getAll();
                dbAccess.close();
            } catch (Exception ex) {
                Debug.e(TAG, ex);
            }

            return tstbInfoList;
        }).setCallbackResult(result ->
        {
            try {
                List<TSTBInfo> tstbInfoList = (List<TSTBInfo>) result;
                if (tstbInfoList.size() > 0) {
                    TSTBInfo tstbInfo = tstbInfoList.get(0);

                }
            } catch (Exception ex) {
                Debug.e(TAG, ex);
            }
        });
    }

    void updateSTBInfo() {
        String TAG = CronService.TAG + "-" + "updateSTBInfo";
        try {
            String appId = getPackageName();
            String mac = Util.getMACAddress("eth0");
            mac = mac.replace(":", "");
            String versionCode = String.valueOf(BuildConfig.VERSION_CODE);
            String versionName = BuildConfig.VERSION_NAME;
            String androidVersion = String.valueOf(Build.VERSION.SDK_INT);

            ModelSTB.updateSTBInfo(mac, appId, versionCode, versionName, androidVersion, new ModelSTB.IUpdateSTBInfo() {
                @Override
                public void onUpdateSTBInfo(ModelApp.appItem result) {
                    if (result != null) {
                        //check update
                        try {
                            /*int currentVersion = getPackageManager().getPackageInfo(getPackageName(), 0).versionCode;
                            if (currentVersion < Integer.valueOf(result.app.versionCode))*/
                            checkUpdate(result);
//                        String dateTime = result.serverTime;
//                        setDeviceTime(dateTime);

                            //write json
//                        writeSTBJsonToFile(result);
//                        ModelSTB.PriceQuotaItem PriceQuotaItem = result.stb;
//                        String variantId = PriceQuotaItem.variantId;
//                        String stbId = PriceQuotaItem.sTBId;
                            insertSTBInfoToRoom(result);
                            getMessage();
                            getGenre();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }
                }

                @Override
                public void onError(ANError e) {
                    Toasty.error(getApplicationContext(), TAG + " " + e.getMessage()).show();
                    getGenre();
                    getMessage();
                }
            });
        } catch (Exception ex) {
            ex.printStackTrace();
            Debug.e(TAG, ex);
        }
    }


    void insertSTBInfoToRoom(ModelApp.appItem stbItem) {
        String TAG = CronService.TAG + "-insertSTBInfoToRoom";
        Async.run(performAsync ->
        {
            int result = 0;
            try {
                ModelSTB.subSTBItem subSTBItem = stbItem.stb;
                if (subSTBItem != null) {
//                    if (subSTBItem.updateQuota.equals("1")) {
                    DbAccess dbAccess = DbAccess.Instance.create(getApplicationContext());
                    List<TSTBInfo> tstbInfoList = dbAccess.daoAccessTSTBInfo().getAll();
                    TSTBInfo tstbInfo = new TSTBInfo();
                    if (tstbInfoList.size() > 0) {
                        tstbInfo = tstbInfoList.get(0);
//                            tstbInfo.totalSong = subSTBItem.totalSong;
//                            dbSong.daoAccessTSTBInfo().updateQuota(tstbInfo.totalSong, tstbInfo.id);
                    } else {
                        tstbInfo.sTBId = subSTBItem.sTBId;
                        tstbInfo.sN = subSTBItem.sN;
//                            tstbInfo.totalSong = subSTBItem.totalSong;
                        tstbInfo.variantId = subSTBItem.variantId;
                        dbAccess.daoAccessTSTBInfo().insert(tstbInfo);
//                        }
                        dbAccess.close();
                    }
                    result = 1;
                } else
                    result = 0;
            } catch (Exception ex) {
                Debug.e(TAG, ex);
            }
            return result;
        }).setCallbackResult(result ->
        {
            try {
                int r = (int) result;
                if (r == 0) {
                    String warning = getApplicationContext().getString(R.string.warning_quota_cant_sync);
                    Toasty.error(getApplicationContext(), warning).show();
                } else {
                    disableFgUpdateQuota();
                }
            } catch (Exception ex) {
                Debug.e(TAG, ex);
            }
        });
    }

    //todo untuk update field updateQuota=0, tujuannya untuk bahwa quota sudah terupdate ke stb setelah terjadi pembelian quota, update quota ini value=1 ketika purchase quota, jika tidak 0, untuk antisipasi jika terjadi gagal sync quota dari STB ke server
    void disableFgUpdateQuota() {
        String TAG = CronService.TAG = "-disableFgUpdateQuota";
        try {
            String mac = Util.getMACAddress("eth0");
            mac = mac.replace(":", "");
            ModelSTB.DisableSyncQuota(mac, new ModelSTB.IDisableSyncQuota() {
                @Override
                public void onDisableSync(JSONObject result) {

                }

                @Override
                public void onError(ANError e) {
                    Debug.e(TAG, e);
                }
            });
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    /*void getYoutube() {
        String TAG = CronService.TAG + "-" + "getYoutube";
        try {
            ModelYoutube.getYoutube(new ModelYoutube.IGetYoutube() {
                @Override
                public void onGetYoutube(ModelYoutube.listYoutubeItem item) {
                    //write json
                    if (item != null)
                        writeYoutubeJsonToFile(item);
                }

                @Override
                public void onError(ANError e) {

                }
            });
        } catch (Exception ex) {
            ex.printStackTrace();
            Debug.e(TAG, ex);
        }

    }*/

    //untuk sync dengan function di SettingCheckUpdateFragment
    void insertTLocalFileAPK(String prmContent) {
        String TAG = CronService.TAG + "-" + "insertTLocalFileAPK";
        try {
            Async.run(performAsync ->
            {
                int result = 0;
                DbAccess dbAccess = DbAccess.Instance.create(getApplicationContext());
                TLocalFileAPK tLocalFileAPK = new TLocalFileAPK();
                tLocalFileAPK.content = prmContent;
                dbAccess.daoAccessTLocalFileAPK().insert(tLocalFileAPK);
                dbAccess.close();
                result = 1;
                return result;

            }).setCallbackResult(result ->
            {
                int prmResult = (int) result;
                if (prmResult == 1) {

                }
            });
        } catch (Exception ex) {
            ex.printStackTrace();
            Debug.e(TAG, ex);
        }
    }

    //set device time from server

    private static void setDeviceTime(String serverTime) {
        try {

            Date dtServer = Util.convertTime(serverTime);
            Date now = new Date();

            //check apakah perlu set time apa tdk ?
            long delta = abs(now.getTime() - dtServer.getTime());

            //exit apabila perbedaan tdk sampai 1 jam
            if (delta < ONE_HOUR) return;

            RootUtil.setAutomaticDateTime(false);

            RootUtil.setDate(dtServer);
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    //update apk
    void checkUpdate(ModelApp.appItem result) {
        try {
            STBControl stbControl = new STBControl();
            boolean alreadyUpdate = false;
            alreadyUpdate = stbControl.downloadUpdate(getApplicationContext(), result);
            if (alreadyUpdate) {

            }
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
        /*try {
            //check update app
            ModelApp.subAppItem subAppItem = result.app;
            int currentVersionCode = BuildConfig.VERSION_CODE;
            if (Integer.valueOf(subAppItem.versionCode) > currentVersionCode && Global.getNextAppCode() < Integer.valueOf(subAppItem.versionCode)) {
                boolean fgDownloadAPK = false;

                String pathUpdate = Global.getPathUpdate();
                if (pathUpdate.equals(""))
                    fgDownloadAPK = true;
                else {
                    List<String> apkInfo = Util.getAPKInfo(getApplicationContext(), pathUpdate);
                    int versionCode = 0;
                    String versionName;
                    String packageName = null;
                    if (apkInfo.size() == 3) {
                        packageName = apkInfo.get(0);
                        versionCode = Integer.valueOf(apkInfo.get(1));
                        versionName = apkInfo.get(2);
                    }

                    if (versionCode > currentVersionCode)
                        fgDownloadAPK = true;

                }
                if (fgDownloadAPK) {
                    String appDir = getFilesDir().toString();
                    String[] fileNameArray = subAppItem.urlApk.split("/");
                    String fileName = fileNameArray[fileNameArray.length - 1];

                    AndroidNetworking.download(subAppItem.urlApk, appDir, fileName).build().startDownload(new DownloadListener() {
                        @Override
                        public void onDownloadComplete() {
                            try {
                                Global.setNextAppCodeName(subAppItem.versionName);
                                Global.setNextAppCode(Integer.valueOf(subAppItem.versionCode));
                                String usbPath = Global.getUSBPath() + "/" + C.PATH_EKB_APK + "/" + fileName;
                                if (usbPath != null) {
                                    String pathAPK = appDir + "/" + fileName;
                                    Global.setPathUpdate(pathAPK);
                                    String result = RootUtil.copyFile(pathAPK, usbPath);
                                    if (result.equals(""))
                                        insertTLocalFileAPK(usbPath);
                                }
                                String message = getApplicationContext().getString(R.string.downloadUpdate);
                                Toasty.info(getApplicationContext(), message).show();
                                sendBcDownloadFinish(true);
                            } catch (Exception ex) {
                                Debug.e(TAG, ex);
                            }
                        }

                        @Override
                        public void onError(ANError anError) {
                            Global.setPathUpdate("");
                        }
                    });
                }
            }
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }*/
    }

    void getGenre() {
        String TAG = CronService.TAG + "-" + "getCategory";
        try {
            ModelSong.getGenre(new ModelSong.IGetGenre() {
                @Override
                public void onGetGenre(ModelSong.listGenreItem result) {
                    if (result != null) {
                        writeGenreToFile(result);
                    }
                }

                @Override
                public void onError(ANError e) {

                }
            });

        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }

    }

    void writeGenreToFile(ModelSong.listGenreItem listGenreItem) {
        String TAG = CronService.TAG + "-" + "writeGenreToFile";
        try {
            Gson gson = new Gson();

            List<ModelSong.genreItem> genreItemList = listGenreItem.listGenre;
            List<TSongCategory> tSongCategoryList = new ArrayList<>();
            TSongCategory dummyYTCat = new TSongCategory();
            dummyYTCat.songCategoryId = 0;
            dummyYTCat.songCategory = "All";

            tSongCategoryList.add(dummyYTCat);

            for (ModelSong.genreItem item : genreItemList) {
                TSongCategory tSongCategory = new TSongCategory();
                tSongCategory.songCategoryId = Integer.valueOf(item.genreId);
                tSongCategory.songCategory = item.genre;
                tSongCategoryList.add(tSongCategory);
            }

            ModelSongLocal.ResultGetSongCategoryList resultGetSongCategoryList = new ModelSongLocal.ResultGetSongCategoryList();
            resultGetSongCategoryList.count = tSongCategoryList.size();
            resultGetSongCategoryList.list = tSongCategoryList;
            String songCatString = gson.toJson(resultGetSongCategoryList);


            String pathSTB = Global.getUSBPath() + "/" + C.PATH_JSON_KARAOKE;

            //write song category
            String usbCategoryPath = pathSTB + "/" + C.JSON_SONG_CATEGORY;
            String tempCategoryPath = getFilesDir() + "/" + C.JSON_SONG_CATEGORY;
            boolean resultWriteYTCategory = Util.writeJsonToFile(tempCategoryPath, songCatString);
            if (resultWriteYTCategory)
                RootUtil.moveFile(tempCategoryPath, usbCategoryPath);

        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }

    }

    void getMessage() {
        String TAG = CronService.TAG + "-getMessage";
        String mac = Util.getMACAddress("eth0");
        mac = mac.replace(":", "");
        ModelMessage.getMessageAll(mac, new ModelMessage.IModelMessage() {
            @Override
            public void onGetAll(ModelMessage.messageListItem result) {
                try {
                    List<ModelMessage.messageItem> messageItems = result.messageItems;
                    //db message dibuat pemanggilan seperti ini untuk menghindari leaked;
                    //dan harus satu per satu di insert agar, tidak replace status message yang sudah dibaca
                    dbMessage = DbMessage.Instance.create(getApplicationContext());
                    for (ModelMessage.messageItem item : messageItems) {
                        insertMessageToRoom(item);
                    }
                    dbMessage.close();
                } catch (Exception ex) {
                    Debug.e(TAG, ex);
                }
            }

            @Override
            public void onError(ANError e) {
                Debug.e(TAG, e);
            }
        });

    }

    void insertMessageToRoom(ModelMessage.messageItem messageItem) {
        Async.run(performAsync ->
        {
            int result = 0;
            try {
                TMessage tMessage = new TMessage();
                tMessage.messageId = Integer.valueOf(messageItem.messageId);
                tMessage.message = messageItem.message;
                tMessage.title = messageItem.title;
                tMessage.from = messageItem.from;
                tMessage.createDate = messageItem.createdDate;
                tMessage.status = C.MESSAGE_STATUS_NEW;
                tMessage.urlImage = messageItem.urlImage;
                tMessage.urlVideo = messageItem.urlVideo;
                dbMessage.daoAccessMessage().insert(tMessage);
//                dbSong.close();
                result = 1;

            } catch (Exception ex) {

            }

            return result;
        }).setCallbackResult(result ->
        {
            int r = (int) result;
            if (r == 1) {
                Global.setUnReadMessage(true);
            }
        });
    }


    public static void registerFinishDownloadReceiver(Context context, BroadcastReceiver br) {
        try {
            IntentFilter intFilt = new IntentFilter(C.MAIN_ACTIVITY_FINISH_DOWNLOAD);
            context.registerReceiver(br, intFilt);
        } catch (Exception e) {
        }
    }

    public static void unregisterFinishDownloadReceiver(Context context, BroadcastReceiver br) {
        try {
            context.unregisterReceiver(br);
        } catch (Exception e) {
        }
    }


    /**
     * untuk send bc sudah selesai download
     */
    void sendBcDownloadFinish(boolean result) {
        try {
            if (result) {
                Intent sendBC = new Intent();
                sendBC.setAction(C.MAIN_ACTIVITY_FINISH_DOWNLOAD);
                sendBroadcast(sendBC);
            }
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    /**
     * broadcast receiver untuk menerima result connected to EKMA
     */
    private final BroadcastReceiver bc = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            try {
                Log.d(TAG, "Connected To EKMA");
//                MgmtService.unregisterEKMAConnectedReceiver(context, bc);
                Bundle bundle = intent.getExtras();
                if (bundle != null) {
                    boolean result = bundle.getBoolean(MgmtService.KEY_CONNECTED);
                    if (result) {
                        EKMAConnectedActivity.startActivity(getApplicationContext());
                    } else {
                        SplashScreenActivity.startActivity(getApplicationContext());
                    }
                }
            } catch (Exception ex) {
                Debug.e(TAG, ex);
            }
        }
    };
}

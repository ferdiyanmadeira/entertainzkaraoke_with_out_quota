package com.madeira.entertainz.karaoke.DBLocal;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class JoinYoutubePlaylistDt implements Serializable
{
    @SerializedName("playlistId")
    public int playlistId;
    @SerializedName("playlistName")
    public String playlistName;
    @SerializedName("sumOfSong")
    public int sumOfSong;
    @SerializedName("sumOfDuration")
    public int sumOfDuration;


}

package com.madeira.entertainz.karaoke.DBLocal;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Database;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

import java.util.List;


@Database(entities = {TElement.class, TElementMedia.class, TSettingElementMedia.class, TSticker.class, TLocalFileAPK.class, TSTBInfo.class, TSongItemServer.class, TRecentSearchSong.class},
        version = 34, exportSchema = false)

public abstract class DbAccess extends RoomDatabase {
    public abstract DaoAccessTheme daoAccessTheme();

    public abstract DaoAccessThemeMedia daoAccessThemeMedia();

    public abstract DaoAccessSticker daoAccessSticker();

    public abstract DaoAccessTLocalFileAPK daoAccessTLocalFileAPK();

    public abstract DaoAccessTSTBInfo daoAccessTSTBInfo();

    public abstract DaoAccessTSongItemServer daoAccessTSongItemServer();

    public static class Instance {
        private static final String DB_THEME = "EntertainzKaraoke.db";

        public static DbAccess create(Context context) {
            return Room.databaseBuilder(context, DbAccess.class, DB_THEME)
                    .fallbackToDestructiveMigration()
//                    .addMigrations(MIGRATION_1_2)
                    .build();
        }

        //example migration jangan digunakan dulu, masih tahap research
      /*  static final Migration MIGRATION_1_2 = new Migration(1, 2) {
            @Override
            public void migrate(SupportSQLiteDatabase database) {
                // Your migration strategy here
                database.execSQL("ALTER TABLE Users ADD COLUMN user_score INTEGER");
            }
        };*/
    }

    @Dao
    public interface DaoAccessTheme {

        @Insert(onConflict = OnConflictStrategy.REPLACE)
        void insert(TElement element);

        @Insert
        void insertAll(List<TElement> list);

        @Query("SELECT * FROM telement")
        List<TElement> getAll();

        @Query("DELETE FROM telement")
        void deleteAll();

    }

    @Dao
    public interface DaoAccessThemeMedia {

        @Insert(onConflict = OnConflictStrategy.REPLACE)
        void insert(TElementMedia elementMedia);

        @Insert(onConflict = OnConflictStrategy.REPLACE)
        void insertAll(List<TElementMedia> list);

        /*@Query("SELECT * FROM telement_media")
        List<TElementMedia> getAll();*/

        @Query("SELECT count(*) FROM telement_media where elementMediaId=:prmElementMediaId")
        int checkExist(int prmElementMediaId);

        @Query("DELETE FROM telement_media")
        void deleteAll();

        @Query("UPDATE telement_media SET urlImage=:prmUrlImage where elementMediaId=:elementMediaId")
        void updateElementMedia(String prmUrlImage, int elementMediaId);

      /*  @Query("UPDATE telement_media SET duration=:prmDuration where elementMediaId=:elementMediaId")
        void updateAllDuration(int prmDuration, int elementMediaId);*/

        @Insert(onConflict = OnConflictStrategy.REPLACE)
        void insertAllSetting(List<TSettingElementMedia> list);

        @Insert(onConflict = OnConflictStrategy.REPLACE)
        void insertSetting(TSettingElementMedia tSettingElementMedia);

        @Query("SELECT * FROM tsetting_element_media")
        List<TSettingElementMedia> getAllSetting();

        @Query("SELECT a.elementMediaId,b.elementId,b.urlImage,a.duration,a.isActive FROM tsetting_element_media a left join telement_media b on a.elementMediaId=b.elementMediaId where a.isActive=1")
        List<JoinSettingElementMedia> joinSettingElementMediaActive();

        @Query("SELECT a.elementMediaId,b.elementId,b.urlImage,a.duration,a.isActive FROM tsetting_element_media a left join telement_media b on a.elementMediaId=b.elementMediaId")
        List<JoinSettingElementMedia> joinSettingElementMedia();


        @Query("SELECT count(*) FROM tsetting_element_media where elementMediaId=:prmElementMediaId")
        int checkExistSetting(int prmElementMediaId);

        @Query("UPDATE tsetting_element_media SET isActive=:isActive where elementMediaId=:elementMediaId")
        void updateSettingMedia(int elementMediaId, int isActive);

        @Query("UPDATE tsetting_element_media SET duration=:duration")
        void updateAllDuration(int duration);
    }

    @Dao
    public interface DaoAccessSticker {

        @Insert(onConflict = OnConflictStrategy.REPLACE)
        void insert(TSticker sticker);

        @Insert
        void insertAll(List<TSticker> list);

        @Query("SELECT * FROM tsticker")
        List<TSticker> getAll();

        @Query("DELETE FROM tsticker")
        void deleteAll();

        @Query("SELECT * FROM tsticker WHERE stickerId=:stickerId")
        TSticker getStickerById(int stickerId);
    }

    @Dao
    public interface DaoAccessTLocalFileAPK {

        @Insert(onConflict = OnConflictStrategy.REPLACE)
        void insertAll(List<TLocalFileAPK> list);

        @Insert(onConflict = OnConflictStrategy.REPLACE)
        void insert(TLocalFileAPK tLocalFileAPK);

        @Query("SELECT * FROM tlocalfile_apk")
        List<TLocalFileAPK> getAll();

        @Query("DELETE FROM tlocalfile_apk")
        void deleteAll();


    }

    @Dao
    public interface DaoAccessTSTBInfo {

        @Insert(onConflict = OnConflictStrategy.FAIL)
        void insert(TSTBInfo TSTBInfo);

        @Query("SELECT * FROM TSTBInfo")
        List<TSTBInfo> getAll();

        @Query("DELETE FROM TSTBInfo")
        void deleteAll();

        @Query("SELECT count(*) FROM TSTBInfo where id=:id")
        int existSong(int id);

       /* @Query("update TSTBInfo set totalSong=:totalSong where id=:id")
        int updateQuota(String totalSong, int id);*/


    }

   // untuk menyimpan lagu yang akan di download dan di resume, ketika device mendadak restart atau semacam nya ketika download
    @Dao
    public interface DaoAccessTSongItemServer {

        @Insert(onConflict = OnConflictStrategy.REPLACE)
        void insertAll(List<TSongItemServer> list);

        @Insert(onConflict = OnConflictStrategy.REPLACE)
        void insert(TSongItemServer TSongItemServer);

        @Query("DELETE FROM tsong_server")
        void deleteAll();

        @Query("DELETE FROM tsong_server where songId=:songId")
        void delete(String songId);

        @Query("SELECT count(*) FROM tsong_server")
        int existSong();

        @Query("SELECT * FROM tsong_server")
        List<TSongItemServer> getAll();

    }


}
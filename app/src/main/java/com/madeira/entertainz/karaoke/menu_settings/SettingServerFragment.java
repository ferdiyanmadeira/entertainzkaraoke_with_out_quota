package com.madeira.entertainz.karaoke.menu_settings;


import android.content.res.ColorStateList;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.madeira.entertainz.karaoke.Debug;
import com.madeira.entertainz.karaoke.R;
import com.madeira.entertainz.karaoke.config.C;
import com.madeira.entertainz.karaoke.config.Global;

import java.util.Date;

/**
 * A simple {@link Fragment} subclass.
 */
public class SettingServerFragment extends Fragment implements SettingsTheme.ISettingsTheme {

    String TAG = "SettingServerFragment";

    View root;
    RelativeLayout rootLL;
    //    EditText companyNameET;
    SettingsTheme settingsTheme;
    Button resetButton;
    EditText serverEditText;

    public SettingServerFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        root = inflater.inflate(R.layout.fragment_setting_server, container, false);
        try {
            bind();
            settingsTheme = new SettingsTheme(getActivity(), this);
            settingsTheme.setTheme();
            setActionObject();
            serverEditText.setText(Global.getApiHostname());

        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
        return root;
    }

    @Override
    public void setThemeUpdateDate(Date lastUpdateTheme) {

    }

    @Override
    public void setListColorTheme(int color) {
        try {
            rootLL.setBackgroundResource(R.drawable.tags_rounded_corners);

            GradientDrawable categoryDrawable = (GradientDrawable) rootLL.getBackground();
            categoryDrawable.setColor(color);
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    @Override
    public void setTextAndSelectionColorTheme(ColorStateList colorSelection, ColorStateList colorText, int colorTextSelected, int colorTextFocus, int colorTextNormal, int colorSelectionSelected, int colorSelectionFocus) {

    }

    void bind() {
        try {
            rootLL = root.findViewById(R.id.rootLL);
            resetButton = root.findViewById(R.id.reset_button);
            serverEditText = root.findViewById(R.id.serverEditText);
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }


    void setActionObject() {
        try {
            resetButton.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View v, boolean hasFocus) {
                    if(hasFocus)
                    {
                        resetButton.setTextColor(getContext().getColor(R.color.black));
                    }
                    else
                    {
                        resetButton.setTextColor(getContext().getColor(R.color.white));
                    }
                }
            });

            resetButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        serverEditText.setText(C.DEFAULT_API);
                        Global.setApiHostname(C.DEFAULT_API);
                    } catch (Exception ex) {
                        Debug.e(TAG, ex);
                    }
                }
            });

            serverEditText.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {
                    Global.setApiHostname(String.valueOf(serverEditText.getText()));
                }
            });

        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }


}

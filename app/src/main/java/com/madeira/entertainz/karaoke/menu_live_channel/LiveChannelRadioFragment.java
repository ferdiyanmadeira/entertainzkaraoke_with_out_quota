package com.madeira.entertainz.karaoke.menu_live_channel;


import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.ColorSpace;
import android.graphics.drawable.GradientDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.chibde.visualizer.LineBarVisualizer;
import com.google.android.exoplayer2.DefaultLoadControl;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.ExoPlayer;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.LoadControl;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.extractor.DefaultExtractorsFactory;
import com.google.android.exoplayer2.extractor.ExtractorsFactory;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.trackselection.AdaptiveVideoTrackSelection;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelection;
import com.google.android.exoplayer2.trackselection.TrackSelector;
import com.google.android.exoplayer2.upstream.BandwidthMeter;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.util.Util;
import com.madeira.entertainz.karaoke.DBLocal.DbAccess;
import com.madeira.entertainz.karaoke.DBLocal.TRadio;
import com.madeira.entertainz.karaoke.Debug;
import com.madeira.entertainz.karaoke.R;
import com.madeira.entertainz.karaoke.model_online.ModelRadio;
import com.madeira.entertainz.library.PerformAsync2;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.Date;


public class LiveChannelRadioFragment extends Fragment implements LiveChannelTheme.ILiveChannelTheme {
    String TAG = "LiveChannelRadioFragment";
    View root;
    LineBarVisualizer lineBarVisualizer;
    SimpleExoPlayer exoPlayer;
    LiveChannelTheme LiveChannelTheme;
    View layoutDetail;
    ColorStateList colorSelection, colorText;
    ImageView logoImageView;
    TextView radioNameTV;
    TextView radioDescTV;
    TextView notStreamingTV;

    ImageView prevImageView;
    ImageView playPauseImageView;
    ImageView nextButton;
    ModelRadio.subRadioItem tRadio = new ModelRadio.subRadioItem();
    boolean isPlaying = false;

    public static final String KEY_RADIO = "KEY_CHANNEL";


    int audioSessionid = 0;

    Runnable run;
    Handler handler;

    private IRadioDetailListener iRadioDetailListener;

    public LiveChannelRadioFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            try {
                tRadio = (ModelRadio.subRadioItem) getArguments().getSerializable(KEY_RADIO);
            }catch (Exception ex)
            {
                Debug.e(TAG, ex);
            }

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        root = inflater.inflate(R.layout.fragment_live_channel_radio, container, false);
        try {

            bind();
            LiveChannelTheme = new LiveChannelTheme(getActivity(), this);
            LiveChannelTheme.setTheme();
            setActionObject();
//            fetchRadio();
            showDetail();
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
        return root;
    }

    void bind() {
        lineBarVisualizer = root.findViewById(R.id.visualizer);
        layoutDetail = root.findViewById(R.id.layout_detail);
        logoImageView = root.findViewById(R.id.logoImageView);
        radioNameTV = root.findViewById(R.id.radioNameTV);
        radioDescTV = root.findViewById(R.id.radioDescTV);
        prevImageView = root.findViewById(R.id.prevImageView);
        playPauseImageView = root.findViewById(R.id.playPauseImageView);
        nextButton = root.findViewById(R.id.nextButton);
        notStreamingTV = root.findViewById(R.id.notStreamingTV);
        playPauseImageView.setImageResource(R.drawable.selector_radio_play);
    }

    void setActionObject() {
        try {
           /* prevImageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    iRadioDetailListener.onPrev(radioId, position);

                }
            });*/

            playPauseImageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (exoPlayer != null) {
                        if (isPlaying) {
                            exoPlayer.setPlayWhenReady(false);
                            exoPlayer.getPlaybackState();
                        } else {
                            exoPlayer.setPlayWhenReady(true);
                            exoPlayer.getPlaybackState();
                        }
                    }
                }
            });

          /*  nextButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    iRadioDetailListener.onNext(radioId, position);

                }
            });*/
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    void setExoPlayer(String url) {
        try {
            BandwidthMeter bandwidthMeter = new DefaultBandwidthMeter();
            final ExtractorsFactory extractorsFactory = new DefaultExtractorsFactory();
            TrackSelection.Factory trackSelectionFactory = new AdaptiveVideoTrackSelection.Factory(bandwidthMeter);
//        DataSource.Factory dateSourceFactory = new DefaultDataSourceFactory(this, Util.getUserAgent(context, getPackageName()), bandwidthMeter);
            DataSource.Factory dataSourceFactory = new DefaultDataSourceFactory(getContext(), Util.getUserAgent(getContext(), getActivity().getApplicationContext().getPackageName()));
            TrackSelector trackSelector = new DefaultTrackSelector(new Handler());
            LoadControl loadControl = new DefaultLoadControl();

            MediaSource mediaSource = new ExtractorMediaSource(Uri.parse(url), dataSourceFactory, extractorsFactory, new Handler(), Throwable::printStackTrace);    // replace Uri with your song url
            exoPlayer = ExoPlayerFactory.newSimpleInstance(getActivity(), trackSelector, loadControl);
            exoPlayer.prepare(mediaSource);
            exoPlayer.setPlayWhenReady(true);
            exoPlayer.addListener(new ExoPlayer.EventListener() {
                @Override
                public void onLoadingChanged(boolean isLoading) {

                }

                @Override
                public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
                    try {
                        if (playWhenReady && playbackState == ExoPlayer.STATE_READY) {
                            // media actually playing
                            isPlaying = true;
                            playPauseImageView.setImageResource(R.drawable.selector_radio_pause);

                            notStreamingTV.setVisibility(View.GONE);
                        } else if (playWhenReady) {
                            // might be idle (plays after prepare()),
                            // buffering (plays when data available)
                            // or ended (plays when seek away from end)
                            isPlaying = false;
                            notStreamingTV.setVisibility(View.VISIBLE);
                            playPauseImageView.setImageResource(R.drawable.selector_radio_play);
                        } else {
                            isPlaying = false;
                            playPauseImageView.setImageResource(R.drawable.selector_radio_play);
                        }
                    } catch (Exception ex) {
                        Log.d(TAG, ex.getMessage());
                    }
                }

                @Override
                public void onTimelineChanged(Timeline timeline, Object manifest) {

                }

                @Override
                public void onPlayerError(ExoPlaybackException error) {

                }

                @Override
                public void onPositionDiscontinuity() {

                }
            });
            handler = new Handler();
            run = new Runnable() {
                @Override
                public void run() {
                    try {
                        if (isPlaying == true && audioSessionid == 0) {
                            audioSessionid = exoPlayer.getAudioSessionId();
                            if (audioSessionid != 0) {
                                lineBarVisualizer.setColor(ContextCompat.getColor(getActivity(), R.color.colorSoyMilk));
                                lineBarVisualizer.setDensity(70);
                                lineBarVisualizer.setPlayer(audioSessionid);
                            }
                        } else {
                            handler.postDelayed(run, 1000);
                        }
                    }catch (Exception ex){
                        Debug.e(TAG,ex);
                    }
                }
            };
            handler.postDelayed(run, 1000);

        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    public void releaseExoPlayer() {
        try {
            if (exoPlayer != null)
                exoPlayer.release();
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    void showDetail()
    {
        try {
            //show detail
//            Uri uri = Uri.fromFile(new File(subChannelItem.urlImage));
//            logoImageView.setImageURI(uri);
            Picasso.with(getActivity())
                    .load(tRadio.urlImage)
                    .fit()
                    .into(logoImageView);
            radioNameTV.setText(tRadio.radioName);
            radioDescTV.setText(tRadio.description);
            setExoPlayer(tRadio.urlRadio);
        }catch (Exception ex)
        {
            Debug.e(TAG, ex);
        }
    }

    void fetchRadio() {

       /* try {
            Context mContext = getContext();
            PerformAsync2.run(performAsync -> {
                int result = 0;
                try {
                    DbAccess dbAccess = DbAccess.Instance.create(mContext);
                    if (radioId != 0)
                        subChannelItem = dbAccess.daoAccessTRadio().getRadioById(radioId);
                    else {
                        TRadio firtRow = dbAccess.daoAccessTRadio().getFirstRowRadio();
                        subChannelItem = firtRow;
                    }
                    dbAccess.close();
                    result = 1;
                } catch (Exception ex) {

                }
                return result;
            }).setCallbackResult(result -> {
                int i = (int) result;
                if (i != 1) {
                    return;
                } else {
                    //show detail
                    Uri uri = Uri.fromFile(new File(subChannelItem.urlImage));
                    logoImageView.setImageURI(uri);
                    radioNameTV.setText(subChannelItem.radioName);
                    radioDescTV.setText(subChannelItem.description);
                    setExoPlayer(subChannelItem.urlRadio);
                }
            });

        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }*/
    }

    @Override
    public void onDetach() {
        super.onDetach();
        releaseExoPlayer();
    }

    @Override
    public void setThemeUpdateDate(Date lastUpdateTheme) {

    }

    @Override
    public void setListColorTheme(int color) {
        try {
            layoutDetail.setBackgroundResource(R.drawable.tags_rounded_corners);
            GradientDrawable categoryDrawable = (GradientDrawable) layoutDetail.getBackground();
            categoryDrawable.setColor(color);
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    @Override
    public void setTextAndSelectionColorTheme(ColorStateList colorSelection, ColorStateList colorText, int colorTextSelected, int colorTextFocus, int colorTextNormal, int colorSelectionSelected, int colorSelectionFocus) {
        try {
            this.colorSelection = colorSelection;
            this.colorText = colorText;
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    public interface IRadioDetailListener {
        void onPrev(int prmRadioId, int position);

        void onNext(int prmRadioId, int position);
    }

    public void setCallback(IRadioDetailListener prmListener) {
        this.iRadioDetailListener = prmListener;
    }



    @Override
    public void onStop() {
        super.onStop();
        try {
            releaseExoPlayer();
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }


}

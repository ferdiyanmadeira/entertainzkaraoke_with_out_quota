package com.madeira.entertainz.karaoke.menu_update_song;

import android.content.res.ColorStateList;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.os.StatFs;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.ScaleAnimation;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import com.androidnetworking.error.ANError;
import com.madeira.entertainz.helper.RecyclerViewAdapter;
import com.madeira.entertainz.karaoke.DBLocal.JoinSongCountPlayed;
import com.madeira.entertainz.karaoke.DBLocal.TSong;
import com.madeira.entertainz.karaoke.DBLocal.TSongItemServer;
import com.madeira.entertainz.karaoke.Debug;
import com.madeira.entertainz.karaoke.R;
import com.madeira.entertainz.karaoke.config.CacheData;
import com.madeira.entertainz.karaoke.model_online.ModelSong;
import com.madeira.entertainz.library.StorageUtils;
import com.madeira.entertainz.library.Util;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Date;
import java.util.Hashtable;
import java.util.List;

import es.dmoral.toasty.Toasty;


public class UpdateSongListFragment extends Fragment implements UpdateSongTheme.IUpdateSongTheme {
    static String TAG = "UpdateSongListFragment";

    public static final String KEY_GENRE_ID = "GENRE_ID";
    private int genreId;
    List<TSong> tSongListLocal = new ArrayList<>();
    Hashtable<String, TSong> hashTSongList = new Hashtable<String, TSong>();
    private boolean isLoading;
    List<TSongItemServer> tSongList = new ArrayList<>();
    List<TSongItemServer> songItemByCategory = new ArrayList<>();

    UpdateSongTheme karaokeTheme;
    RecyclerViewAdapter adapter;

    View root;
    LinearLayout rootLL;
    RecyclerView recyclerView;
    LinearLayout searchLL;
    EditText searchET;
    TextView pressBackTV;
    TextView usedSpaceTV;
    TextView capacitySpaceTV;
    LinearLayout usbInfoLL;
    SeekBar storageSeekBar;
    LinearLayout infoLL;
    TextView selectedSongTV;

    long megTotal;
    long megAvailable;
    long megUsed;
    int selectedSong = 0;

    IUpdateSongListener mListener;

    public interface IUpdateSongListener {
        void onUpdate(TSongItemServer songItem, boolean fgAdd);
    }

    public UpdateSongListFragment() {
        // Required empty public constructor
    }


    public static UpdateSongListFragment newInstance() {
        UpdateSongListFragment fragment = new UpdateSongListFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        String TAG = UpdateSongListFragment.TAG + "-" + "onCreate";
        super.onCreate(savedInstanceState);
        try {
            if (getArguments() != null) {
                genreId = getArguments().getInt(KEY_GENRE_ID);
            } else {
                genreId = 0;
            }
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        root = inflater.inflate(R.layout.fragment_updatesong_list, container, false);
        try {
            bind();
            karaokeTheme = new UpdateSongTheme(getActivity(), this);
            karaokeTheme.setTheme();
            setOnActionObject();
            getUSBInfoSize();
            fetchSongList();
            fetchSong();
//            CacheData.hashSongNameNArtis.clear();
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
        return root;
    }

    void bind() {
        try {
            rootLL = root.findViewById(R.id.rootLL);
            recyclerView = root.findViewById(R.id.recyclerView);
            searchLL = root.findViewById(R.id.searchLL);
            searchET = root.findViewById(R.id.searchET);
            pressBackTV = root.findViewById(R.id.pressBackTV);
            usedSpaceTV = root.findViewById(R.id.used_space_tv);
            capacitySpaceTV = root.findViewById(R.id.capacitySpaceTV);
            usbInfoLL = root.findViewById(R.id.usbInfoLL);
            storageSeekBar = root.findViewById(R.id.storageSeekBar);
            infoLL = root.findViewById(R.id.infoLL);
            selectedSongTV = root.findViewById(R.id.selected_song_tv);
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    @Override
    public void setThemeUpdateDate(Date lastUpdateTheme) {

    }

    @Override
    public void setListColorTheme(int color) {
        try {
            //untuk set round corner dan color
            rootLL.setBackgroundResource(R.drawable.tags_rounded_corners);
            GradientDrawable rootDrawable = (GradientDrawable) rootLL.getBackground();
            rootDrawable.setColor(color);

            searchLL.setBackgroundResource(R.drawable.tags_rounded_corners);
            GradientDrawable searchDrawable = (GradientDrawable) searchLL.getBackground();
            searchDrawable.setColor(color);

            pressBackTV.setBackgroundResource(R.drawable.tags_rounded_corners);
//            GradientDrawable pressbackDrawable = (GradientDrawable) pressBackTV.getBackground();
//            pressbackDrawable.setColor(color);

            usbInfoLL.setBackgroundResource(R.drawable.tags_rounded_corners);
            GradientDrawable usbInfoDrawable = (GradientDrawable) usbInfoLL.getBackground();
            usbInfoDrawable.setColor(color);

          /*  infoLL.setBackgroundResource(R.drawable.tags_rounded_corners);
            GradientDrawable infoDrawable = (GradientDrawable) infoLL.getBackground();
            infoDrawable.setColor(color);*/
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    @Override
    public void setTextAndSelectionColorTheme(ColorStateList colorSelection, ColorStateList colorText, int colorTextSelected, int colorTextFocus, int colorTextNormal, int colorSelectionSelected, int colorSelectionFocus) {
        try {
            this.colorSelection = colorSelection;
            this.colorText = colorText;
            this.colorTextNormal = colorTextNormal;

//        songCategoryTV.setTextColor(colorTextNormal);
//        searchET.setHintTextColor(colorTextNormal);
            searchET.setTextColor(colorTextNormal);
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }


    /**
     * untuk get song yang berada di server
     */
    void fetchSong() {
        String TAG = UpdateSongListFragment.TAG + "-" + "fetchSong";
        try {
            String mac = Util.getMACAddress("eth0");
            mac = mac.replace(":", "");
            ModelSong.getAllSong(mac, new ModelSong.IGetAllSong() {
                @Override
                public void onGetAllSong(ModelSong.allSongItem result) {
                    if (result != null) {
                        tSongList = result.listSong;
                        //untuk validasi lagu sudah terpilih atau belum
                        fetchSongByGenre();
                    }
                }

                @Override
                public void onError(ANError e) {
                    String test;
                }
            });
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    /**
     * get song list dari server berdasarkan category
     */
    void fetchSongByGenre() {
        try {
            if (genreId == 0) {
                songItemByCategory = tSongList;
            } else {
                songItemByCategory = new ArrayList<>();
                for (TSongItemServer item : tSongList) {
                    if (Integer.valueOf(item.genreId) == genreId) {
                        songItemByCategory.add(item);
                    }
                }
            }

            setRecyclerView(songItemByCategory);
        }catch (Exception ex)
        {
            Debug.e(TAG, ex);
        }
    }

    public void setSong(List<TSongItemServer> tSongList) {
        try {
            this.tSongList = tSongList;
        }catch (Exception ex)
        {
            Debug.e(TAG, ex);
        }

    }

    void setRecyclerView(List<TSongItemServer> songItemList) {
        try {
            adapter = new RecyclerViewAdapter(new RecyclerViewAdapter.IRecyclerViewAdapter() {
                @Override
                public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
                    View view = LayoutInflater.from(getActivity()).inflate(R.layout.cell_updatesong_list, parent, false);
                    ItemViewHolder item = new ItemViewHolder(view);
                    return item;
                }

                @Override
                public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
                    try {
                        ItemViewHolder item = (ItemViewHolder) holder;
                        item.bind(songItemList.get(position), position);
                    } catch (Exception ex) {
                        Debug.e(TAG, ex);
                    }
                }

                @Override
                public int getItemCount() {
                    int count = 0;
                    if (songItemList != null)
                        count = songItemList.size();
                    return count;
                }

            });

            GridLayoutManager gridLayoutManager = new GridLayoutManager(getActivity(), 5);
            adapter.setHasStableIds(true);
            recyclerView.setHasFixedSize(true);
            recyclerView.setLayoutManager(gridLayoutManager);
            recyclerView.setAdapter(adapter);
            recyclerView.invalidate();
            adapter.notifyDataSetChanged();

        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    class ItemViewHolder extends RecyclerView.ViewHolder {
        private static final String TAG = "ItemViewHolder";

        View root;
        TextView tvTitle;
        TextView tvSinger;
        ImageView thumbnailIV;
        ImageView isExistIV;
        ImageView downloadedIV;

        public ItemViewHolder(View itemView) {
            super(itemView);
            root = itemView;
            selectedView = root;
            tvTitle = itemView.findViewById(R.id.text_title);
            tvSinger = itemView.findViewById(R.id.text_singer);
            thumbnailIV = itemView.findViewById(R.id.thumbnailIV);
            isExistIV = itemView.findViewById(R.id.isExistIV);
            downloadedIV = itemView.findViewById(R.id.downloaded_iv);
        }

        public void bind(TSongItemServer songItem, int pos) {
           /* PerformAsync2.run(performAsync ->
            {
                TSong tSong = new TSong();
                try {
                    DbSong dbSong = DbSong.Instance.create(getActivity());
                    tSong = dbSong.daoAccessTSong().getSongById(Integer.valueOf(songItem.songId));
                } catch (Exception ex) {

                }
                return tSong;
            }).setCallbackResult(result ->
            {*/


//                TSong tSong = (TSong) result;

//                TSong tSong = hashTSongList.get(songItem.songId);
                com.madeira.entertainz.karaoke.model_room_db.ModelSong.getSongById(getActivity(), songItem.songId, new com.madeira.entertainz.karaoke.model_room_db.ModelSong.IGetSongById() {
                    @Override
                    public void onGeSongById(TSong tSong) {
                        try {
                            tvTitle.setText(songItem.songName);
                            tvSinger.setText(songItem.artist);
                            if (songItem.thumbnail != null) {
                                Picasso.with(getActivity())
                                        .load(songItem.thumbnail).placeholder(R.drawable.blank_thumbnail)
                                        .fit()
                                        .into(thumbnailIV);
                            }
                            if (CacheData.hashSongNameNArtis.containsKey(songItem.songId))
                                isExistIV.setVisibility(View.VISIBLE);
                            else
                                isExistIV.setVisibility(View.GONE);

                            if (tSong == null)
                                downloadedIV.setVisibility(View.GONE);
                            else {
                                downloadedIV.setVisibility(View.VISIBLE);
//                    CacheData.hashSongNameNArtis.put(String.valueOf(songItem.songId), songItem);
                            }

                            if (colorSelection != null) root.setBackgroundTintList(colorSelection);
//            untuk merubah warna text
                            if (colorText != null) {
                                tvSinger.setTextColor(colorText);
                                tvTitle.setTextColor(colorText);
                            }
                            root.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    try {
                                        if (downloadedIV.getVisibility() == View.GONE) {
                                            if (isExistIV.getVisibility() == View.GONE) {
                                                long fileSize = Long.valueOf(songItem.filesize);
                                                long fileSizeMb = fileSize / (1024 * 1024);
                                                long tempMegUsed = megUsed + fileSizeMb;
                                                if (megTotal > tempMegUsed) {
                                                    isExistIV.setVisibility(View.VISIBLE);
                                                    CacheData.hashSongNameNArtis.put(songItem.songId, songItem);
                                                    mListener.onUpdate(songItem, true);
                                                    megUsed = megUsed + fileSizeMb;
                                                    megAvailable = megAvailable - fileSizeMb;
                                                    storageSeekBar.setProgress((int) megUsed);
                                                    usedSpaceTV.setText(Util.thousandFormat(megUsed));
                                                    selectedSong = selectedSong + 1;
                                                    selectedSongTV.setText(String.valueOf(selectedSong));
                                                } else {
                                                    Toasty.error(getActivity(), getString(R.string.yourStorageNotEnough)).show();
                                                }
                                            } else {
                                                CacheData.hashSongNameNArtis.remove(songItem.songId);
                                                isExistIV.setVisibility(View.GONE);
                                                mListener.onUpdate(songItem, false);
                                                long fileSize = Long.valueOf(songItem.filesize);
                                                long fileSizeMb = fileSize / (1024 * 1024);

                                                megUsed = megUsed - fileSizeMb;
                                                megAvailable = megAvailable + fileSizeMb;
                                                storageSeekBar.setProgress((int) megUsed);
                                                usedSpaceTV.setText(Util.thousandFormat(megUsed));
                                                selectedSong = selectedSong - 1;
                                                selectedSongTV.setText(String.valueOf(selectedSong));
                                            }
                                        } else {
                                            Toasty.info(getActivity(), getString(R.string.song_downloaded)).show();
                                        }
                                    } catch (Exception ex) {
                                        Debug.e(TAG, ex);
                                    }
                                }
                            });
                            root.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                                @Override
                                public void onFocusChange(View v, boolean hasFocus) {
                                    if (hasFocus) {
//                                tvTitle.setSelected(true);
//                                tvSinger.setSelected(true);
                                        updateSelectedViewUi(v, true);
                                    } else {
                                        updateSelectedViewUi(null, false);
//                                tvTitle.setSelected(false);
//                                tvSinger.setSelected(false);
                                    }
                                }
                            });
                        } catch (Exception ex) {
                            Debug.e(TAG, ex);
                        }
                    }
                });

//            });


        }

        void selectView(View v) {
            //clear selection
            /*if (selectedView != null) {
                selectedView.setSelected(false);
            }

            //make selection
            selectedView = v;
            selectedView.setSelected(true);*/
        }
    }

    ColorStateList colorSelection, colorText;
    int colorTextNormal;

    //function untuk declare semua action dari object di activity
    void setOnActionObject() {
        try {
            searchET.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {


                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {

                    searchSong(searchET.getText().toString());
                }
            });


            searchET.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View v, boolean hasFocus) {
                    if (!hasFocus)
                        searchET.setHintTextColor(getActivity().getResources().getColor(R.color.texthintcolor_unfocus));
                    else
                        searchET.setHintTextColor(colorTextNormal);

                }
            });
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    void searchSong(String prmKeyword) {
        try {
            List<TSongItemServer> searchingSongList = new ArrayList<>();

            //by artist
            for (TSongItemServer item : songItemByCategory) {
                if (genreId == 0) {
                    if (item.artist.toLowerCase().contains(prmKeyword.toLowerCase())) {
                        searchingSongList.add(item);
                    }
                } else {
                    if (item.artist.toLowerCase().contains(prmKeyword.toLowerCase()) && item.genreId.equals(String.valueOf(genreId))) {
                        searchingSongList.add(item);
                    }
                }

            }
            //by song name
            for (TSongItemServer item : songItemByCategory) {
                if (genreId == 0) {
                    if (item.songName.toLowerCase().contains(prmKeyword.toLowerCase())) {
                        searchingSongList.add(item);
                    }
                } else {
                    if (item.songName.toLowerCase().contains(prmKeyword.toLowerCase()) && item.genreId.equals(String.valueOf(genreId))) {
                        searchingSongList.add(item);
                    }
                }

            }

            if (prmKeyword.equals(""))
                setRecyclerView(songItemByCategory);
            else
                setRecyclerView(searchingSongList);
//        setRecyclerViewCustomAdapter(searchingSongList);
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }

    }

    void updateSelectedViewUi(View view, boolean hasFocus) {
        try {
            if (selectedView != null) {
                scaleView(selectedView, 1.1f, 1);
                ViewCompat.setTranslationZ(selectedView, 0);
            }

            if (hasFocus) {
                selectedView = view;
                ViewCompat.setTranslationZ(selectedView, 100);
//                selectedView.setBackgroundResource(R.drawable.box_white);
                scaleView(selectedView, 1, 1.1f);
            } else {
                //remove selected & focus dari grid
                selectedView = null;
            }
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    public void scaleView(View v, float startScale, float endScale) {
        try {
            Animation anim = new ScaleAnimation(
                    startScale, endScale, // Start and end values for the X axis scaling
                    startScale, endScale, // Start and end values for the Y axis scaling
                    Animation.RELATIVE_TO_SELF, 0.5f, // Pivot point of X scaling
                    Animation.RELATIVE_TO_SELF, 0.5f); // Pivot point of Y scaling
            anim.setFillAfter(true); // Needed to keep the result of the animation
            anim.setDuration(100);
            v.startAnimation(anim);
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    static View selectedView;


    void getUSBInfoSize() {
        String TAG = UpdateSongListFragment.TAG + "-" + "getUSBInfoSize";
        try {
            String usbPath = "";

            List<StorageUtils.StorageInfo> storageInfoList = StorageUtils.getStorageList();
            for (StorageUtils.StorageInfo storageInfo : storageInfoList) {
                if (storageInfo.removable) {
                    String[] split = storageInfo.path.split("/");
                    usbPath = "/storage/" + split[split.length - 1];
                }
            }
            StatFs statFs = new StatFs(usbPath);
            long blockSize = statFs.getBlockSize();
            long totalSize = statFs.getBlockCount() * blockSize;
            long availableSize = statFs.getAvailableBlocks() * blockSize;
//            long freeSize = statFs.getFreeBlocks() * blockSize;
            long usedSize = totalSize - availableSize;

            megTotal = totalSize / (1024 * 1024);
            megAvailable = availableSize / (1024 * 1024);
            megUsed = usedSize / (1024 * 1024);
            usedSpaceTV.setText(Util.thousandFormat(megUsed));
            capacitySpaceTV.setText(Util.thousandFormat(megTotal));
            storageSeekBar.setMax((int) megTotal);
            storageSeekBar.setProgress((int) megUsed);
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    public void setCallBack(IUpdateSongListener listener) {
        mListener = listener;
    }

    /**
     * untuk get semua song list, dan untuk mengetahui song ini sudah beraada di song list atau tidak
     */
    void fetchSongList() {
        com.madeira.entertainz.karaoke.model_room_db.ModelSong.getJoinSong(getActivity(), new com.madeira.entertainz.karaoke.model_room_db.ModelSong.IGetListJoinSong() {
            @Override
            public void onGetListJoinSong(List<JoinSongCountPlayed> tSongList) {
                try {
                    tSongListLocal = (ArrayList)tSongList;
                    // untuk cache ke hashtable agar mempermudah validasi song tersebut sudah ada di song list atau tidak
                    if (tSongListLocal != null) {
                        for (TSong tSong : tSongListLocal) {
                            hashTSongList.put(tSong.songId, tSong);
                        }
                    }
                } catch (Exception ex) {
                    Debug.e(TAG, ex);
                }
            }
        });
        /*PerformAsync2.run(performAsync ->
        {
            List<TSong> result = new ArrayList<>();
            try {
                DbSong dbSong = DbSong.Instance.create(getActivity());
                result = dbSong.daoAccessTSong().getAllTSong();
                dbSong.close();
            } catch (Exception ex) {

            }
            return result;
        }).setCallbackResult(result ->
        {
            try {
                tSongListLocal = (List<TSong>) result;
                // untuk cache ke hashtable agar mempermudah validasi song tersebut sudah ada di song list atau tidak
                if (tSongListLocal != null) {
                    for (TSong tSong : tSongListLocal) {
                        hashTSongList.put(tSong.songId, tSong);
                    }
                    *//*selectedSong = tSongListLocal.size();
                    selectedSongTV.setText(String.valueOf(selectedSong));*//*
                }
            } catch (Exception ex) {
                Debug.e(TAG, ex);
            }

        });*/
    }

/*    void setRecyclerViewCustomAdapter(List<TSongItemServer> songItems) {
        GridLayoutManager gridLayoutManager = new GridLayoutManager(getActivity(), 4);
        recyclerView.setLayoutManager(gridLayoutManager);

        UpdateSongViewAdapter adapter = new UpdateSongViewAdapter(
                songItems,
                getActivity(), colorSelection, colorText, iUpdateSongViewAdapter
        );
//        adapter.setMultiChoiceToolbar(multiChoiceToolbar);
        recyclerView.setAdapter(adapter);
    }*/

  /*  UpdateSongViewAdapter.IUpdateSongViewAdapter iUpdateSongViewAdapter = new UpdateSongViewAdapter.IUpdateSongViewAdapter() {
        @Override
        public void onAddItem(TSongItemServer songItem) {
            String TAG = "IUpdateSongViewAdapter-onAddItem";
            try {
            } catch (Exception ex) {
                Debug.e(TAG, ex);
            }
        }

        @Override
        public void onDeleteItem(TSongItemServer songItem) {
            String TAG = "IUpdateSongViewAdapter-onDeleteItem";
            try {
//                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
//                    addNewSong.removeIf(x -> (x.songName == songItem.songName && x.artist == songItem.artist));
//                }

            } catch (Exception ex) {
                Debug.e(TAG, ex);
            }
        }
    };*/


}

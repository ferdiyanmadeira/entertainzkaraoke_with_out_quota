package com.madeira.entertainz.karaoke.DBLocal;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

@Entity(tableName = "tnews_category")
public class TNewsCategory {

    @PrimaryKey
    @NonNull
    @SerializedName("news_category_id")
    @Expose
    public int newsCategoryId;

    @SerializedName("category")
    @Expose
    public String category;

    @Override
    public boolean equals(@Nullable Object obj) {
        if (obj==null) return false;
        if (!(obj instanceof TNewsCategory)) return false;

        TNewsCategory tNewsCategory = (TNewsCategory) obj;
        return tNewsCategory.newsCategoryId==newsCategoryId;
    }

//    static public List<TNewsCategory> mergeList(List<TNewsCategory> )
}
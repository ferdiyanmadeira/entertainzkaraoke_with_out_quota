package com.madeira.entertainz.karaoke.model;

import com.google.gson.Gson;
import com.madeira.entertainz.karaoke.DBLocal.TSTBInfo;
import com.madeira.entertainz.karaoke.model_online.ModelSTB;
import com.madeira.entertainz.library.ApiError;
import com.madeira.entertainz.library.Util;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.List;

public class ModelSTBLocal {
    static String TAG = "ModelSTBLocal";

    public static ResultQuota getQuota(String filePath) {
        ResultQuota result = null;
                String stringJSON = "";
                try {
                    File file = new File(filePath);
                    FileInputStream fileInputStream = new FileInputStream(file);
                    stringJSON = Util.getJSONFromTextFile(fileInputStream);
                    Gson gson = new Gson();

                    try {
                        ApiError.process(gson, stringJSON);
                        result = gson.fromJson(stringJSON, ResultQuota.class);
                        return result;
                    } catch (ApiError apiError) {
                        apiError.printStackTrace();
                    }


                } catch (IOException e) {
                    e.printStackTrace();
                }

        return result;
    }


    public static TSTBInfo getSTBInfo(String filePath) {
        TSTBInfo result = null;
        String stringJSON = "";
        try {
            File file = new File(filePath);
            FileInputStream fileInputStream = new FileInputStream(file);
            stringJSON = Util.getJSONFromTextFile(fileInputStream);
            Gson gson = new Gson();

            try {
                ApiError.process(gson, stringJSON);
                result = gson.fromJson(stringJSON, TSTBInfo.class);
                return result;
            } catch (ApiError apiError) {
                apiError.printStackTrace();
            }


        } catch (IOException e) {
            e.printStackTrace();
        }

        return result;
    }

    public class ResultQuota {
        public int quota;
    }


   /* class tQuota
    {
        int quota;
    }*/

}

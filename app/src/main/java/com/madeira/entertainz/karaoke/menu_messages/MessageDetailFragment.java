package com.madeira.entertainz.karaoke.menu_messages;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.FrameLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.VideoView;

//import com.madeira.components.ViewGallery;
import com.madeira.components.ViewGallery;
import com.madeira.entertainz.helper.controller.media.VideoControllerView;
import com.madeira.entertainz.karaoke.DBLocal.DbMessage;
import com.madeira.entertainz.karaoke.DBLocal.TMessage;
import com.madeira.entertainz.karaoke.DBLocal.TMessageMedia;
import com.madeira.entertainz.karaoke.Debug;
import com.madeira.entertainz.karaoke.R;
import com.madeira.entertainz.karaoke.config.C;
import com.madeira.entertainz.karaoke.config.Global;
import com.madeira.entertainz.karaoke.model_room_db.ModelMessage;
import com.madeira.entertainz.library.PerformAsync2;
import com.madeira.entertainz.library.Util;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


public class MessageDetailFragment extends Fragment implements MessagesTheme.IMessagesTheme {
    String TAG = "MessageDetailFragment";
    public static final String KEY_MESSAGE_ID = "KEY_MESSAGE_ID";

    View root;
    MessagesTheme messagesTheme;

    static MediaPlayer mediaPlayer;

    TextView messageFrom, messageDateTime, tvTitle;
    View selectionColor;
    View layoutDetail;
    ViewGallery viewGallery;
    WebView webView;
    FrameLayout videoLayout;
    VideoView videoView;
    private boolean mIsComplete;
    Date dtLastUpdate;
    List<TMessageMedia> listMedia = new ArrayList<>();
    static VideoControllerView controller;
    //    ScrollView scrollView;
    ColorStateList colorSelection, colorText;

    // TODO: Rename and change types of parameters
    private int messageId;


    public MessageDetailFragment() {
        // Required empty public constructor
    }

    public static MessageDetailFragment newInstance(String prmMessageId) {
        MessageDetailFragment fragment = new MessageDetailFragment();
        Bundle args = new Bundle();
        args.putString(KEY_MESSAGE_ID, prmMessageId);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        try {
            super.onCreate(savedInstanceState);
            if (getArguments() != null) {
                messageId = getArguments().getInt(KEY_MESSAGE_ID);
                loadMessage();
            }
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        try {
            root = inflater.inflate(R.layout.fragment_message_detail, container, false);
            bind();
            messagesTheme = new MessagesTheme(getContext(), this);
            messagesTheme.setTheme();
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
        return root;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void setThemeUpdateDate(Date lastUpdateTheme) {

    }

    @Override
    public void setListColorTheme(int color) {
        try {
            layoutDetail.setBackgroundResource(R.drawable.tags_rounded_corners);
            GradientDrawable categoryDrawable = (GradientDrawable) layoutDetail.getBackground();
            categoryDrawable.setColor(color);
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    @Override
    public void setTextAndSelectionColorTheme(ColorStateList colorSelection, ColorStateList colorText, int colorTextSelected, int colorTextFocus, int colorTextNormal, int colorSelectionSelected, int colorSelectionFocus) {
        try {
            this.colorSelection = colorSelection;
            this.colorText = colorText;
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
//        tvTitle.setTextColor(colorTextNormal);
//        messageFrom.setTextColor(colorTextNormal);
//        messageDateTime.setTextColor(colorTextNormal);
    }

    void bind() {
        try {
            messageFrom = root.findViewById(R.id.message_from);
            messageDateTime = root.findViewById(R.id.message_datetime);
            selectionColor = root.findViewById(R.id.selectioncolor);
            layoutDetail = root.findViewById(R.id.layout_detail);
            viewGallery = root.findViewById(R.id.view_gallery);
            tvTitle = root.findViewById(R.id.text_title);
            webView = root.findViewById(R.id.webView);
            videoLayout = root.findViewById(R.id.video_layout);
            videoView = root.findViewById(R.id.videoView);
//            scrollView = root.findViewById(R.id.scrollView);
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    void setupViewGallery() {

       /* try {
            viewGallery.setCallback(new ViewGallery.IViewGallery() {
                @Override
                public void onClickPrev(int newIndex) {

                }

                @Override
                public void onClickNext(int newIndex) {

                }

                @Override
                public void onClickImage(int index) {

                }

                @Override
                public void unFocused() {
                    if (!viewGallery.isFocused() && !scrollView.isFocused() && !webView.isFocused()) {
//                    layoutDetail.setBackgroundColor(colorUnSelected);
                    }
                }
            });
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }*/

    }

    @SuppressLint("SetTextI18n")
    public void setText(TMessage tMessage) {
        ModelMessage.updateMessage(getContext(), messageId, C.MESSAGE_STATUS_READ, new ModelMessage.IUpdateMessage() {
            @Override
            public void IUpdateMessage(int result) {
                try {
                    if (result == 1) {
                        messageFrom.setText(tMessage.from);
                        messageDateTime.setText(Util.sdFormatDate(Util.convertMySqlDate(tMessage.createDate)));
                        String message = tMessage.message;
                        webView.getSettings().setJavaScriptEnabled(true);
                        webView.setBackgroundColor(Color.TRANSPARENT);
                        webView.setLayerType(WebView.LAYER_TYPE_SOFTWARE, null);
                        message = message + "<style type=\"text/css\">body{color: #ffffff;}"
                                + "</style>";
                        message = message.replace("<span style=\"color:#fff\">", "\"");
                        webView.loadData(message, "text/html; charset=utf-8", "UTF-8");
                        tvTitle.setText(tMessage.title);
                        loadMedia(tMessage);


//                    viewGallery.setVisibility(View.GONE);

//                    loadMedia(tMessage.messageId);
                        setUnReadMessagePref();
                    }
                }catch (Exception ex)
                {
                    Debug.e(TAG, ex);
                }
            }
        });

    }

    //    void loadMedia(int messageId) {
    void loadMedia(TMessage tMessage) {
        try {
            if (!tMessage.urlImage.equals(null) || !tMessage.urlImage.equals("")) {
                viewGallery.reset();
                viewGallery.setVisibility(View.VISIBLE);
                viewGallery.addImage(tMessage.urlImage);

            }
            else {
                //setupvideo
                if (!tMessage.urlVideo.equals(null) || !tMessage.urlVideo.equals("")) {
                    videoLayout.setVisibility(View.VISIBLE);
                    setupVideo(tMessage.urlVideo);
                }
            }


        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }

     /*   try {
            PerformAsync2.run(new PerformAsync2.Callback() {
                @Override
                public Object onBackground(PerformAsync2 performAsync) {
                    List<TMessageMedia> list = new ArrayList<>();

                    try {
                        DbMessage dbMessage = DbMessage.Instance.create(getActivity().getApplicationContext());
                        list = dbMessage.daoAccessMessage().getAllMedia(messageId);
                        dbMessage.close();
                    }
                    catch (Exception ex)
                    {

                    }

                    return list;
                }
            }).setCallbackResult(new PerformAsync2.CallbackResult() {
                @Override
                public void onResult(Object result) {
                    listMedia = (List<TMessageMedia>) result;

                    //kalo image ada, maka tampilkan view gallery
                    if (listMedia.size() == 0) {
                        videoLayout.setVisibility(View.GONE);
                        return;
                    }

                    for (TMessageMedia media : listMedia) {
                        if (media.urlVideo != null && media.urlVideo.isEmpty() == false) {
                            videoView.setFocusable(true);
                            videoView.setVideoPath(media.urlVideo);
                            videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                                @Override
                                public void onPrepared(MediaPlayer mp) {
                                    mediaPlayer = mp;
                                    mediaPlayer.start();
                                    mediaPlayer.setOnVideoSizeChangedListener(new MediaPlayer.OnVideoSizeChangedListener() {
                                        @Override
                                        public void onVideoSizeChanged(MediaPlayer mp, int arg1, int arg2) {

                                            mediaPlayer.start();

                                        }
                                    });
                                    mediaPlayer.setOnBufferingUpdateListener(new MediaPlayer.OnBufferingUpdateListener() {
                                        @Override
                                        public void onBufferingUpdate(MediaPlayer mp, int percent) {
                                            int percentage = percent;
                                        }
                                    });
                                    mediaPlayer.setOnErrorListener(new MediaPlayer.OnErrorListener() {
                                        @Override
                                        public boolean onError(MediaPlayer mp, int what, int extra) {
                                            return false;
                                        }
                                    });

                                }
                            });
                            videoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                                @Override
                                public void onCompletion(MediaPlayer mp) {
                                    exitController();
                                    mIsComplete = true;
                                }
                            });

                            controller = new VideoControllerView.Builder(getActivity(), new VideoControllerView.MediaPlayerControlListener() {
                                @Override
                                public void start(MediaPlayer mp) {
                                    if (null != videoView) {
                                        videoView.start();
                                        mIsComplete = false;
                                    }
                                }

                                @Override
                                public void pause() {
                                    if (null != videoView) {
                                        videoView.pause();
                                    }
                                }

                                @Override
                                public int getDuration() {
                                    int duration = 0;
                                    try {
                                        if (null != videoView)
                                            duration = videoView.getDuration();
                                        else
                                            return 0;
                                    } catch (Exception ex) {

                                    }
                                    return duration;
                                }

                                @Override
                                public int getCurrentPosition() {
                                    int position = 0;
                                    try {
                                        if (null != videoView)
                                            position = videoView.getCurrentPosition();
                                        else
                                            position = 0;
                                    } catch (Exception ex) {

                                    }
                                    return position;
                                }

                                @Override
                                public void seekTo(int position) {
                                    if (null != videoView) {
                                        videoView.seekTo(position);
                                    }
                                }

                                @Override
                                public boolean isPlaying() {
                                    if (null != videoView)
                                        return videoView.isPlaying();
                                    else
                                        return false;
                                }

                                @Override
                                public boolean isComplete() {
                                    return mIsComplete;
                                }

                                @Override
                                public int getBufferPercentage() {
                                    return 0;
                                }

                                @Override
                                public boolean isFullScreen() {
                                    return getActivity().getRequestedOrientation() == ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE ? true : false;

                                }

                                @Override
                                public void toggleFullScreen() {
                                    if (isFullScreen()) {
                                        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
                                    } else {
                                        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
                                    }
                                }

                                @Override
                                public void exit() {

                                }

                                @Override
                                public void unFocused() {
                                    *//*if (!videoView.isFocused() && !scrollView.isFocused() && !webView.isFocused()) {
//                                    layoutDetail.setBackgroundColor(colorUnSelected);
                                    }*//*
                                }

                                @Override
                                public void nextSong() {

                                }

                                @Override
                                public void prevSong() {

                                }

                                @Override
                                public void equalizer() {

                                }
                            })
//                                .withVideoTitle(tvTitle.getText().toString())
                                    .withVideoSurfaceView(videoView)//to enable toggle display controller view
                                    .canControlBrightness(true)
                                    .canControlVolume(true)
                                    .canSeekVideo(true)
                                    .pauseIcon(R.drawable.selector_pause)
                                    .playIcon(R.drawable.selector_play)
                                    .shrinkIcon(R.drawable.selector_fullscreen_exit)
                                    .stretchIcon(R.drawable.selector_fullscreen)
                                    .build((FrameLayout) root.findViewById(R.id.video_layout));//layout container that hold video play view

                            videoLayout.setVisibility(View.VISIBLE);
//                            viewGallery.setVisibility(View.GONE);
                            break;
                        } else {
                            videoView.setFocusable(false);
                            videoLayout.setVisibility(View.GONE);
//                            viewGallery.setVisibility(View.VISIBLE);
//                            viewGallery.addImage(media.urlImage);

                        }
                    }
                }
            });
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }*/
    }

    void setupVideo(String url) {
        try {
            videoLayout.setVisibility(View.VISIBLE);
            videoView.setFocusable(true);
            videoView.setVideoPath(url);
            videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    mediaPlayer = mp;
                    mediaPlayer.start();
                    mediaPlayer.setOnVideoSizeChangedListener(new MediaPlayer.OnVideoSizeChangedListener() {
                        @Override
                        public void onVideoSizeChanged(MediaPlayer mp, int arg1, int arg2) {

                            mediaPlayer.start();

                        }
                    });
                    mediaPlayer.setOnBufferingUpdateListener(new MediaPlayer.OnBufferingUpdateListener() {
                        @Override
                        public void onBufferingUpdate(MediaPlayer mp, int percent) {
                            int percentage = percent;
                        }
                    });
                    mediaPlayer.setOnErrorListener(new MediaPlayer.OnErrorListener() {
                        @Override
                        public boolean onError(MediaPlayer mp, int what, int extra) {
                            return false;
                        }
                    });

                }
            });
            videoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mp) {
//                    exitController();
                    mIsComplete = true;
                }
            });

        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    void exitController() {
        controller.exitController();
    }

    void loadMessage() {
        ModelMessage.getMessage(getContext(), messageId, new ModelMessage.IGetMessage() {
            @Override
            public void IGetMessage(TMessage tMessage) {
                setText(tMessage);
            }
        });
        /*try {
            PerformAsync2.run(performAsync -> {
                TMessage tMessage = null;
                try {
                    DbMessage dbMessage = DbMessage.Instance.create(getContext());
                    tMessage = dbMessage.daoAccessMessage().getMessageById(messageId);
                    dbMessage.close();
                } catch (Exception ex) {

                }
                return tMessage;
            }).setCallbackResult(result -> {
                TMessage i = (TMessage) result;

                setText(i);
            });
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }*/
    }

    void setUnReadMessagePref() {

        ModelMessage.getListMessage(getContext(), new ModelMessage.IGetListMessage() {
            @Override
            public void onGetListMessage(List<TMessage> TMessageList) {
                boolean hasUnReadMessage = false;
                for (TMessage item : TMessageList) {
                    if (!item.status.equals(C.MESSAGE_STATUS_READ)) {
                        hasUnReadMessage = true;
                        break;
                    }
                }
                Global.setUnReadMessage(hasUnReadMessage);
            }
        });
       /* PerformAsync2.run(performAsync ->
        {
            List<TMessage> tMessageList = new ArrayList<>();
            try {
                DbMessage dbMessage = DbMessage.Instance.create(getContext());
                tMessageList = dbMessage.daoAccessMessage().getAll();
                dbMessage.close();
            } catch (Exception ex) {

            }
            return tMessageList;
        }).setCallbackResult(result ->
        {
            boolean hasUnReadMessage = false;
            List<TMessage> tMessageList = (List<TMessage>) result;
            for (TMessage item : tMessageList) {
                if (!item.status.equals(C.MESSAGE_STATUS_READ)) {
                    hasUnReadMessage = true;
                    break;
                }
            }
            Global.setUnReadMessage(hasUnReadMessage);
        });*/
    }
}

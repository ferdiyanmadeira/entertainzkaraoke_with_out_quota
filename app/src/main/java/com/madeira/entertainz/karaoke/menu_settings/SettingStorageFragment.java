package com.madeira.entertainz.karaoke.menu_settings;


import android.content.res.ColorStateList;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.os.StatFs;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.madeira.entertainz.karaoke.DBLocal.DbSong;
import com.madeira.entertainz.karaoke.DBLocal.JoinSongCountPlayed;
import com.madeira.entertainz.karaoke.DBLocal.TSong;
import com.madeira.entertainz.karaoke.Debug;
import com.madeira.entertainz.karaoke.R;
import com.madeira.entertainz.library.PerformAsync2;
import com.madeira.entertainz.library.StorageUtils;
import com.madeira.entertainz.library.Util;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class SettingStorageFragment extends Fragment implements SettingsTheme.ISettingsTheme {

    static String PARENT_TAG ="SettingStorageFragment";
    View root;
    SettingsTheme settingsTheme;

    LinearLayout rootLL;
    TextView totalSongTV;
    ProgressBar progressBar;
    TextView freeSpaceTV;
    TextView usedSpaceTV;
    TextView totalSpaceTV;

    long megTotal;
    long megAvailable;
    long megUsed;

    public SettingStorageFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        root = inflater.inflate(R.layout.fragment_setting_storage, container, false);
        try {
            bind();
            settingsTheme = new SettingsTheme(getActivity(), this);
            settingsTheme.setTheme();
            getTotalSong();
        } catch (Exception ex) {
            Debug.e(PARENT_TAG, ex);
        }
        return root;
    }

    void bind()
    {
        rootLL = root.findViewById(R.id.rootLL);
        totalSongTV = root.findViewById(R.id.totalSongTV);
        progressBar = root.findViewById(R.id.progressBar);
        freeSpaceTV = root.findViewById(R.id.used_space_tv);
        usedSpaceTV = root.findViewById(R.id.capacitySpaceTV);
        totalSpaceTV = root.findViewById(R.id.total_space_tv);
    }

    @Override
    public void setThemeUpdateDate(Date lastUpdateTheme) {

    }

    @Override
    public void setListColorTheme(int color) {
        rootLL.setBackgroundResource(R.drawable.tags_rounded_corners);

        GradientDrawable categoryDrawable = (GradientDrawable) rootLL.getBackground();
        categoryDrawable.setColor(color);
    }

    @Override
    public void setTextAndSelectionColorTheme(ColorStateList colorSelection, ColorStateList colorText, int colorTextSelected, int colorTextFocus, int colorTextNormal, int colorSelectionSelected, int colorSelectionFocus) {

    }


    void getUSBInfoSize() {
        String TAG = PARENT_TAG + "-" + "getUSBInfoSize";
        try {
            String usbPath = "";

            List<StorageUtils.StorageInfo> storageInfoList = StorageUtils.getStorageList();
            for (StorageUtils.StorageInfo storageInfo : storageInfoList) {
                if (storageInfo.removable) {
                    String[] split = storageInfo.path.split("/");
                    usbPath = "/storage/" + split[split.length - 1];
                }
            }
            StatFs statFs = new StatFs(usbPath);
            long blockSize = statFs.getBlockSize();
            long totalSize = statFs.getBlockCount() * blockSize;
            long availableSize = statFs.getAvailableBlocks() * blockSize;
//            long freeSize = statFs.getFreeBlocks() * blockSize;
            long usedSize = totalSize - availableSize;

            long megTotal = totalSize / (1024 * 1024);
            megAvailable = availableSize / (1024 * 1024);
            megUsed = usedSize / (1024 * 1024);
            freeSpaceTV.setText(Util.thousandFormat(megAvailable));
            usedSpaceTV.setText(Util.thousandFormat(megUsed));
            totalSpaceTV.setText(Util.thousandFormat(megTotal));
            progressBar.setMax((int) megTotal);
            progressBar.setProgress((int) megUsed);
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    void getTotalSong ()
    {
        PerformAsync2.run(performAsync ->
        {
            List<JoinSongCountPlayed> tSongList = new ArrayList<>();
            try
            {
                DbSong dbSong = DbSong.Instance.create(getActivity());
                tSongList = dbSong.daoAccessTSong().getAll();
                dbSong.close();
            }catch (Exception ex)
            {

            }
            return  tSongList;
        }).setCallbackResult(result ->
        {
            List<TSong> tSongList = (List<TSong>) result;
            totalSongTV.setText(String.valueOf(tSongList.size()));
            getUSBInfoSize();
        });
    }
}

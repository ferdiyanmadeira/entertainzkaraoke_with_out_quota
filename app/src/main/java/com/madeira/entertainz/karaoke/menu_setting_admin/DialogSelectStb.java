package com.madeira.entertainz.karaoke.menu_setting_admin;


import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.madeira.entertainz.helper.RecyclerViewAdapter;
import com.madeira.entertainz.karaoke.Debug;
import com.madeira.entertainz.karaoke.R;
import com.madeira.entertainz.karaoke.model.ModelInstaller;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class DialogSelectStb extends DialogFragment {
    private static final String TAG = "DlgRequestSesFrag";

    private static final String DIALOG_TAG = "DIALOG_REQUEST_SESSION";
    private static final String BUNDLE_ADMIN_SESSIONID = "BUNDLE_ADMIN_SESSIONID";
    private static final String BUNDLE_ADMIN_SALT = "BUNDLE_ADMIN_SALT";


    String adminSessionId;
    String adminSalt;

    EditText editSearch;
    Button btnSearch;
    RecyclerView recyclerView;

    RecyclerViewAdapter adapter;

    List<ModelInstaller.ResultGetStbPartial.Stb> list = new ArrayList<>();

    IDialogRequestSessionFragment callback;

    public interface IDialogRequestSessionFragment {
        void onItemSelected(ModelInstaller.ResultGetStbPartial.Stb stb);
    }

    public static DialogSelectStb newInstance(FragmentActivity activity, String adminSessionId, String adminSalt) {
        DialogSelectStb fragment = new DialogSelectStb();

        try {
            FragmentManager fm = activity.getSupportFragmentManager();
            fragment.show(fm, DIALOG_TAG);
            Bundle bundle = new Bundle();
            bundle.putString(BUNDLE_ADMIN_SESSIONID, adminSessionId);
            bundle.putString(BUNDLE_ADMIN_SALT, adminSalt);
            fragment.setArguments(bundle);
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
        return fragment;
    }

    public void setCallback(IDialogRequestSessionFragment callback) {
        this.callback = callback;
    }

    public DialogSelectStb() {
        Log.i(TAG, "DialogRequestSessionFragment");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_dialog_select_stb, container, false);
        try {
            adminSessionId = (String) getArguments().get(BUNDLE_ADMIN_SESSIONID);
            adminSalt = (String) getArguments().get(BUNDLE_ADMIN_SALT);

            // Inflate the layout for this fragment
            bind(v);
            setupButton();
            setupRecylerView();
            fetchData(0);
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }

        return v;
    }

    void bind(View v) {
        try {
            editSearch = v.findViewById(R.id.edit_search);
            btnSearch = v.findViewById(R.id.button_search);
            recyclerView = v.findViewById(R.id.recycler_view);
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }

    }

    void setupButton() {
        try {
            btnSearch.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    fetchData(0);
                }
            });
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    void fetchData(int offset) {

        try {
            ModelInstaller.getStbPartial(adminSessionId, adminSalt, editSearch.getText().toString(), offset, 50, new ModelInstaller.IGetStbPartial() {
                @Override
                public void onGetStbPartial(List<ModelInstaller.ResultGetStbPartial.Stb> list) {

                    //kalo offset==0 artinya search dari awal
                    if (offset == 0) DialogSelectStb.this.list.clear();

                    DialogSelectStb.this.list.addAll(list);
                    adapter.notifyDataSetChanged();
                }

                @Override
                public void onError(Exception e) {
                    Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_LONG).show();
                }
            });
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }

    }

    void setupRecylerView() {
        try
        {
        adapter = new RecyclerViewAdapter(new RecyclerViewAdapter.IRecyclerViewAdapter() {
            @Override
            public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
                View view = LayoutInflater.from(getContext()).inflate(R.layout.cell_dialog_select_stb, parent, false);
                ItemViewHolder item = new ItemViewHolder(view);
                return item;
            }

            @Override
            public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
                ItemViewHolder item = (ItemViewHolder) holder;
                item.bind(list.get(position), position);
            }

            @Override
            public int getItemCount() {
                return list.size();
            }
        });

        recyclerView.setAdapter(adapter);
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    class ItemViewHolder extends RecyclerView.ViewHolder {

        View root;
        TextView tvTitle;
        TextView tvSubTitle;

        public ItemViewHolder(@NonNull View itemView) {
            super(itemView);
            root = itemView;
            tvTitle = itemView.findViewById(R.id.text_title);
            tvSubTitle = itemView.findViewById(R.id.text_sub_title);
        }

        public void bind(ModelInstaller.ResultGetStbPartial.Stb item, int pos) {
            tvTitle.setText(item.stbName);
            tvSubTitle.setText("Room " + item.roomName + " (" + item.location + ")");

            root.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    callback.onItemSelected(item);
                    DialogSelectStb.this.dismiss();
                }
            });
        }
    }

}

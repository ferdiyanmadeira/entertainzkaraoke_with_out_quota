package com.madeira.entertainz.karaoke.DBLocal;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;
import com.madeira.entertainz.library.Util;

import java.util.Date;

@Entity(tableName = "tmessage_image")
public class TMessageMedia {

    @PrimaryKey(autoGenerate = true)
    @SerializedName("message_media_id")
    public int messageMediaId;

    @SerializedName("message_id")
        public int messageId;

    @SerializedName("url_image")
    public String urlImage;


    @SerializedName("url_video")
    public String urlVideo;

    public String caption;

    @SerializedName("update_date")
    public String updateDate;

    public Date getUpdateDate(){
        return Util.convertMySqlDate(updateDate);
    }
}
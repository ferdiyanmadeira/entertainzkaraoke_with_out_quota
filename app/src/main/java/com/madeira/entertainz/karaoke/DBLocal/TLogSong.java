package com.madeira.entertainz.karaoke.DBLocal;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

//todo hanya table temporary, jika online dan berhasil sync, table ini akan di delete semua row nya

@Entity(tableName = "TLogSong")
public class TLogSong implements Serializable {

    @PrimaryKey @NonNull
    @SerializedName("songId")
    public String songId;
    @SerializedName("totalCount")
    public int totalCount;
}

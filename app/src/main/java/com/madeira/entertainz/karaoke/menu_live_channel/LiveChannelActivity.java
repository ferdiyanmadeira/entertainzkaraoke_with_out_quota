package com.madeira.entertainz.karaoke.menu_live_channel;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Handler;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.madeira.entertainz.karaoke.DBLocal.JoinSettingElementMedia;
import com.madeira.entertainz.karaoke.DBLocal.TSticker;
import com.madeira.entertainz.karaoke.Debug;
import com.madeira.entertainz.karaoke.R;
import com.madeira.entertainz.karaoke.config.C;
import com.madeira.entertainz.karaoke.config.Global;
import com.madeira.entertainz.karaoke.menu_main.BottomNavigationFragment;
import com.madeira.entertainz.karaoke.menu_main.ListMoodFragment;
import com.madeira.entertainz.karaoke.menu_navigation.TopNavigationFragment;
import com.madeira.entertainz.karaoke.model_online.ModelChannel;
import com.madeira.entertainz.karaoke.model_online.ModelRadio;
import com.madeira.entertainz.karaoke.model_room_db.ModelElementMedia;
import com.madeira.entertainz.library.Util;

import net.yslibrary.android.keyboardvisibilityevent.KeyboardVisibilityEvent;
import net.yslibrary.android.keyboardvisibilityevent.KeyboardVisibilityEventListener;

import java.io.File;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import es.dmoral.toasty.Toasty;

import static com.madeira.entertainz.karaoke.config.CacheData.lastIndexBackground;

public class LiveChannelActivity extends AppCompatActivity {
    static String TAG = "LiveChannelActivity";
    String lastLanguageId = "";
    String lastTitleApp = "";
    ImageView backgroundIV;
    int lastDuration = 0;
    List<Integer> activeBackgroundId = new ArrayList<>();
    TopNavigationFragment topNavigationFragment = new TopNavigationFragment();
    BottomNavigationFragment bottomNavigationFragment = new BottomNavigationFragment();
    ListMoodFragment listMoodFragment = new ListMoodFragment();
    LiveChannelListFragment liveChannelListFragment = new LiveChannelListFragment();
    LiveChannelSubListRadioFragment liveChannelSubListFragment = new LiveChannelSubListRadioFragment();
    ProgressBar progressBar;
    Handler handler = new Handler();
    Runnable runnable;
    View listMoodView;
    Boolean isFullScreen = false;
    //flag untuk mengetahui sedang akses tv atau radio
    Boolean isTV = false;
    View rootView;
    FrameLayout detailFrameLayout;
    static ILiveChannelActivityListener listener;
    public interface ILiveChannelActivityListener{
        void onFullScreen(boolean fullscreen);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        try {

            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_live_channel);

            bind();
            bindFragment();
            fetchListBackground();
            //for trigger change title
            lastTitleApp = Global.getTitleApp();


            lastDuration = Global.getDuration();
            Util.hideNavigationBar(this);
            KeyboardVisibilityEvent.setEventListener(
                    this,
                    new KeyboardVisibilityEventListener() {
                        @Override
                        public void onVisibilityChanged(boolean isOpen) {
                            // some code depending on keyboard visiblity status
                            if (isOpen) {
                                Toasty.info(LiveChannelActivity.this, getString(R.string.pressBackToHideKeyBoard)).show();
                            }

                        }
                    });

            String languageId = Global.getLanguageId();
            Util.setLanguage(this, languageId);
            lastLanguageId = languageId;
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    public static void startActivity(Activity activity) {
        Intent intent = new Intent(activity, LiveChannelActivity.class);
        activity.startActivity(intent);
//        activity.finish();
    }


    void bind() {
        try {
            backgroundIV = (ImageView) findViewById(R.id.backgroundIV);
            progressBar = findViewById(R.id.progressBar);
            listMoodView = findViewById(R.id.listMoodFrameLayout);
            rootView = findViewById(R.id.root_view);
            detailFrameLayout = findViewById(R.id.detailFrameLayout);
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    void bindFragment() {
//        topNavigationFragment = topNavigationFragment.newInstance(true);
        try {
            topNavigationFragment = topNavigationFragment.newInstance(C.MENU_LIVE_CHANNEL);

            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.topNavFrameLayout, topNavigationFragment);
            ft.replace(R.id.bottomFrameLayout, bottomNavigationFragment);
            ft.replace(R.id.categoryFrameLayout, liveChannelListFragment);
            ft.replace(R.id.listMoodFrameLayout, listMoodFragment);
            Bundle bundle = new Bundle();
            /** sub list yang pertama kali di select adalah radio*/
            bundle.putInt(LiveChannelSubListRadioFragment.KEY_FLAG_TV, C.MENU_LIVE_CHANNEL_RADIO);
            liveChannelSubListFragment.setArguments(bundle);
            ft.replace(R.id.subCategoryFrameLayout, liveChannelSubListFragment);
            ft.commit();
            liveChannelListFragment.setCallback(iLiveChannelListListener);
            bottomNavigationFragment.setCallback(bottomNavigationFragmentListener);
            listMoodFragment.setCallBack(listMoodListener);
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }


    /**
     * untuk fetch list image background
     */
    void fetchListBackground() {
        ModelElementMedia.getListElementMediaActive(this, new ModelElementMedia.IGetListElementMediaActive() {
            @Override
            public void onGetListElementMedia(List<JoinSettingElementMedia> joinSettingElementMediaList) {
                int totalIndex = joinSettingElementMediaList.size();
                runnable = new Runnable() {
                    @Override
                    public void run() {
                        try {
                            if (joinSettingElementMediaList.size() != 0) {
                                if (lastIndexBackground >= totalIndex)
                                    lastIndexBackground = 0;

                                JoinSettingElementMedia joinSettingElementMedia = joinSettingElementMediaList.get(lastIndexBackground);
                                Uri uri = Uri.fromFile(new File(joinSettingElementMedia.urlImage));
//                            Picasso.with(KaraokeActivity.this).load(uri).into(backgroundIV);
                                InputStream inputStream = getContentResolver().openInputStream(uri);
                                BitmapFactory.Options options = new BitmapFactory.Options();
                                options.inPreferredConfig = Bitmap.Config.RGB_565;
                                Bitmap image = BitmapFactory.decodeStream(inputStream, null, options);
                                backgroundIV.setImageBitmap(image);

                                lastIndexBackground++;
                                handler.postDelayed(this::run, joinSettingElementMedia.duration);
                            }
                        } catch (Exception ex) {
                            Debug.e(TAG, ex);
                        }
                    }
                };
                handler.postDelayed(runnable, 100);
            }
        });
    }

    /**
     * untuk handler ketika category di click
     */
    LiveChannelListFragment.ILiveChannelListListener iLiveChannelListListener = new LiveChannelListFragment.ILiveChannelListListener() {
        @Override
        public void onCategoryClick(int categoryId) {
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            Bundle bundle = new Bundle();
            /** reset LiveChannelSubListFragment */
            liveChannelSubListFragment = new LiveChannelSubListRadioFragment();
            try {
                switch (categoryId) {
                    case C.MENU_LIVE_CHANNEL_RADIO:
                        bundle.putInt(LiveChannelSubListRadioFragment.KEY_FLAG_TV, C.MENU_LIVE_CHANNEL_RADIO);
                        isTV = false;
                        break;
                    case C.MENU_LIVE_CHANNEL_TV:
                        bundle.putInt(LiveChannelSubListRadioFragment.KEY_FLAG_TV, C.MENU_LIVE_CHANNEL_TV);
                        isTV = true;
                        break;
                }
                liveChannelSubListFragment.setArguments(bundle);
                ft.replace(R.id.subCategoryFrameLayout, liveChannelSubListFragment);
                ft.commit();
                liveChannelSubListFragment.setCallback(iSubCategoryListListener);
            } catch (Exception ex) {
                Debug.e(TAG, ex);
            }

        }
    };

    /**
     * untuk handle ketika item dari radio atau tv di click
     */
    LiveChannelSubListRadioFragment.ISubCategoryListListener iSubCategoryListListener = new LiveChannelSubListRadioFragment.ISubCategoryListListener() {
        @Override
        public void onRadioCategoryClick(int liveChannel, ModelRadio.subRadioItem tRadio, String url) {

            try {
                FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                Bundle bundle = new Bundle();
                LiveChannelRadioFragment liveChannelRadioFragment = new LiveChannelRadioFragment();
                bundle.putSerializable(liveChannelRadioFragment.KEY_RADIO, tRadio);
                liveChannelRadioFragment.setArguments(bundle);
                ft.replace(R.id.detailFrameLayout, liveChannelRadioFragment);
                ft.commit();

            } catch (Exception ex) {
                Debug.e(TAG, ex);
            }

        }

        @Override
        public void onChannelCategoryClick(int liveChannel, ModelChannel.subChannelItem channelItem, String url) {
            try {
                FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                Bundle bundle = new Bundle();
                LiveChannelTVFragment liveChannelTVFragment = new LiveChannelTVFragment();
                bundle.putSerializable(liveChannelTVFragment.KEY_CHANNEL, channelItem);
                liveChannelTVFragment.setArguments(bundle);
                ft.replace(R.id.detailFrameLayout, liveChannelTVFragment);
                ft.commit();
            } catch (Exception ex) {
                Debug.e(TAG, ex);
            }
        }

        @Override
        public void onFullScreen() {
            if (isFullScreen) {
                setFullScreen(false);

            } else {
                setFullScreen(true);
            }
        }
    };

    /**
     * listener ketika mood di click
     */
    BottomNavigationFragment.IMoodFragmentListener bottomNavigationFragmentListener = new BottomNavigationFragment.IMoodFragmentListener() {
        @Override
        public void onMoodClick() {
            listMoodView.setVisibility(View.VISIBLE);
        }
    };

    /**
     * listener ketika mood dipilih
     */
    ListMoodFragment.IListMoodListener listMoodListener = new ListMoodFragment.IListMoodListener() {
        @Override
        public void onClick(TSticker tSticker) {
            try {
                listMoodView.setVisibility(View.GONE);
                bottomNavigationFragment.updateMood(tSticker);
                Global.setStickerId(tSticker.stickerId);
            } catch (Exception ex) {
                Debug.e(TAG, ex);
            }
        }
    };

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        try {

            switch (keyCode) {
                case KeyEvent.KEYCODE_ENTER:
                    if (isTV) {
                        if (isFullScreen) {
                            setFullScreen(false);

                        } else {
                            setFullScreen(true);
                        }
                    }
                    break;
                case KeyEvent.KEYCODE_BACK:
                    if (isTV) {
                        if (isFullScreen) {
                            setFullScreen(false);
                            return false;
                        } else {
                            finish();
                        }

                    }
                    break;
            }
            if (isTV) {
                //change chanelCategory with number
                if (isFullScreen) {
                    if (keyCode >= 7 && keyCode <= 16) {
//                    processKey(keyCode);
                    }
                }
            }
        } catch (Exception e) {
            Debug.e(TAG, e);
        }

        return super.onKeyDown(keyCode, event);
    }


    void setFullScreen(Boolean prm) {
        try {

            if (prm) {
                //variabel untuk menandakan bahwa screen sedang fullscreen
                isFullScreen = true;
                //big video view

                topNavigationFragment.getView().setVisibility(View.GONE);
                bottomNavigationFragment.getView().setVisibility(View.GONE);
                liveChannelListFragment.getView().setVisibility(View.GONE);
//                liveChannelSubListFragment.getView().setVisibility(View.GONE);
                rootView.setPadding(0, 0, 0, 0);
                RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) detailFrameLayout.getLayoutParams();
                lp.addRule(RelativeLayout.BELOW, 0);
                lp.addRule(RelativeLayout.RIGHT_OF, 0);
                lp.leftMargin = 0;
                detailFrameLayout.setLayoutParams(lp);
                detailFrameLayout.setPadding(0, 0, 0, 0);

                //untuk mengatur ukuran videoview
//                tvVideoViewFragment.setMatchParentVideo(true);
            } else {
                //variabel untuk menandakan bahwa screen tidak fullscreen
                isFullScreen = false;

                //small video view
                topNavigationFragment.getView().setVisibility(View.VISIBLE);
                bottomNavigationFragment.getView().setVisibility(View.VISIBLE);
                liveChannelListFragment.getView().setVisibility(View.VISIBLE);
                liveChannelSubListFragment.getView().setVisibility(View.VISIBLE);
                int dimenPadding = (int) getResources().getDimension(R.dimen.padding_16dp);
                rootView.setPadding(dimenPadding, dimenPadding, dimenPadding, dimenPadding);
                RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) detailFrameLayout.getLayoutParams();
                lp.addRule(RelativeLayout.BELOW, R.id.categoryFrameLayout);
                lp.addRule(RelativeLayout.RIGHT_OF, R.id.subCategoryFrameLayout);
                lp.leftMargin = 20;
                detailFrameLayout.setLayoutParams(lp);
                detailFrameLayout.setPadding(dimenPadding, dimenPadding, dimenPadding, dimenPadding);

                //untuk mengatur ukuran videoview
//                tvVideoViewFragment.setMatchParentVideo(false);
            }
            listener.onFullScreen(prm);
        } catch (Exception e) {
            Debug.e(TAG, e);
        }
    }

    public static void setCallBack(ILiveChannelActivityListener callback)
    {
        try {
            listener = callback;
        }catch (Exception ex)
        {
            Debug.e(TAG, ex);
        }
    }


    @Override
    public void onDestroy()
    {
        try
        {
            super.onDestroy();
            Util.freeMemory();
        }
        catch (Exception ex)
        {
            Debug.e(TAG, ex);
        }
    }


}

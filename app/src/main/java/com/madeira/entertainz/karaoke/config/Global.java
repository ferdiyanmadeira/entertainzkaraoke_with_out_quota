package com.madeira.entertainz.karaoke.config;


import com.madeira.entertainz.karaoke.MainApplication;
import com.madeira.entertainz.karaoke.R;
import com.madeira.entertainz.library.HelperSharedPreferences;

import static com.madeira.entertainz.karaoke.config.C.DEFAULT_API;
import static com.madeira.entertainz.karaoke.config.C.SHARED_PREFERENCES_DEVICE_OWNER_CONFIG;
import static com.madeira.entertainz.karaoke.config.C.SHARED_PREFERENCES_QUOTA;

public class Global {

    static private String sessionId;
    static private String apiHostname;
    static private String USBPath;
    static private String languageId;
    static private int duration = 1000 * 60 * 2;
    static private int stickerIdUsed;
    static private int connectedUSB;
    static private int FirstBand;
    private static int SecondsBand;
    private static int ThirdBand;
    private static int ForthBand;
    private static int FiveBand;
    private static String lastUpdate;
    private static String pathUpdate;
    private static String pathEKB;
    private static boolean unReadMessage = false;
    private static String sN;


    //Setter dan getter apabila di butuhkan
    // di pakai, apabila setter dan getter harus di simpan ke sharedpref
    //
    public static void setSessionId(String value) {
        sessionId = value;
        HelperSharedPreferences.writeString(MainApplication.context, C.SHARED_PREFERENCES_SESSIONID, value);
    }

    public static String getSessionId() {
        if (sessionId == null) {
            sessionId = HelperSharedPreferences.readString(MainApplication.context, C.SHARED_PREFERENCES_SESSIONID, "");
        }
        return sessionId;
    }

    public static void setApiHostname(String value) {
        apiHostname = value;
        HelperSharedPreferences.writeString(MainApplication.context, C.SHARED_PREFERENCES_API_HOSTNAME, value);
    }

    public static String getApiHostname() {
        if (apiHostname == null) {
//            apiHostname = HelperSharedPreferences.readString(MainApplication.context, C.SHARED_PREFERENCES_API_HOSTNAME, DEFAULT_API);
            apiHostname = HelperSharedPreferences.readString(MainApplication.context, C.SHARED_PREFERENCES_API_HOSTNAME, "");
        }
        return apiHostname;
    }


    public static void setUSBPath(String prmUSBPath) {
        USBPath = prmUSBPath;
        HelperSharedPreferences.writeString(MainApplication.context, C.SHARED_PREFERENCES_USB_PATH, USBPath);
    }

    public static String getUSBPath() {
        pathEKB = HelperSharedPreferences.readString(MainApplication.context, C.SHARED_PREFERENCES_USB_PATH, USBPath);
        return pathEKB;
    }


    public static void setTitleApp(String value) {
        HelperSharedPreferences.writeString(MainApplication.context, C.SHARED_PREFERENCES_TITLEAPP, value);
    }

    public static String getTitleApp() {
        String defaultValue = "";
        if (C.FOR_HOTEL)
            defaultValue = MainApplication.context.getString(R.string.titleKaraoke);
        else
            defaultValue = MainApplication.context.getString(R.string.titleKaraokeHeyoo);

        String str = HelperSharedPreferences.readString(MainApplication.context, C.SHARED_PREFERENCES_TITLEAPP, defaultValue);
        return str;
    }

   /* public static String getTitleAppHeyoo() {
        String defaultValue=MainApplication.context.getString(R.string.titleKaraokeHeyoo);
        String str = HelperSharedPreferences.readString(MainApplication.context, C.SHARED_PREFERENCES_TITLEAPP, defaultValue);
        return str;
    }*/

    public static void setDefaultPasswordSetting(String value) {
        HelperSharedPreferences.writeString(MainApplication.context, C.SHARED_PREFERENCES_PASSWORDSETTING, value);
    }

    public static String getDefaultPasswordSetting() {
        String defaultValue = C.default_password_setting;
        String str = HelperSharedPreferences.readString(MainApplication.context, C.SHARED_PREFERENCES_PASSWORDSETTING, defaultValue);
        return str;
    }

    public static void setLanguageId(String languageId) {
        HelperSharedPreferences.writeString(MainApplication.context, C.SHARED_PREFERENCES_LANGUAGEID, languageId);
        Global.languageId = languageId;
    }

    public static String getLanguageId() {
        if (languageId == null) {
            languageId = HelperSharedPreferences.readString(MainApplication.context, C.SHARED_PREFERENCES_LANGUAGEID, "");
        }
        return languageId;
    }

    public static void setDuration(int duration) {
        HelperSharedPreferences.writeInt(MainApplication.context, C.SHARED_PREFERENCES_DURATION, duration);
        Global.duration = duration;
    }

    public static int getDuration() {
        if (duration == 0) {
            int tempDuration = 1000 * 60 * 2;
            duration = HelperSharedPreferences.readInt(MainApplication.context, C.SHARED_PREFERENCES_DURATION, tempDuration);
        }
        return duration;
    }

    public static void setStickerId(int prmStickerId) {
        HelperSharedPreferences.writeInt(MainApplication.context, C.SHARED_PREFERENCES_STICKERID_USED, prmStickerId);
        Global.stickerIdUsed = prmStickerId;
    }

    public static int getStickerId() {
        if (stickerIdUsed == 0) {
            stickerIdUsed = HelperSharedPreferences.readInt(MainApplication.context, C.SHARED_PREFERENCES_STICKERID_USED, 0);
        }
        return stickerIdUsed;
    }

    public static void setConnectedUSB(int prmNotConnectedUSB) {
        HelperSharedPreferences.writeInt(MainApplication.context, C.SHARED_PREFERENCES_CONNECTED_USB, prmNotConnectedUSB);
        Global.connectedUSB = prmNotConnectedUSB;
    }

    public static int getConnectedUSB() {
        if (connectedUSB == 0) {
            connectedUSB = HelperSharedPreferences.readInt(MainApplication.context, C.SHARED_PREFERENCES_CONNECTED_USB, 0);
        }
        return connectedUSB;
    }

    public static void setFirstBand(int prmNotFirstBand) {
        HelperSharedPreferences.writeInt(MainApplication.context, C.SHARED_PREFERENCES_FIRST_BAND, prmNotFirstBand);
        Global.FirstBand = prmNotFirstBand;
    }

    public static int getFirstBand() {
        if (FirstBand == 0) {
            FirstBand = HelperSharedPreferences.readInt(MainApplication.context, C.SHARED_PREFERENCES_FIRST_BAND, 0);
        }
        return FirstBand;
    }

    public static void setSecondsBand(int prmNotSecondsBand) {
        HelperSharedPreferences.writeInt(MainApplication.context, C.SHARED_PREFERENCES_SECONDS_BAND, prmNotSecondsBand);
        Global.SecondsBand = prmNotSecondsBand;
    }

    public static int getSecondsBand() {
        if (SecondsBand == 0) {
            SecondsBand = HelperSharedPreferences.readInt(MainApplication.context, C.SHARED_PREFERENCES_SECONDS_BAND, 0);
        }
        return SecondsBand;
    }


    public static void setThirdBand(int prmNotThirdBand) {
        HelperSharedPreferences.writeInt(MainApplication.context, C.SHARED_PREFERENCES_THIRD_BAND, prmNotThirdBand);
        Global.ThirdBand = prmNotThirdBand;
    }

    public static int getThirdBand() {
        if (ThirdBand == 0) {
            ThirdBand = HelperSharedPreferences.readInt(MainApplication.context, C.SHARED_PREFERENCES_THIRD_BAND, 0);
        }
        return ThirdBand;
    }

    public static void setForthBand(int prmNotForthBand) {
        HelperSharedPreferences.writeInt(MainApplication.context, C.SHARED_PREFERENCES_FORTH_BAND, prmNotForthBand);
        Global.ForthBand = prmNotForthBand;
    }

    public static int getForthBand() {
        if (ForthBand == 0) {
            ForthBand = HelperSharedPreferences.readInt(MainApplication.context, C.SHARED_PREFERENCES_FORTH_BAND, 0);
        }
        return ForthBand;
    }

    public static void setFiveBand(int prmNotFiveBand) {
        HelperSharedPreferences.writeInt(MainApplication.context, C.SHARED_PREFERENCES_FIVE_BAND, prmNotFiveBand);
        Global.FiveBand = prmNotFiveBand;
    }

    public static int getFiveBand() {
        if (FiveBand == 0) {
            FiveBand = HelperSharedPreferences.readInt(MainApplication.context, C.SHARED_PREFERENCES_FIVE_BAND, 0);
        }
        return FiveBand;
    }

    public static void setLastUpdate(String prmLastUpdate) {
        HelperSharedPreferences.writeString(MainApplication.context, C.SHARED_PREFERENCES_LAST_UPDATE, prmLastUpdate);
        Global.lastUpdate = prmLastUpdate;
    }

    public static String getLastUpdate() {
        lastUpdate = HelperSharedPreferences.readString(MainApplication.context, C.SHARED_PREFERENCES_LAST_UPDATE, "");
        return lastUpdate;
    }

    public static void setPathUpdate(String prmPathUpdate) {
        HelperSharedPreferences.writeString(MainApplication.context, C.SHARED_PREFERENCES_PATH_UPDATE, prmPathUpdate);
        Global.pathUpdate = prmPathUpdate;
    }

    public static String getPathUpdate() {
        pathUpdate = HelperSharedPreferences.readString(MainApplication.context, C.SHARED_PREFERENCES_PATH_UPDATE, "");
        return pathUpdate;
    }


    public static void setUnReadMessage(boolean prmUnReadMessage) {
        HelperSharedPreferences.writeBoolean(MainApplication.context, C.SHARED_PREFERENCES_HAVE_UNREAD_MESSAGE, prmUnReadMessage);
        Global.unReadMessage = prmUnReadMessage;
    }

    public static Boolean getUnReadMessage() {
        unReadMessage = HelperSharedPreferences.readBoolean(MainApplication.context, C.SHARED_PREFERENCES_HAVE_UNREAD_MESSAGE, false);
        return unReadMessage;
    }

/*    public static void setElementMediaDuration(int prmNotElementMediaDuration) {
        HelperSharedPreferences.writeInt(MainApplication.context, C.SHARED_PREFERENCES_FORTH_BAND, prmNotElementMediaDuration);
        Global.ELEMENT_MEDIA_DURATION = prmNotElementMediaDuration;
    }

    public static int getElementMediaDuration() {
        if (ELEMENT_MEDIA_DURATION == 0) {
            int twoMinutes = 1000*60*2;
            ELEMENT_MEDIA_DURATION = HelperSharedPreferences.readInt(MainApplication.context, C.SHARED_PREFERENCES_FORTH_BAND, twoMinutes);
        }
        return ELEMENT_MEDIA_DURATION;
    }*/


    public static void setSN(String prmSN) {
        HelperSharedPreferences.writeString(MainApplication.context, C.SHARED_PREFERENCES_SN, prmSN);
        Global.sN = prmSN;
    }

    public static String getSN() {
        sN = HelperSharedPreferences.readString(MainApplication.context, C.SHARED_PREFERENCES_SN, null);
        return sN;
    }


    public static void setEmail(String prmEmail) {
        HelperSharedPreferences.writeString(MainApplication.context, C.SHARED_PREFERENCES_EMAIL, prmEmail);
    }

    public static String getEmail() {
        String email = HelperSharedPreferences.readString(MainApplication.context, C.SHARED_PREFERENCES_EMAIL, "");
        return email;
    }


    public static void setName(String prmName) {
        HelperSharedPreferences.writeString(MainApplication.context, C.SHARED_PREFERENCES_NAME, prmName);
    }

    public static String getName() {
        String name = HelperSharedPreferences.readString(MainApplication.context, C.SHARED_PREFERENCES_NAME, "");
        return name;
    }

    public static void setTimeZone(String prmTimeZone) {
        HelperSharedPreferences.writeString(MainApplication.context, C.SHARED_PREFERENCES_TIME_ZONE, prmTimeZone);
    }

    public static String getTimeZone() {
        String TimeZone = HelperSharedPreferences.readString(MainApplication.context, C.SHARED_PREFERENCES_TIME_ZONE, "Asia/Jakarta");
        return TimeZone;
    }


    public static boolean getDeviceOwnerConfig() {
        return HelperSharedPreferences.readBoolean(MainApplication.context, SHARED_PREFERENCES_DEVICE_OWNER_CONFIG, true);
    }

    public static void setDeviceOwnerConfig(boolean state) {
        HelperSharedPreferences.writeBoolean(MainApplication.context, SHARED_PREFERENCES_DEVICE_OWNER_CONFIG, state);
    }

    public static long getQuota() {
//        return HelperSharedPreferences.readInt(MainApplication.context,SHARED_PREFERENCES_QUOTA, 0);
        return HelperSharedPreferences.readLong(MainApplication.context, SHARED_PREFERENCES_QUOTA, C.DEFAULT_QUOTA);
    }

    public static void setQuota(long totalQuota) {
        HelperSharedPreferences.writeLong(MainApplication.context, SHARED_PREFERENCES_QUOTA, totalQuota);
    }

    public static String getNextAppCodeName() {
        return HelperSharedPreferences.readString(MainApplication.context, C.SHARED_PREFERENCES_NEXT_CODE_NAME, "");
    }

    public static void setNextAppCodeName(String codeName) {
        HelperSharedPreferences.writeString(MainApplication.context, C.SHARED_PREFERENCES_NEXT_CODE_NAME, codeName);
    }

    public static int getNextAppCode() {
        return HelperSharedPreferences.readInt(MainApplication.context, C.SHARED_PREFERENCES_NEXT_VERSION, 0);
    }

    public static void setNextAppCode(int code) {
        HelperSharedPreferences.writeInt(MainApplication.context, C.SHARED_PREFERENCES_NEXT_VERSION, code);
    }


    public static int getPrevAppCode() {
        return HelperSharedPreferences.readInt(MainApplication.context, C.SHARED_PREFERENCES_PREV_VERSION, 0);
    }

    public static void setPrevAppCode(int code) {
        HelperSharedPreferences.writeInt(MainApplication.context, C.SHARED_PREFERENCES_PREV_VERSION, code);
    }
}

package com.madeira.entertainz.karaoke.model_online;

import android.net.Uri;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.OkHttpResponseAndJSONObjectRequestListener;
import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;
import com.madeira.entertainz.karaoke.Debug;
import com.madeira.entertainz.karaoke.config.Global;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.List;

import okhttp3.Response;

import static com.madeira.entertainz.karaoke.config.C.DEFAULT_API;

public class ModelMessage {
    private static final String TAG = "ModelMessage";

    private static final String APIHOST = Global.getApiHostname() + "/v1/message.php";

    public interface IModelMessage {
        void onGetAll(messageListItem result);

        void onError(ANError e);
    }

    static public void getMessageAll(String mac, IModelMessage callBack) {
        try {

            String uri = Uri.parse(APIHOST)
                    .buildUpon()
                    .appendQueryParameter("action", "get_all")
                    .appendQueryParameter("mac", mac)
                    .build().toString();

            AndroidNetworking.get(uri)
                    .setPriority(Priority.MEDIUM)
                    .build()
                    .getAsOkHttpResponseAndJSONObject(new OkHttpResponseAndJSONObjectRequestListener() {
                        @Override
                        public void onResponse(Response okHttpResponse, JSONObject response) {
                            try {
                                JSONArray jsonArray = new JSONArray();
                                jsonArray.put(response);
                                if (okHttpResponse.isSuccessful()) {
                                    if(response.has("errCode"))
                                    {
                                        int errorCode =response.getInt("errCode");
                                        String errorMessage = response.getString("errMsg");
                                        ANError anError = new ANError();
                                        anError.setErrorCode(errorCode);
                                        anError.setErrorBody(errorMessage);
                                        anError.setErrorDetail(errorMessage);
                                        callBack.onError(anError);
                                        return;
                                    }
                                    Gson gson = new Gson();
                                    String jsonString = response.toString();
                                    messageListItem appItem = gson.fromJson(jsonString, messageListItem.class);
                                    callBack.onGetAll(appItem);
                                }
                            } catch (Exception ex) {
                                ex.printStackTrace();
                            }
                        }

                        @Override
                        public void onError(ANError anError) {
                            callBack.onError(anError);
                        }
                    });

        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    public class messageListItem
    {
        @SerializedName("message")
        public List<messageItem> messageItems;
    }

    public class messageItem {
        @SerializedName("messageId")
        public String messageId;
        @SerializedName("message")
        public String message;
        @SerializedName("from")
        public String from;
        @SerializedName("title")
        public String title;
        @SerializedName("created_date")
        public String createdDate;
        @SerializedName("url_image")
        public String urlImage;
        @SerializedName("url_video")
        public String urlVideo;
    }

}

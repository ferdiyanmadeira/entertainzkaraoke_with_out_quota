package com.madeira.entertainz.karaoke.model_room_db;

import android.content.Context;

import com.madeira.entertainz.karaoke.DBLocal.DbAccess;
import com.madeira.entertainz.karaoke.DBLocal.TSticker;
import com.madeira.entertainz.karaoke.Debug;
import com.madeira.entertainz.karaoke.config.Global;
import com.madeira.entertainz.library.PerformAsync2;

import java.util.ArrayList;
import java.util.List;

public class ModelMood {
    static String TAG = "ModelMood";

    /**
     * untuk get list mood
     */
    public interface IGetListMood {
        void onGetListMood(List<TSticker> tStickerList);
    }

    public static void getListMood(Context context, IGetListMood callback) {
        PerformAsync2.run(performAsync ->
        {
            List<TSticker> result = new ArrayList<>();
            try {
                DbAccess dbAccess = DbAccess.Instance.create(context);
                result = dbAccess.daoAccessSticker().getAll();
                dbAccess.close();
            } catch (Exception ex) {
                Debug.e(TAG, ex);
            }
            return result;
        }).
                setCallbackResult(result ->

                {
                    try {
                        List<TSticker> tStickerList = (List<TSticker>) result;
                        callback.onGetListMood(tStickerList);
                    }catch (Exception ex)
                    {
                        Debug.e(TAG, ex);
                    }

                });
    }

    /**
     * untuk get mood/sticker
     */
    public interface IGetMood {
        void IGetMood(TSticker tSticker);
    }

    public static void getMood(Context context, IGetMood callback) {
        PerformAsync2.run(performAsync ->
        {
            TSticker tSticker = null;
            try {
                int lastStickerId = Global.getStickerId();
                if (lastStickerId != 0) {
                    DbAccess dbAccess = DbAccess.Instance.create(context);
                    tSticker = dbAccess.daoAccessSticker().getStickerById(lastStickerId);
                    dbAccess.close();
                }
            } catch (Exception ex) {
                Debug.e(TAG, ex);
            }

            return tSticker;
        }).
                setCallbackResult(result ->

                {
                    try {
                        TSticker tSticker = (TSticker) result;
                        callback.IGetMood(tSticker);
                    }catch (Exception ex)
                    {
                        Debug.e(TAG, ex);
                    }

                });
    }

}

package com.madeira.entertainz.karaoke.model_online;

import android.net.Uri;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.OkHttpResponseAndJSONObjectRequestListener;
import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;
import com.madeira.entertainz.karaoke.DBLocal.TLogSong;
import com.madeira.entertainz.karaoke.Debug;
import com.madeira.entertainz.karaoke.config.Global;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import okhttp3.Response;

import static com.madeira.entertainz.karaoke.config.C.DEFAULT_API;

public class ModelSTB {
    private static final String TAG = "ModelSTB";

    private static final String APIHOST = Global.getApiHostname() + "/v1/stb.php";


    public interface ISyncQuota {
        void onSync(JSONObject result);

        void onError(ANError e);
    }
    static public void syncQuota(String mac, int quota, List<TLogSong> tLogSongArrayList, ISyncQuota callBack) {
        try {
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("mac", mac);
                jsonObject.put("quota", quota);
                JSONArray jsonArray = new JSONArray();
                for (TLogSong tLogSong : tLogSongArrayList) {
                    JSONObject tempTLogSong = new JSONObject();
                    tempTLogSong.put("songId", tLogSong.songId);
                    tempTLogSong.put("totalCount", tLogSong.totalCount);
                    jsonArray.put(tempTLogSong);
                }
                jsonObject.put("listSong", jsonArray);
                String value = jsonObject.toString();

                AndroidNetworking.post(APIHOST)
                        .addJSONObjectBody(jsonObject) // posting json
//                        .setTag("test")
                        .addQueryParameter("action", "updateQuota")
                        .setPriority(Priority.MEDIUM)
                        .build()
                        .getAsOkHttpResponseAndJSONObject(new OkHttpResponseAndJSONObjectRequestListener() {
                            @Override
                            public void onResponse(Response okHttpResponse, JSONObject response) {
                                try {
                                    JSONObject jsonObject1 = null;
                                    if (okHttpResponse.isSuccessful()) {
                                        jsonObject1 = response;
                                        callBack.onSync(response);
                                    }
                                } catch (Exception ex) {

                                }
                            }

                            @Override
                            public void onError(ANError anError) {

                                callBack.onError(anError);
                            }
                        });

            } catch (JSONException e) {
                e.printStackTrace();
            }


        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    public interface IGetInfoSTB {
        void onGetSTBInfo(stbItem result);

        void onError(ANError e);
    }
    static public void getInfoSTB(String mac, IGetInfoSTB callBack) {
        try {
            String uri = Uri.parse(APIHOST)
                    .buildUpon()
                    .appendQueryParameter("action", "get_stb_info")
                    .appendQueryParameter("mac", mac)
                    .build().toString();

            AndroidNetworking.get(uri)
                    .setPriority(Priority.MEDIUM)
                    .build()
                    .getAsOkHttpResponseAndJSONObject(new OkHttpResponseAndJSONObjectRequestListener() {
                        @Override
                        public void onResponse(Response okHttpResponse, JSONObject response) {
                            try {
                                if (okHttpResponse.isSuccessful()) {
                                    Gson gson = new Gson();
                                    String jsonString = response.toString();
                                    stbItem stbItem = gson.fromJson(jsonString, ModelSTB.stbItem.class);
                                    callBack.onGetSTBInfo(stbItem);
                                }
                            } catch (Exception ex) {

                            }
                        }

                        @Override
                        public void onError(ANError anError) {
                            callBack.onError(anError);
                        }
                    });
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    public interface IUpdateSTBInfo {
        void onUpdateSTBInfo(ModelApp.appItem result);

        void onError(ANError e);
    }
    static public void updateSTBInfo(String mac, String appId, String versionCode, String versionName, String androidversion, ModelSTB.IUpdateSTBInfo callBack) {
        try {
            String androidAPI = APIHOST;

            String uri = Uri.parse(APIHOST)
                    .buildUpon()
                    .appendQueryParameter("action", "update_info")
                    .appendQueryParameter("app_id", appId)
                    .appendQueryParameter("mac", mac)
                    .appendQueryParameter("version_code", versionCode)
                    .appendQueryParameter("version_name", versionName)
                    .appendQueryParameter("android_version", androidversion)
                    .appendQueryParameter("android_api", androidAPI)
                    .build().toString();

            AndroidNetworking.get(uri)
                    .setPriority(Priority.MEDIUM)
                    .build()
                    .getAsOkHttpResponseAndJSONObject(new OkHttpResponseAndJSONObjectRequestListener() {
                        @Override
                        public void onResponse(Response okHttpResponse, JSONObject response) {
                            try {
//                                String sResponse = response.toString();
                                JSONArray jsonArray = new JSONArray();
                                jsonArray.put(response);
                                if (okHttpResponse.isSuccessful()) {
                                    if(response.has("errCode"))
                                    {
                                        int errorCode =response.getInt("errCode");
                                        String errorMessage = response.getString("errMsg");
                                        ANError anError = new ANError();
                                        anError.setErrorCode(errorCode);
                                        anError.setErrorBody(errorMessage);
                                        anError.setErrorDetail(errorMessage);
                                        callBack.onError(anError);
                                        return;
                                    }
                                    Gson gson = new Gson();
                                    String jsonString = response.toString();
                                    ModelApp.appItem appItem = gson.fromJson(jsonString, ModelApp.appItem.class);
                                    callBack.onUpdateSTBInfo(appItem);
                                }
                            } catch (Exception ex) {
                                ex.printStackTrace();
                            }
                        }

                        @Override
                        public void onError(ANError anError) {
                            callBack.onError(anError);
                        }
                    });
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    public interface IDisableSyncQuota {
        void onDisableSync(JSONObject result);

        void onError(ANError e);
    }
    static public void DisableSyncQuota(String mac, IDisableSyncQuota callBack) {
        try {
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("mac", mac);

                String value = jsonObject.toString();

                AndroidNetworking.get(APIHOST)
                        .addQueryParameter("action", "updateFlag")
                        .setPriority(Priority.MEDIUM)
                        .build()
                        .getAsOkHttpResponseAndJSONObject(new OkHttpResponseAndJSONObjectRequestListener() {
                            @Override
                            public void onResponse(Response okHttpResponse, JSONObject response) {
                                try {
                                    JSONObject jsonObject1 = null;
                                    if (okHttpResponse.isSuccessful()) {
                                        jsonObject1 = response;
                                        callBack.onDisableSync(response);
                                    }
                                } catch (Exception ex) {

                                }
                            }

                            @Override
                            public void onError(ANError anError) {

                                callBack.onError(anError);
                            }
                        });

            } catch (JSONException e) {
                e.printStackTrace();
            }


        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }


    public class stbItem {
        @SerializedName("stb")
        public subSTBItem stb;
    }

    public class subSTBItem {
        //        @SerializedName("version_code")
//        public String versionCode;
//
        @SerializedName("sTBId")
        public String sTBId;
        @SerializedName("sTBMAC")
        public String sTBMAC;
        @SerializedName("status")
        public String status;
        @SerializedName("sN")
        public String sN;
        @SerializedName("batchNumber")
        public String batchNumber;
        @SerializedName("totalSong")
        public String totalSong;
        @SerializedName("last_seen")
        public String last_seen;
        @SerializedName("plainIV")
        public String plainIV;
        @SerializedName("plainKey")
        public String plainKey;
        @SerializedName("variant_id")
        public String variantId;
        @SerializedName("updateQuota")
        public String updateQuota;
    }

    public static class quotaItem {
        @SerializedName("quota")
        public String quota;
    }


    public static class stbInfoItem {
        @SerializedName("sN")
        public String sN;
        @SerializedName("batchNumber")
        public String batchNumber;
    }
}

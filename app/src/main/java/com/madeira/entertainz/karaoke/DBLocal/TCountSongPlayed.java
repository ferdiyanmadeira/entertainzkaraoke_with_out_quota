package com.madeira.entertainz.karaoke.DBLocal;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

//todo table ini bukan temporary, untuk menyimpan, lagu ini sudah di putar berapa kali, dibuat terpisah dengan tsong, karena tsong akan selalu replace, ketika stb bootup

@Entity(tableName = "TCountSongPlayed")
public class TCountSongPlayed implements Serializable {

    @PrimaryKey @NonNull
    @SerializedName("songId")
    public String songId;
    @SerializedName("totalCount")
    public int totalCount;
}

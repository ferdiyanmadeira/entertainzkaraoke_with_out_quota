package com.madeira.entertainz.karaoke.menu_settings;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.madeira.entertainz.karaoke.Debug;
import com.madeira.entertainz.karaoke.R;
import com.madeira.entertainz.karaoke.model_online.ModelPurchase;
import com.madeira.entertainz.library.Util;

import java.net.URLEncoder;

public class DokuPaymentActivity extends AppCompatActivity {

    final static String PARENT_TAG = "DokuPaymentActivity";

    WebView webView;
    public static String KEY_DOKU_PARAMETER = "KEY_DOKU_PARAMETER";

    ModelPurchase.PurchaseItem item;

    public static void startActivity(Activity activity, ModelPurchase.PurchaseItem purchaseItem) {
        Intent intent = new Intent(activity, DokuPaymentActivity.class);
        intent.putExtra(KEY_DOKU_PARAMETER, purchaseItem);
        activity.startActivity(intent);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        String TAG = PARENT_TAG + "-onCreate";
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_doku_payment);
        try {
            bind();
            item = (ModelPurchase.PurchaseItem) getIntent().getSerializableExtra(KEY_DOKU_PARAMETER);
            setWebView();
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }


    void bind() {
        webView = findViewById(R.id.webView);
    }

    //todo set parameter2 yang dibutuhkan untuk hit api ke doku
    void setWebView() {
        String TAG = PARENT_TAG + "-setWebView";
        try {
            webView.setWebChromeClient(new WebChromeClient());
            webView.setWebViewClient(new WebViewClient());
            webView.clearCache(true);
            webView.clearHistory();
            webView.getSettings().setJavaScriptEnabled(true);
            webView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);

            String urlDoku = item.url;
            String basket =item.priceQuota.quotaId + "," + item.priceQuota.price + ",1," + item.priceQuota.price;
            String mallId = item.mallId;
            String chainMerchant = "NA";
            String currency = item.priceQuota.currency;
            String purchaseCurrency = item.priceQuota.currency;
            String amount =item.priceQuota.price;
            String purchaseAmount =item.priceQuota.price;
            String transId =item.transId;
            String sharedKey =item.sharedKey;
            String words =item.words;
            String requestDateTime = item.requestDate;
            String sessionId =Util.genHash(item.transId, item.priceQuota.quotaId);
            String paymentChannel = "";
            String email ="ferdiyan@madeirareseach.com";
            String name ="Ferdiyan Syah";
            //todo parameter kosong ini hanya di declare saja, jika suatu saat diperlukan bisa digunakan
            String address = "";
            String country = "";
            String state = "";
            String city = "";
            String province = "";
            String zipcode = "";
            String homePhone = "";
            String mobilePhone = "";
            String workPhone = "";
            String birthDate = "";

            String postData = "BASKET=" + basket +
                    "&MALLID=" + mallId +
                    "&CHAINMERCHANT=" + chainMerchant +
                    "&CURRENCY=" + currency +
                    "&PURCHASECURRENCY=" + purchaseCurrency +
                    "&AMOUNT=" + amount +
                    "&PURCHASEAMOUNT=" + purchaseAmount +
                    "&TRANSIDMERCHANT=" + transId +
                    "&SHAREDKEY=" + sharedKey +
                    "&WORDS=" + words +
                    "&REQUESTDATETIME=" + requestDateTime +
                    "&SESSIONID=" + sessionId +
                    "&PAYMENTCHANNEL=" + paymentChannel +
                    "&EMAIL=" + email +
                    "&NAME=" + name +
                    "&ADDRESS=" + address +
                    "&COUNTRY=" + country +
                    "&STATE=" + state +
                    "&CITY=" + city +
                    "&PROVINCE=" + province +
                    "&ZIPCODE=" + zipcode +
                    "&HOMEPHONE=" + homePhone +
                    "&MOBILEPHONE=" + mobilePhone +
                    "&WORKPHONE=" + workPhone +
                    "&BIRTHDATE=" + birthDate;

            //TODO hit api doku
            webView.postUrl(urlDoku, postData.getBytes());
            webView.setBackgroundColor(Color.parseColor("#919191"));
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }
}

package com.madeira.entertainz.karaoke.model;

import android.util.Log;

import com.google.gson.Gson;
import com.madeira.entertainz.karaoke.DBLocal.TSongYoutube;
import com.madeira.entertainz.karaoke.DBLocal.TSongYoutubeCategory;
import com.madeira.entertainz.library.ApiError;
import com.madeira.entertainz.library.Util;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ModelSongYoutubeLocal {

    static String TAG = "ModelSongLocal";

    public static List<TSongYoutube> geTSongYoutubeFromLocal(String filePath) {
        List<TSongYoutube> TSongYoutubeList = new ArrayList<TSongYoutube>();

//        List<TLocalFile> tLocalFileList = Util.getUSBFile("json");
//
//        for (TLocalFile tLocalFile : tLocalFileList) {
//            if (tLocalFile.fileType.equals("json") && tLocalFile.fileName.toLowerCase().equals("song.json")) {
                String stringJSON = "";
                try {
                    File file = new File(filePath);
                    FileInputStream fileInputStream = new FileInputStream(file);
                    stringJSON = Util.getJSONFromTextFile(fileInputStream);
                    Log.d(TAG, filePath);
                    Gson gson = new Gson();

                    //throw apabila ada apiError
                    try {
                        ApiError.process(gson, stringJSON);
                        ResultGeTSongYoutubeList r = gson.fromJson(stringJSON, ResultGeTSongYoutubeList.class);
                        TSongYoutubeList = r.list;
                    } catch (ApiError apiError) {
                        apiError.printStackTrace();
                    }

                    //if no error, next convert the real data

                } catch (IOException e) {
                    e.printStackTrace();
                }
//                break;
//            }
//        }


        return TSongYoutubeList;
    }

    public static List<TSongYoutubeCategory> geTSongYoutubeCategoryFromLocal(String filePath) {
        List<TSongYoutubeCategory> TSongYoutubeCategoryList = new ArrayList<TSongYoutubeCategory>();

//        List<TLocalFile> tLocalFileList = Util.getUSBFile("json");

//        for (TLocalFile tLocalFile : tLocalFileList) {
//            if (tLocalFile.fileType.equals("json") && tLocalFile.fileName.toLowerCase().equals("songcategory.json")) {
                String stringJSON = "";
                try {
                    File file = new File(filePath);
                    FileInputStream fileInputStream = new FileInputStream(file);
                    stringJSON = Util.getJSONFromTextFile(fileInputStream);
                    Log.d(TAG, filePath);
                    Gson gson = new Gson();

                    //throw apabila ada apiError
                    try {
                        ApiError.process(gson, stringJSON);
                        ResultGeTSongYoutubeCategoryList r = gson.fromJson(stringJSON, ResultGeTSongYoutubeCategoryList.class);
                        TSongYoutubeCategoryList = r.list;
                    } catch (ApiError apiError) {
                        apiError.printStackTrace();
                    }

                    //if no error, next convert the real data

                } catch (IOException e) {
                    e.printStackTrace();
                }
//                break;
//            }
//        }


        return TSongYoutubeCategoryList;
    }

    public static class ResultGeTSongYoutubeList {
        public List<TSongYoutube> list;
        public int count;
    }

    public static class ResultGeTSongYoutubeCategoryList {
        public List<TSongYoutubeCategory> list;
        public int count;
    }


}

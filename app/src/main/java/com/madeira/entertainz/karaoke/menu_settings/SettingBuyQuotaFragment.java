package com.madeira.entertainz.karaoke.menu_settings;


import android.app.AlertDialog;
import android.content.RestrictionEntry;
import android.content.res.ColorStateList;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.androidnetworking.error.ANError;
import com.madeira.entertainz.helper.RecyclerViewAdapter;
import com.madeira.entertainz.karaoke.Debug;
import com.madeira.entertainz.karaoke.R;
import com.madeira.entertainz.karaoke.config.Global;
import com.madeira.entertainz.karaoke.model_online.ModelPriceQuota;
import com.madeira.entertainz.karaoke.model_online.ModelPurchase;
import com.madeira.entertainz.library.Util;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import es.dmoral.toasty.Toasty;

/**
 * A simple {@link Fragment} subclass.
 */
public class SettingBuyQuotaFragment extends Fragment implements SettingsTheme.ISettingsTheme {

    static String PARENT_TAG = "SettingBuyQuotaFragment";
    View root;
    SettingsTheme settingsTheme;

    LinearLayout rootLL;
    RecyclerView rv;
    RecyclerViewAdapter adapter;

    List<ModelPriceQuota.PriceQuotaItem> priceQuotaItemList = new ArrayList<>();


    public SettingBuyQuotaFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        root = inflater.inflate(R.layout.fragment_setting_buy_quota, container, false);
        try {
            bind();
            settingsTheme = new SettingsTheme(getActivity(), this);
            settingsTheme.setTheme();
            fetchData();
        } catch (Exception ex) {
            Debug.e(PARENT_TAG, ex);
        }
        return root;
    }

    void bind() {
        rootLL = root.findViewById(R.id.rootLL);

        rv = root.findViewById(R.id.recycler_view);

    }

    @Override
    public void setThemeUpdateDate(Date lastUpdateTheme) {

    }

    @Override
    public void setListColorTheme(int color) {
        rootLL.setBackgroundResource(R.drawable.tags_rounded_corners);

        GradientDrawable categoryDrawable = (GradientDrawable) rootLL.getBackground();
        categoryDrawable.setColor(color);
    }

    @Override
    public void setTextAndSelectionColorTheme(ColorStateList colorSelection, ColorStateList colorText, int colorTextSelected, int colorTextFocus, int colorTextNormal, int colorSelectionSelected, int colorSelectionFocus) {
        this.colorSelection = colorSelection;
        this.colorText = colorText;
    }


    //todo get data price quota
    void fetchData() {
        String TAG = PARENT_TAG + "-fetchData";
        ModelPriceQuota.getPriceQuota(new ModelPriceQuota.IGetPriceQuota() {
            @Override
            public void onGetPriceQuota(ModelPriceQuota.PriceQuotaList result) {

                try {
                    priceQuotaItemList = result.priceQuotaItemList;
                    setRecyclerView();
                } catch (Exception ex) {
                    Debug.e(TAG, ex);
                }
            }

            @Override
            public void onError(ANError e) {
                Debug.e(TAG, e);
            }
        });

    }

    //todo set recyclerview
    void setRecyclerView() {
        String TAG = PARENT_TAG + "-setRecyclerView";
        try {
            adapter = new RecyclerViewAdapter(new RecyclerViewAdapter.IRecyclerViewAdapter() {
                @Override
                public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
                    View view = LayoutInflater.from(getActivity()).inflate(R.layout.cell_price_quota, parent, false);
                    ItemViewHolder item = new ItemViewHolder(view);
                    return item;
                }

                @Override
                public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
                    try {
                        ItemViewHolder item = (ItemViewHolder) holder;
                        item.bind(priceQuotaItemList.get(position), position);
                    } catch (Exception ex) {
                        Debug.e(PARENT_TAG, ex);
                    }
                }

                @Override
                public int getItemCount() {
                    int count = 0;
                    if (priceQuotaItemList != null)
                        count = priceQuotaItemList.size();
                    return count;
                }

            });

            GridLayoutManager gridLayoutManager = new GridLayoutManager(getActivity(), priceQuotaItemList.size());
            adapter.setHasStableIds(true);
            rv.setHasFixedSize(true);
            rv.setLayoutManager(gridLayoutManager);
            rv.setAdapter(adapter);
            rv.invalidate();
            adapter.notifyDataSetChanged();


        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    static View selectedView;
    ColorStateList colorSelection, colorText;


    class ItemViewHolder extends RecyclerView.ViewHolder {
        private static final String TAG = "ItemViewHolder";

        View root;
        ImageView logo;
        TextView quota;
        TextView currency;
        TextView price;
        LinearLayout cellLayout;

        public ItemViewHolder(View itemView) {
            super(itemView);
            root = itemView;
            selectedView = root;
            logo = itemView.findViewById(R.id.logo_image_view);
            quota = itemView.findViewById(R.id.quota_tv);
            currency = itemView.findViewById(R.id.currency_tv);
            price = itemView.findViewById(R.id.price_tv);
            cellLayout = itemView.findViewById(R.id.cell_layout);
        }

        public void bind(ModelPriceQuota.PriceQuotaItem priceQuotaItem, int pos) {
            try {

                Picasso.with(getActivity()).load(priceQuotaItem.urlLogo).into(logo);
                quota.setText(priceQuotaItem.quota);
                currency.setText(priceQuotaItem.currency);
                price.setText(priceQuotaItem.price);

//            untuk merubah warna selection
                if (colorSelection != null) cellLayout.setBackgroundTintList(colorSelection);

                root.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                    @Override
                    public void onFocusChange(View v, boolean hasFocus) {
                        if (hasFocus)
                            selectView(root);
                    }
                });


                root.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        showDialogInputEmailAndName(priceQuotaItem);

                    }
                });


            } catch (Exception ex) {
                Debug.e(TAG, ex);
            }
        }

        void selectView(View v) {
            //clear selection
            if (selectedView != null) {
                selectedView.setSelected(false);
            }

            //make selection
            selectedView = v;
            selectedView.setSelected(true);
        }
    }


    //todo show confirm payment
    void showContinuePayment(ModelPriceQuota.PriceQuotaItem priceQuotaItem, String email, String name) {
        String TAG = PARENT_TAG + "-showContinuePayment";
        try {
            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
            LayoutInflater inflater = this.getLayoutInflater();
            View dialogView = inflater.inflate(R.layout.alert_buy_quota, null);
            dialogBuilder.setView(dialogView);

            TextView packageDesc = dialogView.findViewById(R.id.package_desc_tv);
            Button cancel = dialogView.findViewById(R.id.cancel_button);
            Button payment = dialogView.findViewById(R.id.continue_button);

            String desc = priceQuotaItem.quota + " " + getString(R.string.quota) + " - " + priceQuotaItem.currency + " " + priceQuotaItem.price;
            packageDesc.setText(desc);

            AlertDialog alertDialog = dialogBuilder.create();

            cancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    alertDialog.dismiss();
                }
            });

            payment.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    purchaseQuota(priceQuotaItem.quotaId, email, name);
                    alertDialog.dismiss();
                }
            });

            alertDialog.show();
            payment.requestFocus();

            //todo set width dan height dialog
            WindowManager.LayoutParams layoutParams = alertDialog.getWindow().getAttributes();
            layoutParams.width = ViewGroup.LayoutParams.WRAP_CONTENT;
            alertDialog.show();
            alertDialog.getWindow().setLayout(430, WindowManager.LayoutParams.WRAP_CONTENT);
            Util.hideNavigationBar(getActivity());
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }

    }

    void purchaseQuota(String quotaId, String email, String name) {
        String TAG = PARENT_TAG + "-purchaseQuota";
        ModelPurchase.doPurchase(quotaId, email, name, new ModelPurchase.IDoPurchase() {
            @Override
            public void onDoPurchase(ModelPurchase.PurchaseItem result) {
                try {
                    DokuPaymentActivity.startActivity(getActivity(), result);
                } catch (Exception ex) {
                    Debug.e(TAG, ex);
                }
            }

            @Override
            public void onError(ANError e) {
                Debug.e(TAG, e);
            }
        });
    }


    void showDialogInputEmailAndName(ModelPriceQuota.PriceQuotaItem priceQuotaItem) {
        String TAG = PARENT_TAG + "-showDialogInputEmailAndName";
        try {
            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
            LayoutInflater inflater = this.getLayoutInflater();
            View dialogView = inflater.inflate(R.layout.alert_buy_quota_email, null);
            dialogBuilder.setView(dialogView);

            EditText emailET = dialogView.findViewById(R.id.email_et);
            EditText nameET = dialogView.findViewById(R.id.name_et);
            String email = Global.getEmail();
            String name = Global.getName();
            emailET.setText(email);
            nameET.setText(name);
            Button cancel = dialogView.findViewById(R.id.cancel_button);
            Button payment = dialogView.findViewById(R.id.continue_button);

            AlertDialog alertDialog = dialogBuilder.create();

            cancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    alertDialog.dismiss();
                }
            });

            payment.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //todo check apakah sudah valid email format dan nama tidak kosong
                    String email = emailET.getText().toString();
                    String name = nameET.getText().toString();

                    boolean resultEmail = !TextUtils.isEmpty(email) && android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
                    boolean resultName = !TextUtils.isEmpty(name);

                    //todo purchase quota
                    if (resultEmail && resultName) {
                        Global.setEmail(email);
                        Global.setName(name);
                        showContinuePayment(priceQuotaItem, email, name);
                        alertDialog.dismiss();
                    } else {
                        Toasty.error(getActivity(), "Email or name is not valid").show();
                    }
                }
            });

            alertDialog.show();
            payment.requestFocus();

            //todo set width dan height dialog
            WindowManager.LayoutParams layoutParams = alertDialog.getWindow().getAttributes();
            layoutParams.width = ViewGroup.LayoutParams.WRAP_CONTENT;
            alertDialog.show();
            alertDialog.getWindow().setLayout(430, WindowManager.LayoutParams.WRAP_CONTENT);
            Util.hideNavigationBar(getActivity());
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }


}

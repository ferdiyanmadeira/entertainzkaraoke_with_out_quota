package com.madeira.entertainz.karaoke.model;

import android.util.Log;

import com.google.gson.Gson;
import com.madeira.entertainz.karaoke.DBLocal.TElement;
import com.madeira.entertainz.karaoke.DBLocal.TElementMedia;
import com.madeira.entertainz.karaoke.DBLocal.TLocalFile;
import com.madeira.entertainz.library.ApiError;
import com.madeira.entertainz.library.Util;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.List;

public class ModelThemeLocal {
    static String TAG = "ModelThemeLocal";

    public static ResultGetThemeList getThemeFromLocal(String filePath) {
//        List<TLocalFile> tLocalFileList = Util.getUSBFile("json");
        ResultGetThemeList resultGetThemeList = null;
//        for (TLocalFile tLocalFile : tLocalFileList) {
//            if (tLocalFile.fileType.equals("json") && tLocalFile.fileName.toLowerCase().equals("theme.json")) {
                String stringJSON = "";
                try {
                    File file = new File(filePath);
                    FileInputStream fileInputStream = new FileInputStream(file);
                    stringJSON = Util.getJSONFromTextFile(fileInputStream);
//                    Log.d(TAG, tLocalFile.content);
                    Gson gson = new Gson();

                    //throw apabila ada apiError
                    try {
                        ApiError.process(gson, stringJSON);
                        resultGetThemeList = gson.fromJson(stringJSON, ResultGetThemeList.class);
                        return resultGetThemeList;
                    } catch (ApiError apiError) {
                        apiError.printStackTrace();
                    }

                    //if no error, next convert the real data

                } catch (IOException e) {
                    e.printStackTrace();
                }
//                break;
//
//            }
//        }


        return resultGetThemeList;
    }

    public class ResultGetThemeList {
        public List<TElement> list;
        public int count;
        public List<TElementMedia> media;
    }

}

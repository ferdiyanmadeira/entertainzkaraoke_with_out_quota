package com.madeira.entertainz.karaoke.menu_messages;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Handler;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ImageView;

import com.madeira.entertainz.karaoke.DBLocal.JoinSettingElementMedia;
import com.madeira.entertainz.karaoke.DBLocal.TSticker;
import com.madeira.entertainz.karaoke.Debug;
import com.madeira.entertainz.karaoke.R;
import com.madeira.entertainz.karaoke.config.C;
import com.madeira.entertainz.karaoke.config.Global;
import com.madeira.entertainz.karaoke.menu_main.BottomNavigationFragment;
import com.madeira.entertainz.karaoke.menu_main.ListMoodFragment;
import com.madeira.entertainz.karaoke.menu_navigation.TopNavigationFragment;
import com.madeira.entertainz.karaoke.model_room_db.ModelElementMedia;
import com.madeira.entertainz.library.Util;

import java.io.File;
import java.io.InputStream;
import java.util.List;

import static com.madeira.entertainz.karaoke.config.CacheData.lastIndexBackground;

public class MessagesActivity extends AppCompatActivity {
    String TAG = "MessagesActivity";
    ImageView backgroundIV;
    //    ProgressBar progressBar;
    View listMoodView;

    Handler handler = new Handler();
    Runnable runnable;
    TopNavigationFragment topNavigationFragment = new TopNavigationFragment();
    BottomNavigationFragment bottomNavigationFragment = new BottomNavigationFragment();
    MessageListFragment messageListFragment = new MessageListFragment();
    MessageDetailFragment messageDetailFragment = new MessageDetailFragment();
    ListMoodFragment listMoodFragment = new ListMoodFragment();
    Handler handlerConnection = new Handler();
    Runnable runnableConnection;

    public static void startActivity(Activity activity) {
        Intent intent = new Intent(activity, MessagesActivity.class);
        activity.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        try {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_messages);
            bind();
            bindFragment();
            fetchListBackground();
            Util.hideNavigationBar(this);
            setActionObject();

            String languageId = Global.getLanguageId();
            Util.setLanguage(this, languageId);
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    void bind() {
        try {
            backgroundIV = (ImageView) findViewById(R.id.backgroundIV);
            listMoodView = findViewById(R.id.listMoodFrameLayout);
//            progressBar = findViewById(R.id.progressBar);
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    void bindFragment() {
//        topNavigationFragment = topNavigationFragment.newInstance(true);
        try {
            messageListFragment = new MessageListFragment();
            topNavigationFragment = topNavigationFragment.newInstance(C.MENU_MESSAGES);
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.topNavFrameLayout, topNavigationFragment);
            ft.replace(R.id.bottomFrameLayout, bottomNavigationFragment);
            ft.replace(R.id.categoryFrameLayout, messageListFragment);
            ft.replace(R.id.listMoodFrameLayout, listMoodFragment);
            ft.commit();

            messageListFragment.setCallback(iMessageListListener);
            bottomNavigationFragment.setCallback(bottomNavigationFragmentListener);
            listMoodFragment.setCallBack(listMoodListener);
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    void fetchListBackground() {
        ModelElementMedia.getListElementMediaActive(this, new ModelElementMedia.IGetListElementMediaActive() {
            @Override
            public void onGetListElementMedia(List<JoinSettingElementMedia> joinSettingElementMediaList) {
                int totalIndex = joinSettingElementMediaList.size();
                runnable = new Runnable() {
                    @Override
                    public void run() {
                        try {
                            if (joinSettingElementMediaList.size() != 0) {
                                if (lastIndexBackground >= totalIndex)
                                    lastIndexBackground = 0;

                                JoinSettingElementMedia joinSettingElementMedia = joinSettingElementMediaList.get(lastIndexBackground);
                                Uri uri = Uri.fromFile(new File(joinSettingElementMedia.urlImage));
//                            Picasso.with(KaraokeActivity.this).load(uri).into(backgroundIV);
                                InputStream inputStream = getContentResolver().openInputStream(uri);
                                BitmapFactory.Options options = new BitmapFactory.Options();
                                options.inPreferredConfig = Bitmap.Config.RGB_565;
                                Bitmap image = BitmapFactory.decodeStream(inputStream, null, options);
                                backgroundIV.setImageBitmap(image);

                                lastIndexBackground++;
                                handler.postDelayed(this::run, joinSettingElementMedia.duration);
                            }
                        } catch (Exception ex) {
                            Debug.e(TAG, ex);
                        }
                    }
                };
                handler.postDelayed(runnable, 100);
            }
        });
    }

    @Override
    public void onBackPressed() {
        try {
            super.onBackPressed();
            //agar runable tidak running ketika di finish activity
            handler.removeCallbacks(runnable);
            handler.removeCallbacksAndMessages(null);
            handlerConnection.removeCallbacks(runnableConnection);
            handlerConnection.removeCallbacksAndMessages(null);
            finish();
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    MessageListFragment.IMessageListListener iMessageListListener = new MessageListFragment.IMessageListListener() {
        @Override
        public void onMessageListClick(int messageId) {
            try {
                messageDetailFragment = new MessageDetailFragment();
                Bundle bundle = new Bundle();
                bundle.putInt(MessageDetailFragment.KEY_MESSAGE_ID, messageId);
                messageDetailFragment.setArguments(bundle);
                FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                ft.replace(R.id.detailFrameLayout, messageDetailFragment).addToBackStack(null);
                ft.commit();
            } catch (Exception ex) {
                Debug.e(TAG, ex);
            }
        }
    };


    void setActionObject() {

    }

    void getMessage() {
        /*progressBar.setVisibility(View.VISIBLE);
        try {
            String mac = Util.getMACAddress("eth0");
            mac = mac.replace(":", "");
            ModelMessage.getMessageAll(mac, new ModelMessage.IModelMessage() {
                @Override
                public void onGetAll(ModelMessage.messageListItem result) {
                    progressBar.setVisibility(View.GONE);
                    List<ModelMessage.messageItem> messageItems = result.messageItems;
                    for (ModelMessage.messageItem item : messageItems) {
                        insertMessageToRoom(item);
                    }
                    bindFragment();

                }

                @Override
                public void onError(ANError e) {
                    Debug.e(TAG, e);
                }
            });
        } catch (Exception ex) {
            progressBar.setVisibility(View.GONE);
            Debug.e(TAG, ex);
        }*/

    }

   /* void insertMessageToRoom(ModelMessage.messageItem messageItem) {
        PerformAsync2.run(performAsync ->
        {
            int result = 0;
            try {
                DbAccess dbAccess = DbAccess.Instance.create(MessagesActivity.this);
                TMessage tMessage = new TMessage();
                tMessage.messageId = Integer.valueOf(messageItem.messageId);
                tMessage.message = messageItem.message;
                tMessage.title = messageItem.title;
                tMessage.from = messageItem.from;
                tMessage.createDate = messageItem.createdDate;
                tMessage.status = C.MESSAGE_STATUS_NEW;
                dbAccess.daoAccessMessage().insert(tMessage);
                dbAccess.close();
                result = 1;

            } catch (Exception ex) {

            }

            return result;
        }).setCallbackResult(result ->
        {
            int r = (int) result;

        });
    }*/

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
       /* try {
            if (keyCode == KeyEvent.KEYCODE_DPAD_RIGHT || keyCode == KeyEvent.KEYCODE_DPAD_UP || keyCode == KeyEvent.KEYCODE_DPAD_LEFT || keyCode == KeyEvent.KEYCODE_DPAD_DOWN) {
                if (progressBar.getVisibility() == View.VISIBLE)
                    return true;
            }

        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }*/
        return super.onKeyDown(keyCode, event);
    }


    /**
     * listener ketika mood di click
     */
    BottomNavigationFragment.IMoodFragmentListener bottomNavigationFragmentListener = new BottomNavigationFragment.IMoodFragmentListener() {
        @Override
        public void onMoodClick() {
            listMoodView.setVisibility(View.VISIBLE);
        }
    };

    /**
     * listener ketika mood dipilih
     */
    ListMoodFragment.IListMoodListener listMoodListener = new ListMoodFragment.IListMoodListener() {
        @Override
        public void onClick(TSticker tSticker) {
            try {
                listMoodView.setVisibility(View.GONE);
                bottomNavigationFragment.updateMood(tSticker);
                Global.setStickerId(tSticker.stickerId);
            } catch (Exception ex) {
                Debug.e(TAG, ex);
            }
        }
    };


    @Override
    public void onDestroy() {
        try {
            super.onDestroy();
            Util.freeMemory();
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }
}

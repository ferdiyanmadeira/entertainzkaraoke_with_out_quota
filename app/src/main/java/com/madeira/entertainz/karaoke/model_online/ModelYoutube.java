package com.madeira.entertainz.karaoke.model_online;

import android.content.Context;
import android.net.Uri;
import android.util.Log;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.OkHttpResponseAndJSONObjectRequestListener;
import com.google.gson.Gson;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.madeira.entertainz.karaoke.DBLocal.DbYoutube;
import com.madeira.entertainz.karaoke.DBLocal.TSongYoutube;
import com.madeira.entertainz.karaoke.DBLocal.TSongYoutubeCategory;
import com.madeira.entertainz.karaoke.DBLocal.TSongYoutubePlaylist;
import com.madeira.entertainz.karaoke.DBLocal.TYoutubePlaylist;
import com.madeira.entertainz.karaoke.Debug;
import com.madeira.entertainz.karaoke.config.Global;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.net.InetAddress;
import java.security.cert.CertificateException;
import java.util.List;

import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import okhttp3.OkHttpClient;
import okhttp3.Response;

import static com.madeira.entertainz.karaoke.config.C.DEFAULT_API;

public class ModelYoutube {
    private static final String TAG = "ModelYoutube";

    private static final String APIHOST = Global.getApiHostname() + "/v1/youtube.php";

    public interface IGetYoutube {
        void onGetYoutube(listYoutubeItem item);

        void onError(ANError e);
    }

    static public void getYoutube(int limit, int offset, int genre, IGetYoutube callBack) {
        try {
            String uri = Uri.parse(APIHOST)
                    .buildUpon()
                    .appendQueryParameter("action", "get_all_youtube")
                    .appendQueryParameter("limit", String.valueOf(limit))
                    .appendQueryParameter("offset", String.valueOf(offset))
                    .appendQueryParameter("genre", String.valueOf(genre))
                    .build().toString();

            AndroidNetworking.get(uri)
                    .setPriority(Priority.MEDIUM)
                    .build()
                    .getAsOkHttpResponseAndJSONObject(new OkHttpResponseAndJSONObjectRequestListener() {
                        @Override
                        public void onResponse(Response okHttpResponse, JSONObject response) {
                            try {
                                if (okHttpResponse.isSuccessful()) {
                                    Gson gson = new Gson();
                                    String jsonString = response.toString();
                                    listYoutubeItem item = gson.fromJson(jsonString, listYoutubeItem.class);
                                    callBack.onGetYoutube(item);
                                }
                            } catch (Exception ex) {

                            }
                        }

                        @Override
                        public void onError(ANError anError) {
                            callBack.onError(anError);
                        }
                    });
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }


    public interface IGetYoutubeGenre {
        void onGetYoutubeGenre(listYoutubeCategoryItem item);

        void onError(ANError e);
    }

    static public void getYoutubeGenre( IGetYoutubeGenre callBack) {
        try {
            String uri = Uri.parse(APIHOST)
                    .buildUpon()
                    .appendQueryParameter("action", "get_genre")
                    .build().toString();

            AndroidNetworking.get(uri)
                    .setPriority(Priority.MEDIUM)
                    .build()
                    .getAsOkHttpResponseAndJSONObject(new OkHttpResponseAndJSONObjectRequestListener() {
                        @Override
                        public void onResponse(Response okHttpResponse, JSONObject response) {
                            try {
                                if (okHttpResponse.isSuccessful()) {
                                    Gson gson = new Gson();
                                    String jsonString = response.toString();
                                    listYoutubeCategoryItem item = gson.fromJson(jsonString, listYoutubeCategoryItem.class);
                                    callBack.onGetYoutubeGenre(item);
                                }
                            } catch (Exception ex) {

                            }
                        }

                        @Override
                        public void onError(ANError anError) {
                            callBack.onError(anError);
                        }
                    });
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    public interface IGetYoutubeSearch {
        void onGetYoutubeSearch(listYoutubeItem item);
        void onError(ANError e);
    }

    static public void getYoutubeSearch(int limit, int offset, String keyword, IGetYoutube callBack) {
        try {
            String uri = Uri.parse(APIHOST)
                    .buildUpon()
                    .appendQueryParameter("action", "search_karaoke")
                    .appendQueryParameter("limit", String.valueOf(limit))
                    .appendQueryParameter("offset", String.valueOf(offset))
                    .appendQueryParameter("keyword", String.valueOf(keyword))
                    .build().toString();

            AndroidNetworking.get(uri)
                    .setPriority(Priority.MEDIUM)
                    .build()
                    .getAsOkHttpResponseAndJSONObject(new OkHttpResponseAndJSONObjectRequestListener() {
                        @Override
                        public void onResponse(Response okHttpResponse, JSONObject response) {
                            try {
                                if (okHttpResponse.isSuccessful()) {
                                    Gson gson = new Gson();
                                    String jsonString = response.toString();
                                    listYoutubeItem item = gson.fromJson(jsonString, listYoutubeItem.class);
                                    callBack.onGetYoutube(item);
                                }
                            } catch (Exception ex) {

                            }
                        }

                        @Override
                        public void onError(ANError anError) {
                            callBack.onError(anError);
                        }
                    });
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }


    public interface IGetPopularYoutube {
        void onGetPopularYoutube(ResultGetVideos item);

        void onError(ANError e);
    }

    static public void getPopularYoutube(Context context, IGetPopularYoutube callBack) {
        try {
            String uri = Uri.parse(APIHOST)
                    .buildUpon()
                    .appendQueryParameter("action", "get_popular_youtube")
                    .appendQueryParameter("chart", "mostPopular")
                    .appendQueryParameter("key", "AIzaSyDV0B9sClRimMBdOFZvqUCvzQT6Q10mQL0")
                    .appendQueryParameter("part", "snippet")
                    .appendQueryParameter("maxResults", "48")
                    .appendQueryParameter("regionCode", "ID")
                    //videoCategoryId =10 untuk menampilkan video dgn category music
                    .appendQueryParameter("videoCategoryId", "10")
                    .build().toString();
            AndroidNetworking.get(uri)
                    .setPriority(Priority.MEDIUM)
                    .build()
                    .getAsOkHttpResponseAndJSONObject(new OkHttpResponseAndJSONObjectRequestListener() {
                        @Override
                        public void onResponse(Response okHttpResponse, JSONObject response) {
                            if (okHttpResponse.isSuccessful()) {
                                Gson gson = new Gson();
                                String jsonString = response.toString();
                                ResultGetVideos item = gson.fromJson(jsonString, ResultGetVideos.class);
                                callBack.onGetPopularYoutube(item);
                            }

                        }

                        @Override
                        public void onError(ANError anError) {
                            callBack.onError(anError);
                        }
                    });
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    public interface IGetSearchYoutube {
        void onGetSearchYoutube(ResultGetSearch item);

        void onError(ANError e);
    }

    static public void getSearchYoutube(String keyword, String pageToken, IGetSearchYoutube callBack) {
        try {
            String uri = Uri.parse(APIHOST)
                    .buildUpon()
                    .appendQueryParameter("action", "search")
                    .appendQueryParameter("key", "AIzaSyDV0B9sClRimMBdOFZvqUCvzQT6Q10mQL0")
                    .appendQueryParameter("part", "snippet")
                    .appendQueryParameter("maxResults", "48")
                    .appendQueryParameter("q", keyword)
                    .appendQueryParameter("pageToken", pageToken)
                    .build().toString();
            AndroidNetworking.get(uri)
                    .setPriority(Priority.MEDIUM)
                    .build()
                    .getAsOkHttpResponseAndJSONObject(new OkHttpResponseAndJSONObjectRequestListener() {
                        @Override
                        public void onResponse(Response okHttpResponse, JSONObject response) {
                            if (okHttpResponse.isSuccessful()) {
                                Gson gson = new Gson();
                                String jsonString = response.toString();
                                ResultGetSearch item = gson.fromJson(jsonString, ResultGetSearch.class);
                                callBack.onGetSearchYoutube(item);
                            }

                        }

                        @Override
                        public void onError(ANError anError) {
                            callBack.onError(anError);
                        }
                    });
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }


    public interface IGetYoutubeById {
        void onGetYoutubeById(listYoutubeItem item);

        void onError(ANError e);
    }

    static public void getYoutubeId(List<TSongYoutubePlaylist> tSongYoutubePlaylists , IGetYoutubeById callBack) {
        try {
            JSONObject jsonObject = new JSONObject();
            try {
                JSONArray jsonArray = new JSONArray();
                for (TSongYoutubePlaylist tSongYoutubePlaylist : tSongYoutubePlaylists) {
                    JSONObject tempYoutubeId = new JSONObject();
                    tempYoutubeId.put("youtube_id", tSongYoutubePlaylist.songId);
                    jsonArray.put(tempYoutubeId);
                }
                jsonObject.put("list_youtube_id", jsonArray);
                String value = jsonObject.toString();

                AndroidNetworking.post(APIHOST)
                        .addJSONObjectBody(jsonObject) // posting json
//                        .setTag("test")
                        .addQueryParameter("action", "youtube_detail")
                        .setPriority(Priority.MEDIUM)
                        .build()
                        .getAsOkHttpResponseAndJSONObject(new OkHttpResponseAndJSONObjectRequestListener() {
                            @Override
                            public void onResponse(Response okHttpResponse, JSONObject response) {
                                try {
                                    JSONObject jsonObject1 = null;
                                    if (okHttpResponse.isSuccessful()) {
                                        Gson gson = new Gson();
                                        String jsonString = response.toString();
                                        listYoutubeItem item = gson.fromJson(jsonString, listYoutubeItem.class);
                                        callBack.onGetYoutubeById(item);
                                    }
                                } catch (Exception ex) {

                                }
                            }

                            @Override
                            public void onError(ANError anError) {

                                callBack.onError(anError);
                            }
                        });

            } catch (JSONException e) {
                e.printStackTrace();
            }


        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    public class YoutubeId implements Serializable {
        @SerializedName("youtube_id")
        public String youtubeId;
    }


    public class listYoutubeItem {
        @SerializedName("list_youtube")
        public List<TSongYoutube> listYoutube;
        @SerializedName("total_song")
        public int totalSong;
        /*@SerializedName("list_genre")
        public List<ModelSong.genreItem> listGenre;*/

    }

    public class listYoutubeCategoryItem {
            @SerializedName("list_genre")
        public List<TSongYoutubeCategory> listYoutubeCategory;

    }
    public class youtubeItem {
        @SerializedName("youtube_id")
        public String youtubeId;
        @SerializedName("songName")
        public String songName;
        @SerializedName("artist")
        public String artist;
        @SerializedName("genre_id")
        public String genreId;
        @SerializedName("songURL")
        public String songURL;
        @SerializedName("labelName")
        public String labelName;
        @SerializedName("songWriter")
        public String songWriter;
        @SerializedName("uploadDate")
        public String uploadDate;
        @SerializedName("duration")
        public String duration;
        @SerializedName("thumbnail")
        public String thumbnail;
        @SerializedName("lyric")
        public String lyric;
    }


    public class ResultGetVideos {
        @SerializedName("kind")
        @Expose
        public String kind;
        @SerializedName("etag")
        @Expose
        public String etag;
        @SerializedName("nextPageToken")
        @Expose
        public String nextPageToken;
        @SerializedName("prevPageToken")
        @Expose
        public String prevPageToken;
        @SerializedName("pageInfo")
        @Expose
        public PageInfoVideos pageInfo;
        @SerializedName("items")
        @Expose
        public List<ItemVideos> items = null;
    }

    public class PageInfoVideos {
        @SerializedName("totalResults")
        @Expose
        public Integer totalResults;
        @SerializedName("resultsPerPage")
        @Expose
        public Integer resultsPerPage;
    }

    public class ItemVideos {
        @SerializedName("kind")
        @Expose
        public String kind;
        @SerializedName("etag")
        @Expose
        public String etag;
        @SerializedName("id")
        @Expose
        public String id;
        @SerializedName("snippet")
        @Expose
        public SnippetVideos snippet;
    }

    public class SnippetVideos {
        @SerializedName("publishedAt")
        @Expose
        public String publishedAt;
        @SerializedName("channelId")
        @Expose
        public String channelId;
        @SerializedName("title")
        @Expose
        public String title;
        @SerializedName("description")
        @Expose
        public String description;
        @SerializedName("thumbnails")
        @Expose
        public ThumbnailsVideos thumbnails;
        @SerializedName("channelTitle")
        @Expose
        public String channelTitle;
        @SerializedName("tags")
        @Expose
        public List<String> tags = null;
        @SerializedName("categoryId")
        @Expose
        public String categoryId;
        @SerializedName("liveBroadcastContent")
        @Expose
        public String liveBroadcastContent;
        @SerializedName("defaultLanguage")
        @Expose
        public String defaultLanguage;
        @SerializedName("localized")
        @Expose
        public LocalizedVideos localized;
        @SerializedName("defaultAudioLanguage")
        @Expose
        public String defaultAudioLanguage;
    }

    public class ThumbnailsVideos {
        @SerializedName("default")
        @Expose
        public Default _default;
        @SerializedName("medium")
        @Expose
        public Medium medium;
        @SerializedName("high")
        @Expose
        public High high;
        @SerializedName("standard")
        @Expose
        public Standard standard;
        @SerializedName("maxres")
        @Expose
        public Maxres maxres;
    }

    public class LocalizedVideos {
        @SerializedName("title")
        @Expose
        public String title;
        @SerializedName("description")
        @Expose
        public String description;
    }

    public class Standard {
        @SerializedName("url")
        @Expose
        public String url;
        @SerializedName("width")
        @Expose
        public Integer width;
        @SerializedName("height")
        @Expose
        public Integer height;
    }

    public class Default {
        @SerializedName("url")
        @Expose
        public String url;
        @SerializedName("width")
        @Expose
        public Integer width;
        @SerializedName("height")
        @Expose
        public Integer height;
    }

    public class High {
        @SerializedName("url")
        @Expose
        public String url;
        @SerializedName("width")
        @Expose
        public Integer width;
        @SerializedName("height")
        @Expose
        public Integer height;
    }

    public class Medium {
        @SerializedName("url")
        @Expose
        public String url;
        @SerializedName("width")
        @Expose
        public Integer width;
        @SerializedName("height")
        @Expose
        public Integer height;
    }

    public class Maxres {
        @SerializedName("url")
        @Expose
        public String url;
        @SerializedName("width")
        @Expose
        public Integer width;
        @SerializedName("height")
        @Expose
        public Integer height;
    }


    public class ResultGetSearch {
        @SerializedName("kind")
        @Expose
        public String kind;
        @SerializedName("etag")
        @Expose
        public String etag;
        @SerializedName("nextPageToken")
        @Expose
        public String nextPageToken;
        @SerializedName("prevPageToken")
        @Expose
        public String prevPageToken;
        @SerializedName("regionCode")
        @Expose
        public String regionCode;
        @SerializedName("pageInfo")
        @Expose
        public PageInfoSearch pageInfo;
        @SerializedName("items")
        @Expose
        public List<ItemSearch> items = null;
    }

    public class PageInfoSearch {
        @SerializedName("totalResults")
        @Expose
        public Integer totalResults;
        @SerializedName("resultsPerPage")
        @Expose
        public Integer resultsPerPage;
    }

    public class ItemSearch {
        @SerializedName("kind")
        @Expose
        public String kind;
        @SerializedName("etag")
        @Expose
        public String etag;
        @SerializedName("id")
        @Expose
        public IdSearch id;
        @SerializedName("snippet")
        @Expose
        public SnippetSearch snippet;
    }

    public class IdSearch {
        @SerializedName("kind")
        @Expose
        public String kind;
        @SerializedName("channelId")
        @Expose
        public String channelId;
        @SerializedName("videoId")
        @Expose
        public String videoId;
    }

    public class SnippetSearch {
        @SerializedName("publishedAt")
        @Expose
        public String publishedAt;
        @SerializedName("channelId")
        @Expose
        public String channelId;
        @SerializedName("title")
        @Expose
        public String title;
        @SerializedName("description")
        @Expose
        public String description;
        @SerializedName("thumbnails")
        @Expose
        public ThumbnailsSearch thumbnails;
        @SerializedName("channelTitle")
        @Expose
        public String channelTitle;
        @SerializedName("liveBroadcastContent")
        @Expose
        public String liveBroadcastContent;
    }

    public class ThumbnailsSearch {
        @SerializedName("default")
        @Expose
        public Default _default;
        @SerializedName("medium")
        @Expose
        public Medium medium;
        @SerializedName("high")
        @Expose
        public High high;
    }

}

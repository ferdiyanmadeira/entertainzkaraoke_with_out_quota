package com.madeira.entertainz.karaoke.DBLocal;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

@Entity(tableName = "TSongYoutube")
public class TSongYoutube implements Serializable {

    @PrimaryKey @NonNull
    @SerializedName("youtube_id")
    public String songId;
    @SerializedName("songName")
    public String songName;
    @SerializedName("artist")
    public String artist;
    @SerializedName("genre_id")
    public int songCategoryId;
    @SerializedName("songCategory")
    public String songCategory;
    @SerializedName("songURL")
    public String songURL;
    @SerializedName("vocalSound")
    public int vocalSound;
    @SerializedName("duration")
    public int duration;
    @SerializedName("thumbnail")
    @Nullable
    public String thumbnail;
    @Nullable
    @SerializedName("lyric")
    public String lyric;
    @SerializedName("from_server")
    public int fromServer;
}

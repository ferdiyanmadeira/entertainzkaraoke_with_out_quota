package com.madeira.entertainz.karaoke.model;

import android.net.Uri;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;
import com.madeira.entertainz.karaoke.config.Global;
import com.madeira.entertainz.library.ApiError;
import com.madeira.entertainz.library.PerformAsync2;
import com.madeira.entertainz.library.Util;

import java.util.List;

public class ModelInstaller {
    private static final String TAG = "ModelInstaller";

    private static final String APIHOST = Global.getApiHostname() + "/v1/installer.php";


    public interface ILogin {
        void onLogin(ResultLogin result);
        void onError(Exception e);
    }
    public interface ICreateSession {
        void onCreateSession(String stbSessionId);
        void onError(Exception e);
    }
    public interface IGetStbPartial {
        void onGetStbPartial(List<ResultGetStbPartial.Stb> list);
        void onError(Exception e);
    }

    static public void login(String username, String password, ILogin callback){

        PerformAsync2.run(new PerformAsync2.Callback() {
            @Override
            public Object onBackground(PerformAsync2 performAsync) {

                String hash = Util.genHash(username, password);

                String url = Uri.parse(APIHOST)
                        .buildUpon()
                        .appendQueryParameter("action", "login")
                        .appendQueryParameter("username", username)
                        .appendQueryParameter("hash", hash)
                        .build().toString();

                String result = Util.readText(url);

                Log.i(TAG,"url="+url );
                Log.i(TAG,"result="+result);

                //null karena ada connection error
                if (result==null) return null;

                Gson gson = new Gson();

                //return null apabila ada error
                ApiError apiError = gson.fromJson(result, ApiError.class);
                if (apiError.errCode!=null){
                    return apiError;
                }

                //if no error, next convert the real data
                ResultLogin r = gson.fromJson(result, ResultLogin.class);

                return r;
            }
        }).setCallbackResult( result -> {
            if (result instanceof Exception){
                callback.onError((Exception) result);
            }else{
                callback.onLogin((ResultLogin) result);
            }
        });
    }

    static public void createSession(String adminSessionId, String salt, int stbId, ICreateSession callback){
        PerformAsync2.run(new PerformAsync2.Callback() {
            @Override
            public Object onBackground(PerformAsync2 performAsync) {

                String sig = Util.genHash(String.valueOf(stbId), salt);

                String url = Uri.parse(APIHOST)
                        .buildUpon()
                        .appendQueryParameter("action", "create_session")
                        .appendQueryParameter("sessionId", adminSessionId)
                        .appendQueryParameter("sig", sig)
                        .appendQueryParameter("stbId", String.valueOf(stbId))
                        .build().toString();

                String result = Util.readText(url);

                Log.i(TAG,"url="+url );
                Log.i(TAG,"result="+result);

                Gson gson = new Gson();

                //1. Blank
                //2. Not Json
                //3. Api Error
                ApiError apiError = ApiError.parseApiError(gson, result);
                if (apiError!=null) return apiError;

                //if no error, next convert the real data
                ResultCreateSession r = gson.fromJson(result, ResultCreateSession.class);

                return r.stbSessionId;
            }
        }).setCallbackResult(result -> {

            if (result instanceof Exception){
                callback.onError((Exception) result);
            } else {
                callback.onCreateSession((String) result);
            }
        });
    }

    static public void getStbPartial(String sessionId, String salt, String keyword,
                                     int offset, int limit, IGetStbPartial callback){

        PerformAsync2.run(new PerformAsync2.Callback() {
            @Override
            public Object onBackground(PerformAsync2 performAsync) {

                String sig = Util.genHash(keyword + String.valueOf(offset), salt);

                String url = Uri.parse(APIHOST)
                        .buildUpon()
                        .appendQueryParameter("action", "get_stb_list")
                        .appendQueryParameter("sessionId", sessionId)
                        .appendQueryParameter("sig", sig)
                        .appendQueryParameter("keyword", keyword)
                        .appendQueryParameter("offset", String.valueOf(offset))
                        .appendQueryParameter("limit", String.valueOf(limit))
                        .build().toString();

                String result = Util.readText(url);
                Log.i(TAG,"url="+url );
                Log.i(TAG,"result="+result);

                Gson gson = new Gson();

                //1. Blank
                //2. Not Json
                //3. Api Error
                ApiError apiError = ApiError.parseApiError(gson, result);
                if (apiError!=null) return apiError;

                //if no error, next convert the real data
                ResultGetStbPartial r = gson.fromJson(result, ResultGetStbPartial.class);

                return r.list; //true, apabila result==1
            }
        }).setCallbackResult(new PerformAsync2.CallbackResult() {
            @Override
            public void onResult(Object result) {
                if (result instanceof Exception){
                    callback.onError((Exception) result);
                } else {
                    callback.onGetStbPartial((List<ResultGetStbPartial.Stb>) result);
                }
            }
        });
    }

    public class ResultLogin {
        public int result;

        @SerializedName("admin_session_id")
        public String adminSessionId; //sessionId ini berbeda dgn stb_session

        public String salt;
    }

    public class ResultCreateSession {
        public int result;
        @SerializedName("stb_session_id")
        public String stbSessionId;
    }

    public class ResultGetStbPartial {

        public int count;
        public List<Stb> list = null;

        public class Stb {

            @SerializedName("stb_id")
            public int stbId;
            @SerializedName("stb_name")
            public String stbName;
            @SerializedName("ip_address")
            public Object ipAddress;
            @SerializedName("room_id")
            public int roomId;
            @SerializedName("name")
            public String roomName;
            @SerializedName("location")
            public String location;
        }
    }
}

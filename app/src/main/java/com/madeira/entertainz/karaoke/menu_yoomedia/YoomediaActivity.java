package com.madeira.entertainz.karaoke.menu_yoomedia;

import android.app.Activity;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.VideoView;

import com.madeira.entertainz.karaoke.Debug;
import com.madeira.entertainz.karaoke.R;
import com.madeira.entertainz.karaoke.config.Global;
import com.madeira.entertainz.library.Util;

public class YoomediaActivity extends AppCompatActivity {

    static String TAG = "YoomediaActivity";
    VideoView videoView;

    public static void startActivity(Activity activity) {
        try {
            Intent intent = new Intent(activity, YoomediaActivity.class);
            activity.startActivity(intent);
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            setContentView(R.layout.activity_yoomedia);
            bind();
            setupTV();
            Util.hideNavigationBar(YoomediaActivity.this);

            String languageId = Global.getLanguageId();
            Util.setLanguage(this, languageId);
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    void bind() {
        videoView = findViewById(R.id.videoView);
    }

    void setupTV() {
        try {
            Uri url = Uri.parse("https://video2.onlivestreaming.net/yoolive/livestream/playlist.m3u8");
            videoView.setVideoURI(url);
            videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    videoView.start();
                    mp.start();
                    mp.setOnVideoSizeChangedListener(new MediaPlayer.OnVideoSizeChangedListener() {
                        @Override
                        public void onVideoSizeChanged(MediaPlayer mp, int arg1, int arg2) {
                            mp.start();
                        }
                    });
                }
            });
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    @Override
    public void onDestroy()
    {
        try
        {
            super.onDestroy();
            Util.freeMemory();
        }
        catch (Exception ex)
        {
            Debug.e(TAG, ex);
        }
    }
}

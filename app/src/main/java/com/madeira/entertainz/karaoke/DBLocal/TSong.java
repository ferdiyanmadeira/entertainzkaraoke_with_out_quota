package com.madeira.entertainz.karaoke.DBLocal;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

@Entity(tableName = "tsong")
public class TSong implements Serializable {

    @PrimaryKey @NonNull
    @SerializedName("songId")
    public String songId;
    @SerializedName("songName")
    public String songName;
    @SerializedName("artist")
    public String artist;
    @SerializedName("songCategoryId")
    public int songCategoryId;
    @SerializedName("songURL")
    public String songURL;
    @SerializedName("vocalSound")
    public String vocalSound;
    @SerializedName("duration")
    public String duration;
    @SerializedName("thumbnail")
    public String thumbnail;
    @SerializedName("iv")
    public String iv;
    @SerializedName("key")
    public String key;
    @SerializedName("lyric")
    public String lyric;
}

package com.madeira.eznet;

import android.util.Log;

public class HeaderItem {
    private static final String TAG = "HeaderItem";

    public String key;
    public String value;

    /**
     * parse header dari string
     * @return
     */
    static public HeaderItem parse(String string){
        int idx = string.indexOf(" "); //cari space, yg memisahkan antara key dan value

//        Log.i(TAG, "parse: " + string);

        HeaderItem header = new HeaderItem();

        //apabila tdk ada space, maka string menjadi key nya, dan value = ""
        if (idx<0){
            header.key = string;
            header.value = "";
        } else {
            header.key = string.substring(0, idx);
            header.value = string.substring(idx+1);
        }

        header.key = header.key.toUpperCase();

//        Log.i(TAG, "parse <" + header.key + "=" + header.value + ">");

        return header;
    }
}

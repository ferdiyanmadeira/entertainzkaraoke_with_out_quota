package com.madeira.eznet;

import java.nio.channels.SocketChannel;

public class ProtocolBase {

    protected Header header;
    protected boolean isLoop;

    public ProtocolBase(Header header) {
        this.header = header;
    }

    /**
     * baca data dari socket
     *
     * @param DataBuffer
     * @return true apabila sdh lengkap
     */
    public boolean read(DataBuffer buffer){
        return true;
    }

    public byte[] process(){
        return null;
    }

    public boolean isLoop() {
        return isLoop;
    }
}

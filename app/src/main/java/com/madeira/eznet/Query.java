package com.madeira.eznet;

import android.util.Log;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.HashMap;

public class Query {
    private static final String TAG = "Query";

    private String name; //name of the query
    private HashMap<String, String> hashMap; //parameter

    public Query() {
        hashMap = new HashMap<>();
    }

    public String getName(){
        return name;
    }
    public String get(String key){
        return hashMap.get(key);
    }

    /**
     * Format
     *
     * queryname\n
     * fieldName fieldValue\n
     * \n
     *
     * @param data
     */
    public boolean parse(String data){

        boolean r = false;

        try {
            int idx = 0;
            int idxFound;
            DataBuffer dataBuffer;

            //exit apabila data empty
            if (data==null || data.length()==0) return r;

            while (true){
                idxFound = data.indexOf((byte)'\n', idx);
                if (idxFound<0){
                    // \n tdk di temukan
                    break;
                }
                if (idxFound==idx){
                    r = true;
                    //end of line reached
//                    Log.i(TAG, "parse: EOL");
                    break;
                }

    //            int lineSize = idxFound - idx;
    //            byte []arLine = new byte[lineSize];
    //            System.arraycopy(dataBuffer.getBytes(), idx, arLine,0,lineSize);
                parseLine(data.substring(idx, idxFound));

                idx = idxFound +1;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return r;
    }

    /**
     * parse line by line
     * @param data
     */
    private void parseLine(String data){

        try {
            int idxFound = 0;
//        DataBuffer dataBuffer = new DataBuffer(ar);

            idxFound = data.indexOf((byte) ' ', 0); //cari space

            //space tdk ada, line ini adalah request name
            if (idxFound<0){
                name = data;//UtilByteArray.getString(ar,0,ar.length);
//                Log.i(TAG, "requestName=" + name);
                return;
            }

            String []ar = data.split(" "); //split dgn space

            String fieldName = URLDecoder.decode(ar[0], "UTF-8");
            String fieldValue;

            //kadang ada value empty
            if (ar.length>1){
                fieldValue  = URLDecoder.decode(ar[1], "UTF-8");
            } else {
                fieldValue = "";
            }

            hashMap.put(fieldName, fieldValue);

        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }
}

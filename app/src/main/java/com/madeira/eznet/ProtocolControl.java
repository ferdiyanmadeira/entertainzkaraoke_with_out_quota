package com.madeira.eznet;

import com.madeira.eznet.lib.UtilByteArray;

public class ProtocolControl extends ProtocolBase {
    private static final String TAG = "ProtocolControl";

    private String pingData; //data dari hasil ping
    private int pingCount=0;

    public ProtocolControl(Header header) {
        super(header);

        isLoop = true; //set loop mode
    }

    public String getPingData() {
        return pingData;
    }

    public int getPingCount() {
        return pingCount;
    }

    /**
     * terima ping
     * @param buffer
     * @return
     */
    public boolean read(DataBuffer buffer) {

        byte[]payload = null;
        int idx = 0;
        int idxFound;
        int payloadsize;

        pingCount = 0;
        pingData = null;

        //read semua ping sampai habis
        while (true){
            idxFound = buffer.indexOf((byte)0x0A, idx);
            if (idxFound<0) break; //belum ketemu paket yg lengkap

            payloadsize = idxFound - idx;
            payload = new byte[payloadsize];
            System.arraycopy(buffer.getBytes(), idx, payload, 0 , payloadsize);

            pingCount++;
            idx = idxFound + 1; //next payload
        }

        if (payload!=null){
            String data = UtilByteArray.getString(payload,0, payload.length);

            //cari space
            idxFound = data.indexOf(" ");
            data = data.substring(idxFound+1);
            this.pingData = data;
        }

        if (idx>0){
            buffer.trimLeft(idx); //buang data yg sdh di proses
        }

//        Log.i(TAG, "read: sisadata=" + buffer.length() + "\npingCount=" + pingCount);

        if (pingCount>0) return true; else return false;
    }

//    @Override
    public boolean read_old(DataBuffer buffer) {

        try {
            int idx;
            idx = buffer.indexOf((byte)0x0A, 0);
            if (idx<0) return false; //belum ketemu paket yg lengkap

            byte []ar = new byte[idx];
            System.arraycopy(buffer.getBytes(), 0, ar, 0 , idx);
            buffer.trimLeft(idx+1); //hapus data yg sdh di baca

            String data = UtilByteArray.getString(ar,0, ar.length);

            //cari space
            idx = data.indexOf(" ");
            data = data.substring(idx+1);
            this.pingData = data;

            return true;

        } catch (Exception e) {
            e.printStackTrace();
        }

        return false;
    }

    @Override
    public byte[] process() {
        byte [] payload = composePong(pingData);
        return payload;
    }

    private byte[] composePong(String data){
        if (data==null) return new byte[0]; //return empty array apabila tdk ada data
        String payload = "PONG " + data + "\n";
        return payload.getBytes();
    }
}

package com.madeira.eznet;

import java.nio.channels.SocketChannel;

public abstract class ProtocolEvent {

    public void onBgConnect(String remoteIp) {
    }
    public void onBgDisconnect(String remoteIp) {
    }
    public Response onBgRecv(String remoteIp, String message, String filename){
        return null;
    }
    public void onBgPing(String remoteIp, int count, String pingData){
    }
}

package com.madeira.eznet;

import com.madeira.eznet.lib.UtilByteArray;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;

/**
 * Transport layer utk reply
 *
 * BISA DI HAPUS
 */
public class NetTransport {
    private static final String TAG = "NetTransport";
    private static final int SEND_BUFFER = 8192; //8k

    static public boolean sendStatusError(SocketChannel socketChannel, int errCode){
        Header replyHeader = new Header();

        replyHeader.add(Header.HEADER_STATUS, String.valueOf(errCode));

        try {

            //write header
            ByteBuffer byteBuffer = ByteBuffer.wrap(replyHeader.getBytes());
            socketChannel.write(byteBuffer);

        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }

        return true;
    }

        /**
         * @param message pesan = null apabila tdk ada pesan
         * @param filename filename + fullpath, null apabila tdk ada file
         * @return
         */
    static public boolean send(SocketChannel socketChannel, String message, String filename){

        Header replyHeader;
        File file;
        long filesize = 0;
        byte []msgBytes = new byte[0];
        int msglen = 0;
        String filenameOnly = null;

        //reply dgn file
        if (filename!=null){
            file = new File(filename);
            if (file.exists()==false) return false; //file tdk exist
            filesize = file.length();
            filenameOnly = file.getName(); //set jadi nama file saja
        }

        if (message!=null){
            msgBytes = UtilByteArray.getBytesOfUtf8(message);
            msglen = msgBytes.length;
        }

        replyHeader = new Header();

        try {
            replyHeader.add(Header.HEADER_STATUS, String.valueOf(Error.STATUS_OK));

            //buat header utk reply
            if (msglen>0){
                replyHeader.add(Header.HEADER_TEXT_LEN, String.valueOf(msglen));
            }
            if (filesize>0){
                replyHeader.add(Header.HEADER_FILE_LEN, String.valueOf(filesize));
                replyHeader.add(Header.HEADER_FILENAME, filenameOnly);
            }

            ByteArrayOutputStream os = new ByteArrayOutputStream();

            //write header
            os.write(replyHeader.getBytes());

            //write message apabila ada
            if (msglen>0) os.write(msgBytes); //write msg apabila ada

            //write semua ke socket
            ByteBuffer byteBuffer = ByteBuffer.wrap(os.toByteArray());
            socketChannel.write(byteBuffer);

            //kirim file apabila ada
            if (filesize>0){
                InputStream inputStream = new FileInputStream(filename);
                byte[] buffer = new byte[SEND_BUFFER];
                int readLen;

                byteBuffer = ByteBuffer.wrap(buffer);

                while ((readLen=inputStream.read(buffer))>0){
                    byteBuffer.limit(readLen); //set readLen sebagai limit
                    socketChannel.write(byteBuffer);
                    byteBuffer.rewind();
                }
            }

            return true;

        } catch (IOException e) {
            e.printStackTrace();
        }

        return false;
    }

}

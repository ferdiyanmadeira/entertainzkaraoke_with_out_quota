package com.madeira.eznet;

import com.madeira.eznet.lib.UtilByteArray;
import com.madeira.eznet.lib.UtilRandom;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

public class ProtocolMessage extends ProtocolBase {
    private static final String TAG = "ProtocolText";

    String appPath;

    boolean isDataTextComplete;
    boolean isDataFileComplete;
    private String dataTextResult;

    private OutputStream fileos;
    private String newFilename;
    private int totalWritten = 0;

    public ProtocolMessage(Header header) {
        super(header);

        if (header.getTextLength()==0) isDataTextComplete = true; //set complete bila tdk di pakai
        if (header.getFileSize()==0) isDataFileComplete = true; //set true bila tdk d ipakai
    }

    public String getDataTextResult() {
        return dataTextResult;
    }

    public String getNewFilename() {
        return newFilename;
    }

    public void setAppPath(String appPath) {
        this.appPath = appPath;
    }

    @Override
    public boolean read(DataBuffer buffer) {

        //read data text apabila ada
        if (header.getTextLength()>0 && isDataTextComplete==false){
            isDataTextComplete = readDataText(buffer);
        }

        //exit apabila data text blm di selesaikan
        //shg read file tdk di lakukan dulu
        if (isDataTextComplete==false) return false;

        //read data file apabila ada
        if (header.getFileSize()>0 && isDataFileComplete==false){
            isDataFileComplete = readDataFile(buffer);
        }

        return isDataTextComplete && isDataFileComplete;
    }

    private boolean readDataText(DataBuffer buffer) {

        int dataTextLength = header.getTextLength();

        //apabila data yg di terima blm sama dgn data text yg seharusnya maka exit
        if (buffer.length()<dataTextLength) return false;

        //copy data ke array sesuai dgn length nya
        byte []ar = new byte[dataTextLength];
        System.arraycopy(buffer.getBytes(), 0, ar, 0 , dataTextLength);

        //create string dari ar
        dataTextResult = UtilByteArray.getUtf8(ar);

        //copy sisa data masuk ke buffer
        buffer.trimLeft(ar.length);

//        Log.i(TAG, "readDataText: " + dataTextResult);

        return true;
    }


    private boolean readDataFile(DataBuffer buffer) {

        long dataFileLength = header.getFileSize();

        //tdk perlu proses data file
        if (dataFileLength==0) return true; //exit apabila data file sdh di proses

        if (fileos==null){

            try {
                //buat filename baru
                newFilename = createNewFileFrom(header.getFilename());
                String fullpath = appPath + "/" + newFilename;
//                Log.i(TAG, "processDataFile: fullpath = " + fullpath);

                //buat file utk di write
                File f = new File(fullpath);
                fileos = new FileOutputStream(f);
//                Log.i(TAG, "processDataFile: newFilename=" + newFilename);

            } catch (FileNotFoundException e) {
                e.printStackTrace();
                return false;
            }
        }

        try {

            //hitung jumlah data yg belum di tulis ke file
            long remainingData = dataFileLength - totalWritten;

            //data yg tersedia lbh banyak dari data yg seharusnya di tulis ke file
            //potong data ke berlebih
            if(remainingData<buffer.length()){
                buffer.trimRight((int) (buffer.length()-remainingData));

//                byte []ar = new byte[(int)remainingData];
//                System.arraycopy(buffer.getBytes(), 0, ar, 0 , (int)remainingData);
//                data = ar; //abaikan data sisa
            }

            fileos.write(buffer.getBytes());
            fileos.flush();
            totalWritten += buffer.length();
            buffer.clear();
//            buffer = null;

            if (totalWritten==dataFileLength){
//                isDataFileReceived = true;
                fileos.close();
                fileos = null;

//                Log.i(TAG, "processDataFile: FINISH");

                return true;
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

        return false;
    }

    private String createNewFileFrom(String filename){
        String ext = "";

        //cari ext dari file
        int index = filename.lastIndexOf('.');
        if(index > 0) {
            ext = filename.substring(index + 1);
        }

        String newFilename = UtilRandom.randomString(24);
        if (ext.length()>0){
            newFilename = newFilename + "." + ext;
        }

        return newFilename;
    }
}

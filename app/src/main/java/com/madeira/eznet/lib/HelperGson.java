package com.madeira.eznet.lib;

import com.google.gson.Gson;

import java.io.StringWriter;

public class HelperGson {
    private static final String TAG = "HelperGson";


    public static String toString(Object o){

        StringWriter sw = new StringWriter();
        try {

            Gson gson = new Gson();
            gson.toJson(o, sw);

            return sw.toString();

        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }
}

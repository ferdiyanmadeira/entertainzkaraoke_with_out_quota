package com.madeira.eznet.lib;

import java.nio.charset.StandardCharsets;
import java.util.Arrays;

public class UtilByteArray {
    private static final String TAG = "UtilByteArray";

    public static int getIntBig(byte[] b)
    {
        return getIntBig(b, 0);
    }

    /**
     * convert array to little endian
     *
     * @param b
     * @param index
     * @return
     */
    public static int getIntLittle(byte[] b, int index)
    {
        return   b[0+index] & 0xFF |
                (b[1+index] & 0xFF) << 8 |
                (b[2+index] & 0xFF) << 16 |
                (b[3+index] & 0xFF) << 24;
    }

    /**
     * for android
     * @param b
     * @param index
     * @return
     */
    public static int getIntBig(byte[] b, int index)
    {
        return   b[3+index] & 0xFF |
                (b[2+index] & 0xFF) << 8 |
                (b[1+index] & 0xFF) << 16 |
                (b[0+index] & 0xFF) << 24;
    }

    public static byte[] from(int a)
    {
        return new byte[] {
                (byte) ((a >> 24) & 0xFF),
                (byte) ((a >> 16) & 0xFF),
                (byte) ((a >> 8) & 0xFF),
                (byte) (a & 0xFF)
        };
    }

    public static byte []join(byte[] all, byte[] buffer, int bufferLength){
        int idx = all.length;
        byte []newAll = Arrays.copyOf(all, all.length + bufferLength); //copy and make array bigger
        System.arraycopy(buffer, 0, newAll, idx , bufferLength);
        return newAll;
    }

    /**
     * gabung buffer1 + buffer2
     * @param buffer1
     * @param buffer2
     * @return
     */
    public static byte []join(byte[] buffer1, byte[] buffer2){
        return join(buffer1, buffer2, buffer2.length);
    }

    /**
     * Cari needle di haystack
     *
     * @param needle = pattern yg akan di cari
     * @param haystack = data
     * @param idx = awal posisi pencarian
     * @return -1 = not found
     */
    public static int indexOf(byte needle, byte[] haystack, int idx){
        if (idx<0) return -1;
        if (haystack==null) return -1;
        if ((haystack.length-1) < idx) return -1; //posisi index melampaui haystack

        //cari byte satu persatu
        while ((haystack.length-1)>=idx){
            byte onebyte = haystack[idx];
            if (onebyte==needle) return idx;
            idx++;
        }

        return -1;
    }

    /**
     * Ambil string dari array byte
     *
     * @param data
     * @param start
     * @param length
     * @return
     */
    public static String getString(byte[]data, int start, int length){
        byte []newData = new byte[length];
        System.arraycopy(data, start, newData, 0, length);
        return new String(newData, StandardCharsets.US_ASCII);
    }

    public static String getUtf8(byte []data){
        return new String(data, StandardCharsets.UTF_8);
    }

    public static String getUtf8(byte []data, int start, int length){
        byte []newData = new byte[length];
        System.arraycopy(data, start, newData, 0, length);
        return new String(newData, StandardCharsets.UTF_8);
    }

    public static byte[] getBytesOfUtf8(String utf8String){
        return utf8String.getBytes(StandardCharsets.UTF_8);
    }

}

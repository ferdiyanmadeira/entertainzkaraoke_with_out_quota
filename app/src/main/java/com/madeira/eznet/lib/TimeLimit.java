package com.madeira.eznet.lib;

import java.util.Date;

public class TimeLimit {
    private static final String TAG = "TimeLimit";

    private long t = 0;

    public TimeLimit() {
        reset();
    }

    public void reset(){
        t = new Date().getTime();
    }

    public long check(){
        long t2 = new Date().getTime();
        return t2 - t;
    }
}

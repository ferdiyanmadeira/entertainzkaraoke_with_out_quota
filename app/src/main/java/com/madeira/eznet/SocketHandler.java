package com.madeira.eznet;

import android.util.Log;

import com.madeira.eznet.lib.TimeLimit;
import com.madeira.eznet.lib.UtilByteArray;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.channels.SocketChannel;
import java.nio.file.OpenOption;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;

public class SocketHandler extends Thread{
    private static final String TAG = "SocketHandler";
    private static final boolean NEED_CONTROL = true; //TODO: set to true

    private static final int BUFFER_LENGTH = 102400; //100k
    private static final int READ_TIMEOUT = 120 * 1000; //TODO: set to 120 second

    private static volatile int countProtocolControl = 0; //jumlah protocol control

    ProtocolBase protocol;
    SocketChannel socketChannel;
    ProtocolEvent callback;
    boolean isConnected;
    String appPath;

    String remoteIp;

    DataBuffer dataBuffer;
    Header header;

    boolean isNeedControl = NEED_CONTROL; //flag ini utk mengendalikan apakah control di butuhkan atau tdk

    private int readTimeout = READ_TIMEOUT;

//    InputStream is;

    public SocketHandler(String appPath, SocketChannel socketChannel, ProtocolEvent callback) {
        this.appPath = appPath;
        this.socketChannel = socketChannel;
        this.callback = callback;

        header = new Header();
        dataBuffer = new DataBuffer();

        isConnected = true;

        remoteIp = socketChannel.socket().getInetAddress().getHostAddress();
    }

    public int getReadTimeout() {
        return readTimeout;
    }

    public void setReadTimeout(int readTimeout) {
        this.readTimeout = readTimeout;
    }

    /**
     * buat protocol berdasakarn type
     *
     * @param header
     * @return
     */
    private ProtocolBase buildProtocol(Header header){

        //create protocol control
        if (header.isTypeControl()){
            ProtocolControl protocolControl;
            protocolControl = new ProtocolControl(header);
            return protocolControl;
        }


        //create protocol text
        ProtocolMessage protocolText;
        protocolText = new ProtocolMessage(header);
        protocolText.setAppPath(appPath);
        return protocolText;
    }

    /**
     * hanya read header saja sampai lengkap
     */
    private Header readHeader(){
        Header header = new Header();

//        byte []buffer = new byte[BUFFER_LENGTH]; //test read pakai 2 byte saja
        int len;

        ByteBuffer byteBuffer = ByteBuffer.allocate(BUFFER_LENGTH);

        TimeLimit timeLimit = new TimeLimit();

        try {

            while (true){
                len = socketChannel.read(byteBuffer);

                //apabila sdh kosong maka exit
                if (len<0) break;
                if (len>0) timeLimit.reset(); //reset limit apabila ada data masuk

                dataBuffer.append(byteBuffer.array(), len); //append buffer --> dataBuffer
                byteBuffer.rewind();

                if (header.parse(dataBuffer)) break; //parse data yg sdh masuk, apabila header lengkap, exit

                //exit apabila operasi read ini melebihi time limit
                if (timeLimit.check()>readTimeout) {
                    Log.i(TAG, "readHeader: READ TIMEOUT");
                    isConnected=false;
                    break;
                }
            }

            if (len<0) isConnected = false;

        } catch (Exception e) {
            e.printStackTrace();
        }

        return header;
    }

    private void readContent(){

//        byte []buffer = new byte[BUFFER_LENGTH]; //test read pakai 2 byte saja
        int len;
        ByteBuffer byteBuffer = ByteBuffer.allocate(BUFFER_LENGTH);

        TimeLimit timeLimit = new TimeLimit();

        try {

            //apakah data yg ada pada buffer sdh bisa seluruhnya di proses?
            if (protocol.read(dataBuffer)) return;

            while (true){

                len = socketChannel.read(byteBuffer);
//                Log.i(TAG, "readContent: len=" + len);

                //apabila sdh stop maka exit, len=0 tetap loop
                if (len<0) break;
                if (len>0) timeLimit.reset();;

                dataBuffer.append(byteBuffer.array(), len); //append buffer --> dataBuffer
                byteBuffer.rewind();

                //exit apabila sdh selesai
                if (protocol.read(dataBuffer)) break;

                //exit apabila sdh melampai waktu
                if (timeLimit.check()>readTimeout) {
                    Log.i(TAG, "readContent: READ TIMEOUT");
                    isConnected = false; //set sbg disconnected
                    break;
                }
            }

            if (len<0) isConnected = false;

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void run() {

        //bila init gagal, exit saja
        if (initSocket()==false) {
            return;
        }

        protocol = initProtocol();

        if (protocol instanceof ProtocolControl){
//            Log.i(TAG, "run: ProtocolControl");
            processControl((ProtocolControl) protocol);
        }
        if (protocol instanceof ProtocolMessage){
//            Log.i(TAG, "run: ProtocolText");
            processMessage((ProtocolMessage) protocol);
        }

        closeSocket();
    }

    private boolean initSocket(){

        try {

            socketChannel.configureBlocking(false);
//            is = socketChannel.socket().getInputStream();
            return true;

        } catch (IOException e) {

            e.printStackTrace();
        }

        return false;
    }

    private ProtocolBase initProtocol(){

        try {
            //1. baca header di awal
            header = readHeader();
            if (isConnected==false) return null; //exit krn disconnect

            //2. type message hanya bisa ada apabila control sdh ada
            if (isNeedControl==true && header.isTypeMessage()){
                //exit apabila control blm ada
                if (SocketHandler.countProtocolControl<=0) {
                    sendStatusError(Error.STATUS_ERR_NO_CONTROL);
                    return null;
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

        //3. buat protocol berdasarkan type
        return buildProtocol(header);
    }

    private void processControl(ProtocolControl protocolControl){

        try {

            SocketHandler.countProtocolControl++;
            callback.onBgConnect(remoteIp);

            do {

                //3. baca content dan di proeses di protocol
                readContent();

                if (isConnected==false) break;;

                byte []buffer = protocolControl.process();

                if (buffer.length==0) continue;

                ByteBuffer byteBuffer = ByteBuffer.wrap(buffer);
                socketChannel.write(byteBuffer);
                callback.onBgPing(remoteIp, protocolControl.getPingCount(), protocolControl.getPingData());
            }
            while (isConnected);

        } catch (Exception e) {
            e.printStackTrace();
        }

        SocketHandler.countProtocolControl--;
        callback.onBgDisconnect(remoteIp);
    }

    private void processMessage(ProtocolMessage protocol) {

        try {

            //3. baca content dan di proeses di protocol
            readContent();

            //proses message apabila masih connect
            if (isConnected){
                //proses message
                ProtocolMessage protocolText = (ProtocolMessage) protocol;
                Response response = callback.onBgRecv(remoteIp, protocolText.getDataTextResult(),protocolText.getNewFilename());

                sendResponse(response);

            } else {
//                Log.i(TAG, "processMessage: DISCONNECTED");
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void sendResponse(Response response){

        String filename = null;
        String message = null;

        Header replyHeader;
        File file;
        long filesize = 0;
        byte []msgBytes = new byte[0];
        int msglen = 0;
        String filenameOnly = null;

        if(response!=null){
            filename = response.filename;
            message = response.message;
        }

        //reply dgn file
        if (filename!=null){

            //apakah filename ada ??
            file = new File(filename);
            //kalo tdk ada process filesize
            if (file.exists()) {
                filesize = file.length();
                filenameOnly = file.getName(); //set jadi nama file saja
            }
        }

        if (message!=null){
            msgBytes = UtilByteArray.getBytesOfUtf8(message);
            msglen = msgBytes.length;
        }

        replyHeader = new Header();

        try {
            replyHeader.add(Header.HEADER_STATUS, String.valueOf(Error.STATUS_OK));

            //buat header utk reply
            if (msglen>0){
                replyHeader.add(Header.HEADER_TEXT_LEN, String.valueOf(msglen));
            }
            if (filesize>0){
                replyHeader.add(Header.HEADER_FILE_LEN, String.valueOf(filesize));
                replyHeader.add(Header.HEADER_FILENAME, filenameOnly);
            }

            ByteArrayOutputStream os = new ByteArrayOutputStream();

            //write header
            os.write(replyHeader.getBytes());

            //write message apabila ada
            if (msglen>0) os.write(msgBytes); //write msg apabila ada

            //set blocking=true, sehingga write operation jadi benar saat kirim message & file
            socketChannel.configureBlocking(true);

            //write semua ke socket
            ByteBuffer byteBuffer = ByteBuffer.wrap(os.toByteArray());
            socketChannel.write(byteBuffer);

            //kirim file apabila ada
            if (filesize>0){
                InputStream inputStream = new FileInputStream(filename);
                byte[] buffer = new byte[BUFFER_LENGTH];
                int readLen;

                byteBuffer = ByteBuffer.wrap(buffer);

                while ((readLen=inputStream.read(buffer))>0){
                    byteBuffer.limit(readLen); //set readLen sebagai limit
                    socketChannel.write(byteBuffer);
                    byteBuffer.rewind();
                }
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void sendStatusError(int errCode){
        Header replyHeader = new Header();

        replyHeader.add(Header.HEADER_STATUS, String.valueOf(errCode));

        try {

            //write header
            ByteBuffer byteBuffer = ByteBuffer.wrap(replyHeader.getBytes());
            socketChannel.write(byteBuffer);

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void closeSocket(){
        try {
            socketChannel.close();
            socketChannel = null;

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

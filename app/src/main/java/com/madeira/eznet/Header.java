package com.madeira.eznet;

import com.madeira.eznet.lib.UtilByteArray;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Set;

public class Header {
    private static final String TAG = "Header";

    //header item
    public static final String HEADER_STATUS = "STATUS"; //di pakai saat reply

    private static final String HEADER_TYPE = "TYPE"; //TEXT, FILE
    public static final String HEADER_TEXT_LEN = "TEXT-LEN";
    public static final String HEADER_FILE_LEN = "FILE-LEN";
    public static final String HEADER_FILENAME = "FILENAME";

    private static final String HEADER_TYPE_CONTROL = "CONTROL";
    private static final String HEADER_TYPE_MESSAGE = "MESSAGE";

    private boolean isComplete;
    private HashMap<String, String> hashMap;

    private String dataType;
    private int dataMessageLength; //panjang data text
    private long dataFileLength; //panjang file
    private String filename; //utk type file

    public Header() {
        hashMap = new HashMap<>();
        isComplete = false;
    }

    public boolean isComplete() {
        return isComplete;
    }

    public HashMap<String, String> getHashMap() {
        return hashMap;
    }

    public boolean parse(DataBuffer buffer){
//        Log.i(TAG, "parse: " + buffer.length());

//        byte []buffer = data;
        int idx = 0;
        int idxFound;

        if (buffer.length()==0) return false;

        while (true){

//            Log.i(TAG, "parseHeader: idx=" + idx);
            idxFound = buffer.indexOf((byte)0x0a, idx);
//            idxFound = UtilByteArray.indexOf((byte) 0x0a, data, idx); //cari \n
            if (idxFound<0){
//                Log.i(TAG, "parseHeader: idxFound=-1");
                break;
            }
            if (idx==idxFound) {
                //posisi \n terakhir di temukan, header complete
//                Log.i(TAG, "parseHeader: found end of header");
                idx++; //adjust 1 byte

                //langsung process header apabila sdh lengkap
                isComplete = true;
                processHeader();

                break;
            }

            String line = UtilByteArray.getString(buffer.getBytes(), idx, idxFound-idx);

            HeaderItem header = HeaderItem.parse(line);
            hashMap.put(header.key, header.value);

            idx = idxFound + 1; //idx set to next line
        }

        //copy remaining apabila idx bukan 0
        if (idx>0){
            buffer.trimLeft(idx);
        }

        return isComplete;
    }

    public void add(String key, String value){
        hashMap.put(key, value);
    }

    public byte []getBytes(){
        ByteArrayOutputStream os = new ByteArrayOutputStream();

        //get all key
        Set<String> keys = hashMap.keySet();

        try {
            for (String item: keys) {
                String value = hashMap.get(item);
                os.write(item.getBytes());
                os.write(32); //space
                os.write(value.getBytes());
                os.write(10); // \n
            }
            os.write(10); // \n <--- end of header
            return os.toByteArray();

        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    /**
     * process header apabila sdh lengkap
     */
    private void processHeader(){

        String value;

        //ambil type
        dataType = hashMap.get(HEADER_TYPE);

        //ambil panjang text
        value = hashMap.get(HEADER_TEXT_LEN);
        if (value!=null){
            dataMessageLength = Integer.parseInt(value);
        }

        //ambil filename
        filename = hashMap.get(HEADER_FILENAME);
        value = hashMap.get(HEADER_FILE_LEN);
        if (value!=null){
            dataFileLength = Long.parseLong(value);
        }
    }

//    public boolean isTypeText() {
//        if (dataType==null) return false;
//        return dataType.equals(HEADER_TYPE_TEXT);
//    }

    public boolean isTypeControl() {
        if (dataType==null) return false;
        return dataType.equals(HEADER_TYPE_CONTROL);
    }

    public boolean isTypeMessage() {
        if (dataType==null) return false;
        return dataType.equals(HEADER_TYPE_MESSAGE);
    }

//    public boolean isTypeFile() {
//        if (dataType==null) return false;
//        return dataType.equals(HEADER_TYPE_FILE);
//    }

    public int getTextLength() {
        return dataMessageLength;
    }

    public long getFileSize() {
        return dataFileLength;
    }

    public String getFilename() {
        return filename;
    }
}

package com.madeira.eznet;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

/**
 * This will send sonar packet
 */
public class BroadcastSender {
    private static final String TAG = "BroadcastSender";

    /**
     * convert ip address a.b.c.d --> a.b.c.255
     * @param ipAddress
     * @return
     */
    public static String convertToBroadcastAddress(String ipAddress){
        String[]digit = ipAddress.split("\\.");
        return digit[0] + "." + digit[1] + "." + digit[2] + ".255";
    }

    /**
     * send broadcast
     * @param broadcastAddress
     * @param msg
     * @param port
     */
    public static void send(String broadcastAddress, String msg, int port) {

//        Log.i(TAG, "sendBroadcast: " + msg);

        try {
            DatagramSocket clientSocket = new DatagramSocket();

            clientSocket.setBroadcast(true);
            InetAddress address = InetAddress.getByName(broadcastAddress);

            byte[] sendData;

            sendData = msg.getBytes();
            DatagramPacket sendPacket = new DatagramPacket(sendData, sendData.length, address, port);
            clientSocket.send(sendPacket);

            clientSocket.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

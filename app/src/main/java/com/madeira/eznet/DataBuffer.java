package com.madeira.eznet;

import com.madeira.eznet.lib.UtilByteArray;

public class DataBuffer {
    private static final String TAG = "DataBuffer";

    private byte []buffer;

    public DataBuffer() {
        buffer = new byte[0];
    }

    public DataBuffer(byte [] data) {
        buffer = data;
    }

    public byte[] getBytes() {
        return buffer;
    }

    public int length(){
        if (buffer==null) return 0;
        return buffer.length;
    }

    /**
     * Gabung data lama dan data baru
     *
     * @param data
     * @param len
     */
    public void append(byte []data, int len){
        if (data==null) return;
        buffer = UtilByteArray.join(buffer, data, len);
    }

    /**
     * potong data sebelah kiri
     *
     * @param len
     */
    public void trimLeft(int len){
        if (buffer==null) return;

        int remainingLength = buffer.length - len;
        byte []newBuffer = new byte[remainingLength];

        System.arraycopy(buffer, len, newBuffer, 0 , remainingLength);
        buffer = newBuffer;
    }

    /**
     * potong data sebelah kanan
     * @param len
     */
    public void trimRight(int len){
        if (buffer==null) return;
        if (buffer.length==0) return;
        if (len>=buffer.length) {
            clear();
            return;
        }

        int newSize = buffer.length - len;
        byte []ar = new byte[newSize];
        System.arraycopy(buffer, 0, ar, 0 , newSize);
        buffer = ar;
    }

    public void clear(){
        buffer = new byte[0];
    }

    /**
     * cari data start dari pos
     *
     * @param needle
     * @return -1 = not found
     */
    public int indexOf(byte needle, int searchPos){
        if (buffer==null) return  -1;

        //buffer lebh pendek dari posisi search
        if (buffer.length < searchPos) return -1;

        return UtilByteArray.indexOf(needle, buffer, searchPos); //cari \n
    }

}

package com.madeira.eznet;

import android.util.Log;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.List;

/**
 * Remote procedural call
 */
public class Rpc {
    private static final String TAG = "Rpc";

    private static final String TYPE_INTEGER = "INTEGER";
    private static final String TYPE_LONG = "LONG";
    private static final String TYPE_STRING = "STRING";

    private String className;
    private String methodName;
    private List<RpcItem> listArg;

    private Exception e;

    public Exception getException() {
        return e;
    }

    public Rpc(){
        listArg = new ArrayList<>();
    }

    /**
     * get value argument berdasarkan type data (INTEGER / LONG / STRING)
     * @param idx
     * @return
     */
    public Object getValue(int idx){
        RpcItem item = listArg.get(idx);
        switch (item.type){
            case TYPE_INTEGER:
                return Integer.parseInt(item.value);
            case TYPE_LONG:
                return Long.parseLong(item.value);
            default:
                return item.value;
        }
    }

    /**
     * Format
     *
     * queryname\n
     * fieldName fieldValue\n
     * \n
     *
     * @param data
     */
    public void parse(String data){

        try {
            int idx = 0;
            int idxFound;
            DataBuffer dataBuffer;

            //exit apabila data empty
            if (data==null || data.length()==0) return;

            while (true){
                idxFound = data.indexOf((byte)'\n', idx);
                if (idxFound<0){
                    // \n tdk di temukan
                    break;
                }
                if (idxFound==idx){
                    //end of line reached
//                    Log.i(TAG, "parse: EOL");
                    break;
                }

                //            int lineSize = idxFound - idx;
                //            byte []arLine = new byte[lineSize];
                //            System.arraycopy(dataBuffer.getBytes(), idx, arLine,0,lineSize);
                parseLine(data.substring(idx, idxFound));

                idx = idxFound +1;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * parse line by line
     * @param data
     */
    private void parseLine(String data){

        try {
            int idxFound = 0;

            idxFound = data.indexOf((byte) ' ', 0); //cari space

            //space tdk ada, line ini adalah classname + method name
            if (idxFound<0){
                idxFound = data.lastIndexOf('.'); //cari titik dari belakang

                className = data.substring(0, idxFound);
                methodName = data.substring(idxFound+1, data.length());

//                Log.i(TAG, "classname " + className + " methodName=" + methodName);
                return;
            }

            String []ar = data.split(" "); //split dgn space

            String fieldType  = ar[0];
            String fieldName = ar[1];
            String fieldValue;

            //kadang ada value empty
            if (ar.length>2){
                fieldValue  = URLDecoder.decode(ar[2], "UTF-8");
            } else {
                fieldValue = "";
            }

            RpcItem item = new RpcItem(fieldType, fieldName, fieldValue);

            listArg.add(item);

        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }

    /**
     * call static method berdasarkan dari message
     *
     * hanya bisa call static method
     * @return
     */
    public Object invoke(){

        try {
            //build parameter
            Object []arArg = new Object[listArg.size()];
            for (int idx=0; idx<listArg.size(); idx++) {
                arArg[idx] = getValue(idx);
            }

//            Log.i(TAG, "invoke: \n" + className + " " + methodName);
            return invokeStaticMethod(className, methodName, arArg);

        } catch (Exception e) {
            this.e = e;
            e.printStackTrace();
        }

        return null;
    }

    private Object invokeStaticMethod(String className, String methodName, Object... args){
        try {
            //cari class sesuai nama
            Class<?> clazz = Class.forName(className);

            //handle args
            if (args==null) args = new Object[]{};
            Class[] argClasses = new Class[args.length];
            //fill class type utk setiap arg
            for(int idx=0; idx<args.length; idx++){
                argClasses[idx] = args[idx].getClass();
            }

            Method m = clazz.getDeclaredMethod(methodName, argClasses);

            return m.invoke(null, args);

        } catch (ClassNotFoundException e) {
            this.e = e;
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            this.e = e;
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            this.e = e;
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            this.e = e;
            e.printStackTrace();
        }

        return null;
    }
}

package com.madeira.eznet;

public class RpcItem {
    public String type; //STRING, INT
    public String key;
    public String value;

    public RpcItem(String type, String key, String value) {
        this.type = type;
        this.key = key;
        this.value = value;
    }
}

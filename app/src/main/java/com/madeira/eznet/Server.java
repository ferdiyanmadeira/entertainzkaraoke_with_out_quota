package com.madeira.eznet;

import com.madeira.eznet.BroadcastSender;
import com.madeira.eznet.lib.UtilNetworkDiscovery;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.channels.CancelledKeyException;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.nio.channels.spi.SelectorProvider;
import java.util.Iterator;

public class Server extends Thread {
    private static final String TAG = "Server";

    private static final int DEFAULT_PORT = 56789;

    //utk broadcast
    private String appId;
    private String ipAddress;
    private String broadcastAddress;
    private String payload;

    private int connectionCounter;
    private Selector selector;
    private int port = DEFAULT_PORT;
    private String appPath;

    public Server(String appPath, String appId) {
        this.appPath = appPath;
        this.appId = appId;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public void finish(){
        interrupt();
    }

    ProtocolEvent socketHandlerCallback;

    /**
     * event ini di pakai saat ada connection
     * @param callback
     */
    public void setSocketHandlerCallback(ProtocolEvent callback){
        this.socketHandlerCallback = callback;
    }

    @Override
    public void run() {

        init();

        mainLoop();

        release();
    }

    private void init(){
        ServerSocketChannel sChan;

        try {

            //utk broadcast
            ipAddress = UtilNetworkDiscovery.getIPAddress(true);
            broadcastAddress = BroadcastSender.convertToBroadcastAddress(ipAddress);
            //payload utk auto discovery
            payload = "{\n" +
                    "  \"application_id\": \"" + appId + "\",\n" +
                    "  \"ip_address\": \"" + ipAddress + "\",\n" +
                    "  \"port\": "+port+"\n" +
                    "}";

            //setup socket utk listening
            selector = SelectorProvider.provider().openSelector();
            sChan = ServerSocketChannel.open();
            InetSocketAddress iaddr = new InetSocketAddress("0.0.0.0", port);

            sChan.configureBlocking(false);
            sChan.socket().bind(iaddr);

            System.out.println("Running on port:" + sChan.socket().getLocalPort());

            //add socket to listener
            sChan.register(selector, SelectionKey.OP_ACCEPT);

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void mainLoop(){

        try {
            while (true){

                //kirim broadcast
                BroadcastSender.send(broadcastAddress, payload, port);

                processSelector();

                Thread.sleep(1000);
            }
        } catch (InterruptedException e) {
            return;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void release(){

        try {

            socketHandlerCallback = null;
            selector.close();
            selector = null;

        } catch (IOException e) {
//            e.printStackTrace();
        }

    }

    private void processSelector(){
        SelectionKey key = null;

        try {

            int channel = selector.selectNow();

            if (channel == 0) return;

            Iterator<SelectionKey> it = selector.selectedKeys().iterator();

            while (it.hasNext()) {

                key = it.next();
                it.remove();

                if (!key.isValid()) {
                    continue;
                }

                // Finish connection in case of an error
                if (key.isConnectable()) {
                    SocketChannel ssc = (SocketChannel) key.channel();
                    if (ssc.isConnectionPending()) {
                        ssc.finishConnect();
                    }
                }

                //accept incoming connection
                if (key.isAcceptable()) {
                    onAccept(key);
                }

            }
        } catch (CancelledKeyException e){
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void onAccept(SelectionKey key){

        try {
            ServerSocketChannel ssc = (ServerSocketChannel) key.channel();
            SocketChannel newClient = ssc.accept();

            SocketHandler socketHandler = new SocketHandler(appPath, newClient, socketHandlerCallback);
            socketHandler.start();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}

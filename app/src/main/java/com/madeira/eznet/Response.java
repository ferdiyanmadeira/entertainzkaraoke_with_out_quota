package com.madeira.eznet;

public class Response {
    private static final String TAG = "Response";

    public String message;
    public String filename;

    public Response(String message, String filename) {
        this.message = message;
        this.filename = filename;
    }

    public Response(String message) {
        this.message = message;
    }
}

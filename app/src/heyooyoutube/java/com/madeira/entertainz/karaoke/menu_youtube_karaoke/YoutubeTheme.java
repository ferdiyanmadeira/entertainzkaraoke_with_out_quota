package com.madeira.entertainz.karaoke.menu_youtube_karaoke;

import android.content.Context;
import android.content.res.ColorStateList;

import com.madeira.entertainz.karaoke.DBLocal.TElement;
import com.madeira.entertainz.karaoke.Debug;
import com.madeira.entertainz.karaoke.R;
import com.madeira.entertainz.karaoke.config.C;
import com.madeira.entertainz.karaoke.config.CacheData;

import java.util.Date;

public class YoutubeTheme {
    String TAG = "YoutubeTheme";
    public IYoutubeThemeListener callback;
    public Context context;
    public Date lastUpdateTheme;

    public interface IYoutubeThemeListener {
        void setThemeUpdateDate(Date lastUpdateTheme);

        void setListColorTheme(int color);

        void setTextAndSelectionColorTheme(ColorStateList colorSelection, ColorStateList colorText, int colorTextSelected, int colorTextFocus, int colorTextNormal, int colorSelectionSelected, int colorSelectionFocus);
    }

    public YoutubeTheme(Context context, IYoutubeThemeListener callback) {
        this.callback = callback;
        this.context = context;
    }

    public void setTheme() {
        try {
            lastUpdateTheme = new Date();

            //exit apabila theme kosong
            if (CacheData.hashElement == null || CacheData.hashElement.isEmpty()) return;

            setListColor();
            setTextAndSelectionColor();
            callback.setThemeUpdateDate(lastUpdateTheme);
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }


    public void setListColor() {
        try {
            TElement element;
            int color = context.getResources().getColor(R.color.theme_main_list_background_color);

            element = CacheData.hashElement.get(C.THEME_KARAOKE_LIST_BACKGROUND);
            if (element != null) {
                color = element.parseColor();
            }

            callback.setListColorTheme(color);
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }


    public void setTextAndSelectionColor() {
        try {
            ColorStateList colorSelection, colorText;

            TElement elementTextSelected, elementTextFocus, elementTextNormal, elementSelectionSelected, elementSelectionFocus;
            int colorTextSelected = context.getResources().getColor(R.color.default_text_color_selected);
            int colorTextFocus = context.getResources().getColor(R.color.default_text_color_focus);
            int colorTextNormal = context.getResources().getColor(R.color.default_text_color_normal);
            int colorSelectionSelected = context.getResources().getColor(R.color.default_list_selection_selected);
            int colorSelectionFocus = context.getResources().getColor(R.color.default_list_list_selection_focus);
           /* int colorTextSelected = context.getResources().getColor(R.color.theme_main_text_selected);
            int colorTextFocus = context.getResources().getColor(R.color.theme_main_text_focus);
            int colorTextNormal = context.getResources().getColor(R.color.theme_main_text_color);
            int colorSelectionSelected = context.getResources().getColor(R.color.theme_main_selection_selected);
            int colorSelectionFocus = context.getResources().getColor(R.color.theme_main_selection_focus);

            elementTextSelected = CacheData.hashElement.get(C.THEME_KARAOKE_TEXTCOLOR_SELECTED);
            elementTextFocus = CacheData.hashElement.get(C.THEME_KARAOKE_TEXTCOLOR_FOCUS);
            elementTextNormal = CacheData.hashElement.get(C.THEME_KARAOKE_TEXTCOLOR_NORMAL);

            elementSelectionSelected = CacheData.hashElement.get(C.THEME_KARAOKE_LIST_SELECTION_SELECTED);
            elementSelectionFocus = CacheData.hashElement.get(C.THEME_KARAOKE_LIST_SELECTION_FOCUS);

            if (elementTextSelected != null) colorTextSelected = elementTextSelected.parseColor();
            if (elementTextFocus != null) colorTextFocus = elementTextFocus.parseColor();
            if (elementTextNormal != null) colorTextNormal = elementTextNormal.parseColor();
            if (elementSelectionSelected != null)
                colorSelectionSelected = elementSelectionSelected.parseColor();
            if (elementSelectionFocus != null)
                colorSelectionFocus = elementSelectionFocus.parseColor();*/

            colorSelection = new ColorStateList(
                    new int[][]{
                            new int[]{android.R.attr.state_selected},
                            new int[]{android.R.attr.state_focused}
                    },
                    new int[]{
                            colorSelectionSelected,
                            colorSelectionFocus
                    }
            );

            colorText = new ColorStateList(
                    new int[][]{
                            new int[]{android.R.attr.state_selected},
                            new int[]{android.R.attr.state_focused},
                            new int[]{}
                    },
                    new int[]{
                            colorTextSelected,
                            colorTextFocus,
                            colorTextNormal
                    }
            );

            callback.setTextAndSelectionColorTheme(colorSelection, colorText, colorTextSelected, colorTextFocus, colorTextNormal, colorSelectionSelected, colorSelectionFocus);
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }


}

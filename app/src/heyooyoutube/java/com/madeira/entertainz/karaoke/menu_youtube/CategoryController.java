package com.madeira.entertainz.karaoke.menu_youtube;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.madeira.entertainz.karaoke.BuildConfig;
import com.madeira.entertainz.karaoke.Debug;
import com.madeira.entertainz.karaoke.R;
import com.madeira.entertainz.karaoke.model.ModelYoutube;
import com.madeira.entertainz.library.EzRecyclerViewAdapter;

import java.util.ArrayList;
import java.util.List;


/**
 * class ini berfungsi sbg controller dari youtube category
 */
public class CategoryController {
    private static final String TAG = "CategoryController";

    RecyclerView recyclerView;
    EzRecyclerViewAdapter adapter;

    String regionCode;

    List<ModelYoutube.ItemCategory> mList = new ArrayList<>();

    Context context;

    EventListener callback;

    public interface EventListener {
        void onClick(String categoryId);
    }

    public CategoryController(){

    }

    public void attach(Context context, String regionCode, RecyclerView recyclerView){

        this.context = context;
        this.regionCode = regionCode;
        this.recyclerView = recyclerView;

        setupRecyclerView();

        fetchCategory();
    }

    public void addEventListener(EventListener callback){
        this.callback = callback;
    }

    private void setupRecyclerView(){

        adapter = new EzRecyclerViewAdapter(new EzRecyclerViewAdapter.IEzRecyclerViewAdapter() {
            @Override
            public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
                ItemViewHolder item = null;
                try {
                    View view = LayoutInflater.from(context).inflate(R.layout.cell_youtube_category, parent, false);
                    item = new ItemViewHolder(view);

                } catch (Exception e) {
                    Debug.e(TAG, e);
                }
                return item;
            }

            @Override
            public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
                ItemViewHolder itemViewHolder = (ItemViewHolder) holder;
                itemViewHolder.bind(mList.get(position), position);
            }

            @Override
            public int getItemCount() {
                return mList.size();
            }

            @Override
            public long getItemId(int pos) {
                return Long.parseLong(mList.get(pos).id);
            }
        });

        recyclerView.setAdapter(adapter);
    }

    private void fetchCategory(){
        Log.i(TAG, "fetchCategory: ");

        ModelYoutube.fetchCategory(regionCode, new ModelYoutube.OnFetchCategory() {
            @Override
            public void onSucess(List<ModelYoutube.ItemCategory> list) {
                try {
                    if (list == null) return;

                    //add category most popular, dgn categoryId = 0
                    ModelYoutube.ItemCategory category0 = new ModelYoutube.ItemCategory();
                    category0.id = "0";
                    mList.add(category0);

                    mList.addAll(list);
                    adapter.notifyDataSetChanged();

                    //focus pada category pertama
                    setFocus();
                }catch (Exception ex)
                {
                    Debug.e(TAG, ex);
                }
            }

            @Override
            public void onError(Exception e) {
            }
        });
    }

////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////

    View selectedItem;

    class ItemViewHolder extends RecyclerView.ViewHolder {
        private static final String TAG = "ItemViewHolder";

        TextView textView;

        ItemViewHolder(View itemView) {
            super(itemView);

            try {
                textView = itemView.findViewById(R.id.text_view_category);

            } catch (Exception e) {
                Debug.e(TAG, e);
            }
        }

        public void bind(ModelYoutube.ItemCategory item, int pos) {
            try {

                String title;

                if (pos==0){
                    title = "Most Popular";
                }else{
                    title = item.snippet.title;
                }

                textView.setText(title);

                //select first item, apabila blm ada
                if (selectedItem==null && pos==0) {
                    selectedItem = itemView;
                    itemView.setSelected(true);
                    itemView.requestFocus();
                }

//                itemView.setOnFocusChangeListener(new View.OnFocusChangeListener() {
//                    @Override
//                    public void onFocusChange(View view, boolean b) {
//                        if (b){
//
//                            //deselect previous item
//                            if (selectedItem!=null) selectedItem.setSelected(false);
//
//                            //select item in focus
//                            view.setSelected(true);
//
//                            selectedItem = view;
//
//                            //apabila yg di focus item yg berbeda maka lakukan callback
//                            if (callback!=null) callback.onClick(item.id);
//
//                        } else {
////                            Log.i(TAG, "onFocusChange: LOST " + item.category);
//                        }
//                    }
//                });


                itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        if (BuildConfig.DEBUG) Log.i(TAG, "onClick: " + item.id);

                        //handle selection
                        //hanya satu yg bisa di select
                        if (selectedItem!=null) selectedItem.setSelected(false);
                        selectedItem = itemView;
                        selectedItem.setSelected(true);
//                        idxSelectedItem = pos;

                        //handle click di category
                        if (callback!=null) callback.onClick(item.id);
                    }
                });

            } catch (Exception e) {
                Debug.e(TAG, e);
            }
        }
    }

    public View getFocusedChild(){
        return recyclerView.getFocusedChild();
    }

    /**
     * Focus ke item yg sdh di select, apabila blm ada maka ke first item
     * @return
     */
    public boolean setFocus(){

        Log.i(TAG, "setFocusAndEnableView: ");

        try {

            //jadikan first item selecteditem apabile blm ada
            if (selectedItem==null) selectedItem = recyclerView.getChildAt(0);

            //select item
            return selectedItem.requestFocus();

        } catch (Exception e) {
            e.printStackTrace();
        }

        return false;
    }

}

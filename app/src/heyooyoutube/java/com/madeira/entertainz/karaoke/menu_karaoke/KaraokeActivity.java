package com.madeira.entertainz.karaoke.menu_karaoke;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.madeira.entertainz.karaoke.DBLocal.JoinSettingElementMedia;
import com.madeira.entertainz.karaoke.DBLocal.TSticker;
import com.madeira.entertainz.karaoke.Debug;
import com.madeira.entertainz.karaoke.R;
import com.madeira.entertainz.karaoke.config.C;
import com.madeira.entertainz.karaoke.config.Global;
import com.madeira.entertainz.karaoke.menu_main.BottomNavigationFragment;
import com.madeira.entertainz.karaoke.menu_main.ListMoodFragment;
import com.madeira.entertainz.karaoke.menu_navigation.TopNavigationFragment;
import com.madeira.entertainz.karaoke.menu_playlist.PlaylistActivity;
import com.madeira.entertainz.karaoke.model_room_db.ModelElementMedia;
import com.madeira.entertainz.library.Util;

import net.yslibrary.android.keyboardvisibilityevent.KeyboardVisibilityEvent;
import net.yslibrary.android.keyboardvisibilityevent.KeyboardVisibilityEventListener;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.List;

import es.dmoral.toasty.Toasty;

import static com.madeira.entertainz.karaoke.config.CacheData.lastIndexBackground;

public class KaraokeActivity extends AppCompatActivity {
    String TAG = "KaraokeActivity";
    ImageView backgroundIV;
    FrameLayout popupFrameLayout;
    View listMoodView;

    public static final String KEY_PLAYLIST_ID = "KEY_PLAYLIST_ID";
    int playlistId = 0;

    TopNavigationFragment topNavigationFragment = new TopNavigationFragment();
    KaraokeCategoryFragment karaokeCategoryFragment = new KaraokeCategoryFragment();
    KaraokeListFragment karaokeListFragment = new KaraokeListFragment();
    BottomNavigationFragment bottomNavigationFragment = new BottomNavigationFragment();
    ListMoodFragment listMoodFragment = new ListMoodFragment();

    //    KaraokePopupFragment karaokePopupFragment = new KaraokePopupFragment();
    Handler handler = new Handler();
    Runnable runnable;

    public static void startActivity(Activity activity, Integer prmPlaylistId) {
        Intent intent = new Intent(activity, KaraokeActivity.class);
        intent.putExtra(KEY_PLAYLIST_ID, prmPlaylistId);
        activity.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            setContentView(R.layout.activity_karaoke);
            bind();
            fetchListBackground();
            bindFragment();
            playlistId = getIntent().getIntExtra(KEY_PLAYLIST_ID, 0);
            Util.hideNavigationBar(this);
            KeyboardVisibilityEvent.setEventListener(
                    this,
                    new KeyboardVisibilityEventListener() {
                        @Override
                        public void onVisibilityChanged(boolean isOpen) {
                            // some code depending on keyboard visiblity status
                            if (isOpen) {
                                Toasty.info(KaraokeActivity.this, getString(R.string.pressBackToHideKeyBoard)).show();
                            }

                        }
                    });

            String languageId = Global.getLanguageId();
            Util.setLanguage(this, languageId);
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }

    }

    void bind() {
        backgroundIV = (ImageView) findViewById(R.id.backgroundIV);
        listMoodView = findViewById(R.id.listMoodFrameLayout);
        popupFrameLayout = (FrameLayout) findViewById(R.id.popupFrameLayout);
    }

    void bindFragment() {
        try {
//        topNavigationFragment = topNavigationFragment.newInstance(true);
            topNavigationFragment = topNavigationFragment.newInstance(C.MENU_SEARCH_SONG);
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.topNavFrameLayout, topNavigationFragment);
            ft.replace(R.id.bottomFrameLayout, bottomNavigationFragment);
            ft.replace(R.id.categoryFrameLayout, karaokeCategoryFragment);
            ft.replace(R.id.listMoodFrameLayout, listMoodFragment);

            Bundle listBundle = new Bundle();
            listBundle.putInt(karaokeListFragment.KEY_CATEGORY_ID, 0);
            listBundle.putString(karaokeListFragment.KEY_CATEGORY_TITLE, getString(R.string.all));
            listBundle.putInt(KEY_PLAYLIST_ID, getIntent().getIntExtra(KEY_PLAYLIST_ID, 0));
            karaokeListFragment.setArguments(listBundle);
            ft.replace(R.id.songFrameLayout, karaokeListFragment).addToBackStack(null);

            ft.commit();
            karaokeCategoryFragment.setCallback(iKaraokeCategoryListener);
            karaokeListFragment.setCallback(iKaraokeListListener);
            bottomNavigationFragment.setCallback(bottomNavigationFragmentListener);
            listMoodFragment.setCallBack(listMoodListener);
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }


    }

    void fetchListBackground() {
        ModelElementMedia.getListElementMediaActive(KaraokeActivity.this, new ModelElementMedia.IGetListElementMediaActive() {
            @Override
            public void onGetListElementMedia(List<JoinSettingElementMedia> joinSettingElementMediaList) {
                int totalIndex = joinSettingElementMediaList.size();
                runnable = new Runnable() {
                    @Override
                    public void run() {
                        if (joinSettingElementMediaList.size() != 0) {
                            if (lastIndexBackground >= totalIndex)
                                lastIndexBackground = 0;

                            JoinSettingElementMedia joinSettingElementMedia = joinSettingElementMediaList.get(lastIndexBackground);
                            Uri uri = Uri.fromFile(new File(joinSettingElementMedia.urlImage));
//                            Picasso.with(KaraokeActivity.this).load(uri).into(backgroundIV);
                            try {
                                InputStream inputStream = getContentResolver().openInputStream(uri);
                                BitmapFactory.Options options = new BitmapFactory.Options();
                                options.inPreferredConfig = Bitmap.Config.RGB_565;
                                Bitmap image = BitmapFactory.decodeStream(inputStream, null, options);
                                backgroundIV.setImageBitmap(image);
                            } catch (FileNotFoundException e) {
                                e.printStackTrace();
                            }
                            lastIndexBackground++;
                            handler.postDelayed(this::run, joinSettingElementMedia.duration);
                        }

                    }
                };
                handler.postDelayed(runnable, 100);
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        //agar runable tidak running ketika di finish activity
        try {
            Util.hideNavigationBar(this);
            if (popupFrameLayout.getVisibility() == View.GONE) {
                if (playlistId == 0) {
                    handler.removeCallbacks(runnable);
                    handler.removeCallbacksAndMessages(null);
                    finish();
                } else {
                    PlaylistActivity.startActivity(KaraokeActivity.this, playlistId);
                    finish();
                }
            } else {
                popupFrameLayout.setVisibility(View.GONE);
            }
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    KaraokeCategoryFragment.IKaraokeCategoryListener iKaraokeCategoryListener = new KaraokeCategoryFragment.IKaraokeCategoryListener() {
        @Override
        public void onCategoryClick(int categoryId, String category) {
            //detach first, for refresh fragment

//            karaokeListFragment.newInstance(categoryId);
            try {
                karaokeListFragment = new KaraokeListFragment();
                Bundle args = new Bundle();
                args.putInt(KaraokeListFragment.KEY_CATEGORY_ID, categoryId);
                args.putString(KaraokeListFragment.KEY_CATEGORY_TITLE, category);
                args.putInt(KEY_PLAYLIST_ID, getIntent().getIntExtra(KEY_PLAYLIST_ID, 0));
                karaokeListFragment.setArguments(args);

                FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                ft.replace(R.id.songFrameLayout, karaokeListFragment).addToBackStack(null);
                ft.commit();
            } catch (Exception ex) {
                Debug.e(TAG, ex);
            }
        }
    };

    KaraokeListFragment.IKaraokeListListener iKaraokeListListener = new KaraokeListFragment.IKaraokeListListener() {
        @Override
        public void onListClick(int songId) {
          /*  Bundle bundle = new Bundle();
            bundle.putInt(KaraokePopupFragment.KEY_SONGID, songId);
            karaokePopupFragment.setArguments(bundle);
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.popupFrameLayout, karaokePopupFragment);
            ft.commit();
            popupFrameLayout.setVisibility(View.VISIBLE);*/
        }
    };

    @Override
    public void onResume() {
        super.onResume();
        try {
            Util.hideNavigationBar(this);
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    /**
     * listener ketika mood di click
     */
    BottomNavigationFragment.IMoodFragmentListener bottomNavigationFragmentListener = new BottomNavigationFragment.IMoodFragmentListener() {
        @Override
        public void onMoodClick() {
            listMoodView.setVisibility(View.VISIBLE);
        }
    };

    /**
     * listener ketika mood dipilih
     */
    ListMoodFragment.IListMoodListener listMoodListener = new ListMoodFragment.IListMoodListener() {
        @Override
        public void onClick(TSticker tSticker) {
            try {
                listMoodView.setVisibility(View.GONE);
                bottomNavigationFragment.updateMood(tSticker);
                Global.setStickerId(tSticker.stickerId);
            } catch (Exception ex) {
                Debug.e(TAG, ex);
            }
        }
    };

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
//        super.onActivityResult(requestCode, resultCode, data);

        try {
            //ugly hack, hack ini dibutuhkan, krn fragment tdk menerima event ini.
            //shg harus di pass dari activity

            //pass event ini ke fragment
            if (karaokeListFragment != null)
                karaokeListFragment.onActivityResult(requestCode, resultCode, data);
        }catch (Exception ex)
        {
            Debug.e(TAG, ex);
        }
    }

    @Override
    public void onDestroy()
    {
        try
        {
            super.onDestroy();
            Util.freeMemory();
        }
        catch (Exception ex)
        {
            Debug.e(TAG, ex);
        }
    }
}

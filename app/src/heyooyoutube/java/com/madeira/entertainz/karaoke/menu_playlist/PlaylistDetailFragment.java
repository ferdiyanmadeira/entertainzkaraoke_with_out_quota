package com.madeira.entertainz.karaoke.menu_playlist;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.ScaleAnimation;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.madeira.entertainz.controller.USBControl;
import com.madeira.entertainz.karaoke.DBLocal.JoinSongCountPlayed;
import com.madeira.entertainz.karaoke.DBLocal.JoinSongPlaylist;
import com.madeira.entertainz.karaoke.Debug;
import com.madeira.entertainz.karaoke.R;
import com.madeira.entertainz.karaoke.config.C;
import com.madeira.entertainz.karaoke.config.Global;
import com.madeira.entertainz.karaoke.menu_karaoke.KaraokeActivity;
import com.madeira.entertainz.karaoke.DBLocal.DbSong;
import com.madeira.entertainz.karaoke.model_room_db.ModelPlaylist;
import com.madeira.entertainz.karaoke.player.ExoPlayerActivity;
import com.madeira.entertainz.library.EzRecyclerViewAdapter;
import com.madeira.entertainz.library.PerformAsync2;
import com.madeira.entertainz.library.Util;
import com.madeira.entertainz.library.UtilRenderScript;
import com.mukesh.OtpView;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import es.dmoral.toasty.Toasty;


public class PlaylistDetailFragment extends Fragment implements PlaylistTheme.IPlaylistThemeListener {
    String TAG = "PlaylistDetailFragment";

    public static final String KEY_PLAYLIST_ID = "KEY_PLAYLIST_ID";
    public static final String KEY_PLAYLIST_NAME = "KEY_PLAYLIST_NAME";

    private int playlistId;
    private String playlistName;
    static String passwordSetting = "";

    PlaylistTheme playlistTheme;
    ArrayList<JoinSongPlaylist> tSongPlaylistList = new ArrayList<>();
    EzRecyclerViewAdapter adapter;
    USBControl usbControl = new USBControl();

    View root;
    LinearLayout rootLL;
    RecyclerView recyclerView;
    LinearLayout actionLL;
    Button renameButton;
    Button addSongButton;
    Button deleteButton;
    TextView playlistNameTV;
    EditText playlistNameET;


    private IPlaylistDetailListener mListener;

    public PlaylistDetailFragment() {
        // Required empty public constructor
    }

    public interface IPlaylistDetailListener {
        void onListClick(ArrayList<JoinSongCountPlayed> tSongs, int position);

        void onDeleteOrRenamePlaylist();
    }

    public static PlaylistDetailFragment newInstance(String prmPlaylistId, String prmPlaylistName) {
        PlaylistDetailFragment fragment = new PlaylistDetailFragment();
        Bundle args = new Bundle();
        args.putString(KEY_PLAYLIST_ID, prmPlaylistId);
        args.putString(KEY_PLAYLIST_NAME, prmPlaylistName);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        try {
            super.onCreate(savedInstanceState);
            if (getArguments() != null) {
                playlistId = getArguments().getInt(KEY_PLAYLIST_ID);
                playlistName = getArguments().getString(KEY_PLAYLIST_NAME);
                fetchSongPlaylist();
            }
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        root = inflater.inflate(R.layout.fragment_playlist_detail, container, false);
        try {
            bind();
            playlistTheme = new PlaylistTheme(getActivity(), this);
            playlistTheme.setTheme();
            setOnActionObject();
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
        return root;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void setThemeUpdateDate(Date lastUpdateTheme) {

    }

    @Override
    public void setListColorTheme(int color) {
        try {
            rootLL.setBackgroundResource(R.drawable.tags_rounded_corners);
            GradientDrawable rootDrawable = (GradientDrawable) rootLL.getBackground();
            rootDrawable.setColor(color);

            actionLL.setBackgroundResource(R.drawable.tags_rounded_corners);
            GradientDrawable searchDrawable = (GradientDrawable) actionLL.getBackground();
            searchDrawable.setColor(color);
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    @Override
    public void setTextAndSelectionColorTheme(ColorStateList colorSelection, ColorStateList colorText, int colorTextSelected, int colorTextFocus, int colorTextNormal, int colorSelectionSelected, int colorSelectionFocus) {
        try {
            this.colorSelection = colorSelection;
            this.colorText = colorText;
            playlistNameTV.setTextColor(colorTextNormal);
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    public void setCallback(IPlaylistDetailListener listener) {
        this.mListener = listener;
    }

    void bind() {
        rootLL = root.findViewById(R.id.rootLL);
        recyclerView = root.findViewById(R.id.recyclerView);
        actionLL = root.findViewById(R.id.actionLL);
        renameButton = root.findViewById(R.id.renameButton);
        playlistNameTV = root.findViewById(R.id.playlistNameTV);
        addSongButton = root.findViewById(R.id.addSongButton);
        deleteButton = root.findViewById(R.id.deletePlaylistButton);
        playlistNameET = root.findViewById(R.id.playlistNameET);
        playlistNameTV.setText(playlistName);
        playlistNameET.setText(playlistName);
    }

    void fetchSongPlaylist() {
        Context mContext = getActivity();
        if (mContext != null) {
            ModelPlaylist.getPlaylistById(mContext, playlistId, new ModelPlaylist.IGetPlaylistById() {
                @Override
                public void onGetListPlaylistById(List<JoinSongPlaylist> joinSongPlaylists) {
                    try {
                        tSongPlaylistList.clear();
                        tSongPlaylistList.addAll(joinSongPlaylists);
                        setRecyclerView();
                        Log.d(TAG, String.valueOf(tSongPlaylistList.size()));
                    } catch (Exception ex) {
                        Debug.e(TAG, ex);
                    }
                }
            });
        }
    }

    /**
     * reload ini hanya di panggil saat user merubah order
     */
    void reloadPlaylist() {
        Log.i(TAG, "reloadPlaylist: ");
        ModelPlaylist.getPlaylistById(getContext(), playlistId, new ModelPlaylist.IGetPlaylistById() {
            @Override
            public void onGetListPlaylistById(List<JoinSongPlaylist> joinSongPlaylists) {
                try {
                    //replace current list, dgn list yg baru di load dari room
                    tSongPlaylistList = (ArrayList<JoinSongPlaylist>) joinSongPlaylists;
                    adapter.notifyDataSetChanged();
                } catch (Exception ex) {
                    Debug.e(TAG, ex);
                }
            }
        });

    }

    void setRecyclerView() {
        try {
            adapter = new EzRecyclerViewAdapter(new EzRecyclerViewAdapter.IEzRecyclerViewAdapter() {
                @Override
                public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
                    View view = LayoutInflater.from(getActivity()).inflate(R.layout.cell_playlist_detail, parent, false);
                    ItemViewHolder item = new ItemViewHolder(view);
                    return item;
                }

                @Override
                public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
                    ItemViewHolder item = (ItemViewHolder) holder;
                    item.bind(tSongPlaylistList.get(position), position);
                }

                @Override
                public int getItemCount() {
                    return tSongPlaylistList.size();
                }

                @Override
                public long getItemId(int pos) {
                    return tSongPlaylistList.get(pos).songPlaylistId;
                }
            });

            GridLayoutManager gridLayoutManager = new GridLayoutManager(getActivity(), 5);
            recyclerView.setLayoutManager(gridLayoutManager);
            recyclerView.setAdapter(adapter);
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }

    }

    class ItemViewHolder extends RecyclerView.ViewHolder {
        private static final String TAG = "ItemViewHolder";

        View root;
        TextView tvTitle;
        TextView tvSinger;
        ImageView thumbnailIV;

        public ItemViewHolder(View itemView) {
            super(itemView);
            root = itemView;
            selectedView = root;
            tvTitle = itemView.findViewById(R.id.text_title);
            tvSinger = itemView.findViewById(R.id.text_singer);
            thumbnailIV = itemView.findViewById(R.id.thumbnailIV);
        }

        public void bind(JoinSongPlaylist tSongPlaylist, int pos) {
            try {
                tvTitle.setText(tSongPlaylist.songName);
                tvSinger.setText(tSongPlaylist.artist);
                if (tSongPlaylist.thumbnail != null) {
                    UtilRenderScript.fetchBitmap(getContext(), tSongPlaylist.thumbnail, 300, thumbnailIV);

                }


                if (colorSelection != null) root.setBackgroundTintList(colorSelection);
//            untuk merubah warna text
                if (colorText != null) {
                    tvTitle.setTextColor(colorText);
                    tvSinger.setTextColor(colorText);
                }


                root.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        showDetailSong(tSongPlaylist, pos);
                        Util.hideNavigationBar(getActivity());
                    }
                });

                root.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                    @Override
                    public void onFocusChange(View v, boolean hasFocus) {
                        if (hasFocus) {
                            updateSelectedViewUi(v, true);
                        } else {
                            updateSelectedViewUi(null, false);

                        }
                    }
                });
            } catch (Exception ex) {
                Debug.e(TAG, ex);
            }
        }


    }

    ColorStateList colorSelection, colorText;

    //function untuk declare semua action dari object di activity
    void setOnActionObject() {

        try {
            renameButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    playlistNameET.setVisibility(View.VISIBLE);
                    playlistNameTV.setVisibility(View.GONE);
                    playlistNameET.requestFocus();
                }
            });
            addSongButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    KaraokeActivity.startActivity(getActivity(), playlistId);
                    getActivity().finish();
                }
            });
            deleteButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
//                    showDialogDeletePlaylist();
                    showDialogConfirmDeletePlaylist();
                }
            });

            playlistNameET.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {
                    if (!playlistName.equals(s.toString())) {
                        ModelPlaylist.renamePlaylist(getActivity(), playlistId, s.toString(), new ModelPlaylist.IRenamePlaylist() {
                            @Override
                            public void onRenamePlaylist(int result) {
                                if (result == 1) {
                                    playlistNameTV.setText(playlistNameET.getText());
                                    usbControl.writePlaylistFile(getActivity());
                                } else if (result== 2) {
                                    Toasty.error(getActivity(), getString(R.string.playlistAlreadyExists)).show();
                                }
                            }
                        });
                    }
                }
            });

            playlistNameET.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View v, boolean hasFocus) {
                    if (!hasFocus) {
                        mListener.onDeleteOrRenamePlaylist();
                    } else {
                        Util.showKeyboard(getActivity());
                        playlistNameET.setSelection(playlistNameET.getText().length());
                    }
                }
            });
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }


    }

    void showDetailSong(JoinSongPlaylist tSongPlaylist, int prmPosition) {
        try {
            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
            LayoutInflater inflater = this.getLayoutInflater();
            View dialogView = inflater.inflate(R.layout.alert_playlist_song, null);
            dialogBuilder.setView(dialogView);

            ImageView thumbnailIV = dialogView.findViewById(R.id.thumbnailIV);
            TextView titleTV = dialogView.findViewById(R.id.titleTV);
            TextView singerTV = dialogView.findViewById(R.id.singerTV);
            Button playSongButton = dialogView.findViewById(R.id.playSongButton);
            Button changeOrderButton = dialogView.findViewById(R.id.changeOrderButton);
            Button deleteSongButton = dialogView.findViewById(R.id.deleteSongButton);

            if (tSongPlaylist.thumbnail != null) {
                UtilRenderScript.fetchBitmap(getContext(), tSongPlaylist.thumbnail, 300, thumbnailIV);
//            Picasso.with(getActivity()).load(tSongPlaylist.thumbnail).into(thumbnailIV);
            }
            titleTV.setText(tSongPlaylist.songName);
            singerTV.setText(tSongPlaylist.artist);
            AlertDialog alertDialog = dialogBuilder.create();

            playSongButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ExoPlayerActivity.startActivity(getActivity(), tSongPlaylistList, prmPosition);
                    alertDialog.dismiss();

                }
            });

            changeOrderButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    //tampilkan activity utk change order
                    PlaylistChangeOrderActivity.startActivity(getActivity(), playlistId, tSongPlaylist.songId);
                    alertDialog.dismiss();
                }
            });

            deleteSongButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    deleteSong(tSongPlaylist, prmPosition);
                    alertDialog.dismiss();
                }
            });

            alertDialog.show();
            WindowManager.LayoutParams layoutParams = alertDialog.getWindow().getAttributes();
            layoutParams.width = ViewGroup.LayoutParams.WRAP_CONTENT;
            alertDialog.show();
            alertDialog.getWindow().setLayout(430, WindowManager.LayoutParams.WRAP_CONTENT);
            Util.hideNavigationBar(getActivity());
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }

    }

    void deleteSong(JoinSongPlaylist tSongPlaylist, int prmPosition) {
        try {
            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getContext());
            LayoutInflater inflater = this.getLayoutInflater();
            View dialogView = inflater.inflate(R.layout.alert_delete_confirmation, null);
            dialogBuilder.setView(dialogView);
            Button okButton = (Button) dialogView.findViewById(R.id.okButton);
            Button cancelButton = (Button) dialogView.findViewById(R.id.cancelButton);

            AlertDialog alertDialog = dialogBuilder.create();

            okButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ModelPlaylist.deleteSongPlaylist(getContext(),  tSongPlaylist.playlistId, tSongPlaylist.songId, new ModelPlaylist.IDeleteSongPlaylist() {
                        @Override
                        public void onDeleteSongPlaylist(int result) {
                            if (result == 1) {
                                fetchSongPlaylist();
                                usbControl.writePlaylistFile(getContext());
                            } else {
                                Toasty.error(getActivity(), getString(R.string.failedDeleteSong)).show();
                            }
                            alertDialog.dismiss();
                        }
                    });
                }
            });

            cancelButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    alertDialog.dismiss();

                }
            });

            alertDialog.show();
            WindowManager.LayoutParams layoutParams = alertDialog.getWindow().getAttributes();
            layoutParams.width = ViewGroup.LayoutParams.WRAP_CONTENT;
            alertDialog.show();
            alertDialog.getWindow().setLayout(350, WindowManager.LayoutParams.WRAP_CONTENT);
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }

    }


    void updateSelectedViewUi(View view, boolean hasFocus) {
        try {
            if (selectedView != null) {
                scaleView(selectedView, 1.1f, 1);
                ViewCompat.setTranslationZ(selectedView, 0);
            }

            if (hasFocus) {
                selectedView = view;
                ViewCompat.setTranslationZ(selectedView, 100);
//                selectedView.setBackgroundResource(R.drawable.box_white);
                scaleView(selectedView, 1, 1.1f);
            } else {
                //remove selected & focus dari grid
                selectedView = null;
            }
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    public void scaleView(View v, float startScale, float endScale) {
        try {
            Animation anim = new ScaleAnimation(
                    startScale, endScale, // Start and end values for the X axis scaling
                    startScale, endScale, // Start and end values for the Y axis scaling
                    Animation.RELATIVE_TO_SELF, 0.5f, // Pivot point of X scaling
                    Animation.RELATIVE_TO_SELF, 0.5f); // Pivot point of Y scaling
            anim.setFillAfter(true); // Needed to keep the result of the animation
            anim.setDuration(100);
            v.startAnimation(anim);
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    static View selectedView;

    void showDialogConfirmDeletePlaylist() {
        try {
            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getContext());
            LayoutInflater inflater = this.getLayoutInflater();
            View dialogView = inflater.inflate(R.layout.alert_delete_playlist_confirmation, null);
            dialogBuilder.setView(dialogView);

            Button okButton = (Button) dialogView.findViewById(R.id.okButton);
            Button cancelButton = (Button) dialogView.findViewById(R.id.cancelButton);

            AlertDialog alertDialog = dialogBuilder.create();

            okButton.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    deletePlaylist();
                    alertDialog.dismiss();
                }
            });

            cancelButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    alertDialog.dismiss();

                }
            });

            alertDialog.show();
            WindowManager.LayoutParams layoutParams = alertDialog.getWindow().getAttributes();
            layoutParams.width = ViewGroup.LayoutParams.WRAP_CONTENT;
            alertDialog.show();
            alertDialog.getWindow().setLayout(430, WindowManager.LayoutParams.WRAP_CONTENT);
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    void showDialogDeletePlaylist() {
        try {
            passwordSetting = Global.getDefaultPasswordSetting();
            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getContext());
            LayoutInflater inflater = this.getLayoutInflater();
            View dialogView = inflater.inflate(R.layout.alert_enter_setting, null);
            dialogBuilder.setView(dialogView);

//        EditText passwordET = dialogView.findViewById(R.id.passwordET);
            OtpView otpView = dialogView.findViewById(R.id.otpView);
            Button okButton = (Button) dialogView.findViewById(R.id.okButton);
            Button cancelButton = (Button) dialogView.findViewById(R.id.cancelButton);
            Button forgotPasswordButton = dialogView.findViewById(R.id.forgotPasswordButton);
            TextView infoTV = dialogView.findViewById(R.id.infoTV);

            AlertDialog alertDialog = dialogBuilder.create();

            okButton.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    if (passwordSetting.equals(otpView.getText().toString())) {
                        deletePlaylist();
                        alertDialog.dismiss();
                    } else {
                        otpView.setError(getString(R.string.yourPasswordWrong));
                        otpView.setText("");
                        otpView.requestFocus();
                    }
                }
            });

            cancelButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    alertDialog.dismiss();

                }
            });

            forgotPasswordButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Global.setDefaultPasswordSetting(C.default_password_setting);
                    passwordSetting = C.default_password_setting;
                    infoTV.setVisibility(View.VISIBLE);
                }
            });

            otpView.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {
                    int length = s.length();
                    if (length == 6) {
                        okButton.requestFocus();
                    }
                }
            });


            alertDialog.show();
            WindowManager.LayoutParams layoutParams = alertDialog.getWindow().getAttributes();
            layoutParams.width = ViewGroup.LayoutParams.WRAP_CONTENT;
            alertDialog.show();
            alertDialog.getWindow().setLayout(430, WindowManager.LayoutParams.WRAP_CONTENT);
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    void deletePlaylist() {
        ModelPlaylist.deletePlaylist(getContext(), playlistId, new ModelPlaylist.IDeletePlaylist() {
            @Override
            public void onDeletePlaylist(int result) {
                if (result == 1) {
                    adapter.notifyDataSetChanged();
                    mListener.onDeleteOrRenamePlaylist();
                    usbControl.writePlaylistFile(getContext());
                }
            }
        });

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
//        super.onActivityResult(requestCode, resultCode, data);

        Log.i(TAG, "onActivityResult: " + requestCode);

        //check apakah ada request code dari change order ??
        //kalo ada artinya user melakukan change order
        if (PlaylistChangeOrderActivity.checkRequestCode(requestCode)) {
            reloadPlaylist();
        }
    }


}

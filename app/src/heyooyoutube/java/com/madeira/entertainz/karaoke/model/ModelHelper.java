package com.madeira.entertainz.karaoke.model;

import com.google.gson.Gson;
import com.madeira.entertainz.library.ApiError;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.Arrays;

public class ModelHelper {
    private static final String TAG = "ModelHelper";

    static public String httpRequest(Gson gson, String urlString) throws ApiError {
        byte []all = null;
        ApiError error;

//        Log.i(TAG,"url="+urlString );

        try {
            URLConnection conn = new URL(urlString).openConnection();
            conn.setConnectTimeout(10000);
            conn.setReadTimeout(10000);

            int bufferLength;
            byte []buffer = new byte[8192];

            BufferedInputStream bins = new BufferedInputStream(conn.getInputStream());

            all = new byte[0];
            while ( (bufferLength = bins.read(buffer)) > 0 ) {
                all = join(all, buffer, bufferLength);
            }

            bins.close();

        } catch (MalformedURLException e) {
            e.printStackTrace();
            throw  new ApiError(ApiError.ERR_INVALID_URL, e.getMessage());
        } catch (IOException e){
            e.printStackTrace();
            throw  new ApiError(ApiError.ERR_IO_EXCEPTION, e.getMessage());
        }

        String result = new String(all);

        ApiError apiError = ApiError.parseApiError(gson, result);

        if (apiError!=null) throw apiError;

        return result;
    }

    private static byte []join(byte[] all, byte[] buffer, int bufferLength){
        int idx = all.length;
        byte []newAll = Arrays.copyOf(all, all.length + bufferLength); //copy and make array bigger
        System.arraycopy(buffer, 0, newAll, idx , bufferLength);
        return newAll;
    }


}

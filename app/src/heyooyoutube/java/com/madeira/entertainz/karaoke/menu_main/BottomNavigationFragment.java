package com.madeira.entertainz.karaoke.menu_main;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.androidnetworking.error.ANError;
import com.madeira.entertainz.controller.STBControl;
import com.madeira.entertainz.karaoke.DBLocal.TSticker;
import com.madeira.entertainz.karaoke.Debug;
import com.madeira.entertainz.karaoke.DownloadSongService;
import com.madeira.entertainz.karaoke.DownloadYoutubeService;
import com.madeira.entertainz.karaoke.R;
import com.madeira.entertainz.karaoke.DBLocal.TElement;
import com.madeira.entertainz.karaoke.config.C;
import com.madeira.entertainz.karaoke.config.Global;
import com.madeira.entertainz.karaoke.model_online.ModelApp;
import com.madeira.entertainz.karaoke.model_room_db.ModelMood;
import com.madeira.entertainz.library.AnimFadeInOut;
import com.madeira.entertainz.library.UtilRenderScript;

import java.util.Date;

import es.dmoral.toasty.Toasty;

public class BottomNavigationFragment extends Fragment implements MainTheme.IMainThemeListener {

    String TAG = "BottomNavigationFragment";
    private static final int ANIM_NEW_MESSAGE_DURATION = 1000;

    View root;
    RelativeLayout rootRL;
    MainTheme mainTheme;
    IMoodFragmentListener callback;
    LinearLayout moodLL;
    ImageView moodIV;
    static TextView moodTV;
    TextView dateTV;
    Handler handler = new Handler();
    Runnable runnable;
    ImageView internetIV;
    Runnable internetRunnable;
    Handler internetHandler = new Handler();

    LinearLayout downloadView;
    TextView remainingContentTV;
    TextView totalContentTV;
    ProgressBar progressBar;
    TextView remainingKbTV;
    TextView totalKbTV;

    LinearLayout youtubeDownloadView;
    TextView youtubeRemainingContentTV;
    TextView youtubeTotalContentTV;
    ProgressBar youtubeProgressBar;
    TextView youtubeRemainingKbTV;
    TextView youtubeTotalKbTV;

    LinearLayout downloadAPKView;
    ProgressBar apkProgressBar;
    TextView apkRemainingKbTV;
    TextView apkTotalKbTV;

    ColorStateList colorSelection, colorText;
    Runnable runCheckMessage;
    Handler handlerCheckMessage = new Handler();
    TextView tvNewMessage;
    AnimFadeInOut animFadeInOut;
    STBControl stbControl = new STBControl();

    public BottomNavigationFragment() {
        // Required empty public constructor
    }


    public static BottomNavigationFragment newInstance(String param1, String param2) {
        BottomNavigationFragment fragment = new BottomNavigationFragment();
        Bundle args = new Bundle();
       /* args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);*/
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
       /* if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }*/

        showDateTime();
        checkInternet();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        root = inflater.inflate(R.layout.fragment_bottom_navigation, container, false);
        try {
            bind();
            mainTheme = new MainTheme(getActivity(), this);
            mainTheme.setTheme();
            setActionObject();
            fetchMood();
            /** get download info dari receiver */
            DownloadSongService.registerReceiver(getActivity(), bc);

            /** get download apk info dari receiver */
            stbControl.registerReceiver(getActivity(), brAPKDownload);
            checkMessageRoom();
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
        return root;
    }

    void bind() {
        try {
            rootRL = (RelativeLayout) root.findViewById(R.id.rootRL);
            moodLL = root.findViewById(R.id.moodLL);
            moodIV = root.findViewById(R.id.moodIV);
            moodTV = root.findViewById(R.id.moodText);
            dateTV = root.findViewById(R.id.date_tv);
            internetIV = root.findViewById(R.id.internet_iv);
            downloadView = root.findViewById(R.id.download_view);
            remainingContentTV = root.findViewById(R.id.remainingContentTV);
            totalContentTV = root.findViewById(R.id.totalContentTV);
            progressBar = root.findViewById(R.id.progressBar);
            remainingKbTV = root.findViewById(R.id.remainingKbTV);
            totalKbTV = root.findViewById(R.id.totalKbTV);
            tvNewMessage = root.findViewById(R.id.text_new_message);
            downloadAPKView = root.findViewById(R.id.download_apk_view);
            apkProgressBar = root.findViewById(R.id.apk_progressbar);
            apkRemainingKbTV = root.findViewById(R.id.apk_remainingKbTV);
            apkTotalKbTV = root.findViewById(R.id.apk_totalKbTV);

            youtubeDownloadView = root.findViewById(R.id.download_youtube_view);
            youtubeRemainingContentTV = root.findViewById(R.id.youtubeRemainingContentTV);
            youtubeTotalContentTV = root.findViewById(R.id.youtubeTotalContentTV);
            youtubeProgressBar = root.findViewById(R.id.youtubeProgressBar);
            youtubeRemainingKbTV = root.findViewById(R.id.youtubeRemainingKbTV);
            youtubeTotalKbTV = root.findViewById(R.id.youtubeTotalKbTV);

        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }

    }

    @Override
    public void setThemeUpdateDate(Date lastUpdateTheme) {

    }

    @Override
    public void setBackgroundTheme(TElement element) {

    }

    @Override
    public void setListColorTheme(int color) {

    }

    @Override
    public void setFooterColorTheme(int color) {
        rootRL.setBackgroundColor(color);
    }

    @Override
    public void setTextAndSelectionColorTheme(ColorStateList colorSelection, ColorStateList colorText, int colorTextSelected, int colorTextFocus, int colorTextNormal, int colorSelectionSelected, int colorSelectionFocus) {
        try {
            this.colorSelection = colorSelection;
            this.colorText = colorText;
            if (this.colorText != null) moodTV.setTextColor(this.colorText);

//            untuk merubah warna selection
            if (this.colorSelection != null) moodTV.setBackgroundTintList(this.colorSelection);
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    @Override
    public void setHeaderColorTheme(int color) {

    }

   /* @Override
    public void getResult(int resultCode, Bundle resultData) {

        try
        {
            Log.d(TAG, "DOWNLOAD SONG");
        }
        catch (Exception ex)
        {
            Debug.e(TAG, ex);
        }
    }*/


    public interface IMoodFragmentListener {
        void onMoodClick();
    }

    public void setCallback(IMoodFragmentListener callback) {
        this.callback = callback;
    }

    void setActionObject() {
        moodLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    callback.onMoodClick();
                } catch (Exception ex) {
                    Debug.e(TAG, ex);
                }
            }
        });

        moodLL.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                try {
                    if (hasFocus)
                        moodTV.setSelected(true);
                    else
                        moodTV.setSelected(false);
                } catch (Exception ex) {
                    Debug.e(TAG, ex);
                }
            }
        });

    }

    /**
     * untuk update sticker
     */
    public void updateMood(TSticker tSticker) {
        try {
            UtilRenderScript.fetchBitmap(getContext(), tSticker.urlImage, 300, moodIV);
            moodTV.setText(tSticker.stickerName);
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }

    }

    /**
     * fetch sticker / mood berdasarkan id
     */
    void fetchMood() {
        ModelMood.getMood(getActivity(), new ModelMood.IGetMood() {
            @Override
            public void IGetMood(TSticker tSticker) {
                try {
                    if (tSticker != null) {
                        updateMood(tSticker);
                    }
                } catch (Exception ex) {
                    Debug.e(TAG, ex);
                }
            }
        });
    }

    /**
     * tampilkan jam , tanggal dan info apakah connect internet atau tidak
     */
    void showDateTime() {
        runnable = new Runnable() {
            @Override
            public void run() {
                try {
//                    SimpleDateFormat sdfDate = new SimpleDateFormat("E, d MMM yyyy HH:mm:ss");
                    String dateTime = stbControl.setCurrentDateTime();
                    dateTV.setText(dateTime);
                    handler.postDelayed(runnable, C.changeTimeInterval);
                } catch (Exception ex) {
                    Debug.e(TAG, ex);
                }
            }
        };
        handler.postDelayed(runnable, C.oneSecondInterval);

    }

    void checkInternet() {
        internetRunnable = new Runnable() {
            @Override
            public void run() {
                /** harus menggunakan model, jika tidak, akan terjadi lag, disetiap pemanggilan onbackground ketika function ini di panggil*/
                ModelApp.checkInternet(new ModelApp.ICheckConnectToHost() {
                    @Override
                    public void onConnected(boolean connected) {
                        if (connected) {
                            internetIV.setBackgroundResource(R.drawable.ic_internet_connected);
                        } else
                            internetIV.setBackgroundResource(R.drawable.ic_internet_not_connected);
                    }

                    @Override
                    public void onError(ANError e) {
                        internetIV.setBackgroundResource(R.drawable.ic_internet_not_connected);
                    }
                });
              /*  boolean checkInternet = Util.checkInternet(getActivity());
                if (checkInternet) {
                    internetIV.setBackgroundResource(R.drawable.ic_internet_connected);
                } else
                    internetIV.setBackgroundResource(R.drawable.ic_internet_not_connected);*/
                internetHandler.postDelayed(internetRunnable, C.checkConnectionInterval);
            }
        };
        internetHandler.postDelayed(internetRunnable, C.oneSecondInterval);

    }

    @Override
    public void onDetach() {
        super.onDetach();
        try {
            //todo set null interface
            callback = null;
            //todo disable runnable jam
            handler.removeCallbacks(runnable);
            //todo disable runnable check internet
            internetHandler.removeCallbacks(internetRunnable);
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        try {
            //unregister broadcast receiver
            DownloadSongService.unregisterReceiver(getActivity(), bc);
            STBControl.unregisterReceiver(getActivity(), brAPKDownload);
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    /**
     * broadcast receiver untuk menerima result dari download song
     */
    private final BroadcastReceiver bc = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // Now you can call all your fragments method here
            try {
//                Log.d(TAG, "DOWNLOAD SONG");

                Bundle bundle = intent.getExtras();
                if (bundle != null) {
                    //set call back untuk ketika finish download dan hanya dipanggil sekali
                    if (downloadView.getVisibility() == View.GONE) {
                        DownloadSongService.registerFinishDownloadReceiver(getActivity(), brDownloadFinish);
                        downloadView.setVisibility(View.VISIBLE);
                    }
                    String downloadedSize = bundle.getString(DownloadSongService.KEY_DOWNLOADED_SIZE);
                    String speed = bundle.getString(DownloadSongService.KEY_DOWNLOAD_SPEED);
                    String totalSize = bundle.getString(DownloadSongService.KEY_TOTAL_SIZE);
                    String downloadItemOrder = bundle.getString(DownloadSongService.KEY_DOWNLOAD_ITEM_ORDER);
                    String sumItemDownload = bundle.getString(DownloadSongService.KEY_SUM_ITEM_DOWNLOAD);
                    double kbDownloaded = bundle.getDouble(DownloadSongService.KEY_DOWNLOADED_IN_KB);
                    double totalSizeInKb = bundle.getDouble(DownloadSongService.KEY_TOTAL_SIZE_IN_KB);

                    remainingContentTV.setText(downloadItemOrder);
                    totalContentTV.setText(sumItemDownload);
                    progressBar.setMax((int) totalSizeInKb);
                    progressBar.setProgress((int) kbDownloaded);
                    remainingKbTV.setText(downloadedSize + " " + "("+totalSize+")");
                    totalKbTV.setText(speed);
                }
            } catch (Exception ex) {
                Debug.e(TAG, ex);
            }
        }
    };
    /**
     * broadcast receiver untuk ketika download song telah selesai
     */
    private final BroadcastReceiver brDownloadFinish = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            try {
                Log.d(TAG, "DOWNLOAD FINISH");
                downloadView.setVisibility(View.GONE);
                DownloadSongService.unregisterFinishDownloadReceiver(getActivity(), brDownloadFinish);
                Bundle bundle = intent.getExtras();
                if (bundle != null) {
                    boolean result = bundle.getBoolean(DownloadSongService.KEY_RESULT_DOWNLOAD);
                    if (result) {
                        Toasty.info(getActivity(), getActivity().getString(R.string.info_download_song_finish)).show();
                    } else {
                        Toasty.info(getActivity(), getActivity().getString(R.string.warning_download_song_failed)).show();
                    }
                }
            } catch (Exception ex) {
                Debug.e(TAG, ex);
            }
        }
    };

    /**
     * check apakah ada message yang belum dibaca
     */
    public void checkMessageRoom() {
        try {
            runCheckMessage = new Runnable() {
                @Override
                public void run() {
                    try {
                        boolean hasUnReadMessage = Global.getUnReadMessage();
                        if (hasUnReadMessage) {
                            showIconUnreadMessage();
                        } else
                            tvNewMessage.setVisibility(View.INVISIBLE);

                        handlerCheckMessage.postDelayed(runCheckMessage, C.oneSecondInterval);
                    } catch (Exception ex) {
                        Debug.e(TAG, ex);
                    }
                }
            };
            handlerCheckMessage.postDelayed(runCheckMessage, C.oneSecondInterval);
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }


    /**
     * set animation logo unread message
     */
    void showIconUnreadMessage() {
        try {
            tvNewMessage.setVisibility(View.VISIBLE);
            if (animFadeInOut == null) {
                animFadeInOut = new AnimFadeInOut(tvNewMessage);
                animFadeInOut.start(ANIM_NEW_MESSAGE_DURATION);
            }
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }

    }

    /**
     * broadcast receiver untuk show progress upgrade version apk
     */
    private final BroadcastReceiver brAPKDownload = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // Now you can call all your fragments method here
            try {
//                Log.d(TAG, "DOWNLOAD SONG");

                Bundle bundle = intent.getExtras();
                if (bundle != null) {
                    //set call back untuk ketika finish download dan hanya dipanggil sekali
                    if (downloadAPKView.getVisibility() == View.GONE) {
                        stbControl.registerFinishDownloadReceiver(getActivity(), brAPKFinishDownload);
//                        DownloadSongService.registerFinishDownloadReceiver(getActivity(), brDownloadFinish);
                        downloadAPKView.setVisibility(View.VISIBLE);
                    }
                    String downloadedSize = bundle.getString(STBControl.KEY_DOWNLOADED_SIZE);
                    String speed = bundle.getString(STBControl.KEY_DOWNLOAD_SPEED);
                    String totalSize = bundle.getString(STBControl.KEY_TOTAL_SIZE);
                    double kbDownloaded = bundle.getDouble(STBControl.KEY_DOWNLOADED_IN_KB);
                    double totalSizeInKb = bundle.getDouble(STBControl.KEY_TOTAL_SIZE_IN_KB);

                    apkProgressBar.setMax((int) totalSizeInKb);
                    apkProgressBar.setProgress((int) kbDownloaded);
                    apkRemainingKbTV.setText(downloadedSize + " " + "("+totalSize+")");
                    apkTotalKbTV.setText(speed);
                }
            } catch (Exception ex) {
                Debug.e(TAG, ex);
            }
        }
    };

    /**
     * broadcast receiver untuk ketika download update apk telah selesai
     */
    private final BroadcastReceiver brAPKFinishDownload = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            try {
                Log.d(TAG, "DOWNLOAD FINISH");
                downloadAPKView.setVisibility(View.GONE);
                stbControl.unregisterFinishDownloadReceiver(getActivity(), brAPKFinishDownload);
                Bundle bundle = intent.getExtras();
                if (bundle != null) {
                    boolean result = bundle.getBoolean(DownloadSongService.KEY_RESULT_DOWNLOAD);
                  /*  if (result) {
                        Toasty.info(getActivity(), getActivity().getString(R.string.info_download_song_finish)).show();
                    } else {
                        Toasty.info(getActivity(), getActivity().getString(R.string.warning_download_song_failed)).show();
                    }*/
                }
                stbControl.updateApp(getActivity());
            } catch (Exception ex) {
                Debug.e(TAG, ex);
            }
        }
    };


    /**
     * broadcast receiver untuk menerima result dari download youtube
     */
    private final BroadcastReceiver brYoutubeDownload = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // Now you can call all your fragments method here
            try {
//                Log.d(TAG, "DOWNLOAD SONG");

                Bundle bundle = intent.getExtras();
                if (bundle != null) {
                    //set call back untuk ketika finish download dan hanya dipanggil sekali
                    if (youtubeDownloadView.getVisibility() == View.GONE) {
                        DownloadYoutubeService.registerFinishDownloadReceiver(getActivity(), brYoutubeDownloadFinish);
                        youtubeDownloadView.setVisibility(View.VISIBLE);
                    }
                    String downloadedSize = bundle.getString(DownloadYoutubeService.KEY_DOWNLOADED_SIZE);
                    String speed = bundle.getString(DownloadYoutubeService.KEY_DOWNLOAD_SPEED);
                    String totalSize = bundle.getString(DownloadYoutubeService.KEY_TOTAL_SIZE);
                    String downloadItemOrder = bundle.getString(DownloadYoutubeService.KEY_DOWNLOAD_ITEM_ORDER);
                    String sumItemDownload = bundle.getString(DownloadYoutubeService.KEY_SUM_ITEM_DOWNLOAD);
                    double kbDownloaded = bundle.getDouble(DownloadYoutubeService.KEY_DOWNLOADED_IN_KB);
                    double totalSizeInKb = bundle.getDouble(DownloadYoutubeService.KEY_TOTAL_SIZE_IN_KB);

                    youtubeRemainingContentTV.setText(downloadItemOrder);
                    youtubeTotalContentTV.setText(sumItemDownload);
                    youtubeProgressBar.setMax((int) totalSizeInKb);
                    youtubeProgressBar.setProgress((int) kbDownloaded);
                    youtubeRemainingKbTV.setText(downloadedSize + " " + "("+totalSize+")");
                    youtubeTotalKbTV.setText(speed);
                }
            } catch (Exception ex) {
                Debug.e(TAG, ex);
            }
        }
    };
    /**
     * broadcast receiver untuk ketika download youtube telah selesai
     */
    private final BroadcastReceiver brYoutubeDownloadFinish = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            try {
                Log.d(TAG, "DOWNLOAD FINISH");
                youtubeDownloadView.setVisibility(View.GONE);
                DownloadYoutubeService.unregisterFinishDownloadReceiver(getActivity(), brYoutubeDownloadFinish);
                Bundle bundle = intent.getExtras();
                if (bundle != null) {
                    boolean result = bundle.getBoolean(DownloadYoutubeService.KEY_RESULT_DOWNLOAD);
                    if (result) {
                        Toasty.info(getActivity(), getActivity().getString(R.string.info_download_song_finish)).show();
                    } else {
                        Toasty.info(getActivity(), getActivity().getString(R.string.warning_download_song_failed)).show();
                    }
                }
            } catch (Exception ex) {
                Debug.e(TAG, ex);
            }
        }
    };


}


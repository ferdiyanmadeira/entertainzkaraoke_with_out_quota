package com.madeira.entertainz.karaoke.menu_main;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.ColorStateList;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.net.wifi.WifiManager;
import android.os.Handler;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.error.ANError;
import com.madeira.entertainz.controller.STBControl;
import com.madeira.entertainz.karaoke.CronService;
import com.madeira.entertainz.karaoke.DBLocal.JoinSettingElementMedia;
import com.madeira.entertainz.karaoke.DBLocal.TElement;
import com.madeira.entertainz.karaoke.DBLocal.TSongCategory;
import com.madeira.entertainz.karaoke.DBLocal.TSongItemServer;
import com.madeira.entertainz.karaoke.DBLocal.TSticker;
import com.madeira.entertainz.karaoke.Debug;
import com.madeira.entertainz.karaoke.DownloadSongService;
import com.madeira.entertainz.karaoke.MainApplication;
import com.madeira.entertainz.karaoke.R;
import com.madeira.entertainz.karaoke.SplashScreenActivity;
import com.madeira.entertainz.karaoke.config.C;
import com.madeira.entertainz.karaoke.config.Global;
import com.madeira.entertainz.karaoke.menu_accessories.AccessoriesActivity;
import com.madeira.entertainz.karaoke.menu_karaoke.KaraokeActivity;
import com.madeira.entertainz.karaoke.menu_live_channel.LiveChannelActivity;
import com.madeira.entertainz.karaoke.menu_messages.MessagesActivity;
import com.madeira.entertainz.karaoke.menu_navigation.NavigationFragment;
import com.madeira.entertainz.karaoke.menu_news.NewsActivity;
import com.madeira.entertainz.karaoke.menu_playlist.PlaylistActivity;
import com.madeira.entertainz.karaoke.exclude.RadioActivity;
import com.madeira.entertainz.karaoke.menu_setting_admin.SettingAdminActivity;
import com.madeira.entertainz.karaoke.menu_settings.SettingActivity;
import com.madeira.entertainz.karaoke.menu_update_song.UpdateSongActivity;
import com.madeira.entertainz.karaoke.menu_youtube.YouTubeActivity;
import com.madeira.entertainz.karaoke.menu_youtube_karaoke.YoutubeKaraokeActivity;
import com.madeira.entertainz.karaoke.menu_youtube_karaoke_playlist.YoutubePlaylistActivity;
import com.madeira.entertainz.karaoke.model_online.ModelApp;
import com.madeira.entertainz.karaoke.model_room_db.ModelElementMedia;
import com.madeira.entertainz.karaoke.model_room_db.ModelSong;
//import com.madeira.entertainz.library.HomeWatcher;
import com.madeira.entertainz.library.KeyGuard;
import com.madeira.entertainz.library.Util;
import com.mukesh.OtpView;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import com.arthenica.mobileffmpeg.Config;
import com.arthenica.mobileffmpeg.FFmpeg;

import static com.madeira.entertainz.karaoke.config.CacheData.lastIndexBackground;

public class MainActivity extends AppCompatActivity implements MainTheme.IMainThemeListener {

    static String TAG = "MainActivity";
    int intentFromOTT = 0;

    ImageView backgroundIV;
    Handler handler = new Handler();
    View listMoodView;
    ProgressBar progressBar;
    boolean firstCreateActivity = false;
    NavigationFragment navigationFragment = new NavigationFragment();
    BottomNavigationFragment bottomNavigationFragment = new BottomNavigationFragment();
    MenuFragment menuFragment = new MenuFragment();
    ListMoodFragment listMoodFragment = new ListMoodFragment();
    //    TopNavigationFragment navigationFragment;
    KeyGuard kg = new KeyGuard(C.PIN_FOR_SETTING);
    static String passwordSetting = "";
    static int countWrongPassword = 0;
    MainTheme mainTheme;
    ColorStateList colorSelection, colorText;
    STBControl stbControl = new STBControl();

    public static void startActivity(Activity activity, int prmIntentFromOTT) {
        try {
            Intent intent = new Intent(activity, MainActivity.class);
//            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.putExtra(SplashScreenActivity.KEY_INTENT_FROM_OTT, prmIntentFromOTT);
            activity.startActivity(intent);
            if (prmIntentFromOTT != 0)
                activity.finish();
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }


    public static void startActivityFromEkma(Context activity) {
        try {
            Intent intent = new Intent(activity, MainActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            activity.startActivity(intent);

        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            setContentView(R.layout.activity_main);
            mainTheme = new MainTheme(this, this);
            mainTheme.setTheme();
            Intent cronIntent = new Intent(MainActivity.this, CronService.class);
            startService(cronIntent);
            bind();
            bindFragment(false);


            fetchListBackground();
            intentFromOTT = getIntent().getIntExtra(SplashScreenActivity.KEY_INTENT_FROM_OTT, 0);
            Util.hideNavigationBar(MainActivity.this);
            SettingActivity.setCallback(iSettingActivityListener);

            String languageId = Global.getLanguageId();
            Util.setLanguage(this, languageId);
            //check update dan show update alert
            stbControl.updateApp(MainActivity.this);
            fetchDownloadSong();
            Util.hideNavigationBar(MainActivity.this);
            showSuccessUpdate();

            //register broadcast receiver
            CronService.registerFinishDownloadReceiver(MainActivity.this, brDownloadFinish);

//            mergeFile();

        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    @Override
    protected void onPause() {
        CronService.unregisterFinishDownloadReceiver(this, brDownloadFinish);
        super.onPause();
    }

    void bind() {
        try {
            backgroundIV = findViewById(R.id.backgroundIV);
            listMoodView = findViewById(R.id.listMoodView);
            progressBar = findViewById(R.id.progressBar);
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    /**
     * resetFragment = true (jika ingin reset semua fragment), resetfragment = false (untuk pertama kali create activity)
     */
    void bindFragment(boolean resetFragment) {

        try {
            if (resetFragment) {
//                navigationFragment = new NavigationFragment();
                bottomNavigationFragment = new BottomNavigationFragment();
//                menuFragment = new MenuFragment();
                listMoodFragment = new ListMoodFragment();
            }
            navigationFragment = navigationFragment.newInstance("");
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.topNavFrameLayout, navigationFragment).addToBackStack(null);
            ft.replace(R.id.moodFrameLayout, bottomNavigationFragment);
            ft.replace(R.id.mainMenuFrameLayout, menuFragment);
            ft.replace(R.id.listMoodFrameLayout, listMoodFragment);
            ft.commit();
            menuFragment.setCallback(menuMainFragmentCallback);
            bottomNavigationFragment.setCallback(bottomNavigationFragmentListener);
            listMoodFragment.setCallBack(listMoodListener);
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    void fetchListBackground() {
        ModelElementMedia.getListElementMediaActive(MainActivity.this, new ModelElementMedia.IGetListElementMediaActive() {
            @Override
            public void onGetListElementMedia(List<JoinSettingElementMedia> joinSettingElementMediaList) {
                try {
                    int totalIndex = joinSettingElementMediaList.size();
                    Runnable runnable = new Runnable() {
                        @Override
                        public void run() {
                            try {
                                if (joinSettingElementMediaList.size() != 0) {
                                    if (lastIndexBackground >= totalIndex)
                                        lastIndexBackground = 0;

                                    JoinSettingElementMedia joinSettingElementMedia = joinSettingElementMediaList.get(lastIndexBackground);
                                    File file = new File(joinSettingElementMedia.urlImage);
                                    if (!file.exists()) {
                                        navigationFragment = navigationFragment.newInstance("");
                                        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                                        ft.replace(R.id.topNavFrameLayout, navigationFragment).addToBackStack(null);
                                        ft.commit();
                                    }
                                    Uri uri = Uri.fromFile(file);
//                    Picasso.with(MainActivity.this).load(uri).into(backgroundIV);
                                    try {
                                        InputStream inputStream = getContentResolver().openInputStream(uri);
                                        BitmapFactory.Options options = new BitmapFactory.Options();
                                        options.inPreferredConfig = Bitmap.Config.RGB_565;
                                        Bitmap image = BitmapFactory.decodeStream(inputStream, null, options);
                                        backgroundIV.setImageBitmap(image);
                                    } catch (FileNotFoundException e) {
                                        e.printStackTrace();
                                    }
                                    lastIndexBackground++;
                                    handler.postDelayed(this::run, joinSettingElementMedia.duration);
                                }
                            } catch (Exception ex) {
                                Debug.e(TAG, ex);
                            }

                        }
                    };
                    handler.postDelayed(runnable, 100);
                } catch (Exception ex) {
                    Log.d(TAG, ex.getMessage());
                }

            }
        });
    }

    MenuFragment.IMenuMainFragmentListener menuMainFragmentCallback = new MenuFragment.IMenuMainFragmentListener() {
        @Override
        public void onClickMenu(String menuName, int menuId) {
            Log.i(TAG, "onFocusChange=" + menuName + " " + menuId);
            intentToActivity(menuId);
        }
    };

    void intentToActivity(int menuId) {

        try {
            switch (menuId) {
                case C.MENU_SEARCH_SONG:
                    Log.d(TAG, "MENU_SEARCH_SONG Clicked");
                    //set playlistId 0 jika bukan untuk add song dari halaman playlist
                    KaraokeActivity.startActivity(MainActivity.this, 0);
                    break;
                case C.MENU_PLAYLIST:
                    Log.d(TAG, "MENU_PLAYLIST Clicked");
                    PlaylistActivity.startActivity(MainActivity.this, 0);
                    break;
                case C.MENU_UPDATE_SONG:
                    Log.d(TAG, "MENU_UPDATE_SONG Clicked");
//                    UpdateSongActivity.startActivity(MainActivity.this);
                    showDownloadKaraoke();
                    break;
                case C.MENU_ACCESSORIES:
                    Log.d(TAG, "MENU_ACCESSORIES Clicked");
                    showEShopping();
                    break;
                case C.MENU_MESSAGES:
                    Log.d(TAG, "MENU_MESSAGES Clicked");
                    MessagesActivity.startActivity(MainActivity.this);
                    break;
                case C.MENU_SETTING:
                    Log.d(TAG, "MENU_SETTING Clicked");
//                    showDialogSetting();
                    SettingActivity.startActivity(MainActivity.this, intentFromOTT);
                    break;
                case C.MENU_LIVE_CHANNEL:
                    Log.d(TAG, "MENU_LIVE_CHANNEL Clicked");
                    showLiveChannel();
                    break;
                case C.MENU_YOUTUBE:
                    Log.d(TAG, "MENU_YOUTUBE Clicked");
                    showYoutubeNow(); //versi tanpa check internet
                    break;
                case C.MENU_YOUTUBE_PLAYLIST:
                    Log.d(TAG, "MENU_YOUTUBE_PLAYLIST Clicked");
                    showYoutubePlaylist();
                    break;
                case C.MENU_NEWS:
                    Log.d(TAG, "MENU_NEWS Clicked");
                    showNews();
                    break;
                default:
                    Toast.makeText(this, "Not implemented", Toast.LENGTH_SHORT).show();
                    break;
            }

            Util.hideNavigationBar(MainActivity.this);
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        try {
            if (keyCode == KeyEvent.KEYCODE_BACK) {
                Util.hideNavigationBar(MainActivity.this);
                //preventing default implementation previous to android.os.Build.VERSION_CODES.ECLAIR
                if (intentFromOTT == 0) {
                    Log.d(TAG, "Back Pressed");
                    if (listMoodView.getVisibility() == View.VISIBLE)
                        listMoodView.setVisibility(View.GONE);
                    return true;
                } else {
                    //minimize app
                    moveTaskToBack(true);
                    finish();
                }
            } else if (keyCode == KeyEvent.KEYCODE_DPAD_RIGHT || keyCode == KeyEvent.KEYCODE_DPAD_UP || keyCode == KeyEvent.KEYCODE_DPAD_LEFT || keyCode == KeyEvent.KEYCODE_DPAD_DOWN) {
                if (progressBar.getVisibility() == View.VISIBLE)
                    return true;
            } else if (keyCode == 306) {
                return false;
            } else if (keyCode == KeyEvent.KEYCODE_MENU) {
                MessagesActivity.startActivity(MainActivity.this);
                return true;
            }
            if (kg.processKey(keyCode)) {
                SettingAdminActivity.startActivity(this);
            }
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
        return super.onKeyDown(keyCode, event);
    }

    void showDialogSetting() {
        try {
            passwordSetting = Global.getDefaultPasswordSetting();
            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
            LayoutInflater inflater = this.getLayoutInflater();
            View dialogView = inflater.inflate(R.layout.alert_enter_setting, null);
            dialogBuilder.setView(dialogView);

//        EditText passwordET = dialogView.findViewById(R.id.passwordET);
            OtpView otpView = dialogView.findViewById(R.id.otpView);
            Button okButton = (Button) dialogView.findViewById(R.id.okButton);
            Button cancelButton = (Button) dialogView.findViewById(R.id.cancelButton);
            Button forgotPasswordButton = dialogView.findViewById(R.id.forgotPasswordButton);
            TextView infoTV = dialogView.findViewById(R.id.infoTV);

            otpView.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {
                    int length = s.length();
                    if (length == 6) {
                        okButton.requestFocus();
                    }
                }
            });

            AlertDialog alertDialog = dialogBuilder.create();

            okButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (passwordSetting.equals(otpView.getText().toString())) {
                        SettingActivity.startActivity(MainActivity.this, intentFromOTT);
                        alertDialog.dismiss();
                    } else {
                        countWrongPassword++;
                        otpView.setError(getString(R.string.wrong_password));
                        otpView.setText("");
                        otpView.requestFocus();
                    }
                    if (countWrongPassword >= 3) {
                        forgotPasswordButton.setVisibility(View.VISIBLE);
                    }
                }
            });

            cancelButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    alertDialog.dismiss();

                }
            });

            forgotPasswordButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Global.setDefaultPasswordSetting(C.default_password_setting);
                    passwordSetting = C.default_password_setting;
                    infoTV.setVisibility(View.VISIBLE);
                }
            });


            alertDialog.show();
            WindowManager.LayoutParams layoutParams = alertDialog.getWindow().getAttributes();
            layoutParams.width = ViewGroup.LayoutParams.WRAP_CONTENT;
            alertDialog.show();
            alertDialog.getWindow().setLayout(430, WindowManager.LayoutParams.WRAP_CONTENT);
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    void showYoutube() {
        try {
            progressBar.setVisibility(View.VISIBLE);
            ModelApp.checkInternet(new ModelApp.ICheckConnectToHost() {
                @Override
                public void onConnected(boolean connected) {
                    progressBar.setVisibility(View.GONE);
                    if (connected) {
                        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(MainActivity.this, AlertDialog.THEME_HOLO_DARK);
                        LayoutInflater inflater = getLayoutInflater();
                        View dialogView = inflater.inflate(R.layout.alert_youtube, null);
                        dialogBuilder.setView(dialogView);
                        RelativeLayout youtubeView = dialogView.findViewById(R.id.youtube_view);
                        RelativeLayout youtubeKaraokeView = dialogView.findViewById(R.id.youtube_karaoke_view);
                        TextView youtubeTV = dialogView.findViewById(R.id.youtube_desc_tv);
                        TextView youtubeKaraokeTV = dialogView.findViewById(R.id.youtube_karaoke_desc_tv);

                        //harus dibuat seperti ini, karena tidak bisa pakai backgroundtintlist
                        youtubeView.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                            @Override
                            public void onFocusChange(View v, boolean hasFocus) {
                                if (hasFocus) {
                                    youtubeTV.setTextColor(getColor(R.color.button_text_selected_color));
                                    youtubeKaraokeTV.setTextColor(getColor(R.color.button_text_normal_color));
                                }
                            }
                        });


                        youtubeKaraokeView.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                            @Override
                            public void onFocusChange(View v, boolean hasFocus) {
                                if (hasFocus) {
                                    youtubeTV.setTextColor(getColor(R.color.button_text_normal_color));
                                    youtubeKaraokeTV.setTextColor(getColor(R.color.button_text_selected_color));
                                }

                            }
                        });

                        AlertDialog alertDialog = dialogBuilder.create();

                        youtubeView.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                alertDialog.dismiss();
                                YouTubeActivity.startActivity(MainActivity.this);
                            }
                        });

                        youtubeKaraokeView.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                alertDialog.dismiss();
                                YoutubeKaraokeActivity.startActivity(MainActivity.this, 0);
                            }
                        });

                        alertDialog.show();
                        WindowManager.LayoutParams layoutParams = alertDialog.getWindow().getAttributes();
                        layoutParams.width = ViewGroup.LayoutParams.WRAP_CONTENT;
                        alertDialog.getWindow().setLayout(450, 170);
                        alertDialog.show();
                        Util.hideNavigationBar(MainActivity.this);
                    } else {
                        showDialogToConnectWifi();

                    }
                }

                @Override
                public void onError(ANError e) {
                    progressBar.setVisibility(View.GONE);
                    showDialogToConnectWifi();
                }
            });

        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    /**
     * Versi show you tube tanpa check internet
     */
    void showYoutubeNow(){
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(MainActivity.this, AlertDialog.THEME_HOLO_DARK);
        LayoutInflater inflater = getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.alert_youtube, null);
        dialogBuilder.setView(dialogView);
        RelativeLayout youtubeView = dialogView.findViewById(R.id.youtube_view);
        RelativeLayout youtubeKaraokeView = dialogView.findViewById(R.id.youtube_karaoke_view);
        TextView youtubeTV = dialogView.findViewById(R.id.youtube_desc_tv);
        TextView youtubeKaraokeTV = dialogView.findViewById(R.id.youtube_karaoke_desc_tv);

        //harus dibuat seperti ini, karena tidak bisa pakai backgroundtintlist
        youtubeView.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    youtubeTV.setTextColor(getColor(R.color.button_text_selected_color));
                    youtubeKaraokeTV.setTextColor(getColor(R.color.button_text_normal_color));
                }
            }
        });


        youtubeKaraokeView.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    youtubeTV.setTextColor(getColor(R.color.button_text_normal_color));
                    youtubeKaraokeTV.setTextColor(getColor(R.color.button_text_selected_color));
                }

            }
        });

        AlertDialog alertDialog = dialogBuilder.create();

        youtubeView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                YouTubeActivity.startActivity(MainActivity.this);
            }
        });

        youtubeKaraokeView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                YoutubeKaraokeActivity.startActivity(MainActivity.this, 0);
            }
        });

        alertDialog.show();
        WindowManager.LayoutParams layoutParams = alertDialog.getWindow().getAttributes();
        layoutParams.width = ViewGroup.LayoutParams.WRAP_CONTENT;
        alertDialog.getWindow().setLayout(450, 170);
        alertDialog.show();
        Util.hideNavigationBar(MainActivity.this);
    }

    void showYoutubePlaylist() {
        try {
            progressBar.setVisibility(View.VISIBLE);
            ModelApp.checkInternet(new ModelApp.ICheckConnectToHost() {
                @Override
                public void onConnected(boolean connected) {
                    progressBar.setVisibility(View.GONE);
                    if (connected) {
                        YoutubePlaylistActivity.startActivity(MainActivity.this, 0);
                    } else {
                        showDialogToConnectWifi();

                    }
                }

                @Override
                public void onError(ANError e) {
                    progressBar.setVisibility(View.GONE);
                    showDialogToConnectWifi();
                }
            });
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    void showNews() {
        try {
            progressBar.setVisibility(View.VISIBLE);
            ModelApp.checkInternet(new ModelApp.ICheckConnectToHost() {
                @Override
                public void onConnected(boolean connected) {
                    progressBar.setVisibility(View.GONE);
                    if (connected) {
                        NewsActivity.startActivity(MainActivity.this);
                    } else {
                        showDialogToConnectWifi();
                    }
                }

                @Override
                public void onError(ANError e) {
                    progressBar.setVisibility(View.GONE);
                    showDialogToConnectWifi();
                }
            });
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    void showLiveChannel() {
        try {
            progressBar.setVisibility(View.VISIBLE);
            ModelApp.checkInternet(new ModelApp.ICheckConnectToHost() {
                @Override
                public void onConnected(boolean connected) {
                    progressBar.setVisibility(View.GONE);
                    if (connected) {
                        LiveChannelActivity.startActivity(MainActivity.this);
                    } else {
                        showDialogToConnectWifi();

                    }
                }

                @Override
                public void onError(ANError e) {
                    progressBar.setVisibility(View.GONE);
                    showDialogToConnectWifi();
                }
            });

        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    void showDownloadKaraoke() {
        try {
            progressBar.setVisibility(View.VISIBLE);
            ModelApp.checkInternet(new ModelApp.ICheckConnectToHost() {
                @Override
                public void onConnected(boolean connected) {
                    progressBar.setVisibility(View.GONE);
                    if (connected) {
                        checkDownloadSong();
                    } else {
                        showDialogToConnectWifi();

                    }
                }

                @Override
                public void onError(ANError e) {
                    progressBar.setVisibility(View.GONE);
                    showDialogToConnectWifi();
                }
            });
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    void showEShopping() {
        try {
            progressBar.setVisibility(View.VISIBLE);
            ModelApp.checkInternet(new ModelApp.ICheckConnectToHost() {
                @Override
                public void onConnected(boolean connected) {
                    progressBar.setVisibility(View.GONE);
                    if (connected) {
                        AccessoriesActivity.startActivity(MainActivity.this);
                    } else {
                        showDialogToConnectWifi();
                    }
                }

                @Override
                public void onError(ANError e) {
                    progressBar.setVisibility(View.GONE);
                    showDialogToConnectWifi();
                }
            });
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }


    /**
     * untuk check, apakah ada list dowload atau tidak
     */

    void checkDownloadSong() {
        ModelSong.getListServerSong(MainActivity.this, new ModelSong.IGetListServerSong() {
            @Override
            public void onGetListServerSong(List<TSongItemServer> tSongList) {
                try {
                    if (tSongList.size() == 0) {
                        UpdateSongActivity.startActivity(MainActivity.this);

                    } else {
                        showAlertDownloadingSong();
                    }
                } catch (Exception ex) {
                    Debug.e(TAG, ex);
                }
            }
        });
        /*PerformAsync2.run(performAsync ->
        {
            int totalSong = 0;
            try {
                DbAccess dbAccess = DbAccess.Instance.create(MainActivity.this);
                totalSong = dbAccess.daoAccessTSongItemServer().existSong();
                dbAccess.close();
            } catch (Exception ex) {
                Debug.e(TAG, ex);
            }

            return totalSong;
        }).setCallbackResult(result ->
        {
            int totalSong = (int) result;
            if (totalSong == 0) {
                UpdateSongActivity.startActivity(MainActivity.this);
            } else {
                showAlertDownloadingSong();
            }
        });*/

    }

    /**
     * show alert ini ketika sedang download lagu
     */

    void showAlertDownloadingSong() {
        try {
            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(MainActivity.this);
            LayoutInflater inflater = this.getLayoutInflater();
            View dialogView = inflater.inflate(R.layout.alert_stop_download, null);
            dialogBuilder.setView(dialogView);

            Button stopDownload = (Button) dialogView.findViewById(R.id.stop_download_button);
            Button continueDownload = (Button) dialogView.findViewById(R.id.continue_download_button);

            AlertDialog alertDialog = dialogBuilder.create();

            stopDownload.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    alertDialog.dismiss();
                    deleteAllSongLocal();
                    stopService(new Intent(getApplicationContext(), DownloadSongService.class));
                }
            });

            continueDownload.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    alertDialog.dismiss();
                }
            });

            alertDialog.show();
            WindowManager.LayoutParams layoutParams = alertDialog.getWindow().getAttributes();
            layoutParams.width = ViewGroup.LayoutParams.WRAP_CONTENT;
            alertDialog.show();
            alertDialog.getWindow().setLayout(430, WindowManager.LayoutParams.WRAP_CONTENT);
            Util.hideNavigationBar(MainActivity.this);
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }

    }


    void showRadio() {
        try {
            /*progressBar.setVisibility(View.VISIBLE);
            PerformAsync2.run(performAsync -> {
                int checkInternet = Util.isConnectedInternet();

                return checkInternet;
            }).setCallbackResult(result ->
            {
                progressBar.setVisibility(View.GONE);
                int checkInternet = (int) result;
                if (checkInternet != 0) {
                    RadioActivity.startActivity(MainActivity.this);
                } else {
                    showDialogToConnectWifi();
                }
            });*/

            progressBar.setVisibility(View.VISIBLE);
            ModelApp.checkInternet(new ModelApp.ICheckConnectToHost() {
                @Override
                public void onConnected(boolean connected) {
                    progressBar.setVisibility(View.GONE);
                    if (connected) {
                        RadioActivity.startActivity(MainActivity.this);
                    } else {
                        showDialogToConnectWifi();

                    }
                }

                @Override
                public void onError(ANError e) {
                    progressBar.setVisibility(View.GONE);
                    showDialogToConnectWifi();
                }
            });

        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    void showDialogToConnectWifi() {
        try {
            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(MainActivity.this);
            LayoutInflater inflater = this.getLayoutInflater();
            View dialogView = inflater.inflate(R.layout.alert_not_connected_internet, null);
            dialogBuilder.setView(dialogView);

            TextView textView = dialogView.findViewById(R.id.textView);
            Button connectButton = dialogView.findViewById(R.id.connect_button);
            Button cancelButton = dialogView.findViewById(R.id.cancel_button);
            textView.setText(getString(R.string.you_not_connect_internet));

            AlertDialog alertDialog = dialogBuilder.create();

            connectButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    alertDialog.dismiss();
                    Intent intent = new Intent(WifiManager.ACTION_PICK_WIFI_NETWORK);
                    startActivity(intent);
                }
            });

            cancelButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    alertDialog.dismiss();

                }
            });

            alertDialog.show();
            WindowManager.LayoutParams layoutParams = alertDialog.getWindow().getAttributes();
            layoutParams.width = ViewGroup.LayoutParams.WRAP_CONTENT;
            alertDialog.show();
            alertDialog.getWindow().setLayout(430, WindowManager.LayoutParams.WRAP_CONTENT);
            Util.hideNavigationBar(MainActivity.this);
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }


    public void onResume() {
        super.onResume();
        try {
            //todo check apakah activity pertama kali open, atau back dari activity lain
            if (firstCreateActivity == false) {
                firstCreateActivity = true;
            } else {
                bindFragment(true);
            }
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }

    }

    public void onBackPressed() {
        super.onBackPressed();

        Util.hideNavigationBar(MainActivity.this);
        //preventing default implementation previous to android.os.Build.VERSION_CODES.ECLAIR
        if (intentFromOTT == 0) {
            Log.d(TAG, "Back Pressed");
            if (listMoodView.getVisibility() == View.VISIBLE) {
                listMoodView.setVisibility(View.GONE);
                menuFragment.setFocusAndEnableView(true);
            }
        } else {
            //minimize app
            moveTaskToBack(true);
            finish();
        }

    }


    SettingActivity.ISettingActivityListener iSettingActivityListener = new SettingActivity.ISettingActivityListener() {
        @Override
        public void onRefreshTitleApp() {
            try {
                navigationFragment = navigationFragment.newInstance("");
                FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                ft.replace(R.id.topNavFrameLayout, navigationFragment).addToBackStack(null);
                ft.commitAllowingStateLoss();
            } catch (Exception ex) {
                Debug.e(TAG, ex);
            }
        }

        @Override
        public void onChangeBackground() {
            fetchListBackground();
        }

        @Override
        public void onChangeLanguage() {
            try {
                String languageId = Global.getLanguageId();
                Util.setLanguage(MainActivity.this, languageId);

                MainActivity.startActivity(MainActivity.this, 0);
                finish();
            } catch (Exception ex) {
                Debug.e(TAG, ex);
            }
        }
    };

    BottomNavigationFragment.IMoodFragmentListener bottomNavigationFragmentListener = new BottomNavigationFragment.IMoodFragmentListener() {
        @Override
        public void onMoodClick() {
            listMoodView.setVisibility(View.VISIBLE);
            menuFragment.setFocusAndEnableView(false);
//            menuFragment.setEnableView(false);
        }
    };

    ListMoodFragment.IListMoodListener listMoodListener = new ListMoodFragment.IListMoodListener() {
        @Override
        public void onClick(TSticker tSticker) {
            try {
                listMoodView.setVisibility(View.GONE);
                menuFragment.setFocusAndEnableView(true);
                bottomNavigationFragment.updateMood(tSticker);
                Global.setStickerId(tSticker.stickerId);
            } catch (Exception ex) {
                Debug.e(TAG, ex);
            }
        }
    };


    /**
     * muncul ketika selesai update
     */
    void showSuccessUpdate() {
        int prevVersion = Global.getPrevAppCode();
        if (prevVersion == 0) {
            try {
                prevVersion = getPackageManager().getPackageInfo(getPackageName(), 0).versionCode;
                Global.setPrevAppCode(prevVersion);
                return;
            } catch (PackageManager.NameNotFoundException e) {
                e.printStackTrace();
            }
        }
        if (prevVersion != 0) {
            try {
                int currentVersion = getPackageManager().getPackageInfo(getPackageName(), 0).versionCode;
                if (currentVersion > prevVersion) {
                    Global.setPrevAppCode(currentVersion);
                    AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(MainActivity.this);
                    LayoutInflater inflater = this.getLayoutInflater();
                    View dialogView = inflater.inflate(R.layout.alert_success_update, null);
                    dialogBuilder.setView(dialogView);

                    TextView textView = dialogView.findViewById(R.id.textView);
                    Button continueButton = dialogView.findViewById(R.id.continue_button);
                    String currentVersionName = getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
                    /** untuk different color di 1 textview */
                    String text1 = getString(R.string.updateApp_success);
                    String text2 = Util.getColoredSpanned(getString(R.string.updateApp_success2), "#ff3333");
                    String text3 = String.format(getString(R.string.updateApp_success3), currentVersionName);
                    textView.setText(Html.fromHtml(text1 + " " + text2 + "<br/>" + text3));

                    AlertDialog alertDialog = dialogBuilder.create();

                    continueButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            alertDialog.dismiss();

                        }
                    });

                    alertDialog.show();
                    WindowManager.LayoutParams layoutParams = alertDialog.getWindow().getAttributes();
                    layoutParams.width = ViewGroup.LayoutParams.WRAP_CONTENT;
                    alertDialog.show();
                    alertDialog.getWindow().setLayout(430, WindowManager.LayoutParams.WRAP_CONTENT);
                    Util.hideNavigationBar(MainActivity.this);
                }

            } catch (Exception ex) {
                Debug.e(TAG, ex);
            }
        }

    }


    /**
     * delete all json song download dari local
     */
    void deleteAllSongLocal() {
        ModelSong.deleteServerSong(MainActivity.this, new ModelSong.IDeleteServerSong() {
            @Override
            public void onDeleteSong(int result) {

            }
        });

    }


    /**
     * check apakah ada song yang belum selesai di download, jika ada resume download song
     */
    void fetchDownloadSong() {
        ModelSong.getListServerSong(MainActivity.this, new ModelSong.IGetListServerSong() {
            @Override
            public void onGetListServerSong(List<TSongItemServer> tSongList) {
                try {
                    /** di convert dulu ke arraylist */
                    ArrayList<TSongItemServer> tSongItemServerList = (ArrayList<TSongItemServer>) tSongList;
                    if (tSongList.size() > 0) {
                        Intent intent = new Intent(getApplicationContext(), DownloadSongService.class);
                        intent.putExtra(DownloadSongService.KEY_SONGITEMLIST, tSongItemServerList);
                        startService(intent);
                    }
                } catch (Exception ex) {
                    Debug.e(TAG, ex);
                }
            }
        });

    }


    /**
     * broadcast receiver untuk ketika download apk telah selesai
     */
    private final BroadcastReceiver brDownloadFinish = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            try {
                Log.d(TAG, "DOWNLOAD APK FINISH");
                stbControl.updateApp(MainActivity.this);
            } catch (Exception ex) {
                Debug.e(TAG, ex);
            }
        }
    };


    @Override
    public void setThemeUpdateDate(Date lastUpdateTheme) {

    }

    @Override
    public void setBackgroundTheme(TElement element) {

    }

    @Override
    public void setListColorTheme(int color) {

    }

    @Override
    public void setFooterColorTheme(int color) {

    }

    @Override
    public void setTextAndSelectionColorTheme(ColorStateList colorSelection, ColorStateList colorText, int colorTextSelected, int colorTextFocus, int colorTextNormal, int colorSelectionSelected, int colorSelectionFocus) {
        try {
            this.colorSelection = colorSelection;
            this.colorText = colorText;
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    @Override
    public void setHeaderColorTheme(int color) {

    }

    void mergeFile()
    {
        try
        {
            String video ="/sdcard/Download/videoplayback.mp4";
            String audio ="/sdcard/Download/videoplayback.m4a";
            String output ="/sdcard/Download/output.mp4";
            int rc = FFmpeg.execute("-i "+video+" -i "+audio+" -c:v copy -c:a aac -map 0:v:0 -map 1:a:0 "+output+"");

            if (rc == 0) {
                Log.i(Config.TAG, "Command execution completed successfully.");
            } else if (rc == 1) {
                Log.i(Config.TAG, "Command execution cancelled by user.");
            } else {
                Log.i(Config.TAG, String.format("Command execution failed with rc=%d and the output below.", rc));
                Config.printLastCommandOutput(Log.INFO);
            }
        }catch (Exception ex)
        {

        }
    }


   /* protected void onPause() {
        super.onPause();
        ActivityManager activityManager = (ActivityManager) getApplicationContext()
                .getSystemService(Context.ACTIVITY_SERVICE);
        activityManager.moveTaskToFront(getTaskId(), 0);
    }*/
}

package com.madeira.entertainz.karaoke.menu_youtube_karaoke_playlist;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.ScaleAnimation;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.androidnetworking.error.ANError;
import com.madeira.entertainz.controller.USBControl;
import com.madeira.entertainz.helper.RecyclerViewAdapter;
import com.madeira.entertainz.karaoke.DBLocal.JoinSongYoutubePlaylist;
import com.madeira.entertainz.karaoke.DBLocal.TSong;
import com.madeira.entertainz.karaoke.DBLocal.TSongYoutube;
import com.madeira.entertainz.karaoke.DBLocal.TSongYoutubePlaylist;
import com.madeira.entertainz.karaoke.DBLocal.TYoutubePlaylist;
import com.madeira.entertainz.karaoke.Debug;
import com.madeira.entertainz.karaoke.R;
import com.madeira.entertainz.karaoke.config.C;
import com.madeira.entertainz.karaoke.config.Global;
import com.madeira.entertainz.karaoke.menu_playlist.PlaylistTheme;
import com.madeira.entertainz.karaoke.menu_youtube_karaoke.YoutubeKaraokeActivity;
import com.madeira.entertainz.karaoke.model_online.ModelYoutube;
import com.madeira.entertainz.karaoke.model_room_db.ModelYoutubePlaylist;
import com.madeira.entertainz.karaoke.player.YoutubePlayerActivity;
import com.madeira.entertainz.library.Util;
import com.mukesh.OtpView;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import es.dmoral.toasty.Toasty;


public class YoutubePlaylistDetailFragment extends Fragment implements PlaylistTheme.IPlaylistThemeListener {
    String TAG = "YoutubePlaylistDetailFragment";

    public static final String KEY_PLAYLIST_ID = "KEY_PLAYLIST_ID";
    public static final String KEY_PLAYLIST_NAME = "KEY_PLAYLIST_NAME";

    private int playlistId;
    private String playlistName;
    static String passwordSetting = "";

    PlaylistTheme playlistTheme;
    List<TSongYoutubePlaylist> tSongYoutubePlaylistArrayList = new ArrayList<>();
//    List<JoinSongYoutubePlaylist> joinSongYoutubePlaylists = new ArrayList<>();
    RecyclerViewAdapter adapter;

    View root;
    LinearLayout rootLL;
    RecyclerView recyclerView;
    LinearLayout actionLL;
    Button renameButton;
    Button addSongButton;
    Button deleteButton;
    TextView playlistNameTV;
    EditText playlistNameET;
    USBControl usbControl = new USBControl();


    private IPlaylistDetailListener mListener;

    public YoutubePlaylistDetailFragment() {
        // Required empty public constructor
    }

    public interface IPlaylistDetailListener {
        void onListClick(ArrayList<TSong> tSongs, int position);

        void onDeleteOrRenamePlaylist();
    }

    public static YoutubePlaylistDetailFragment newInstance(String prmPlaylistId, String prmPlaylistName) {
        YoutubePlaylistDetailFragment fragment = new YoutubePlaylistDetailFragment();
        Bundle args = new Bundle();
        args.putString(KEY_PLAYLIST_ID, prmPlaylistId);
        args.putString(KEY_PLAYLIST_NAME, prmPlaylistName);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            playlistId = getArguments().getInt(KEY_PLAYLIST_ID);
            playlistName = getArguments().getString(KEY_PLAYLIST_NAME);
            fetchSongPlaylist();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        root = inflater.inflate(R.layout.fragment_youtube_playlist_detail, container, false);
        try {
            bind();
            setRecyclerView();

            playlistTheme = new PlaylistTheme(getActivity(), this);
            playlistTheme.setTheme();
            setOnActionObject();
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
        return root;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void setThemeUpdateDate(Date lastUpdateTheme) {

    }

    @Override
    public void setListColorTheme(int color) {
        try {
            rootLL.setBackgroundResource(R.drawable.tags_rounded_corners);
            GradientDrawable rootDrawable = (GradientDrawable) rootLL.getBackground();
            rootDrawable.setColor(color);

            actionLL.setBackgroundResource(R.drawable.tags_rounded_corners);
            GradientDrawable searchDrawable = (GradientDrawable) actionLL.getBackground();
            searchDrawable.setColor(color);
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    @Override
    public void setTextAndSelectionColorTheme(ColorStateList colorSelection, ColorStateList colorText, int colorTextSelected, int colorTextFocus, int colorTextNormal, int colorSelectionSelected, int colorSelectionFocus) {
        try {
            this.colorSelection = colorSelection;
            this.colorText = colorText;
            playlistNameTV.setTextColor(colorTextNormal);
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    public void setCallback(IPlaylistDetailListener listener) {
        this.mListener = listener;
    }

    void bind() {
        rootLL = (LinearLayout) root.findViewById(R.id.rootLL);
        recyclerView = (RecyclerView) root.findViewById(R.id.recyclerView);
        actionLL = (LinearLayout) root.findViewById(R.id.actionLL);
        renameButton = (Button) root.findViewById(R.id.renameButton);
        playlistNameTV = (TextView) root.findViewById(R.id.playlistNameTV);
        addSongButton = (Button) root.findViewById(R.id.addSongButton);
        deleteButton = (Button) root.findViewById(R.id.deletePlaylistButton);
        playlistNameET = (EditText) root.findViewById(R.id.playlistNameET);
        playlistNameTV.setText(playlistName);
        playlistNameET.setText(playlistName);
    }

    /**
     * nama function ini harusnya loadSongPlaylist bukan fetchSongPlaylist
     * fetch artinya loading data yg jauh (download)
     */
    void fetchSongPlaylist() {

        ModelYoutubePlaylist.getSongPlaylistByPlaylistId(getContext(), playlistId, new ModelYoutubePlaylist.IGetSongPlaylist() {
            @Override
            public void onGetListSongPlaylist(List<TSongYoutubePlaylist> tSongPlaylistList) {
                try {
                    tSongYoutubePlaylistArrayList = tSongPlaylistList;

                    adapter.notifyDataSetChanged();

//                    if (tSongPlaylistList.size() != 0)
//                        fetchYoutubeDetail();
                } catch (Exception ex) {
                    Debug.e(TAG, ex);
                }
            }
        });
        /*try {
            PerformAsync2.run(performAsync -> {
                int result = 0;
                try {
                    DbYoutube dbYoutube = DbYoutube.Instance.create(getActivity());
                    if (playlistId != 0)
                        tSongYoutubePlaylistArrayList = dbYoutube.daoAccessTSongYoutubePlaylist().getByPlaylistId(playlistId);
                    Log.d(TAG, String.valueOf(tSongYoutubePlaylistArrayList.size()));
                    dbYoutube.close();
                    result = 1;
                } catch (Exception ex) {

                }
                return result;
            }).setCallbackResult(result -> {
                int i = (int) result;
                if (i != 1) {
                    return;
                } else {
                    fetchYoutubeDetail();
                    Log.d(TAG, String.valueOf(tSongYoutubePlaylistArrayList.size()));
                }
            });

        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }*/
    }

    /**
     * harus di set satu persatu, karena data2 tersebut tidak di simpan di db local TSongYoutubePlaylist,
     */
//    void fetchYoutubeDetail() {
//        joinSongYoutubePlaylists.clear();
//        ModelYoutube.getYoutubeId(tSongYoutubePlaylistArrayList, new ModelYoutube.IGetYoutubeById() {
//            @Override
//            public void onGetYoutubeById(ModelYoutube.listYoutubeItem item) {
//                try {
//                    for (TSongYoutubePlaylist tSongPlaylist : tSongYoutubePlaylistArrayList) {
//                        for (TSongYoutube tSongYoutube : item.listYoutube) {
//                            if (tSongPlaylist.songId.equals(tSongYoutube.songId)) {
//                                JoinSongYoutubePlaylist joinSongYoutubePlaylist = new JoinSongYoutubePlaylist();
//                                joinSongYoutubePlaylist.playlistId = tSongPlaylist.playlistId;
//                                joinSongYoutubePlaylist.songId = tSongPlaylist.songId;
//                                joinSongYoutubePlaylist.artist = tSongYoutube.artist;
//                                joinSongYoutubePlaylist.duration = tSongYoutube.duration;
//                                joinSongYoutubePlaylist.songName = tSongYoutube.songName;
//                                joinSongYoutubePlaylist.songURL = tSongYoutube.songURL;
//                                joinSongYoutubePlaylist.thumbnail = tSongYoutube.thumbnail;
//                                joinSongYoutubePlaylist.lyric = tSongYoutube.lyric;
//                                joinSongYoutubePlaylist.vocalSound = tSongYoutube.vocalSound;
//                                joinSongYoutubePlaylist.songPlaylistId = tSongPlaylist.songPlaylistId;
//                                joinSongYoutubePlaylists.add(joinSongYoutubePlaylist);
//                            }
//                        }
//
//                    }
////                    setRecyclerView();
//                } catch (Exception ex) {
//                    Debug.e(TAG, ex);
//                }
//            }
//
//            @Override
//            public void onError(ANError e) {
//
//            }
//        });
//    }

    void setRecyclerView() {
        try {
            adapter = new RecyclerViewAdapter(new RecyclerViewAdapter.IRecyclerViewAdapter() {
                @Override
                public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
                    View view = LayoutInflater.from(getActivity()).inflate(R.layout.cell_playlist_detail, parent, false);
                    ItemViewHolder item = new ItemViewHolder(view);
                    return item;
                }

                @Override
                public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
                    ItemViewHolder item = (ItemViewHolder) holder;
                    item.bind(tSongYoutubePlaylistArrayList.get(position), position);
                }

                @Override
                public int getItemCount() {
                    return tSongYoutubePlaylistArrayList.size();
                }
            });
            adapter.notifyDataSetChanged();

            GridLayoutManager gridLayoutManager = new GridLayoutManager(getActivity(), 5);
            recyclerView.setLayoutManager(gridLayoutManager);
            recyclerView.setAdapter(adapter);
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    class ItemViewHolder extends RecyclerView.ViewHolder {
        private static final String TAG = "ItemViewHolder";

        View root;
        TextView tvTitle;
        TextView tvSinger;
        ImageView thumbnailIV;

        public ItemViewHolder(View itemView) {
            super(itemView);
            root = itemView;
            selectedView = root;
            tvTitle = itemView.findViewById(R.id.text_title);
            tvSinger = itemView.findViewById(R.id.text_singer);
            thumbnailIV = itemView.findViewById(R.id.thumbnailIV);
        }

        public void bind(TSongYoutubePlaylist song, int pos) {

            tvTitle.setText(song.songName);
            tvSinger.setText(song.artist);
            if (song.thumbnail != null) {
//                Uri uri = Uri.fromFile(new File(tSongPlaylist.thumbnail));
//                UtilRenderScript.fetchBitmap(getContext(), tSongPlaylist.thumbnail, 300, thumbnailIV);
                Picasso.with(getActivity()).load(song.thumbnail).into(thumbnailIV);

            }


            if (colorSelection != null) root.setBackgroundTintList(colorSelection);
//            untuk merubah warna text
            if (colorText != null) {
                tvTitle.setTextColor(colorText);
                tvSinger.setTextColor(colorText);
            }


            root.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showDetailSong(song, pos);
                    Util.hideNavigationBar(getActivity());
                }
            });

            root.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View v, boolean hasFocus) {
                    if (hasFocus) {
                        updateSelectedViewUi(v, true);
                    } else {
                        updateSelectedViewUi(null, false);

                    }
                }
            });
        }


    }

    ColorStateList colorSelection, colorText;

    //function untuk declare semua action dari object di activity
    void setOnActionObject() {

        try {
            renameButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    playlistNameET.setVisibility(View.VISIBLE);
                    playlistNameTV.setVisibility(View.GONE);
                    playlistNameET.requestFocus();
                }
            });
            addSongButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    YoutubeKaraokeActivity.startActivity(getActivity(), playlistId);
                    getActivity().finish();
                }
            });
            deleteButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
//                    showDialogDeletePlaylist();
                    showDialogConfirmDeletePlaylist();
                }
            });

            playlistNameET.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {
                    if (!playlistName.equals(s.toString())) {
                        renamePlaylist(s.toString());
                        usbControl.writeYoutubeSongsPlaylistToFile(getActivity());
                    }
                }
            });

            playlistNameET.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View v, boolean hasFocus) {
                    if (!hasFocus) {
//                    playlistNameET.setVisibility(View.GONE);
//                    playlistNameTV.setVisibility(View.VISIBLE);
                        mListener.onDeleteOrRenamePlaylist();
                    } else {
                        playlistNameET.setSelection(playlistNameET.getText().length());
                    }
                }
            });
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }

    }

    /**
     * untuk rename playlist
     */
    void renamePlaylist(String prmPlaylistName) {
        ModelYoutubePlaylist.renamePlaylist(getContext(), prmPlaylistName, playlistId, new ModelYoutubePlaylist.IRenamePlaylist() {
            @Override
            public void onRenamePlaylist(int result) {
                try {
                    int r = (int) result;
                    if (r == 1) {
                        playlistNameTV.setText(playlistNameET.getText());
                    } else if (r == 2) {
                        Toasty.error(getActivity(), getString(R.string.playlistAlreadyExists)).show();
                    }
                } catch (Exception ex) {
                    Debug.e(TAG, ex);
                }
            }
        });

    }

    void showDetailSong(TSongYoutubePlaylist song, int prmPosition) {
        try {
            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
            LayoutInflater inflater = this.getLayoutInflater();
            View dialogView = inflater.inflate(R.layout.alert_playlist_song, null);
            dialogBuilder.setView(dialogView);

            ImageView thumbnailIV = (ImageView) dialogView.findViewById(R.id.thumbnailIV);
            TextView titleTV = (TextView) dialogView.findViewById(R.id.titleTV);
            TextView singerTV = (TextView) dialogView.findViewById(R.id.singerTV);
            Button playSongButton = (Button) dialogView.findViewById(R.id.playSongButton);
            Button deleteSongButton = (Button) dialogView.findViewById(R.id.deleteSongButton);
            Button changeOrderButton = (Button) dialogView.findViewById(R.id.changeOrderButton);

            if (song.thumbnail != null) {
//            UtilRenderScript.fetchBitmap(getContext(), tSongPlaylist.thumbnail, 300, thumbnailIV);
                Picasso.with(getActivity()).load(song.thumbnail).into(thumbnailIV);
            }
            titleTV.setText(song.songName);
            singerTV.setText(song.artist);
            AlertDialog alertDialog = dialogBuilder.create();

            playSongButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
//                Toast.makeText(getContext(), "play song", Toast.LENGTH_LONG).show();
                    ArrayList<TSongYoutube> list = new ArrayList<>();
                    for (TSongYoutubePlaylist itemSongPlaylist : tSongYoutubePlaylistArrayList) {
                        TSongYoutube tSongYoutube = new TSongYoutube();
                        tSongYoutube.songId = String.valueOf(itemSongPlaylist.songId);
                        tSongYoutube.songName = itemSongPlaylist.songName;
                        tSongYoutube.artist = itemSongPlaylist.artist;
                        tSongYoutube.songURL = itemSongPlaylist.songURL;
                        tSongYoutube.vocalSound = itemSongPlaylist.vocalSound;
                        tSongYoutube.duration = itemSongPlaylist.duration;
                        tSongYoutube.thumbnail = itemSongPlaylist.thumbnail;
                        tSongYoutube.lyric = itemSongPlaylist.lyric;
                        list.add(tSongYoutube);
                    }

                    YoutubePlayerActivity.startActivity(getActivity(), list, prmPosition);
                    alertDialog.dismiss();

                }
            });

            deleteSongButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    deleteSong(song, prmPosition);
                    alertDialog.dismiss();
                }
            });
            changeOrderButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    //tampilkan activity utk change order
                    YoutubePlaylistChangeOrderActivity.startActivity(getActivity(), playlistId, song.songId);
                    alertDialog.dismiss();
                }
            });
            alertDialog.show();
            WindowManager.LayoutParams layoutParams = alertDialog.getWindow().getAttributes();
            layoutParams.width = ViewGroup.LayoutParams.WRAP_CONTENT;
            alertDialog.show();
            alertDialog.getWindow().setLayout(430, WindowManager.LayoutParams.WRAP_CONTENT);
            Util.hideNavigationBar(getActivity());
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    void deleteSong(TSongYoutubePlaylist song, int prmPosition) {
        try {
            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getContext());
            LayoutInflater inflater = this.getLayoutInflater();
            View dialogView = inflater.inflate(R.layout.alert_delete_confirmation, null);
            dialogBuilder.setView(dialogView);
            Button okButton = (Button) dialogView.findViewById(R.id.okButton);
            Button cancelButton = (Button) dialogView.findViewById(R.id.cancelButton);

            AlertDialog alertDialog = dialogBuilder.create();

            okButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ModelYoutubePlaylist.deleteSongPlaylist(getContext(), song.playlistId, song.songId, new ModelYoutubePlaylist.IDeleteSongPlaylist() {
                        @Override
                        public void onDeleteSongPlaylist(int result) {
                            int r = (int) result;
                            if (r == 1) {
                                fetchSongPlaylist();
                            } else {
                                Toasty.error(getActivity(), getString(R.string.failedDeleteSong)).show();
                            }
                            alertDialog.dismiss();
                        }
                    });
                }
            });

            cancelButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    alertDialog.dismiss();

                }
            });

            alertDialog.show();
            WindowManager.LayoutParams layoutParams = alertDialog.getWindow().getAttributes();
            layoutParams.width = ViewGroup.LayoutParams.WRAP_CONTENT;
            alertDialog.show();
            alertDialog.getWindow().setLayout(350, WindowManager.LayoutParams.WRAP_CONTENT);
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }


    void updateSelectedViewUi(View view, boolean hasFocus) {
        try {
            if (selectedView != null) {
                scaleView(selectedView, 1.1f, 1);
                ViewCompat.setTranslationZ(selectedView, 0);
            }

            if (hasFocus) {
                selectedView = view;
                ViewCompat.setTranslationZ(selectedView, 100);
//                selectedView.setBackgroundResource(R.drawable.box_white);
                scaleView(selectedView, 1, 1.1f);
            } else {
                //remove selected & focus dari grid
                selectedView = null;
            }
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    public void scaleView(View v, float startScale, float endScale) {
        try {
            Animation anim = new ScaleAnimation(
                    startScale, endScale, // Start and end values for the X axis scaling
                    startScale, endScale, // Start and end values for the Y axis scaling
                    Animation.RELATIVE_TO_SELF, 0.5f, // Pivot point of X scaling
                    Animation.RELATIVE_TO_SELF, 0.5f); // Pivot point of Y scaling
            anim.setFillAfter(true); // Needed to keep the result of the animation
            anim.setDuration(100);
            v.startAnimation(anim);
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    static View selectedView;

    void showDialogDeletePlaylist() {
        try {
            passwordSetting = Global.getDefaultPasswordSetting();
            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getContext());
            LayoutInflater inflater = this.getLayoutInflater();
            View dialogView = inflater.inflate(R.layout.alert_enter_setting, null);
            dialogBuilder.setView(dialogView);

//        EditText passwordET = dialogView.findViewById(R.id.passwordET);
            OtpView otpView = dialogView.findViewById(R.id.otpView);
            Button okButton = (Button) dialogView.findViewById(R.id.okButton);
            Button cancelButton = (Button) dialogView.findViewById(R.id.cancelButton);
            Button forgotPasswordButton = dialogView.findViewById(R.id.forgotPasswordButton);
            TextView infoTV = dialogView.findViewById(R.id.infoTV);

            AlertDialog alertDialog = dialogBuilder.create();

            okButton.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    if (passwordSetting.equals(otpView.getText().toString())) {
                        deletePlaylist();
                        alertDialog.dismiss();
                    } else {
                        otpView.setError(getString(R.string.yourPasswordWrong));
                        otpView.setText("");
                        otpView.requestFocus();
                    }
                }
            });

            cancelButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    alertDialog.dismiss();

                }
            });

            forgotPasswordButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Global.setDefaultPasswordSetting(C.default_password_setting);
                    passwordSetting = C.default_password_setting;
                    infoTV.setVisibility(View.VISIBLE);
                }
            });

            otpView.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {
                    int length = s.length();
                    if (length == 6) {
                        okButton.requestFocus();
                    }
                }
            });


            alertDialog.show();
            WindowManager.LayoutParams layoutParams = alertDialog.getWindow().getAttributes();
            layoutParams.width = ViewGroup.LayoutParams.WRAP_CONTENT;
            alertDialog.show();
            alertDialog.getWindow().setLayout(430, WindowManager.LayoutParams.WRAP_CONTENT);
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    void deletePlaylist() {
        ModelYoutubePlaylist.deletePlaylist(getActivity(), playlistId, new ModelYoutubePlaylist.IDeletePlaylist() {
            @Override
            public void onDeletePlaylist(int result) {
                int i = (int) result;
                if (i == 1) {
                    usbControl.writeYoutubeSongsPlaylistToFile(getContext());
                    adapter.notifyDataSetChanged();
                    mListener.onDeleteOrRenamePlaylist();
                }
            }
        });
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        Log.i(TAG, "onActivityResult: " + requestCode);

        //check apakah ada request code dari change order ??
        //kalo ada artinya user melakukan change order
        if (YoutubePlaylistChangeOrderActivity.checkRequestCode(requestCode)) {
//            reloadPlaylist();
            fetchSongPlaylist();
        }
    }

    /**
     * reload ini hanya di panggil saat user merubah order
     */
    void reloadPlaylist() {
        Log.i(TAG, "reloadPlaylist: ");

       /* try {
            PerformAsync2.run(performAsync -> {

                try {
                    DbYoutube dbAccess = DbYoutube.Instance.create(getContext());
                    List<TSongYoutubePlaylist> list = dbAccess.daoAccessTSongYoutubePlaylist().getByPlaylistId(playlistId);
                    dbAccess.close();
                    return list;

                } catch (Exception ex) {
                    ex.printStackTrace();
                }
                return null;

            }).setCallbackResult(result -> {

                try {
                    if (result == null) return;

                    //replace current list, dgn list yg baru di load dari room
                    tSongYoutubePlaylistArrayList = (List<TSongYoutubePlaylist>) result;
                    adapter.notifyDataSetChanged();

                } catch (Exception e) {
                    e.printStackTrace();
                }
            });

        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }*/
    }


    void showDialogConfirmDeletePlaylist() {
        try {
            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getContext());
            LayoutInflater inflater = this.getLayoutInflater();
            View dialogView = inflater.inflate(R.layout.alert_delete_playlist_confirmation, null);
            dialogBuilder.setView(dialogView);

            Button okButton = (Button) dialogView.findViewById(R.id.okButton);
            Button cancelButton = (Button) dialogView.findViewById(R.id.cancelButton);

            AlertDialog alertDialog = dialogBuilder.create();

            okButton.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    deletePlaylist();
                    alertDialog.dismiss();
                }
            });

            cancelButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    alertDialog.dismiss();

                }
            });

            alertDialog.show();
            WindowManager.LayoutParams layoutParams = alertDialog.getWindow().getAttributes();
            layoutParams.width = ViewGroup.LayoutParams.WRAP_CONTENT;
            alertDialog.show();
            alertDialog.getWindow().setLayout(430, WindowManager.LayoutParams.WRAP_CONTENT);
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

}

package com.madeira.entertainz.karaoke.menu_youtube_karaoke_playlist;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.madeira.entertainz.helper.RecyclerViewAdapter;
import com.madeira.entertainz.karaoke.DBLocal.DbYoutube;
import com.madeira.entertainz.karaoke.DBLocal.TYoutubePlaylist;
import com.madeira.entertainz.karaoke.Debug;
import com.madeira.entertainz.karaoke.R;
import com.madeira.entertainz.karaoke.menu_karaoke.KaraokeActivity;
import com.madeira.entertainz.karaoke.menu_playlist.PlaylistTheme;
import com.madeira.entertainz.karaoke.model_room_db.ModelYoutubePlaylist;
import com.madeira.entertainz.library.PerformAsync2;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


public class YoutubePlaylistListFragment extends Fragment implements PlaylistTheme.IPlaylistThemeListener {
    String TAG = "YoutubePlaylistListFragment";

    View root;
    LinearLayout playlistLL;
    TextView playlistTV;
    LinearLayout recyclerLL;
    RecyclerView recyclerView;
    RecyclerViewAdapter adapter;
    private IPlaylistListener mListener;

    int playlistId = 0;

    PlaylistTheme playlistTheme;
    List<TYoutubePlaylist> tPlaylists = new ArrayList<>();

    public YoutubePlaylistListFragment() {
        // Required empty public constructor
    }

    public interface IPlaylistListener {
        // TODO: Update argument type and name
        void onCategoryClick(int playlistId, String playlistName);
    }


    public static YoutubePlaylistListFragment newInstance() {
        YoutubePlaylistListFragment fragment = new YoutubePlaylistListFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            playlistId = getArguments().getInt(KaraokeActivity.KEY_PLAYLIST_ID);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        root = inflater.inflate(R.layout.fragment_youtube_playlist_list, container, false);
        try {
            bind();

            setRecyclerView();

            playlistTheme = new PlaylistTheme(getActivity(), this);
            playlistTheme.setTheme();
            fetchPlaylist();
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
        return root;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    @Override
    public void setThemeUpdateDate(Date lastUpdateTheme) {

    }

    @Override
    public void setListColorTheme(int color) {
        try {
            playlistLL.setBackgroundResource(R.drawable.tags_rounded_corners);
            recyclerLL.setBackgroundResource(R.drawable.tags_rounded_corners);

            GradientDrawable categoryDrawable = (GradientDrawable) playlistLL.getBackground();
            categoryDrawable.setColor(color);

            GradientDrawable recycleDrawable = (GradientDrawable) recyclerLL.getBackground();
            recycleDrawable.setColor(color);
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    @Override
    public void setTextAndSelectionColorTheme(ColorStateList colorSelection, ColorStateList colorText, int colorTextSelected, int colorTextFocus, int colorTextNormal, int colorSelectionSelected, int colorSelectionFocus) {
        try {
            playlistTV.setTextColor(colorTextNormal);
            this.colorSelection = colorSelection;
            this.colorText = colorText;
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    public void setCallback(IPlaylistListener listener) {
        this.mListener = listener;
    }

    void bind() {
        playlistLL = (LinearLayout) root.findViewById(R.id.categoryLL);
        playlistTV = (TextView) root.findViewById(R.id.categoryTV);
        recyclerLL = (LinearLayout) root.findViewById(R.id.recyclerLL);
        recyclerView = (RecyclerView) root.findViewById(R.id.recyclerView);
    }

    void fetchPlaylist() {
        ModelYoutubePlaylist.getPlaylist(getContext(), new ModelYoutubePlaylist.IGetPlaylist() {
            @Override
            public void onGetListPlaylist(List<TYoutubePlaylist> tYoutubePlaylistList) {
                tPlaylists = tYoutubePlaylistList;

                adapter.notifyDataSetChanged();

                Log.d(TAG, String.valueOf(tPlaylists.size()));
            }
        });
      /*  try {
            PerformAsync2.run(performAsync -> {
                int result = 0;
                try {
                    DbYoutube dbYoutube = DbYoutube.Instance.create(getActivity());
                    tPlaylists = dbYoutube.daoAccessTYoutubePlaylist().getAll();
                    Log.d(TAG, String.valueOf(tPlaylists.size()));
                    dbYoutube.close();
                    result = 1;
                } catch (Exception ex) {

                }
                return result;
            }).setCallbackResult(result -> {
                int i = (int) result;
                if (i != 1) {
                    return;
                } else {
                    if (tPlaylists.size() == 0) {

                    } else {
                        setRecyclerView();
                    }
                    Log.d(TAG, String.valueOf(tPlaylists.size()));
                }
            });

        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }*/
    }

    void setRecyclerView() {
        try {
            adapter = new RecyclerViewAdapter(new RecyclerViewAdapter.IRecyclerViewAdapter() {
                @Override
                public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
                    View view = LayoutInflater.from(getActivity()).inflate(R.layout.cell_karaoke_category, parent, false);
                    ItemViewHolder item = new ItemViewHolder(view);
                    return item;
                }

                @Override
                public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
                    ItemViewHolder item = (ItemViewHolder) holder;
                    item.bind(tPlaylists.get(position), position);
                }

                @Override
                public int getItemCount() {
                    return tPlaylists.size();
                }
            });

            adapter.notifyDataSetChanged();
            recyclerView.setAdapter(adapter);
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    //view ini khusus di pakai utk ItemViewHolder, utk selectedItemView
    View selectedView;
    int selectedItem;

    class ItemViewHolder extends RecyclerView.ViewHolder {
        private static final String TAG = "ItemViewHolder";

        View root;
        TextView tvTitle;

        public ItemViewHolder(View itemView) {
            super(itemView);
            root = itemView;

            tvTitle = itemView.findViewById(R.id.text_title);

//            selectionColor = itemView.findViewById(R.id.selectioncolor);
        }

        public void bind(TYoutubePlaylist tPlaylist, int pos) {
            try {
                tvTitle.setText(tPlaylist.playlistName);

                if (playlistId == 0) {
                    if (selectedItem == pos) {
                        selectView(root);
                        mListener.onCategoryClick(tPlaylist.playlistId, tPlaylist.playlistName);

                    }
                    if (pos == 0) {
                        root.setSelected(true);
                    }
                } else {
                    if (tPlaylist.playlistId == playlistId) {
                        selectView(root);
                        mListener.onCategoryClick(tPlaylist.playlistId, tPlaylist.playlistName);
                        selectedItem = pos;
                        root.requestFocus();
                    }

                }

//            untuk merubah warna selection
                if (colorSelection != null) root.setBackgroundTintList(colorSelection);

//            untuk merubah warna text
                if (colorText != null) tvTitle.setTextColor(colorText);


                root.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        selectView(v);
                        selectedItem = pos;
                        mListener.onCategoryClick(tPlaylist.playlistId, tPlaylist.playlistName);

                    }
                });
            } catch (Exception ex) {
                Debug.e(TAG, ex);
            }
        }

        void selectView(View v) {
            //clear selection
            if (selectedView != null) {
                selectedView.setSelected(false);
            }

            //make selection
            selectedView = v;
            selectedView.setSelected(true);
        }
    }

    ColorStateList colorSelection, colorText;
}

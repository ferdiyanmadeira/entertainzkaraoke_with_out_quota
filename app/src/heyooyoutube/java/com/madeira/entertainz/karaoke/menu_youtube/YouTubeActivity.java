package com.madeira.entertainz.karaoke.menu_youtube;

import android.app.Activity;
import android.app.ActivityOptions;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.madeira.entertainz.karaoke.DBLocal.DbRecentSearchSong;
import com.madeira.entertainz.karaoke.DBLocal.TRecentSearchSong;
import com.madeira.entertainz.karaoke.DBLocal.TSticker;
import com.madeira.entertainz.karaoke.Debug;
import com.madeira.entertainz.karaoke.R;
import com.madeira.entertainz.karaoke.config.C;
import com.madeira.entertainz.karaoke.config.Global;
import com.madeira.entertainz.karaoke.menu_main.BottomNavigationFragment;
import com.madeira.entertainz.karaoke.menu_main.ListMoodFragment;
import com.madeira.entertainz.karaoke.model.ModelYoutube;
import com.madeira.entertainz.karaoke.model_room_db.ModelRecentSearch;
import com.madeira.entertainz.library.EzRecyclerViewAdapter;
import com.madeira.entertainz.library.PerformAsync2;
import com.madeira.entertainz.library.Util;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class YouTubeActivity extends AppCompatActivity {

    private static final String TAG = "YouTubeActivity";

    private static final int ITEM_PER_ROW = 4;
    private static final int LOADING_TRESHOLH = 8;
    private static final String REGION_CODE = "ID"; //INDONESIA

    private static final String FETCH_BY_SEARCH = "SEARCH";
    private static final String FETCH_BY_CATEGORY = "CATEGORY";
    BottomNavigationFragment bottomNavigationFragment = new BottomNavigationFragment();
    ListMoodFragment listMoodFragment = new ListMoodFragment();
    View listMoodView;

//    List<String> listTitle = new ArrayList<>();
//    List<String> listDescription = new ArrayList<>();
//    List<String> listThumbnail = new ArrayList<>();
//    List<String> listid = new ArrayList<>();

//        YouTubeTheme youTubeTheme = new YouTubeTheme(this, this);

    RecyclerView recyclerView;
    EditText editText;

    EzRecyclerViewAdapter adapter;
    GridLayoutManager manager;
    String nextPageToken, prevPageToken;
    String categoryId = "0"; //0=no category

    String query = "", fetchBy = "";
    Boolean focus = true;
    Date lastUpdateTheme = new Date();

    List<ModelYoutube.ItemVideo> mList = new ArrayList<>();

    CategoryController categoryController;
    RelativeLayout recentView;
    ImageView iconImageView;
    TextView recentTV;
    ImageView searchHistoryButton;

    public static void startActivity(Activity activity) {
        try {
            Intent intent = new Intent(activity, YouTubeActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

            activity.startActivity(intent,
                    ActivityOptions.makeSceneTransitionAnimation(activity).toBundle());
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        try {
            setContentView(R.layout.activity_you_tube);
            bind();
            bindFragment();

            recyclerView = findViewById(R.id.recycler_view);
//        prev = findViewById(R.id.prev);
//        next = findViewById(R.id.next);
            editText = findViewById(R.id.edit_text_search);
//        buttonsearch = findViewById(R.id.button_search);

            editText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                @Override
                public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                    if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                        query = editText.getText().toString();

//                    next.setVisibility(View.GONE);
//                    prev.setVisibility(View.GONE);

//                    clearRecyclerView();
                        searchVideo(query, "");

//                    buttonsearch.setFocusable(false);
                        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
                        return true;
                    }
                    return false;
                }
            });

//            youTubeTheme.setTheme();
            setupRecyclerView();
            buttonControl();
//            setupObserver();

            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
            hideNavigationBar();
            installNavigationListener();

            setupCategory();

            //first time load
            fetchVideos("", categoryId);
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }


    void buttonControl() {

        editText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {

                try {
                    Log.i(TAG, "onEditorAction: " + keyEvent.toString());

                    if (keyEvent.getAction() == KeyEvent.ACTION_DOWN) {
                        if (keyEvent.getKeyCode() == KeyEvent.KEYCODE_ENTER ||
                                keyEvent.getKeyCode() == KeyEvent.KEYCODE_DPAD_CENTER) {
                            String keyword = editText.getText().toString();
                            if (!keyword.equals(""))
                                addRecentSearch(keyword);
                            startSearch();

                            return true;
                        }
                    }

                } catch (Exception ex) {
                    Debug.e(TAG, ex);
                }
                return false;
            }
        });

        editText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                try {
                    if (!hasFocus) {
//                        Util.closeKeyboard(YouTubeActivity.this);
                    } else {
                        Util.showKeyboard(YouTubeActivity.this);
                    }
                } catch (Exception ex) {
                    Debug.e(TAG, ex);
                }

            }
        });

        recentView.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                try {
                    if (hasFocus) {
                        iconImageView.setBackgroundResource(R.drawable.ic_recent_black);
                        recentTV.setTextColor(getColor(R.color.black));
                    } else {
                        iconImageView.setBackgroundResource(R.drawable.ic_recent_white);
                        recentTV.setTextColor(getColor(R.color.white));
                    }
                } catch (Exception ex) {
                    Debug.e(TAG, ex);
                }
            }
        });

        recentView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    showRecentSearch();
                } catch (Exception ex) {
                    Debug.e(TAG, ex);
                }
            }
        });

        searchHistoryButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    showRecentSearch();
                } catch (Exception ex) {
                    Debug.e(TAG, ex);
                }
            }
        });

    }

    private void setupCategory() {
        try {
            categoryController = new CategoryController();
            categoryController.attach(this, REGION_CODE, findViewById(R.id.recycler_view_category));

            categoryController.addEventListener(new CategoryController.EventListener() {
                @Override
                public void onClick(String categoryId) {

                    try {
                        //tdk perlu update apabila category sdh sama
                        if (categoryId == YouTubeActivity.this.categoryId) return;

                        YouTubeActivity.this.categoryId = categoryId;
                        fetchVideos("", categoryId);
                    } catch (Exception ex) {
                        Debug.e(TAG, ex);
                    }
                }
            });
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    boolean isFetchVideoActive;

    void fetchVideos(String pageToken, String categoryId) {
//        next.setVisibility(View.GONE);
//        prev.setVisibility(View.GONE);

        try {
            if (isFetchVideoActive) return;
            if (pageToken == null) return;

            isFetchVideoActive = true;

            fetchBy = FETCH_BY_CATEGORY;

            //apabila pagetoken==empty, artinya search dari awal
            if (pageToken.isEmpty()) {
                mList.clear();
                if (adapter != null) adapter.notifyDataSetChanged();
            }

            if (Util.isNetworkAvailable(this)) {
                ModelYoutube.getVideos(pageToken, categoryId, REGION_CODE, new ModelYoutube.OnGetVideos() {
                    @Override
                    public void onGetVideos(List<ModelYoutube.ItemVideo> list, String token) {

                        try {
                            isFetchVideoActive = false;

                            Log.i(TAG, "onGetVideos: " + list.size() + " token=" + token);

                            if (list == null) return;

                            nextPageToken = token;

                            mList.addAll(list);

                            adapter.notifyDataSetChanged();
                        } catch (Exception ex) {
                            Debug.e(TAG, ex);
                        }
                    }

                    @Override
                    public void onError(Exception e) {
                        isFetchVideoActive = false;
                    }
                });
            }
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    private void startSearch() {
        try {
            query = editText.getText().toString();
            searchVideo(query, "");
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    boolean isSearchActive;

    void searchVideo(String q, String pageToken) {

        try {
//        next.setVisibility(View.GONE);
//        prev.setVisibility(View.GONE);

            //exit apabila search lagi jalan
            if (isSearchActive) return;

            //exit apabila q kosong
            if (q == null) return;
            if (q.isEmpty()) return;

            //exit apabila token null
            if (pageToken == null) return;

            query = q;
            fetchBy = FETCH_BY_SEARCH;

            //hapus semua content
            if (pageToken.isEmpty()) {
                mList.clear();
                if (adapter != null) adapter.notifyDataSetChanged();
            }

            if (Util.isNetworkAvailable(this)) {
                ModelYoutube.getSearch(query, pageToken, new ModelYoutube.OnGetSearch() {
                    @Override
                    public void onGetSearch(List<ModelYoutube.ItemVideo> list, String token) {

//                    buttonsearch.setFocusable(true);

                        isSearchActive = false;

                        if (list == null) return;

                        nextPageToken = token;

                        mList.addAll(list);

                        adapter.notifyDataSetChanged();
                    }

                    @Override
                    public void onError(Exception e) {
                        isSearchActive = false;
//                    buttonsearch.setFocusable(true);
                    }
                });
            } else {
                Toast.makeText(YouTubeActivity.this, "There is Problem in Connection", Toast.LENGTH_SHORT).show();
            }
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

//    void clearRecyclerView(){
//        listTitle.clear();
//        listDescription.clear();
//        listThumbnail.clear();
//        listid.clear();
//
//        adapter.notifyDataSetChanged();
//
//        focus=true;
//    }

    void setupRecyclerView() {
        try {
            //set kolom
            manager = (GridLayoutManager) recyclerView.getLayoutManager();
            manager.setSpanCount(ITEM_PER_ROW);

            adapter = new EzRecyclerViewAdapter(new EzRecyclerViewAdapter.IEzRecyclerViewAdapter() {
                @Override
                public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
                    ItemViewHolder item = null;
                    try {
                        View view = LayoutInflater.from(YouTubeActivity.this).inflate(R.layout.cell_youtube_list, parent, false);
                        item = new ItemViewHolder(view);
                    } catch (Exception e) {
                        Debug.e(TAG, e);
                    }
                    return item;
                }

                @Override
                public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
                    try {
//                        String title = "Title " + (position+1);//array.get(position);
                        ItemViewHolder item = (ItemViewHolder) holder;
                        item.bind(mList.get(position), position);
//                        item.bind(listTitle, listDescription, listThumbnail, position);
                    } catch (Exception e) {
                        Debug.e(TAG, e);
                    }
                }

                @Override
                public int getItemCount() {
                    return mList.size();
                }

                @Override
                public long getItemId(int pos) {
                    return pos;
                }
            });

            adapter.setHasStableIds(true);

            recyclerView.setAdapter(adapter);

            recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);

                    try {
                        //user scroll up, exit
                        if (dy < 0) return;

                        int lastItemVisible = manager.findLastVisibleItemPosition();

                        fetchMore(lastItemVisible);


//                        //tdk perlu load more kalo sedang load data
//                        if (isLoading) return;
//
//                        //check apakah harus load more ??
////                    int totalItem = linearLayoutManager.getItemCount();
//                        int lastItemVisible = manager.findLastVisibleItemPosition();
//
//                        int offset = mList.size();
//
////                    Log.i(TAG, "offset=" + offset + " lastItemVisibie=" + lastItemVisible);
//
//                        if (offset <= lastItemVisible + LOADING_TRESHOLH) {
//                            fetchVideos(nextPageToken, categoryId); //ambil data sebanyak jumlah array
//                            //                        loadMore(offset);
//                            //                        Log.i(TAG, "Fetch more " + lastItemVisible + " / " + offset);
//                        } else {
//                            //                        isBottom = false;
//                        }
                    } catch (Exception e) {
//                        Debug.e(TAG, e);
                    }

                }
            });

        } catch (Exception e) {
            Debug.e(TAG, e);
        }
    }

    private void fetchMore(int lastItemVisible) {

        try {

            int offset = mList.size();

            if (fetchBy.equals(FETCH_BY_CATEGORY)) {

                if (isFetchVideoActive) return;

                if (offset <= lastItemVisible + LOADING_TRESHOLH) {
                    fetchVideos(nextPageToken, categoryId); //ambil data sebanyak jumlah array
                    //                        loadMore(offset);
                    //                        Log.i(TAG, "Fetch more " + lastItemVisible + " / " + offset);
                } else {
                    //                        isBottom = false;
                }

                return;
            }

            if (fetchBy.equals(FETCH_BY_SEARCH)) {
                if (isSearchActive) return;

                if (offset <= lastItemVisible + LOADING_TRESHOLH) {
                    searchVideo(query, nextPageToken); //ambil data sebanyak jumlah array
                    //                        loadMore(offset);
                    //                        Log.i(TAG, "Fetch more " + lastItemVisible + " / " + offset);
                } else {
                    //                        isBottom = false;
                }
            }
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    class ItemViewHolder extends RecyclerView.ViewHolder {
        private static final String TAG = "ItemViewHolder";

        //            View layoutContainer;
        TextView tvTitle;
        ImageView imagePoster;

        ItemViewHolder(View itemView) {
            super(itemView);

            try {
                tvTitle = itemView.findViewById(R.id.text_title);
//                    layoutContainer = itemView.findViewById(R.id.layout_container);
                imagePoster = itemView.findViewById(R.id.image_poster);

            } catch (Exception e) {
                Debug.e(TAG, e);
            }
        }

        //        public void bind(List<String> listTitle, List<String> listDescription, List<String> listThumbnail, int pos){
        public void bind(ModelYoutube.ItemVideo item, int pos) {
            try {

                String title = String.valueOf(item.title);
                tvTitle.setText(Html.fromHtml(Html.fromHtml(title).toString()));

                //focus di item pertama, dan agar tidak dipanggil kembali
//                if (pos==0 && focus){
//                    itemView.requestFocus();
//                    focus = false;
//
//                    UtilAnim.scaleView(itemView, 1.0f, 1.15f);
//                    itemView.setTranslationZ(10);
//                }

                Picasso.with(YouTubeActivity.this).load(item.urlThumb).into(imagePoster);

//                itemView.setOnFocusChangeListener(new View.OnFocusChangeListener() {
//                    @Override
//                    public void onFocusChange(View v, boolean hasFocus) {
//                        try {
//                            if (hasFocus){
//                                UtilAnim.scaleView(itemView, 1.0f, 1.17f);
//                                itemView.setTranslationZ(10);
//                            } else {
//                                UtilAnim.scaleView(itemView, 1.17f, 1.0f);
//                                itemView.setTranslationZ(1);
//                            }
//
//                        } catch (Exception e){
//                            Debug.e(TAG, e);
//                        }
//                    }
//                });

                itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        YouTubeVideoActivity.startActivity(YouTubeActivity.this, mList.get(pos).id);
                    }
                });

            } catch (Exception e) {
                Debug.e(TAG, e);
            }
        }
    }

    void hideNavigationBar() {
        try {
            View decorView = getWindow().getDecorView();
// Hide both the navigation bar and the status bar.
// SYSTEM_UI_FLAG_FULLSCREEN is only available on Android 4.1 and higher, but as
// a general r ule, you should design your app to hide the status bar whenever you
// hide the navigation bar.
//        int uiOptions = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION | View.SYSTEM_UI_FLAG_FULLSCREEN;
//        decorView.setSystemUiVisibility(uiOptions);

            decorView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                    | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
        } catch (Exception e) {
            Debug.e(TAG, e);
        }
    }

    void installNavigationListener() {
        try {
            View decorView = getWindow().getDecorView();

            //this add event, will hide again if shown
            decorView.setOnSystemUiVisibilityChangeListener(new View.OnSystemUiVisibilityChangeListener() {
                @Override
                public void onSystemUiVisibilityChange(int visibility) {
                    // Note that system bars will only be "visible" if none of the
                    // LOW_PROFILE, HIDE_NAVIGATION, or FULLSCREEN flags are set.
                    if ((visibility & View.SYSTEM_UI_FLAG_FULLSCREEN) == 0) {
                        hideNavigationBar();
                    } else {
                        // adjustments to your UI, such as hiding the action bar or
                        // other navigational controls.
                    }
                }
            });
        } catch (Exception e) {
            Debug.e(TAG, e);
        }
    }

//        void setupObserver(){
//            try {
//                Notification.ldTheme.observe(this, new Observer<Date>() {
//                    @Override
//                    public void onChanged(@Nullable Date date) {
//                        try {
//                            //check apakah perlu update theme?
//                            Log.i("date",""+date);
//                            if (date.compareTo(lastUpdateTheme)>0){
//                                youTubeTheme.setTheme();
//                            }
//                        } catch (Exception e) {
//                            Debug.e(TAG, e);
//                        }
//                    }
//                });
//
//                Notification.ldIsEmergencyWarning.observe(this, new Observer<Boolean>() {
//                    @Override
//                    public void onChanged(Boolean enable) {
//                        if (enable){
//                            Log.i(TAG, "ldIsEmergencyWarning=" + enable);
//                            EmergencyWarningActivity.startActivity(getBaseContext());
//                            finish();
//                        }
//                    }
//                });
//
//
//            } catch (Exception e) {
//                Debug.e(TAG, e);
//            }
//        }

//        @Override
//        public void setThemeUpdateDate(Date lastUpdateTheme) {
//            try {
//                this.lastUpdateTheme = lastUpdateTheme;
//            } catch (Exception e) {
//                Debug.e(TAG, e);
//            }
//        }

//        @Override
//        public void setBackgroundTheme(TElement element) {
//
//        }

//        @Override
//        public void setListColorTheme(int color) {
//        }

//        @Override
//        public void setTextandSelectionCollorTheme(ColorStateList colorSelection, ColorStateList colorText, int colorTextSelected, int colorTextFocus, int colorTextNormal, int colorSelectionSelected, int colorSelectionFocus) {
//            try {
//                if (colorSelection!=null){
////                buttonsearch.setBackgroundTintList(colorSelection);
////                prev.setBackgroundTintList(colorSelection);
////                next.setBackgroundTintList(colorSelection);
//                }
//            } catch (Exception e) {
//                Debug.e(TAG, e);
//            }
//        }

//    @Override
//    public boolean onKeyDown(int keyCode, KeyEvent event) {
//
//        //exit apabila handle focus return true
//        if (handleFocus(keyCode, event)) return true;
//
//        return super.onKeyDown(keyCode, event);
//    }

    /**
     * Khusus utk handle focus dgn DPAD
     *
     * @param keyCode
     * @param event
     * @return
     */
    private boolean handleFocus(int keyCode, KeyEvent event) {

        View current = getCurrentFocus();

        if (current == null) return false;

        switch (keyCode) {
            case KeyEvent.KEYCODE_DPAD_DOWN:
                return onDpadDown(current, event);

            case KeyEvent.KEYCODE_DPAD_UP:
                return onDpadUp(current, event);
        }

        return false;
    }

    /**
     * DPAD DOWN di tekan
     *
     * @param event
     * @param view
     * @return
     */
    private boolean onDpadDown(View view, KeyEvent event) {

        try {
            //apabila posisi focus ada pada text search, maka focus pada categoru
            if (view.getId() == R.id.edit_text_search) {

                categoryController.setFocus();
                return true;
            }
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
        return false;
    }

    private boolean onDpadUp(View view, KeyEvent event) {

        try {
            if (recyclerView == null) return false;

            View child = recyclerView.getFocusedChild();

            if (child == null) return false;

            int idx = recyclerView.getChildAdapterPosition(child);

            //apabila item yg focus ada pada posisi teratas, maka category akan di focus kan
            if (idx < ITEM_PER_ROW) return categoryController.setFocus();
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }

        return false;
    }

    void bind() {
        try {
            listMoodView = findViewById(R.id.listMoodFrameLayout);
            recentView = findViewById(R.id.recent_view);
            iconImageView = findViewById(R.id.icon_image_view);
            recentTV = findViewById(R.id.recent_tv);
            searchHistoryButton = findViewById(R.id.search_history_button);

        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    void bindFragment() {

        try {
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.bottomFrameLayout, bottomNavigationFragment);
            ft.replace(R.id.listMoodFrameLayout, listMoodFragment);
            Bundle bundle = new Bundle();
            ft.commit();
            bottomNavigationFragment.setCallback(bottomNavigationFragmentListener);
            listMoodFragment.setCallBack(listMoodListener);
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }

    }


    /**
     * listener ketika mood di click
     */
    BottomNavigationFragment.IMoodFragmentListener bottomNavigationFragmentListener = new BottomNavigationFragment.IMoodFragmentListener() {
        @Override
        public void onMoodClick() {
            listMoodView.setVisibility(View.VISIBLE);
        }
    };

    /**
     * listener ketika mood dipilih
     */
    ListMoodFragment.IListMoodListener listMoodListener = new ListMoodFragment.IListMoodListener() {
        @Override
        public void onClick(TSticker tSticker) {
            try {
                listMoodView.setVisibility(View.GONE);
                bottomNavigationFragment.updateMood(tSticker);
                Global.setStickerId(tSticker.stickerId);
            } catch (Exception ex) {
                Debug.e(TAG, ex);
            }
        }
    };


    /**
     * show recent lagu yang pernah dicari
     */
    void showRecentSearch() {
        try {
            RecentSearchYoutubeActivity.startActivity(YouTubeActivity.this);
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }


    /**
     * untuk tambahkan recent search ke local db
     */
    void addRecentSearch(String songName) {

        ModelRecentSearch.addRecentSearch(YouTubeActivity.this, songName, C.SEARCH_SONG_YOUTUBE, new ModelRecentSearch.IAddRecentSearch() {
            @Override
            public void onAddRecentSearch(int result) {

            }
        });



    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
//        super.onActivityResult(requestCode, resultCode, data);
        try {
            String keyword = data.getStringExtra(RecentSearchYoutubeActivity.INTENT_RESULT);
            editText.setText(keyword);
            query = keyword;
            searchVideo(query, "");
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    @Override
    public void onDestroy()
    {
        try
        {
            super.onDestroy();
            Util.freeMemory();
        }
        catch (Exception ex)
        {
            Debug.e(TAG, ex);
        }
    }
}

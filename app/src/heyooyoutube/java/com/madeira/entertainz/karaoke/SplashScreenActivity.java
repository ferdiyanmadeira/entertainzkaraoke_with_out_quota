package com.madeira.entertainz.karaoke;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.agrawalsuneet.dotsloader.loaders.LinearDotsLoader;
import com.agrawalsuneet.dotsloader.loaders.ZeeLoader;
import com.madeira.entertainz.karaoke.model_room_db.ModelRoom;
import com.madeira.entertainz.controller.STBControl;
import com.madeira.entertainz.controller.USBControl;
import com.madeira.entertainz.karaoke.DBLocal.TElement;
import com.madeira.entertainz.karaoke.DBLocal.TElementMedia;
import com.madeira.entertainz.karaoke.DBLocal.TLocalFile;
import com.madeira.entertainz.karaoke.DBLocal.TLocalFileAPK;
import com.madeira.entertainz.karaoke.DBLocal.TMessage;
import com.madeira.entertainz.karaoke.DBLocal.TMessageMedia;
import com.madeira.entertainz.karaoke.DBLocal.TPlaylist;
import com.madeira.entertainz.karaoke.DBLocal.TSong;
import com.madeira.entertainz.karaoke.DBLocal.TSongCategory;
import com.madeira.entertainz.karaoke.DBLocal.TSongPlaylist;
import com.madeira.entertainz.karaoke.DBLocal.TSongYoutubePlaylist;
import com.madeira.entertainz.karaoke.DBLocal.TSticker;
import com.madeira.entertainz.karaoke.DBLocal.TYoutubePlaylist;
import com.madeira.entertainz.karaoke.config.C;
import com.madeira.entertainz.karaoke.config.CacheData;
import com.madeira.entertainz.karaoke.config.Global;
import com.madeira.entertainz.karaoke.menu_main.MainActivity;
import com.madeira.entertainz.karaoke.menu_setting_admin.SettingAdminActivity;
import com.madeira.entertainz.karaoke.model.ModelMessageLocal;
import com.madeira.entertainz.karaoke.model.ModelThemeLocal;
import com.madeira.entertainz.library.KeyGuard;
import com.madeira.entertainz.library.Util;

import java.util.ArrayList;
import java.util.List;

public class SplashScreenActivity extends AppCompatActivity {

    ZeeLoader zeeLoader;
    String TAG = "SplashScreenActivity";
    KeyGuard kg = new KeyGuard(C.PIN_FOR_SETTING);
    LinearDotsLoader linearDotsLoader;
    STBControl stbControl = new STBControl();
    USBControl usbControl = new USBControl();
    ModelRoom modelRoom = new ModelRoom();

    int scanUSB = 0;

    public static final String KEY_INTENT_FROM_OTT = "KEY_INTENT_FROM_OTT";
    int intentFromOTT = 0;

    public static void startActivity(Context activity) {
        Intent intent = new Intent(activity, SplashScreenActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        activity.startActivity(intent);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        try {
            bind();
            stbControl.setTimeZone(SplashScreenActivity.this);
            stbControl.forceWifiOn();
            boolean rPermission = stbControl.forcePermission(SplashScreenActivity.this);
            if (rPermission)
                fetchContent();
            stbControl.setAndRemoveDeviceOwner(SplashScreenActivity.this);
            stbControl.hideSystemUI(SplashScreenActivity.this);
            intentFromOTT = getIntent().getIntExtra(KEY_INTENT_FROM_OTT, 0);
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }


    void bind() {
        try {
            zeeLoader = findViewById(R.id.zeeLoader);
            linearDotsLoader = findViewById(R.id.linearDotsLoader);
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    void fetchContent() {
//        zeeLoader.setVisibility(View.VISIBLE);
        linearDotsLoader.setVisibility(View.VISIBLE);
        try {
            //get semua data dari fd
            List<TLocalFileAPK> localFileAPKList = new ArrayList<>();
            List<TLocalFile> tLocalFileList = Util.getUSBFile("");
            List<TLocalFile> tLocalFileListJson = Util.getUSBFile("json");
            tLocalFileList.addAll(tLocalFileListJson);

            for (TLocalFile item : tLocalFileList) {
                //fetch apk file
                if (item.contentType.equals("apk")) {
                    TLocalFile tLocalFile = CacheData.hashFile.get(item.fileName);
                    if (tLocalFile == null) {
                        TLocalFileAPK tLocalFileAPK = new TLocalFileAPK();
                        tLocalFileAPK.content = item.content;
                        localFileAPKList.add(tLocalFileAPK);
                    }
                }
                CacheData.hashFile.put(item.fileName, item);
            }

            //fetch element dan element media
            List<TElement> tElements = new ArrayList<>();
            List<TElementMedia> tElementMedia = new ArrayList<>();
            ModelThemeLocal.ResultGetThemeList resultGetThemeList = usbControl.getContentElement();
            if(resultGetThemeList!=null) {
                tElements = resultGetThemeList.list;
                tElementMedia = usbControl.replacePathImageElementMedia(resultGetThemeList.media);
            }
            //fetch song categori dan song
            List<TSongCategory> tSongCategories = new ArrayList<>();
            List<TSong> tSongList = new ArrayList<>();
            tSongCategories = usbControl.getSongCategories();
            tSongList = usbControl.getSongFullPath(SplashScreenActivity.this);

            //fetch message dan message media
            List<TMessage> listMessage = new ArrayList<>();
            List<TMessageMedia> listMessageMedia = new ArrayList<>();
            ModelMessageLocal.ResultGetMessageList resultGetMessageList = usbControl.getMessage();
            if(resultGetMessageList!=null) {
                listMessage = resultGetMessageList.list;
                listMessageMedia = usbControl.replaceMessageMedia(resultGetMessageList.medias);
            }
            //fetch sticker
            List<TSticker> tStickerList = new ArrayList<>();
            tStickerList = usbControl.getSticker();

            //fetch playlist
            List<TPlaylist> tPlaylistList = new ArrayList<>();
            tPlaylistList = usbControl.getPlaylist();

            //fetch song playlist
            List<TSongPlaylist> tSongPlaylists = new ArrayList<>();
            tSongPlaylists = usbControl.getSongPlaylist();

            //fetch youtube playlist
            List<TYoutubePlaylist> tYoutubePlaylists = new ArrayList<>();
            tYoutubePlaylists = usbControl.getYoutubePlaylist();

            //insert song youtube playlist
            List<TSongYoutubePlaylist> tSongYoutubePlaylists = new ArrayList<>();
            tSongYoutubePlaylists = usbControl.getSongYoutubePlaylist();

            scanUSB = scanUSB + 1;
            modelRoom.insertContent(tElements, tElementMedia, tSongList, tSongCategories, listMessage, listMessageMedia, tStickerList, tPlaylistList, tSongPlaylists, tYoutubePlaylists, tSongYoutubePlaylists, localFileAPKList, SplashScreenActivity.this, new ModelRoom.IInsertContent() {
                @Override
                public void onInsert(int result) {
                    try {
                        if (result != 1) {
                            if (scanUSB <= C.LIMIT_RESCAN_USB)
                                fetchContent();
                            else {
                                showUSBUnplug();
                                scanUSB = 0;
                            }
                        } else {
                            Global.setConnectedUSB(1);
                            MainActivity.startActivity(SplashScreenActivity.this, intentFromOTT);
                            finish();
                        }
                    } catch (Exception ex) {
                        Debug.e(TAG, ex);
                    }
                }
            });

        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }

    }


    void showUSBUnplug() {
        try {
            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(SplashScreenActivity.this);
            LayoutInflater inflater = this.getLayoutInflater();
            View dialogView = inflater.inflate(R.layout.alert_rescan_usb, null);
            dialogBuilder.setView(dialogView);

            TextView textView = (TextView) dialogView.findViewById(R.id.textView);
            Button rescanButton = (Button) dialogView.findViewById(R.id.rescanButton);
            Button cancelButton = (Button) dialogView.findViewById(R.id.cancelButton);
            textView.setText(getString(R.string.plugUSB));

            AlertDialog alertDialog = dialogBuilder.create();

            rescanButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        alertDialog.dismiss();
                        fetchContent();
                    } catch (Exception ex) {
                        Debug.e(TAG, ex);
                    }
                }
            });

            cancelButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        alertDialog.dismiss();
                        Global.setConnectedUSB(0);
                        MainActivity.startActivity(SplashScreenActivity.this, intentFromOTT);
                        finish();
                    } catch (Exception ex) {
                        Debug.e(TAG, ex);
                    }
                }
            });

            alertDialog.show();
            WindowManager.LayoutParams layoutParams = alertDialog.getWindow().getAttributes();
            layoutParams.width = ViewGroup.LayoutParams.WRAP_CONTENT;
            alertDialog.show();
            alertDialog.getWindow().setLayout(430, WindowManager.LayoutParams.WRAP_CONTENT);
            Util.hideNavigationBar(SplashScreenActivity.this);
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }

    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {

        switch (keyCode) {
            case KeyEvent.KEYCODE_BACK:
                Toast.makeText(this, "Key back", Toast.LENGTH_SHORT).show();
                return true;
            case KeyEvent.KEYCODE_DPAD_CENTER:
                fetchContent();
        }

        if (kg.processKey(keyCode)) {
            SettingAdminActivity.startActivity(this);
        }

        return super.onKeyDown(keyCode, event);
    }

    /*void setWritableFolderData() {
        try {

            Process p = Runtime.getRuntime().exec("su");
            DataOutputStream dos = new DataOutputStream(p.getOutputStream());
            dos.writeBytes("chmod 775 /data\n");
//            dos.writeBytes("pm grant com.madeira.entertainz.karaoke android.permission.READ_EXTERNAL_STORAGE\n");

            dos.flush();
            dos.close();
            p.waitFor();
            BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));
            StringBuffer output = new StringBuffer();
            StringBuffer errorSB = new StringBuffer();

            BufferedReader stdError = new BufferedReader(new
                    InputStreamReader(p.getErrorStream()));

            String line = "";
            String result = "";
            while ((line = reader.readLine()) != null) {
                output.append(line + "; ");
            }

            while ((line = stdError.readLine()) != null) {
                errorSB.append(line + "; ");
            }

            result = output.toString() + errorSB.toString();

//            Toast.makeText(getApplicationContext(), result, Toast.LENGTH_LONG).show();
            Log.d(TAG, result);

        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
            Log.d(TAG, e.getMessage());

        }
    }*/

    @Override
    protected void onResume() {
        super.onResume();

    }

    @Override
    public void onDestroy() {
        try {
            super.onDestroy();
            Util.freeMemory();
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }
}

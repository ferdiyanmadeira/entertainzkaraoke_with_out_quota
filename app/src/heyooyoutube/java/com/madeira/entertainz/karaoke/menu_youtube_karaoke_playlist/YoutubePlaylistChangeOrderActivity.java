package com.madeira.entertainz.karaoke.menu_youtube_karaoke_playlist;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.madeira.entertainz.controller.USBControl;
import com.madeira.entertainz.karaoke.BaseActivity;
import com.madeira.entertainz.karaoke.DBLocal.DbAccess;
import com.madeira.entertainz.karaoke.DBLocal.DbYoutube;
import com.madeira.entertainz.karaoke.DBLocal.JoinSongYoutubePlaylist;
import com.madeira.entertainz.karaoke.DBLocal.TSongYoutubePlaylist;
import com.madeira.entertainz.karaoke.Debug;
import com.madeira.entertainz.karaoke.R;
import com.madeira.entertainz.karaoke.model_room_db.ModelYoutubePlaylist;
import com.madeira.entertainz.library.EzRecyclerViewAdapter;
import com.madeira.entertainz.library.PerformAsync2;
import com.madeira.entertainz.library.UtilArrayList;

import java.util.ArrayList;
import java.util.List;

public class YoutubePlaylistChangeOrderActivity extends BaseActivity {
    private static final String TAG = "YoutubePlaylistChangeOrderActivity";

    public static final int ACTIVITY_REQUEST_CODE = 1000; //apabila request code bentrok dgn yg lain silahkan di rubah

    private static final String INTENT_PLAYLIST_ID = "INTENT_PLAYLIST_ID";
    private static final String INTENT_SONG_ID = "INTENT_SONG_ID";

    private static final String INTENT_RESULT = "SAVE";

    RecyclerView recyclerView;
    EzRecyclerViewAdapter adapter;

    int playlistId; //playlist yg ada current song
    String songId; //song yg akan di rubah order nya

    List<TSongYoutubePlaylist> list; //load dari db langsung

    /**
     * result 1 = order berubah, 0 = tdk berubah
     *
     * @param activity
     */
    public static void startActivity(Activity activity, int playlistId, String songId) {
        Intent intent;
        intent = new Intent(activity, YoutubePlaylistChangeOrderActivity.class);

        intent.putExtra(INTENT_SONG_ID, songId);
        intent.putExtra(INTENT_PLAYLIST_ID, playlistId);

        activity.startActivityForResult(intent, ACTIVITY_REQUEST_CODE);
    }

    public static boolean checkRequestCode(int reqCode) {
        return reqCode == ACTIVITY_REQUEST_CODE;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        try {
            playlistId = getIntent().getIntExtra(INTENT_PLAYLIST_ID, 0);
            songId = getIntent().getStringExtra(INTENT_SONG_ID);

            setContentView(R.layout.activity_youtube_playlist_change_order);

            recyclerView = findViewById(R.id.recycler_view);

            list = new ArrayList<>();

            loadPlaylist();

            setupRecyclerView();
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {

        switch (keyCode) {
            case KeyEvent.KEYCODE_DPAD_CENTER:
            case KeyEvent.KEYCODE_ENTER:
                saveAndExit();
                break;

            case KeyEvent.KEYCODE_DPAD_UP:
                moveUp();
                break;
            case KeyEvent.KEYCODE_DPAD_DOWN:
                moveDown();
                break;
        }

        return super.onKeyDown(keyCode, event);
    }

    private void setupRecyclerView() {

        adapter = new EzRecyclerViewAdapter(new EzRecyclerViewAdapter.IEzRecyclerViewAdapter() {
            @Override
            public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
                ItemViewHolder item = null;
                try {
                    View view = LayoutInflater.from(getBaseContext()).inflate(R.layout.cell_playlist_youtube_change_order, parent, false);
                    item = new ItemViewHolder(view);

                } catch (Exception e) {
                    e.printStackTrace();
                }
                return item;
            }

            @Override
            public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
                ItemViewHolder item = (ItemViewHolder) holder;
                item.bind(list.get(position));
            }

            @Override
            public int getItemCount() {
                return list.size();
            }

            @Override
            public long getItemId(int pos) {
                return list.get(pos).songPlaylistId;
            }
        });

        recyclerView.setAdapter(adapter);
    }

    /**
     * ambil play list dari room
     */
    private void loadPlaylist() {

        ModelYoutubePlaylist.getSongPlaylistByPlaylistId(YoutubePlaylistChangeOrderActivity.this, playlistId, new ModelYoutubePlaylist.IGetSongPlaylist() {
            @Override
            public void onGetListSongPlaylist(List<TSongYoutubePlaylist> tSongPlaylistList) {
                try {
                    if (tSongPlaylistList.size() == 0) return;
                    //replace list yg ada dgn yg dari room
                    list = tSongPlaylistList;
                    adapter.notifyDataSetChanged();
                } catch (Exception ex) {
                    Debug.e(TAG, ex);
                }
            }
        });

       /* ModelYoutubePlaylist.getSongPlaylist(YoutubePlaylistChangeOrderActivity.this, new ModelYoutubePlaylist.IGetSongPlaylist() {
            @Override
            public void onGetListSongPlaylist(List<TSongYoutubePlaylist> tSongPlaylistList) {
                //TODO: show error apabila null
                if (tSongPlaylistList == null) return;

                //replace list yg ada dgn yg dari room
                list = (List<TSongYoutubePlaylist>) tSongPlaylistList;


                adapter.notifyDataSetChanged();
            }
        });*/

       /* PerformAsync2.run(new PerformAsync2.Callback() {
            @Override
            public Object onBackground(PerformAsync2 performAsync) {

                try {
                    //baca playlist dari room
                    DbYoutube dbYoutube = DbYoutube.Instance.create(getBaseContext());
                    List<TSongYoutubePlaylist> list = dbYoutube.daoAccessTSongYoutubePlaylist().getByPlaylistId(playlistId);
                    dbYoutube.close();

                    return list;
                } catch (Exception e) {
                    e.printStackTrace();
                }

                return null;
            }
        }).setCallbackResult(new PerformAsync2.CallbackResult() {
            @Override
            public void onResult(Object result) {

                //TODO: show error apabila null
                if (result == null) return;

                //replace list yg ada dgn yg dari room
                list = (List<JoinSongYoutubePlaylist>) result;


                adapter.notifyDataSetChanged();
            }
        });*/

    }

    /**
     * move current song to up
     */
    private void moveUp() {
        try {
            int currentSong = TSongYoutubePlaylist.getIndexBySongId(list, songId);
            UtilArrayList.moveUp(list, currentSong);

            adapter.notifyDataSetChanged();
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    private void moveDown() {
        try {
            int currentSong = TSongYoutubePlaylist.getIndexBySongId(list, songId);
            UtilArrayList.moveDown(list, currentSong);

            adapter.notifyDataSetChanged();
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    private void saveAndExit() {

        ModelYoutubePlaylist.changeOrderSongPlaylist(YoutubePlaylistChangeOrderActivity.this, playlistId, list, new ModelYoutubePlaylist.IChangeOrderSongPlaylist() {
            @Override
            public void onChangeOrderSongPlaylist(int result) {
                try {

                    //simpan perubahan order ke file
                    USBControl.writeSongYoutubePlaylist(getBaseContext());

                    //now exit this activity
                    Intent returnIntent = new Intent();
                    returnIntent.putExtra(INTENT_RESULT, 1);
                    setResult(Activity.RESULT_OK, returnIntent);
                    finish();
                } catch (Exception ex) {
                    Debug.e(TAG, ex);
                }
            }
        });
    }

////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////

    class ItemViewHolder extends RecyclerView.ViewHolder {
        private static final String TAG = "ItemViewHolder";

        TextView tvTitle;
        TextView tvArtist;

        ItemViewHolder(View itemView) {
            super(itemView);

            try {
                tvTitle = itemView.findViewById(R.id.text_view_title);
                tvArtist = itemView.findViewById(R.id.text_view_artist);

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        public void bind(TSongYoutubePlaylist item) {

            try {
                tvTitle.setText(item.songName);
                tvArtist.setText(item.artist);

                if (item.songId.equals(songId)) {
                    itemView.setSelected(true);
                } else {
                    itemView.setSelected(false);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}

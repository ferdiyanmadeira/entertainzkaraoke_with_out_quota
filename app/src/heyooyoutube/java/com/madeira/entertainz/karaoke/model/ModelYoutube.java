package com.madeira.entertainz.karaoke.model;

import android.net.Uri;
import android.os.AsyncTask;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.madeira.entertainz.library.ApiError;
import com.madeira.entertainz.library.PerformAsync;
import com.madeira.entertainz.library.PerformAsync2;
import com.madeira.entertainz.library.Util;

import java.util.ArrayList;
import java.util.List;

import static com.madeira.entertainz.library.ApiError.ERR_EXCEPTION;


public class ModelYoutube {
    private static final String TAG = "ModelYoutube";

    private static final String APIHOST = "https://www.googleapis.com/youtube/v3/";
    private static final String KEY = "AIzaSyDV0B9sClRimMBdOFZvqUCvzQT6Q10mQL0";
    private static final String CHART = "mostPopular";

    public interface OnGetSearch {
        void onGetSearch(List<ItemVideo> list, String nextPageToken);
        void onError(Exception e);
    }

    public static void getSearch(String search, String pageToken, final OnGetSearch callback){

        PerformAsync.run(null, new PerformAsync.IPerformAsync() {
            @Override
            public Object onPerformAsyncBackground(Object reference) {

                String url = Uri.parse(APIHOST + "search?part=snippet&maxResults=48&key=AIzaSyDV0B9sClRimMBdOFZvqUCvzQT6Q10mQL0")
                        .buildUpon()
                        .appendQueryParameter("q", search)
                        .appendQueryParameter("pageToken", pageToken)
                        .build().toString();

                String result = Util.readText(url);

                //nothing return
                if (result==null) return new Exception("Unable to reach server");

                Gson gson = new Gson();
                ApiError apiError = gson.fromJson(result, ApiError.class);

                //check apakah error?
                if (apiError.errCode!=null){
                    return new Exception(apiError.errMsg + " (" + apiError.errCode + ")");
                }

                //success
                ResultGetSearch r = gson.fromJson(result, ResultGetSearch.class);

                //hasil dari search akan di normalisasi, shg output video list akan tetap sama

                if (r==null) return null;
                if (r.items==null) return null;

                List<ItemVideo> list = new ArrayList<>();

                for (ItemSearch itemSearch: r.items) {

                    ItemVideo itemVideo = new ItemVideo();

                    itemVideo.id = itemSearch.id.videoId;
                    itemVideo.title = itemSearch.snippet.title;
                    itemVideo.description = itemSearch.snippet.description;
                    itemVideo.urlThumb = itemSearch.snippet.thumbnails.medium.url;

                    list.add(itemVideo);
                }

                return new Object[] { list, r.nextPageToken };
            }

            @Override
            public void onPerformAsyncResult(Object reference, Object result) {
                if(result instanceof Exception){
                    callback.onError((Exception) result);
                } else {

                    Object []ar = (Object[]) result;

                    callback.onGetSearch((List<ItemVideo>)ar[0], (String)ar[1]);
                }
            }
        });
    }


    public interface OnGetSearchVideoDetail {
        void onGetSearchVideoDetail(ResultGetSearchVideo list);
        void onError(Exception e);
    }

    public static void getSearchVideoDetail(String id, final OnGetSearchVideoDetail callback){

        PerformAsync.run(null, new PerformAsync.IPerformAsync() {
            @Override
            public Object onPerformAsyncBackground(Object reference) {

                String url = Uri.parse(APIHOST + "videos?key=AIzaSyDV0B9sClRimMBdOFZvqUCvzQT6Q10mQL0&part=snippet")
                        .buildUpon()
                        .appendQueryParameter("id", id)
                        .build().toString();

                String result = Util.readText(url);

                //nothing return
                if (result==null) return new Exception("Unable to reach server");

                Gson gson = new Gson();
                ApiError apiError = gson.fromJson(result, ApiError.class);

                //check apakah error?
                if (apiError.errCode!=null){
                    return new Exception(apiError.errMsg + " (" + apiError.errCode + ")");
                }

                //success
                return gson.fromJson(result,ResultGetSearchVideo.class);
            }

            @Override
            public void onPerformAsyncResult(Object reference, Object result) {
                if(result instanceof Exception){
                    callback.onError((Exception) result);
                } else {
                    callback.onGetSearchVideoDetail((ResultGetSearchVideo) result);
                }
            }
        });
    }


    public interface OnGetVideos {
        void onGetVideos(List<ItemVideo> list, String nextPageToken);
        void onError(Exception e);
    }

    public interface OnFetchCategory {
        void onSucess(List<ItemCategory> list);
        void onError(Exception e);
    }

    public static void getVideos(String pageToken, String categoryId, String regionCode, final OnGetVideos callback){

        PerformAsync.run(null, new PerformAsync.IPerformAsync() {
            @Override
            public Object onPerformAsyncBackground(Object reference) {

                String url = Uri.parse(APIHOST + "videos")
                        .buildUpon()
                        .appendQueryParameter("key", KEY)
                        .appendQueryParameter("part", "snippet")
                        .appendQueryParameter("chart", CHART)
                        .appendQueryParameter("regionCode", regionCode)
                        .appendQueryParameter("videoCategoryId", categoryId)
                        .appendQueryParameter("pageToken", pageToken)
                        .appendQueryParameter("maxResults", String.valueOf(48))
                        .build().toString();

                try {

                    String result = Util.readText(url);

                    //nothing return
                    if (result==null) return new Exception("Unable to reach server");

                    Gson gson = new Gson();
                    ApiError apiError = gson.fromJson(result, ApiError.class);

                    //check apakah error?
                    if (apiError.errCode!=null){
                        return new Exception(apiError.errMsg + " (" + apiError.errCode + ")");
                    }

                    //success
                    ResultGetVideos r =  gson.fromJson(result,ResultGetVideos.class);

                    List<ItemVideo> list = new ArrayList<>();

                    for (ItemVideos itemVideos: r.items) {

                        ItemVideo item = new ItemVideo();

                        item.id = itemVideos.id;
                        item.title = itemVideos.snippet.title;
                        item.description = itemVideos.snippet.description;
                        item.urlThumb = itemVideos.snippet.thumbnails.medium.url;
                        list.add(item);
                    }

                    return new Object[] { list, r.nextPageToken };
                } catch (JsonSyntaxException e) {
                    e.printStackTrace();
                }

                return ApiError.newInstance(ERR_EXCEPTION, "Error Exception");
            }

            @Override
            public void onPerformAsyncResult(Object reference, Object result) {
                if(result instanceof Exception){
                    callback.onError((Exception) result);
                } else {

                    Object []ar = (Object[]) result;

                    callback.onGetVideos((List<ItemVideo>) ar[0], (String) ar[1]);
                }
            }
        });
    }

    public static AsyncTask fetchCategory(String regionCode, OnFetchCategory callback){
        return PerformAsync2.run(new PerformAsync2.Callback() {
            @Override
            public Object onBackground(PerformAsync2 performAsync) {

                String url = Uri.parse(APIHOST + "videoCategories")
                        .buildUpon()
                        .appendQueryParameter("key", KEY)
                        .appendQueryParameter("part", "snippet")
                        .appendQueryParameter("regionCode", regionCode)
                        .build().toString();

                String result = Util.readText(url);

                Gson gson = new Gson();

                //process response apabila ada error, null/empty/apiError
                ApiError e = ApiError.processError(gson, result);
                if (e!=null) return e; //error akan di pass ke onError

                //if no error, next convert the real data
                ResultFetchCategory r = gson.fromJson(result, ResultFetchCategory.class);

                return r.items;
            }
        }).setCallbackResult(new PerformAsync2.CallbackResult() {
            @Override
            public void onResult(Object result) {
                if (result instanceof Exception){
                    callback.onError((Exception) result);
                } else {
                    callback.onSucess((List<ItemCategory>) result);
                }
            }
        });
    }

    ////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////

    static public class ItemVideo {
        public String id;
        public String title;
        public String description;
        public String urlThumb;
    }

    public class ResultGetSearch {
        @SerializedName("kind")
        @Expose
        public String kind;
        @SerializedName("etag")
        @Expose
        public String etag;
        @SerializedName("nextPageToken")
        @Expose
        public String nextPageToken;
        @SerializedName("prevPageToken")
        @Expose
        public String prevPageToken;
        @SerializedName("regionCode")
        @Expose
        public String regionCode;
        @SerializedName("pageInfo")
        @Expose
        public PageInfoSearch pageInfo;
        @SerializedName("items")
        @Expose
        public List<ItemSearch> items = null;
    }

    public class PageInfoSearch {
        @SerializedName("totalResults")
        @Expose
        public Integer totalResults;
        @SerializedName("resultsPerPage")
        @Expose
        public Integer resultsPerPage;
    }

    public class ItemSearch {
        @SerializedName("kind")
        @Expose
        public String kind;
        @SerializedName("etag")
        @Expose
        public String etag;
        @SerializedName("id")
        @Expose
        public IdSearch id;
        @SerializedName("snippet")
        @Expose
        public SnippetSearch snippet;
    }

    public class IdSearch {
        @SerializedName("kind")
        @Expose
        public String kind;
        @SerializedName("channelId")
        @Expose
        public String channelId;
        @SerializedName("videoId")
        @Expose
        public String videoId;
    }

    public class SnippetSearch {
        @SerializedName("publishedAt")
        @Expose
        public String publishedAt;
        @SerializedName("channelId")
        @Expose
        public String channelId;
        @SerializedName("title")
        @Expose
        public String title;
        @SerializedName("description")
        @Expose
        public String description;
        @SerializedName("thumbnails")
        @Expose
        public ThumbnailsSearch thumbnails;
        @SerializedName("channelTitle")
        @Expose
        public String channelTitle;
        @SerializedName("liveBroadcastContent")
        @Expose
        public String liveBroadcastContent;
    }

    public class ThumbnailsSearch {
        @SerializedName("default")
        @Expose
        public Default _default;
        @SerializedName("medium")
        @Expose
        public Medium medium;
        @SerializedName("high")
        @Expose
        public High high;
    }

    ////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////

    public class ResultGetSearchVideo{
        @SerializedName("kind")
        @Expose
        public String kind;
        @SerializedName("etag")
        @Expose
        public String etag;
        @SerializedName("pageInfo")
        @Expose
        public PageInfoSearchVideo pageInfo;
        @SerializedName("items")
        @Expose
        public List<ItemSearchVideo> items = null;
    }

    public class PageInfoSearchVideo {
        @SerializedName("totalResults")
        @Expose
        public Integer totalResults;
        @SerializedName("resultsPerPage")
        @Expose
        public Integer resultsPerPage;
    }

    public class ItemSearchVideo {
        @SerializedName("kind")
        @Expose
        public String kind;
        @SerializedName("etag")
        @Expose
        public String etag;
        @SerializedName("id")
        @Expose
        public String id;
        @SerializedName("snippet")
        @Expose
        public SnippetSearchVideo snippet;
    }

    public class SnippetSearchVideo {
        @SerializedName("publishedAt")
        @Expose
        public String publishedAt;
        @SerializedName("channelId")
        @Expose
        public String channelId;
        @SerializedName("title")
        @Expose
        public String title;
        @SerializedName("description")
        @Expose
        public String description;
        @SerializedName("thumbnails")
        @Expose
        public ThumbnailsSearchVideo thumbnails;
        @SerializedName("channelTitle")
        @Expose
        public String channelTitle;
        @SerializedName("tags")
        @Expose
        public List<String> tags = null;
        @SerializedName("categoryId")
        @Expose
        public String categoryId;
        @SerializedName("liveBroadcastContent")
        @Expose
        public String liveBroadcastContent;
        @SerializedName("localized")
        @Expose
        public LocalizedSearchVideo localized;
        @SerializedName("defaultAudioLanguage")
        @Expose
        public String defaultAudioLanguage;
    }

    public class ThumbnailsSearchVideo {
        @SerializedName("default")
        @Expose
        public Default _default;
        @SerializedName("medium")
        @Expose
        public Medium medium;
        @SerializedName("high")
        @Expose
        public High high;
        @SerializedName("standard")
        @Expose
        public Standard standard;
    }

    public class LocalizedSearchVideo {
        @SerializedName("title")
        @Expose
        public String title;
        @SerializedName("description")
        @Expose
        public String description;
    }

    ////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////

    public class ResultGetVideos {
        @SerializedName("kind")
        @Expose
        public String kind;
        @SerializedName("etag")
        @Expose
        public String etag;
        @SerializedName("nextPageToken")
        @Expose
        public String nextPageToken;
        @SerializedName("prevPageToken")
        @Expose
        public String prevPageToken;
        @SerializedName("pageInfo")
        @Expose
        public PageInfoVideos pageInfo;
        @SerializedName("items")
        @Expose
        public List<ItemVideos> items = null;
    }

    public class PageInfoVideos {
        @SerializedName("totalResults")
        @Expose
        public Integer totalResults;
        @SerializedName("resultsPerPage")
        @Expose
        public Integer resultsPerPage;
    }

    public class ItemVideos {
        @SerializedName("kind")
        @Expose
        public String kind;
        @SerializedName("etag")
        @Expose
        public String etag;
        @SerializedName("id")
        @Expose
        public String id;
        @SerializedName("snippet")
        @Expose
        public SnippetVideos snippet;
    }

    public class SnippetVideos {
        @SerializedName("publishedAt")
        @Expose
        public String publishedAt;
        @SerializedName("channelId")
        @Expose
        public String channelId;
        @SerializedName("title")
        @Expose
        public String title;
        @SerializedName("description")
        @Expose
        public String description;
        @SerializedName("thumbnails")
        @Expose
        public ThumbnailsVideos thumbnails;
        @SerializedName("channelTitle")
        @Expose
        public String channelTitle;
        @SerializedName("tags")
        @Expose
        public List<String> tags = null;
        @SerializedName("categoryId")
        @Expose
        public String categoryId;
        @SerializedName("liveBroadcastContent")
        @Expose
        public String liveBroadcastContent;
        @SerializedName("defaultLanguage")
        @Expose
        public String defaultLanguage;
        @SerializedName("localized")
        @Expose
        public LocalizedVideos localized;
        @SerializedName("defaultAudioLanguage")
        @Expose
        public String defaultAudioLanguage;
    }

    public class ThumbnailsVideos {
        @SerializedName("default")
        @Expose
        public Default _default;
        @SerializedName("medium")
        @Expose
        public Medium medium;
        @SerializedName("high")
        @Expose
        public High high;
        @SerializedName("standard")
        @Expose
        public Standard standard;
        @SerializedName("maxres")
        @Expose
        public Maxres maxres;
    }

    public class LocalizedVideos {
        @SerializedName("title")
        @Expose
        public String title;
        @SerializedName("description")
        @Expose
        public String description;
    }

    ////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////

    public class Standard {
        @SerializedName("url")
        @Expose
        public String url;
        @SerializedName("width")
        @Expose
        public Integer width;
        @SerializedName("height")
        @Expose
        public Integer height;
    }

    public class Default {
        @SerializedName("url")
        @Expose
        public String url;
        @SerializedName("width")
        @Expose
        public Integer width;
        @SerializedName("height")
        @Expose
        public Integer height;
    }

    public class High {
        @SerializedName("url")
        @Expose
        public String url;
        @SerializedName("width")
        @Expose
        public Integer width;
        @SerializedName("height")
        @Expose
        public Integer height;
    }

    public class Medium {
        @SerializedName("url")
        @Expose
        public String url;
        @SerializedName("width")
        @Expose
        public Integer width;
        @SerializedName("height")
        @Expose
        public Integer height;
    }

    public class Maxres {
        @SerializedName("url")
        @Expose
        public String url;
        @SerializedName("width")
        @Expose
        public Integer width;
        @SerializedName("height")
        @Expose
        public Integer height;
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////

    public class ResultFetchCategory {
        @SerializedName("kind")
        @Expose
        public String kind;
        @SerializedName("etag")
        @Expose
        public String etag;
        @SerializedName("items")
        @Expose
        public List<ItemCategory> items = null;
    }

    static public class ItemCategory {

        @SerializedName("kind")
        @Expose
        public String kind;
        @SerializedName("etag")
        @Expose
        public String etag;
        @SerializedName("id")
        @Expose
        public String id;
        @SerializedName("snippet")
        @Expose
        public SnippetCategory snippet;
    }

    public class SnippetCategory {
        @SerializedName("channelId")
        @Expose
        public String channelId;
        @SerializedName("title")
        @Expose
        public String title;
        @SerializedName("assignable")
        @Expose
        public boolean assignable;
    }
}

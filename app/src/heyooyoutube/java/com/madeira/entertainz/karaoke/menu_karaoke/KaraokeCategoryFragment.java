package com.madeira.entertainz.karaoke.menu_karaoke;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.madeira.entertainz.helper.RecyclerViewAdapter;
import com.madeira.entertainz.karaoke.Debug;
import com.madeira.entertainz.karaoke.R;
import com.madeira.entertainz.karaoke.DBLocal.DbSong;
import com.madeira.entertainz.karaoke.DBLocal.TSongCategory;
import com.madeira.entertainz.karaoke.model_room_db.ModelSong;
import com.madeira.entertainz.library.PerformAsync2;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class KaraokeCategoryFragment extends Fragment implements KaraokeTheme.IKaraokeThemeListener {
    String TAG = "KaraokeCategoryFragment";

    View root;
    LinearLayout categoryLL;
    TextView categoryTV;
    LinearLayout recyclerLL;
    RecyclerView recyclerView;
    RecyclerViewAdapter adapter;

    private IKaraokeCategoryListener mListener;
    KaraokeTheme karaokeTheme;
    List<TSongCategory> songCategoryList = new ArrayList<>();


    public KaraokeCategoryFragment() {
        // Required empty public constructor
    }

    public static KaraokeCategoryFragment newInstance() {
        KaraokeCategoryFragment fragment = new KaraokeCategoryFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        root = inflater.inflate(R.layout.fragment_karaoke_category, container, false);
        try {
            bind();
            karaokeTheme = new KaraokeTheme(getActivity(), this);
            karaokeTheme.setTheme();
            fetchSongCategory();
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
        return root;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
       /* if (context instanceof IKaraokeCategoryListener) {
            mListener = (IKaraokeCategoryListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }*/
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void setThemeUpdateDate(Date lastUpdateTheme) {

    }

    @Override
    public void setListColorTheme(int color) {
        try {
            categoryLL.setBackgroundResource(R.drawable.tags_rounded_corners);
            recyclerLL.setBackgroundResource(R.drawable.tags_rounded_corners);

            GradientDrawable categoryDrawable = (GradientDrawable) categoryLL.getBackground();
            categoryDrawable.setColor(color);

            GradientDrawable recycleDrawable = (GradientDrawable) recyclerLL.getBackground();
            recycleDrawable.setColor(color);
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    @Override
    public void setTextAndSelectionColorTheme(ColorStateList colorSelection, ColorStateList colorText, int colorTextSelected, int colorTextFocus, int colorTextNormal, int colorSelectionSelected, int colorSelectionFocus) {

        try {
            categoryTV.setTextColor(colorTextNormal);
            this.colorSelection = colorSelection;
            this.colorText = colorText;
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }

    }


    public interface IKaraokeCategoryListener {
        // TODO: Update argument type and name
        void onCategoryClick(int categoryId, String category);
    }

    void bind() {
        categoryLL = (LinearLayout) root.findViewById(R.id.categoryLL);
        categoryTV = (TextView) root.findViewById(R.id.categoryTV);
        recyclerLL = (LinearLayout) root.findViewById(R.id.recyclerLL);
        recyclerView = (RecyclerView) root.findViewById(R.id.recyclerView);
    }

    void fetchSongCategory() {
        ModelSong.getSongCategory(getActivity(), new ModelSong.IGetListSongCategory() {
            @Override
            public void onGetListSongCategory(List<TSongCategory> tSongCategoryList) {
                try {
                    songCategoryList = tSongCategoryList;
                    setRecyclerView();
                } catch (Exception ex) {
                    Debug.e(TAG, ex);
                }
            }
        });

    }

    void setRecyclerView() {
        try {
            adapter = new RecyclerViewAdapter(new RecyclerViewAdapter.IRecyclerViewAdapter() {
                @Override
                public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
                    View view = LayoutInflater.from(getActivity()).inflate(R.layout.cell_karaoke_category, parent, false);
                    ItemViewHolder item = new ItemViewHolder(view);
                    return item;
                }

                @Override
                public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
                    ItemViewHolder item = (ItemViewHolder) holder;
                    item.bind(songCategoryList.get(position), position);
                }

                @Override
                public int getItemCount() {
                    return songCategoryList.size();
                }
            });

            recyclerView.setAdapter(adapter);
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    //view ini khusus di pakai utk ItemViewHolder, utk selectedItemView
    View selectedView;
    int selectedItem;

    class ItemViewHolder extends RecyclerView.ViewHolder {
        private static final String TAG = "ItemViewHolder";

        View root;
        TextView tvTitle;

        public ItemViewHolder(View itemView) {
            super(itemView);
            root = itemView;

            tvTitle = itemView.findViewById(R.id.text_title);

//            selectionColor = itemView.findViewById(R.id.selectioncolor);
        }

        public void bind(TSongCategory tSongCategory, int pos) {
            try {
                tvTitle.setText(tSongCategory.songCategory);

                if (selectedItem == pos) {
                    selectView(root);
                }

//            untuk merubah warna selection
                if (colorSelection != null) root.setBackgroundTintList(colorSelection);

//            untuk merubah warna text
                if (colorText != null) tvTitle.setTextColor(colorText);

                if (pos == 0) {
                    root.setSelected(true);
                }

                root.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        selectView(v);
                        selectedItem = pos;
                        mListener.onCategoryClick(tSongCategory.songCategoryId, tSongCategory.songCategory);
                    }
                });
            } catch (Exception ex) {
                Debug.e(TAG, ex);
            }

        }

        void selectView(View v) {
            try {
                //clear selection
                if (selectedView != null) {
                    selectedView.setSelected(false);
                }

                //make selection
                selectedView = v;
                selectedView.setSelected(true);
            } catch (Exception ex) {
                Debug.e(TAG, ex);
            }
        }
    }

    ColorStateList colorSelection, colorText;

    public void setCallback(IKaraokeCategoryListener listener) {
        this.mListener = listener;
    }

}

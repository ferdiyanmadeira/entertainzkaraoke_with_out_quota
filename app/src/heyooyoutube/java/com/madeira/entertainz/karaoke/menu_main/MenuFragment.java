package com.madeira.entertainz.karaoke.menu_main;


import android.content.res.ColorStateList;
import android.graphics.ColorFilter;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.ScaleAnimation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.madeira.entertainz.helper.RecyclerViewAdapter;
import com.madeira.entertainz.karaoke.Debug;
import com.madeira.entertainz.karaoke.R;
import com.madeira.entertainz.karaoke.DBLocal.TElement;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static com.madeira.entertainz.karaoke.config.C.MENU_ACCESSORIES;
import static com.madeira.entertainz.karaoke.config.C.MENU_MESSAGES;
import static com.madeira.entertainz.karaoke.config.C.MENU_NEWS;
import static com.madeira.entertainz.karaoke.config.C.MENU_PLAYLIST;
import static com.madeira.entertainz.karaoke.config.C.MENU_RADIO;
import static com.madeira.entertainz.karaoke.config.C.MENU_SEARCH_SONG;
import static com.madeira.entertainz.karaoke.config.C.MENU_SETTING;
import static com.madeira.entertainz.karaoke.config.C.MENU_UPDATE_SONG;
import static com.madeira.entertainz.karaoke.config.C.MENU_VIDIO;
import static com.madeira.entertainz.karaoke.config.C.MENU_LIVE_CHANNEL;
import static com.madeira.entertainz.karaoke.config.C.MENU_YOUTUBE;
import static com.madeira.entertainz.karaoke.config.C.MENU_YOUTUBE_PLAYLIST;


public class MenuFragment extends Fragment implements MainTheme.IMainThemeListener {
    String TAG = "MenuFragment";

    RecyclerView recyclerView;
    RecyclerView recyclerView2;
    View root;
    MainTheme mainTheme;
    RelativeLayout rootRL;
    ColorStateList colorSelection, colorText;

    public interface IMenuMainFragmentListener {
        void onClickMenu(String menuName, int menuId);
    }


    RecyclerViewAdapter adapter;
    RecyclerViewAdapter adapter2;
    List<Integer> listMenuId = new ArrayList<>();
    List<Integer> listMenuId2 = new ArrayList<>();
    IMenuMainFragmentListener callback;
    ColorFilter filterBw;
    ColorFilter filterColor;

    public MenuFragment() {
        // Required empty public constructor


    }


    public static MenuFragment newInstance() {
        MenuFragment fragment = new MenuFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            if (getArguments() != null) {

            }

            listMenuId.add(MENU_SEARCH_SONG);
            listMenuId.add(MENU_PLAYLIST);
            listMenuId.add(MENU_UPDATE_SONG);
            listMenuId.add(MENU_YOUTUBE);
            listMenuId.add(MENU_YOUTUBE_PLAYLIST);


//        listMenuId2.add(MENU_VIDIO);
            listMenuId2.add(MENU_NEWS);
            listMenuId2.add(MENU_LIVE_CHANNEL);
//        listMenuId2.add(MENU_RADIO);
            listMenuId2.add(MENU_MESSAGES);
            listMenuId2.add(MENU_ACCESSORIES);
            listMenuId2.add(MENU_SETTING);
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        root = inflater.inflate(R.layout.fragment_menu, container, false);
        try {
            bind();
            setColorFilter();
            mainTheme = new MainTheme(getActivity(), this);
            mainTheme.setTheme();
            setupRecyclerView();
            setupRecyclerViewBottom();
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
        return root;
    }


    void bind() {
        try {
            rootRL = (RelativeLayout) root.findViewById(R.id.rootRL);
            recyclerView = (RecyclerView) root.findViewById(R.id.recyclerView);
            recyclerView2 = root.findViewById(R.id.recyclerView2);
        }catch (Exception ex)
        {
            Debug.e(TAG, ex);
        }
    }

    void setupRecyclerView() {

        try {

            adapter = new RecyclerViewAdapter(new RecyclerViewAdapter.IRecyclerViewAdapter() {

                @Override
                public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
                    View view = LayoutInflater.from(getContext()).inflate(R.layout.cell_menu,
                            parent, false);
                    return new ItemViewHolder(view);
                }

                @Override
                public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
                    ItemViewHolder item = (ItemViewHolder) holder;

                    int menuId = listMenuId.get(position);
                    String title = getMenuTitle(menuId);
                    int imageId = getMenuImage(menuId);

                    item.bind(title, imageId, menuId);
                }

                @Override
                public int getItemCount() {
                    return listMenuId.size();
                }
            });


            GridLayoutManager gridLayoutManager = new GridLayoutManager(getActivity(), listMenuId.size());
            recyclerView.setLayoutManager(gridLayoutManager);
            recyclerView.setAdapter(adapter);
            recyclerView.requestFocus();
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }

    }

    void setupRecyclerViewBottom() {

        try {
            adapter2 = new RecyclerViewAdapter(new RecyclerViewAdapter.IRecyclerViewAdapter() {

                @Override
                public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
                    View view = LayoutInflater.from(getContext()).inflate(R.layout.cell_menu,
                            parent, false);
                    return new ItemViewHolder(view);
                }

                @Override
                public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
                    ItemViewHolder item = (ItemViewHolder) holder;

                    int menuId = listMenuId2.get(position);
                    String title = getMenuTitle(menuId);
                    int imageId = getMenuImage(menuId);
                    item.bind(title, imageId, menuId);
                }

                @Override
                public int getItemCount() {
                    return listMenuId2.size();
                }
            });

            GridLayoutManager gridLayoutManager = new GridLayoutManager(getActivity(), listMenuId2.size());
            recyclerView2.setLayoutManager(gridLayoutManager);
            recyclerView2.setAdapter(adapter2);
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }

    }

    void setColorFilter() {
        try {
            ColorMatrix matrix = new ColorMatrix();
            matrix.setSaturation(0);

            filterBw = new ColorMatrixColorFilter(matrix);

            matrix.setSaturation(1);
            filterColor = new ColorMatrixColorFilter(matrix);
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    String getMenuTitle(int menuId) {
        String title = null;

        try {
            switch (menuId) {
                case MENU_SEARCH_SONG:
                    title = getString(R.string.title_search_song);
                    break;
                case MENU_PLAYLIST:
                    title = getString(R.string.title_playlist);
                    break;
                case MENU_UPDATE_SONG:
                    title = getString(R.string.title_update_song);
                    break;
                case MENU_ACCESSORIES:
                    title = getString(R.string.title_accessories);
                    break;
                case MENU_MESSAGES:
                    title = getString(R.string.title_messages);
                    break;
                case MENU_SETTING:
                    title = getString(R.string.title_setting);
                    break;
                case MENU_YOUTUBE:
                    title = getString(R.string.title_youtube);
                    break;
                case MENU_LIVE_CHANNEL:
                    title = getString(R.string.title_live_channel);
                    break;
                case MENU_VIDIO:
                    title = getString(R.string.title_vidio);
                    break;
                case MENU_RADIO:
                    title = getString(R.string.title_radio);
                    break;
                case MENU_YOUTUBE_PLAYLIST:
                    title = getString(R.string.title_youtube_playlist);
                    break;
                case MENU_NEWS:
                    title = getString(R.string.news);
            }
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
        return title;
    }

    int getMenuImage(int menuId) {
        int imageId = 0;

        try {
            switch (menuId) {

                case MENU_SEARCH_SONG:
                    imageId = R.drawable.selector_menu_search_song;
                    break;
                case MENU_PLAYLIST:
                    imageId = R.drawable.selector_menu_playlist;
                    break;
                case MENU_UPDATE_SONG:
                    imageId = R.drawable.selector_menu_update_song;
                    break;
                case MENU_ACCESSORIES:
                    imageId = R.drawable.selector_menu_shopping;
                    break;
                case MENU_MESSAGES:
                    imageId = R.drawable.selector_menu_messages;
                    break;
                case MENU_SETTING:
                    imageId = R.drawable.selector_menu_setting;
                    break;
                case MENU_LIVE_CHANNEL:
                    imageId = R.drawable.selector_menu_live_channel;
                    break;
                case MENU_YOUTUBE:
                    imageId = R.drawable.selector_menu_youtube;
                    break;
                case MENU_VIDIO:
                    imageId = R.drawable.menu_vidio;
                    break;
                case MENU_RADIO:
                    imageId = R.drawable.menu_radio;
                    break;
                case MENU_YOUTUBE_PLAYLIST:
                    imageId = R.drawable.selector_menu_youtube_playlist;
                    break;
                case MENU_NEWS:
                    imageId = R.drawable.selector_menu_news;
                    break;
            }
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }

        return imageId;
    }

    public void setCallback(IMenuMainFragmentListener callback) {
        this.callback = callback;
    }

    class ItemViewHolder extends RecyclerView.ViewHolder {

        View root;
        TextView titleTV;
        ImageView imageIV;
        CardView cardView;
        LinearLayout titleLayout;

        public ItemViewHolder(View itemView) {
            super(itemView);
            root = itemView;
            titleTV = itemView.findViewById(R.id.text_title);
            imageIV = itemView.findViewById(R.id.image_view);
            imageIV.setColorFilter(filterBw);
            cardView = itemView.findViewById(R.id.card_view);
            titleLayout = itemView.findViewById(R.id.title_ll);
        }

        public void bind(String title, int imageId, int menuId) {

            try {
                titleTV.setText(title);
                imageIV.setImageResource(imageId);
//                if (colorText != null) titleTV.setTextColor(colorText);

//            untuk merubah warna selection
//                if (colorSelection != null) titleTV.setBackgroundTintList(colorSelection);

                itemView.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                    @Override
                    public void onFocusChange(View v, boolean hasFocus) {
                        updateFocus(root, hasFocus);
//                        GradientDrawable categoryDrawable = (GradientDrawable) titleLayout.getBackground();

//                        if (hasFocus) {
////                            categoryDrawable.setColor(getContext().getColor(R.color.button_selected_color));
////                            titleTV.setSelected(true);
////                            imageIV.setColorFilter(filterColor);
////                            imageIV.setSelected(true);
//                        } else {
////                            categoryDrawable.setColor(getContext().getColor(R.color.theme_main_list_black_background_color));
////                            titleTV.setSelected(false);
////                            imageIV.clearFocus();
////                            imageIV.setColorFilter(filterBw);
//                        }
                    }
                });

                itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        callback.onClickMenu(title, menuId);
                    }
                });
            } catch (Exception ex) {
                Debug.e(TAG, ex);
            }

        }
    }

    void updateFocus(View view, boolean hasFocus) {

        try {
            if (hasFocus) {
                ViewCompat.setTranslationZ(view, 3);
                scaleView(view, 1, 1.05f);
            } else {
                scaleView(view, 1.05f, 1);
                ViewCompat.setTranslationZ(view, 0);
            }
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    void scaleView(View v, float startScale, float endScale) {
        try {
            Animation anim = new ScaleAnimation(
                    startScale, endScale, // Start and end values for the X axis scaling
                    startScale, endScale, // Start and end values for the Y axis scaling
                    Animation.RELATIVE_TO_SELF, 0.5f, // Pivot point of X scaling
                    Animation.RELATIVE_TO_SELF, 0.5f); // Pivot point of Y scaling
            anim.setFillAfter(true); // Needed to keep the result of the animation
            anim.setDuration(100);
            v.startAnimation(anim);
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    @Override
    public void setThemeUpdateDate(Date lastUpdateTheme) {

    }

    @Override
    public void setBackgroundTheme(TElement element) {

    }

    @Override
    public void setListColorTheme(int color) {
        rootRL.setBackgroundColor(color);
    }

    @Override
    public void setFooterColorTheme(int color) {

    }

    @Override
    public void setTextAndSelectionColorTheme(ColorStateList colorSelection, ColorStateList colorText, int colorTextSelected, int colorTextFocus, int colorTextNormal, int colorSelectionSelected, int colorSelectionFocus) {
        try {
            this.colorSelection = colorSelection;
            this.colorText = colorText;
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    @Override
    public void setHeaderColorTheme(int color) {

    }

    @Override
    public void onDetach() {
        super.onDetach();
        callback = null;
    }

    /**
     * untuk enable / disable view ketika list mood fragment sedang diakses
     * belum berhasil, isinya masih di comment
     */
    public void setFocusAndEnableView(boolean prm) {

        /*root.setFocusable(prm);
        root.setFocusableInTouchMode(prm);*/
    }

}

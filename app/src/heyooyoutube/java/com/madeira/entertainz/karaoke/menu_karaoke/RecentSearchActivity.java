package com.madeira.entertainz.karaoke.menu_karaoke;

import android.app.Activity;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.madeira.entertainz.helper.RecyclerViewAdapter;
import com.madeira.entertainz.karaoke.DBLocal.DbRecentSearchSong;
import com.madeira.entertainz.karaoke.DBLocal.TRecentSearchSong;
import com.madeira.entertainz.karaoke.Debug;
import com.madeira.entertainz.karaoke.R;
import com.madeira.entertainz.karaoke.config.C;
import com.madeira.entertainz.karaoke.menu_main.MainActivity;
import com.madeira.entertainz.karaoke.model_room_db.ModelRecentSearch;
import com.madeira.entertainz.karaoke.model_room_db.ModelSong;
import com.madeira.entertainz.library.PerformAsync2;
import com.madeira.entertainz.library.Util;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class RecentSearchActivity extends AppCompatActivity implements KaraokeTheme.IKaraokeThemeListener {
    private static final String TAG = "RecentSearchActivity";
    RecyclerView recyclerView;
    RecyclerViewAdapter adapter;
    List<TRecentSearchSong> tRecentSearchSongList = new ArrayList<>();
    KaraokeTheme karaokeTheme;
    public static final int ACTIVITY_REQUEST_CODE = 1001; //apabila request code bentrok dgn yg lain silahkan di rubah
    public static final String INTENT_RESULT = "SELECT";

    public static void startActivity(Activity activity) {
        try {
            Intent intent;
            intent = new Intent(activity, RecentSearchActivity.class);
            activity.startActivityForResult(intent, ACTIVITY_REQUEST_CODE);
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            setContentView(R.layout.activity_recent_search);
            bind();
            karaokeTheme = new KaraokeTheme(RecentSearchActivity.this, this);
//            karaokeTheme.setTheme();
            Util.hideNavigationBar(this);

            fetchRecentSearchSong();
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    void bind() {
        try {
            recyclerView = findViewById(R.id.recycler_view);

        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    void fetchRecentSearchSong() {
        ModelRecentSearch.getRecentSong(RecentSearchActivity.this, C.SEARCH_SONG_KARAOKE, new ModelRecentSearch.IGetListRecentSong() {
            @Override
            public void onGetListRecentSong(List<TRecentSearchSong> prmTRecentSearchSongList) {
                tRecentSearchSongList=prmTRecentSearchSongList;
                setRecyclerView();

            }
        });

        /*PerformAsync2.run(performAsync ->
        {
            try {
                DbRecentSearchSong dbRecentSearchSong = DbRecentSearchSong.Instance.create(RecentSearchActivity.this);
                tRecentSearchSongList = dbRecentSearchSong.daoAccessRecentSearchSong().getAll(C.SEARCH_SONG_KARAOKE);
                dbRecentSearchSong.close();
            } catch (Exception ex) {
                Debug.e(TAG, ex);
            }
            return tRecentSearchSongList;
        }).setCallbackResult(result ->
        {
            try {
                setRecyclerView();
            } catch (Exception ex) {
                Debug.e(TAG, ex);
            }
        });*/
    }

    void setRecyclerView() {
        try {
            adapter = new RecyclerViewAdapter(new RecyclerViewAdapter.IRecyclerViewAdapter() {
                @Override
                public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
                    View view = LayoutInflater.from(RecentSearchActivity.this).inflate(R.layout.cell_recent_search, parent, false);
                    ItemViewHolder item = new ItemViewHolder(view);
                    return item;
                }

                @Override
                public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
                    ItemViewHolder item = (ItemViewHolder) holder;
                    item.bind(tRecentSearchSongList.get(position), position);
                }

                @Override
                public int getItemCount() {
                    return tRecentSearchSongList.size();
                }
            });

            recyclerView.setAdapter(adapter);
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }


    //view ini khusus di pakai utk ItemViewHolder, utk selectedItemView
    View selectedView;
    int selectedItem;

    @Override
    public void setThemeUpdateDate(Date lastUpdateTheme) {

    }

    @Override
    public void setListColorTheme(int color) {
        try {
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    @Override
    public void setTextAndSelectionColorTheme(ColorStateList colorSelection, ColorStateList colorText, int colorTextSelected, int colorTextFocus, int colorTextNormal, int colorSelectionSelected, int colorSelectionFocus) {
        try {
            this.colorSelection = colorSelection;
            this.colorText = colorText;
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    class ItemViewHolder extends RecyclerView.ViewHolder {
        private static final String TAG = "ItemViewHolder";

        View root;
        TextView tvTitle;

        public ItemViewHolder(View itemView) {
            super(itemView);
            root = itemView;

            tvTitle = itemView.findViewById(R.id.text_title);

//            selectionColor = itemView.findViewById(R.id.selectioncolor);
        }

        public void bind(TRecentSearchSong tRecentSearchSong, int pos) {
            try {
                tvTitle.setText(tRecentSearchSong.recentSong);

////            untuk merubah warna selection
//                if (colorSelection != null) root.setBackgroundTintList(colorSelection);
//
////            untuk merubah warna text
//                if (colorText != null) tvTitle.setTextColor(colorText);


                root.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        try {
                            selectView(v);
                            selectedItem = pos;
//                        mListener.onPlaylistListClick(tRecentSearchSong.playlistId, tRecentSearchSong.playlistName);
                            Intent returnIntent = new Intent();
                            returnIntent.putExtra(INTENT_RESULT, tRecentSearchSong.recentSong);
                            setResult(Activity.RESULT_OK, returnIntent);
                            finish();
                        }catch (Exception ex)
                        {
                            Debug.e(TAG, ex);
                        }
                    }
                });
            } catch (Exception ex) {
                Debug.e(TAG, ex);
            }
        }

        void selectView(View v) {
            //clear selection
            if (selectedView != null) {
                selectedView.setSelected(false);
            }

            //make selection
            selectedView = v;
            selectedView.setSelected(true);
        }
    }

    ColorStateList colorSelection, colorText;

    public static boolean checkRequestCode(int reqCode){
        return reqCode == ACTIVITY_REQUEST_CODE;
    }

}

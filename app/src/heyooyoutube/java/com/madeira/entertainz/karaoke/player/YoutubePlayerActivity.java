package com.madeira.entertainz.karaoke.player;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;
import android.net.Uri;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.method.ArrowKeyMovementMethod;
import android.text.method.MovementMethod;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.Transformation;
import android.view.animation.TranslateAnimation;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.madeira.entertainz.helper.controller.media.VideoControllerView;
import com.madeira.entertainz.helper.equalizer.PopupEQ;
import com.madeira.entertainz.karaoke.DBLocal.TSong;
import com.madeira.entertainz.karaoke.DBLocal.TSongYoutube;
import com.madeira.entertainz.karaoke.Debug;
import com.madeira.entertainz.karaoke.R;
import com.madeira.entertainz.karaoke.config.Global;
import com.madeira.entertainz.library.Util;
import com.pierfrancescosoffritti.youtubeplayer.player.AbstractYouTubePlayerListener;
import com.pierfrancescosoffritti.youtubeplayer.player.PlayerConstants;
import com.pierfrancescosoffritti.youtubeplayer.player.YouTubePlayer;
import com.pierfrancescosoffritti.youtubeplayer.player.YouTubePlayerInitListener;
import com.pierfrancescosoffritti.youtubeplayer.player.YouTubePlayerListener;
import com.pierfrancescosoffritti.youtubeplayer.player.YouTubePlayerView;
import com.pierfrancescosoffritti.youtubeplayer.ui.PlayerUIController;

import java.io.File;
import java.net.MalformedURLException;
import java.util.ArrayList;


public class YoutubePlayerActivity extends AppCompatActivity {
    String TAG = "PlayerActivity";

    private static final int LYRIC_SCROLL_SIZE = 30;

    private static final int ANIM_DURATION_PLAYBACK_CONTROL = 200;
    private static final int ANIM_DURATION_LYIRC = 200;

    private static final int DURATION_AUTOHIDE_PLAYBACK_CONTROL = 3000; //3 second

    YouTubePlayerView youTubePlayerView;
    private YouTubePlayer youtubePlayer;
    Boolean isMenuVisible = true;
    Boolean playerReady = false;
    Float currentSecond = Float.valueOf(0);
    String youtubeId = "";
    private static final int RQS_ErrorDialog = 1;
    public static String KEY_TSONG = "KEY_TSONG";
    public static String KEY_LASTPOSITION = "KEY_LASTPOSITION";

    ArrayList<TSongYoutube> songArrayList = new ArrayList<>();
    boolean isMute = true;
    static TSongYoutube tSong = null;
    static int lastPosition = 0;
    TextView title;
    TextView artist;
    TextView duration;
    static LinearLayout controlVocalLL;
    static ImageView muteIV;
    static ImageView unMuteIV;

    Runnable runCountDown;
    Handler handler;
    static int tempDuration = 0;
    private boolean mIsComplete;
    PopupEQ eq;
    RelativeLayout rootRL;

    boolean isPlaybackControlVisible = false;
    RelativeLayout controllerRL;
    ImageButton prevSecButton;
    ImageButton prevButton;
    ImageButton playButton;
    ImageButton nextButton;
    ImageButton nextSecButton;
    ImageButton equalizerButton;

    private static final int interval = 5;
    int lastState = -100;
    boolean youtubeReady = false;
    LinearLayout layout_bottom;
    TextView bottom_time_current;
    SeekBar bottom_seekbar;
    TextView bottom_time;

    boolean isLyricVisible = false;
    ViewGroup containerLyric;
    ImageView imageViewScrollDown;
    ImageView imageViewScrollUp;
    ImageView imageViewShowHide;

    PlayerUIController uiController;

    HandlerUiLyric handlerUiLyric;

    public static void startActivity(Activity activity, ArrayList<TSongYoutube> tSongArrayList, int prmLastPosition) {
        Intent intent = new Intent(activity, YoutubePlayerActivity.class);

        intent.putExtra(KEY_TSONG, tSongArrayList);
        intent.putExtra(KEY_LASTPOSITION, prmLastPosition);
        activity.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        try {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_youtube_player);
            songArrayList = (ArrayList<TSongYoutube>) getIntent().getSerializableExtra(KEY_TSONG);
            lastPosition = getIntent().getIntExtra(KEY_LASTPOSITION, 0);
            bind();

            handlerUiLyric = new HandlerUiLyric(containerLyric);

            setActionObject();
            if (songArrayList.size() != 0) {
//            playSong(songArrayList, lastPosition);
                playSongWithLibrary(songArrayList, lastPosition);
            }
            Util.hideNavigationBar(YoutubePlayerActivity.this);

            String languageId = Global.getLanguageId();
            Util.setLanguage(this, languageId);

            setupYoutubePlayerUi();

        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    void bind() {
        title = findViewById(R.id.title);
        artist = findViewById(R.id.artist);
        duration = findViewById(R.id.duration);
        controlVocalLL = findViewById(R.id.controlVocalLL);
        muteIV = findViewById(R.id.muteIV);
        unMuteIV = findViewById(R.id.unMuteIV);
        rootRL = findViewById(R.id.rootRL);
        youTubePlayerView = findViewById(R.id.youtube_player_view);

        controllerRL = findViewById(R.id.controllerRL);
        prevSecButton = findViewById(R.id.prevSecButton);
        prevButton = findViewById(R.id.prevButton);
        playButton = findViewById(R.id.playButton);
        nextButton = findViewById(R.id.nextButton);
        nextSecButton = findViewById(R.id.nextSecButton);
        equalizerButton = findViewById(R.id.equalizerButton);
        layout_bottom = findViewById(R.id.layout_bottom);
        bottom_time_current = findViewById(R.id.bottom_time_current);
        bottom_seekbar = findViewById(R.id.bottom_seekbar);
        bottom_time = findViewById(R.id.bottom_time);
        playButton.setBackgroundResource(R.drawable.selector_play);

        containerLyric = findViewById(R.id.container_lyric);
        imageViewScrollUp = findViewById(R.id.image_view_scroll_up);
        imageViewScrollDown = findViewById(R.id.image_view_scroll_down);
        imageViewShowHide = findViewById(R.id.image_view_showhide);


    }

    void setActionObject() {
        try {
            prevSecButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (youtubePlayer != null) {
                        float seekTo = currentSecond - interval;
                        youtubePlayer.seekTo(seekTo);
                    }
                }
            });

            prevButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (songArrayList.size() <= lastPosition + 1)
                        lastPosition = lastPosition - 1;
                    else
                        lastPosition = 0;
               /* if (lastPosition <= songArrayList.size() - 1) {
                    lastPosition = 0;
                } else
                    lastPosition = lastPosition - 1;*/
//                                            youtubePlayer.release();
                    playSongWithLibrary(songArrayList, lastPosition);
                }
            });

            playButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (youtubePlayer != null) {
                        if (lastState == 2) {
                            youtubePlayer.play();
                            playButton.setBackgroundResource(R.drawable.selector_pause);
                        } else if (lastState != 0) {
                            youtubePlayer.pause();
                            playButton.setBackgroundResource(R.drawable.selector_play);
                        }
                    }
                }
            });

            nextButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (lastPosition == songArrayList.size() - 1) {
                        lastPosition = 0;
                    } else
                        lastPosition = lastPosition + 1;
//                                            youtubePlayer.release();
                    playSongWithLibrary(songArrayList, lastPosition);
                }
            });

            nextSecButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (youtubePlayer != null) {
                        float seekTo = currentSecond + interval;
                        youtubePlayer.seekTo(seekTo);
                    }
                }
            });

            equalizerButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                }
            });
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    private void setupYoutubePlayerUi(){

        uiController = youTubePlayerView.getPlayerUIController();

        uiController.showCurrentTime(true);
        uiController.showDuration(true);
        uiController.showSeekBar(true);

        uiController.showMenuButton(false);
        uiController.showPlayPauseButton(false);
        uiController.showYouTubeButton(false);

    }

    public void playSongWithLibrary(ArrayList<TSongYoutube> prmTSongList, int position) {
        try {
            youtubeReady = false;
            lastPosition = position;
            tSong = prmTSongList.get(position);
            title.setText(tSong.songName);
            artist.setText(tSong.artist);
            tempDuration = Integer.valueOf(tSong.duration) * 1000;
            duration.setText(Util.convertMilisToTimeWithText(tempDuration));

            String lyric = tSong.lyric;//"Tetesan air mata jatuh lagi\r\n---Ingat kau di sana, kau tertawa, kau bahagia\r\n---Cobalah sejenak sabar menanti\r\n---Tunggu aku pulang bawa sebuah penawar lara\r\n\r\n---'Ku lakukan semua ini untukmu\r\n---Agar kita bahagia untuk selamanya\r\n---Aku lelah, lelah 'ku terbiasa\r\n---Sakit 'ku terbiasa, semua untuk kamu\r\n---'Tak mengapa peluhku bercucuran\r\n---Sakit pun 'ku acuhkan, semua demi kamu\r\n---'Ku lakukan semua ini untukmu\r\n---Agar kita bahagia, bahagia kita bersama, oh\r\n---Aku lelah, lelah 'ku terbiasa\r\n---Sakit 'ku terbiasa, semua… END";

            //empty string apabila lyric null
            if (lyric==null) lyric = "";

//            lyric = lyric.replaceAll("\r\n", "\n"); //rubah /r/n jadi /n
//            lyric = lyric.replaceAll("\n", "\n\n"); //rubah single /n jadi /n/n, agar ada jarak antara 1 bait dgn bait selanjutnya

            handlerUiLyric.setLyric(lyric);

            if (prmTSongList != null) {
                try {
                    youtubeId = Util.extractYoutubeId(tSong.songURL);

                    //custom player ui
//                    playerUi = youTubePlayerView.inflateCustomPlayerUI(R.layout.youtube_player_ui);

                    youTubePlayerView.initialize(new YouTubePlayerInitListener() {
                        @Override
                        public void onInitSuccess(final com.pierfrancescosoffritti.youtubeplayer.player.YouTubePlayer initializedYouTubePlayer) {
                            initializedYouTubePlayer.addListener(new AbstractYouTubePlayerListener() {
                                @Override
                                public void onReady() {
                                    bottom_time.setText(Util.convertMilisToTime2(Integer.valueOf(tSong.duration)* 1000));
                                    bottom_seekbar.setMax(Integer.valueOf(tSong.duration));
                                    youtubeReady =true;
                                    youtubePlayer = initializedYouTubePlayer;
                                    youTubePlayerView.enterFullScreen();

                                    //pindahin dari bawah ke atas, biar lebih kelihatan proses initialisasinya
                                    initializedYouTubePlayer.loadVideo(youtubeId, 0);

                                    //ini utk capture event
                                    youtubePlayer.addListener(new YouTubePlayerListener() {
                                        @Override
                                        public void onReady() {

                                        }

                                        @Override
                                        public void onStateChange(int state) {
                                            lastState = state;
                                            if (state == 0) {
                                          /*  controlBar.setVisibility(View.VISIBLE);
                                            btnPlay.setVisibility(View.GONE);
                                            btnPause.setVisibility(View.GONE);
                                            btnReLoad.setVisibility(View.VISIBLE);
                                            btnReLoad.requestFocus();*/
                                                if (lastPosition == prmTSongList.size() - 1) {
                                                    lastPosition = 0;
                                                } else
                                                    lastPosition = lastPosition + 1;
//                                            youtubePlayer.release();
                                                playSongWithLibrary(prmTSongList, lastPosition);
                                                mIsComplete = true;
                                            } else if (state == 2)
                                                playButton.setBackgroundResource(R.drawable.selector_play);
                                            else
                                                playButton.setBackgroundResource(R.drawable.selector_pause);


                                        }

                                        @Override
                                        public void onPlaybackQualityChange(@NonNull String playbackQuality) {

                                        }

                                        @Override
                                        public void onPlaybackRateChange(@NonNull String playbackRate) {

                                        }

                                        @Override
                                        public void onError(int error) {

                                        }

                                        @Override
                                        public void onApiChange() {

                                        }

                                        @Override
                                        public void onCurrentSecond(float second) {
                                            currentSecond = second;
                                            int secondInt = (int)second;
                                            bottom_seekbar.setProgress(secondInt);
                                            bottom_time_current.setText(Util.convertMilisToTime2(secondInt* 1000));
                                            playerReady = true;
                                        }

                                        @Override
                                        public void onVideoDuration(float duration) {
                                        }

                                        @Override
                                        public void onVideoLoadedFraction(float loadedFraction) {

                                        }

                                        @Override
                                        public void onVideoId(@NonNull String videoId) {

                                        }
                                    });

                                }

                            });


                        }
                    }, true);

                    youTubePlayerView.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                        @Override
                        public void onFocusChange(View v, boolean hasFocus) {
                            if (hasFocus) {
                                /*if (controllerRL.getVisibility() == View.GONE) {
                                    controllerRL.setVisibility(View.VISIBLE);
                                    playButton.requestFocus();
                                }*/
                            } else {

                            }
                        }
                    });
                    youTubePlayerView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            animHideLyric();

                            animShowPlaybackControl();
                            startAutoHidePlaybackControl();


//                            Log.i(TAG, "onClick: ");
//
//                            if (controllerRL.getVisibility() == View.GONE) {
//                                controllerRL.setVisibility(View.VISIBLE);
//                                layout_bottom.setVisibility(View.VISIBLE);
//                                playButton.requestFocus();
//                            }
                        }
                    });




                } catch (MalformedURLException e) {
                    e.printStackTrace();
                }
            }
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    public void stopKaraoke() {
//        if (videoView.isPlaying())
//            videoView.stopPlayback();
    }

    @Override
    public void onBackPressed() {

        //hide playback control apabila back button di tekan
        if(isPlaybackControlVisible){
            animHidePlaybackControl();
            return;
        }

//        if (controllerRL.getVisibility() == View.VISIBLE) {
//            controllerRL.setVisibility(View.GONE);
//            layout_bottom.setVisibility(View.GONE);
//            return;
//        }

        if (youTubePlayerView != null) {
            youTubePlayerView.release();
        }
        super.onBackPressed();

    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {

        switch (keyCode) {
            case KeyEvent.KEYCODE_DPAD_UP:
            case KeyEvent.KEYCODE_DPAD_DOWN:
                if (isLyricVisible){
                    handlerUiLyric.onKeyDown(keyCode, event);
                    return true;
                }
                break;

//            case KeyEvent.KEYCODE_DPAD_UP:
//                if (isLyricVisible){
//                    scrollLyricUp();
//                }
//                break;

//            case KeyEvent.KEYCODE_DPAD_DOWN:
//                if (isLyricVisible){
//                    scrollLyricDown();
//                }
//                break;

            case KeyEvent.KEYCODE_DPAD_LEFT:
            case KeyEvent.KEYCODE_DPAD_RIGHT:
                if (isPlaybackControlVisible){
                    startAutoHidePlaybackControl();
                }
                break;
        }

        return super.onKeyDown(keyCode, event);
    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {

        switch (keyCode){

            case KeyEvent.KEYCODE_DPAD_RIGHT:

                //right arrow, utk show/hide lyric, apabila playback control tdk show
                if (isPlaybackControlVisible==false){
                    if (isLyricVisible){
                        animHideLyric();
                    } else{
                        animShowLyric();
                    }
                    return true;
                }
                break;
        }

//        Util.hideNavigationBar(YoutubePlayerActivity.this);
        return super.onKeyUp(keyCode, event);
    }

    private void showPlaybackControl(){
        isPlaybackControlVisible = true;

        controllerRL.setVisibility(View.VISIBLE);
        layout_bottom.setVisibility(View.VISIBLE);

        playButton.requestFocus();
    }

    private void hidePlaybackControl(){
        isPlaybackControlVisible = false;

        controllerRL.setVisibility(View.GONE);
        layout_bottom.setVisibility(View.GONE);
    }

    private void showLyric(){
        containerLyric.setVisibility(View.VISIBLE);
        isLyricVisible = true;
    }

    private void hideLyric(){
        containerLyric.setVisibility(View.INVISIBLE);
        isLyricVisible = false;
    }


    final int screenWidth  = Resources.getSystem().getDisplayMetrics().widthPixels;
    int[] location = new int[2];

    /**
     * Animate show lyric dari kanan ke kiri
     */
    private void animShowLyric(){

        //exit apabila lyric sdh show
        if (isLyricVisible) return;

        isLyricVisible = true;

        containerLyric.getLocationOnScreen(location); //posisi containerlyric di layar

        //cari width posisi containerlyric dgn sisi kanan screen
        float initialPos = screenWidth - location[0];

        Animation animation = new TranslateAnimation(initialPos, 0,0, 0);
        animation.setDuration(ANIM_DURATION_LYIRC);
        animation.setFillAfter(false);

        containerLyric.startAnimation(animation);
        containerLyric.setVisibility(View.VISIBLE);
    }

    /**
     * Animate hide lyric dari kiri ke kanan
     */
    private void animHideLyric(){

        //exit apabila lyric sdh hide
        if (isLyricVisible==false) return;

        isLyricVisible = false;

        float finalPos = screenWidth - location[0];

        Animation animation = new TranslateAnimation(0, finalPos,0, 0);
        animation.setDuration(ANIM_DURATION_LYIRC);
        animation.setFillAfter(false);

        containerLyric.startAnimation(animation);
        containerLyric.setVisibility(View.INVISIBLE);
    }


    /**
     * Animate show playback control, fade in
     */
    private void animShowPlaybackControl(){

        try {
            //exit apabila playback control sdh show
            if (isPlaybackControlVisible) return;

            isPlaybackControlVisible = true;

            Animation animation = new AlphaAnimation(0f, 1f);
            animation.setDuration(ANIM_DURATION_PLAYBACK_CONTROL);
            animation.setFillAfter(false);

            controllerRL.startAnimation(animation);

            controllerRL.setVisibility(View.VISIBLE);
//        layout_bottom.setVisibility(View.VISIBLE);

            if (uiController != null) uiController.showUI(true);

            playButton.requestFocus();
        }catch (Exception ex)
        {
            Debug.e(TAG, ex);
        }
    }

    /**
     * Animate hide playback control, fade out
     */
    private void animHidePlaybackControl(){

        try {
            //exit apabila playback control sdh hide
            if (isPlaybackControlVisible == false) return;

            isPlaybackControlVisible = false;

            Animation animation = new AlphaAnimation(1f, 0);
            animation.setDuration(ANIM_DURATION_PLAYBACK_CONTROL);
            animation.setFillAfter(false);

            controllerRL.startAnimation(animation);

            controllerRL.setVisibility(View.INVISIBLE);
            layout_bottom.setVisibility(View.INVISIBLE);
        }catch (Exception ex)
        {
            Debug.e(TAG, ex);
        }
    }

    /**
     * Start delay hide dalam waktu yg di tetapkan
     * fungsi ini bisa di call berkali2, previous run akan di cancel.
     */
    private void startAutoHidePlaybackControl(){

        try {
            //create handler apabila blm ada
            if (handler == null) {
                handler = new Handler();
            }

            //cancell all previous run
            cancelAutoHidePlaybackControl();

            //
            handler.postDelayed(runAutoHidePlaybackControl, DURATION_AUTOHIDE_PLAYBACK_CONTROL);
        }
        catch (Exception ex)
        {
            Debug.e(TAG, ex);
        }
    }

    private void cancelAutoHidePlaybackControl(){
        try {
            if (handler == null) return;

            //remove all previous run
            handler.removeCallbacks(runAutoHidePlaybackControl);
        }catch (Exception ex)
        {
            Debug.e(TAG, ex);
        }
    }

    Runnable runAutoHidePlaybackControl = new Runnable() {
        @Override
        public void run() {
            animHidePlaybackControl();
        }
    };

    @Override
    public void onDestroy()
    {
        try
        {
            super.onDestroy();
            Util.freeMemory();
        }
        catch (Exception ex)
        {
            Debug.e(TAG, ex);
        }
    }


    /**
     * class ini utk handle ui lyric saat up dan down
     */
    class HandlerUiLyric {
        private static final String TAG = "HandlerUiLyric";
        private static final int FOCUS_BACKGROUND_COLOR = 0x20FFFFFF;
        private static final int MAX_LINE = 7;
        private static final int MIDDLE_LINE = 4;

        TextView lyricLine1;
        TextView lyricLine2;
        TextView lyricLine3;
        TextView lyricLine4;
        TextView lyricLine5;
        TextView lyricLine6;
        TextView lyricLine7;

        TextView textViewFocus; //line yg dalam focus

        String[] arLine = new String[] { }; //create empty array

        int linePos; //start dari 0
        int cursorPos; //start dari 1

        public HandlerUiLyric(View root){
            bind(root);
        }

        public void setLyric(String lyric) {

            if (lyric==null || lyric.length()==0) return;

            lyric = lyric.replaceAll("\r\n", "\n");

            arLine = lyric.split("\n");

            ArrayList<String> listNew = new ArrayList<>();

            int idx = 0;
            while (idx<(arLine.length)){
                String text = arLine[idx];
                text = text.replaceAll("\n", ""); //hapus semua \n
                if (text.length()>0) listNew.add(text);
                idx++;
            }

            arLine = listNew.toArray(new String[0]);

            clear();
            update();
        }

        private void bind(View root) {
            lyricLine1 = root.findViewById(R.id.lyric_line_1);
            lyricLine2 = root.findViewById(R.id.lyric_line_2);
            lyricLine3 = root.findViewById(R.id.lyric_line_3);
            lyricLine4 = root.findViewById(R.id.lyric_line_4);
            lyricLine5 = root.findViewById(R.id.lyric_line_5);
            lyricLine6 = root.findViewById(R.id.lyric_line_6);
            lyricLine7 = root.findViewById(R.id.lyric_line_7);
        }

        public boolean onKeyDown(int keyCode, KeyEvent event) {

            if (arLine.length==0) return false;

            switch (keyCode){
                case KeyEvent.KEYCODE_DPAD_UP:
                    scrollUp();
                    return true;

                case KeyEvent.KEYCODE_DPAD_DOWN:
                    scrollDown();
                    return true;
            }

            return false;
        }

        private void clear(){
             lyricLine1.setText(null);
             lyricLine2.setText(null);
             lyricLine3.setText(null);
             lyricLine4.setText(null);
             lyricLine5.setText(null);
             lyricLine6.setText(null);
             lyricLine7.setText(null);
        }

        private void update(){
            if (arLine.length==0) return;

            if ((arLine.length-linePos)>0) lyricLine1.setText(arLine[linePos]);
            if ((arLine.length-linePos)>1) lyricLine2.setText(arLine[linePos+1]);
            if ((arLine.length-linePos)>2) lyricLine3.setText(arLine[linePos+2]);
            if ((arLine.length-linePos)>3) lyricLine4.setText(arLine[linePos+3]);
            if ((arLine.length-linePos)>4) lyricLine5.setText(arLine[linePos+4]);
            if ((arLine.length-linePos)>5) lyricLine6.setText(arLine[linePos+5]);
            if ((arLine.length-linePos)>6) lyricLine7.setText(arLine[linePos+6]);

            setFocus(cursorPos);
        }

        private void setFocus(int cursorPos){
            if (textViewFocus!=null) textViewFocus.setBackgroundColor(0);

            if (cursorPos<1) cursorPos=1;
            if (cursorPos>7) cursorPos=7;

            switch (cursorPos){
                case 1:
                    textViewFocus = lyricLine1;
                    break;
                case 2:
                    textViewFocus = lyricLine2;
                    break;
                case 3:
                    textViewFocus = lyricLine3;
                    break;
                case 4:
                    textViewFocus = lyricLine4;
                    break;
                case 5:
                    textViewFocus = lyricLine5;
                    break;
                case 6:
                    textViewFocus = lyricLine6;
                    break;
                case 7:
                    textViewFocus = lyricLine7;
                    break;
            }

            textViewFocus.setBackgroundColor(FOCUS_BACKGROUND_COLOR);
        }

        private void scrollUp(){
            int delta = arLine.length - linePos;
            int numberOfLineLeft = delta;

            if (linePos==0){
                cursorPos--;
                if (cursorPos<1) cursorPos=1;
                update();
                return;
            }

            if (cursorPos==MIDDLE_LINE){
                linePos--;
                update();
                return;
            }

            cursorPos--;
            if (cursorPos<1) cursorPos=1;
            update();
        }

        private void scrollDown(){
            int delta = arLine.length - linePos;
            int numberOfLineLeft = delta;

            //apabila numberOfLine <= MAX_LINE artinya tdk boleh scroll lagi
            if (numberOfLineLeft<=MAX_LINE){
                cursorPos++;
                if (cursorPos>delta) cursorPos = delta;
                update();
                return;
            }

            if (cursorPos==MIDDLE_LINE){
                linePos++;
                update();
                return;
            }

            cursorPos++;
            update();
        }

    }
}

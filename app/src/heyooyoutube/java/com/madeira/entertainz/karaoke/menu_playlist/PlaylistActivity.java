package com.madeira.entertainz.karaoke.menu_playlist;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.GradientDrawable;
import android.net.Uri;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.madeira.entertainz.karaoke.DBLocal.JoinSettingElementMedia;
import com.madeira.entertainz.karaoke.DBLocal.JoinSongCountPlayed;
import com.madeira.entertainz.karaoke.DBLocal.TPlaylist;
import com.madeira.entertainz.karaoke.DBLocal.TSticker;
import com.madeira.entertainz.karaoke.Debug;
import com.madeira.entertainz.karaoke.R;
import com.madeira.entertainz.karaoke.config.C;
import com.madeira.entertainz.karaoke.config.Global;
import com.madeira.entertainz.karaoke.menu_karaoke.KaraokeActivity;
import com.madeira.entertainz.karaoke.menu_main.BottomNavigationFragment;
import com.madeira.entertainz.karaoke.menu_main.ListMoodFragment;
import com.madeira.entertainz.karaoke.menu_navigation.TopNavigationFragment;
import com.madeira.entertainz.karaoke.model_room_db.ModelElementMedia;
import com.madeira.entertainz.karaoke.model_room_db.ModelPlaylist;
import com.madeira.entertainz.library.Util;

import net.yslibrary.android.keyboardvisibilityevent.KeyboardVisibilityEvent;
import net.yslibrary.android.keyboardvisibilityevent.KeyboardVisibilityEventListener;

import java.io.File;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import es.dmoral.toasty.Toasty;

import static com.madeira.entertainz.karaoke.config.CacheData.lastIndexBackground;

public class PlaylistActivity extends AppCompatActivity implements PlaylistTheme.IPlaylistThemeListener {
    String TAG = "PlaylistActivity";
    ImageView backgroundIV;
    LinearLayout blankPlaylistLL;
    Button playlistIdButton;
    int playlistId = 0;
    View listMoodView;

    TopNavigationFragment topNavigationFragment = new TopNavigationFragment();
    BottomNavigationFragment bottomNavigationFragment = new BottomNavigationFragment();
    PlaylistListFragment playlistListFragment = new PlaylistListFragment();
    PlaylistDetailFragment playlistDetailFragment = new PlaylistDetailFragment();
    ListMoodFragment listMoodFragment = new ListMoodFragment();
    //    KaraokePopupFragment karaokePopupFragment = new KaraokePopupFragment();
    Handler handler = new Handler();
    Runnable runnable;

    PlaylistTheme playlistTheme;


    public static void startActivity(Activity activity, int playlistId) {
        Intent intent = new Intent(activity, PlaylistActivity.class);
        intent.putExtra(KaraokeActivity.KEY_PLAYLIST_ID, playlistId);
        activity.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        try {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_playlist);
            bind();
            playlistTheme = new PlaylistTheme(this, this);
            playlistTheme.setTheme();
            setActionObject();
            fetchListBackground();
            bindFragment();
            checkPlaylist();
            Util.hideNavigationBar(PlaylistActivity.this);
            playlistId = getIntent().getIntExtra(KaraokeActivity.KEY_PLAYLIST_ID, 0);
            KeyboardVisibilityEvent.setEventListener(
                    this,
                    new KeyboardVisibilityEventListener() {
                        @Override
                        public void onVisibilityChanged(boolean isOpen) {
                            // some code depending on keyboard visiblity status
                            if (isOpen) {
                                Toasty.info(PlaylistActivity.this, getString(R.string.pressBackToHideKeyBoard)).show();
                            }

                        }
                    });

            String languageId = Global.getLanguageId();
            Util.setLanguage(this, languageId);
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    void bind() {
        backgroundIV = (ImageView) findViewById(R.id.backgroundIV);
        blankPlaylistLL = findViewById(R.id.blankPlaylistLL);
        listMoodView = findViewById(R.id.listMoodFrameLayout);
        playlistIdButton = findViewById(R.id.playlistIdButton);
    }

    void setActionObject() {
        playlistIdButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showAddPlaylist();
            }
        });
    }

    /**
     * untuk check ada playlist atau tidak, jika ada munculkan fragment
     */

    void checkPlaylist() {
        ModelPlaylist.getPlaylist(PlaylistActivity.this, new ModelPlaylist.IGetPlaylist() {
            @Override
            public void onGetListPlaylist(List<TPlaylist> tPlaylistList) {
                if (tPlaylistList.size() == 0)
                    hideFragment(true);
                else
                    hideFragment(false);
            }
        });
    }

    void bindFragment() {
        try {
            topNavigationFragment = topNavigationFragment.newInstance(C.MENU_PLAYLIST);
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.topNavFrameLayout, topNavigationFragment);
            ft.replace(R.id.bottomFrameLayout, bottomNavigationFragment);
            ft.replace(R.id.listMoodFrameLayout, listMoodFragment);

            playlistId = getIntent().getIntExtra(KaraokeActivity.KEY_PLAYLIST_ID, 0);
            Bundle bundle = new Bundle();
            bundle.putInt(KaraokeActivity.KEY_PLAYLIST_ID, playlistId);
            playlistListFragment.setArguments(bundle);
            ft.replace(R.id.categoryFrameLayout, playlistListFragment).addToBackStack(null);

            ft.commit();
            playlistListFragment.setCallback(iPlaylistListener);
            bottomNavigationFragment.setCallback(bottomNavigationFragmentListener);
            listMoodFragment.setCallBack(listMoodListener);
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    void fetchListBackground() {
        ModelElementMedia.getListElementMediaActive(this, new ModelElementMedia.IGetListElementMediaActive() {
            @Override
            public void onGetListElementMedia(List<JoinSettingElementMedia> joinSettingElementMediaList) {
                int totalIndex = joinSettingElementMediaList.size();
                runnable = new Runnable() {
                    @Override
                    public void run() {
                        try {
                            if (joinSettingElementMediaList.size() != 0) {
                                if (lastIndexBackground >= totalIndex)
                                    lastIndexBackground = 0;

                                JoinSettingElementMedia joinSettingElementMedia = joinSettingElementMediaList.get(lastIndexBackground);
                                Uri uri = Uri.fromFile(new File(joinSettingElementMedia.urlImage));
//                            Picasso.with(KaraokeActivity.this).load(uri).into(backgroundIV);
                                InputStream inputStream = getContentResolver().openInputStream(uri);
                                BitmapFactory.Options options = new BitmapFactory.Options();
                                options.inPreferredConfig = Bitmap.Config.RGB_565;
                                Bitmap image = BitmapFactory.decodeStream(inputStream, null, options);
                                backgroundIV.setImageBitmap(image);

                                lastIndexBackground++;
                                handler.postDelayed(this::run, joinSettingElementMedia.duration);
                            }
                        } catch (Exception ex) {
                            Debug.e(TAG, ex);
                        }
                    }
                };
                handler.postDelayed(runnable, 100);
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        try {
            Util.hideNavigationBar(this);

            //agar runable tidak running ketika di finish activity
            handler.removeCallbacks(runnable);
            handler.removeCallbacksAndMessages(null);
            finish();

        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    PlaylistListFragment.IPlaylistListener iPlaylistListener = new PlaylistListFragment.IPlaylistListener() {
        @Override
        public void onPlaylistListClick(int playlistId, String playlistName) {
            try {
                //detach first, for refresh fragment

                playlistDetailFragment = new PlaylistDetailFragment();
                Bundle args = new Bundle();
                args.putInt(PlaylistDetailFragment.KEY_PLAYLIST_ID, playlistId);
                args.putString(PlaylistDetailFragment.KEY_PLAYLIST_NAME, playlistName);
                playlistDetailFragment.setArguments(args);
                FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                ft.replace(R.id.songFrameLayout, playlistDetailFragment).addToBackStack(null);
                ft.commit();
                playlistDetailFragment.setCallback(iPlaylistDetailListener);
            } catch (Exception ex) {
                Debug.e(TAG, ex);
            }
        }
    };

    PlaylistDetailFragment.IPlaylistDetailListener iPlaylistDetailListener = new PlaylistDetailFragment.IPlaylistDetailListener() {

        @Override
        public void onListClick(ArrayList<JoinSongCountPlayed> tSongs, int position) {

        }

        @Override
        public void onDeleteOrRenamePlaylist() {
            try {
                PlaylistActivity.startActivity(PlaylistActivity.this, 0);
                finish();
            } catch (Exception ex) {
                Debug.e(TAG, ex);
            }
        }
    };

    public void onResume() {
        super.onResume();
        Util.hideNavigationBar(PlaylistActivity.this);
    }

    void hideFragment(boolean prmHide) {
        try {
            View playlistView = findViewById(R.id.categoryFrameLayout);
            View playlistDtView = findViewById(R.id.songFrameLayout);
            if (prmHide) {
                playlistDtView.setVisibility(View.GONE);
                playlistView.setVisibility(View.GONE);
                blankPlaylistLL.setVisibility(View.VISIBLE);
                playlistIdButton.requestFocus();

            } else {
                playlistDtView.setVisibility(View.VISIBLE);
                playlistView.setVisibility(View.VISIBLE);
                blankPlaylistLL.setVisibility(View.GONE);
            }
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    void showAddPlaylist() {
        try {
            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(PlaylistActivity.this);
            LayoutInflater inflater = this.getLayoutInflater();
            View dialogView = inflater.inflate(R.layout.alert_add_playlist, null);
            dialogBuilder.setView(dialogView);
            AlertDialog alertDialog = dialogBuilder.create();
            EditText playlistET = (EditText) dialogView.findViewById(R.id.playlistET);
            Button saveButton = (Button) dialogView.findViewById(R.id.saveButton);
            Button cancelButton = (Button) dialogView.findViewById(R.id.cancelButton);
            saveButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (!TextUtils.isEmpty(playlistET.getText().toString())) {
                        savePlaylist(playlistET.getText().toString());
                        alertDialog.dismiss();
                    } else {
                        playlistET.setError(getString(R.string.playlistMustFilled));
                        playlistET.requestFocus();
                    }
                }
            });


            cancelButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    alertDialog.dismiss();
                }
            });


            alertDialog.show();
            WindowManager.LayoutParams layoutParams = alertDialog.getWindow().getAttributes();
            layoutParams.width = ViewGroup.LayoutParams.WRAP_CONTENT;
            alertDialog.show();
            alertDialog.getWindow().setLayout(530, WindowManager.LayoutParams.WRAP_CONTENT);
            Util.hideNavigationBar(PlaylistActivity.this);
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    void savePlaylist(String playlistname) {
        ModelPlaylist.savePlaylist(PlaylistActivity.this, playlistname, new ModelPlaylist.ISavePlaylist() {
            @Override
            public void onSavePlaylist(int result) {
                try {
                    int prmLastPlaylistId = (int) result;
                    if (prmLastPlaylistId == 0) {
                        Toasty.error(PlaylistActivity.this, R.string.failedAddPlaylist).show();
                    } else {
                        PlaylistActivity.startActivity(PlaylistActivity.this, prmLastPlaylistId);
                        finish();
                    }
                } catch (Exception ex) {
                    Debug.e(TAG, ex);
                }
            }
        });
    }

    @Override
    public void setThemeUpdateDate(Date lastUpdateTheme) {

    }

    @Override
    public void setListColorTheme(int color) {
        try {
            blankPlaylistLL.setBackgroundResource(R.drawable.tags_rounded_corners);

            GradientDrawable categoryDrawable = (GradientDrawable) blankPlaylistLL.getBackground();
            categoryDrawable.setColor(color);


        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    @Override
    public void setTextAndSelectionColorTheme(ColorStateList colorSelection, ColorStateList colorText, int colorTextSelected, int colorTextFocus, int colorTextNormal, int colorSelectionSelected, int colorSelectionFocus) {

    }

    /**
     * listener ketika mood di click
     */
    BottomNavigationFragment.IMoodFragmentListener bottomNavigationFragmentListener = new BottomNavigationFragment.IMoodFragmentListener() {
        @Override
        public void onMoodClick() {
            listMoodView.setVisibility(View.VISIBLE);
        }
    };

    /**
     * listener ketika mood dipilih
     */
    ListMoodFragment.IListMoodListener listMoodListener = new ListMoodFragment.IListMoodListener() {
        @Override
        public void onClick(TSticker tSticker) {
            try {
                listMoodView.setVisibility(View.GONE);
                bottomNavigationFragment.updateMood(tSticker);
                Global.setStickerId(tSticker.stickerId);
            } catch (Exception ex) {
                Debug.e(TAG, ex);
            }
        }
    };

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
//        super.onActivityResult(requestCode, resultCode, data);

        //ugly hack, hack ini dibutuhkan, krn fragment tdk menerima event ini.
        //shg harus di pass dari activity

        //pass event ini ke fragment
        if (playlistDetailFragment != null)
            playlistDetailFragment.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onDestroy() {
        try {
            super.onDestroy();
            Util.freeMemory();
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }
}

package com.madeira.entertainz.karaoke.menu_youtube;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.SparseArray;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.madeira.entertainz.helper.RecyclerViewAdapter;
import com.madeira.entertainz.karaoke.DBLocal.DbAccess;
import com.madeira.entertainz.karaoke.DBLocal.TDownloadYoutube;
import com.madeira.entertainz.karaoke.DBLocal.TSongCategory;
import com.madeira.entertainz.karaoke.DBLocal.TSongItemServer;
import com.madeira.entertainz.karaoke.Debug;
import com.madeira.entertainz.karaoke.DownloadSongService;
import com.madeira.entertainz.karaoke.DownloadYoutubeService;
import com.madeira.entertainz.karaoke.R;
import com.madeira.entertainz.karaoke.menu_karaoke.KaraokeCategoryFragment;
import com.madeira.entertainz.karaoke.model_room_db.ModelSong;
import com.madeira.entertainz.library.PerformAsync2;
import com.madeira.entertainz.library.Util;
import com.madeira.ytextractor.ExtractorException;
import com.madeira.ytextractor.YoutubeStreamExtractor;
import com.madeira.ytextractor.model.YTMedia;
import com.madeira.ytextractor.model.YTSubtitles;
import com.madeira.ytextractor.model.YoutubeMeta;
import com.madeira.ytextractor.utils.LogUtils;
import com.pierfrancescosoffritti.youtubeplayer.player.AbstractYouTubePlayerListener;
import com.pierfrancescosoffritti.youtubeplayer.player.YouTubePlayer;
import com.pierfrancescosoffritti.youtubeplayer.player.YouTubePlayerInitListener;
import com.pierfrancescosoffritti.youtubeplayer.player.YouTubePlayerListener;
import com.pierfrancescosoffritti.youtubeplayer.player.YouTubePlayerView;
import com.pierfrancescosoffritti.youtubeplayer.ui.PlayerUIController;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import es.dmoral.toasty.Toasty;

public class YouTubeVideoActivity extends AppCompatActivity {
    private static final String TAG = "YouTubeVideoActivity";
    private static final String INTENT_VIDEO_ID = "INTENT_VIDEO_ID";

    private static final int ANIM_DURATION_PLAYBACK_CONTROL = 200;
    boolean isPlaybackControlVisible = false;
    Handler handler;

    private static final int DURATION_AUTOHIDE_PLAYBACK_CONTROL = 3000; //3 second
    YouTubePlayerView youTubePlayerView;
    //    ImageButton btnPlay, btnPause, btnReLoad;
//    LinearLayout controlBar;
    View blackLayout;
    RelativeLayout controllerRL;
    ImageButton prevSecButton;
    ImageButton playButton;
    ImageButton nextSecButton;
    ImageButton equalizerButton;
    ImageButton downloadButton;
    private static final int interval = 5;
    int lastState = -100;
    String videoId = "";
    YouTubePlayer youTubePlayer;
    Boolean isMenuVisible = true, playerReady = false;
    Float currentSecond;
    List<TSongCategory> songCategoryList = new ArrayList<>();
    RecyclerView recyclerView;
    RecyclerViewAdapter adapter;
    String urlVideo = "";
    String urlAudio = "";
    double duration=0;
    String songName="";
    String artist="";

    public static void startActivity(Context context, String youtubeId) {
        Intent intent = new Intent(context, YouTubeVideoActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        intent.putExtra(INTENT_VIDEO_ID, youtubeId);

        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            setContentView(R.layout.activity_you_tube_video);
            youTubePlayerView = findViewById(R.id.youtube_player_view);
            blackLayout = findViewById(R.id.blackLayout);
            controllerRL = findViewById(R.id.controllerRL);
            prevSecButton = findViewById(R.id.prevSecButton);
            playButton = findViewById(R.id.playButton);
            nextSecButton = findViewById(R.id.nextSecButton);
            equalizerButton = findViewById(R.id.equalizerButton);
            downloadButton = findViewById(R.id.downloadButton);
            youTubePlayerView.initialize(new YouTubePlayerInitListener() {
                @Override
                public void onInitSuccess(final YouTubePlayer initializedYouTubePlayer) {
                    initializedYouTubePlayer.addListener(new AbstractYouTubePlayerListener() {
                        @Override
                        public void onReady() {

                            try {
                                playButton.setBackgroundResource(R.drawable.selector_pause);
                                youTubePlayer = initializedYouTubePlayer;
                                youTubePlayerView.enterFullScreen();
                                setActionObject();
                                youTubePlayer.addListener(new YouTubePlayerListener() {
                                    @Override
                                    public void onReady() {

                                    }

                                    @Override
                                    public void onStateChange(int state) {
                                        lastState = state;
                                        if (state == 0) {
                                            blackLayout.setVisibility(View.VISIBLE);

                                        }

                                    }

                                    @Override
                                    public void onPlaybackQualityChange(@NonNull String playbackQuality) {

                                    }

                                    @Override
                                    public void onPlaybackRateChange(@NonNull String playbackRate) {

                                    }

                                    @Override
                                    public void onError(int error) {

                                    }

                                    @Override
                                    public void onApiChange() {

                                    }

                                    @Override
                                    public void onCurrentSecond(float second) {
                                        currentSecond = second;
                                        playerReady = true;
                                    }

                                    @Override
                                    public void onVideoDuration(float duration) {

                                    }

                                    @Override
                                    public void onVideoLoadedFraction(float loadedFraction) {

                                    }

                                    @Override
                                    public void onVideoId(@NonNull String videoId) {

                                    }
                                });

                                videoId = getIntent().getStringExtra(INTENT_VIDEO_ID);
                                initializedYouTubePlayer.loadVideo(videoId, 0);
                                PlayerUIController uiController = youTubePlayerView.getPlayerUIController();

                                uiController.showCurrentTime(true);
                                uiController.showDuration(true);
                                uiController.showSeekBar(true);
                                uiController.showUI(true);

                                uiController.showMenuButton(false);
                                uiController.showPlayPauseButton(false);
                                uiController.showYouTubeButton(false);

                            } catch (Exception ex) {
                                Debug.e(TAG, ex);
                            }
                        }
                    });
                }
            }, true);

            hideNavigationBar();
            installNavigationListener();
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    void setActionObject() {
        youTubePlayerView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                animShowPlaybackControl();
                startAutoHidePlaybackControl();
            }
        });

        prevSecButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (youTubePlayer != null) {
                    float seekTo = currentSecond - interval;
                    youTubePlayer.seekTo(seekTo);
                }
            }
        });


        playButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (youTubePlayer != null) {
                    if (lastState == 2) {
                        youTubePlayer.play();
                        playButton.setBackgroundResource(R.drawable.selector_pause);
                    } else if (lastState != 0) {
                        youTubePlayer.pause();
                        playButton.setBackgroundResource(R.drawable.selector_play);
                    }
                }
            }
        });


        nextSecButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (youTubePlayer != null) {
                    float seekTo = currentSecond + interval;
                    youTubePlayer.seekTo(seekTo);
                }
            }
        });

        downloadButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                extractorYoutube();
            }
        });


    }

    /**
     * Animate show playback control, fade in
     */
    private void animShowPlaybackControl() {

        try {
            //exit apabila playback control sdh show
            if (isPlaybackControlVisible) return;

            isPlaybackControlVisible = true;

            Animation animation = new AlphaAnimation(0f, 1f);
            animation.setDuration(ANIM_DURATION_PLAYBACK_CONTROL);
            animation.setFillAfter(false);

            controllerRL.startAnimation(animation);

            controllerRL.setVisibility(View.VISIBLE);


            playButton.requestFocus();
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    /**
     * Animate hide playback control, fade out
     */
    private void animHidePlaybackControl() {

        try {
            //exit apabila playback control sdh hide
            if (isPlaybackControlVisible == false) return;

            isPlaybackControlVisible = false;

            Animation animation = new AlphaAnimation(1f, 0);
            animation.setDuration(ANIM_DURATION_PLAYBACK_CONTROL);
            animation.setFillAfter(false);

            controllerRL.startAnimation(animation);

            controllerRL.setVisibility(View.INVISIBLE);
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }


    /**
     * Start delay hide dalam waktu yg di tetapkan
     * fungsi ini bisa di call berkali2, previous run akan di cancel.
     */
    private void startAutoHidePlaybackControl() {

        try {
            //create handler apabila blm ada
            if (handler == null) {
                handler = new Handler();
            }

            //cancell all previous run
            cancelAutoHidePlaybackControl();

            //
            handler.postDelayed(runAutoHidePlaybackControl, DURATION_AUTOHIDE_PLAYBACK_CONTROL);
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    private void cancelAutoHidePlaybackControl() {
        try {
            if (handler == null) return;

            //remove all previous run
            handler.removeCallbacks(runAutoHidePlaybackControl);
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    Runnable runAutoHidePlaybackControl = new Runnable() {
        @Override
        public void run() {
            animHidePlaybackControl();
        }
    };

    void hideNavigationBar() {
        try {
            View decorView = getWindow().getDecorView();
// Hide both the navigation bar and the status bar.
// SYSTEM_UI_FLAG_FULLSCREEN is only available on Android 4.1 and higher, but as
// a general r ule, you should design your app to hide the status bar whenever you
// hide the navigation bar.
//        int uiOptions = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION | View.SYSTEM_UI_FLAG_FULLSCREEN;
//        decorView.setSystemUiVisibility(uiOptions);

            decorView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                    | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
        } catch (Exception e) {
            Debug.e(TAG, e);
        }
    }

    void installNavigationListener() {
        try {
            View decorView = getWindow().getDecorView();

            //this add event, will hide again if shown
            decorView.setOnSystemUiVisibilityChangeListener(new View.OnSystemUiVisibilityChangeListener() {
                @Override
                public void onSystemUiVisibilityChange(int visibility) {
                    // Note that system bars will only be "visible" if none of the
                    // LOW_PROFILE, HIDE_NAVIGATION, or FULLSCREEN flags are set.
                    if ((visibility & View.SYSTEM_UI_FLAG_FULLSCREEN) == 0) {
                        hideNavigationBar();
                    } else {
                        // adjustments to your UI, such as hiding the action bar or
                        // other navigational controls.
                    }
                }
            });
        } catch (Exception e) {
            Debug.e(TAG, e);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        youTubePlayerView.release();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        switch (keyCode) {
            case KeyEvent.KEYCODE_DPAD_LEFT:
                if (playerReady) {
                    youTubePlayer.seekTo(currentSecond - 10);
                }
                break;

            case KeyEvent.KEYCODE_DPAD_RIGHT:
                if (playerReady) {
                    youTubePlayer.seekTo(currentSecond + 10);
                }
                break;

            case KeyEvent.KEYCODE_MENU:
                if (isMenuVisible) {
//                    controlBar.setVisibility(View.GONE);

                    isMenuVisible = false;
                } else {
                    isMenuVisible = true;
                }
                return true;

            case KeyEvent.KEYCODE_BACK:
                playerReady = false;
                break;
        }
        return super.onKeyDown(keyCode, event);
    }

    /**
     * untuk extract url downloadable dari youtube
     **/
    void extractorYoutube() {
//        String youtubeLink = "http://youtube.com/watch?v=ew3lh7_DAeU";

        if (!videoId.equals("")) {
//            String youtubeLink = "https://www.youtube.com/watch?v=" + videoId + "&rel=0&vq=hd1080";
            String youtubeLink = "https://www.youtube.com/watch?v=" + videoId;
            new YoutubeStreamExtractor(new YoutubeStreamExtractor.ExtractorListner() {

                @Override
                public void onExtractionDone(List<YTMedia> adativeStream, final List<YTMedia> muxedStream, List<YTSubtitles> subtitles, YoutubeMeta meta) {
                    //sort desc list
                    Collections.sort(adativeStream, new Comparator<YTMedia>() {
                        @Override
                        public int compare(YTMedia o1, YTMedia o2) {
                            return o2.getItag()<o1.getItag()?-1:1;
                        }
                    });
                    for (YTMedia item : adativeStream) {
                        int itag = item.getItag();
                        if (item.getAudioChannels() == 0) {
                            if (itag == 136)//720
                                urlVideo = item.getUrl();
                            else if (itag == 135)//480
                                urlVideo = item.getUrl();
                        }
                        else
                        {
                            if (itag == 141)//256k
                                urlAudio = item.getUrl();
                            else if (itag == 140)//128k
                                urlAudio = item.getUrl();
                        }
                        if(!urlVideo.equals("")&&!urlAudio.equals("")) {
                            duration = item.getApproxDurationMs()/1000;
                            break;
                        }

                    }

                    /*ArrayList<String> urls_li = new ArrayList<>();
                    for (YTMedia c : muxedStream) {
                        urls_li.add(c.getUrl());
                    }
                    for (YTMedia media : adativeStream) {
                        urls_li.add(media.getUrl());
                    }*/

                    //Toast.makeText(getApplicationContext(), meta.getTitle(), Toast.LENGTH_LONG).show();
//                    Toast.makeText(getApplicationContext(), meta.getAuthor(), Toast.LENGTH_LONG).show();


                    String[] splitTitle = meta.getTitle().split(" - ");
                    if(splitTitle.length==2)
                    {
                        artist = splitTitle[0].trim();
                        songName = splitTitle[1].trim();
                    }
                    else
                        songName = meta.getTitle();


                    if (adativeStream.isEmpty()) {
                        LogUtils.log("null ha");
                        return;
                    }
                    if (muxedStream.isEmpty()) {
                        LogUtils.log("null ha");
                        return;
                    }
                   showAlertSelectedCategory();

                }


                @Override
                public void onExtractionGoesWrong(final ExtractorException e) {

                    Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_LONG).show();


                }
            }).useDefaultLogin().Extract(youtubeLink);
        }
    }


    //** untuk memilih
    void showAlertSelectedCategory() {
        try {
            String TAG = "showAlertSelectedCategory";
            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
            LayoutInflater inflater = this.getLayoutInflater();
            View dialogView = inflater.inflate(R.layout.alert_set_category_download_youtube, null);
            dialogBuilder.setView(dialogView);

//            TextView textView = (TextView) dialogView.findViewById(R.id.textView);
//            Button updateButton = (Button) dialogView.findViewById(R.id.okButton);
//            textView.setText(getString(R.string.alreadyLastVersion));

            recyclerView = dialogView.findViewById(R.id.recyclerView);

            AlertDialog alertDialog = dialogBuilder.create();

//            updateButton.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    alertDialog.dismiss();
//
//                }
//            });

            alertDialog.show();
            WindowManager.LayoutParams layoutParams = alertDialog.getWindow().getAttributes();
            layoutParams.width = ViewGroup.LayoutParams.WRAP_CONTENT;
            alertDialog.show();
            fetchSongCategory();
            alertDialog.getWindow().setLayout(430, WindowManager.LayoutParams.WRAP_CONTENT);
            Util.hideNavigationBar(this);
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }

    }


    void fetchSongCategory() {
        ModelSong.getSongCategory(this, new ModelSong.IGetListSongCategory() {
            @Override
            public void onGetListSongCategory(List<TSongCategory> tSongCategoryList) {
                try {
                    songCategoryList = tSongCategoryList;
                    setRecyclerView();
                } catch (Exception ex) {
                    Debug.e(TAG, ex);
                }
            }
        });

    }

    void setRecyclerView() {
        try {
            adapter = new RecyclerViewAdapter(new RecyclerViewAdapter.IRecyclerViewAdapter() {
                @Override
                public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
                    View view = LayoutInflater.from(YouTubeVideoActivity.this).inflate(R.layout.cell_karaoke_category, parent, false);
                    ItemViewHolder item = new ItemViewHolder(view);
                    return item;
                }

                @Override
                public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
                    ItemViewHolder item = (ItemViewHolder) holder;
                    item.bind(songCategoryList.get(position), position);
                }

                @Override
                public int getItemCount() {
                    return songCategoryList.size();
                }
            });

            recyclerView.setAdapter(adapter);
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    //view ini khusus di pakai utk ItemViewHolder, utk selectedItemView
    View selectedView;
    int selectedItem;

    class ItemViewHolder extends RecyclerView.ViewHolder {
        private static final String TAG = "ItemViewHolder";

        View root;
        TextView tvTitle;

        public ItemViewHolder(View itemView) {
            super(itemView);
            root = itemView;

            tvTitle = itemView.findViewById(R.id.text_title);

//            selectionColor = itemView.findViewById(R.id.selectioncolor);
        }

        public void bind(TSongCategory tSongCategory, int pos) {
            try {
                tvTitle.setText(tSongCategory.songCategory);

                if (selectedItem == pos) {
                    selectView(root);
                }

//            untuk merubah warna selection
                if (colorSelection != null) root.setBackgroundTintList(colorSelection);

//            untuk merubah warna text
                if (colorText != null) tvTitle.setTextColor(colorText);

                if (pos == 0) {
                    root.setSelected(true);
                }

                root.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        selectView(v);
                        selectedItem = pos;
//                        mListener.onCategoryClick(tSongCategory.songCategoryId, tSongCategory.songCategory);
                        TDownloadYoutube tDownloadYoutube = new TDownloadYoutube();
                        tDownloadYoutube.songId = videoId;
                        tDownloadYoutube.songName= songName;
                        tDownloadYoutube.artist = artist;
                        tDownloadYoutube.duration = (int)duration;
                        tDownloadYoutube.thumbnail ="https://img.youtube.com/vi/"+videoId+"/maxresdefault.jpg";
                        tDownloadYoutube.songCategoryId = tSongCategory.songCategoryId;
                        tDownloadYoutube.songCategory = tSongCategory.songCategory;
                        tDownloadYoutube.urlSound = urlAudio;
                        tDownloadYoutube.urlVideo = urlVideo;
                        ArrayList<TDownloadYoutube> songItemList = new ArrayList<>();
                        songItemList.add(tDownloadYoutube);

                        Intent intent = new Intent(getApplicationContext(), DownloadYoutubeService.class);
                        intent.putExtra(DownloadYoutubeService.KEY_SONGITEMLIST, songItemList);
                        startService(intent);
                        finish();
                    }
                });
            } catch (Exception ex) {
                Debug.e(TAG, ex);
            }

        }

        void selectView(View v) {
            try {
                //clear selection
                if (selectedView != null) {
                    selectedView.setSelected(false);
                }

                //make selection
                selectedView = v;
                selectedView.setSelected(true);
            } catch (Exception ex) {
                Debug.e(TAG, ex);
            }
        }
    }

    ColorStateList colorSelection, colorText;
}

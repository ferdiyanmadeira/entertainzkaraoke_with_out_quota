package com.madeira.entertainz.karaoke.menu_playlist;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.madeira.entertainz.controller.USBControl;
import com.madeira.entertainz.karaoke.BaseActivity;
import com.madeira.entertainz.karaoke.DBLocal.DbSong;
import com.madeira.entertainz.karaoke.DBLocal.JoinSongPlaylist;
import com.madeira.entertainz.karaoke.DBLocal.TSongPlaylist;
import com.madeira.entertainz.karaoke.Debug;
import com.madeira.entertainz.karaoke.R;
import com.madeira.entertainz.karaoke.menu_youtube.CategoryController;
import com.madeira.entertainz.karaoke.model.ModelSongLocal;
import com.madeira.entertainz.karaoke.model.ModelYoutube;
import com.madeira.entertainz.karaoke.model_room_db.ModelPlaylist;
import com.madeira.entertainz.library.EzRecyclerViewAdapter;
import com.madeira.entertainz.library.PerformAsync2;
import com.madeira.entertainz.library.UtilArrayList;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class PlaylistChangeOrderActivity extends BaseActivity {
    private static final String TAG = "PlaylistChangeOrder";

    public static final int ACTIVITY_REQUEST_CODE = 1000; //apabila request code bentrok dgn yg lain silahkan di rubah

    private static final String INTENT_PLAYLIST_ID = "INTENT_PLAYLIST_ID";
    private static final String INTENT_SONG_ID = "INTENT_SONG_ID";

    private static final String INTENT_RESULT = "SAVE";

    RecyclerView recyclerView;
    EzRecyclerViewAdapter adapter;

    int playlistId; //playlist yg ada current song
    String songId; //song yg akan di rubah order nya

    List<JoinSongPlaylist> list; //load dari db langsung
    USBControl usbControl = new USBControl();
    /**
     * result 1 = order berubah, 0 = tdk berubah
     * @param activity
     */
    public static void startActivity(Activity activity, int playlistId, String songId){
        Intent intent;
        intent = new Intent(activity, PlaylistChangeOrderActivity.class);

        intent.putExtra(INTENT_SONG_ID, songId);
        intent.putExtra(INTENT_PLAYLIST_ID, playlistId);

        activity.startActivityForResult(intent, ACTIVITY_REQUEST_CODE);
    }

    public static boolean checkRequestCode(int reqCode){
        return reqCode == ACTIVITY_REQUEST_CODE;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        playlistId = getIntent().getIntExtra(INTENT_PLAYLIST_ID,0);
        songId = getIntent().getStringExtra(INTENT_SONG_ID);

        setContentView(R.layout.activity_playlist_change_order);

        recyclerView = findViewById(R.id.recycler_view);

        list = new ArrayList<>();

        loadPlaylist();

        setupRecyclerView();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {

        switch (keyCode){
            case KeyEvent.KEYCODE_DPAD_CENTER:
            case KeyEvent.KEYCODE_ENTER:
                saveAndExit();
                break;

            case KeyEvent.KEYCODE_DPAD_UP:
                moveUp();
                break;
            case KeyEvent.KEYCODE_DPAD_DOWN:
                moveDown();
                break;
        }

        return super.onKeyDown(keyCode, event);
    }

    private void setupRecyclerView(){

        adapter = new EzRecyclerViewAdapter(new EzRecyclerViewAdapter.IEzRecyclerViewAdapter() {
            @Override
            public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
                ItemViewHolder item = null;
                try {
                    View view = LayoutInflater.from(getBaseContext()).inflate(R.layout.cell_playlist_youtube_change_order, parent, false);
                    item = new ItemViewHolder(view);

                } catch (Exception e) {
                    e.printStackTrace();
                }
                return item;
            }

            @Override
            public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
                ItemViewHolder item = (ItemViewHolder) holder;
                item.bind(list.get(position));
            }

            @Override
            public int getItemCount() {
                return list.size();
            }

            @Override
            public long getItemId(int pos) {
                return list.get(pos).songPlaylistId;
            }
        });

        recyclerView.setAdapter(adapter);
    }

    /**
     * ambil play list dari room
     */
    private void loadPlaylist(){

        ModelPlaylist.getPlaylistById(PlaylistChangeOrderActivity.this, playlistId, new ModelPlaylist.IGetPlaylistById() {
            @Override
            public void onGetListPlaylistById(List<JoinSongPlaylist> joinSongPlaylists) {
                //TODO: show error apabila null
                if (joinSongPlaylists.size()==0) return;

                //replace list yg ada dgn yg dari room
                list = joinSongPlaylists;


                adapter.notifyDataSetChanged();
            }
        });
    }

    /**
     * move current song to up
     */
    private void moveUp(){
        try {
            int currentSong = JoinSongPlaylist.getIndexBySongId(list, songId);
            UtilArrayList.moveUp(list, currentSong);
            adapter.notifyDataSetChanged();
        }catch (Exception ex)
        {
            Debug.e(TAG, ex);
        }
    }

    private void moveDown(){
        try {
            int currentSong = JoinSongPlaylist.getIndexBySongId(list, songId);
            UtilArrayList.moveDown(list, currentSong);
            adapter.notifyDataSetChanged();
        }catch (Exception ex)
        {
            Debug.e(TAG, ex);
        }
    }

    private void saveAndExit(){

        ModelPlaylist.ChangeOrderSongPlaylist(PlaylistChangeOrderActivity.this, playlistId, list, new ModelPlaylist.IChangeOrderSongPlaylist() {
            @Override
            public void onChangeOrderSongPlaylist(int result) {
                try {
                    //now exit this activity
                    usbControl.writePlaylistFile(PlaylistChangeOrderActivity.this);
                    Intent returnIntent = new Intent();
                    returnIntent.putExtra(INTENT_RESULT, 1);
                    setResult(Activity.RESULT_OK, returnIntent);
                    finish();
                }catch (Exception ex)
                {
                    Debug.e(TAG, ex);
                }
            }
        });

    }

////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////

    class ItemViewHolder extends RecyclerView.ViewHolder {
        private static final String TAG = "ItemViewHolder";

        TextView tvTitle;
        TextView tvArtist;

        ItemViewHolder(View itemView) {
            super(itemView);

            try {
                tvTitle = itemView.findViewById(R.id.text_view_title);
                tvArtist = itemView.findViewById(R.id.text_view_artist);

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        public void bind(JoinSongPlaylist item) {

            try {
                tvTitle.setText(item.songName);
                tvArtist.setText(item.artist);

                if (item.songId.equals(songId)){
                    itemView.setSelected(true);
                } else {
                    itemView.setSelected(false);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}

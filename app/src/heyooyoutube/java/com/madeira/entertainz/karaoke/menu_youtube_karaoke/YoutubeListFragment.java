package com.madeira.entertainz.karaoke.menu_youtube_karaoke;

import android.app.AlertDialog;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.ScaleAnimation;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.androidnetworking.error.ANError;
import com.madeira.entertainz.controller.USBControl;
import com.madeira.entertainz.helper.RecyclerViewAdapter;
import com.madeira.entertainz.karaoke.DBLocal.JoinYoutubePlaylistDt;
import com.madeira.entertainz.karaoke.DBLocal.TSongYoutube;
import com.madeira.entertainz.karaoke.Debug;
import com.madeira.entertainz.karaoke.R;
import com.madeira.entertainz.karaoke.config.C;
import com.madeira.entertainz.karaoke.menu_youtube_karaoke_playlist.YoutubePlaylistActivity;
import com.madeira.entertainz.karaoke.model_online.ModelYoutube;
import com.madeira.entertainz.karaoke.model_room_db.ModelRecentSearch;
import com.madeira.entertainz.karaoke.model_room_db.ModelYoutubePlaylist;
import com.madeira.entertainz.karaoke.player.YoutubePlayerActivity;
import com.madeira.entertainz.library.Util;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import es.dmoral.toasty.Toasty;

public class YoutubeListFragment extends Fragment implements com.madeira.entertainz.karaoke.menu_youtube_karaoke.YoutubeTheme.IYoutubeThemeListener {
    String TAG = "YoutubeListFragment";

    public static final String KEY_CATEGORY_ID = "CATEGORY_ID";
    public static final String KEY_CATEGORY_TITLE = "CATEGORY_TITLE";
    private boolean isLoading;

//    List<Uri>

    private int categoryId;
    //    private String categoryTitle;
    private int playlistId = 0;

    YoutubeTheme YoutubeTheme;
    List<TSongYoutube> tSongYoutubeArrayList = new ArrayList<>();
    RecyclerViewAdapter adapter;
    RecyclerViewAdapter adapterPlaylist;

    View root;
    RelativeLayout rootLL;
    RecyclerView recyclerView;
    LinearLayout searchLL;
    Button playlistButton;
    EditText searchET;
    RelativeLayout recentView;
    ImageView iconImageView;
    TextView totalSongTV;

    int nextOffset = 0;
    //    TextView songCategoryTV;
    private IYoutubeListListener mListener;
    GridLayoutManager gridLayoutManager;
    ProgressBar progressBar;
    USBControl usbControl = new USBControl();
    TextView recentTV;

    public YoutubeListFragment() {
        // Required empty public constructor
    }


    public static YoutubeListFragment newInstance(int prmCategoryId) {
        YoutubeListFragment fragment = new YoutubeListFragment();
        try {
            Bundle args = new Bundle();
            args.putInt(KEY_CATEGORY_ID, prmCategoryId);
            fragment.setArguments(args);
        } catch (Exception ex) {
        }
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            if (getArguments() != null) {
                categoryId = getArguments().getInt(KEY_CATEGORY_ID);
//            categoryTitle = getArguments().getString(KEY_CATEGORY_TITLE);
                playlistId = getArguments().getInt(YoutubeKaraokeActivity.KEY_PLAYLIST_ID);
            } else {
                categoryId = 0;
            }

        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        root = inflater.inflate(R.layout.fragment_youtube_list, container, false);
        try {
            bind();
            YoutubeTheme = new YoutubeTheme(getActivity(), this);
            YoutubeTheme.setTheme();
            setOnActionObject();
            fetchSong(C.LIMIT_FETCH_DATA, 0);
            setRecyclerView();
//            recyclerViewState = recyclerView.getLayoutManager().onSaveInstanceState();
//            recyclerView.getLayoutManager().onRestoreInstanceState(recyclerViewState);
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
        return root;
    }

    @Override
    public void setThemeUpdateDate(Date lastUpdateTheme) {

    }

    @Override
    public void setListColorTheme(int color) {
        try {
            //untuk set round corner dan color
            rootLL.setBackgroundResource(R.drawable.tags_rounded_corners);
            GradientDrawable rootDrawable = (GradientDrawable) rootLL.getBackground();
            rootDrawable.setColor(color);

            searchLL.setBackgroundResource(R.drawable.tags_rounded_corners);
            GradientDrawable searchDrawable = (GradientDrawable) searchLL.getBackground();
            searchDrawable.setColor(color);
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    @Override
    public void setTextAndSelectionColorTheme(ColorStateList colorSelection, ColorStateList colorText, int colorTextSelected, int colorTextFocus, int colorTextNormal, int colorSelectionSelected, int colorSelectionFocus) {
        try {
            this.colorSelection = colorSelection;
            this.colorText = colorText;
            this.colorTextNormal = colorTextNormal;

//        songCategoryTV.setTextColor(colorTextNormal);
//        searchET.setHintTextColor(colorTextNormal);
            searchET.setTextColor(colorTextNormal);
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    void bind() {
        try {
            rootLL = root.findViewById(R.id.rootLL);
            recyclerView = root.findViewById(R.id.recyclerView);
            searchLL = root.findViewById(R.id.searchLL);
            playlistButton = root.findViewById(R.id.playlistButton);
            searchET = root.findViewById(R.id.searchET);
            recentView = root.findViewById(R.id.recent_view);
            iconImageView = root.findViewById(R.id.icon_image_view);
            progressBar = root.findViewById(R.id.progressBar);
            totalSongTV = root.findViewById(R.id.total_song_tv);
            recentTV = root.findViewById(R.id.recent_tv);
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    void fetchSong(int limit, int offset) {

        //apabila ini adalah category personal list maka loading dari db
        if (categoryId == YoutubeCategoryFragment.CATEGORY_PERSONAL_LIST) {

            com.madeira.entertainz.karaoke.model_room_db.ModelYoutube.loadPersonalList(getContext(), new com.madeira.entertainz.karaoke.model_room_db.ModelYoutube.OnLoadPersonalList() {
                @Override
                public void onLoadPersonalList(List<TSongYoutube> list) {
                    Log.i(TAG, "onLoadPersonalList: " + list.size());

                    tSongYoutubeArrayList.clear();
                    tSongYoutubeArrayList.addAll(list);
                    long totalSong = list.size();
                    totalSongTV.setText(Util.thousandFormat(totalSong));
                    adapter.notifyDataSetChanged();
                }
            });

            return;
        }

        //tdk perlu show progress bar, krn focus berubah shg menyusahkan utk resume scrolling
//        progressBar.setVisibility(View.VISIBLE);
        ModelYoutube.getYoutube(limit, offset, categoryId, new ModelYoutube.IGetYoutube() {
            @Override
            public void onGetYoutube(ModelYoutube.listYoutubeItem item) {
                try {
                    progressBar.setVisibility(View.GONE);
                    if (offset == 0)
                        tSongYoutubeArrayList.clear();
                    tSongYoutubeArrayList.addAll(item.listYoutube);
                    long totalSong = item.totalSong;
                    totalSongTV.setText(Util.thousandFormat(totalSong));
                    adapter.notifyDataSetChanged();
//                    setRecyclerView();
                } catch (Exception ex) {
                    Debug.e(TAG, ex);
                }
            }

            @Override
            public void onError(ANError e) {
                progressBar.setVisibility(View.GONE);
            }
        });


    }

    void setRecyclerView() {
        try {
            adapter = new RecyclerViewAdapter(new RecyclerViewAdapter.IRecyclerViewAdapter() {
                @Override
                public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
                    View view = LayoutInflater.from(getActivity()).inflate(R.layout.cell_youtube_karaoke_list, parent, false);
                    ItemViewHolder item = new ItemViewHolder(view);
                    return item;
                }

                @Override
                public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
                    try {
                        ItemViewHolder item = (ItemViewHolder) holder;
                        item.bind(tSongYoutubeArrayList.get(position), position);
                    } catch (Exception ex) {
                        Debug.e(TAG, ex);
                    }
                }

                @Override
                public int getItemCount() {
                    int size = 0;
                    try {
                        size = tSongYoutubeArrayList.size();
                    } catch (Exception ex) {
                        Debug.e(TAG, ex);
                    }
                    return size;
                }
            });

            //utk menghindari lost focus saat notifyDataSetChanged
            adapter.setHasStableIds(true);

            gridLayoutManager = new GridLayoutManager(getActivity(), 5);
            recyclerView.setLayoutManager(gridLayoutManager);
            recyclerView.setAdapter(adapter);
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    class ItemViewHolder extends RecyclerView.ViewHolder {
        private static final String TAG = "ItemViewHolder";

        View root;
        TextView tvTitle;
        TextView tvSinger;
        ImageView thumbnailIV;

        public ItemViewHolder(View itemView) {
            super(itemView);
            root = itemView;
            selectedView = root;
            tvTitle = itemView.findViewById(R.id.text_title);
            tvSinger = itemView.findViewById(R.id.text_singer);
            thumbnailIV = itemView.findViewById(R.id.thumbnailIV);
        }

        public void bind(TSongYoutube tSong, int pos) {
            try {
                tvTitle.setText(tSong.songName);
                tvSinger.setText(tSong.artist);
                if (tSong.thumbnail != null) {
//                UtilRenderScript.fetchBitmap(getContext(), tSong.thumbnail, 300, thumbnailIV);
                    Picasso.with(getActivity())
                            .load(tSong.thumbnail)
                            .into(thumbnailIV);
                }


                if (colorSelection != null) root.setBackgroundTintList(colorSelection);
//            untuk merubah warna text
                if (colorText != null) {
                    tvTitle.setTextColor(colorText);
                    tvSinger.setTextColor(colorText);
                }


                root.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
//                    mListener.onListClick(tSong.songId);

                        showDetailSong(tSong);
                    }
                });
                root.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                    @Override
                    public void onFocusChange(View v, boolean hasFocus) {
                        if (hasFocus) {
                            updateSelectedViewUi(v, true);
                        } else {
                            updateSelectedViewUi(null, false);

                        }
                    }
                });
            } catch (Exception ex) {
                Debug.e(TAG, ex);
            }
        }

        void selectView(View v) {
            //clear selection
            /*if (selectedView != null) {
                selectedView.setSelected(false);
            }

            //make selection
            selectedView = v;
            selectedView.setSelected(true);*/
        }
    }

    ColorStateList colorSelection, colorText;
    int colorTextNormal;

    //function untuk declare semua action dari object di activity
    void setOnActionObject() {
        try {
            searchET.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {
                    try {
                        //reset next offset
                        nextOffset = 0;
                        searchSong(C.LIMIT_FETCH_DATA, 0, s.toString());
                    } catch (Exception ex) {
                        Debug.e(TAG, ex);
                    }
                }
            });

            playlistButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    YoutubePlaylistActivity.startActivity(getActivity(), 0);
                }
            });

            searchET.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View v, boolean hasFocus) {
                   /* if (!hasFocus)
                        searchET.setHintTextColor(getActivity().getResources().getColor(R.color.texthintcolor_unfocus));
                    else
                        searchET.setHintTextColor(colorTextNormal);*/

                    try {
                        if (!hasFocus) {
//                            Util.closeKeyboard(getActivity());
                            searchET.setHintTextColor(getActivity().getResources().getColor(R.color.texthintcolor_unfocus));
                            String keyword = searchET.getText().toString();
                            if (!keyword.equals(""))
                                addRecentSearch(keyword);
                        } else {
                            Util.showKeyboard(getActivity());
                            searchET.setHintTextColor(colorTextNormal);
                        }
                    } catch (Exception ex) {
                        Debug.e(TAG, ex);
                    }

                }
            });

            recentView.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View v, boolean hasFocus) {
                    try {
                        if (hasFocus) {
                            iconImageView.setBackgroundResource(R.drawable.ic_recent_black);
                            recentTV.setTextColor(getContext().getColor(R.color.black));
                        } else {
                            iconImageView.setBackgroundResource(R.drawable.ic_recent_white);
                            recentTV.setTextColor(getContext().getColor(R.color.white));
                        }
                    } catch (Exception ex) {
                        Debug.e(TAG, ex);
                    }
                }
            });

            recentView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        showRecentSearch();
                    } catch (Exception ex) {
                        Debug.e(TAG, ex);
                    }
                }
            });

            recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                    super.onScrollStateChanged(recyclerView, newState);
                    try {
                        //load 10 item sebelum last item, shg akan memberikan effect continous
                        final int BATAS_LOADING = 10;

                        if (gridLayoutManager.findLastVisibleItemPosition() >= tSongYoutubeArrayList.size() - BATAS_LOADING) {

                            // We have reached the end of the recycler view.
                            nextOffset++;
                            if (searchET.getText().toString().equals("")) {
                                fetchSong(C.LIMIT_FETCH_DATA, tSongYoutubeArrayList.size());
                            } else {
                                searchSong(C.LIMIT_FETCH_DATA, tSongYoutubeArrayList.size(), searchET.getText().toString());
                            }
                        }

                    } catch (Exception ex) {
                        Debug.e(TAG, ex);
                    }
                }
            });
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    void searchSong(int limit, int offset, String prmKeyword) {
        try {
            progressBar.setVisibility(View.VISIBLE);
            List<TSongYoutube> newListYoutube = new ArrayList<>();
            if (categoryId == YoutubeCategoryFragment.CATEGORY_PERSONAL_LIST) {
                for(TSongYoutube item : tSongYoutubeArrayList)
                {
                    if(item.songName.toLowerCase().contains(prmKeyword.toLowerCase()))
                    {
                        newListYoutube.add(item);
                    }
                    else {
                        if (item.artist.toLowerCase().contains(prmKeyword.toLowerCase())) {
                            newListYoutube.add((item));
                        }
                    }
                }
                tSongYoutubeArrayList.clear();
                tSongYoutubeArrayList.addAll(newListYoutube);
                long totalSong = tSongYoutubeArrayList.size();
                progressBar.setVisibility(View.GONE);
                totalSongTV.setText(Util.thousandFormat(totalSong));
                adapter.notifyDataSetChanged();
                return;
            }

            ModelYoutube.getYoutubeSearch(limit, offset, prmKeyword, new ModelYoutube.IGetYoutube() {
                @Override
                public void onGetYoutube(ModelYoutube.listYoutubeItem item) {
                    try {
                        progressBar.setVisibility(View.GONE);
                        if (offset == 0)
                            tSongYoutubeArrayList.clear();
                        tSongYoutubeArrayList.addAll(item.listYoutube);
                        adapter.notifyDataSetChanged();
//                        setRecyclerView();
                    } catch (Exception ex) {
                        Debug.e(TAG, ex);
                    }

                }

                @Override
                public void onError(ANError e) {
                    progressBar.setVisibility(View.GONE);
                }
            });
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    public void setCallback(IYoutubeListListener listener) {
        this.mListener = listener;
    }

    public interface IYoutubeListListener {
        // TODO: Update argument type and name
        void onListClick(int songId);
    }

    void showDetailSong(TSongYoutube prmTSongYoutube) {
        try {
            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
            LayoutInflater inflater = this.getLayoutInflater();
            View dialogView = inflater.inflate(R.layout.alert_youtube_song, null);
            dialogBuilder.setView(dialogView);

            ImageView thumbnailIV = (ImageView) dialogView.findViewById(R.id.thumbnailIV);
            TextView titleTV = (TextView) dialogView.findViewById(R.id.titleTV);
            TextView singerTV = (TextView) dialogView.findViewById(R.id.singerTV);
            Button playSongButton = (Button) dialogView.findViewById(R.id.playSongButton);
            Button addToPlaylistButton = (Button) dialogView.findViewById(R.id.addToPlaylistButton);
            if (prmTSongYoutube.thumbnail != null) {
//            Uri uri = Uri.fromFile(new File(prmTSongYoutube.thumbnail));
//            Picasso.with(getActivity()).load(uri).into(thumbnailIV);
//            UtilRenderScript.fetchBitmap(getActivity(), prmTSongYoutube.thumbnail, 300, thumbnailIV);
                Picasso.with(getActivity()).load(prmTSongYoutube.thumbnail).into(thumbnailIV);
            }
            titleTV.setText(prmTSongYoutube.songName);
            singerTV.setText(prmTSongYoutube.artist);
            AlertDialog alertDialog = dialogBuilder.create();

            playSongButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
//                Toast.makeText(getContext(), "play song", Toast.LENGTH_LONG).show();
//                    ArrayList<TSongYoutube> tSongArrayList = new ArrayList<>();
//                    tSongArrayList.add(prmTSongYoutube);
//                    YoutubePlayerActivity.startActivity(getActivity(), tSongArrayList, 0);

                    playSong(prmTSongYoutube);

                    alertDialog.dismiss();

                }
            });

            addToPlaylistButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    alertDialog.dismiss();
                    if (playlistId == 0) {
                        showDialogPlaylist(prmTSongYoutube);
                    } else {
                        savePlaylist(prmTSongYoutube, "", playlistId, true);
                    }

                }
            });

            if (playlistId == 0)
                playSongButton.setVisibility(View.VISIBLE);
            else
                playSongButton.setVisibility(View.GONE);

            alertDialog.show();
            WindowManager.LayoutParams layoutParams = alertDialog.getWindow().getAttributes();
            layoutParams.width = ViewGroup.LayoutParams.WRAP_CONTENT;
            alertDialog.show();
            alertDialog.getWindow().setLayout(430, WindowManager.LayoutParams.WRAP_CONTENT);
            Util.hideNavigationBar(getActivity());
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    void playSong(TSongYoutube song) {
        ArrayList<TSongYoutube> tSongArrayList = new ArrayList<>();
        tSongArrayList.add(song);
        YoutubePlayerActivity.startActivity(getActivity(), tSongArrayList, 0);

    }

    void showDialogPlaylist(TSongYoutube prmTSongYoutube) {
        ModelYoutubePlaylist.getJoinPlaylist(getContext(), new ModelYoutubePlaylist.IGetJoinPlaylist() {
            @Override
            public void onGetListPlaylist(List<JoinYoutubePlaylistDt> joinYoutubePlaylistDts) {
                try {
                    List<JoinYoutubePlaylistDt> joinYoutubePlaylistDtList = joinYoutubePlaylistDts;
                    AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
                    LayoutInflater inflater = getLayoutInflater();
                    View dialogView = inflater.inflate(R.layout.alert_listof_playlist, null);
                    dialogBuilder.setView(dialogView);

                    RecyclerView recyclerView = (RecyclerView) dialogView.findViewById(R.id.recyclerView);
                    LinearLayout notExistPlaylistLL = (LinearLayout) dialogView.findViewById(R.id.notExistPlaylistLL);
                    LinearLayout existPlaylistLL = (LinearLayout) dialogView.findViewById(R.id.existPlaylistLL);
                    EditText playlistET = (EditText) dialogView.findViewById(R.id.playlistET);
                    Button saveButton = (Button) dialogView.findViewById(R.id.saveButton);
                    Button addPlaylistButton = (Button) dialogView.findViewById(R.id.addPlaylistButton);
                    Button cancelButton = (Button) dialogView.findViewById(R.id.cancelButton);
                    existPlaylistLL.setVisibility(View.VISIBLE);
                    saveButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
//                        savePlaylist(prmTSongYoutube, playlistET.getText().toString(), 0);
                            if (!TextUtils.isEmpty(playlistET.getText().toString()))
                                savePlaylist(prmTSongYoutube, playlistET.getText().toString(), 0, true);
                            else {
                                playlistET.setError(getString(R.string.playlistMustFilled));
                                playlistET.requestFocus();
                            }
//                            Toasty.err
                        }
                    });

                    addPlaylistButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            notExistPlaylistLL.setVisibility(View.VISIBLE);
                            addPlaylistButton.setVisibility(View.GONE);
                        }
                    });
                    cancelButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            notExistPlaylistLL.setVisibility(View.GONE);
                            addPlaylistButton.setVisibility(View.VISIBLE);
                        }
                    });
                    setRecyclerViewPlaylist(recyclerView, joinYoutubePlaylistDtList, prmTSongYoutube);

                    AlertDialog alertDialog = dialogBuilder.create();
                    alertDialog.show();
                    WindowManager.LayoutParams layoutParams = alertDialog.getWindow().getAttributes();
                    layoutParams.width = ViewGroup.LayoutParams.WRAP_CONTENT;
                    alertDialog.show();
                    alertDialog.getWindow().setLayout(530, WindowManager.LayoutParams.WRAP_CONTENT);
                    Util.hideNavigationBar(getActivity());
                } catch (Exception ex) {
                    Debug.e(TAG, ex);
                }
            }
        });

    }


    void savePlaylist(TSongYoutube song, String prmPlaylistName, int prmPlaylistId, boolean fgIntent) {

        ModelYoutubePlaylist.saveSongPlaylist(getContext(), song, prmPlaylistName, prmPlaylistId, new ModelYoutubePlaylist.ISaveSongPlaylist() {
            @Override
            public void onSaveSongPlaylist(int result) {
                try {
                    int i = (int) result;
                    //1 success
                    //2 duplicate playlist
                    //0 failed
                    //3 song already in playlist
                    if (i == 1) {
                        // jika fgintent true,
                        if (fgIntent) {
                            Toasty.success(getActivity(), getString(R.string.successAddSong)).show();
                            showConfirmSelectingSong(prmPlaylistId);
                        } else {
                            YoutubePlaylistActivity.startActivity(getActivity(), prmPlaylistId);
                            getActivity().finish();
                        }
//                        writePlaylistSongsToFile();
                        usbControl.writeYoutubeSongsPlaylistToFile(getContext());
                    } else if (i == 2) {
                        Toasty.error(getActivity(), getString(R.string.playlistAlreadyExists)).show();
                    } else if (i == 3) {
                        Toasty.error(getActivity(), getString(R.string.songAlreadyExists)).show();
                    }
                } catch (Exception ex) {
                    Debug.e(TAG, ex);
                }
            }
        });
    }

    void setRecyclerViewPlaylist(RecyclerView prmRecyclerView, List<JoinYoutubePlaylistDt> prmJoinYoutubePlaylistDts, TSongYoutube prmTSongYoutube) {
        try {
            adapterPlaylist = new RecyclerViewAdapter(new RecyclerViewAdapter.IRecyclerViewAdapter() {
                @Override
                public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
                    View view = LayoutInflater.from(getActivity()).inflate(R.layout.cell_listof_playlist, parent, false);
                    ItemViewHolderPlaylist item = new ItemViewHolderPlaylist(view);
                    return item;
                }

                @Override
                public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
                    ItemViewHolderPlaylist item = (ItemViewHolderPlaylist) holder;
                    item.bind(prmJoinYoutubePlaylistDts.get(position), position, prmTSongYoutube);
                }

                @Override
                public int getItemCount() {
                    return prmJoinYoutubePlaylistDts.size();
                }
            });

            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
            prmRecyclerView.setLayoutManager(linearLayoutManager);
            prmRecyclerView.setAdapter(adapterPlaylist);
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    class ItemViewHolderPlaylist extends RecyclerView.ViewHolder {
        private static final String TAG = "ItemViewHolderPlaylist";

        View root;
        TextView playlistTV;
        TextView sumOfSongsTV;
        TextView totalDurationTV;
        LinearLayout selectioncolor;

        public ItemViewHolderPlaylist(View itemView) {
            super(itemView);
            root = itemView;
            playlistTV = itemView.findViewById(R.id.playlistTV);
            sumOfSongsTV = itemView.findViewById(R.id.sumOfSongsTV);
            totalDurationTV = itemView.findViewById(R.id.totalDurationTV);
            selectioncolor = itemView.findViewById(R.id.selectioncolor);
        }

        public void bind(JoinYoutubePlaylistDt tPlaylist, int pos, TSongYoutube tSong) {
            try {
                playlistTV.setText(tPlaylist.playlistName);
                int milis = tPlaylist.sumOfDuration * 1000;
                totalDurationTV.setText(Util.convertMilisToTime2(milis));
                sumOfSongsTV.setText(String.valueOf(tPlaylist.sumOfSong));

                if ((pos % 2) == 0) {
                    // number is even
                    int color = ContextCompat.getColor(getContext(), R.color.first_row_table_dialog);
                    root.setBackgroundColor(color);
                } else {
                    // number is odd
                    int color = ContextCompat.getColor(getContext(), R.color.second_row_table_dialog);
                    root.setBackgroundColor(color);
                }
//            tvSinger.setText(tPlaylist.artist);

//            if (colorSelection != null) root.setBackgroundTintList(colorSelection);


                selectioncolor.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
//                    showDetailSong(tPlaylist);
                        savePlaylist(tSong, "", tPlaylist.playlistId, false);
                    }
                });
            } catch (Exception ex) {
                Debug.e(TAG, ex);
            }
        }

    }

    void updateSelectedViewUi(View view, boolean hasFocus) {
        try {
            if (selectedView != null) {
                scaleView(selectedView, 1.1f, 1);
                ViewCompat.setTranslationZ(selectedView, 0);
            }

            if (hasFocus) {
                selectedView = view;
                ViewCompat.setTranslationZ(selectedView, 100);
//                selectedView.setBackgroundResource(R.drawable.box_white);
                scaleView(selectedView, 1, 1.1f);
            } else {
                //remove selected & focus dari grid
                selectedView = null;
            }
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    public void scaleView(View v, float startScale, float endScale) {
        try {
            Animation anim = new ScaleAnimation(
                    startScale, endScale, // Start and end values for the X axis scaling
                    startScale, endScale, // Start and end values for the Y axis scaling
                    Animation.RELATIVE_TO_SELF, 0.5f, // Pivot point of X scaling
                    Animation.RELATIVE_TO_SELF, 0.5f); // Pivot point of Y scaling
            anim.setFillAfter(true); // Needed to keep the result of the animation
            anim.setDuration(100);
            v.startAnimation(anim);
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    static View selectedView;

    /**
     * show recent lagu yang pernah dicari
     */
    void showRecentSearch() {
        try {
            RecentSearchYoutubeKaraokeActivity.startActivity(getActivity());
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    /**
     * untuk tambahkan recent search ke local db
     */
    void addRecentSearch(String songName) {

        ModelRecentSearch.addRecentSearch(getContext(), songName, C.SEARCH_SONG_YOUTUBE_KARAOKE, new ModelRecentSearch.IAddRecentSearch() {
            @Override
            public void onAddRecentSearch(int result) {

            }
        });

//        PerformAsync2.run(new PerformAsync2.Callback() {
//            @Override
//            public Object onBackground(PerformAsync2 performAsync) {
//                List<TRecentSearchSong> tRecentSearchSongList = new ArrayList<>();
//                try {
//                    DbRecentSearchSong dbRecentSearchSong = DbRecentSearchSong.Instance.create(getActivity());
//                    List<TRecentSearchSong> tRecentSearchSongs = dbRecentSearchSong.daoAccessRecentSearchSong().getAll(C.SEARCH_SONG_YOUTUBE_KARAOKE);
//
//                    //delete recent id satu persatu yang lebih dari 50
//                    for (int i = 50; i < tRecentSearchSongs.size(); i++) {
//                        dbRecentSearchSong.daoAccessRecentSearchSong().deleteById(tRecentSearchSongs.get(i).recentId);
//                    }
//
//                    TRecentSearchSong tRecentSearchSong = dbRecentSearchSong.daoAccessRecentSearchSong().checkSong(songName, C.SEARCH_SONG_YOUTUBE_KARAOKE);
//                    if (tRecentSearchSong != null) {
//                        //hapus recent terakhir yang menggunakan song name ini, dan create ulang
//                        dbRecentSearchSong.daoAccessRecentSearchSong().deleteById(tRecentSearchSong.recentId);
//                    }
//                    TRecentSearchSong addTRecentSearchSong = new TRecentSearchSong();
//                    addTRecentSearchSong.recentSong = songName;
//                    addTRecentSearchSong.category = C.SEARCH_SONG_YOUTUBE_KARAOKE;
//                    dbRecentSearchSong.daoAccessRecentSearchSong().insert(addTRecentSearchSong);
//                    dbRecentSearchSong.close();
//                } catch (Exception ex) {
//                    Debug.e(TAG, ex);
//                }
//                return tRecentSearchSongList;
//            }
//        }).setCallbackResult(new PerformAsync2.CallbackResult() {
//            @Override
//            public void onResult(Object result) {
//                try {
//                } catch (Exception ex) {
//                    Debug.e(TAG, ex);
//                }
//            }
//        });


    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
//        super.onActivityResult(requestCode, resultCode, data);
        try {
            Log.i(TAG, "onActivityResult: " + requestCode);

            //check apakah ada request code dari  ??
            //kalo ada artinya user melakukan change order
            if (RecentSearchYoutubeKaraokeActivity.checkRequestCode(requestCode)) {
                String keyword = data.getStringExtra(RecentSearchYoutubeKaraokeActivity.INTENT_RESULT);
                searchET.setText(keyword);
                //reset next offset;
                nextOffset = 0;
                searchSong(C.LIMIT_FETCH_DATA, tSongYoutubeArrayList.size(), keyword);
            }
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }


    /**
     * untuk confirm apakah mau select song lagi atau langsung ke playlist
     */
    void showConfirmSelectingSong(int prmPlaylistId) {
        try {
            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
            LayoutInflater inflater = this.getLayoutInflater();
            View dialogView = inflater.inflate(R.layout.alert_back_selecting, null);
            dialogBuilder.setView(dialogView);
            Button backSelectionButton = dialogView.findViewById(R.id.back_button);
            Button gotoPlaylistButton = dialogView.findViewById(R.id.goto_button);
            AlertDialog alertDialog = dialogBuilder.create();
            backSelectionButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    alertDialog.dismiss();

                }
            });

            gotoPlaylistButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    alertDialog.dismiss();
                    YoutubePlaylistActivity.startActivity(getActivity(), prmPlaylistId);
                    getActivity().finish();
                }
            });


            alertDialog.show();
            WindowManager.LayoutParams layoutParams = alertDialog.getWindow().getAttributes();
            layoutParams.width = ViewGroup.LayoutParams.WRAP_CONTENT;
            alertDialog.show();
            alertDialog.getWindow().setLayout(430, WindowManager.LayoutParams.WRAP_CONTENT);
            Util.hideNavigationBar(getActivity());
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }


}

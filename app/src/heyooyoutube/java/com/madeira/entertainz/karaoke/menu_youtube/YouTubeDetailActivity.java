package com.madeira.entertainz.karaoke.menu_youtube;

import android.app.Activity;
import android.app.ActivityOptions;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.DrawableContainer;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.StateListDrawable;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.madeira.entertainz.karaoke.Debug;
import com.madeira.entertainz.karaoke.R;
import com.madeira.entertainz.library.Util;

import java.util.Date;

public class YouTubeDetailActivity extends Activity implements YoutubeTheme.IYoutubeThemeListener {
    private static final String TAG = "YouTubeDetailActivity";
    private static final String INTENT_URL = "INTENT_URL";
    private static final String INTENT_TITLE = "INTENT_TITLE";
    private static final String INTENT_DESCRIPTION = "INTENT_DESCRIPTION";

    YoutubeTheme youTubeTheme = new YoutubeTheme(this, this);

    TextView title, description;
    Button btndesc, btnplay;

    Date lastUpdateTheme = new Date();

    public static void startActivity(Activity activity, String url, String title, String description) {
        Intent intent = new Intent(activity, YouTubeDetailActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        intent.putExtra(INTENT_URL, url);
        intent.putExtra(INTENT_TITLE, title);
        intent.putExtra(INTENT_DESCRIPTION, description);

        activity.startActivity(intent,
                ActivityOptions.makeSceneTransitionAnimation(activity).toBundle());
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_youtube_detail);
        getWindow().setBackgroundDrawableResource(android.R.color.transparent);

        title = findViewById(R.id.title);
        description = findViewById(R.id.description);
        btndesc = findViewById(R.id.btndesc);
        btnplay = findViewById(R.id.btnplay);

        if (!getIntent().getStringExtra(INTENT_TITLE).isEmpty() ){
            title.setText(getIntent().getStringExtra(INTENT_TITLE));
        } else {
            title.setText("This Video Has No Title");
        }

        if (!getIntent().getStringExtra(INTENT_DESCRIPTION).isEmpty() ){
            description.setText(getIntent().getStringExtra(INTENT_DESCRIPTION));
        } else {
            description.setText("This Video Has No Description");
        }

        btndesc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                description.setVisibility(View.VISIBLE);
            }
        });

        btnplay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!getIntent().getStringExtra(INTENT_URL).isEmpty()) {
                    YouTubeVideoActivity.startActivity(YouTubeDetailActivity.this, getIntent().getStringExtra(INTENT_URL));
                } else {
                    Toast.makeText(YouTubeDetailActivity.this, "No Data Received", Toast.LENGTH_SHORT).show();
                }
            }
        });

        youTubeTheme.setTheme();
        hideNavigationBar();
        installNavigationListener();
    }

    void hideNavigationBar() {
        try {
            View decorView = getWindow().getDecorView();
// Hide both the navigation bar and the status bar.
// SYSTEM_UI_FLAG_FULLSCREEN is only available on Android 4.1 and higher, but as
// a general r ule, you should design your app to hide the status bar whenever you
// hide the navigation bar.
//        int uiOptions = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION | View.SYSTEM_UI_FLAG_FULLSCREEN;
//        decorView.setSystemUiVisibility(uiOptions);

            decorView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                    | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
        } catch (Exception e) {
            Debug.e(TAG, e);
        }
    }

    void installNavigationListener() {
        try {
            View decorView = getWindow().getDecorView();

            //this add event, will hide again if shown
            decorView.setOnSystemUiVisibilityChangeListener(new View.OnSystemUiVisibilityChangeListener() {
                @Override
                public void onSystemUiVisibilityChange(int visibility) {
                    // Note that system bars will only be "visible" if none of the
                    // LOW_PROFILE, HIDE_NAVIGATION, or FULLSCREEN flags are set.
                    if ((visibility & View.SYSTEM_UI_FLAG_FULLSCREEN) == 0) {
                        hideNavigationBar();
                    } else {
                        // adjustments to your UI, such as hiding the action bar or
                        // other navigational controls.
                    }
                }
            });
        } catch (Exception e) {
            Debug.e(TAG, e);
        }
    }

    void setBorderColor(int color, TextView textView){
        try {
            //utk set border text view
            StateListDrawable drawable = (StateListDrawable)textView.getBackground();
            DrawableContainer.DrawableContainerState dcs = (DrawableContainer.DrawableContainerState)drawable.getConstantState();
            Drawable[] drawableItems = dcs.getChildren();
            GradientDrawable selectedDrawable = (GradientDrawable)drawableItems[0]; // item 1
            GradientDrawable focusDrawable = (GradientDrawable)drawableItems[1]; // item 2

            selectedDrawable.setStroke((int) Util.dp2Px(4), color);
            focusDrawable.setStroke((int) Util.dp2Px(4), color);
        } catch (Exception e) {
            Debug.e(TAG, e);
        }
    }

    @Override
    public void setThemeUpdateDate(Date lastUpdateTheme) {
        try {
            this.lastUpdateTheme = lastUpdateTheme;
        } catch (Exception e) {
            Debug.e(TAG, e);
        }
    }


    @Override
    public void setListColorTheme(int color) {

    }

    @Override
    public void setTextAndSelectionColorTheme(ColorStateList colorSelection, ColorStateList colorText, int colorTextSelected, int colorTextFocus, int colorTextNormal, int colorSelectionSelected, int colorSelectionFocus) {
        try {
            if (colorSelection!=null){
                btnplay.setBackgroundTintList(colorSelection);
                btndesc.setBackgroundTintList(colorSelection);

                setBorderColor(colorSelectionFocus, title);
                setBorderColor(colorSelectionFocus, description);
            }
        } catch (Exception e) {
            Debug.e(TAG, e);
        }
    }

}

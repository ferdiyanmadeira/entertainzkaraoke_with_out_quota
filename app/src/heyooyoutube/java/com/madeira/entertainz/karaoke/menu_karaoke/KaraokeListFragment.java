package com.madeira.entertainz.karaoke.menu_karaoke;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.ScaleAnimation;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.QuickContactBadge;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.madeira.entertainz.controller.USBControl;
import com.madeira.entertainz.helper.RecyclerViewAdapter;
import com.madeira.entertainz.karaoke.DBLocal.JoinSongCountPlayed;
import com.madeira.entertainz.karaoke.DBLocal.JoinSongPlaylist;
import com.madeira.entertainz.karaoke.Debug;
import com.madeira.entertainz.karaoke.R;
import com.madeira.entertainz.karaoke.config.C;
import com.madeira.entertainz.karaoke.menu_playlist.PlaylistActivity;
import com.madeira.entertainz.karaoke.DBLocal.DbSong;
import com.madeira.entertainz.karaoke.DBLocal.JoinPlaylistDt;
import com.madeira.entertainz.karaoke.DBLocal.TPlaylist;
import com.madeira.entertainz.karaoke.DBLocal.TSongPlaylist;
import com.madeira.entertainz.karaoke.model_room_db.ModelRecentSearch;
import com.madeira.entertainz.karaoke.model_room_db.ModelSong;
import com.madeira.entertainz.karaoke.model_room_db.ModelPlaylist;
import com.madeira.entertainz.karaoke.player.ExoPlayerActivity;
import com.madeira.entertainz.library.PerformAsync2;
import com.madeira.entertainz.library.Util;
import com.madeira.entertainz.library.UtilRenderScript;
import com.madeira.entertainz.library.UtilWriteJSONFile;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import es.dmoral.toasty.Toasty;

public class KaraokeListFragment extends Fragment implements KaraokeTheme.IKaraokeThemeListener {
    static String TAG = "KaraokeListFragment";
    public static final String KEY_CATEGORY_ID = "CATEGORY_ID";
    public static final String KEY_CATEGORY_TITLE = "CATEGORY_TITLE";
    private int categoryId;
    private int playlistId = 0;


    KaraokeTheme karaokeTheme;
    List<JoinSongCountPlayed> songCountPlayedArrayList = new ArrayList<>();
    RecyclerViewAdapter adapter;
    RecyclerViewAdapter adapterPlaylist;

    View root;
    LinearLayout rootLL;
    RecyclerView recyclerView;
    LinearLayout searchLL;
    Button playlistButton;
    EditText searchET;
    RelativeLayout recentView;
    ImageView iconImageView;
    USBControl usbControl = new USBControl();
    private IKaraokeListListener mListener;
    TextView recentTV;
    ImageView searchHistoryButton;

    public KaraokeListFragment() {
        // Required empty public constructor
    }


    public static KaraokeListFragment newInstance(int prmCategoryId) {
        KaraokeListFragment fragment = new KaraokeListFragment();
        Bundle args = new Bundle();
        args.putInt(KEY_CATEGORY_ID, prmCategoryId);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {

        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        root = inflater.inflate(R.layout.fragment_karaoke_list, container, false);
        try {
            bind();
            karaokeTheme = new KaraokeTheme(getActivity(), this);
            karaokeTheme.setTheme();
            setOnActionObject();
            if (getArguments() != null) {
                categoryId = getArguments().getInt(KEY_CATEGORY_ID);
//            categoryTitle = getArguments().getString(KEY_CATEGORY_TITLE);
                playlistId = getArguments().getInt(KaraokeActivity.KEY_PLAYLIST_ID);
            } else {
                categoryId = 0;
            }
            fetchSongByCategory();
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
        return root;
    }

    @Override
    public void setThemeUpdateDate(Date lastUpdateTheme) {

    }

    @Override
    public void setListColorTheme(int color) {
        try {
            //untuk set round corner dan color
            rootLL.setBackgroundResource(R.drawable.tags_rounded_corners);
            GradientDrawable rootDrawable = (GradientDrawable) rootLL.getBackground();
            rootDrawable.setColor(color);

            searchLL.setBackgroundResource(R.drawable.tags_rounded_corners);
            GradientDrawable searchDrawable = (GradientDrawable) searchLL.getBackground();
            searchDrawable.setColor(color);
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    @Override
    public void setTextAndSelectionColorTheme(ColorStateList colorSelection, ColorStateList colorText, int colorTextSelected, int colorTextFocus, int colorTextNormal, int colorSelectionSelected, int colorSelectionFocus) {
        try {
            this.colorSelection = colorSelection;
            this.colorText = colorText;
            this.colorTextNormal = colorTextNormal;
            searchET.setTextColor(colorTextNormal);
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    void bind() {
        try {
            rootLL = root.findViewById(R.id.rootLL);
            recyclerView = root.findViewById(R.id.recyclerView);
            searchLL = root.findViewById(R.id.searchLL);
            playlistButton = root.findViewById(R.id.playlistButton);
            searchET = root.findViewById(R.id.searchET);
            recentView = root.findViewById(R.id.recent_view);
            iconImageView = root.findViewById(R.id.icon_image_view);
            recentTV = root.findViewById(R.id.recent_tv);
            searchHistoryButton = root.findViewById(R.id.search_history_button);
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    void fetchSongByCategory() {
        setRecyclerView();
        if (categoryId != 0) {
            ModelSong.getSongByCategory(getActivity(), categoryId, new ModelSong.IGetListSongByCategory() {
                @Override
                public void onGetListSongByCategory(List<JoinSongCountPlayed> tSongList) {
                    songCountPlayedArrayList = tSongList;
                    adapter.notifyDataSetChanged();
                }
            });
        } else {
            ModelSong.getJoinSong(getActivity(), new ModelSong.IGetListJoinSong() {
                @Override
                public void onGetListJoinSong(List<JoinSongCountPlayed> tSongList) {
                    songCountPlayedArrayList = tSongList;
                    adapter.notifyDataSetChanged();
                }
            });
        }

    }

    void setRecyclerView() {
        try {
            adapter = new RecyclerViewAdapter(new RecyclerViewAdapter.IRecyclerViewAdapter() {
                @Override
                public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
                    View view = LayoutInflater.from(getActivity()).inflate(R.layout.cell_karaoke_list, parent, false);
                    ItemViewHolder item = new ItemViewHolder(view);
                    return item;
                }

                @Override
                public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
                    try {
                        if (songCountPlayedArrayList.size() != 0) {
                            ItemViewHolder item = (ItemViewHolder) holder;
                            item.bind(songCountPlayedArrayList.get(position), position);
                        }
                    } catch (Exception ex) {
                        Debug.e(TAG, ex);
                    }
                }

                @Override
                public int getItemCount() {
                    return songCountPlayedArrayList.size();
                }
            });

            GridLayoutManager gridLayoutManager = new GridLayoutManager(getActivity(), 5);
            recyclerView.setLayoutManager(gridLayoutManager);
//            adapter.setHasStableIds(true);
            recyclerView.setAdapter(adapter);
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    class ItemViewHolder extends RecyclerView.ViewHolder {
        private static final String TAG = "ItemViewHolder";

        View root;
        TextView tvTitle;
        TextView tvSinger;
        ImageView thumbnailIV;
        TextView textDuration;
        TextView textCount;

        public ItemViewHolder(View itemView) {
            super(itemView);
            try {
                root = itemView;
                selectedView = root;
                tvTitle = itemView.findViewById(R.id.text_title);
                tvSinger = itemView.findViewById(R.id.text_singer);
                thumbnailIV = itemView.findViewById(R.id.thumbnailIV);
                textDuration = itemView.findViewById(R.id.text_dur);
                textCount = itemView.findViewById(R.id.text_count_played);
            } catch (Exception ex) {
                Debug.e(TAG, ex);
            }

        }

        public void bind(JoinSongCountPlayed tSong, int pos) {
            try {
                tvTitle.setText(tSong.songName);
                tvSinger.setText(tSong.artist);
                int tempDuration = Integer.valueOf(tSong.duration) * 1000;
                textDuration.setText(Util.convertMilisToTime(tempDuration));
                String count = String.valueOf(tSong.totalCount);
                textCount.setText(count);
//                textCount.setText(count);

                if (tSong.thumbnail != null) {
                    UtilRenderScript.fetchBitmap(getContext(), tSong.thumbnail, 300, thumbnailIV);
              /*  Picasso.with(getActivity())
                        .load(tSong.thumbnail)
                        .into(thumbnailIV);*/
                }


                if (colorSelection != null) root.setBackgroundTintList(colorSelection);
//            untuk merubah warna text
                if (colorText != null) {
                    tvTitle.setTextColor(colorText);
                    tvSinger.setTextColor(colorText);
                }


                root.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
//                    mListener.onListClick(tSong.songId);
                        showDetailSong(tSong);
                    }
                });
                root.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                    @Override
                    public void onFocusChange(View v, boolean hasFocus) {
                        if (hasFocus) {
                            updateSelectedViewUi(v, true);
                        } else {
                            updateSelectedViewUi(null, false);

                        }
                    }
                });
            } catch (Exception ex) {
                Debug.e(TAG, ex);
            }
        }

        void selectView(View v) {
            //clear selection
            /*if (selectedView != null) {
                selectedView.setSelected(false);
            }

            //make selection
            selectedView = v;
            selectedView.setSelected(true);*/
        }
    }

    ColorStateList colorSelection, colorText;
    int colorTextNormal;

    //function untuk declare semua action dari object di activity
    void setOnActionObject() {
        try {
            searchET.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {
                    searchSong(s.toString());
                }
            });

            playlistButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    PlaylistActivity.startActivity(getActivity(), 0);
                }
            });

            searchET.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View v, boolean hasFocus) {
                    try {
                        if (!hasFocus) {
//                            Util.closeKeyboard(getActivity());
                            searchET.setHintTextColor(getActivity().getResources().getColor(R.color.texthintcolor_unfocus));
                            String keyword = searchET.getText().toString();
                            if (!keyword.equals(""))
                                addRecentSearch(keyword);
                        } else {
                            Util.showKeyboard(getActivity());
                            searchET.setHintTextColor(colorTextNormal);
                        }
                    } catch (Exception ex) {
                        Debug.e(TAG, ex);
                    }

                }
            });
            recentView.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View v, boolean hasFocus) {
                    try {
                        if (hasFocus) {
                            iconImageView.setBackgroundResource(R.drawable.ic_recent_black);
                            recentTV.setTextColor(getContext().getColor(R.color.black));
                        } else {
                            iconImageView.setBackgroundResource(R.drawable.ic_recent_white);
                            recentTV.setTextColor(getContext().getColor(R.color.white));
                        }
                    } catch (Exception ex) {
                        Debug.e(TAG, ex);
                    }
                }
            });

            searchHistoryButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        showRecentSearch();
                    } catch (Exception ex) {
                        Debug.e(TAG, ex);
                    }
                }
            });

            recentView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        showRecentSearch();
                    } catch (Exception ex) {
                        Debug.e(TAG, ex);
                    }
                }
            });
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    /**
     * untuk tambahkan recent search ke local db
     */
    void addRecentSearch(String songName) {

        ModelRecentSearch.addRecentSearch(getActivity(), songName, C.SEARCH_SONG_KARAOKE, new ModelRecentSearch.IAddRecentSearch() {
            @Override
            public void onAddRecentSearch(int result) {

            }
        });

    }

    /**
     * untuk mencari lagu, berdasarkan keyword
     */
    void searchSong(String prmKeyword) {
        try {
            Context mContext = getActivity();
            if (mContext != null) {
                if (categoryId == 0) {
                    ModelSong.getSearchSong(mContext, prmKeyword, new ModelSong.IGetListSearchSong() {
                        @Override
                        public void onGetListSearchSong(List<JoinSongCountPlayed> tSongList) {
                            songCountPlayedArrayList = tSongList;
                            adapter.notifyDataSetChanged();
                        }
                    });
                } else {
                    ModelSong.getSearchSongByCategory(mContext, prmKeyword, categoryId, new ModelSong.IGetListSearchSongByCategory() {
                        @Override
                        public void onGetListSearchSongByCategory(List<JoinSongCountPlayed> tSongList) {
                            songCountPlayedArrayList = tSongList;
                            adapter.notifyDataSetChanged();
                        }
                    });
                }
            }

        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    public void setCallback(IKaraokeListListener listener) {
        try {
            this.mListener = listener;
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    public interface IKaraokeListListener {
        // TODO: Update argument type and name
        void onListClick(int songId);
    }

    void showDetailSong(JoinSongCountPlayed prmTSong) {
        try {
            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
            LayoutInflater inflater = this.getLayoutInflater();
            View dialogView = inflater.inflate(R.layout.alert_karaoke_song, null);
            dialogBuilder.setView(dialogView);

            ImageView thumbnailIV = (ImageView) dialogView.findViewById(R.id.thumbnailIV);
            TextView titleTV = (TextView) dialogView.findViewById(R.id.titleTV);
            TextView singerTV = (TextView) dialogView.findViewById(R.id.singerTV);
            Button playSongButton = (Button) dialogView.findViewById(R.id.playSongButton);
            Button addToPlaylistButton = (Button) dialogView.findViewById(R.id.addToPlaylistButton);
            if (prmTSong.thumbnail != null) {
                UtilRenderScript.fetchBitmap(getActivity(), prmTSong.thumbnail, 300, thumbnailIV);
            }
            titleTV.setText(prmTSong.songName);
            singerTV.setText(prmTSong.artist);
            AlertDialog alertDialog = dialogBuilder.create();

            playSongButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ArrayList<JoinSongCountPlayed> tSongArrayList = new ArrayList<>();
                    tSongArrayList.add(prmTSong);
                    //harus di convert ke joinsongplaylist, kaalau tidak di convert akan loading lama di playlist, jika ada song playlist nya banyak
                    ArrayList<JoinSongPlaylist> joinSongPlaylists = new ArrayList<>();
                    for (JoinSongCountPlayed itemSongPlaylist : tSongArrayList) {
                        JoinSongPlaylist tSong = new JoinSongPlaylist();
                        tSong.songId = String.valueOf(itemSongPlaylist.songId);
                        tSong.songName = itemSongPlaylist.songName;
                        tSong.artist = itemSongPlaylist.artist;
                        tSong.songURL = itemSongPlaylist.songURL;
                        tSong.vocalSound = Integer.valueOf(itemSongPlaylist.vocalSound);
                        tSong.duration = Integer.valueOf(itemSongPlaylist.duration);
                        tSong.thumbnail = itemSongPlaylist.thumbnail;
                        tSong.iv = itemSongPlaylist.iv;
                        tSong.key = itemSongPlaylist.key;
                        tSong.lyric = itemSongPlaylist.lyric;
                        joinSongPlaylists.add(tSong);
                    }
                    ExoPlayerActivity.startActivity(getActivity(), joinSongPlaylists, 0);
                    alertDialog.dismiss();

                }
            });

            addToPlaylistButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (playlistId == 0) {
                        showPlaylist(prmTSong);
                        alertDialog.dismiss();
                    } else {
                        savePlaylist(prmTSong, "", playlistId, true);
                        alertDialog.dismiss();
                    }

                }
            });

            if (playlistId == 0)
                playSongButton.setVisibility(View.VISIBLE);
            else
                playSongButton.setVisibility(View.GONE);

            alertDialog.show();
            WindowManager.LayoutParams layoutParams = alertDialog.getWindow().getAttributes();
            layoutParams.width = ViewGroup.LayoutParams.WRAP_CONTENT;
            alertDialog.show();
            alertDialog.getWindow().setLayout(430, WindowManager.LayoutParams.WRAP_CONTENT);
            Util.hideNavigationBar(getActivity());
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }

    }

    void showPlaylist(JoinSongCountPlayed prmTSong) {
        ModelPlaylist.getPlaylistDt(getActivity(), new ModelPlaylist.IGetPlaylistDt() {
            @Override
            public void onGetListPlaylistDt(List<JoinPlaylistDt> tPlaylistList) {
                showDialogPlaylist(tPlaylistList, prmTSong);
            }
        });
    }

    void showDialogPlaylist(List<JoinPlaylistDt> tPlaylistList, JoinSongCountPlayed prmTSong) {
        try {

            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
            LayoutInflater inflater = this.getLayoutInflater();
            View dialogView = inflater.inflate(R.layout.alert_listof_playlist, null);
            dialogBuilder.setView(dialogView);

            RecyclerView recyclerView = dialogView.findViewById(R.id.recyclerView);
            LinearLayout notExistPlaylistLL = dialogView.findViewById(R.id.notExistPlaylistLL);
            LinearLayout existPlaylistLL = dialogView.findViewById(R.id.existPlaylistLL);
            EditText playlistET = dialogView.findViewById(R.id.playlistET);
            Button saveButton = dialogView.findViewById(R.id.saveButton);
            Button addPlaylistButton = dialogView.findViewById(R.id.addPlaylistButton);
            Button cancelButton = dialogView.findViewById(R.id.cancelButton);
            existPlaylistLL.setVisibility(View.VISIBLE);
            saveButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (!TextUtils.isEmpty(playlistET.getText().toString()))
                        savePlaylist(prmTSong, playlistET.getText().toString(), 0, true);
                    else {
                        playlistET.setError(getString(R.string.playlistMustFilled));
                        playlistET.requestFocus();
                    }
                }
            });

            addPlaylistButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    notExistPlaylistLL.setVisibility(View.VISIBLE);
                    addPlaylistButton.setVisibility(View.GONE);
                }
            });
            cancelButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    notExistPlaylistLL.setVisibility(View.GONE);
                    addPlaylistButton.setVisibility(View.VISIBLE);
                }
            });

            setRecyclerViewPlaylist(recyclerView, tPlaylistList, prmTSong);

            AlertDialog alertDialog = dialogBuilder.create();
            alertDialog.show();
            WindowManager.LayoutParams layoutParams = alertDialog.getWindow().getAttributes();
            layoutParams.width = ViewGroup.LayoutParams.WRAP_CONTENT;
            alertDialog.show();
            alertDialog.getWindow().setLayout(530, WindowManager.LayoutParams.WRAP_CONTENT);
            Util.hideNavigationBar(getActivity());
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    void savePlaylist(JoinSongCountPlayed prmTSong, String prmPlaylistName, int prmPlaylistId, boolean fgIntent) {
        ModelPlaylist.saveSongPlaylist(getActivity(), prmTSong.songId, prmPlaylistName, prmPlaylistId, new ModelPlaylist.ISaveSongPlaylist() {
            @Override
            public void onSaveSongPlaylist(int result) {
                try {
                    int i = (int) result;
                    //1 success
                    //2 duplicate playlist
                    //0 failed
                    //3 song already in playlist
                    if (i == 1) {
                        // jika fgintent true,
                        if (fgIntent) {
                            Toasty.success(getActivity(), getString(R.string.successAddSong)).show();
                            showConfirmSelectingSong(prmPlaylistId);
                        } else {
                            PlaylistActivity.startActivity(getActivity(), prmPlaylistId);
                            getActivity().finish();
                        }

                        usbControl.writePlaylistFile(getContext());
//                        writePlaylistSongsToFile();
                    } else if (i == 2) {
                        Toasty.error(getActivity(), getString(R.string.playlistAlreadyExists)).show();
                    } else if (i == 3) {
                        Toasty.error(getActivity(), getString(R.string.songAlreadyExists)).show();
                    }
                } catch (Exception ex) {
                    Debug.e(TAG, ex);
                }
            }
        });
    }


    void setRecyclerViewPlaylist(RecyclerView prmRecyclerView, List<JoinPlaylistDt> prmJoinPlaylistDts, JoinSongCountPlayed prmTSong) {
        try {
            adapterPlaylist = new RecyclerViewAdapter(new RecyclerViewAdapter.IRecyclerViewAdapter() {
                @Override
                public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
                    View view = LayoutInflater.from(getActivity()).inflate(R.layout.cell_listof_playlist, parent, false);
                    ItemViewHolderPlaylist item = new ItemViewHolderPlaylist(view);
                    return item;
                }

                @Override
                public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
                    ItemViewHolderPlaylist item = (ItemViewHolderPlaylist) holder;
                    item.bind(prmJoinPlaylistDts.get(position), position, prmTSong);
                }

                @Override
                public int getItemCount() {
                    return prmJoinPlaylistDts.size();
                }
            });

            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
            prmRecyclerView.setLayoutManager(linearLayoutManager);
            prmRecyclerView.setAdapter(adapterPlaylist);
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    class ItemViewHolderPlaylist extends RecyclerView.ViewHolder {
        private static final String TAG = "ItemViewHolderPlaylist";

        View root;
        TextView playlistTV;
        TextView sumOfSongsTV;
        TextView totalDurationTV;
        LinearLayout selectioncolor;

        public ItemViewHolderPlaylist(View itemView) {
            super(itemView);
            root = itemView;
            playlistTV = itemView.findViewById(R.id.playlistTV);
            sumOfSongsTV = itemView.findViewById(R.id.sumOfSongsTV);
            totalDurationTV = itemView.findViewById(R.id.totalDurationTV);
            selectioncolor = itemView.findViewById(R.id.selectioncolor);
        }

        public void bind(JoinPlaylistDt tPlaylist, int pos, JoinSongCountPlayed tSong) {
            try {
                playlistTV.setText(tPlaylist.playlistName);
                int milis = tPlaylist.sumOfDuration * 1000;
                totalDurationTV.setText(Util.convertMilisToTime(milis));
                sumOfSongsTV.setText(String.valueOf(tPlaylist.sumOfSong));

                if ((pos % 2) == 0) {
                    // number is even
                    int color = ContextCompat.getColor(getContext(), R.color.first_row_table_dialog);
                    root.setBackgroundColor(color);
                } else {
                    // number is odd
                    int color = ContextCompat.getColor(getContext(), R.color.second_row_table_dialog);
                    root.setBackgroundColor(color);
                }

                selectioncolor.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        savePlaylist(tSong, "", tPlaylist.playlistId, false);
                    }
                });
            } catch (Exception ex) {
                Debug.e(TAG, ex);
            }
        }

    }

    void updateSelectedViewUi(View view, boolean hasFocus) {
        try {
            if (selectedView != null) {
                scaleView(selectedView, 1.1f, 1);
                ViewCompat.setTranslationZ(selectedView, 0);
            }

            if (hasFocus) {
                selectedView = view;
                ViewCompat.setTranslationZ(selectedView, 100);
                scaleView(selectedView, 1, 1.1f);
            } else {
                //remove selected & focus dari grid
                selectedView = null;
            }
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    public void scaleView(View v, float startScale, float endScale) {
        try {
            Animation anim = new ScaleAnimation(
                    startScale, endScale, // Start and end values for the X axis scaling
                    startScale, endScale, // Start and end values for the Y axis scaling
                    Animation.RELATIVE_TO_SELF, 0.5f, // Pivot point of X scaling
                    Animation.RELATIVE_TO_SELF, 0.5f); // Pivot point of Y scaling
            anim.setFillAfter(true); // Needed to keep the result of the animation
            anim.setDuration(100);
            v.startAnimation(anim);
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    static View selectedView;

    /**
     * show recent lagu yang pernah dicari
     */
    void showRecentSearch() {
        try {
            RecentSearchActivity.startActivity(getActivity());
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
//        super.onActivityResult(requestCode, resultCode, data);
        try {
            Log.i(TAG, "onActivityResult: " + requestCode);

            //check apakah ada request code dari  ??
            //kalo ada artinya user melakukan change order
            if (RecentSearchActivity.checkRequestCode(requestCode)) {
                String keyword = data.getStringExtra(RecentSearchActivity.INTENT_RESULT);
                searchET.setText(keyword);
                searchSong(keyword);
            }
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }


    /**
     * untuk confirm apakah mau select song lagi atau langsung ke playlist
     */
    void showConfirmSelectingSong(int prmPlaylistId) {
        try {
            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
            LayoutInflater inflater = this.getLayoutInflater();
            View dialogView = inflater.inflate(R.layout.alert_back_selecting, null);
            dialogBuilder.setView(dialogView);
            Button backSelectionButton = dialogView.findViewById(R.id.back_button);
            Button gotoPlaylistButton = dialogView.findViewById(R.id.goto_button);
            AlertDialog alertDialog = dialogBuilder.create();
            backSelectionButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    alertDialog.dismiss();

                }
            });

            gotoPlaylistButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    alertDialog.dismiss();
                    PlaylistActivity.startActivity(getActivity(), prmPlaylistId);
                    getActivity().finish();
                }
            });

            alertDialog.show();
            WindowManager.LayoutParams layoutParams = alertDialog.getWindow().getAttributes();
            layoutParams.width = ViewGroup.LayoutParams.WRAP_CONTENT;
            alertDialog.show();
            alertDialog.getWindow().setLayout(430, WindowManager.LayoutParams.WRAP_CONTENT);
            Util.hideNavigationBar(getActivity());
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }
}

package com.madeira.entertainz.karaoke.player;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Surface;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.VideoView;

import com.madeira.entertainz.helper.controller.media.VideoControllerView;
import com.madeira.entertainz.helper.equalizer.PopupEQ;
import com.madeira.entertainz.karaoke.DBLocal.DbAccess;
import com.madeira.entertainz.karaoke.DBLocal.TLogSong;
import com.madeira.entertainz.karaoke.DBLocal.TSong;
import com.madeira.entertainz.karaoke.Debug;
import com.madeira.entertainz.karaoke.R;
import com.madeira.entertainz.karaoke.config.C;
import com.madeira.entertainz.karaoke.config.Global;
import com.madeira.entertainz.library.PerformAsync2;
import com.madeira.entertainz.library.Util;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import static android.view.KeyEvent.KEYCODE_MENU;

public class PlayerActivity extends AppCompatActivity {
    static String TAG = "PlayerActivity";
    public static String KEY_TSONG = "KEY_TSONG";
    public static String KEY_LASTPOSITION = "KEY_LASTPOSITION";

    ArrayList<TSong> songArrayList = new ArrayList<>();
    boolean isMute = true;

    VideoView videoView;
    static MediaPlayer mediaPlayer;
    //    static List<TSongPlaylist> tSongPlaylists = new ArrayList<>();
    static TSong tSong = null;
    static int lastPosition = 0;
    TextView title;
    TextView artist;
    TextView duration;
    static LinearLayout controlVocalLL;
    static ImageView muteIV;
    static ImageView unMuteIV;

    Runnable runCountDown;
    static int tempDuration = 0;
    static VideoControllerView controller;
    private boolean mIsComplete;
    ProgressBar progressBar;
    PopupEQ eq;
    RelativeLayout rootRL;
    Handler handler = new Handler();
    Runnable runnable;
    int count = 0;

    public static void startActivity(Activity activity, ArrayList<TSong> tSongArrayList, int prmLastPosition) {
        try {
            Intent intent = new Intent(activity, PlayerActivity.class);
            intent.putExtra(KEY_TSONG, tSongArrayList);
            intent.putExtra(KEY_LASTPOSITION, prmLastPosition);
            activity.startActivity(intent);
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            setContentView(R.layout.activity_player);
            songArrayList = (ArrayList<TSong>) getIntent().getSerializableExtra(KEY_TSONG);
            lastPosition = getIntent().getIntExtra(KEY_LASTPOSITION, 0);
            bind();
            if (songArrayList.size() != 0) {
                playSong(songArrayList, lastPosition);
            }
            Util.hideNavigationBar(PlayerActivity.this);

            String languageId = Global.getLanguageId();
            Util.setLanguage(this, languageId);
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    void bind() {
        videoView = findViewById(R.id.videoView);
        title = findViewById(R.id.title);
        artist = findViewById(R.id.artist);
        duration = findViewById(R.id.duration);
        progressBar = findViewById(R.id.progressBar);
        controlVocalLL = findViewById(R.id.controlVocalLL);
        muteIV = findViewById(R.id.muteIV);
        unMuteIV = findViewById(R.id.unMuteIV);
        rootRL = findViewById(R.id.rootRL);

    }

    public void playSong(ArrayList<TSong> prmTSongList, int position) {
        //clear cache dir before play
//        deleteTempFiles(getCacheDir());
        try {
            lastPosition = position;
            tSong = prmTSongList.get(position);
            title.setText(tSong.songName);
            artist.setText(tSong.artist);
            tempDuration = Integer.valueOf(tSong.duration) * 1000;
            duration.setText(Util.convertMilisToTimeWithText(tempDuration));
//        progressBar.setVisibility(View.VISIBLE);

            if (prmTSongList != null) {

                File file = new File(tSong.songURL);
                int length = (int) file.length();
//            String url = Util.replaceRAF(tSong.songURL);

//            String url = Util.splitFile(tSong.songURL, length);
                Uri url = Uri.fromFile(file);
                videoView.setVideoPath(url.toString());
          /*  String fileName = Util.splitFileAndReplace(tSong.songURL, PlayerActivity.this);
            if (fileName != null) {
                videoView.setVideoPath(fileName);
            }
*/
           /* try {
                File path= Util.decrypt(tSong.songURL, PlayerActivity.this);
                videoView.setVideoPath(path.getAbsolutePath());
            } catch (IOException e) {
                e.printStackTrace();
            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
            } catch (NoSuchPaddingException e) {
                e.printStackTrace();
            } catch (InvalidKeyException e) {
                e.printStackTrace();
            }*/
                videoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                    @Override
                    public void onCompletion(MediaPlayer mp) {

//
                        if (lastPosition == prmTSongList.size() - 1)
//                        lastPosition = 0;
                            return;
                        else
                            lastPosition = lastPosition + 1;
                        playSong(prmTSongList, lastPosition);
                        mIsComplete = true;
                    }
                });



//                videoView.setOnCapturedPointerListener(new );


                controller = new VideoControllerView.Builder(PlayerActivity.this, new VideoControllerView.MediaPlayerControlListener() {
                    @Override
                    public void start(MediaPlayer mp) {
                        if (null != videoView) {
                            progressBar.setVisibility(View.GONE);
                            videoView.start();
                            mediaPlayer = mp;
                            mIsComplete = false;
                            muteVocal(true, PlayerActivity.this);
                            controller.refreshProgress();
                            mediaPlayer.setOnBufferingUpdateListener(new MediaPlayer.OnBufferingUpdateListener() {
                                @Override
                                public void onBufferingUpdate(MediaPlayer mp, int percent) {
                                    int percentage = percent;
                                    if (percentage >= 100)
                                        progressBar.setVisibility(View.GONE);
//                                else
//                                    progressBar.setVisibility(View.VISIBLE);
                                }
                            });
                            mediaPlayer.setOnErrorListener(new MediaPlayer.OnErrorListener() {
                                @Override
                                public boolean onError(MediaPlayer mp, int what, int extra) {
                                    progressBar.setVisibility(View.GONE);
                                    return false;
                                }
                            });
                            videoView.setFocusable(true);
                            videoView.setClickable(true);
                            increaseLogSong(tSong.songId);
                        }
                    }

                    @Override
                    public void pause() {
                        if (null != videoView) {
                            videoView.pause();
                        }
                    }

                    @Override
                    public int getDuration() {
                        int duration = 0;
                        try {
                            if (null != videoView)
                                duration = videoView.getDuration();
                            else
                                return 0;
                        } catch (Exception ex) {

                        }
                        return duration;
                    }

                    @Override
                    public int getCurrentPosition() {
                        int position = 0;
                        try {
                            if (null != videoView)
                                position = videoView.getCurrentPosition();
                            else
                                position = 0;
                        } catch (Exception ex) {

                        }
                        return position;
                    }

                    @Override
                    public void seekTo(int position) {
                        if (null != videoView) {
                            videoView.seekTo(position);
                        }
                    }

                    @Override
                    public boolean isPlaying() {
                        if (null != videoView) {
//                        progressBar.setVisibility(View.GONE);
//                        controller.refreshProgress();
                            return videoView.isPlaying();
                        } else {
//                        progressBar.setVisibility(View.VISIBLE);
                            return false;
                        }
                    }

                    @Override
                    public boolean isComplete() {
                        progressBar.setVisibility(View.GONE);
                        return mIsComplete;
                    }

                    @Override
                    public int getBufferPercentage() {
                        return 0;
                    }

                    @Override
                    public boolean isFullScreen() {
                        return getRequestedOrientation() == ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE ? true : false;

                    }

                    @Override
                    public void toggleFullScreen() {
                        if (isFullScreen()) {
                            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
                        } else {
                            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
                        }
                    }

                    @Override
                    public void exit() {

                    }

                    @Override
                    public void unFocused() {

                    }

                    @Override
                    public void nextSong() {
                        if (null != videoView) {

                            if (prmTSongList.size() > lastPosition + 1)
                                lastPosition = lastPosition + 1;
                            else
                                lastPosition = 0;
                            controller.unShow();
//                    controller=null;
//                    controller.refreshProgress();
                            playSong(prmTSongList, lastPosition);
                        }
                    }

                    @Override
                    public void prevSong() {
                        if (null != videoView) {

                            if (prmTSongList.size() <= lastPosition + 1)
                                lastPosition = lastPosition - 1;
                            else
                                lastPosition = 0;
                            controller.unShow();
//                    controller=null;
//                    controller.refreshProgress();
//                    progressBar.setVisibility(View.VISIBLE);
                            playSong(prmTSongList, lastPosition);
                        }
                    }

                    @Override
                    public void equalizer() {
                        showEqualizer();
                    }
                })
//                                .withVideoTitle(tvTitle.getText().toString())
                        .withVideoSurfaceView(videoView)//to enable toggle display controller view
                        .canControlBrightness(false)
                        .canControlVolume(false)
                        .canSeekVideo(false)
                        .build((FrameLayout) findViewById(R.id.video_layout));//layout container that hold video play view


//            controller.refreshProgress();

//                videoView.setFocusable(true);
//                videoView.requestFocus();

            }
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    public void stopKaraoke() {
        try {
            if (videoView.isPlaying())
                videoView.stopPlayback();
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    public static void muteVocal(boolean mute, Context context) {
        try {
            controlVocalLL.setVisibility(View.VISIBLE);
            if (mediaPlayer != null) {

                if (mute) {
                    muteIV.setVisibility(View.VISIBLE);
                    unMuteIV.setVisibility(View.GONE);
                    if (Integer.valueOf(tSong.vocalSound) == 2) {
                        mediaPlayer.setVolume(1f, 0f);
                        if (!mediaPlayer.isPlaying())
                            mediaPlayer.start();
//                Toast.makeText(context, "Vocal sound is mute", Toast.LENGTH_LONG).show();
                    } else if (Integer.valueOf(tSong.vocalSound) == 1) {
                        mediaPlayer.setVolume(0f, 1f);
//                mediaPlayer.setVolume(1f, 0f);
                        if (!mediaPlayer.isPlaying())
                            mediaPlayer.start();
//                Toast.makeText(context, "Vocal sound is mute", Toast.LENGTH_LONG).show();
                    } else {
                        mediaPlayer.setVolume(1f, 1f);
                        if (!mediaPlayer.isPlaying())
                            mediaPlayer.start();
//                Toast.makeText(context, "Can't mute vocal sound", Toast.LENGTH_LONG).show();
                    }
                } else {
                    muteIV.setVisibility(View.GONE);
                    unMuteIV.setVisibility(View.VISIBLE);
                    mediaPlayer.setVolume(1f, 1f);
                    if (!mediaPlayer.isPlaying())
                        mediaPlayer.start();
//            Toast.makeText(context, "Vocal sound is unmute", Toast.LENGTH_LONG).show();
                }

                Animation fadeOut = new AlphaAnimation(1, 0);
                fadeOut.setInterpolator(new AccelerateInterpolator()); // and this
                fadeOut.setStartOffset(3000);
                fadeOut.setDuration(3000);

                fadeOut.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                        controlVocalLL.setVisibility(View.GONE);
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {

                    }
                });
                controlVocalLL.startAnimation(fadeOut);
            }
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    @Override
    public void onBackPressed() {
        try {
            if (!controller.isShowing()) {
                stopKaraoke();
                overridePendingTransition(0, 0);
            } else {
                controller.unShow();
                return;
            }
            super.onBackPressed();
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }

    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        try {
            if (event.getKeyCode() == KEYCODE_MENU) {
                isMute = isMute == true ? false : true;
                muteVocal(isMute, this);
            }

            Util.hideNavigationBar(PlayerActivity.this);

        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
        return super.onKeyUp(keyCode, event);
    }

    private boolean deleteTempFiles(File file) {
        if (file.isDirectory()) {
            File[] files = file.listFiles();
            if (files != null) {
                for (File f : files) {
                    if (f.isDirectory()) {
                        deleteTempFiles(f);
                    } else {
                        f.delete();
                    }
                }
            }
        }
        return file.delete();
    }


    void showEqualizer() {
        try {
            if (mediaPlayer == null) return;
            eq = new PopupEQ(this, mediaPlayer.getAudioSessionId(), Surface.ROTATION_0);
            eq.show(rootRL);
        } catch (Exception ex) {
            Debug.e(TAG, ex);
        }
    }

    void increaseLogSong(String prmSongId) {
        try {
            int increase = 1000;
            runnable = new Runnable() {
                @Override
                public void run() {
                    count = count + increase;
                    if (count > C.LIMIT_COUNT_SONG) {
                        try {
                            int songId = Integer.valueOf(prmSongId);
                            PerformAsync2.run(performAsync ->
                            {
                                int result = 0;
                                try {
                                    DbAccess dbAccess = DbAccess.Instance.create(PlayerActivity.this);
                                    DbAccess.DaoAccessTLogSong dbDaoAccessTLogSong = dbAccess.daoAccessTLogSong();
                                    int rowNumber = dbDaoAccessTLogSong.existSong(songId);
                                    if (rowNumber == 0) {
                                        TLogSong tLogSong = new TLogSong();
                                        tLogSong.songId = String.valueOf(songId);
                                        tLogSong.totalCount = 1;
                                        dbDaoAccessTLogSong.insert(tLogSong);
                                        result = 1;

                                    } else {
                                        dbDaoAccessTLogSong.increaseCount(songId);
                                        result = 1;
                                    }
                                    List<TLogSong> tLogSongList = dbDaoAccessTLogSong.getAll();

                                    dbAccess.close();
                                } catch (Exception ex) {
                                    Log.d(TAG, "line : " + ex.getStackTrace()[0].getLineNumber() + ex.getMessage());
                                    result = 0;
                                }

                                return result;

                            }).setCallbackResult(result ->
                            {
                                int prm = (int) result;
                                if (prm == 1) {

                                }
                                handler.removeCallbacks(runnable);
                            });
                        } catch (Exception ex) {
                            Debug.e(TAG, ex);
                        }

                    } else {
                        //post delayed runnable selama 1 detik
                        handler.postDelayed(runnable, increase);
                    }
                }
            };
            //post delayed runnable selama 1 detik
            handler.postDelayed(runnable, increase);
        }catch (Exception ex)
        {
            Debug.e(TAG, ex);
        }
    }

}

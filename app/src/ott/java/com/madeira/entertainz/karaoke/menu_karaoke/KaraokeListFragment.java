package com.madeira.entertainz.karaoke.menu_karaoke;

import android.app.AlertDialog;
import android.content.res.ColorStateList;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.GradientDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.ScaleAnimation;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.madeira.entertainz.helper.RecyclerViewAdapter;
import com.madeira.entertainz.karaoke.R;
import com.madeira.entertainz.karaoke.menu_playlist.PlaylistActivity;
import com.madeira.entertainz.karaoke.player.PlayerActivity;
import com.madeira.entertainz.karaoke.DBLocal.DbAccess;
import com.madeira.entertainz.karaoke.DBLocal.JoinPlaylistDt;
import com.madeira.entertainz.karaoke.DBLocal.TPlaylist;
import com.madeira.entertainz.karaoke.DBLocal.TSong;
import com.madeira.entertainz.karaoke.DBLocal.TSongPlaylist;
import com.madeira.entertainz.library.PerformAsync2;
import com.madeira.entertainz.library.Util;
import com.madeira.entertainz.library.UtilRenderScript;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import es.dmoral.toasty.Toasty;

public class KaraokeListFragment extends Fragment implements KaraokeTheme.IKaraokeTheme {
    String TAG = "YoutubeListFragment";

    public static final String KEY_CATEGORY_ID = "CATEGORY_ID";
    public static final String KEY_CATEGORY_TITLE = "CATEGORY_TITLE";
    private boolean isLoading;

//    List<Uri>

    private int categoryId;
    //    private String categoryTitle;
    private int playlistId = 0;

    KaraokeTheme karaokeTheme;
    List<TSong> tSongList = new ArrayList<>();
    RecyclerViewAdapter adapter;
    RecyclerViewAdapter adapterPlaylist;

    View root;
    LinearLayout rootLL;
    RecyclerView recyclerView;
    LinearLayout searchLL;
    Button playlistButton;
    EditText searchET;
    //    TextView songCategoryTV;
    private IKaraokeListListener mListener;


    public KaraokeListFragment() {
        // Required empty public constructor
    }


    public static KaraokeListFragment newInstance(int prmCategoryId) {
        KaraokeListFragment fragment = new KaraokeListFragment();
        Bundle args = new Bundle();
        args.putInt(KEY_CATEGORY_ID, prmCategoryId);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            categoryId = getArguments().getInt(KEY_CATEGORY_ID);
//            categoryTitle = getArguments().getString(KEY_CATEGORY_TITLE);
            playlistId = getArguments().getInt(KaraokeActivity.KEY_PLAYLIST_ID);
        } else {
            categoryId = 0;
        }
        fetchSongCategory();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        root = inflater.inflate(R.layout.fragment_karaoke_list, container, false);
        bind();
        karaokeTheme = new KaraokeTheme(getActivity(), this);
        karaokeTheme.setTheme();
        setOnActionObject();
        return root;
    }

    @Override
    public void setThemeUpdateDate(Date lastUpdateTheme) {

    }

    @Override
    public void setListColorTheme(int color) {
        //untuk set round corner dan color
        rootLL.setBackgroundResource(R.drawable.tags_rounded_corners);
        GradientDrawable rootDrawable = (GradientDrawable) rootLL.getBackground();
        rootDrawable.setColor(color);

        searchLL.setBackgroundResource(R.drawable.tags_rounded_corners);
        GradientDrawable searchDrawable = (GradientDrawable) searchLL.getBackground();
        searchDrawable.setColor(color);
    }

    @Override
    public void setTextAndSelectionColorTheme(ColorStateList colorSelection, ColorStateList colorText, int colorTextSelected, int colorTextFocus, int colorTextNormal, int colorSelectionSelected, int colorSelectionFocus) {
        this.colorSelection = colorSelection;
        this.colorText = colorText;
        this.colorTextNormal = colorTextNormal;

//        songCategoryTV.setTextColor(colorTextNormal);
//        searchET.setHintTextColor(colorTextNormal);
        searchET.setTextColor(colorTextNormal);
    }

    void bind() {
        rootLL = (LinearLayout) root.findViewById(R.id.rootLL);
        recyclerView = (RecyclerView) root.findViewById(R.id.recyclerView);
        searchLL = (LinearLayout) root.findViewById(R.id.searchLL);
        playlistButton = (Button) root.findViewById(R.id.playlistButton);
        searchET = (EditText) root.findViewById(R.id.searchET);
//        songCategoryTV = (TextView) root.findViewById(R.id.categoryTV);
//        songCategoryTV.setText(categoryTitle);

//        songCategoryTV = (TextView) root.findViewById(R.id.songCategoryTV);
    }

    void fetchSongCategory() {
        try {
            PerformAsync2.run(performAsync -> {
                DbAccess dbAccess = DbAccess.Instance.create(getActivity());
                if (categoryId != 0)
                    tSongList = dbAccess.daoAccessTSong().getByCategory(categoryId);
                else
                    tSongList = dbAccess.daoAccessTSong().getAll();
                Log.d(TAG, String.valueOf(tSongList.size()));
                dbAccess.close();
                return 1;
            }).setCallbackResult(result -> {
                int i = (int) result;
                if (i != 1) {
                    return;
                } else {
                    setRecyclerView();
                    Log.d(TAG, String.valueOf(tSongList.size()));
                }
            });

        } catch (Exception ex) {
            Log.d(TAG, String.valueOf(ex.getMessage()));
        }
    }

    void setRecyclerView() {
        adapter = new RecyclerViewAdapter(new RecyclerViewAdapter.IRecyclerViewAdapter() {
            @Override
            public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
                View view = LayoutInflater.from(getActivity()).inflate(R.layout.cell_karaoke_list, parent, false);
                ItemViewHolder item = new ItemViewHolder(view);
                return item;
            }

            @Override
            public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
                ItemViewHolder item = (ItemViewHolder) holder;
                item.bind(tSongList.get(position), position);
            }

            @Override
            public int getItemCount() {
                return tSongList.size();
            }
        });

        GridLayoutManager gridLayoutManager = new GridLayoutManager(getActivity(), 3);
        recyclerView.setLayoutManager(gridLayoutManager);
        adapter.setHasStableIds(true);
        recyclerView.setAdapter(adapter);
 /*       recyclerView.setHasFixedSize(true);
        recyclerView.setItemViewCacheSize(200);
        recyclerView.setDrawingCacheEnabled(true);
        recyclerView.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_LOW);*/
        /*recyclerView.getViewTreeObserver().addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
            @Override
            public boolean onPreDraw() {
                Log.d(TAG, "onPreDraw");
                return false;
            }
        });*/
    }

    class ItemViewHolder extends RecyclerView.ViewHolder {
        private static final String TAG = "ItemViewHolder";

        View root;
        TextView tvTitle;
        TextView tvSinger;
        ImageView thumbnailIV;

        public ItemViewHolder(View itemView) {
            super(itemView);
            root = itemView;
            selectedView = root;
            tvTitle = itemView.findViewById(R.id.text_title);
            tvSinger = itemView.findViewById(R.id.text_singer);
            thumbnailIV = itemView.findViewById(R.id.thumbnailIV);
        }

        public void bind(TSong tSong, int pos) {
            tvTitle.setText(tSong.songName);
            tvSinger.setText(tSong.artist);
            if (tSong.thumbnail != null) {
                UtilRenderScript.fetchBitmap(getContext(), tSong.thumbnail, 300, thumbnailIV);
            /*    Picasso.with(getActivity())
                        .load(tSong.thumbnail)
                        .into(thumbnailIV);*/
            }


            if (colorSelection != null) root.setBackgroundTintList(colorSelection);
//            untuk merubah warna text
            if (colorText != null) {
                tvTitle.setTextColor(colorText);
                tvSinger.setTextColor(colorText);
            }


            root.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
//                    mListener.onListClick(tSong.songId);
                    showDetailSong(tSong);
                }
            });
            root.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View v, boolean hasFocus) {
                    if (hasFocus) {
                        updateSelectedViewUi(v, true);
                    } else {
                        updateSelectedViewUi(null, false);

                    }
                }
            });
        }

        void selectView(View v) {
            //clear selection
            /*if (selectedView != null) {
                selectedView.setSelected(false);
            }

            //make selection
            selectedView = v;
            selectedView.setSelected(true);*/
        }
    }

    ColorStateList colorSelection, colorText;
    int colorTextNormal;

    //function untuk declare semua action dari object di activity
    void setOnActionObject() {
        searchET.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                searchSong(s.toString());
            }
        });

        playlistButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PlaylistActivity.startActivity(getActivity(), 0);
            }
        });

        searchET.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus)
                    searchET.setHintTextColor(getActivity().getResources().getColor(R.color.texthintcolor_unfocus));
                else
                    searchET.setHintTextColor(colorTextNormal);

            }
        });
    }

    void searchSong(String prmKeyword) {
        try {
            PerformAsync2.run(performAsync -> {
                DbAccess dbAccess = DbAccess.Instance.create(getActivity());
                if (categoryId != 0)
                    tSongList = dbAccess.daoAccessTSong().getSearchByCategory(categoryId, prmKeyword);
                else
                    tSongList = dbAccess.daoAccessTSong().getSearchAll(prmKeyword);
                Log.d(TAG, String.valueOf(tSongList.size()));
                dbAccess.close();
                return 1;
            }).setCallbackResult(result -> {
                int i = (int) result;
                if (i != 1) {
                    return;
                } else {
//                    setRecyclerView();
                    Log.d(TAG, String.valueOf(tSongList.size()));
                    adapter.notifyDataSetChanged();
                }
            });

        } catch (Exception ex) {
            Log.d(TAG, String.valueOf(ex.getMessage()));
        }
    }

    public void setCallback(IKaraokeListListener listener) {
        this.mListener = listener;
    }

    public interface IKaraokeListListener {
        // TODO: Update argument type and name
        void onListClick(int songId);
    }

    void showDetailSong(TSong prmTSong) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.alert_karaoke_song, null);
        dialogBuilder.setView(dialogView);

        ImageView thumbnailIV = (ImageView) dialogView.findViewById(R.id.thumbnailIV);
        TextView titleTV = (TextView) dialogView.findViewById(R.id.titleTV);
        TextView singerTV = (TextView) dialogView.findViewById(R.id.singerTV);
        Button playSongButton = (Button) dialogView.findViewById(R.id.playSongButton);
        Button addToPlaylistButton = (Button) dialogView.findViewById(R.id.addToPlaylistButton);
        if (prmTSong.thumbnail != null) {
            Uri uri = Uri.fromFile(new File(prmTSong.thumbnail));
//            Picasso.with(getActivity()).load(uri).into(thumbnailIV);
            UtilRenderScript.fetchBitmap(getActivity(), prmTSong.thumbnail, 300, thumbnailIV);
        }
        titleTV.setText(prmTSong.songName);
        singerTV.setText(prmTSong.artist);
        AlertDialog alertDialog = dialogBuilder.create();

        playSongButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Toast.makeText(getContext(), "play song", Toast.LENGTH_LONG).show();
                ArrayList<TSong> tSongArrayList = new ArrayList<>();
                tSongArrayList.add(prmTSong);
                PlayerActivity.startActivity(getActivity(), tSongArrayList, 0);
                alertDialog.dismiss();

            }
        });

        addToPlaylistButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (playlistId == 0) {
                    showDialogPlaylist(prmTSong);
                    alertDialog.dismiss();
                } else {
                    savePlaylist(prmTSong, "", playlistId);
                }

            }
        });

        if (playlistId == 0)
            playSongButton.setVisibility(View.VISIBLE);
        else
            playSongButton.setVisibility(View.GONE);

        alertDialog.show();
        WindowManager.LayoutParams layoutParams = alertDialog.getWindow().getAttributes();
        layoutParams.width = ViewGroup.LayoutParams.WRAP_CONTENT;
        alertDialog.show();
        alertDialog.getWindow().setLayout(430, WindowManager.LayoutParams.WRAP_CONTENT);
        Util.hideNavigationBar(getActivity());

    }

    void showDialogPlaylist(TSong prmTSong) {
        PerformAsync2.run(performAsync ->
        {
            DbAccess dbAccess = DbAccess.Instance.create(getActivity());
            List<JoinPlaylistDt> tPlaylistList = dbAccess.daoAccessTPlaylist().joinPlaylist();
            dbAccess.close();
            return tPlaylistList;
        }).setCallbackResult(result ->
        {
            List<JoinPlaylistDt> tPlaylistList = (List<JoinPlaylistDt>) result;

            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
            LayoutInflater inflater = this.getLayoutInflater();
            View dialogView = inflater.inflate(R.layout.alert_listof_playlist, null);
            dialogBuilder.setView(dialogView);

            RecyclerView recyclerView = (RecyclerView) dialogView.findViewById(R.id.recyclerView);
            LinearLayout notExistPlaylistLL = (LinearLayout) dialogView.findViewById(R.id.notExistPlaylistLL);
            LinearLayout existPlaylistLL = (LinearLayout) dialogView.findViewById(R.id.existPlaylistLL);
            EditText playlistET = (EditText) dialogView.findViewById(R.id.playlistET);
            Button saveButton = (Button) dialogView.findViewById(R.id.saveButton);
            Button addPlaylistButton = (Button) dialogView.findViewById(R.id.addPlaylistButton);
            Button cancelButton = (Button) dialogView.findViewById(R.id.cancelButton);
            existPlaylistLL.setVisibility(View.VISIBLE);
            saveButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    savePlaylist(prmTSong, playlistET.getText().toString(), 0);
                }
            });

            addPlaylistButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    notExistPlaylistLL.setVisibility(View.VISIBLE);
                    addPlaylistButton.setVisibility(View.GONE);
                }
            });
            cancelButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    notExistPlaylistLL.setVisibility(View.GONE);
                    addPlaylistButton.setVisibility(View.VISIBLE);
                }
            });

            setRecyclerViewPlaylist(recyclerView, tPlaylistList, prmTSong);

            AlertDialog alertDialog = dialogBuilder.create();
            alertDialog.show();
            WindowManager.LayoutParams layoutParams = alertDialog.getWindow().getAttributes();
            layoutParams.width = ViewGroup.LayoutParams.WRAP_CONTENT;
            alertDialog.show();
            alertDialog.getWindow().setLayout(530, WindowManager.LayoutParams.WRAP_CONTENT);
            Util.hideNavigationBar(getActivity());

        });

    }

    void savePlaylist(TSong prmTSong, String prmPlaylistName, int prmPlaylistId) {
        PerformAsync2.run(performAsync ->
        {
            int result = 0;
            try {
                DbAccess dbAccess = DbAccess.Instance.create(getActivity());

                TPlaylist tPlaylist = new TPlaylist();
                int lastPlaylistId = 0;
                if (prmPlaylistId == 0) {
                    //cek apakah sudah ada nama playlist yg sama
                    int isExist = dbAccess.daoAccessTPlaylist().existPlaylistName(prmPlaylistName);
                    if (isExist == 0) {
                        tPlaylist.playlistName = prmPlaylistName;
                        dbAccess.daoAccessTPlaylist().insert(tPlaylist);
                        lastPlaylistId = dbAccess.daoAccessTPlaylist().lastId();
                    } else
                        result = 2;
                } else {
                    lastPlaylistId = prmPlaylistId;
                }
                if (lastPlaylistId != 0) {
                    int isExistSong = dbAccess.daoAccessTSongPlaylist().existSongPlaylistBySongId(Integer.valueOf(prmTSong.songId), lastPlaylistId);
                    if (isExistSong == 0) {
                        TSongPlaylist tSongPlaylist = new TSongPlaylist();
                        tSongPlaylist.playlistId = lastPlaylistId;
                        tSongPlaylist.songId = Integer.valueOf(prmTSong.songId);
                        tSongPlaylist.songName = prmTSong.songName;
                        tSongPlaylist.artist = prmTSong.artist;
                        tSongPlaylist.songURL = prmTSong.songURL;
                        tSongPlaylist.vocalSound = Integer.valueOf(prmTSong.vocalSound);
                        tSongPlaylist.duration = Integer.valueOf(prmTSong.duration);
                        tSongPlaylist.thumbnail = prmTSong.thumbnail;
                        dbAccess.daoAccessTSongPlaylist().insert(tSongPlaylist);
                        result = 1;
                    } else
                        result = 3;
                }
                dbAccess.close();

            } catch (Exception ex) {
                result = 0;
            }
            return result;
        }).setCallbackResult(result ->
        {
            int i = (int) result;
            //1 success
            //2 duplicate playlist
            //0 failed
            //3 song already in playlist
            if (i == 1) {
                PlaylistActivity.startActivity(getActivity(), prmPlaylistId);
                getActivity().finish();
            } else if (i == 2) {
                Toasty.error(getActivity(), getString(R.string.playlistAlreadyExists)).show();
//                Toast.makeText(getActivity(), getString(R.string.playlistAlreadyExists), Toast.LENGTH_LONG).show();
            } else if (i == 3) {
                Toasty.error(getActivity(), getString(R.string.songAlreadyExists)).show();
//                Toast.makeText(getActivity(), getString(R.string.songAlreadyExists), Toast.LENGTH_LONG).show();
            }
        });
    }

    void setRecyclerViewPlaylist(RecyclerView prmRecyclerView, List<JoinPlaylistDt> prmJoinPlaylistDts, TSong prmTSong) {
        adapterPlaylist = new RecyclerViewAdapter(new RecyclerViewAdapter.IRecyclerViewAdapter() {
            @Override
            public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
                View view = LayoutInflater.from(getActivity()).inflate(R.layout.cell_listof_playlist, parent, false);
                ItemViewHolderPlaylist item = new ItemViewHolderPlaylist(view);
                return item;
            }

            @Override
            public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
                ItemViewHolderPlaylist item = (ItemViewHolderPlaylist) holder;
                item.bind(prmJoinPlaylistDts.get(position), position, prmTSong);
            }

            @Override
            public int getItemCount() {
                return prmJoinPlaylistDts.size();
            }
        });

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        prmRecyclerView.setLayoutManager(linearLayoutManager);
        prmRecyclerView.setAdapter(adapterPlaylist);
    }

    class ItemViewHolderPlaylist extends RecyclerView.ViewHolder {
        private static final String TAG = "ItemViewHolderPlaylist";

        View root;
        TextView playlistTV;
        TextView sumOfSongsTV;
        TextView totalDurationTV;
        LinearLayout selectioncolor;

        public ItemViewHolderPlaylist(View itemView) {
            super(itemView);
            root = itemView;
            playlistTV = itemView.findViewById(R.id.playlistTV);
            sumOfSongsTV = itemView.findViewById(R.id.sumOfSongsTV);
            totalDurationTV = itemView.findViewById(R.id.totalDurationTV);
            selectioncolor = itemView.findViewById(R.id.selectioncolor);
        }

        public void bind(JoinPlaylistDt tPlaylist, int pos, TSong tSong) {
            playlistTV.setText(tPlaylist.playlistName);
            int milis = tPlaylist.sumOfDuration * 1000;
            totalDurationTV.setText(Util.convertMilisToTime(milis));
            sumOfSongsTV.setText(String.valueOf(tPlaylist.sumOfSong));

            if ((pos % 2) == 0) {
                // number is even
                int color = ContextCompat.getColor(getContext(), R.color.first_row_table_dialog);
                root.setBackgroundColor(color);
            } else {
                // number is odd
                int color = ContextCompat.getColor(getContext(), R.color.second_row_table_dialog);
                root.setBackgroundColor(color);
            }
//            tvSinger.setText(tPlaylist.artist);

//            if (colorSelection != null) root.setBackgroundTintList(colorSelection);


            selectioncolor.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
//                    showDetailSong(tPlaylist);
                    savePlaylist(tSong, "", tPlaylist.playlistId);
                }
            });
        }

    }

    void updateSelectedViewUi(View view, boolean hasFocus) {
        if (selectedView != null) {
            scaleView(selectedView, 1.1f, 1);
            ViewCompat.setTranslationZ(selectedView, 0);
        }

        if (hasFocus) {
            selectedView = view;
            ViewCompat.setTranslationZ(selectedView, 100);
//                selectedView.setBackgroundResource(R.drawable.box_white);
            scaleView(selectedView, 1, 1.1f);
        } else {
            //remove selected & focus dari grid
            selectedView = null;
        }
    }

    public void scaleView(View v, float startScale, float endScale) {
        Animation anim = new ScaleAnimation(
                startScale, endScale, // Start and end values for the X axis scaling
                startScale, endScale, // Start and end values for the Y axis scaling
                Animation.RELATIVE_TO_SELF, 0.5f, // Pivot point of X scaling
                Animation.RELATIVE_TO_SELF, 0.5f); // Pivot point of Y scaling
        anim.setFillAfter(true); // Needed to keep the result of the animation
        anim.setDuration(100);
        v.startAnimation(anim);
    }

    static View selectedView;


    public static int calculateInSampleSize(
            BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) >= reqHeight
                    && (halfWidth / inSampleSize) >= reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }

    public static Bitmap decodeSampledBitmapFromResource(String path,
                                                         int reqWidth, int reqHeight) {

        // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(path, options);

        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeFile(path, options);
    }

}

package com.madeira.entertainz.karaoke.menu_main;

import android.content.Context;
import android.content.res.ColorStateList;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.madeira.entertainz.helper.RecyclerViewAdapter;
import com.madeira.entertainz.karaoke.DBLocal.DbAccess;
import com.madeira.entertainz.karaoke.DBLocal.TElement;
import com.madeira.entertainz.karaoke.DBLocal.TSticker;
import com.madeira.entertainz.karaoke.R;
import com.madeira.entertainz.library.PerformAsync2;
import com.madeira.entertainz.library.UtilRenderScript;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link ListMoodFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link ListMoodFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ListMoodFragment extends Fragment implements MainTheme.IMainTheme {

    String TAG = "ListMoodFragment";
    View root;
    RecyclerView recyclerView;
    RecyclerViewAdapter adapter;
    MainTheme mainTheme;
    ListMoodListener callback;


    public ListMoodFragment() {
        // Required empty public constructor
    }


    public static ListMoodFragment newInstance() {
        ListMoodFragment fragment = new ListMoodFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        root = inflater.inflate(R.layout.fragment_list_mood, container, false);
        bind();
        mainTheme = new MainTheme(getContext(), this);
        mainTheme.setTheme();
        fetchSticker();
        return root;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
      /*  if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }*/
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
       /* if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }*/
    }

    @Override
    public void onDetach() {
        super.onDetach();
//        mListener = null;
    }

    @Override
    public void setThemeUpdateDate(Date lastUpdateTheme) {

    }

    @Override
    public void setBackgroundTheme(TElement element) {

    }

    @Override
    public void setListColorTheme(int color) {

    }

    @Override
    public void setFooterColorTheme(int color) {

    }

    @Override
    public void setTextAndSelectionColorTheme(ColorStateList colorSelection, ColorStateList colorText, int colorTextSelected, int colorTextFocus, int colorTextNormal, int colorSelectionSelected, int colorSelectionFocus) {
        this.colorSelection = colorSelection;
        this.colorText = colorText;
    }

    @Override
    public void setHeaderColorTheme(int color) {

    }


    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    void bind() {
        recyclerView = root.findViewById(R.id.recyclerView);
    }

    void fetchSticker() {
        try {
            PerformAsync2.run(performAsync ->
            {
                List<TSticker> tStickerList = new ArrayList<>();

                DbAccess dbAccess = DbAccess.Instance.create(getContext());
                tStickerList = dbAccess.daoAccessSticker().getAll();
                dbAccess.close();

                return tStickerList;
            }).setCallbackResult(result ->
            {
                List<TSticker> tStickerList = (List<TSticker>) result;
                if (tStickerList != null) {
                    setRecyclerView(tStickerList);
                }
            });
        } catch (Exception ex) {
            Log.d(TAG, ex.getMessage());
        }
    }

    void setRecyclerView(List<TSticker> tStickerList) {
        adapter = new RecyclerViewAdapter(new RecyclerViewAdapter.IRecyclerViewAdapter() {
            @Override
            public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
                View view = LayoutInflater.from(getActivity()).inflate(R.layout.cell_sticker, parent, false);
                ItemViewHolder item = new ItemViewHolder(view);
                return item;
            }

            @Override
            public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
                ItemViewHolder item = (ItemViewHolder) holder;
                item.bind(tStickerList.get(position), position);
            }

            @Override
            public int getItemCount() {
                return tStickerList.size();
            }
        });

        recyclerView.setAdapter(adapter);
    }

    //view ini khusus di pakai utk ItemViewHolder, utk selectedItemView
    View selectedView;
    int selectedItem;

    class ItemViewHolder extends RecyclerView.ViewHolder {
        private static final String TAG = "ItemViewHolder";

        View root;
        TextView tvTitle;
        ImageView stickerIV;

        public ItemViewHolder(View itemView) {
            super(itemView);
            root = itemView;

            tvTitle = itemView.findViewById(R.id.text_title);
            stickerIV = itemView.findViewById(R.id.stickerIV);
//            selectionColor = itemView.findViewById(R.id.selectioncolor);
        }

        public void bind(TSticker tSticker, int pos) {
            tvTitle.setText(tSticker.stickerName);
            UtilRenderScript.fetchBitmap(getContext(), tSticker.urlImage, 300, stickerIV);
            if (selectedItem == pos) {
                selectView(root);
                root.requestFocus();
            }

//            untuk merubah warna selection
            if (colorSelection != null) root.setBackgroundTintList(colorSelection);

//            untuk merubah warna text
            if (colorText != null) tvTitle.setTextColor(colorText);

            if (pos == 0) {
                root.setSelected(true);
                root.requestFocus();
            }

            root.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    selectView(v);
                    selectedItem = pos;
                    callback.onClick(tSticker);
                }
            });

        }

        void selectView(View v) {
            //clear selection
            if (selectedView != null) {
                selectedView.setSelected(false);
            }

            //make selection
            selectedView = v;
            selectedView.setSelected(true);
        }
    }

    ColorStateList colorSelection, colorText;

    public interface ListMoodListener {
        void onClick(TSticker tSticker);
    }

    public void setCallBack(ListMoodListener callback) {
        this.callback = callback;
    }

}

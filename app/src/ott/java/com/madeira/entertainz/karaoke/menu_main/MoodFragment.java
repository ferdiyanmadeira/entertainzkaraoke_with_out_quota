package com.madeira.entertainz.karaoke.menu_main;


import android.content.res.ColorStateList;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.animation.FastOutLinearInInterpolator;
import android.support.v4.view.animation.FastOutSlowInInterpolator;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.madeira.entertainz.karaoke.DBLocal.DbAccess;
import com.madeira.entertainz.karaoke.DBLocal.TSticker;
import com.madeira.entertainz.karaoke.R;
import com.madeira.entertainz.karaoke.DBLocal.TElement;
import com.madeira.entertainz.karaoke.config.C;
import com.madeira.entertainz.karaoke.config.Global;
import com.madeira.entertainz.library.PerformAsync2;
import com.madeira.entertainz.library.UtilRenderScript;

import java.util.Date;

import es.dmoral.toasty.Toasty;

public class MoodFragment extends Fragment implements MainTheme.IMainTheme {

    String TAG = "BottomNavigationFragment";

    View root;
    RelativeLayout rootRL;
    MainTheme mainTheme;
    MoodFragmentListener callback;
    LinearLayout moodLL;
    ImageView moodIV;
    ImageView logoIV;
    static TextView moodTV;
    ImageView apavmiLogoIV;

    public MoodFragment() {
        // Required empty public constructor
    }


    public static MoodFragment newInstance(String param1, String param2) {
        MoodFragment fragment = new MoodFragment();
        Bundle args = new Bundle();
       /* args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);*/
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
       /* if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }*/
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        root = inflater.inflate(R.layout.fragment_bottom_navigation, container, false);
        bind();
        mainTheme = new MainTheme(getActivity(), this);
        mainTheme.setTheme();
        setActionObject();
        fetchMood();
        return root;
    }

    void bind() {
        rootRL = (RelativeLayout) root.findViewById(R.id.rootRL);
        moodLL = root.findViewById(R.id.moodLL);
        moodIV = root.findViewById(R.id.moodIV);
        moodTV = root.findViewById(R.id.moodText);
        logoIV = root.findViewById(R.id.logoIV);
        apavmiLogoIV = root.findViewById(R.id.apavmiLogoIV);
        logoIV.setVisibility(View.GONE);

    }

    @Override
    public void setThemeUpdateDate(Date lastUpdateTheme) {

    }

    @Override
    public void setBackgroundTheme(TElement element) {

    }

    @Override
    public void setListColorTheme(int color) {

    }

    @Override
    public void setFooterColorTheme(int color) {
        rootRL.setBackgroundColor(color);
    }

    @Override
    public void setTextAndSelectionColorTheme(ColorStateList colorSelection, ColorStateList colorText, int colorTextSelected, int colorTextFocus, int colorTextNormal, int colorSelectionSelected, int colorSelectionFocus) {

    }

    @Override
    public void setHeaderColorTheme(int color) {

    }

    public interface MoodFragmentListener {
        void onMoodClick();
    }

    public void setCallback(MoodFragmentListener callback) {
        this.callback = callback;
    }

    void setActionObject() {
        moodLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callback.onMoodClick();
            }
        });
    }

    public void updateMood(TSticker tSticker) {
        UtilRenderScript.fetchBitmap(getContext(), tSticker.urlImage, 300, moodIV);
        moodTV.setText(tSticker.stickerName);

    }

    void fetchMood() {
        try {
            PerformAsync2.run(performAsync ->
            {
                TSticker tSticker = null;
                int lastStickerId = Global.getStickerId();
                if (lastStickerId != 0) {
                    DbAccess dbAccess = DbAccess.Instance.create(getContext());
                    tSticker = dbAccess.daoAccessSticker().getStickerById(lastStickerId);
                    dbAccess.close();
                }

                return tSticker;
            }).setCallbackResult(result ->
            {
                TSticker tSticker = (TSticker) result;
                if (tSticker != null) {
                    updateMood(tSticker);
                }
            });
        } catch (Exception ex) {
            Log.d(TAG, ex.getMessage());
        }
    }


}

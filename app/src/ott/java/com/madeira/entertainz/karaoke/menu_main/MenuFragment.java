package com.madeira.entertainz.karaoke.menu_main;


import android.content.res.ColorStateList;
import android.graphics.ColorFilter;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.ScaleAnimation;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.madeira.entertainz.helper.RecyclerViewAdapter;
import com.madeira.entertainz.karaoke.R;
import com.madeira.entertainz.karaoke.DBLocal.TElement;
import com.madeira.entertainz.karaoke.config.C;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static com.madeira.entertainz.karaoke.config.C.MENU_ACCESSORIES;
import static com.madeira.entertainz.karaoke.config.C.MENU_MESSAGES;
import static com.madeira.entertainz.karaoke.config.C.MENU_PLAYLIST;
import static com.madeira.entertainz.karaoke.config.C.MENU_SEARCH_SONG;
import static com.madeira.entertainz.karaoke.config.C.MENU_SETTING;
import static com.madeira.entertainz.karaoke.config.C.MENU_UPDATE_SONG;
import static com.madeira.entertainz.karaoke.config.C.MENU_VIDIO;
import static com.madeira.entertainz.karaoke.config.C.MENU_YOOLIVE;
import static com.madeira.entertainz.karaoke.config.C.MENU_YOUTUBE;


public class MenuFragment extends Fragment implements MainTheme.IMainTheme {
    String TAG = "MenuFragment";

    RecyclerView recyclerView;
    View root;
    MainTheme mainTheme;
    RelativeLayout rootRL;
    ColorStateList colorSelection, colorText;

    public interface IMenuMainFragment {
        void onClickMenu(String menuName, int menuId);
    }


    RecyclerViewAdapter adapter;
    List<Integer> listMenuId = new ArrayList<>();
    IMenuMainFragment callback;
    ColorFilter filterBw;
    ColorFilter filterColor;

    public MenuFragment() {
        // Required empty public constructor


    }


    public static MenuFragment newInstance() {
        MenuFragment fragment = new MenuFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }

            listMenuId.add(MENU_SEARCH_SONG);
            listMenuId.add(MENU_PLAYLIST);
            listMenuId.add(MENU_UPDATE_SONG);
            listMenuId.add(MENU_ACCESSORIES);
            listMenuId.add(MENU_MESSAGES);
            listMenuId.add(MENU_SETTING);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        root = inflater.inflate(R.layout.fragment_menu, container, false);
        bind();
        setColorFilter();
        mainTheme = new MainTheme(getActivity(), this);
        mainTheme.setTheme();
        setupRecyclerView();
        recyclerView.requestFocus();
        return root;
    }


    void bind() {
        rootRL = (RelativeLayout) root.findViewById(R.id.rootRL);
        recyclerView = (RecyclerView) root.findViewById(R.id.recyclerView);
    }

    void setupRecyclerView() {

        adapter = new RecyclerViewAdapter(new RecyclerViewAdapter.IRecyclerViewAdapter() {

            @Override
            public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
                View view = LayoutInflater.from(getContext()).inflate(R.layout.cell_menu,
                        parent, false);
                return new ItemViewHolder(view);
            }

            @Override
            public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
                ItemViewHolder item = (ItemViewHolder) holder;

                int menuId = listMenuId.get(position);
                String title = getMenuTitle(menuId);
                int imageId = getMenuImage(menuId);

                item.bind(title, imageId, menuId);
            }

            @Override
            public int getItemCount() {
                return listMenuId.size();
            }
        });
        GridLayoutManager gridLayoutManager = new GridLayoutManager(getActivity(), listMenuId.size());
        recyclerView.setLayoutManager(gridLayoutManager);
        recyclerView.setAdapter(adapter);


    }

    void setColorFilter() {
        ColorMatrix matrix = new ColorMatrix();
        matrix.setSaturation(0);

        filterBw = new ColorMatrixColorFilter(matrix);

        matrix.setSaturation(1);
        filterColor = new ColorMatrixColorFilter(matrix);
    }

    String getMenuTitle(int menuId) {
        String title = null;

        switch (menuId) {
            case MENU_SEARCH_SONG:
                title = getString(R.string.title_search_song);
                break;
            case MENU_PLAYLIST:
                title = getString(R.string.title_playlist);
                break;
            case MENU_UPDATE_SONG:
                title = getString(R.string.title_update_song);
                break;
            case MENU_ACCESSORIES:
                title = getString(R.string.title_accessories);
                break;
            case MENU_MESSAGES:
                title = getString(R.string.title_messages);
                break;
            case MENU_SETTING:
                title = getString(R.string.title_setting);
                break;
            case MENU_YOUTUBE:
                title = getString(R.string.title_youtube);
                break;
            case MENU_YOOLIVE:
                title = getString(R.string.title_live_channel);
                break;
            case MENU_VIDIO:
                title = getString(R.string.title_vidio);
                break;
        }

        return title;
    }

    int getMenuImage(int menuId) {
        int imageId = 0;

        switch (menuId) {

            case MENU_SEARCH_SONG:
                imageId = R.drawable.menu_search_song;
                break;
            case MENU_PLAYLIST:
                imageId = R.drawable.menu_playlist;
                break;
            case MENU_UPDATE_SONG:
                imageId = R.drawable.menu_update_song;
                break;
            case MENU_ACCESSORIES:
                imageId = R.drawable.menu_shopping;
                break;
            case MENU_MESSAGES:
                imageId = R.drawable.menu_messages;
                break;
            case MENU_SETTING:
                imageId = R.drawable.menu_setting;
                break;
            case MENU_YOOLIVE:
                imageId = R.drawable.menu_yoo;
                break;
            case MENU_YOUTUBE:
                imageId = R.drawable.menu_youtube;
                break;
            case MENU_VIDIO:
                imageId = R.drawable.menu_vidio;
                break;
        }

        return imageId;
    }

    public void setCallback(IMenuMainFragment callback) {
        this.callback = callback;
    }

    class ItemViewHolder extends RecyclerView.ViewHolder {
        private static final String TAG = "ItemViewHolder";

        View root;
        TextView titleTV;
        ImageView imageIV;
        CardView cardView;

        public ItemViewHolder(View itemView) {
            super(itemView);

            root = itemView;
            titleTV = itemView.findViewById(R.id.text_title);
            imageIV = itemView.findViewById(R.id.image_view);

            imageIV.setColorFilter(filterBw);
            cardView = itemView.findViewById(R.id.card_view);

        }

        public void bind(String title, int imageId, int menuId) {

            titleTV.setText(title);
            imageIV.setImageResource(imageId);
            if (colorText != null) titleTV.setTextColor(colorText);

            cardView.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View v, boolean hasFocus) {
                    updateFocus(root, hasFocus);
                    if (hasFocus) {
                        imageIV.setColorFilter(filterColor);
                    } else {
                        imageIV.setColorFilter(filterBw);
                    }
                }
            });

            cardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    callback.onClickMenu(title, menuId);
                }
            });

        }
    }

    void updateFocus(View view, boolean hasFocus) {

        if (hasFocus) {
            ViewCompat.setTranslationZ(view, 15);
            scaleView(view, 1, 1.3f);
        } else {
            scaleView(view, 1.3f, 1);
            ViewCompat.setTranslationZ(view, 0);
        }
    }

    public void scaleView(View v, float startScale, float endScale) {
        Animation anim = new ScaleAnimation(
                startScale, endScale, // Start and end values for the X axis scaling
                startScale, endScale, // Start and end values for the Y axis scaling
                Animation.RELATIVE_TO_SELF, 0.5f, // Pivot point of X scaling
                Animation.RELATIVE_TO_SELF, 0.5f); // Pivot point of Y scaling
        anim.setFillAfter(true); // Needed to keep the result of the animation
        anim.setDuration(100);
        v.startAnimation(anim);
    }

    @Override
    public void setThemeUpdateDate(Date lastUpdateTheme) {

    }

    @Override
    public void setBackgroundTheme(TElement element) {

    }

    @Override
    public void setListColorTheme(int color) {
        rootRL.setBackgroundColor(color);
    }

    @Override
    public void setFooterColorTheme(int color) {

    }

    @Override
    public void setTextAndSelectionColorTheme(ColorStateList colorSelection, ColorStateList colorText, int colorTextSelected, int colorTextFocus, int colorTextNormal, int colorSelectionSelected, int colorSelectionFocus) {
        this.colorSelection = colorSelection;
        this.colorText = colorText;

    }

    @Override
    public void setHeaderColorTheme(int color) {

    }

    @Override
    public void onDetach() {
        super.onDetach();
        callback = null;
    }
}

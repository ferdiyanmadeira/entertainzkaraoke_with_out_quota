package com.madeira.entertainz.karaoke.menu_karaoke;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Handler;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.madeira.entertainz.karaoke.DBLocal.JoinSettingElementMedia;
import com.madeira.entertainz.karaoke.R;
import com.madeira.entertainz.karaoke.config.Global;
import com.madeira.entertainz.karaoke.menu_navigation.TopNavigationFragment;
import com.madeira.entertainz.karaoke.DBLocal.DbAccess;
import com.madeira.entertainz.library.PerformAsync2;
import com.madeira.entertainz.library.Util;

import net.yslibrary.android.keyboardvisibilityevent.KeyboardVisibilityEvent;
import net.yslibrary.android.keyboardvisibilityevent.KeyboardVisibilityEventListener;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import es.dmoral.toasty.Toasty;

import static com.madeira.entertainz.karaoke.config.CacheData.lastIndexBackground;

public class KaraokeActivity extends AppCompatActivity {
    String TAG = "KaraokeActivity";
    ImageView backgroundIV;
    FrameLayout popupFrameLayout;

    public static final String KEY_PLAYLIST_ID = "KEY_PLAYLIST_ID";
    int playlistId = 0;

    TopNavigationFragment topNavigationFragment = new TopNavigationFragment();
    KaraokeCategoryFragment karaokeCategoryFragment = new KaraokeCategoryFragment();
    KaraokeListFragment karaokeListFragment = new KaraokeListFragment();
    //    KaraokePopupFragment karaokePopupFragment = new KaraokePopupFragment();
    Handler handler = new Handler();
    Runnable runnable;

    public static void startActivity(Activity activity, Integer prmPlaylistId) {
        Intent intent = new Intent(activity, KaraokeActivity.class);
        intent.putExtra(KEY_PLAYLIST_ID, prmPlaylistId);
        activity.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_karaoke);
        bind();
        fetchListBackground();
        bindFragment();
        playlistId = getIntent().getIntExtra(KEY_PLAYLIST_ID, 0);
        Util.hideNavigationBar(this);
        KeyboardVisibilityEvent.setEventListener(
                this,
                new KeyboardVisibilityEventListener() {
                    @Override
                    public void onVisibilityChanged(boolean isOpen) {
                        // some code depending on keyboard visiblity status
                        if (isOpen) {
                            Toasty.info(KaraokeActivity.this, getString(R.string.pressBackToHideKeyBoard)).show();
                        }

                    }
                });

        String languageId = Global.getLanguageId();
        Util.setLanguage(this, languageId);

    }

    void bind() {
        backgroundIV = (ImageView) findViewById(R.id.backgroundIV);
        popupFrameLayout = (FrameLayout) findViewById(R.id.popupFrameLayout);
    }

    void bindFragment() {
//        topNavigationFragment = topNavigationFragment.newInstance(true);
        topNavigationFragment = topNavigationFragment.newInstance(getString(R.string.title_search_song));
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.topNavFrameLayout, topNavigationFragment);

        ft.replace(R.id.categoryFrameLayout, karaokeCategoryFragment);

        Bundle listBundle = new Bundle();
        listBundle.putInt(karaokeListFragment.KEY_CATEGORY_ID, 0);
        listBundle.putString(karaokeListFragment.KEY_CATEGORY_TITLE, getString(R.string.all));
        listBundle.putInt(KEY_PLAYLIST_ID, getIntent().getIntExtra(KEY_PLAYLIST_ID, 0));
        karaokeListFragment.setArguments(listBundle);
        ft.replace(R.id.songFrameLayout, karaokeListFragment).addToBackStack(null);

        ft.commit();
        karaokeCategoryFragment.setCallback(iKaraokeCategoryListener);
        karaokeListFragment.setCallback(iKaraokeListListener);



    }

    void fetchListBackground() {
        PerformAsync2.run(performAsync ->
                {
                    List<JoinSettingElementMedia> result = new ArrayList<>();
                    DbAccess dbAccess = DbAccess.Instance.create(KaraokeActivity.this);
                    result = dbAccess.daoAccessThemeMedia().joinSettingElementMediaActive();
                    return result;
                }
        ).setCallbackResult(result ->
        {
            List<JoinSettingElementMedia> joinSettingElementMediaList = (List<JoinSettingElementMedia>) result;
            int totalIndex = joinSettingElementMediaList.size();
            runnable = new Runnable() {
                @Override
                public void run() {
                    if (joinSettingElementMediaList.size() != 0) {
                        if (lastIndexBackground >= totalIndex)
                            lastIndexBackground = 0;

                        JoinSettingElementMedia joinSettingElementMedia = joinSettingElementMediaList.get(lastIndexBackground);
                        Uri uri = Uri.fromFile(new File(joinSettingElementMedia.urlImage));
//                    Picasso.with(KaraokeActivity.this).load(uri).into(backgroundIV);
                        try {
                            InputStream inputStream = getContentResolver().openInputStream(uri);
                            BitmapFactory.Options options = new BitmapFactory.Options();
                            options.inPreferredConfig = Bitmap.Config.RGB_565;
                            Bitmap image = BitmapFactory.decodeStream(inputStream, null, options);
                            backgroundIV.setImageBitmap(image);
                        } catch (FileNotFoundException e) {
                            e.printStackTrace();
                        }
                        lastIndexBackground++;
                        handler.postDelayed(this::run, joinSettingElementMedia.duration);
                    }

                }
            };
            handler.postDelayed(runnable, 100);
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        //agar runable tidak running ketika di finish activity
        Util.hideNavigationBar(this);
        if (popupFrameLayout.getVisibility() == View.GONE) {
            handler.removeCallbacks(runnable);
            handler.removeCallbacksAndMessages(null);
            finish();
        } else {
            popupFrameLayout.setVisibility(View.GONE);
        }
    }

    KaraokeCategoryFragment.IKaraokeCategoryListener iKaraokeCategoryListener = new KaraokeCategoryFragment.IKaraokeCategoryListener() {
        @Override
        public void onCategoryClick(int categoryId, String category) {
            //detach first, for refresh fragment

//            karaokeListFragment.newInstance(categoryId);
            karaokeListFragment = new KaraokeListFragment();
            Bundle args = new Bundle();
            args.putInt(KaraokeListFragment.KEY_CATEGORY_ID, categoryId);
            args.putString(KaraokeListFragment.KEY_CATEGORY_TITLE, category);
            args.putInt(KEY_PLAYLIST_ID, getIntent().getIntExtra(KEY_PLAYLIST_ID, 0));
            karaokeListFragment.setArguments(args);

            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.songFrameLayout, karaokeListFragment).addToBackStack(null);
            ft.commit();
        }
    };

    KaraokeListFragment.IKaraokeListListener iKaraokeListListener = new KaraokeListFragment.IKaraokeListListener() {
        @Override
        public void onListClick(int songId) {
          /*  Bundle bundle = new Bundle();
            bundle.putInt(KaraokePopupFragment.KEY_SONGID, songId);
            karaokePopupFragment.setArguments(bundle);
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.popupFrameLayout, karaokePopupFragment);
            ft.commit();
            popupFrameLayout.setVisibility(View.VISIBLE);*/
        }
    };

    @Override
    public void onResume() {
        super.onResume();
        Util.hideNavigationBar(this);
    }
}

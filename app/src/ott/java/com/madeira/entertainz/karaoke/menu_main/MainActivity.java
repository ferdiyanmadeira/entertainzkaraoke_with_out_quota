package com.madeira.entertainz.karaoke.menu_main;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.KeyguardManager;
import android.content.ComponentName;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.net.wifi.WifiManager;
import android.os.Handler;
import android.os.PowerManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.kinda.alert.KAlertDialog;
import com.madeira.entertainz.karaoke.DBLocal.JoinSettingElementMedia;
import com.madeira.entertainz.karaoke.DBLocal.TSticker;
import com.madeira.entertainz.karaoke.R;
import com.madeira.entertainz.karaoke.SplashScreenActivity;
import com.madeira.entertainz.karaoke.config.C;
import com.madeira.entertainz.karaoke.config.Global;
import com.madeira.entertainz.karaoke.menu_accessories.AccessoriesActivity;
import com.madeira.entertainz.karaoke.menu_karaoke.KaraokeActivity;
import com.madeira.entertainz.karaoke.menu_messages.MessagesActivity;
import com.madeira.entertainz.karaoke.menu_navigation.TopNavigationFragment;
import com.madeira.entertainz.karaoke.menu_playlist.PlaylistActivity;
import com.madeira.entertainz.karaoke.DBLocal.DbAccess;
import com.madeira.entertainz.karaoke.menu_setting_admin.SettingAdminActivity;
import com.madeira.entertainz.karaoke.menu_settings.SettingActivity;
import com.madeira.entertainz.karaoke.menu_update_song.UpdateSongActivity;
import com.madeira.entertainz.karaoke.menu_yoomedia.YoomediaActivity;
import com.madeira.entertainz.library.KeyGuard;
import com.madeira.entertainz.library.PerformAsync2;
import com.madeira.entertainz.library.Util;
import com.mukesh.OtpView;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import static com.madeira.entertainz.karaoke.config.C.APPLICATION_ID_VIDIO;
import static com.madeira.entertainz.karaoke.config.C.YOUTUBE_ID;
import static com.madeira.entertainz.karaoke.config.CacheData.lastIndexBackground;

public class MainActivity extends AppCompatActivity {

    String TAG = "MainActivity";
    int intentFromOTT = 0;

    ImageView backgroundIV;
    Handler handler = new Handler();
    View listMoodView;

    TopNavigationFragment topNavigationFragment = new TopNavigationFragment();
    MoodFragment moodFragment = new MoodFragment();
    MenuFragment menuFragment = new MenuFragment();
    ListMoodFragment listMoodFragment = new ListMoodFragment();
    //    TopNavigationFragment topNavigationFragment;
    KeyGuard kg = new KeyGuard(C.PIN_FOR_SETTING);
    static String passwordSetting = "";
    static int countWrongPassword = 0;


    public static void startActivity(Activity activity, int prmIntentFromOTT) {
        Intent intent = new Intent(activity, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra(SplashScreenActivity.KEY_INTENT_FROM_OTT, prmIntentFromOTT);
        activity.startActivity(intent);
        if (prmIntentFromOTT != 0)
            activity.finish();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        bind();
        bindFragment();
        fetchListBackground();
        intentFromOTT = getIntent().getIntExtra(SplashScreenActivity.KEY_INTENT_FROM_OTT, 0);
        Util.hideNavigationBar(MainActivity.this);
        SettingActivity.setCallback(iSettingActivityListener);

      /*  PowerManager pm = (PowerManager) getSystemService(POWER_SERVICE);
        PowerManager.WakeLock wl = pm.newWakeLock(PowerManager.FULL_WAKE_LOCK
                | PowerManager.ACQUIRE_CAUSES_WAKEUP
                | PowerManager.ON_AFTER_RELEASE, "INFO");
        wl.acquire();

        KeyguardManager km = (KeyguardManager) getSystemService(KEYGUARD_SERVICE);
        KeyguardManager.KeyguardLock kl = km.newKeyguardLock("name");
        kl.disableKeyguard();*/

        String languageId = Global.getLanguageId();
        Util.setLanguage(this, languageId);
    }

    void bind() {
        backgroundIV = (ImageView) findViewById(R.id.backgroundIV);
        listMoodView = findViewById(R.id.listMoodFrameLayout);
    }

    void bindFragment() {

        topNavigationFragment = topNavigationFragment.newInstance("");
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.topNavFrameLayout, topNavigationFragment).addToBackStack(null);
        ft.replace(R.id.moodFrameLayout, moodFragment);
        ft.replace(R.id.mainMenuFrameLayout, menuFragment);
        ft.replace(R.id.listMoodFrameLayout, listMoodFragment);
        ft.commit();
        menuFragment.setCallback(menuMainFragmentCallback);
        moodFragment.setCallback(moodFragmentListener);
        listMoodFragment.setCallBack(listMoodListener);
    }

    void fetchListBackground() {
        PerformAsync2.run(performAsync ->
                {
                    List<JoinSettingElementMedia> result = new ArrayList<>();
                    DbAccess dbAccess = DbAccess.Instance.create(MainActivity.this);
                    result = dbAccess.daoAccessThemeMedia().joinSettingElementMediaActive();
                    return result;
                }
        ).setCallbackResult(result ->
        {
            try {
                List<JoinSettingElementMedia> joinSettingElementMediaList = (List<JoinSettingElementMedia>) result;
                int totalIndex = joinSettingElementMediaList.size();
                Runnable runnable = new Runnable() {
                    @Override
                    public void run() {
                        if (joinSettingElementMediaList.size() != 0) {
                            if (lastIndexBackground >= totalIndex)
                                lastIndexBackground = 0;

                            JoinSettingElementMedia joinSettingElementMedia = joinSettingElementMediaList.get(lastIndexBackground);
                            File file = new File(joinSettingElementMedia.urlImage);
                            if(!file.exists())
                            {
                                topNavigationFragment = topNavigationFragment.newInstance("");
                                FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                                ft.replace(R.id.topNavFrameLayout, topNavigationFragment).addToBackStack(null);
                                ft.commit();
                            }
                            Uri uri = Uri.fromFile(file);
//                    Picasso.with(MainActivity.this).load(uri).into(backgroundIV);
                            try {
                                InputStream inputStream = getContentResolver().openInputStream(uri);
                                BitmapFactory.Options options = new BitmapFactory.Options();
                                options.inPreferredConfig = Bitmap.Config.RGB_565;
                                Bitmap image = BitmapFactory.decodeStream(inputStream, null, options);
                                backgroundIV.setImageBitmap(image);
                            } catch (FileNotFoundException e) {
                                e.printStackTrace();
                            }
                            lastIndexBackground++;
                            handler.postDelayed(this::run, joinSettingElementMedia.duration);
                        }

                    }
                };
                handler.postDelayed(runnable, 100);
            } catch (Exception ex) {
                Log.d(TAG, ex.getMessage());
            }


        });
    }

    MenuFragment.IMenuMainFragment menuMainFragmentCallback = new MenuFragment.IMenuMainFragment() {
        @Override
        public void onClickMenu(String menuName, int menuId) {
            Log.i(TAG, "onFocusChange=" + menuName + " " + menuId);
            intentToActivity(menuId);
        }
    };

    void intentToActivity(int menuId) {

        switch (menuId) {
            case C.MENU_SEARCH_SONG:
                Log.d(TAG, "MENU_SEARCH_SONG Clicked");
                //set playlistId 0 jika bukan untuk add song dari halaman playlist
                KaraokeActivity.startActivity(MainActivity.this, 0);
                break;
            case C.MENU_PLAYLIST:
                Log.d(TAG, "MENU_PLAYLIST Clicked");
                PlaylistActivity.startActivity(MainActivity.this, 0);
                break;
            case C.MENU_UPDATE_SONG:
                Log.d(TAG, "MENU_UPDATE_SONG Clicked");
                UpdateSongActivity.startActivity(MainActivity.this);
                break;
            case C.MENU_ACCESSORIES:
                Log.d(TAG, "MENU_ACCESSORIES Clicked");
                AccessoriesActivity.startActivity(MainActivity.this);
                break;
            case C.MENU_MESSAGES:
                Log.d(TAG, "MENU_MESSAGES Clicked");
                MessagesActivity.startActivity(MainActivity.this);
                break;
            case C.MENU_SETTING:
                Log.d(TAG, "MENU_SETTING Clicked");
                showDialogSetting();
                break;
            case C.MENU_YOOLIVE:
                Log.d(TAG, "MENU_LIVE_CHANNEL Clicked");
                showYooLiveTV();
                break;
            case C.MENU_YOUTUBE:
                Log.d(TAG, "MENU_YOUTUBE Clicked");
                showYoutube();
                break;
            case C.MENU_VIDIO:
                Log.d(TAG, "MENU_VIDIO Clicked");
                showVidio();
                break;
            default:
                Toast.makeText(this, "Not implemented", Toast.LENGTH_SHORT).show();
                break;
        }

        Util.hideNavigationBar(MainActivity.this);

    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            Util.hideNavigationBar(MainActivity.this);
            //preventing default implementation previous to android.os.Build.VERSION_CODES.ECLAIR
            if (intentFromOTT == 0) {
                Log.d(TAG, "Back Pressed");
                if (listMoodView.getVisibility() == View.VISIBLE)
                    listMoodView.setVisibility(View.GONE);
                return true;
            } else {
                //minimize app
                moveTaskToBack(true);
                finish();
            }
        }
        if (kg.processKey(keyCode)) {
            SettingAdminActivity.startActivity(this);
        }
        return super.onKeyDown(keyCode, event);
    }

    void showDialogSetting() {
        passwordSetting = Global.getDefaultPasswordSetting();
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.alert_enter_setting, null);
        dialogBuilder.setView(dialogView);

//        EditText passwordET = dialogView.findViewById(R.id.passwordET);
        OtpView otpView = dialogView.findViewById(R.id.otpView);
        Button okButton = (Button) dialogView.findViewById(R.id.okButton);
        Button cancelButton = (Button) dialogView.findViewById(R.id.cancelButton);
        Button forgotPasswordButton = dialogView.findViewById(R.id.forgotPasswordButton);
        TextView infoTV = dialogView.findViewById(R.id.infoTV);

        otpView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                int length = s.length();
                if (length == 6) {
                    okButton.requestFocus();
                }
            }
        });

        AlertDialog alertDialog = dialogBuilder.create();

        okButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (passwordSetting.equals(otpView.getText().toString())) {
                    SettingActivity.startActivity(MainActivity.this, intentFromOTT);
                    alertDialog.dismiss();
                } else {
                    countWrongPassword++;
                    otpView.setError("Your password wrong");
                    otpView.setText("");
                    otpView.requestFocus();
                }
                if (countWrongPassword >= 3) {
                    forgotPasswordButton.setVisibility(View.VISIBLE);
                }
            }
        });

        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                alertDialog.dismiss();

            }
        });

        forgotPasswordButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Global.setDefaultPasswordSetting(C.default_password_setting);
                passwordSetting = C.default_password_setting;
                infoTV.setVisibility(View.VISIBLE);
            }
        });


        alertDialog.show();
        WindowManager.LayoutParams layoutParams = alertDialog.getWindow().getAttributes();
        layoutParams.width = ViewGroup.LayoutParams.WRAP_CONTENT;
        alertDialog.show();
        alertDialog.getWindow().setLayout(430, WindowManager.LayoutParams.WRAP_CONTENT);

    }

    void showVidio()
    {
        int checkInternet = Util.getInternetType(MainActivity.this);
        if(checkInternet!=0)
        {
            Intent launchIntent = getPackageManager().getLaunchIntentForPackage(APPLICATION_ID_VIDIO);
            if (launchIntent != null) {
                startActivity(launchIntent);//null pointer check in case package name was not found
            }
        }
        else
        {
            showDialogToConnectWifi();
        }
    }

    void showYoutube()
    {
        int checkInternet = Util.getInternetType(MainActivity.this);
        if(checkInternet!=0)
        {
            Intent launchIntent = getPackageManager().getLaunchIntentForPackage(YOUTUBE_ID);
            if (launchIntent != null) {
                startActivity(launchIntent);//null pointer check in case package name was not found
            }
        }
        else
        {
            showDialogToConnectWifi();
        }
    }

    void showYooLiveTV()
    {
        int checkInternet = Util.getInternetType(MainActivity.this);
        if(checkInternet!=0)
        {
            YoomediaActivity.startActivity(MainActivity.this);
        }
        else
        {
            showDialogToConnectWifi();
        }
    }

    void showDialogToConnectWifi()
    {
        KAlertDialog pDialog = new KAlertDialog(MainActivity.this, KAlertDialog.WARNING_TYPE);
        pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        pDialog.setTitleText("Information");
        pDialog.setContentText("Heyoo karaoke anda tidak terkoneksi ke internet. Tolong sambungkan WIFI ke internet");
        pDialog.setCancelable(true);
        pDialog.setConfirmText("Sambungkan");
//        pDialog.setCancelText(getString(R.string.cancel));
        pDialog.setConfirmClickListener(new KAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(KAlertDialog kAlertDialog) {
                pDialog.dismissWithAnimation();
                Intent intent = new Intent(WifiManager.ACTION_PICK_WIFI_NETWORK);
                startActivity(intent);
            }
        });
//        pDialog.setCancelClickListener(new KAlertDialog.OnSweetClickListener() {
//            @Override
//            public void onClick(KAlertDialog kAlertDialog) {
//                kAlertDialog.dismissWithAnimation();
//            }
//        });
        pDialog.show();
    }


    public void onResume() {
        super.onResume();
        Util.hideNavigationBar(MainActivity.this);
    }


    SettingActivity.ISettingActivityListener iSettingActivityListener = new SettingActivity.ISettingActivityListener() {
        @Override
        public void onRefreshTitleApp() {
            topNavigationFragment = topNavigationFragment.newInstance("");
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.topNavFrameLayout, topNavigationFragment).addToBackStack(null);
            ft.commitAllowingStateLoss();
        }

        @Override
        public void onChangeBackground() {
            fetchListBackground();
        }

        @Override
        public void onChangeLanguage() {
            String languageId = Global.getLanguageId();
            Util.setLanguage(MainActivity.this, languageId);

            MainActivity.startActivity(MainActivity.this, 0 );
            finish();
        }
    };

    MoodFragment.MoodFragmentListener moodFragmentListener = new MoodFragment.MoodFragmentListener() {
        @Override
        public void onMoodClick() {
            listMoodView.setVisibility(View.VISIBLE);
        }
    };

    ListMoodFragment.ListMoodListener listMoodListener = new ListMoodFragment.ListMoodListener() {
        @Override
        public void onClick(TSticker tSticker) {
            listMoodView.setVisibility(View.GONE);
            moodFragment.updateMood(tSticker);
            Global.setStickerId(tSticker.stickerId);
        }
    };


}

package com.madeira.entertainz.karaoke.menu_playlist;

import android.app.AlertDialog;
import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.drawable.GradientDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.ScaleAnimation;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.madeira.entertainz.helper.RecyclerViewAdapter;
import com.madeira.entertainz.karaoke.R;
import com.madeira.entertainz.karaoke.config.C;
import com.madeira.entertainz.karaoke.config.Global;
import com.madeira.entertainz.karaoke.menu_karaoke.KaraokeActivity;
import com.madeira.entertainz.karaoke.player.PlayerActivity;
import com.madeira.entertainz.karaoke.DBLocal.DbAccess;
import com.madeira.entertainz.karaoke.DBLocal.TSong;
import com.madeira.entertainz.karaoke.DBLocal.TSongPlaylist;
import com.madeira.entertainz.library.PerformAsync2;
import com.madeira.entertainz.library.Util;
import com.madeira.entertainz.library.UtilRenderScript;
import com.mukesh.OtpView;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import es.dmoral.toasty.Toasty;


public class PlaylistDetailFragment extends Fragment implements PlaylistTheme.IPlaylistTheme {
    String TAG = "PlaylistDetailFragment";

    public static final String KEY_PLAYLIST_ID = "KEY_PLAYLIST_ID";
    public static final String KEY_PLAYLIST_NAME = "KEY_PLAYLIST_NAME";

    private int playlistId;
    private String playlistName;
    static String passwordSetting = "";

    PlaylistTheme playlistTheme;
    List<TSongPlaylist> tSongPlaylistList = new ArrayList<>();
    RecyclerViewAdapter adapter;

    View root;
    LinearLayout rootLL;
    RecyclerView recyclerView;
    LinearLayout actionLL;
    Button renameButton;
    Button addSongButton;
    Button deleteButton;
    TextView playlistNameTV;
    EditText playlistNameET;


    private IPlaylistDetailListener mListener;

    public PlaylistDetailFragment() {
        // Required empty public constructor
    }

    public interface IPlaylistDetailListener {
        void onListClick(ArrayList<TSong> tSongs, int position);

        void onDeleteOrRenamePlaylist();
    }

    public static PlaylistDetailFragment newInstance(String prmPlaylistId, String prmPlaylistName) {
        PlaylistDetailFragment fragment = new PlaylistDetailFragment();
        Bundle args = new Bundle();
        args.putString(KEY_PLAYLIST_ID, prmPlaylistId);
        args.putString(KEY_PLAYLIST_NAME, prmPlaylistName);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            playlistId = getArguments().getInt(KEY_PLAYLIST_ID);
            playlistName = getArguments().getString(KEY_PLAYLIST_NAME);
            fetchSongPlaylist();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        root = inflater.inflate(R.layout.fragment_playlist_detail, container, false);
        bind();
        playlistTheme = new PlaylistTheme(getActivity(), this);
        playlistTheme.setTheme();
        setOnActionObject();
        return root;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void setThemeUpdateDate(Date lastUpdateTheme) {

    }

    @Override
    public void setListColorTheme(int color) {
        rootLL.setBackgroundResource(R.drawable.tags_rounded_corners);
        GradientDrawable rootDrawable = (GradientDrawable) rootLL.getBackground();
        rootDrawable.setColor(color);

        actionLL.setBackgroundResource(R.drawable.tags_rounded_corners);
        GradientDrawable searchDrawable = (GradientDrawable) actionLL.getBackground();
        searchDrawable.setColor(color);
    }

    @Override
    public void setTextAndSelectionColorTheme(ColorStateList colorSelection, ColorStateList colorText, int colorTextSelected, int colorTextFocus, int colorTextNormal, int colorSelectionSelected, int colorSelectionFocus) {
        this.colorSelection = colorSelection;
        this.colorText = colorText;
        playlistNameTV.setTextColor(colorTextNormal);
    }

    public void setCallback(IPlaylistDetailListener listener) {
        this.mListener = listener;
    }

    void bind() {
        rootLL = (LinearLayout) root.findViewById(R.id.rootLL);
        recyclerView = (RecyclerView) root.findViewById(R.id.recyclerView);
        actionLL = (LinearLayout) root.findViewById(R.id.actionLL);
        renameButton = (Button) root.findViewById(R.id.renameButton);
        playlistNameTV = (TextView) root.findViewById(R.id.playlistNameTV);
        addSongButton = (Button) root.findViewById(R.id.addSongButton);
        deleteButton = (Button) root.findViewById(R.id.deletePlaylistButton);
        playlistNameET = (EditText) root.findViewById(R.id.playlistNameET);
        playlistNameTV.setText(playlistName);
        playlistNameET.setText(playlistName);
    }

    void fetchSongPlaylist() {
        try {
            PerformAsync2.run(performAsync -> {
                DbAccess dbAccess = DbAccess.Instance.create(getActivity());
//                dbAccess.daoAccessTSongPlaylist().deleteAll();
//                dbAccess.daoAccessTPlaylist().deleteAll();
                if (playlistId != 0)
                    tSongPlaylistList = dbAccess.daoAccessTSongPlaylist().getByPlaylistId(playlistId);
                Log.d(TAG, String.valueOf(tSongPlaylistList.size()));
                dbAccess.close();
                return 1;
            }).setCallbackResult(result -> {
                int i = (int) result;
                if (i != 1) {
                    return;
                } else {
                    setRecyclerView();
                    Log.d(TAG, String.valueOf(tSongPlaylistList.size()));
                }
            });

        } catch (Exception ex) {
            Log.d(TAG, String.valueOf(ex.getMessage()));
        }
    }

    void setRecyclerView() {
        adapter = new RecyclerViewAdapter(new RecyclerViewAdapter.IRecyclerViewAdapter() {
            @Override
            public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
                View view = LayoutInflater.from(getActivity()).inflate(R.layout.cell_playlist_detail, parent, false);
                ItemViewHolder item = new ItemViewHolder(view);
                return item;
            }

            @Override
            public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
                ItemViewHolder item = (ItemViewHolder) holder;
                item.bind(tSongPlaylistList.get(position), position);
            }

            @Override
            public int getItemCount() {
                return tSongPlaylistList.size();
            }
        });

        GridLayoutManager gridLayoutManager = new GridLayoutManager(getActivity(), 3);
        recyclerView.setLayoutManager(gridLayoutManager);
        recyclerView.setAdapter(adapter);
        /*recyclerView.setHasFixedSize(true);
        recyclerView.setItemViewCacheSize(20);
        recyclerView.setDrawingCacheEnabled(true);
        recyclerView.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);*/
    }

    class ItemViewHolder extends RecyclerView.ViewHolder {
        private static final String TAG = "ItemViewHolder";

        View root;
        TextView tvTitle;
        TextView tvSinger;
        ImageView thumbnailIV;

        public ItemViewHolder(View itemView) {
            super(itemView);
            root = itemView;
            selectedView = root;
            tvTitle = itemView.findViewById(R.id.text_title);
            tvSinger = itemView.findViewById(R.id.text_singer);
            thumbnailIV = itemView.findViewById(R.id.thumbnailIV);
        }

        public void bind(TSongPlaylist tSongPlaylist, int pos) {
            tvTitle.setText(tSongPlaylist.songName);
            tvSinger.setText(tSongPlaylist.artist);
            if (tSongPlaylist.thumbnail != null) {
                Uri uri = Uri.fromFile(new File(tSongPlaylist.thumbnail));
//                Picasso.with(getActivity()).load(uri).into(thumbnailIV);
                /*try {
                    InputStream inputStream = getActivity().getContentResolver().openInputStream(uri);
                    BitmapFactory.Options options = new BitmapFactory.Options();
                    options.inPreferredConfig = Bitmap.Config.RGB_565;
                    Bitmap image = BitmapFactory.decodeStream(inputStream, null, options);
                    thumbnailIV.setImageBitmap(image);
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }*/

                /*InputStream inputStream = null;
                try {
                    inputStream = getActivity().getContentResolver().openInputStream(uri);
                    BitmapFactory.Options options = new BitmapFactory.Options();
                    options.inSampleSize = 8;
                    Bitmap preview_bitmap = BitmapFactory.decodeStream(inputStream, null, options);
                    thumbnailIV.setImageBitmap(preview_bitmap);
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }*/
                UtilRenderScript.fetchBitmap(getContext(), tSongPlaylist.thumbnail, 300, thumbnailIV);

            }


            if (colorSelection != null) root.setBackgroundTintList(colorSelection);
//            untuk merubah warna text
            if (colorText != null) {
                tvTitle.setTextColor(colorText);
                tvSinger.setTextColor(colorText);
            }


            root.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showDetailSong(tSongPlaylist, pos);
                    Util.hideNavigationBar(getActivity());
                }
            });

            root.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View v, boolean hasFocus) {
                    if (hasFocus) {
                        updateSelectedViewUi(v, true);
                    } else {
                        updateSelectedViewUi(null, false);

                    }
                }
            });
        }


    }

    ColorStateList colorSelection, colorText;

    //function untuk declare semua action dari object di activity
    void setOnActionObject() {

        renameButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playlistNameET.setVisibility(View.VISIBLE);
                playlistNameTV.setVisibility(View.GONE);
                playlistNameET.requestFocus();
            }
        });
        addSongButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                KaraokeActivity.startActivity(getActivity(), playlistId);
                getActivity().finish();
            }
        });
        deleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDialogDeletePlaylist();
            }
        });

        playlistNameET.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (!playlistName.equals(s.toString())) {
                    PerformAsync2.run(performAsync ->
                    {
                        int result = 0;
                        try {
                            DbAccess dbAccess = DbAccess.Instance.create(getContext());

                            int isExist = dbAccess.daoAccessTPlaylist().existPlaylistName(s.toString());
                            if (isExist == 0) {
                                dbAccess.daoAccessTPlaylist().renamePlaylist(playlistId, s.toString());
                                result = 1;
                            } else
                                result = 2;
                            dbAccess.close();
                        } catch (Exception ex) {
                            result = 0;
                        }
                        return result;
                    }).setCallbackResult(result ->
                    {
                        int r = (int) result;
                        if (r == 1) {
                            playlistNameTV.setText(playlistNameET.getText());
                        } else if (r == 2) {
                            Toasty.error(getActivity(), getString(R.string.playlistAlreadyExists)).show();
//                            Toast.makeText(getActivity(), "Playlist name already exists", Toast.LENGTH_LONG).show();
                        }
                    });
                }
            }
        });

        playlistNameET.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
//                    playlistNameET.setVisibility(View.GONE);
//                    playlistNameTV.setVisibility(View.VISIBLE);
                    mListener.onDeleteOrRenamePlaylist();
                }
            }
        });

    }

    void showDetailSong(TSongPlaylist tSongPlaylist, int prmPosition) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.alert_playlist_song, null);
        dialogBuilder.setView(dialogView);

        ImageView thumbnailIV = (ImageView) dialogView.findViewById(R.id.thumbnailIV);
        TextView titleTV = (TextView) dialogView.findViewById(R.id.titleTV);
        TextView singerTV = (TextView) dialogView.findViewById(R.id.singerTV);
        Button playSongButton = (Button) dialogView.findViewById(R.id.playSongButton);
        Button deleteSongButton = (Button) dialogView.findViewById(R.id.deleteSongButton);

        if (tSongPlaylist.thumbnail != null) {
            UtilRenderScript.fetchBitmap(getContext(), tSongPlaylist.thumbnail, 300, thumbnailIV);
        }
        titleTV.setText(tSongPlaylist.songName);
        singerTV.setText(tSongPlaylist.artist);
        AlertDialog alertDialog = dialogBuilder.create();

        playSongButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Toast.makeText(getContext(), "play song", Toast.LENGTH_LONG).show();
                ArrayList<TSong> tSongArrayList = new ArrayList<>();
                for (TSongPlaylist itemSongPlaylist : tSongPlaylistList) {
                    TSong tSong = new TSong();
                    tSong.songId = String.valueOf(itemSongPlaylist.songId);
                    tSong.songName = itemSongPlaylist.songName;
                    tSong.artist = itemSongPlaylist.artist;
                    tSong.songURL = itemSongPlaylist.songURL;
                    tSong.vocalSound = String.valueOf(itemSongPlaylist.vocalSound);
                    tSong.duration = String.valueOf(itemSongPlaylist.duration);
                    tSong.thumbnail = itemSongPlaylist.thumbnail;
                    tSongArrayList.add(tSong);
                }
                PlayerActivity.startActivity(getActivity(), tSongArrayList, prmPosition);
                alertDialog.dismiss();

            }
        });

        deleteSongButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteSong(tSongPlaylist, prmPosition);
                alertDialog.dismiss();
            }
        });

        alertDialog.show();
        WindowManager.LayoutParams layoutParams = alertDialog.getWindow().getAttributes();
        layoutParams.width = ViewGroup.LayoutParams.WRAP_CONTENT;
        alertDialog.show();
        alertDialog.getWindow().setLayout(430, WindowManager.LayoutParams.WRAP_CONTENT);
        Util.hideNavigationBar(getActivity());

    }

    void deleteSong(TSongPlaylist tSongPlaylist, int prmPosition) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getContext());
        LayoutInflater inflater = this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.alert_delete_confirmation, null);
        dialogBuilder.setView(dialogView);
        Button okButton = (Button) dialogView.findViewById(R.id.okButton);
        Button cancelButton = (Button) dialogView.findViewById(R.id.cancelButton);

        AlertDialog alertDialog = dialogBuilder.create();

        okButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PerformAsync2.run(performAsync ->
                {
                    int result = 0;
                    try {
                        DbAccess dbAccess = DbAccess.Instance.create(getActivity());
                        dbAccess.daoAccessTSongPlaylist().delete(tSongPlaylist.songId);
                        dbAccess.close();
                        result = 1;
                    } catch (Exception ex) {
                        result = 0;
                    }
                    return result;
                }).setCallbackResult(result ->
                {
                    int r = (int) result;
                    if (r == 1) {
                        fetchSongPlaylist();
                    } else {
//                        Toast.makeText(getActivity(), "", Toast.LENGTH_LONG).show();
                        Toasty.error(getActivity(), getString(R.string.failedDeleteSong)).show();
                    }
                });
            }
        });

        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();

            }
        });

        alertDialog.show();
        WindowManager.LayoutParams layoutParams = alertDialog.getWindow().getAttributes();
        layoutParams.width = ViewGroup.LayoutParams.WRAP_CONTENT;
        alertDialog.show();
        alertDialog.getWindow().setLayout(350, WindowManager.LayoutParams.WRAP_CONTENT);

    }


    void updateSelectedViewUi(View view, boolean hasFocus) {
        if (selectedView != null) {
            scaleView(selectedView, 1.1f, 1);
            ViewCompat.setTranslationZ(selectedView, 0);
        }

        if (hasFocus) {
            selectedView = view;
            ViewCompat.setTranslationZ(selectedView, 100);
//                selectedView.setBackgroundResource(R.drawable.box_white);
            scaleView(selectedView, 1, 1.1f);
        } else {
            //remove selected & focus dari grid
            selectedView = null;
        }
    }

    public void scaleView(View v, float startScale, float endScale) {
        Animation anim = new ScaleAnimation(
                startScale, endScale, // Start and end values for the X axis scaling
                startScale, endScale, // Start and end values for the Y axis scaling
                Animation.RELATIVE_TO_SELF, 0.5f, // Pivot point of X scaling
                Animation.RELATIVE_TO_SELF, 0.5f); // Pivot point of Y scaling
        anim.setFillAfter(true); // Needed to keep the result of the animation
        anim.setDuration(100);
        v.startAnimation(anim);
    }

    static View selectedView;

    void showDialogDeletePlaylist() {
        passwordSetting = Global.getDefaultPasswordSetting();
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getContext());
        LayoutInflater inflater = this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.alert_enter_setting, null);
        dialogBuilder.setView(dialogView);

//        EditText passwordET = dialogView.findViewById(R.id.passwordET);
        OtpView otpView = dialogView.findViewById(R.id.otpView);
        Button okButton = (Button) dialogView.findViewById(R.id.okButton);
        Button cancelButton = (Button) dialogView.findViewById(R.id.cancelButton);
        Button forgotPasswordButton = dialogView.findViewById(R.id.forgotPasswordButton);
        TextView infoTV = dialogView.findViewById(R.id.infoTV);

        AlertDialog alertDialog = dialogBuilder.create();

        okButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (passwordSetting.equals(otpView.getText().toString())) {
                    deletePlaylist();
                    alertDialog.dismiss();
                } else {
                    otpView.setError(getString(R.string.yourPasswordWrong));
                    otpView.setText("");
                    otpView.requestFocus();
                }
            }
        });

        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();

            }
        });

        forgotPasswordButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Global.setDefaultPasswordSetting(C.default_password_setting);
                passwordSetting = C.default_password_setting;
                infoTV.setVisibility(View.VISIBLE);
            }
        });

        otpView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                int length = s.length();
                if (length == 6) {
                    okButton.requestFocus();
                }
            }
        });


        alertDialog.show();
        WindowManager.LayoutParams layoutParams = alertDialog.getWindow().getAttributes();
        layoutParams.width = ViewGroup.LayoutParams.WRAP_CONTENT;
        alertDialog.show();
        alertDialog.getWindow().setLayout(430, WindowManager.LayoutParams.WRAP_CONTENT);

    }

    void deletePlaylist() {
        PerformAsync2.run(performAsync ->
        {
            int result = 0;
            try {
                DbAccess dbAccess = DbAccess.Instance.create(getContext());
                dbAccess.daoAccessTPlaylist().delete(playlistId);
                dbAccess.daoAccessTSongPlaylist().deleteByPlaylist(playlistId);
                dbAccess.close();
                result = 1;
            } catch (Exception ex) {
                result = 0;
            }
            return result;
        }).setCallbackResult(result ->
        {
            int i = (int) result;
            if (i == 1) {
                adapter.notifyDataSetChanged();
                mListener.onDeleteOrRenamePlaylist();
            }
        });
    }

}

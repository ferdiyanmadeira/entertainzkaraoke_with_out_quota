package com.madeira.entertainz.karaoke.menu_playlist;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Handler;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.madeira.entertainz.karaoke.DBLocal.JoinSettingElementMedia;
import com.madeira.entertainz.karaoke.R;
import com.madeira.entertainz.karaoke.config.Global;
import com.madeira.entertainz.karaoke.menu_karaoke.KaraokeActivity;
import com.madeira.entertainz.karaoke.menu_navigation.TopNavigationFragment;
import com.madeira.entertainz.karaoke.DBLocal.DbAccess;
import com.madeira.entertainz.karaoke.DBLocal.TSong;
import com.madeira.entertainz.library.PerformAsync2;
import com.madeira.entertainz.library.Util;

import net.yslibrary.android.keyboardvisibilityevent.KeyboardVisibilityEvent;
import net.yslibrary.android.keyboardvisibilityevent.KeyboardVisibilityEventListener;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import es.dmoral.toasty.Toasty;

import static com.madeira.entertainz.karaoke.config.CacheData.lastIndexBackground;

public class PlaylistActivity extends AppCompatActivity {
    String TAG = "PlaylistActivity";
    ImageView backgroundIV;
    LinearLayout blankPlaylistLL;
    int playlistId=0;

    TopNavigationFragment topNavigationFragment = new TopNavigationFragment();
    PlaylistListFragment playlistListFragment = new PlaylistListFragment();
    PlaylistDetailFragment playlistDetailFragment = new PlaylistDetailFragment();
    //    KaraokePopupFragment karaokePopupFragment = new KaraokePopupFragment();
    Handler handler = new Handler();
    Runnable runnable;

    public static void startActivity(Activity activity, int playlistId) {
        Intent intent = new Intent(activity, PlaylistActivity.class);
        intent.putExtra(KaraokeActivity.KEY_PLAYLIST_ID, playlistId);
        activity.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_playlist);
        bind();
        fetchListBackground();
        bindFragment();
        checkPlaylist();
        Util.hideNavigationBar(PlaylistActivity.this);
        playlistId= getIntent().getIntExtra(KaraokeActivity.KEY_PLAYLIST_ID,0);
        KeyboardVisibilityEvent.setEventListener(
                this,
                new KeyboardVisibilityEventListener() {
                    @Override
                    public void onVisibilityChanged(boolean isOpen) {
                        // some code depending on keyboard visiblity status
                        if (isOpen) {
                            Toasty.info(PlaylistActivity.this, getString(R.string.pressBackToHideKeyBoard)).show();
                        }

                    }
                });

        String languageId = Global.getLanguageId();
        Util.setLanguage(this, languageId);
    }

    void bind() {
        backgroundIV = (ImageView) findViewById(R.id.backgroundIV);
        blankPlaylistLL = findViewById(R.id.blankPlaylistLL);
    }

    void checkPlaylist()
    {
        try {
            PerformAsync2.run(performAsync ->
            {
                int result =0;
                DbAccess dbAccess = DbAccess.Instance.create(this);
                result = dbAccess.daoAccessTPlaylist().checkExistPlaylist();
                dbAccess.close();
                return result;
            }).setCallbackResult(result ->
            {
                int r =(int)result;
                if(r==0)
                    hideFragment(false);
                else
                    hideFragment(true);
            });
        }
        catch (Exception ex)
        {
            Log.d(TAG,ex.getMessage());
        }
    }

    void bindFragment() {
//        topNavigationFragment = topNavigationFragment.newInstance(true);
        topNavigationFragment = topNavigationFragment.newInstance(getString(R.string.title_playlist));
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.topNavFrameLayout, topNavigationFragment);

        playlistId= getIntent().getIntExtra(KaraokeActivity.KEY_PLAYLIST_ID,0);
        Bundle bundle = new Bundle();
        bundle.putInt(KaraokeActivity.KEY_PLAYLIST_ID, playlistId);
        playlistListFragment.setArguments(bundle);
        ft.replace(R.id.categoryFrameLayout, playlistListFragment).addToBackStack(null);

        ft.commit();
        playlistListFragment.setCallback(iPlaylistListener);

    }

    void fetchListBackground() {
        PerformAsync2.run(performAsync ->
                {
                    List<JoinSettingElementMedia> result = new ArrayList<>();
                    DbAccess dbAccess = DbAccess.Instance.create(PlaylistActivity.this);
                    result = dbAccess.daoAccessThemeMedia().joinSettingElementMediaActive();
                    return result;
                }
        ).setCallbackResult(result ->
        {
            List<JoinSettingElementMedia> joinSettingElementMediaList = (List<JoinSettingElementMedia>) result;
            int totalIndex = joinSettingElementMediaList.size();
            runnable = new Runnable() {
                @Override
                public void run() {
                    if (joinSettingElementMediaList.size() != 0) {
                        if (lastIndexBackground >= totalIndex)
                            lastIndexBackground = 0;

                        JoinSettingElementMedia joinSettingElementMedia = joinSettingElementMediaList.get(lastIndexBackground);
                        Uri uri = Uri.fromFile(new File(joinSettingElementMedia.urlImage));
//                    Picasso.with(PlaylistActivity.this).load(uri).into(backgroundIV);
                        try {
                            InputStream inputStream = getContentResolver().openInputStream(uri);
                            BitmapFactory.Options options = new BitmapFactory.Options();
                            options.inPreferredConfig = Bitmap.Config.RGB_565;
                            Bitmap image = BitmapFactory.decodeStream(inputStream, null, options);
                            backgroundIV.setImageBitmap(image);
                        } catch (FileNotFoundException e) {
                            e.printStackTrace();
                        }
                        lastIndexBackground++;
                        handler.postDelayed(this::run, joinSettingElementMedia.duration);
                    }

                }
            };
            handler.postDelayed(runnable, 100);
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Util.hideNavigationBar(this);

        //agar runable tidak running ketika di finish activity
        handler.removeCallbacks(runnable);
        handler.removeCallbacksAndMessages(null);
        finish();
    }

    PlaylistListFragment.IPlaylistListener iPlaylistListener = new PlaylistListFragment.IPlaylistListener() {
        @Override
        public void onCategoryClick(int playlistId, String playlistName) {
            //detach first, for refresh fragment

//            playlistDetailFragment.newInstance(playlistId);
            playlistDetailFragment = new PlaylistDetailFragment();
            Bundle args = new Bundle();
            args.putInt(PlaylistDetailFragment.KEY_PLAYLIST_ID, playlistId);
            args.putString(PlaylistDetailFragment.KEY_PLAYLIST_NAME, playlistName);
            playlistDetailFragment.setArguments(args);
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.songFrameLayout, playlistDetailFragment).addToBackStack(null);
            ;
            ft.commit();
            playlistDetailFragment.setCallback(iPlaylistDetailListener);
        }
    };

    PlaylistDetailFragment.IPlaylistDetailListener iPlaylistDetailListener = new PlaylistDetailFragment.IPlaylistDetailListener() {

        @Override
        public void onListClick(ArrayList<TSong> tSongs, int position) {

        }

        @Override
        public void onDeleteOrRenamePlaylist() {

            PlaylistActivity.startActivity(PlaylistActivity.this,0);
            finish();
        }
    };

    public void onResume() {
        super.onResume();
        Util.hideNavigationBar(PlaylistActivity.this);
    }

    void hideFragment(boolean prmHide)
    {
        View playlistView = findViewById(R.id.categoryFrameLayout);
        View playlistDtView = findViewById(R.id.songFrameLayout);
        if(prmHide)
        {
            playlistDtView.setVisibility(View.VISIBLE);
            playlistView.setVisibility(View.VISIBLE);
            blankPlaylistLL.setVisibility(View.GONE);
        }
        else
        {
            playlistDtView.setVisibility(View.GONE);
            playlistView.setVisibility(View.GONE);
            blankPlaylistLL.setVisibility(View.VISIBLE);
        }
    }
}

package com.madeira.entertainz.karaoke.menu_playlist;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.madeira.entertainz.helper.RecyclerViewAdapter;
import com.madeira.entertainz.karaoke.R;
import com.madeira.entertainz.karaoke.DBLocal.DbAccess;
import com.madeira.entertainz.karaoke.DBLocal.TPlaylist;
import com.madeira.entertainz.karaoke.menu_karaoke.KaraokeActivity;
import com.madeira.entertainz.library.PerformAsync2;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


public class PlaylistListFragment extends Fragment implements PlaylistTheme.IPlaylistTheme {
    String TAG = "PlaylistListFragment";

    View root;
    LinearLayout playlistLL;
    TextView playlistTV;
    LinearLayout recyclerLL;
    RecyclerView recyclerView;
    RecyclerViewAdapter adapter;
    private IPlaylistListener mListener;

    int playlistId = 0;

    PlaylistTheme playlistTheme;
    List<TPlaylist> tPlaylists = new ArrayList<>();

    public PlaylistListFragment() {
        // Required empty public constructor
    }

    public interface IPlaylistListener {
        // TODO: Update argument type and name
        void onCategoryClick(int playlistId, String playlistName);
    }


    public static PlaylistListFragment newInstance() {
        PlaylistListFragment fragment = new PlaylistListFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            playlistId = getArguments().getInt(KaraokeActivity.KEY_PLAYLIST_ID);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        root = inflater.inflate(R.layout.fragment_playlist_list, container, false);
        bind();
        playlistTheme = new PlaylistTheme(getActivity(), this);
        playlistTheme.setTheme();
        fetchPlaylist();
        return root;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    @Override
    public void setThemeUpdateDate(Date lastUpdateTheme) {

    }

    @Override
    public void setListColorTheme(int color) {
        playlistLL.setBackgroundResource(R.drawable.tags_rounded_corners);
        recyclerLL.setBackgroundResource(R.drawable.tags_rounded_corners);

        GradientDrawable categoryDrawable = (GradientDrawable) playlistLL.getBackground();
        categoryDrawable.setColor(color);

        GradientDrawable recycleDrawable = (GradientDrawable) recyclerLL.getBackground();
        recycleDrawable.setColor(color);
    }

    @Override
    public void setTextAndSelectionColorTheme(ColorStateList colorSelection, ColorStateList colorText, int colorTextSelected, int colorTextFocus, int colorTextNormal, int colorSelectionSelected, int colorSelectionFocus) {
        playlistTV.setTextColor(colorTextNormal);
        this.colorSelection = colorSelection;
        this.colorText = colorText;
    }

    public void setCallback(IPlaylistListener listener) {
        this.mListener = listener;
    }

    void bind() {
        playlistLL = (LinearLayout) root.findViewById(R.id.categoryLL);
        playlistTV = (TextView) root.findViewById(R.id.categoryTV);
        recyclerLL = (LinearLayout) root.findViewById(R.id.recyclerLL);
        recyclerView = (RecyclerView) root.findViewById(R.id.recyclerView);
    }

    void fetchPlaylist() {
        try {
            PerformAsync2.run(performAsync -> {
                DbAccess dbAccess = DbAccess.Instance.create(getActivity());
                tPlaylists = dbAccess.daoAccessTPlaylist().getAll();
                Log.d(TAG, String.valueOf(tPlaylists.size()));
                dbAccess.close();
                return 1;
            }).setCallbackResult(result -> {
                int i = (int) result;
                if (i != 1) {
                    return;
                } else {
                    setRecyclerView();
                    Log.d(TAG, String.valueOf(tPlaylists.size()));
                }
            });

        } catch (Exception ex) {
            Log.d(TAG, String.valueOf(ex.getMessage()));
        }
    }

    void setRecyclerView() {
        adapter = new RecyclerViewAdapter(new RecyclerViewAdapter.IRecyclerViewAdapter() {
            @Override
            public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
                View view = LayoutInflater.from(getActivity()).inflate(R.layout.cell_karaoke_category, parent, false);
                ItemViewHolder item = new ItemViewHolder(view);
                return item;
            }

            @Override
            public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
                ItemViewHolder item = (ItemViewHolder) holder;
                item.bind(tPlaylists.get(position), position);
            }

            @Override
            public int getItemCount() {
                return tPlaylists.size();
            }
        });

        recyclerView.setAdapter(adapter);
    }

    //view ini khusus di pakai utk ItemViewHolder, utk selectedItemView
    View selectedView;
    int selectedItem;

    class ItemViewHolder extends RecyclerView.ViewHolder {
        private static final String TAG = "ItemViewHolder";

        View root;
        TextView tvTitle;

        public ItemViewHolder(View itemView) {
            super(itemView);
            root = itemView;

            tvTitle = itemView.findViewById(R.id.text_title);

//            selectionColor = itemView.findViewById(R.id.selectioncolor);
        }

        public void bind(TPlaylist tPlaylist, int pos) {
            tvTitle.setText(tPlaylist.playlistName);

            if (playlistId == 0) {
                if (selectedItem == pos) {
                    selectView(root);
                    mListener.onCategoryClick(tPlaylist.playlistId, tPlaylist.playlistName);

                }
                if (pos == 0) {
                    root.setSelected(true);
                }
            } else {
                if (tPlaylist.playlistId == playlistId) {
                    selectView(root);
                    mListener.onCategoryClick(tPlaylist.playlistId, tPlaylist.playlistName);
                    selectedItem=pos;
                    root.requestFocus();
                }

            }

//            untuk merubah warna selection
            if (colorSelection != null) root.setBackgroundTintList(colorSelection);

//            untuk merubah warna text
            if (colorText != null) tvTitle.setTextColor(colorText);



            root.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    selectView(v);
                    selectedItem = pos;
                    mListener.onCategoryClick(tPlaylist.playlistId, tPlaylist.playlistName);

                }
            });
        }

        void selectView(View v) {
            //clear selection
            if (selectedView != null) {
                selectedView.setSelected(false);
            }

            //make selection
            selectedView = v;
            selectedView.setSelected(true);
        }
    }

    ColorStateList colorSelection, colorText;
}

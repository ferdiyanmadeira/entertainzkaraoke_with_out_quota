package com.madeira.entertainz.karaoke.menu_radio;


import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.madeira.entertainz.helper.RecyclerViewAdapter;
import com.madeira.entertainz.karaoke.DBLocal.DbAccess;
import com.madeira.entertainz.karaoke.DBLocal.TRadio;
import com.madeira.entertainz.karaoke.DBLocal.TSongCategory;
import com.madeira.entertainz.karaoke.R;
import com.madeira.entertainz.library.PerformAsync2;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


public class RadioListFragment extends Fragment implements RadioTheme.IRadioTheme {
    String TAG = "RadioListFragment";

    public static final String KEY_LAST_POSITION="KEY_LAST_POSITION";
    public static final String KEY_FLAG_INCREASE="KEY_FLAG_INCREASE";
    public static final String KEY_RADIOID = "KEY_CHANNEL";

    int lastPosition;
    boolean flagIncrease=false;
    int radioId;

    View root;
    LinearLayout categoryLL;
    TextView categoryTV;
    LinearLayout recyclerLL;
    RecyclerView recyclerView;
    RecyclerViewAdapter adapter;

    private IRadioListListener mListener;
    RadioTheme radioTheme;
    List<TRadio> tRadioList = new ArrayList<>();

    public RadioListFragment() {
        // Required empty public constructor
    }

    public static RadioListFragment newInstance(String param1, String param2) {
        RadioListFragment fragment = new RadioListFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            radioId = getArguments().getInt(KEY_RADIOID);
            lastPosition = getArguments().getInt(KEY_LAST_POSITION);
            flagIncrease = getArguments().getBoolean(KEY_FLAG_INCREASE);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        root = inflater.inflate(R.layout.fragment_radio_list, container, false);
        bind();
        radioTheme = new RadioTheme(getActivity(), this);
        radioTheme.setTheme();
        fetchSongCategory();
        return root;
    }

    @Override
    public void setThemeUpdateDate(Date lastUpdateTheme) {

    }

    @Override
    public void setListColorTheme(int color) {
        categoryLL.setBackgroundResource(R.drawable.tags_rounded_corners);
        recyclerLL.setBackgroundResource(R.drawable.tags_rounded_corners);

        GradientDrawable categoryDrawable = (GradientDrawable) categoryLL.getBackground();
        categoryDrawable.setColor(color);

        GradientDrawable recycleDrawable = (GradientDrawable) recyclerLL.getBackground();
        recycleDrawable.setColor(color);
    }

    @Override
    public void setTextAndSelectionColorTheme(ColorStateList colorSelection, ColorStateList colorText, int colorTextSelected, int colorTextFocus, int colorTextNormal, int colorSelectionSelected, int colorSelectionFocus) {
        categoryTV.setTextColor(colorTextNormal);
        this.colorSelection = colorSelection;
        this.colorText = colorText;
    }

    public interface IRadioListListener {
        // TODO: Update argument type and name
        void onCategoryClick(int categoryId, int position);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
       /* if (context instanceof IRadioListListener) {
            mListener = (IRadioListListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }*/
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;

    }

    void bind() {
        categoryLL = (LinearLayout) root.findViewById(R.id.categoryLL);
        categoryTV = (TextView) root.findViewById(R.id.categoryTV);
        recyclerLL = (LinearLayout) root.findViewById(R.id.recyclerLL);
        recyclerView = (RecyclerView) root.findViewById(R.id.recyclerView);
    }

    void fetchSongCategory() {
        try {
            PerformAsync2.run(performAsync -> {
                DbAccess dbAccess = DbAccess.Instance.create(getActivity());
                tRadioList = dbAccess.daoAccessTRadio().getAll();
                Log.d(TAG, String.valueOf(tRadioList.size()));
                dbAccess.close();
                return 1;
            }).setCallbackResult(result -> {
                int i = (int) result;
                if (i != 1) {
                    return;
                } else {
                    setRecyclerView();
                    Log.d(TAG, String.valueOf(tRadioList.size()));
                }
            });

        } catch (Exception ex) {
            Log.d(TAG, String.valueOf(ex.getMessage()));
        }
    }

    void setRecyclerView() {
        adapter = new RecyclerViewAdapter(new RecyclerViewAdapter.IRecyclerViewAdapter() {
            @Override
            public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
                View view = LayoutInflater.from(getActivity()).inflate(R.layout.cell_radio_list, parent, false);
                ItemViewHolder item = new ItemViewHolder(view);
                return item;
            }

            @Override
            public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
                ItemViewHolder item = (ItemViewHolder) holder;
                item.bind(tRadioList.get(position), position);
            }

            @Override
            public int getItemCount() {
                return tRadioList.size();
            }
        });
        recyclerView.setAdapter(adapter);
    }

    //view ini khusus di pakai utk ItemViewHolder, utk selectedItemView
    View selectedView;
    int selectedItem;

    class ItemViewHolder extends RecyclerView.ViewHolder {
        private static final String TAG = "ItemViewHolder";

        View root;
        TextView tvTitle;

        public ItemViewHolder(View itemView) {
            super(itemView);
            root = itemView;
            tvTitle = itemView.findViewById(R.id.text_title);
        }

        public void bind(TRadio tRadio, int pos) {
            tvTitle.setText(tRadio.radioName);
            if (selectedItem == pos) {
                selectView(root);
            }
//            untuk merubah warna selection
            if (colorSelection != null) root.setBackgroundTintList(colorSelection);
//            untuk merubah warna text
            if (colorText != null) tvTitle.setTextColor(colorText);
            if (pos == 0) {
                root.setSelected(true);
            }

            if(flagIncrease)
            {
                if(pos==lastPosition+1)
                {
                    root.setSelected(true);
                    selectedItem = pos;
                    mListener.onCategoryClick(tRadio.radioId, pos);
                }
            }
            else
            {
                if(pos==lastPosition-1)
                {
                    root.setSelected(true);
                    selectedItem = pos;
                    mListener.onCategoryClick(tRadio.radioId, pos);
                }
            }

            root.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    selectView(v);
                    selectedItem = pos;
                    mListener.onCategoryClick(tRadio.radioId, pos);
                }
            });

        }

        void selectView(View v) {
            //clear selection
            if (selectedView != null) {
                selectedView.setSelected(false);
            }

            //make selection
            selectedView = v;
            selectedView.setSelected(true);
        }
    }

    ColorStateList colorSelection, colorText;

    public void setCallback(IRadioListListener listener) {
        this.mListener = listener;
    }

}

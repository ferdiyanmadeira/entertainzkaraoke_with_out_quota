package com.madeira.entertainz.karaoke.menu_radio;


import android.content.res.ColorStateList;
import android.graphics.drawable.GradientDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.chibde.visualizer.LineBarVisualizer;
import com.google.android.exoplayer2.ExoPlayer;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.extractor.DefaultExtractorsFactory;
import com.google.android.exoplayer2.extractor.ExtractorsFactory;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelection;
import com.google.android.exoplayer2.upstream.BandwidthMeter;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.util.Log;
import com.google.android.exoplayer2.util.Util;
import com.madeira.entertainz.karaoke.DBLocal.DbAccess;
import com.madeira.entertainz.karaoke.DBLocal.TRadio;
import com.madeira.entertainz.karaoke.R;
import com.madeira.entertainz.library.PerformAsync2;

import java.io.File;
import java.util.Date;

import static com.madeira.entertainz.library.Util.getPackageName;

public class RadioDetailFragment extends Fragment implements RadioTheme.IRadioTheme {
    String TAG = "LiveChannelDetailFragment";
    View root;
    LineBarVisualizer lineBarVisualizer;
    ExoPlayer exoPlayer;
    RadioTheme radioTheme;
    View layoutDetail;
    ColorStateList colorSelection, colorText;
    ImageView logoImageView;
    TextView radioNameTV;
    TextView radioDescTV;
    ImageView prevImageView;
    ImageView playPauseImageView;
    ImageView nextButton;
    TRadio tRadio = new TRadio();
    boolean isPlaying = false;

    public static final String KEY_RADIOID = "KEY_CHANNEL";
    public static final String KEY_POSITION = "KEY_POSITION";

    private int radioId;
    private int position;

    private IRadioDetailListener iRadioDetailListener;

    public RadioDetailFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            radioId = getArguments().getInt(KEY_RADIOID);
            position = getArguments().getInt(KEY_POSITION);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        root = inflater.inflate(R.layout.fragment_radio_detail, container, false);
        bind();
        setActionObject();
        fetchRadio();
        return root;
    }

    void bind() {
        lineBarVisualizer = root.findViewById(R.id.visualizer);
        layoutDetail = root.findViewById(R.id.layout_detail);
        logoImageView = root.findViewById(R.id.logoImageView);
        radioNameTV = root.findViewById(R.id.radioNameTV);
        radioDescTV = root.findViewById(R.id.radioDescTV);
        prevImageView = root.findViewById(R.id.prevImageView);
        playPauseImageView = root.findViewById(R.id.playPauseImageView);
        nextButton = root.findViewById(R.id.nextButton);
        playPauseImageView.setImageResource(R.drawable.selector_radio_play);
    }

    void setActionObject() {
        prevImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                iRadioDetailListener.onPrev(radioId, position);

            }
        });

        playPauseImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isPlaying) {
                    exoPlayer.setPlayWhenReady(false);
                    exoPlayer.getPlaybackState();
                } else {
                    exoPlayer.setPlayWhenReady(true);
                    exoPlayer.getPlaybackState();
                }
            }
        });

        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                iRadioDetailListener.onNext(radioId, position);

            }
        });
    }

    void setExoPlayer(String url) {
        try {
            BandwidthMeter bandwidthMeter = new DefaultBandwidthMeter();
            final ExtractorsFactory extractorsFactory = new DefaultExtractorsFactory();
            TrackSelection.Factory trackSelectionFactory = new AdaptiveTrackSelection.Factory(bandwidthMeter);
//        DataSource.Factory dateSourceFactory = new DefaultDataSourceFactory(this, Util.getUserAgent(context, getPackageName()), bandwidthMeter);
            DataSource.Factory dataSourceFactory = new DefaultDataSourceFactory(getContext(), Util.getUserAgent(getContext(), getPackageName()));
            MediaSource mediaSource = new ExtractorMediaSource(Uri.parse(url), dataSourceFactory, extractorsFactory, new Handler(), Throwable::printStackTrace);    // replace Uri with your song url
            exoPlayer = ExoPlayerFactory.newSimpleInstance(getActivity(), new DefaultTrackSelector(trackSelectionFactory));
            exoPlayer.prepare(mediaSource);
            exoPlayer.setPlayWhenReady(true);
            exoPlayer.addListener(new Player.DefaultEventListener() {
                @Override
                public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
                    try {
                        if (playWhenReady && playbackState == Player.STATE_READY) {
                            // media actually playing
                            isPlaying = true;
                            playPauseImageView.setImageResource(R.drawable.selector_radio_pause);
                            Player.AudioComponent audioComponent = exoPlayer.getAudioComponent();
                            int audioSessionid = audioComponent.getAudioSessionId();
                            lineBarVisualizer.setColor(ContextCompat.getColor(getActivity(), R.color.colorSoyMilk));
                            lineBarVisualizer.setDensity(70);
                            lineBarVisualizer.setPlayer(audioSessionid);
                        } else if (playWhenReady) {
                            // might be idle (plays after prepare()),
                            // buffering (plays when data available)
                            // or ended (plays when seek away from end)
                            isPlaying = false;
                            playPauseImageView.setImageResource(R.drawable.selector_radio_play);
                        } else {
                            isPlaying = false;
                            playPauseImageView.setImageResource(R.drawable.selector_radio_play);
                        }
                    } catch (Exception ex) {
                        Log.d(TAG, ex.getMessage());
                    }
                }
            });

        } catch (Exception ex) {
            Log.d(TAG, ex.getMessage());
        }
    }

    public void releaseExoPlayer() {
        if (exoPlayer != null)
            exoPlayer.release();
    }

    void fetchRadio() {
        try {
            PerformAsync2.run(performAsync -> {
                DbAccess dbAccess = DbAccess.Instance.create(getActivity());
                if (radioId != 0)
                    tRadio = dbAccess.daoAccessTRadio().getRadioById(radioId);
                else {
                    TRadio firtRow = dbAccess.daoAccessTRadio().getFirstRowRadio();
                    tRadio = firtRow;
                }
                dbAccess.close();
                return 1;
            }).setCallbackResult(result -> {
                int i = (int) result;
                if (i != 1) {
                    return;
                } else {
                    //show detail
                    Uri uri = Uri.fromFile(new File(tRadio.urlImage));
                    logoImageView.setImageURI(uri);
                    radioNameTV.setText(tRadio.radioName);
                    radioDescTV.setText(tRadio.description);
                    setExoPlayer(tRadio.urlRadio);
                }
            });

        } catch (Exception ex) {
            android.util.Log.d(TAG, String.valueOf(ex.getMessage()));
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        releaseExoPlayer();
    }

    @Override
    public void setThemeUpdateDate(Date lastUpdateTheme) {

    }

    @Override
    public void setListColorTheme(int color) {
        layoutDetail.setBackgroundResource(R.drawable.tags_rounded_corners);
        GradientDrawable categoryDrawable = (GradientDrawable) layoutDetail.getBackground();
        categoryDrawable.setColor(color);
    }

    @Override
    public void setTextAndSelectionColorTheme(ColorStateList colorSelection, ColorStateList colorText, int colorTextSelected, int colorTextFocus, int colorTextNormal, int colorSelectionSelected, int colorSelectionFocus) {
        this.colorSelection = colorSelection;
        this.colorText = colorText;
    }

    public interface IRadioDetailListener {
        void onPrev(int prmRadioId, int position);

        void onNext(int prmRadioId, int position);
    }

    public void setCallback(IRadioDetailListener prmListener) {
        this.iRadioDetailListener = prmListener;
    }
}

package com.madeira.entertainz.karaoke.model;

import android.net.Uri;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;
import com.madeira.entertainz.library.Async;

import java.util.List;

public class ModelYoutubeInfo {
    private static final String TAG = "ModelYoutubeInfo";

    private static final String APIHOST = "https://www.googleapis.com/youtube/v3/videos?part=contentDetails";

    public interface OnGetList {
        void onGetList(List<items> list);

        void onError(Exception e);
    }

    public static final List<items> getYoutubeInfo(String youtubeId, String apiKey) throws Exception {
        String url = Uri.parse(APIHOST)
                .buildUpon()
                .appendQueryParameter("id", youtubeId)
                .appendQueryParameter("key", apiKey)
                .build().toString();

        Gson gson = new Gson();
        String result = ModelHelper.httpRequest(gson, url);

        ResultGetList r = gson.fromJson(result, ResultGetList.class);
        return r.itemsList;
    }

    public static final void getYoutubeInfo(String youtubeId, String apiKey, OnGetList callback) {

        Async.run(new Async.Callback() {
            @Override
            public Object onBackground(Async async) {

                try {

                    List<items> list = getYoutubeInfo(youtubeId, apiKey);

                    return list;

                } catch (Exception e) {
//                    Debug.e(TAG, e);
                    return e;
                }
            }
        }).setCallbackResult(new Async.CallbackResult() {
            @Override
            public void onResult(Object result) {
                if (result instanceof Exception) {
                    callback.onError((Exception) result);
                } else {
                    callback.onGetList((List<items>) result);
                }
            }
        });
    }

    public class ResultGetList {
        @SerializedName("items")
        public List<items> itemsList = null;
    }

    public class items {
        @SerializedName("kind")
        public String kind;

        @SerializedName("etag")
        public String etag;

        @SerializedName("id")
        public String id;

        @SerializedName("contentDetails")
        public details contentDetails;
    }

    public class details {
        @SerializedName("duration")
        public String duration;

        @SerializedName("dimension")
        public String dimension;

        @SerializedName("definition")
        public String definition;

        @SerializedName("caption")
        public String caption;

        @SerializedName("licensedContent")
        public boolean licensedContent;

        @SerializedName("projection")
        public String projection;
    }

//    public class Ads {
//
//        @SerializedName("ads_id")
//        public String adsId;
//
//        @SerializedName("spot_id")
//        public String spotId;
//
//        @SerializedName("url_image")
//        public String urlImage;
//
//        @SerializedName("show_counter")
//        public String showCounter;
//
//        @SerializedName("active_date")
//        public Object activeDate;
//
//        @SerializedName("expire_date")
//        public Object expireDate;
//
//        @SerializedName("create_date")
//        public String createDate;
//
//        @SerializedName("update_date")
//        public String updateDate;
//
//        @SerializedName("system_type")
//        public String systemType;
//    }

}

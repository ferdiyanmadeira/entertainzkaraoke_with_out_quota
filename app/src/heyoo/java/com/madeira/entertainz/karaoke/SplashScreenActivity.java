package com.madeira.entertainz.karaoke;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.admin.DevicePolicyManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.os.BatteryManager;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.agrawalsuneet.dotsloader.loaders.LinearDotsLoader;
import com.agrawalsuneet.dotsloader.loaders.ZeeLoader;
import com.kinda.alert.KAlertDialog;
import com.madeira.entertainz.karaoke.DBLocal.DbAccess;
import com.madeira.entertainz.karaoke.DBLocal.TElement;
import com.madeira.entertainz.karaoke.DBLocal.TElementMedia;
import com.madeira.entertainz.karaoke.DBLocal.TLocalFile;
import com.madeira.entertainz.karaoke.DBLocal.TLocalFileAPK;
import com.madeira.entertainz.karaoke.DBLocal.TMessage;
import com.madeira.entertainz.karaoke.DBLocal.TMessageMedia;
import com.madeira.entertainz.karaoke.DBLocal.TRadio;
import com.madeira.entertainz.karaoke.DBLocal.TSettingElementMedia;
import com.madeira.entertainz.karaoke.DBLocal.TSong;
import com.madeira.entertainz.karaoke.DBLocal.TSongCategory;
import com.madeira.entertainz.karaoke.DBLocal.TSticker;
import com.madeira.entertainz.karaoke.config.C;
import com.madeira.entertainz.karaoke.config.CacheData;
import com.madeira.entertainz.karaoke.config.Global;
import com.madeira.entertainz.karaoke.menu_main.MainActivity;
import com.madeira.entertainz.karaoke.menu_setting_admin.SettingAdminActivity;
import com.madeira.entertainz.karaoke.model.ModelMessageLocal;
import com.madeira.entertainz.karaoke.model.ModelRadioLocal;
import com.madeira.entertainz.karaoke.model.ModelSongLocal;
import com.madeira.entertainz.karaoke.model.ModelStickerLocal;
import com.madeira.entertainz.karaoke.model.ModelThemeLocal;
import com.madeira.entertainz.library.KeyGuard;
import com.madeira.entertainz.library.PerformAsync2;
import com.madeira.entertainz.library.Util;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import static android.content.Intent.ACTION_MAIN;
import static android.content.Intent.CATEGORY_DEFAULT;
import static android.content.Intent.CATEGORY_HOME;

public class SplashScreenActivity extends AppCompatActivity {

    ZeeLoader zeeLoader;
    String TAG = "SplashScreenActivity";
    KeyGuard kg = new KeyGuard(C.PIN_FOR_SETTING);
    LinearDotsLoader linearDotsLoader;

    public static final String KEY_INTENT_FROM_OTT = "KEY_INTENT_FROM_OTT";
    int intentFromOTT = 0;

    public static void startActivity(Activity activity) {
        Intent intent = new Intent(activity, SplashScreenActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        activity.startActivity(intent);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        bind();
        /*AndPermission.with(this)
                .runtime()
                .permission(Permission.Group.STORAGE)
                .onGranted(permissions -> {
                    fetchContent();
                    hideSystemUI();
                })
                .onDenied(permissions -> {
                })
                .start();*/
        if (BuildConfig.DEBUG == false)
            setDeviceOwner(true);
        else
            setDeviceOwner(false);
        hideSystemUI();
        forcePermission();
        forceWifiOn();
//        copyAsset();
        intentFromOTT = getIntent().getIntExtra(KEY_INTENT_FROM_OTT, 0);
       /* String unique_id = Util.getMACAddress("wlan0");
        String unique_id2 = Util.getMACAddress("eth0");
        Log.d(TAG, unique_id);

        try {
//            String sdcard = "/mnt/sdcard/usb";
//            String sdcard = Environment.getExternalStorageDirectory().getAbsolutePath() + "/usb";
//            String sdcard = "/storage/DA18-EBFA";
//            String sdcard = "/mnt/media_rw/DA18-EBFA";
//            String sdcard = "/mnt/runtime/write/DA18-EBFA ";

//            writeToFile("/data/usb/data/mr/test.txt", "I love my country and my people", 31);
//            writeToFile("/data/usb/test.txt", "I love my country and my people", 31);


//            String sdcard = Environment.getExternalStorageDirectory().getAbsolutePath();
//            writeToFile("Hello World", sdcard + "/test.txt");

//            writeToFile(sdcard + "/file.txt", "i love you 3000", 1);

//            String pathFile ="/data/mr/1/ADELE # SEND MY LOVE#Barat#Right.des";
//            String filePath = Util.splitFileAndReplace(pathFile, 2000000);

//            generateNoteOnSD("/mnt/usb/data/mr/test.txt");
        } catch (Exception ex) {
            ex.printStackTrace();
        }*/
    }


    void bind() {
        zeeLoader = findViewById(R.id.zeeLoader);
        linearDotsLoader = findViewById(R.id.linearDotsLoader);
    }

    void fetchContent() {
//        zeeLoader.setVisibility(View.VISIBLE);
        linearDotsLoader.setVisibility(View.VISIBLE);
        PerformAsync2.run(performAsync ->
        {
            int result = 0;
            try {
                //get semua data dari fd
                List<TLocalFileAPK> localFileAPKList = new ArrayList<>();
                List<TLocalFile> tLocalFileList = Util.getUSBFile("");
                List<TLocalFile> tLocalFileListJson = Util.getUSBFile("json");
                tLocalFileList.addAll(tLocalFileListJson);

                for (TLocalFile item : tLocalFileList) {
                    //fetch apk file
                    if (item.contentType.equals("apk")) {
                        TLocalFile tLocalFile = CacheData.hashFile.get(item.fileName);
                        if (tLocalFile == null) {
                            TLocalFileAPK tLocalFileAPK = new TLocalFileAPK();
                            tLocalFileAPK.id = item.id;
                            tLocalFileAPK.content = item.content;
                            tLocalFileAPK.contentType = item.contentType;
                            tLocalFileAPK.fileName = item.fileName;
                            tLocalFileAPK.fileType = item.fileType;
                            localFileAPKList.add(tLocalFileAPK);
                        }
                    }
                    CacheData.hashFile.put(item.fileName, item);
                }

                List<TElement> tElements = new ArrayList<>();
                List<TElementMedia> tElementMedia = new ArrayList<>();
                List<TSongCategory> tSongCategories = new ArrayList<>();
                List<TSong> tSongList = new ArrayList<>();
                List<TMessage> listMessage = new ArrayList<TMessage>();
                List<TMessageMedia> listMessageMedia = new ArrayList<TMessageMedia>();
                List<TSticker> tStickerList = new ArrayList<>();
                List<TRadio> tRadioList = new ArrayList<>();


                try {
                    TLocalFile tLocalFileTheme = CacheData.hashFile.get(C.JSON_THEME);
                    ModelThemeLocal.ResultGetThemeList resultGetThemeList = ModelThemeLocal.getThemeFromLocal(tLocalFileTheme.content);
                    tElements = resultGetThemeList.list;

                    //get data theme image
                    List<TElementMedia> orilisTElementMedia = resultGetThemeList.media;
                    for (Iterator<TElementMedia> itemIterator = orilisTElementMedia.iterator(); itemIterator.hasNext(); ) {
                        TElementMedia item = itemIterator.next();
                        TLocalFile tLocalFile = CacheData.hashFile.get(item.urlImage);
                        if (tLocalFile != null) {
                            TElementMedia TElementMedia = new TElementMedia();
                            TElementMedia = item;
                            if (item.urlImage != null && item.urlImage.equals(tLocalFile.fileName)) {
                                TElementMedia.urlImage = tLocalFile.content;
                                tElementMedia.add(TElementMedia);
                            }
                        }
//                    }
                    }
                } catch (Exception ex) {
                    Log.d(TAG, ex.getMessage());
                    result = 0;
                }

                // untuk check data keberapa yang ada trouble
                int i = 0;
                try {
                    TLocalFile tLocalFileSongCategories = CacheData.hashFile.get(C.JSON_SONG_CATEGORY);
                    tSongCategories = ModelSongLocal.geTSongCategoryFromLocal(tLocalFileSongCategories.content);

                    TLocalFile tLocalFileSong = CacheData.hashFile.get(C.JSON_SONG);
                    List<TSong> oriTSongList = ModelSongLocal.geTSongFromLocal(tLocalFileSong.content);
                    for (Iterator<TSong> TSongIterator = oriTSongList.iterator(); TSongIterator.hasNext(); ) {
                        TSong itemTsong = TSongIterator.next();
                        i++;
                        TLocalFile tLocalFileVideo = CacheData.hashFile.get(itemTsong.songURL);
                        if (tLocalFileVideo != null) {
                            TSong tempTSong = new TSong();
                            tempTSong = itemTsong;
                            tempTSong.songURL = tLocalFileVideo.content;
                            TLocalFile tLocalFileImage = CacheData.hashFile.get(itemTsong.thumbnail);
                            if (tLocalFileImage != null)
                                tempTSong.thumbnail = tLocalFileImage.content;
                            tSongList.add(tempTSong);
                        }
                    }
                } catch (Exception ex) {
                    Log.d(TAG, " item " + String.valueOf(i));
                    result = 0;

                }


                //get data message & message media from fd
                TLocalFile tLocalFileMessage = CacheData.hashFile.get(C.JSON_MESSAGE);
                if (tLocalFileMessage != null) {
                    ModelMessageLocal.ResultGetMessageList resultGetMessageList = ModelMessageLocal.geMessageFromLocal(tLocalFileMessage.content);
                    listMessage = resultGetMessageList.list;
                    List<TMessageMedia> orilistMessageMedia = resultGetMessageList.medias;
                    for (Iterator<TMessageMedia> itemIterator = orilistMessageMedia.iterator(); itemIterator.hasNext(); ) {
                        TMessageMedia item = itemIterator.next();
                        TMessageMedia tMessageMedia = new TMessageMedia();
                        tMessageMedia = item;
                        if (item.urlImage != null) {
                            TLocalFile tLocalFile = CacheData.hashFile.get(tMessageMedia.urlImage);
                            tMessageMedia.urlImage = tLocalFile.content;
                            listMessageMedia.add(tMessageMedia);
                        } else if (item.urlVideo != null) {
                            TLocalFile tLocalFile = CacheData.hashFile.get(tMessageMedia.urlVideo);
                            tMessageMedia.urlVideo = tLocalFile.content;
                            listMessageMedia.add(tMessageMedia);
                        }
                    }
                }

                //fetch sticker itemsList
                try {
                    TLocalFile tLocalFileTheme = CacheData.hashFile.get(C.JSON_STICKER);
                    ModelStickerLocal.ResultGetStickerList resultGetStickerList = ModelStickerLocal.getStickerFromLocal(tLocalFileTheme.content);
                    List<TSticker> oriTStickerList = resultGetStickerList.list;
                    for (Iterator<TSticker> TSongIterator = oriTStickerList.iterator(); TSongIterator.hasNext(); ) {
                        TSticker itemTSticker = TSongIterator.next();
                        TLocalFile tlocalImage = CacheData.hashFile.get(itemTSticker.urlImage);
                        if (tlocalImage != null) {
                            TSticker tempTSticker = new TSticker();
                            tempTSticker = itemTSticker;
                            tempTSticker.urlImage = tlocalImage.content;
                            tStickerList.add(tempTSticker);
                        }
                    }

                } catch (Exception ex) {
                    Log.d(TAG, ex.getMessage());
                    result = 0;
                }

                //fetch radio itemsList
                try {
                    TLocalFile tLocalFileTheme = CacheData.hashFile.get(C.JSON_RADIO);
                    ModelRadioLocal.ResultGetRadioList resultGetRadioList = ModelRadioLocal.getRadioFromLocal(tLocalFileTheme.content);
                    List<TRadio> oriTRadioList = resultGetRadioList.list;
                    for (Iterator<TRadio> TSongIterator = oriTRadioList.iterator(); TSongIterator.hasNext(); ) {
                        TRadio itemTRadio = TSongIterator.next();
                        TLocalFile tlocalImage = CacheData.hashFile.get(itemTRadio.urlImage);
                        if (tlocalImage != null) {
                            TRadio tempTRadio = new TRadio();
                            tempTRadio = itemTRadio;
                            tempTRadio.urlImage = tlocalImage.content;
                            tRadioList.add(tempTRadio);
                        }
                    }

                } catch (Exception ex) {
                    Log.d(TAG, ex.getMessage());
                    result = 0;
                }

                DbAccess dbAccess = DbAccess.Instance.create(SplashScreenActivity.this);
                if (tElements.size() != 0) {
                    dbAccess.daoAccessTheme().deleteAll();
                    dbAccess.daoAccessTheme().insertAll(tElements);
                } else {
                    //load theme dari db
                    tElements = dbAccess.daoAccessTheme().getAll();
                }
                CacheData.listElement = tElements;

                //pindahin semua ke hashtable utk faster search
                if (tElements.isEmpty() == false) {
                    for (TElement item : tElements) {
                        CacheData.hashElement.put(item.elementId, item);
                    }
                }

                if (tElementMedia != null) {
                    for (TElementMedia item : tElementMedia) {
                        int exist = dbAccess.daoAccessThemeMedia().checkExist(item.elementMediaId);
                        if (exist != 0) {
                            dbAccess.daoAccessThemeMedia().updateElementMedia(item.urlImage, item.elementMediaId);
                        } else {
                            dbAccess.daoAccessThemeMedia().insert(item);

                        }
                        int checkExistSetting = dbAccess.daoAccessThemeMedia().checkExistSetting(item.elementMediaId);
                        if (checkExistSetting == 0) {
                            TSettingElementMedia tSettingElementMedia = new TSettingElementMedia();
                            tSettingElementMedia.elementMediaId = item.elementMediaId;
                            tSettingElementMedia.duration = item.duration;
                            tSettingElementMedia.isActive = 1;
                            dbAccess.daoAccessThemeMedia().insertSetting(tSettingElementMedia);
                        }
                    }
                }

                dbAccess.daoAccessTSongCategory().deleteAll();
                if (tSongCategories != null) {
                    if (tSongCategories.size() != 0) {
                        dbAccess.daoAccessTSongCategory().insertAll(tSongCategories);
                        result = 1;
                    }
                }

                dbAccess.daoAccessTSong().deleteAll();
                if (tSongList != null) {
                    if (tSongList.size() != 0) {
                        dbAccess.daoAccessTSong().insertAll(tSongList);
                        result = 1;
                    }
                }
                if (listMessage != null) {
                    if (listMessage.size() != 0) {
                        dbAccess.daoAccessMessage().deleteAll();
                        dbAccess.daoAccessMessage().insertAll(listMessage);
                        result = 1;
                    }
                }

                if (listMessageMedia != null) {
                    if (listMessageMedia.size() != 0) {
//                    dbAccess.daoAccessMessage().deleteAllMedia();
                        dbAccess.daoAccessMessage().insertAllMedia(listMessageMedia);
                        result = 1;
                    }
                }
                if (tStickerList != null) {
                    if (tStickerList.size() != 0) {
                        dbAccess.daoAccessSticker().deleteAll();
                        dbAccess.daoAccessSticker().insertAll(tStickerList);
                        result = 1;
                    }
                }

                if (tRadioList != null) {
                    if (tRadioList.size() != 0) {
                        dbAccess.daoAccessTRadio().deleteAll();
                        dbAccess.daoAccessTRadio().insertAll(tRadioList);
                        result = 1;
                    }
                }
                if (localFileAPKList != null) {
                    if (localFileAPKList.size() != 0) {
                        dbAccess.daoAccessTLocalFileAPK().deleteAll();
                        dbAccess.daoAccessTLocalFileAPK().insertAll(localFileAPKList);
                        result = 1;
                    }
                }
            } catch (Exception ex) {
                result = 0;
            }
            return result;
        }).

                setCallbackResult(result ->

                {
                    int i = (int) result;
                    if (i != 1) {
//                        showWarningUSBUnplug(SplashScreenActivity.this, KAlertDialog.WARNING_TYPE, getString(R.string.usbStorage), getString(R.string.plugUSB));
                        showUSBUnplug();
                        return;
                    } else {
                        Global.setConnectedUSB(1);
                        MainActivity.startActivity(SplashScreenActivity.this, intentFromOTT);
                        finish();
                    }

                });
    }

    private void hideSystemUI() {
        View decorView = getWindow().getDecorView();
        decorView.setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION // hide nav bar
                        | View.SYSTEM_UI_FLAG_FULLSCREEN // hide status bar
                        | View.SYSTEM_UI_FLAG_IMMERSIVE);
    }

    protected void showWarningUSBUnplug(Context context, int dialogType, String titleText, String contentText) {
//        zeeLoader.setVisibility(View.GONE);
        linearDotsLoader.setVisibility(View.GONE);
        KAlertDialog pDialog = new KAlertDialog(context, dialogType);
        pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        pDialog.setTitleText(titleText);
        pDialog.setContentText(contentText);
        pDialog.setCancelable(true);
        pDialog.setConfirmText(getString(R.string.rescan));
        pDialog.setCancelText(getString(R.string.cancel));
        pDialog.setConfirmClickListener(new KAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(KAlertDialog kAlertDialog) {
                pDialog.dismissWithAnimation();
                fetchContent();
            }
        });
        pDialog.setCancelClickListener(new KAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(KAlertDialog kAlertDialog) {
                Global.setConnectedUSB(0);
                MainActivity.startActivity(SplashScreenActivity.this, intentFromOTT);
            }
        });
        pDialog.show();
    }

    void showUSBUnplug() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(SplashScreenActivity.this);
        LayoutInflater inflater = this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.alert_rescan_usb, null);
        dialogBuilder.setView(dialogView);

        TextView textView = (TextView) dialogView.findViewById(R.id.textView);
        Button rescanButton = (Button) dialogView.findViewById(R.id.rescanButton);
        Button cancelButton = (Button) dialogView.findViewById(R.id.cancelButton);
        textView.setText(getString(R.string.plugUSB));

        AlertDialog alertDialog = dialogBuilder.create();

        rescanButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                fetchContent();
            }
        });

        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                Global.setConnectedUSB(0);
                MainActivity.startActivity(SplashScreenActivity.this, intentFromOTT);
                finish();
            }
        });

        alertDialog.show();
        WindowManager.LayoutParams layoutParams = alertDialog.getWindow().getAttributes();
        layoutParams.width = ViewGroup.LayoutParams.WRAP_CONTENT;
        alertDialog.show();
        alertDialog.getWindow().setLayout(430, WindowManager.LayoutParams.WRAP_CONTENT);
        Util.hideNavigationBar(SplashScreenActivity.this);

    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {

        switch (keyCode) {
            case KeyEvent.KEYCODE_BACK:
                Toast.makeText(this, "Key back", Toast.LENGTH_SHORT).show();
                return true;
            case KeyEvent.KEYCODE_DPAD_CENTER:
                fetchContent();
        }

        if (kg.processKey(keyCode)) {
            SettingAdminActivity.startActivity(this);
        }

        return super.onKeyDown(keyCode, event);
    }

    void enableKioskMode() {
        ComponentName componentName;
        DevicePolicyManager devicePolicyManager;

        componentName = MyDeviceAdminReceiver.getComponentName(this);
        devicePolicyManager = (DevicePolicyManager) getSystemService(Context.DEVICE_POLICY_SERVICE);
        //check apakah sdh dalam kiosk mode?
        if (devicePolicyManager.isDeviceOwnerApp(BuildConfig.APPLICATION_ID) == false) {
            hideSystemUI();
            Log.i(TAG, "NORMAL mode");
            return;
        }

        Log.i(TAG, "KIOSK mode");

        String[] ar = {BuildConfig.APPLICATION_ID, C.YOUTUBE_ID, C.APPLICATION_ID_FILEMANAGER, C.APPLICATION_ID_VIDIO};
        devicePolicyManager.setLockTaskPackages(componentName, ar);
        startLockTask();

//            devicePolicyManager.setLockTaskFeatures(componentName,
//                    DevicePolicyManager.LOCK_TASK_FEATURE_NONE);

        //set our app as default app
        IntentFilter intentFilter = new IntentFilter(ACTION_MAIN);
        intentFilter.addCategory(CATEGORY_HOME);
        intentFilter.addCategory(CATEGORY_DEFAULT);
        devicePolicyManager.addPersistentPreferredActivity(componentName, intentFilter, new ComponentName(BuildConfig.APPLICATION_ID, SplashScreenActivity.class.getName()));

        //disable keyguarg, shg saat reboot otomatis masuk ke app
//            devicePolicyManager.setKeyguardDisabled(componentName, true);

        //screen on always
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        //never dimmed walaupun di charge
        int opt = BatteryManager.BATTERY_PLUGGED_AC | BatteryManager.BATTERY_PLUGGED_USB | BatteryManager.BATTERY_PLUGGED_WIRELESS;

        hideSystemUI();

    }

    void setDeviceOwner(boolean deviceOwner) {
        try {
            String packageName = getApplicationContext().getPackageName();

            Process p = Runtime.getRuntime().exec("su");
            DataOutputStream dos = new DataOutputStream(p.getOutputStream());
//                dos.writeBytes("pm enable com.droidlogic.mboxlauncher\n");

            dos.writeBytes("adb shell\n");
            if (deviceOwner) {
//                    dos.writeBytes("pm enable com.droidlogic.mboxlauncher\n");
                dos.writeBytes("dpm set-device-owner " + packageName + "/com.madeira.entertainz.karaoke.MyDeviceAdminReceiver\n");

            } else {
                dos.writeBytes("dpm remove-active-admin  " + packageName + "/com.madeira.entertainz.karaoke.MyDeviceAdminReceiver\n");

            }
          /*  if (deviceOwner) {
//                    dos.writeBytes("pm enable com.droidlogic.mboxlauncher\n");
                dos.writeBytes("dpm set-device-owner " + "com.madeira.entertainz.karaoke" + "/.MyDeviceAdminReceiver\n");

            } else {
                dos.writeBytes("dpm remove-active-admin  " + "com.madeira.entertainz.karaoke" + "/.MyDeviceAdminReceiver\n");

            }*/
            dos.flush();
            dos.close();
            p.waitFor();
            BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));
            BufferedReader stdError = new BufferedReader(new
                    InputStreamReader(p.getErrorStream()));
            StringBuffer output = new StringBuffer();
            StringBuffer errorSB = new StringBuffer();

            String result = "";
            String line = "";
            while ((line = reader.readLine()) != null) {
                output.append(line + "n");
            }

            while ((line = stdError.readLine()) != null) {
                errorSB.append(line + "; ");
            }
            result = output.toString() + errorSB.toString();


            if (deviceOwner)
                enableKioskMode();
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
            Log.d(TAG, e.getMessage());

        }
    }

    void forcePermission() {
        try {

            String packageName = getApplicationContext().getPackageName();
            Process p = Runtime.getRuntime().exec("su");
            DataOutputStream dos = new DataOutputStream(p.getOutputStream());

            dos.writeBytes("adb shell\n");
            dos.writeBytes("pm grant " + packageName + " android.permission.WRITE_EXTERNAL_STORAGE\n");
            dos.writeBytes("pm grant " + packageName + " android.permission.READ_EXTERNAL_STORAGE\n");
            dos.writeBytes("pm grant " + packageName + " android.permission.RECORD_AUDIO\n");
//            dos.writeBytes("pm grant " + packageName + " android.permission.MANAGE_DOCUMENTS\n");
            dos.writeBytes("pm grant " + packageName + " android.permission.INSTALL_PACKAGES\n");
            dos.writeBytes("pm grant " + packageName + " android.permission.WRITE_MEDIA_STORAGE\n");


            dos.flush();
            dos.close();
            p.waitFor();
            BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));
            StringBuffer output = new StringBuffer();
            StringBuffer errorSB = new StringBuffer();

            BufferedReader stdError = new BufferedReader(new
                    InputStreamReader(p.getErrorStream()));

            String line = "";
            String result = "";
            while ((line = reader.readLine()) != null) {
                output.append(line + "; ");
            }

            while ((line = stdError.readLine()) != null) {
                errorSB.append(line + "; ");
            }

            result = output.toString() + errorSB.toString();

            Log.d(TAG, result);
            fetchContent();

        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
            Log.d(TAG, e.getMessage());

        }
    }

    void forceWifiOn() {
        try {

            Process p = Runtime.getRuntime().exec("su");
            DataOutputStream dos = new DataOutputStream(p.getOutputStream());

            dos.writeBytes("adb shell\n");
            dos.writeBytes("svc wifi enable\n");
//            dos.writeBytes("pm grant com.madeira.entertainz.karaoke android.permission.READ_EXTERNAL_STORAGE\n");

            dos.flush();
            dos.close();
            p.waitFor();
            BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));
            StringBuffer output = new StringBuffer();
            StringBuffer errorSB = new StringBuffer();

            BufferedReader stdError = new BufferedReader(new
                    InputStreamReader(p.getErrorStream()));

            String line = "";
            String result = "";
            while ((line = reader.readLine()) != null) {
                output.append(line + "; ");
            }

            while ((line = stdError.readLine()) != null) {
                errorSB.append(line + "; ");
            }

            result = output.toString() + errorSB.toString();

//            Toast.makeText(getApplicationContext(), result, Toast.LENGTH_LONG).show();
            Log.d(TAG, result);

        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
            Log.d(TAG, e.getMessage());

        }
    }

    void setWritableFolderData() {
        try {

            Process p = Runtime.getRuntime().exec("su");
            DataOutputStream dos = new DataOutputStream(p.getOutputStream());
            dos.writeBytes("chmod 775 /data\n");
//            dos.writeBytes("pm grant com.madeira.entertainz.karaoke android.permission.READ_EXTERNAL_STORAGE\n");

            dos.flush();
            dos.close();
            p.waitFor();
            BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));
            StringBuffer output = new StringBuffer();
            StringBuffer errorSB = new StringBuffer();

            BufferedReader stdError = new BufferedReader(new
                    InputStreamReader(p.getErrorStream()));

            String line = "";
            String result = "";
            while ((line = reader.readLine()) != null) {
                output.append(line + "; ");
            }

            while ((line = stdError.readLine()) != null) {
                errorSB.append(line + "; ");
            }

            result = output.toString() + errorSB.toString();

//            Toast.makeText(getApplicationContext(), result, Toast.LENGTH_LONG).show();
            Log.d(TAG, result);

        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
            Log.d(TAG, e.getMessage());

        }
    }

    @Override
    protected void onResume() {
        super.onResume();
//        if (intentFromOTT == 0)
//        fetchContent();
//        else
//            finish();

    }

}

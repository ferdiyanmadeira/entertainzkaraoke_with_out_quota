package com.madeira.entertainz.karaoke.menu_radio;

import android.content.Context;
import android.content.res.ColorStateList;

import com.madeira.entertainz.karaoke.DBLocal.TElement;
import com.madeira.entertainz.karaoke.R;
import com.madeira.entertainz.karaoke.config.C;
import com.madeira.entertainz.karaoke.config.CacheData;

import java.util.Date;

public class RadioTheme {
    String TAG="RADIOTheme";
    public IRadioTheme iRadioTheme;
    public Context context;
    public Date lastUpdateTheme;

    public interface IRadioTheme {
        void setThemeUpdateDate(Date lastUpdateTheme);
        void setListColorTheme(int color);
        void setTextAndSelectionColorTheme(ColorStateList colorSelection, ColorStateList colorText, int colorTextSelected, int colorTextFocus, int colorTextNormal, int colorSelectionSelected, int colorSelectionFocus);
    }

    public RadioTheme(Context context, IRadioTheme callback){
//        this.iRadioTheme = iRadioTheme;
        iRadioTheme = callback;
        this.context = context;
    }

    public void setTheme(){
        lastUpdateTheme = new Date();

        //exit apabila theme kosong
        if (CacheData.hashElement==null || CacheData.hashElement.isEmpty()) return;

        setListColor();
        setTextAndSelectionColor();
        iRadioTheme.setThemeUpdateDate(lastUpdateTheme);
    }



    public void setListColor(){
        TElement element;
        int color = context.getResources().getColor(R.color.theme_main_list_background_color);

        element = CacheData.hashElement.get(C.THEME_RADIO_LIST_BACKGROUND);
        if (element!=null) {
            color = element.parseColor();
        }

        iRadioTheme.setListColorTheme(color);
    }



    public void setTextAndSelectionColor(){
        ColorStateList colorSelection, colorText;

        TElement elementTextSelected, elementTextFocus, elementTextNormal, elementSelectionSelected, elementSelectionFocus;
        int colorTextSelected = context.getResources().getColor(R.color.theme_main_text_selected);
        int colorTextFocus = context.getResources().getColor(R.color.theme_main_text_focus);
        int colorTextNormal = context.getResources().getColor(R.color.theme_main_text_color);
        int colorSelectionSelected = context.getResources().getColor(R.color.theme_main_selection_selected);
        int colorSelectionFocus = context.getResources().getColor(R.color.theme_main_selection_focus);

        elementTextSelected = CacheData.hashElement.get(C.THEME_RADIO_TEXTCOLOR_SELECTED);
        elementTextFocus = CacheData.hashElement.get(C.THEME_RADIO_TEXTCOLOR_FOCUS);
        elementTextNormal = CacheData.hashElement.get(C.THEME_RADIO_TEXTCOLOR_NORMAL);

        elementSelectionSelected = CacheData.hashElement.get(C.THEME_RADIO_LIST_SELECTION_SELECTED);
        elementSelectionFocus = CacheData.hashElement.get(C.THEME_RADIO_LIST_SELECTION_FOCUS);

        if (elementTextSelected!=null) colorTextSelected = elementTextSelected.parseColor();
        if (elementTextFocus!=null) colorTextFocus = elementTextFocus.parseColor();
        if (elementTextNormal!=null) colorTextNormal = elementTextNormal.parseColor();
        if (elementSelectionSelected!=null) colorSelectionSelected = elementSelectionSelected.parseColor();
        if (elementSelectionFocus!=null) colorSelectionFocus = elementSelectionFocus.parseColor();

        colorSelection = new ColorStateList(
                new int[][]{
                        new int[]{android.R.attr.state_selected},
                        new int[]{android.R.attr.state_focused}
                },
                new int[] {
                        colorSelectionSelected,
                        colorSelectionFocus
                }
        );

        colorText = new ColorStateList(
                new int[][]{
                        new int[]{android.R.attr.state_selected},
                        new int[]{android.R.attr.state_focused},
                        new int[]{}
                },
                new int[] {
                        colorTextSelected,
                        colorTextFocus,
                        colorTextNormal
                }
        );

        iRadioTheme.setTextAndSelectionColorTheme(colorSelection, colorText, colorTextSelected, colorTextFocus, colorTextNormal, colorSelectionSelected, colorSelectionFocus);
    }


}
